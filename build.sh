#!/bin/bash -e

echo "===> Compiling DATE monitoring library..."
cd date
DATEDIR=$(pwd)
rm -rf monitoring/Linux lib include
./build.sh

echo "===> Compiling eventDumpAllNew..."
cd eventDumpAllNew
make clean
make
cd ../..

echo "===> Compiling libsdc..."
if [[ "${SDC_DIR}" == "" ]] ; then
    rm -rf sdc
    git clone --depth 1 --branch v0.2 https://github.com/CrankOne/sdc.git
    make -C sdc install
fi

echo "===> Configuring Coral..."
cd coral
export ROOTSYS=$(root-config --prefix)
./configure --without-ORACLE --without-MySQL --disable-CERN_LIBRARY --disable-HIGZ --disable-HBOOK --disable-ZEBRA --without-CLHEP --without-RFIO --without-XROOTD --without-EXPAT --without-TGEANT
rm -rf lib/Linux
mkdir -p lib/Linux
echo ""

echo "===> Compiling GEM library..."
cd src/geom/gem
touch dummy0.pcm
make clean
make -j1 LIB_TYPE=static
echo ""

echo "===> Compiling MuMega library..."
cd ../mumega
touch dummy1.pcm
make clean
make -j1 LIB_TYPE=static
echo ""

echo "===> Compiling decoding library..."
cd ../../DaqDataDecoding
./configure --with-DATE_LIB=$DATEDIR
make clean
make -j4
echo ""

echo "===> Compiling P348 library..."
cd ../../../p348reco
make clean
make libp348.a
echo ""

echo "===> Compiling MurphyTV..."
cd ../coral/src/DaqDataDecoding/examples/MurphyTV
make clean
make

echo "===> Compiling Coool..."
cd ../../../coool
./configure --with-ROOT=$ROOTSYS
make clean
make -j4
