# this script converts the cppcheck text report to the .json format
# https://gitlab.cern.ch/help/ci/testing/code_quality.md#implement-a-custom-tool
#
# (debug tip) verify output .json with
#   https://jsonformatter.curiousconcept.com/

BEGIN {
  print "["
}

function getoneline(fname, lineno) {
  i=0
  
  while ((getline line < fname) > 0) {
    i++
    if (i == lineno) {
      // remove stray CR char
      gsub("\r", "", line)
      
      close(fname)
      return line
    }
  }
  
  close(fname)
  return ""
}

function sha1text(text) {
  # remove ' and " to avoid shell command escaping problems
  gsub("'", "", text)
  gsub("\"", "", text)
  
  cmd = "echo '" text "' | sha1sum"
  
  if ((cmd | getline sha1) > 0) {
    sub(/ .*/, "", sha1)
  }
  else {
    print "ERROR: sha1text() failed to hash text"
    exit 1
  }
  
  close(cmd)
  return sha1
}

# convert severity string from cppcheck to gitlab notation
function cppcheck2gitlab(text) {
  if (text == "error") {return "critical"}
  if (text == "warning") {return "major"}
  if (text == "performance") {return "minor"}
  if (text == "portability") {return "minor"}
  
  # information or style
  return "info"
}

{
  if(NR!=1) print ","
  
  # input line format: "{file};{line};{severity};{id};{message}"
  
  contexthash = sha1text(getoneline($1, $2) $3 $4 $5)
  
  # remove the char `"` to avoid json format break
  # TODO: proper escaping
  msg = $5
  gsub("\"", "", msg)
  
  severity = cppcheck2gitlab($3)
  
  printf "{\"description\":\"(%s) %s\", \"fingerprint\":\"%s\", \"severity\":\"%s\", \"check_name\":\"%s\", \"location\": {\"path\":\"%s\", \"lines\": {\"begin\":%d } } }\n", $3, msg, contexthash, severity, $4, $1, $2
}

END {
  print"]"
}
