/* dateCleanup.c : utility to perform a complete cleanup of all DATE
 * resources on a given set of machines.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>

#define TIMEOUT 15
#ifndef TRUE
 #define TRUE (0 == 0 )
#endif
#ifndef FALSE
 #define FALSE (0 == 1)
#endif

const char * const rshs[] = {
  "/usr/bin/rsh",
  "/usr/local/bin/rsh",
  NULL
};
const char *rshCommand;
int verbose = FALSE;
struct hostDescStruct {
  void  *next;
  char  *hostname;
  pid_t  pid;
} *hosts = NULL;
char *myName;

void usage() {
  fprintf( stderr, "Usage: %s [-h] [-q] [-d] hostName1 hostName2 ..\n\
\t-h: print help page and exit\n\
\t-q: quiet\n\
\t-d: debug\n", myName );
  exit(1);
}

void parseArgs( const int argc, char ** const argv ) {
  int i;
  struct hostDescStruct *curr = NULL, *new;

  if ( argc <= 1 ) usage();
  for ( i = 1; i != argc; i++ ) {
    if ( strcmp( argv[i], "-h" ) == 0 ) {
      usage();
    } else if ( strcmp( argv[i], "-q" ) == 0 ) {
      freopen( "/dev/null", "w", stdout );
    } else if ( strcmp( argv[i], "-d" ) == 0 ||
		strcmp( argv[i], "-v" ) == 0 ) {
      verbose = TRUE;
    } else if ( argv[i][0] == '-' ) {
      usage();
    } else {
      if ( (new = malloc( sizeof( *new ) )) == NULL ) {
	perror( "malloc" );
	exit( 1 );
      }
      if ( (new->hostname = strdup( argv[i] )) == NULL ) {
	perror( "strdup" );
	exit( 1 );
      }
      if ( curr == NULL )
	hosts = new;
      else
	curr->next = new;
      new->next = NULL;
      curr = new;
    }
  }
} /* End of parseArgs */

void findRsh() {
  int i;

  for ( i = 0; rshs[i] != NULL; i++ ) {
    FILE *dummy;

    if ( verbose ) printf( "Searching for command \"%s\"...", rshs[i] );
    if ( (dummy = fopen( rshs[i], "r" )) != NULL ) fclose( dummy );
    if ( dummy != NULL ) {
      if ( verbose ) printf( "OK\n" );
      rshCommand = rshs[i];
      return;
    }
    if ( verbose ) printf( "nope\n" );
  }
  fprintf( stderr, "No suitable \"rsh\" command found...\n" );
  exit( 1 );
} /* End of findRsh */

void doCommand( struct hostDescStruct *ptr ) {
  char  command[1024];
  FILE *pipe;
  char  result[1024];
  int   i;
  char  cmd[1024];

  if ( myName[0] == '/' ) {
    strcpy( cmd, myName );
  } else {
    getcwd( cmd, sizeof(cmd) );
    snprintf( &cmd[strlen(cmd)], sizeof(cmd)-strlen(cmd), "/%s", myName );
  }
  
  snprintf( command, sizeof(command),
	    "%s %s '%s.sh' 2>&1",
	    rshCommand,
	    ptr->hostname,
	    cmd );
  
  if ( verbose ) printf( "> %s\n", command );

  if ( (pipe = popen( command, "r" )) == NULL ) {
    perror( "Command failed " );
    fprintf( stderr, "Command was:\"%s\"\n", command );
    exit(1);
  }
  i = 0;
  while ( !feof( pipe ) ) {
    result[i] = (char)fgetc( pipe );
    if ( result[i] == '\n' || i == sizeof(result)-2 ) {
      if ( result[i] == '\n' ) result[i] = 0;
      result[i+1] = 0;
      printf( "%s: %s\n", ptr->hostname, result );
      i = 0;
    } else {
      i++;
    }
  }
  pclose( pipe );
  
  exit(0);
} /* End of doCommand */

void startCleanup() {
  struct hostDescStruct *curr;

  if ( setuid(0) != 0 ) perror( "setuid " );
  if ( verbose )
    printf( "Starting cleanup. RSH command: \"%s\"\n", rshCommand );
  for ( curr = hosts; curr != NULL; curr = curr->next ) {
    if ( (curr->pid = fork()) == 0 ) doCommand( curr );
  }
} /* End of startCleanup */

void waitForDone() {
  struct hostDescStruct *curr;
  time_t startTime = time( NULL );
  do {
    int missing = 0;
    for ( curr = hosts; curr != NULL; curr = curr->next ) {
      if ( curr->pid != 0 ) {
	if ( waitpid( curr->pid, NULL, WNOHANG ) == 0 ) missing++;
	else curr->pid = 0;
      }
    }
    if ( missing == 0 ) return;
    usleep( 100000 );
  } while ( time( NULL ) - startTime < TIMEOUT );

  printf( "Cleanup failed on:" );
  for ( curr = hosts; curr != NULL; curr = curr->next ) {
    if ( curr->pid != 0 ) printf( " %s", curr->hostname );
  }
  printf( "\n" );
}

int main( int argc, char **argv ) {
  myName = argv[0];
  parseArgs( argc, argv );
  findRsh();
  startCleanup();
  waitForDone();
  printf( "Cleanup command(s) completed\n" );
  exit(0);
} /* End of main */
