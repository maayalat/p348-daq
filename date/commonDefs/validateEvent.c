/*
			   validateEvent.c
			   ===============

  Validate the current definition of the DATE event header using
  direct and derived information.

  This utility must be updated for each new version of the DATE event
  header.

  History:
   V01.00  RD  23-Nov-00 Created
   V01.01  RD  30-Apr-04 Added Common Data Header
   V01.02  RD  24-Jun-04 Added HLT DECISION
   V01.03  RD  25-May-05 Added timestamp
   V01.04  RD  17-Aug-05 Added vanguard/rearguard
   V01.05  RD  05-Sep-05 Added system/detector software trigger
   V01.06  RD  14-Sep-05 Changed VAN/REARGUARD into START/END_OF_DATA
*/
#define DESCRIPTION "ALICE DATE event header validator"
#define VID "V01.06"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>

#include "event.h"

#ifdef AIX
static
#endif
char validateEventIdent[]="@(#)"""__FILE__ """: """ DESCRIPTION \
           """ """ VID """ compiled """ __DATE__ """ """__TIME__"";

/* The Event versions we are able to handle. When the event
   definition changes, do the necessary changes to this validator
   and then update the OUR_* to reflect the changes
 */
#define OUR_VID 0x00030006
#define OUR_CDH_VERSION 1

#ifndef TRUE
 #define TRUE ( 0 == 0 )
#endif
#ifndef FALSE
 #define FALSE ( 0 == 1 )
#endif

/* Swap bytes and words of the given entity */
void doSwap( void *entity, int size ) {
  int i;
  unsigned long32 *p = entity;

  for ( i = 0; i < size; i += 4 ) {
    p[i] = (p[i] & 0xffff0000) >> 16 | ((p[i] << 16) & 0xffff0000);
    p[i] = (p[i] & 0xff00ff00) >> 8 | ((p[i] << 8) & 0xff00ff00);
  }
} /* End of doSwap */

/* Check the Event version number in event.h vs the event version
   number we have declared to be able to handle */
void checkVersionNumber() {
  /* If the following assert fails, it means that we are not able to
     validate events of this type. Either the symbol OUR_VID is not
     properly set or the EVENT_CURRENT_VERSION has been changed and
     this program has not been updated. */
  assert( OUR_VID == EVENT_CURRENT_VERSION );
} /* End of checkVersionNumber */

/* Check the EVENT_HEAD_BASE_SIZE */
void checkHeaderLength() {
  struct eventHeaderStruct header;

  /* If the following assert fails, the EVENT_HEAD_BASE_SIZE field is
     not correctly set: check the definition of EVENT_HEAD_BASE_SIZE */
  assert( sizeof( struct eventHeaderStruct ) == EVENT_HEAD_BASE_SIZE );
  assert( sizeof( header ) == EVENT_HEAD_BASE_SIZE );
} /* End of checkHeaderLength */

/* Check the preamble of an event header */
void checkPreamble() {
  /* To ensure a correct decoding, the event header must have certain
     words (common to all versions) early in the structure. This
     allows a certain, error-free interpretation of the header
     regardless from the version. */
  struct eventHeaderStruct header;
  void *p1 = &header.eventSize;
  void *p2 = &header.eventMagic;
  void *p3 = &header.eventHeadSize;
  void *p4 = &header.eventVersion;
  void *max;
  void *min;
  int   delta;

#define MAX( x, y ) (x > y ? x : y)
  max = MAX( p1, p2 );
  max = MAX( max, p3 );
  max = MAX( max, p4 );

#define MIN( x, y ) (x < y ? x : y)
  min = MIN( p1, p2 );
  min = MIN( min, p3 );
  min = MIN( min, p4 );

#define ABS( x ) ( (int)(x) < 0 ? -(int)(x) : (int)(x) )
  delta = MIN( ABS( p2 - p1 ), ABS( p4 - p3 ) );
  delta = MIN( delta, ABS( p2 - p3 ) );
  delta = MIN( delta, ABS( p2 - p4 ) );

  /* If the following assert fails, there are some "foreign" fields in
     the header preamble and decoding may fail */
  assert( max - min == delta * 3 );
} /* End of checkPreamble */

/* Check the "magic" field of the event header */
void checkMagic() {
  eventMagicType magic;
  int magicSize = sizeof( eventMagicType );
  int i;
  struct eventHeaderStruct header;
  unsigned char *p;

  /* If this assert fails, there is an incompatibility between the
     type of the magic field and the definition of the magic field in
     the event header */
  assert( magicSize == sizeof( header.eventMagic ) );

  /* If this assert fails, the definition of the MAGIC is wrong */
  assert( EVENT_MAGIC_NUMBER != EVENT_MAGIC_NUMBER_SWAPPED );

  /* If the following assert fails, the definition of
     EVENT_MAGIC_NUMBER_SWAPPED is not correct */
  magic = EVENT_MAGIC_NUMBER;
  doSwap( &magic, magicSize );
  assert( magic == EVENT_MAGIC_NUMBER_SWAPPED );

  /* If the following assert fails, the definition of MAGIC may lead
     to errors. To accomplish its function, MAGIC must have all bytes
     different from each other and different from obvious/common
     values (zeros, ones, ASCIIs). If two bytes have the same value,
     misinterpretations may occour. */
  magic = EVENT_MAGIC_NUMBER;
  for ( p = (unsigned char *)&magic, i = 0; i != magicSize; i++ ) {
    int j;

    assert( p[i] != 0 );	/* String termination */
    assert( p[i] != 0xff );	/* All ones */
    assert( p[i] != 0x20 );	/* ASCII SPACE */
    
    for ( j = 0; j != magicSize; j++ ) {
      if ( i != j ) assert( p[i] != p[j] );
    }
  }

  /* If the following assert fails, something is wrong in the way the
     event magic is organized */
  for ( p = (unsigned char *)&magic, i = 0;
	i != magicSize;
	i++, p++ )
    *p = 0;
  assert( magic == 0 );

  /* If the following asserts fail, there is something wrong in the
     way the event magic can be accessed as a single entity */
  for ( i = 0; i != magicSize; i++ ) {
    int j;

    for ( p = ( unsigned char *)&magic, j = 0; j != magicSize; j++, *p++ = 0 );
    assert( magic == 0 );
    p = (unsigned char *)&magic;
    p[i] = 0xff;
    assert( magic != 0 );
  }
  
  /* If the following asserts fail, there is something wrong in the
     way the event magic can be accessed as an array */
  for ( i = 0; i != magicSize; i++ ) {
    int j;

    for ( p = ( unsigned char *)&magic, j = 0; j != magicSize; p[j++] = 0 );
    assert( magic == 0 );
    p = (unsigned char *)&magic;
    p[i] = 0xff;
    assert( magic != 0 );
  }

} /* End of checkMagic */

/* Check the event type field */
void checkEventType() {
  struct eventHeaderStruct header;
  enum   eventTypeEnum     type;
  const  eventTypeType     invalidType = 0;
         unsigned char     *p;
         eventTypeType     t;
	 int               i;

  /* The following assert validates the reset of the type as a whole */
  header.eventType = 0;
  assert( header.eventType == 0 );

  /* The following asserts validate the range of event types */
  assert ( EVENT_TYPE_MIN < EVENT_TYPE_MAX );

  /* This assert is vital for several of the tests below */
  assert ( EVENT_TYPE_MIN > invalidType );

  type = (getpid() % (EVENT_TYPE_MAX-1)) + 1;
  header.eventType = invalidType;
  switch ( type ) {
  case startOfRun       : header.eventType = START_OF_RUN;       break;
  case endOfRun         : header.eventType = END_OF_RUN;         break;
  case startOfRunFiles  : header.eventType = START_OF_RUN_FILES; break;
  case endOfRunFiles    : header.eventType = END_OF_RUN_FILES;   break;
  case startOfBurst     : header.eventType = START_OF_BURST;     break;
  case endOfBurst       : header.eventType = END_OF_BURST;       break;
  case physicsEvent     : header.eventType = PHYSICS_EVENT;      break;
  case calibrationEvent : header.eventType = CALIBRATION_EVENT;  break;
  case formatError      : header.eventType = EVENT_FORMAT_ERROR; break;
  case startOfData      : header.eventType = START_OF_DATA;      break;
  case endOfData        : header.eventType = END_OF_DATA;        break;
  case systemSoftwareTriggerEvent :
               header.eventType = SYSTEM_SOFTWARE_TRIGGER_EVENT; break;
  case detectorSoftwareTriggerEvent :
               header.eventType = DETECTOR_SOFTWARE_TRIGGER_EVENT; break;
  } /* If compilation gives a warning here, the definition of
       eventTypeEnum is incomplete */
  assert( header.eventType != invalidType );

  for ( header.eventType = EVENT_TYPE_MIN;
	header.eventType <= EVENT_TYPE_MAX;
	header.eventType++ ) {
    type = invalidType;
    switch ( header.eventType ) {
    case START_OF_RUN :       type = startOfRun;       break;
    case END_OF_RUN :         type = endOfRun;         break;
    case START_OF_RUN_FILES : type = startOfRunFiles;  break;
    case END_OF_RUN_FILES :   type = endOfRunFiles;    break;
    case START_OF_BURST :     type = startOfBurst;     break;
    case END_OF_BURST :       type = endOfBurst;       break;
    case PHYSICS_EVENT :      type = physicsEvent;     break;
    case CALIBRATION_EVENT :  type = calibrationEvent; break;
    case EVENT_FORMAT_ERROR : type = formatError;      break;
    case START_OF_DATA :      type = startOfData;      break;
    case END_OF_DATA :        type = endOfData;        break;
    case SYSTEM_SOFTWARE_TRIGGER_EVENT :
                    type = systemSoftwareTriggerEvent; break;
    case DETECTOR_SOFTWARE_TRIGGER_EVENT :
                  type = detectorSoftwareTriggerEvent; break;
    } /* Compilations error(s) here indicate an incomplete definition
         of the eventTypeEnum type */

    /* If this assert fails, the definition of the eventTypeEnum may
       be incomplete */
    assert ( type != invalidType );
  }

  /* If this assert fails, there is a mismatch between the type
     eventTypeType and the field type of the event header */
  assert( sizeof( t ) == sizeof( header.eventType ) );

  /* If the following assert fails, there is a problem of access to a
     variable of type eventTypeType */
  for ( p = (unsigned char *)&t, t = 0, i = 0; i != sizeof( t ); i++ )
    assert( *p++ == 0 );
  
  /* If the following assert fails, there is a problem of access to a
     variable of type eventTypeType as an array of chars */
  for ( p = (unsigned char *)&t, t = 0, i = 0; i != sizeof( t ); i++ )
    assert( p[i] == 0 );

  /* Here we verify that changing one byte of the event type changes
     the event type as a whole */
  for ( p = (unsigned char *)&t, i = 0; i != sizeof( t ); i++ ) {
    t = 0;
    assert( t == 0 );
    assert( p[i] == 0 );
    p[i] = 1;
    assert( p[i] != 0 );
    assert( t != 0 );
  }
} /* End of checkEventType */

/* Check the way bits are assigned within the event ID */
void checkEventIdBits() {
  eventIdType id;
  unsigned char   *ptrB = (unsigned char *)&id;
  unsigned long32 *ptrW = (unsigned long32 *)&id;
  int i;

  LOAD_EVENT_ID( id, 0, 0, 0 );
  for ( i = 0; i != EVENT_ID_BYTES; i++ ) assert( ptrB[i] == 0 );
  for ( i = 0; i != EVENT_ID_WORDS; i++ ) assert( ptrW[i] == 0 );

  LOAD_EVENT_ID( id, 0x0abcdef, 0x123456, 0x789 );
  assert( ptrW[1] == 0x23456789 );
  assert( ptrW[0] == 0x0abcdef1 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( id ) == 0x789 );
  assert( EVENT_ID_GET_ORBIT( id ) == 0x123456 );
  assert( EVENT_ID_GET_PERIOD( id ) == 0x0abcdef );

  EVENT_ID_SET_BUNCH_CROSSING( id, 0xabc );
  assert( ptrW[1] == 0x23456abc );
  assert( ptrW[0] == 0x0abcdef1 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( id ) == 0xabc );
  assert( EVENT_ID_GET_ORBIT( id ) == 0x123456 );

  EVENT_ID_SET_ORBIT( id, 0x654321 );
  assert( ptrW[1] == 0x54321abc );
  assert( ptrW[0] == 0x0abcdef6 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( id ) == 0xabc );
  assert( EVENT_ID_GET_ORBIT( id ) == 0x654321 );

  LOAD_EVENT_ID( id, 0xf123456, 0, 0 );
  assert( ptrW[1] == 0 );
  assert( ptrW[0] == 0xf1234560 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( id ) == 0 );
  assert( EVENT_ID_GET_ORBIT( id ) == 0 );
  assert( EVENT_ID_GET_PERIOD( id ) == 0xf123456 );

  LOAD_RAW_EVENT_ID( id, 0, 0, 0 );
  assert( EVENT_ID_GET_NB_IN_RUN( id ) == 0 );
  assert( EVENT_ID_GET_BURST_NB( id ) == 0 );
  assert( EVENT_ID_GET_NB_IN_BURST( id ) == 0 );
  /* The following two lines may not be true for all encoding schemes */
  assert( ptrW[0] == 0 );
  assert( ptrW[1] == 0 );

  LOAD_RAW_EVENT_ID( id, 1, 2, 3 );
  assert( EVENT_ID_GET_NB_IN_RUN( id )   == 1 );
  assert( EVENT_ID_GET_BURST_NB( id )    == 2 );
  assert( EVENT_ID_GET_NB_IN_BURST( id ) == 3 );
  /* The following two lines may not be true for all encoding schemes */
  assert( ptrW[0] != 0 );
  assert( ptrW[1] != 0 );

  LOAD_RAW_EVENT_ID( id,
		     (unsigned int)0x0123456789abcdefLL,
		     0x567,
		     0x34567 );
  assert( EVENT_ID_GET_NB_IN_RUN( id )   == 0x89abcdef );
  assert( EVENT_ID_GET_BURST_NB( id )    == 0x567 );
  assert( EVENT_ID_GET_NB_IN_BURST( id ) == 0x34567 );

  /* Check the ZERO_EVENT_ID for Fixed target and Collider modes */
  LOAD_EVENT_ID( id, 1, 1, 1 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( id ) != 0 );
  assert( EVENT_ID_GET_PERIOD( id )         != 0 );
  assert( EVENT_ID_GET_ORBIT( id )          != 0 );
  ZERO_EVENT_ID( id );
  assert( EVENT_ID_GET_BUNCH_CROSSING( id ) == 0 );
  assert( EVENT_ID_GET_PERIOD( id )         == 0 );
  assert( EVENT_ID_GET_ORBIT( id )          == 0 );

  LOAD_RAW_EVENT_ID( id, 1, 1, 1 );
  assert( EVENT_ID_GET_NB_IN_RUN( id )   != 0 );
  assert( EVENT_ID_GET_BURST_NB( id )    != 0 );
  assert( EVENT_ID_GET_NB_IN_BURST( id ) != 0 );
  ZERO_EVENT_ID( id );
  assert( EVENT_ID_GET_NB_IN_RUN( id )   == 0 );
  assert( EVENT_ID_GET_BURST_NB( id )    == 0 );
  assert( EVENT_ID_GET_NB_IN_BURST( id ) == 0 );
} /* End of checkEventIdBits */

/* Check the order of event IDs and the macros written for testing and
   initialisation */
void checkEventIdOrder() {
  eventIdType ev1, ev2;

  /* Start with two reset events */
  LOAD_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 0, 0 );
  
  assert( EQ_EVENT_ID( ev1, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev1 ) );
  
  assert( EQ_EVENT_ID( ev2, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev2 ) );
  
  assert( EQ_EVENT_ID( ev1, ev2 ) );
  assert( EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  /* Try with equal, non-reset events */
  LOAD_EVENT_ID( ev1, 2, 1, 0 );
  LOAD_EVENT_ID( ev2, 2, 1, 0 );
  
  assert( EQ_EVENT_ID( ev1, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev1 ) );
  
  assert( EQ_EVENT_ID( ev2, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev2 ) );
  
  assert( EQ_EVENT_ID( ev1, ev2 ) );
  assert( EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );
  
  LOAD_EVENT_ID( ev1, 1, 2, 0 );
  LOAD_EVENT_ID( ev2, 1, 2, 0 );
  
  assert( EQ_EVENT_ID( ev1, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev1 ) );
  
  assert( EQ_EVENT_ID( ev2, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev2 ) );
  
  assert( EQ_EVENT_ID( ev1, ev2 ) );
  assert( EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );
  
  LOAD_EVENT_ID( ev1, 8, 3, 4 );
  LOAD_EVENT_ID( ev2, 8, 3, 4 );
  
  assert( EQ_EVENT_ID( ev1, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev1 ) );
  
  assert( EQ_EVENT_ID( ev2, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev2 ) );
  
  assert( EQ_EVENT_ID( ev1, ev2 ) );
  assert( EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  /* Make ev1 greater (more recent, younger) of ev2 */
  LOAD_EVENT_ID( ev1, 0, 0, 1 );
  LOAD_EVENT_ID( ev2, 0, 0, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 0, 1, 0 );
  LOAD_EVENT_ID( ev2, 0, 0, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 0, 1, 1 );
  LOAD_EVENT_ID( ev2, 0, 0, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 0, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 1, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 1, 0, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 1, 1, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 1, 0, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 1, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_EVENT_ID( ev2, 1, 1, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  /* Make ev2 greater (more recent, younger) of ev1 */
  LOAD_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 0, 2 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( GT_EVENT_ID( ev2, ev1 ) );
  assert( LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( !GE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( !LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 3, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( GT_EVENT_ID( ev2, ev1 ) );
  assert( LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( !GE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( !LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 4, 5 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( GT_EVENT_ID( ev2, ev1 ) );
  assert( LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( !GE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( !LE_EVENT_ID( ev2, ev1 ) );

  /* Now try with non-reset events */
  LOAD_EVENT_ID( ev1, 300, 100, 200 );

  LOAD_EVENT_ID( ev2, 200, 1, 2 );
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev2, 300, 200, 2 );
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( GT_EVENT_ID( ev2, ev1 ) );
  assert( LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( !GE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( !LE_EVENT_ID( ev2, ev1 ) );

  LOAD_EVENT_ID( ev2, 300, 100, 300 );
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( !GT_EVENT_ID( ev1, ev2 ) );
  assert( GT_EVENT_ID( ev2, ev1 ) );
  assert( LT_EVENT_ID( ev1, ev2 ) );
  assert( !LT_EVENT_ID( ev2, ev1 ) );
  assert( !GE_EVENT_ID( ev1, ev2 ) );
  assert( GE_EVENT_ID( ev2, ev1 ) );
  assert( LE_EVENT_ID( ev1, ev2 ) );
  assert( !LE_EVENT_ID( ev2, ev1 ) );

  /* Try again with raw events */
  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 0, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 0, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 1, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 1, 0, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 1, 1, 0 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 1, 0, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 1, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  LOAD_RAW_EVENT_ID( ev1, 1000, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 1, 1, 1 );
  
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  assert( !EQ_EVENT_ID( ev2, ev1 ) );
  assert( GT_EVENT_ID( ev1, ev2 ) );
  assert( !GT_EVENT_ID( ev2, ev1 ) );
  assert( !LT_EVENT_ID( ev1, ev2 ) );
  assert( LT_EVENT_ID( ev2, ev1 ) );
  assert( GE_EVENT_ID( ev1, ev2 ) );
  assert( !GE_EVENT_ID( ev2, ev1 ) );
  assert( !LE_EVENT_ID( ev1, ev2 ) );
  assert( LE_EVENT_ID( ev2, ev1 ) );

  /* Check the event copy macro */
  LOAD_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_EVENT_ID( ev2,
		 EVENT_ID_MAX_PERIOD,
		 EVENT_ID_MAX_ORBIT,
		 EVENT_ID_MAX_BUNCH_CROSSING );
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  COPY_EVENT_ID( ev1, ev2 );
  assert( EQ_EVENT_ID( ev1, ev2 ) );
  assert( EVENT_ID_GET_ORBIT( ev1 ) == EVENT_ID_GET_ORBIT( ev2 ) );
  assert( EVENT_ID_GET_BUNCH_CROSSING( ev1 ) ==
	  EVENT_ID_GET_BUNCH_CROSSING( ev2 ) );
  assert( EVENT_ID_GET_ORBIT( ev2 ) == 0 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( ev2 ) == 0 );
  
  LOAD_EVENT_ID( ev1, 0xfedcba9, 0x123456, 0x789 );
  LOAD_EVENT_ID( ev2,
		 EVENT_ID_MAX_PERIOD,
		 EVENT_ID_MAX_ORBIT,
		 EVENT_ID_MAX_BUNCH_CROSSING );
  assert( !EQ_EVENT_ID( ev1, ev2 ) );
  COPY_EVENT_ID( ev1, ev2 );
  assert( EQ_EVENT_ID( ev1, ev2 ) );
  assert( EVENT_ID_GET_ORBIT( ev1 ) == EVENT_ID_GET_ORBIT( ev2 ) );
  assert( EVENT_ID_GET_BUNCH_CROSSING( ev1 ) ==
	  EVENT_ID_GET_BUNCH_CROSSING( ev2 ) );
  assert( EVENT_ID_GET_ORBIT( ev2 ) == 0x123456);
  assert( EVENT_ID_GET_BUNCH_CROSSING( ev2 ) == 0x789 );

  /* Check the ADD_EVENT_ID macro */
  LOAD_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_EVENT_ID( ev2, 0, 0, 1 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 0 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 0 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 1 );
  
  LOAD_EVENT_ID( ev1, 0, 1, 0 );
  LOAD_EVENT_ID( ev2, 0, 2, 0 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 0 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 3 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 0 );
  
  LOAD_EVENT_ID( ev1, 2, 0, 0 );
  LOAD_EVENT_ID( ev2, 4, 0, 0 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 6 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 0 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 0 );
  
  LOAD_EVENT_ID( ev1, 7, 4, 1 );
  LOAD_EVENT_ID( ev2, 1, 2, 3 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 8 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 6 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 4 );

  LOAD_RAW_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 0, 1 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 0 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 0 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 1 );

  LOAD_RAW_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 2, 0 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 0 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 2 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 0 );

  LOAD_RAW_EVENT_ID( ev1, 0, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 3, 0, 0 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 3 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 0 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 0 );

  LOAD_RAW_EVENT_ID( ev1, 7, 4, 1 );
  LOAD_RAW_EVENT_ID( ev2, 5, 3, 2 );
  ADD_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 12 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 7 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 3 );

  /* Check the SUB_EVENT_ID macro */
  LOAD_EVENT_ID( ev1, 0, 0, 2 );
  LOAD_EVENT_ID( ev2, 0, 0, 1 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 0 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 0 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 1 );
  
  LOAD_EVENT_ID( ev1, 0, 4, 0 );
  LOAD_EVENT_ID( ev2, 0, 1, 0 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 0 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 3 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 0 );
  
  LOAD_EVENT_ID( ev1, 8, 0, 0 );
  LOAD_EVENT_ID( ev2, 2, 0, 0 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 6 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 0 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 0 );
  
  LOAD_EVENT_ID( ev1, 9, 8, 7 );
  LOAD_EVENT_ID( ev2, 1, 2, 3 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_PERIOD( ev1 ) == 8 &&
	  EVENT_ID_GET_ORBIT( ev1 ) == 6 &&
	  EVENT_ID_GET_BUNCH_CROSSING( ev1 ) == 4 );

  LOAD_RAW_EVENT_ID( ev1, 0, 0, 2 );
  LOAD_RAW_EVENT_ID( ev2, 0, 0, 1 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 0 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 0 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 1 );

  LOAD_RAW_EVENT_ID( ev1, 0, 6, 0 );
  LOAD_RAW_EVENT_ID( ev2, 0, 4, 0 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 0 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 2 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 0 );

  LOAD_RAW_EVENT_ID( ev1, 6, 0, 0 );
  LOAD_RAW_EVENT_ID( ev2, 3, 0, 0 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 3 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 0 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 0 );

  LOAD_RAW_EVENT_ID( ev1, 17, 10, 5 );
  LOAD_RAW_EVENT_ID( ev2,  5,  3, 2 );
  SUB_EVENT_ID( ev1, ev2 );
  assert( EVENT_ID_GET_NB_IN_RUN( ev1 ) == 12 &&
	  EVENT_ID_GET_BURST_NB( ev1 ) == 7 &&
	  EVENT_ID_GET_NB_IN_BURST( ev1 ) == 3 );
} /* End of checkEventIdOrder */

/* Check the eventId field of the Event header */
void checkEventId() {
  struct eventHeaderStruct header;
  eventIdType eventId;
  unsigned char *id = (unsigned char *)&eventId;
  int i;

  /* Some basic checks on the size of the eventId type and the eventId
     of the event header. Note that all the checks below should word
     independently from the basic type of the event ID */
  assert( sizeof( eventIdType ) > 0 );
  assert( sizeof( eventIdType ) == EVENT_ID_BYTES );
  assert( sizeof( eventIdType ) == sizeof( header.eventId ) );
  assert( sizeof( eventId ) == sizeof( eventIdType ) );
  assert( sizeof( eventId ) == sizeof( header.eventId ) );
  assert( EVENT_ID_WORDS == (EVENT_ID_BYTES >> 2) );
  assert( EVENT_ID_WORDS == (EVENT_ID_BYTES / 4 ) );
  assert( ( EVENT_ID_WORDS << 2 ) == EVENT_ID_BYTES );
  assert( ( EVENT_ID_WORDS * 4 ) == EVENT_ID_BYTES );

  /* The check below may not word if the macro RESET_EVENT_ID does not
     set the whole event ID variable to all zeros */
  for ( i = 0; i != EVENT_ID_BYTES; id[i++]=1 );
  for ( i = 0; i != EVENT_ID_BYTES; i++ ) assert( id[i] != 0 );
  LOAD_EVENT_ID( eventId, 0, 0, 0 );
  for ( i = 0; i != EVENT_ID_BYTES; i++ ) assert( id[i] == 0 );

  /* Check the way bits are aaigned within the event ID */
  checkEventIdBits();

  /* Now the check on "ordered" event IDs */
  checkEventIdOrder();
} /* End of checkEventId */

/* Calculate the number of system attributes set */
int numSystemAttributes( eventTypeAttributeType attr ) {
  int c;
  int i;
  for ( c = 0, i = FIRST_SYSTEM_ATTRIBUTE; i != LAST_SYSTEM_ATTRIBUTE+1; i++ )
    if ( TEST_SYSTEM_ATTRIBUTE( attr, i ) ) c++;

  return c;
} /* End of numSystemAttributes */

/* Calculate the number of user attributes set */
int numUserAttributes( eventTypeAttributeType attr ) {
  int c;
  int i;
  for ( c = 0, i = FIRST_USER_ATTRIBUTE; i != LAST_USER_ATTRIBUTE+1; i++ )
    if ( TEST_USER_ATTRIBUTE( attr, i ) ) c++;

  return c;
} /* End of numUserAttributes */

/* Count the number of attributes set in a given typeAttribute */
int numAttributesSet( eventTypeAttributeType a ) {
  unsigned char *p;
  int r1;
  int r2;
  int r3;
  int r4;
  int r5;
  int r6;
  int i;

  /* Count the bits byte per byte */
  for ( r1=0, p=(unsigned char *)a, i = 0; i != ALL_ATTRIBUTE_BYTES; i++ ) {
    int b;
    for ( b = 1; b != 0x100; b *= 2 )
      if ( (p[i] & b) != 0 ) r1++;
  }

  /* Count the bits using the TEST_ANY_ATTRIBUTE macro */
  for ( r2 = 0, i = 0; i != ALL_ATTRIBUTE_BITS; i++ )
    if ( TEST_ANY_ATTRIBUTE( a, i ) )
      r2++;

  assert( r1 == r2 );
  
  /* Count the bits using the TEST_USER and TEST_SYSTEM macros */
  for ( r3 = 0, i = 0; i != USER_ATTRIBUTE_BITS; i++ )
    if ( TEST_USER_ATTRIBUTE( a, i ) )
      r3++;
  for ( r4 = 0; i != USER_ATTRIBUTE_BITS + SYSTEM_ATTRIBUTE_BITS; i++ )
    if ( TEST_SYSTEM_ATTRIBUTE( a, i ) )
      r4++;
  assert( r1 == r3 + r4 );

  /* Count the USER and SYSTEM attributes manually */
  for ( r5 = 0, i = 0; i != USER_ATTRIBUTE_BYTES; i++ ) {
    int b;
    for ( b = 1; b != 0x100; b *= 2 )
      if ( (p[i] & b) != 0 )
	r5++;
  }
  for ( r6 = 0; i != USER_ATTRIBUTE_BYTES+SYSTEM_ATTRIBUTE_BYTES; i++ ) {
    int b;
    for ( b = 1; b != 0x100; b *= 2 )
      if ( (p[i] & b) != 0 )
	r6++;
  }
  assert( r3 == r5 );
  assert( r4 == r6 );
  assert( r1 == r5 + r6 );
  assert( r5 == numUserAttributes( a ) );
  assert( r6 == numSystemAttributes( a ) );
  
  return r1;
} /* End of numAttributesSet */

/* Check the typeAttribute field of the event header */
void checkTypeAttribute() {
  struct eventHeaderStruct header;
  eventTypeAttributeType attr, attr1;
  int i;
  int diffs;

  /* Some basic checks on the sizes */
  assert( sizeof( attr ) == sizeof( eventTypeAttributeType ) );
  assert( sizeof( attr ) == sizeof( header.eventTypeAttribute ) );

  assert( sizeof( attr ) == ALL_ATTRIBUTE_BYTES );
  assert( sizeof( attr ) / 4    == ALL_ATTRIBUTE_WORDS );
  assert( (sizeof( attr ) >> 2) == ALL_ATTRIBUTE_WORDS );
  assert( sizeof( attr ) * 8    == ALL_ATTRIBUTE_BITS );
  assert( (sizeof( attr ) << 3) == ALL_ATTRIBUTE_BITS );

  assert( ALL_ATTRIBUTE_WORDS == SYSTEM_ATTRIBUTE_WORDS+USER_ATTRIBUTE_WORDS );
  assert( ALL_ATTRIBUTE_BYTES == SYSTEM_ATTRIBUTE_BYTES+USER_ATTRIBUTE_BYTES );
  assert( ALL_ATTRIBUTE_BITS  == SYSTEM_ATTRIBUTE_BITS +USER_ATTRIBUTE_BITS  );

  assert( ALL_ATTRIBUTE_WORDS    > 0 );
  assert( SYSTEM_ATTRIBUTE_WORDS > 0 );
  assert( USER_ATTRIBUTE_WORDS   > 0 );

  assert( SYSTEM_ATTRIBUTE_BYTES / 4    == SYSTEM_ATTRIBUTE_WORDS );
  assert( (SYSTEM_ATTRIBUTE_BYTES >> 2) == SYSTEM_ATTRIBUTE_WORDS );
  assert( SYSTEM_ATTRIBUTE_BYTES * 8    == SYSTEM_ATTRIBUTE_BITS );
  assert( (SYSTEM_ATTRIBUTE_BYTES << 3) == SYSTEM_ATTRIBUTE_BITS );

  assert( USER_ATTRIBUTE_BYTES / 4    == USER_ATTRIBUTE_WORDS );
  assert( (USER_ATTRIBUTE_BYTES >> 2) == USER_ATTRIBUTE_WORDS );
  assert( USER_ATTRIBUTE_BYTES * 8    == USER_ATTRIBUTE_BITS );
  assert( (USER_ATTRIBUTE_BYTES << 3) == USER_ATTRIBUTE_BITS );
  
  assert( FIRST_USER_ATTRIBUTE >= 0 );
  assert( LAST_USER_ATTRIBUTE  >= 0 );
  assert( FIRST_USER_ATTRIBUTE < LAST_USER_ATTRIBUTE );
  assert( LAST_USER_ATTRIBUTE - FIRST_USER_ATTRIBUTE + 1 ==
	  USER_ATTRIBUTE_BITS );

  assert( FIRST_SYSTEM_ATTRIBUTE >= 0 );
  assert( LAST_SYSTEM_ATTRIBUTE  >= 0 );
  assert( FIRST_SYSTEM_ATTRIBUTE <  LAST_SYSTEM_ATTRIBUTE );
  assert( LAST_SYSTEM_ATTRIBUTE - FIRST_SYSTEM_ATTRIBUTE + 1 ==
	  SYSTEM_ATTRIBUTE_BITS );

  /* Some parts of the code rely on the fact that user attributes stay
     on the low part of the mask and the system attributes use the
     high part of the mask */
  assert( LAST_USER_ATTRIBUTE < FIRST_SYSTEM_ATTRIBUTE );

  /* If this assert fails, there may be a hole between system and user
     attributes */
  assert( LAST_USER_ATTRIBUTE + 1 == FIRST_SYSTEM_ATTRIBUTE );

  for ( i = 0; i != ALL_ATTRIBUTE_BITS; i++ ) {
    int j;

    for ( j = 0; j != ALL_ATTRIBUTE_WORDS; attr[ j++ ] = 0 );

    /* This check validates that clearing the attribute mask word by
       word also clears all the bits */
    for ( j = 0; j != ALL_ATTRIBUTE_BITS; j++ )
      assert( !TEST_ANY_ATTRIBUTE( attr, j ) );
    assert( numAttributesSet( attr ) == 0 );

    /* Now we check that setting one attribute results in one and only
       one attribute set. We also check that the bit is set in the
       right set (SYSTEM or USER) of the typeAttribute */
    SET_ANY_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == 1 );
    assert( TEST_ANY_ATTRIBUTE( attr, i ) );
    for ( j = 0; j != ALL_ATTRIBUTE_BITS; j++ )
      if ( i != j ) {
	assert( !TEST_ANY_ATTRIBUTE( attr, j ) );
	if ( j < USER_ATTRIBUTE_BITS )
	  assert( !TEST_USER_ATTRIBUTE( attr, j ) );
	else
	  assert( !TEST_SYSTEM_ATTRIBUTE( attr, j ) );
      } else {
	assert( TEST_ANY_ATTRIBUTE( attr, j ) );
	if ( j < USER_ATTRIBUTE_BITS )
	  assert( TEST_USER_ATTRIBUTE( attr, j ) );
	else
	  assert( TEST_SYSTEM_ATTRIBUTE( attr, j ) );
      }

    /* Now we reset the attribute and check that we are back to all
       zeroes */
    CLEAR_ANY_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == 0 );

    /* We set again the bit using the SYSTEM_* and USER_* macros */
    if ( i < USER_ATTRIBUTE_BITS ) {
      SET_USER_ATTRIBUTE( attr, i );
      assert( TEST_USER_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == 1 );
      CLEAR_USER_ATTRIBUTE( attr, i );
      assert( !TEST_USER_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == 0 );
    } else {
      SET_SYSTEM_ATTRIBUTE( attr, i );
      assert( TEST_SYSTEM_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == 1 );
      CLEAR_SYSTEM_ATTRIBUTE( attr, i );
      assert( !TEST_SYSTEM_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == 0 );
    }
  }

  /* Here we check the RESET_*_ATTRIBUTES macros */
  for ( i = 0; i != ALL_ATTRIBUTE_WORDS; attr[i++] = i );
  assert( numAttributesSet( attr ) != 0 );
  RESET_ATTRIBUTES( attr );
  assert( numAttributesSet( attr ) == 0 );
  for ( i = 0; i != ALL_ATTRIBUTE_WORDS; attr[i++] = i );
  assert( numAttributesSet( attr ) != 0 );
  RESET_USER_ATTRIBUTES( attr );
  assert( numAttributesSet( attr ) != 0 );
  RESET_SYSTEM_ATTRIBUTES( attr );
  assert( numAttributesSet( attr ) == 0 );
  for ( i = 0; i != ALL_ATTRIBUTE_WORDS; attr[i++] = i );
  assert( numAttributesSet( attr ) != 0 );
  RESET_SYSTEM_ATTRIBUTES( attr );
  assert( numAttributesSet( attr ) != 0 );
  RESET_USER_ATTRIBUTES( attr );
  assert( numAttributesSet( attr ) == 0 );

  for ( i = 0; i != ALL_ATTRIBUTE_BITS; i++ ) {
    int j;

    for ( j = 0; j != ALL_ATTRIBUTE_WORDS; attr[ j++ ] = 0xffffffff );

    /* This check validates that setting the attribute mask word by
       word also sets all the bits */
    for ( j = 0; j != ALL_ATTRIBUTE_BITS; j++ )
      assert( TEST_ANY_ATTRIBUTE( attr, j ) );
    assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS );

    /* Now we check that clearing one attribute results in one and
       only one attribute cleard. We also check that the bit is set in
       the right set (SYSTEM or USER) of the typeAttribute */
    CLEAR_ANY_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS-1 );
    assert( !TEST_ANY_ATTRIBUTE( attr, i ) );
    for ( j = 0; j != ALL_ATTRIBUTE_BITS; j++ ) {
      if ( i != j ) {
	assert( TEST_ANY_ATTRIBUTE( attr, j ) );
	if ( j < USER_ATTRIBUTE_BITS )
	  assert( TEST_USER_ATTRIBUTE( attr, j ) );
	else
	  assert( TEST_SYSTEM_ATTRIBUTE( attr, j ) );
      } else {
	assert( !TEST_ANY_ATTRIBUTE( attr, j ) );
	if ( j < USER_ATTRIBUTE_BITS )
	  assert( !TEST_USER_ATTRIBUTE( attr, j ) );
	else
	  assert( !TEST_SYSTEM_ATTRIBUTE( attr, j ) );
      }
    }
    
    /* Now we set the attribute and check that we are back to all ones */
    SET_ANY_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS );

    /* We set again the bit using the SYSTEM_* and USER_* macros */
    if ( i < USER_ATTRIBUTE_BITS ) {
      CLEAR_USER_ATTRIBUTE( attr, i );
      assert( !TEST_USER_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS-1 );
      SET_USER_ATTRIBUTE( attr, i );
      assert( TEST_USER_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS );
    } else {
      CLEAR_SYSTEM_ATTRIBUTE( attr, i );
      assert( !TEST_SYSTEM_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS-1 );
      SET_SYSTEM_ATTRIBUTE( attr, i );
      assert( TEST_SYSTEM_ATTRIBUTE( attr, i ) );
      assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS );
    }
  }

  /* Check the assignment of the SYSTEM attributes. Add any new system
     attribute at the bottom of the list */
  RESET_ATTRIBUTES( attr );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_P_START );
  assert( numSystemAttributes( attr ) == 1 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_P_END );
  assert( numSystemAttributes( attr ) == 2 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_SWAPPED );
  assert( numSystemAttributes( attr ) == 3 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_PAGED );
  assert( numSystemAttributes( attr ) == 4 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_DATA_TRUNCATED );
  assert( numSystemAttributes( attr ) == 5 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_ERROR );
  assert( numSystemAttributes( attr ) == 6 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_SUPER_EVENT );
  assert( numSystemAttributes( attr ) == 7 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_ORBIT_BC );
  assert( numSystemAttributes( attr ) == 8 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_KEEP_PAGES );
  assert( numSystemAttributes( attr ) == 9 );
  assert( numUserAttributes( attr ) == 0 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_HLT_DECISION );
  assert( numSystemAttributes( attr ) == 10 );
  assert( numUserAttributes( attr ) == 0 );

  /* Check the "copy attributes" macros */
  RESET_ATTRIBUTES( attr );
  SET_USER_ATTRIBUTE( attr, 1  );
  SET_USER_ATTRIBUTE( attr, 23 );
  SET_USER_ATTRIBUTE( attr, 40 );
  SET_USER_ATTRIBUTE( attr, 43 );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_SWAPPED );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_PAGED );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_DATA_TRUNCATED );

  RESET_ATTRIBUTES( attr1 );
  COPY_ALL_ATTRIBUTES( attr, attr1 );
  for ( i = 0; i != ALL_ATTRIBUTE_BITS; i++ )
    assert( TEST_ANY_ATTRIBUTE( attr, i ) ?
	      TEST_ANY_ATTRIBUTE( attr1, i ) :
	      !TEST_ANY_ATTRIBUTE( attr1, i ) );

  RESET_ATTRIBUTES( attr1 );
  COPY_SYSTEM_ATTRIBUTES( attr, attr1 );
  for ( i = 0, diffs = 0; i != ALL_ATTRIBUTE_BITS; i++ ) {
    if ( TEST_ANY_ATTRIBUTE( attr, i ) ) {
      if ( !TEST_ANY_ATTRIBUTE( attr1, i ) ) diffs++;
    } else {
      if ( TEST_ANY_ATTRIBUTE( attr1, i ) ) diffs++;
    }
  }
  assert( diffs == 4 );

  RESET_ATTRIBUTES( attr1 );
  COPY_USER_ATTRIBUTES( attr, attr1 );
  for ( i = 0, diffs = 0; i != ALL_ATTRIBUTE_BITS; i++ ) {
    if ( TEST_ANY_ATTRIBUTE( attr, i ) ) {
      if ( !TEST_ANY_ATTRIBUTE( attr1, i ) ) diffs++;
    } else {
      if ( TEST_ANY_ATTRIBUTE( attr1, i ) ) diffs++;
    }
  }
  assert( diffs == 3 );

  /* Validate the "FLIP_ATTRIBUTE" macros */
  RESET_ATTRIBUTES( attr );
  for ( i = 0; i != ALL_ATTRIBUTE_BITS; i++ ) {
    FLIP_ANY_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == i+1 );
  }
  for ( i = 0; i != ALL_ATTRIBUTE_BITS; i++ ) {
    FLIP_ANY_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == ALL_ATTRIBUTE_BITS-i-1 );
  }
  
  RESET_ATTRIBUTES( attr );
  for ( i = 0; i != USER_ATTRIBUTE_BITS; i++ ) {
    FLIP_USER_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == i+1 );
  }
  for ( i = 0; i != USER_ATTRIBUTE_BITS; i++ ) {
    FLIP_USER_ATTRIBUTE( attr, i );
    assert( numAttributesSet( attr ) == USER_ATTRIBUTE_BITS-i-1 );
  }

  RESET_ATTRIBUTES( attr );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_P_START );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_P_END );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_SWAPPED );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_PAGED );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_SUPER_EVENT );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_ORBIT_BC );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_KEEP_PAGES );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_HLT_DECISION );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_DATA_TRUNCATED );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
  SET_SYSTEM_ATTRIBUTE( attr, ATTR_EVENT_ERROR );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );

  /* ------------------- NEGATIVE tests ----------------- */
  /* Here we must takes some guessing on the way bits are */
  /* assigned. Don't be surprised if you change something */
  /* and some of the tests begin to fail, simply change   */
  /* the way bits are assumed to be...                    */
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );

  /* We assume that the following attribute is not used */
  i = 84;
  SET_SYSTEM_ATTRIBUTE( attr, i );
  assert( !SYSTEM_ATTRIBUTES_OK( attr ) );
  FLIP_SYSTEM_ATTRIBUTE( attr, i );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );

  /* We assume that the following bit is the last of the non-errors and a */
  /* gap exists between the non-errors and the errors                     */
  i = ATTR_HLT_DECISION+1;
  SET_SYSTEM_ATTRIBUTE( attr, i );
  assert( !SYSTEM_ATTRIBUTES_OK( attr ) );
  FLIP_SYSTEM_ATTRIBUTE( attr, i );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );

  /* We assume that the following bit is the first of the errors and a gap */
  /* exists between the errors and the non-errors                          */
  i = ATTR_EVENT_DATA_TRUNCATED-1;
  SET_SYSTEM_ATTRIBUTE( attr, i );
  assert( !SYSTEM_ATTRIBUTES_OK( attr ) );
  FLIP_SYSTEM_ATTRIBUTE( attr, i );
  assert( SYSTEM_ATTRIBUTES_OK( attr ) );
} /* End of checkTypeAttribute */

/* Check the triggerPattern field of the Event header */
void checkTriggerPattern() {
  struct eventHeaderStruct header;
  eventTriggerPatternType eventTriggerPattern, eventTriggerPattern1;
  int i, j;

  /* Some basic checks on the sizes */
  assert( sizeof( eventTriggerPattern ) > 0 );
  assert( sizeof( eventTriggerPattern ) == EVENT_TRIGGER_PATTERN_WORDS*4 );
  assert( sizeof( eventTriggerPattern ) == sizeof( eventTriggerPatternType ) );
  assert( sizeof( header.eventTriggerPattern ) ==
	  sizeof( eventTriggerPattern ) );

  ZERO_TRIGGER_PATTERN( eventTriggerPattern );
  for ( i = 0; i != EVENT_TRIGGER_PATTERN_WORDS; i++ )
    assert( eventTriggerPattern[i] == 0 );

  memset( eventTriggerPattern, 0xff, EVENT_TRIGGER_PATTERN_WORDS*4 );
  for ( i = 0; i != EVENT_TRIGGER_PATTERN_WORDS; i++ )
    assert( eventTriggerPattern[i] != 0 );
  ZERO_TRIGGER_PATTERN( eventTriggerPattern );
  for ( i = 0; i != EVENT_TRIGGER_PATTERN_WORDS; i++ )
    assert( eventTriggerPattern[i] == 0 );

  ZERO_TRIGGER_PATTERN( eventTriggerPattern );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    SET_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    CLEAR_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    FLIP_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    FLIP_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ ) {
    ZERO_TRIGGER_PATTERN( eventTriggerPattern );
    SET_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
    for ( j = EVENT_TRIGGER_ID_MIN; j <= EVENT_TRIGGER_ID_MAX; j++ )
      assert( (j == i && TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) ||
	      (j != i && !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) );
    assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  }
  
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ ) {
    for ( j = EVENT_TRIGGER_ID_MIN; j <= EVENT_TRIGGER_ID_MAX; j++ )
      SET_TRIGGER_IN_PATTERN( eventTriggerPattern, j );
    CLEAR_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
    for ( j = EVENT_TRIGGER_ID_MIN; j <= EVENT_TRIGGER_ID_MAX; j++ )
      assert( (j == i && !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) ||
	      (j != i && TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) );
    assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  }

  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ ) {
    ZERO_TRIGGER_PATTERN( eventTriggerPattern );
    FLIP_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
    for ( j = EVENT_TRIGGER_ID_MIN; j <= EVENT_TRIGGER_ID_MAX; j++ )
      assert( (j == i && TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) ||
	      (j != i && !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) );
    assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  }
  
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ ) {
    for ( j = EVENT_TRIGGER_ID_MIN; j <= EVENT_TRIGGER_ID_MAX; j++ )
      SET_TRIGGER_IN_PATTERN( eventTriggerPattern, j );
    FLIP_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
    for ( j = EVENT_TRIGGER_ID_MIN; j <= EVENT_TRIGGER_ID_MAX; j++ )
      assert( (j == i && !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) ||
	      (j != i && TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, j)) );
    assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  }

  ZERO_TRIGGER_PATTERN( eventTriggerPattern );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  VALIDATE_TRIGGER_PATTERN( eventTriggerPattern );
  assert( !TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  INVALIDATE_TRIGGER_PATTERN( eventTriggerPattern );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );

  INVALIDATE_TRIGGER_PATTERN( eventTriggerPattern );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    SET_TRIGGER_IN_PATTERN( eventTriggerPattern, i );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  VALIDATE_TRIGGER_PATTERN( eventTriggerPattern );
  assert( !TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
  INVALIDATE_TRIGGER_PATTERN( eventTriggerPattern );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ )
    assert( TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );

  ZERO_TRIGGER_PATTERN( eventTriggerPattern );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern, 1 );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern, 45 );
  ZERO_TRIGGER_PATTERN( eventTriggerPattern1 );
  VALIDATE_TRIGGER_PATTERN( eventTriggerPattern1 );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern1, 1 );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern1, 2 );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern1, 44 );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_INVALID( eventTriggerPattern1 ) );
  assert( TRIGGER_PATTERN_VALID( eventTriggerPattern1 ) );
  COPY_TRIGGER_PATTERN( eventTriggerPattern, eventTriggerPattern1 );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern ) );
  assert( TRIGGER_PATTERN_INVALID( eventTriggerPattern1 ) );
  assert( !TRIGGER_PATTERN_VALID( eventTriggerPattern1 ) );
  for ( i = EVENT_TRIGGER_ID_MIN; i <= EVENT_TRIGGER_ID_MAX; i++ ) {
    if ( i == 1 || i == 45 ) {
      assert( TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
      assert( TEST_TRIGGER_IN_PATTERN( eventTriggerPattern1, i ) );
    } else {
      assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern, i ) );
      assert( !TEST_TRIGGER_IN_PATTERN( eventTriggerPattern1, i ) );
    }
  }
  
  ZERO_TRIGGER_PATTERN( eventTriggerPattern );
  assert( TRIGGER_PATTERN_OK(eventTriggerPattern) );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern, EVENT_TRIGGER_ID_MIN );
  assert( TRIGGER_PATTERN_OK(eventTriggerPattern) );
  SET_TRIGGER_IN_PATTERN( eventTriggerPattern, EVENT_TRIGGER_ID_MAX );
  assert( TRIGGER_PATTERN_OK(eventTriggerPattern) );
  VALIDATE_TRIGGER_PATTERN(eventTriggerPattern);
  assert( TRIGGER_PATTERN_OK(eventTriggerPattern) );
  memset( eventTriggerPattern, 0xff, sizeof(eventTriggerPattern) );
  assert( !TRIGGER_PATTERN_OK(eventTriggerPattern) );
  INVALIDATE_TRIGGER_PATTERN(eventTriggerPattern);
  assert( !TRIGGER_PATTERN_OK(eventTriggerPattern) );
} /* End of checkTriggerPattern */

/* Check the DetectorPattern field of the Event header */
void checkDetectorPattern() {
  struct eventHeaderStruct header;
  eventDetectorPatternType eventDetectorPattern, eventDetectorPattern1;
  int i, j;

  /* Some basic checks on the sizes */
  assert( sizeof( eventDetectorPattern ) > 0 );
  assert( sizeof( eventDetectorPattern ) == EVENT_DETECTOR_PATTERN_WORDS*4 );
  assert( sizeof( eventDetectorPattern ) ==
	  sizeof( eventDetectorPatternType ) );
  assert( sizeof( header.eventDetectorPattern ) ==
	  sizeof( eventDetectorPattern ) );

  ZERO_DETECTOR_PATTERN( eventDetectorPattern );
  assert( eventDetectorPattern[0] == 0 );

  memset( &eventDetectorPattern, 0xff, EVENT_DETECTOR_PATTERN_WORDS*4 );
  assert( eventDetectorPattern != 0 );
  ZERO_DETECTOR_PATTERN( eventDetectorPattern );
  assert( eventDetectorPattern[0] == 0 );

  ZERO_DETECTOR_PATTERN( eventDetectorPattern );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    SET_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    CLEAR_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    FLIP_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    FLIP_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ ) {
    ZERO_DETECTOR_PATTERN( eventDetectorPattern );
    SET_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
    for ( j = EVENT_DETECTOR_ID_MIN; j <= EVENT_DETECTOR_ID_MAX; j++ )
      assert( (j == i && TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) ||
	      (j != i && !TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) );
    assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  }
  
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ ) {
    for ( j = EVENT_DETECTOR_ID_MIN; j <= EVENT_DETECTOR_ID_MAX; j++ )
      SET_DETECTOR_IN_PATTERN( eventDetectorPattern, j );
    CLEAR_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
    for ( j = EVENT_DETECTOR_ID_MIN; j <= EVENT_DETECTOR_ID_MAX; j++ )
      assert( (j == i && !TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) ||
	      (j != i && TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) );
    assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  }

  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ ) {
    ZERO_DETECTOR_PATTERN( eventDetectorPattern );
    FLIP_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
    for ( j = EVENT_DETECTOR_ID_MIN; j <= EVENT_DETECTOR_ID_MAX; j++ )
      assert( (j == i && TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) ||
	      (j != i && !TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) );
    assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  }
  
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ ) {
    for ( j = EVENT_DETECTOR_ID_MIN; j <= EVENT_DETECTOR_ID_MAX; j++ )
      SET_DETECTOR_IN_PATTERN( eventDetectorPattern, j );
    FLIP_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
    for ( j = EVENT_DETECTOR_ID_MIN; j <= EVENT_DETECTOR_ID_MAX; j++ )
      assert( (j == i && !TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) ||
	      (j != i && TEST_DETECTOR_IN_PATTERN(eventDetectorPattern,j)) );
    assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  }

  ZERO_DETECTOR_PATTERN( eventDetectorPattern );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  VALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( !DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  INVALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );

  INVALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    SET_DETECTOR_IN_PATTERN( eventDetectorPattern, i );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  VALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( !DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
  INVALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ )
    assert( TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );

  ZERO_DETECTOR_PATTERN( eventDetectorPattern );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern, 1 );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern, 7 );
  ZERO_DETECTOR_PATTERN( eventDetectorPattern1 );
  VALIDATE_DETECTOR_PATTERN( eventDetectorPattern1 );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern1, 1 );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern1, 2 );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern1, 8 );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_INVALID( eventDetectorPattern1 ) );
  assert( DETECTOR_PATTERN_VALID( eventDetectorPattern1 ) );
  COPY_DETECTOR_PATTERN( eventDetectorPattern, eventDetectorPattern1 );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern ) );
  assert( DETECTOR_PATTERN_INVALID( eventDetectorPattern1 ) );
  assert( !DETECTOR_PATTERN_VALID( eventDetectorPattern1 ) );
  for ( i = EVENT_DETECTOR_ID_MIN; i <= EVENT_DETECTOR_ID_MAX; i++ ) {
    if ( i == 1 || i == 7 ) {
      assert( TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
      assert( TEST_DETECTOR_IN_PATTERN( eventDetectorPattern1, i ) );
    } else {
      assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern, i ) );
      assert( !TEST_DETECTOR_IN_PATTERN( eventDetectorPattern1, i ) );
    }
  }

  ZERO_DETECTOR_PATTERN( eventDetectorPattern );
  assert( DETECTOR_PATTERN_OK( eventDetectorPattern ) );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern, EVENT_DETECTOR_ID_MIN );
  assert( DETECTOR_PATTERN_OK( eventDetectorPattern ) );
  SET_DETECTOR_IN_PATTERN( eventDetectorPattern, EVENT_DETECTOR_ID_MAX );
  assert( DETECTOR_PATTERN_OK( eventDetectorPattern ) );
  VALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( DETECTOR_PATTERN_OK( eventDetectorPattern ) );
  memset( &eventDetectorPattern, 0xfe, sizeof(eventDetectorPattern) );
  assert( !DETECTOR_PATTERN_OK( eventDetectorPattern ) );
  INVALIDATE_DETECTOR_PATTERN( eventDetectorPattern );
  assert( !DETECTOR_PATTERN_OK( eventDetectorPattern ) );
} /* End of checkDetectorPattern */

/* Check the Host IDs (LDC id, GDC id etc...) */
void checkHostId () {
  eventHostIdType id;
  struct eventHeaderStruct header;
  unsigned int i;
  time_t startTime;

  assert( sizeof( eventHostIdType ) == sizeof( id ) );
  assert( sizeof( eventLdcIdType )  == sizeof( id ) );
  assert( sizeof( eventGdcIdType )  == sizeof( id ) );
  assert( sizeof( id ) == sizeof( header.eventLdcId ) );
  assert( sizeof( id ) == sizeof( header.eventGdcId ) );
  assert( sizeof( header.eventLdcId ) == sizeof( header.eventGdcId ) );

  srandom( 0 );
  startTime = time( NULL );
  do {
    for ( i = 0; i != 1000; i++ ) {
      id = random();
      if ( id >= HOST_ID_MIN && id <= HOST_ID_MAX )
	assert( id != VOID_ID );
    }
  } while ( time( NULL ) - startTime < 1 );
} /* End of checkHostId */

/* Check the timestamp */
void checkTimestamp () {
  struct eventHeaderStruct evH;
  struct eventHeaderStruct *evHptr = &evH;
#ifdef __ia64__
  const int expSize = 4;
#else
  time_t t0;
  const int expSize = sizeof( time_t );
#endif

  assert( expSize == sizeof( eventTimestampType ) );
#ifndef __ia64__
  assert( sizeof( t0 ) == sizeof( eventTimestampType ) );
  assert( sizeof( t0 ) == sizeof( evH.eventTimestamp ) );
  assert( sizeof( t0 ) == sizeof( evHptr->eventTimestamp ) );
#endif
  assert( expSize == sizeof( eventTimestampType ) );
  assert( expSize == sizeof( evH.eventTimestamp ) );
  assert( expSize == sizeof( evHptr->eventTimestamp ) );
  assert( sizeof( eventTimestampType ) == sizeof( evH.eventTimestamp ) );
} /* End of checkTimestamp */

/* Check the Event header structure */
void checkEventHeaderStruct() {
  checkHeaderLength();
  checkPreamble();
  checkMagic();
  checkEventType();
  checkEventId();
  checkTypeAttribute();
  checkTriggerPattern();
  checkDetectorPattern();
  checkHostId();
  checkTimestamp();
} /* End of checkEventHeaderStruct */

/* Check the event structure (header + payload) */
void checkEventStruct() {
  unsigned char buffer[1000];	/* Our event */
  struct eventHeaderStruct *header = (struct eventHeaderStruct *)buffer;
  struct eventStruct *event = (struct eventStruct *)buffer;
  long p1, p2;
  int i;
  unsigned char *p;

  /* Some basic checks on the various pointers */
  assert( (void *)buffer == (void *)header );
  assert( (void *)event  == (void *)header );
  assert( (void *)buffer == (void *)event );

  /* Be sure that our buffer is big enough */
  assert( sizeof( buffer ) > sizeof( header ) );
  assert( sizeof( buffer ) > sizeof( event ) );
  
  /* Verify that the payload starts where it is supposed to */
  p1 = (long)buffer;
  p2 = (long)&(event->eventRawData);
  assert( p1 + EVENT_HEAD_BASE_SIZE == p2 );

  /* Fill the buffer with zeros and the event header with unique
     patterns */
  for ( i = 0; i != sizeof( buffer ); buffer[i++] = 0 );
  header->eventSize = 0x11121314;
  header->eventMagic = EVENT_MAGIC_NUMBER;
  header->eventHeadSize = 0xf1f2f3f4;
  header->eventVersion = 0x21222324;
  header->eventType = 0xe1e2e3e4;
  header->eventRunNb = 0xa1a2a3a4;
  for ( p=(unsigned char *)&(header->eventId), i = 0; i!=EVENT_ID_BYTES; i++ )
    p[i] = 0x30 | i;
  header->eventTriggerPattern[0] = 0xd1d2d3d4;
  header->eventTriggerPattern[1] = 0xd5d6d7d8;
  header->eventDetectorPattern[0]  = 0x61626364;
  for ( p=(unsigned char *)&(header->eventTypeAttribute), i = 0;
	i != ALL_ATTRIBUTE_BYTES;
	i++ )
    p[i] = 0x40 | i;
  header->eventLdcId = 0xc1c2c3c4;
  header->eventGdcId = 0x51525354;
  header->eventTimestamp = 0x66116611;

  /* The header must have all fields != 0 */
  for ( i = 0; i != EVENT_HEAD_BASE_SIZE; i++ )
    assert( buffer[i] != 0 );

  /* The buffer must still be zero */
  for ( ; i != sizeof( buffer ); i++ )
    assert( buffer[i] == 0 );

  /* The header and the event header must have the same fields set to
     the same value */
  assert( header->eventSize == event->eventHeader.eventSize );
  assert( header->eventMagic == event->eventHeader.eventMagic );
  assert( header->eventHeadSize == event->eventHeader.eventHeadSize );
  assert( header->eventVersion == event->eventHeader.eventVersion );
  assert( header->eventType == event->eventHeader.eventType );
  assert( header->eventRunNb == event->eventHeader.eventRunNb );
  assert( EQ_EVENT_ID( header->eventId, event->eventHeader.eventId ) );
  assert( header->eventTriggerPattern ==
	  event->eventHeader.eventTriggerPattern );
  for ( i = 0; i != ALL_ATTRIBUTE_WORDS; i++ )
    assert( header->eventTypeAttribute[i] ==
	    event->eventHeader.eventTypeAttribute[i] );
  assert( header->eventLdcId == event->eventHeader.eventLdcId );
  assert( header->eventGdcId == event->eventHeader.eventGdcId );

  /* Now do the test the other way around: reset the first bytes of
     the buffer and set the payload portion. The header must have all
     fields set to zero. */
  for ( i = 0; i != EVENT_HEAD_BASE_SIZE; i++ )
    buffer[i] = 0;
  for ( ; i != sizeof( buffer ); i++ )
    buffer[i] = i;
  assert( header->eventSize == 0 );
  assert( header->eventMagic == 0 );
  assert( header->eventHeadSize == 0 );
  assert( header->eventVersion == 0 );
  assert( header->eventType == 0 );
  assert( header->eventRunNb == 0 );
  assert( EVENT_ID_GET_BUNCH_CROSSING( header->eventId) == 0 &&
	  EVENT_ID_GET_ORBIT( header->eventId ) == 0 );
  assert( header->eventTriggerPattern[0] == 0 );
  assert( header->eventTriggerPattern[1] == 0 );
  for ( i = 0; i != ALL_ATTRIBUTE_WORDS; i++ )
    assert( header->eventTypeAttribute[i] == 0 );
  assert( header->eventLdcId == 0 );
  assert( header->eventGdcId == 0 );
} /* End of checkEventStruct */

/* Create and validate a test case, following the example presented at
   the ALICE offline meeting on November 11, 2000 (see
   http://aldwww.cern.ch/Presentations/OFFLINE.07.11.2000/eventFormatBW.ps
   for details). We will create the paged event following a procedure
   that matches closely what can be done in real life by the readout
   program. We will then decode the same event using a procedure
   similar to the one that the recording library or the monitoring
   library could follow internally. Finally we will check the data
   content, to verify the correctness of the procedures. */

/* Define the memory banks used to store the base header, the extended
   header, the equipments vector+header and the payloads */
#define NUM_BANKS 3		/* Number of memory banks     */
#define BANK_SIZE 4000		/* Size in bytes of each bank */
unsigned char bank0[ BANK_SIZE ];
unsigned char bank1[ BANK_SIZE ];
unsigned char bank2[ BANK_SIZE ];
void *banks[NUM_BANKS] = {	/* The table to map a bank ID to the bank   */
  bank0, bank1, bank2		/* base address. In real life, this will be */
};				/* done using shared memory                 */
/* We simulate three equipments, each giving a payload. The first
   equipment is composed of two physical input links, each providing
   some data:

     equipment 1 -> first payload in bank1
                    second payload in bank1
     equipment 2 -> payload in bank2
     equipment 3 -> payload in bank2

   We will also have an extended header in bank 0. */
struct {
  unsigned int elementBankId;
  unsigned int elementStartOffset;
  unsigned int elementSize;
} descriptors[] = {

  /* The event header */
  { 0,     4,  sizeof( struct eventHeaderStruct ) +
               sizeof( struct vectorPayloadDescriptorStruct ) +
               3 * ( sizeof( struct equipmentHeaderStruct ) +
		     sizeof( struct eventVectorStruct ) ) },

  /* The extended header */
  { 0,  4*80,  4*100 },

  /* The first equipment second-level vector and two data blocks */
  { 1, 4*  11,  2* sizeof( struct eventVectorStruct ) },
  { 1, 4*  21,  4* 311 },
  { 1, 4* 333,  4*  11 },

  /* The second equipment data block */
  { 2, 4*111,  4*55 },

  /* The third equipment data block */
  { 2,  4*26,  4*33 },

  { 0, 0, 0 } /* Terminator */
};

void initBanks();
void checkBanks();
void ldcEquipment( struct eventVectorStruct * );
void firstEquipment( struct equipmentDescriptorStruct * );
void secondEquipment( struct equipmentDescriptorStruct * );
void thirdEquipment( struct equipmentDescriptorStruct * );

/* Create the test event */
void createTestEvent() {
  struct eventHeaderStruct *header;
  struct vectorPayloadDescriptorStruct *payload;
  struct equipmentDescriptorStruct *equipments;
  int i;
  void *ptr;

  initBanks();
  
  /* Load the payloads using some easy-to-recognize patterns:
       first block:  10 11 12 13 ...
       second block: 20 21 22 23 ...
       third block:  30 31 32 33 ...
     and so on
   */
  for ( i = 0; descriptors[i].elementSize != 0; i++ ) {
    int j;
    unsigned char seed = (i << 4) & 0xf0;
    unsigned char *p = banks[ descriptors[i].elementBankId ] +
                       descriptors[i].elementStartOffset;
    for ( j = descriptors[i].elementSize; j != 0; j-- ) {
      *p++ = seed;
      seed = (seed & 0xf0) | ( (seed+1) & 0x0f );
    }
  }
  checkBanks();

  /* This is the pointer returned by fifoGetFree() (or equivalent) for
     the allocation of the event header */
  ptr = banks[ descriptors[0].elementBankId ] +
    descriptors[0].elementStartOffset;

  /* These pointers we calculate ourselves */
  header = ptr;
  payload = ptr + sizeof( *header );
  equipments = ptr + sizeof( *header ) + sizeof( *payload );

  /* Load the event header */
  header->eventSize = EVENT_HEAD_BASE_SIZE +
    sizeof( struct vectorPayloadDescriptorStruct ) +
    3 * sizeof( struct equipmentDescriptorStruct );
  assert( descriptors[0].elementSize == header->eventSize );
  header->eventMagic = EVENT_MAGIC_NUMBER;
  header->eventHeadSize = EVENT_HEAD_BASE_SIZE;
  header->eventVersion = EVENT_CURRENT_VERSION;
  header->eventType = PHYSICS_EVENT;
  header->eventRunNb = 0;
  LOAD_EVENT_ID( header->eventId, 0, 0, 1 );
  ZERO_TRIGGER_PATTERN(header->eventTriggerPattern);
  RESET_ATTRIBUTES( header->eventTypeAttribute );
  SET_SYSTEM_ATTRIBUTE( header->eventTypeAttribute, ATTR_EVENT_PAGED );
  header->eventLdcId = 1;
  header->eventGdcId = 2;
  checkBanks();

  /* Initialize the payload */
  payload->eventNumEquipments = 3;
  payload->eventExtensionVector.eventVectorSize = 0;
  /* Are the next three steps really needed? */
  payload->eventExtensionVector.eventVectorPointsToVector = FALSE;
  payload->eventExtensionVector.eventVectorBankId = -1;
  payload->eventExtensionVector.eventVectorStartOffset = 0;
  checkBanks();
  for ( i = 0; i != 3; i++ ) {
    /* Reset the equipment header */
    RESET_ATTRIBUTES( equipments[i].equipmentHeader.equipmentTypeAttribute );
    /* The following fields of the equipment header should be loaded
       either from the equipment itself or from configuration
       data... */
    equipments[i].equipmentHeader.equipmentType = i;
    equipments[i].equipmentHeader.equipmentId = 0;
    equipments[i].equipmentHeader.equipmentBasicElementSize = 1<<(i-1);

    /* Reset the equipment vector */
    equipments[i].equipmentVector.eventVectorSize = 0;
    /* The following fields should not be loaded. It is risky to leave
       them unloaded: if the equipment does not load them (by mistake)
       then they will be left to whatever they had and this could
       corrupt other data... */
    equipments[i].equipmentVector.eventVectorPointsToVector = FALSE;
    equipments[i].equipmentVector.eventVectorBankId = -1;
    equipments[i].equipmentVector.eventVectorStartOffset = 0;

    /* The following field is unused: to load or not to load is only a
       matter of personal preference */
    equipments[i].equipmentVector.eventVectorSize = 0;
    
    checkBanks();
  }

  /* Call the equipments */
  ldcEquipment( &payload->eventExtensionVector );
  checkBanks();
  firstEquipment( &equipments[0] );
  checkBanks();
  secondEquipment( &equipments[1] );
  checkBanks();
  thirdEquipment( &equipments[2] );
  checkBanks();
} /* End of createTestEvent */

/* Simulate the "LDC equipment" loading the extended vector */
void ldcEquipment( struct eventVectorStruct *eventExtensionVector ) {
  eventExtensionVector->eventVectorPointsToVector = FALSE;
  eventExtensionVector->eventVectorBankId = descriptors[1].elementBankId;
  eventExtensionVector->eventVectorSize = descriptors[1].elementSize;
  eventExtensionVector->eventVectorStartOffset =
                                       descriptors[1].elementStartOffset;
} /* End of ldcEquipment */

void firstEquipment( struct equipmentDescriptorStruct *eq ) {
  /* The structure myVec normally would be allocated from a local pool */
  struct eventVectorStruct *myVec =
    banks[descriptors[2].elementBankId] +
    descriptors[2].elementStartOffset;

  assert( 2 * sizeof( *myVec ) == descriptors[2].elementSize );

  /* This is the data coming from the first link (payload 1A) */
  myVec[0].eventVectorBankId = descriptors[3].elementBankId;
  myVec[0].eventVectorPointsToVector = FALSE;
  myVec[0].eventVectorSize = descriptors[3].elementSize;
  myVec[0].eventVectorStartOffset =
    descriptors[3].elementStartOffset;

  /* This is the data coming from the second link (payload 1B) */
  myVec[1].eventVectorBankId = descriptors[4].elementBankId;
  myVec[1].eventVectorPointsToVector = FALSE;
  myVec[1].eventVectorSize = descriptors[4].elementSize;
  myVec[1].eventVectorStartOffset =
    descriptors[4].elementStartOffset;
  
  eq->equipmentVector.eventVectorBankId = descriptors[2].elementBankId;
  eq->equipmentVector.eventVectorPointsToVector = TRUE;
  eq->equipmentVector.eventVectorSize = 2;
  eq->equipmentVector.eventVectorStartOffset =
    descriptors[2].elementStartOffset;
} /* End of firstEquipment */

void secondEquipment( struct equipmentDescriptorStruct *eq ) {
  eq->equipmentVector.eventVectorBankId = descriptors[5].elementBankId;
  eq->equipmentVector.eventVectorPointsToVector = FALSE;
  eq->equipmentVector.eventVectorSize = descriptors[5].elementSize;
  eq->equipmentVector.eventVectorStartOffset =
    descriptors[5].elementStartOffset;
} /* End of secondEquipment */

void thirdEquipment( struct equipmentDescriptorStruct *eq ) {
  eq->equipmentVector.eventVectorBankId = descriptors[6].elementBankId;
  eq->equipmentVector.eventVectorPointsToVector = FALSE;
  eq->equipmentVector.eventVectorSize = descriptors[6].elementSize;
  eq->equipmentVector.eventVectorStartOffset =
    descriptors[6].elementStartOffset;
} /* End of thirdEquipment */

/* Check the test case we have created */
unsigned char result[ NUM_BANKS * BANK_SIZE ];
unsigned int deriveSize( struct eventHeaderStruct * );
void *getPayload( void *, struct eventVectorStruct * );
void checkTestEvent() {
  void *startPtr;
  struct eventHeaderStruct *header;
  struct vectorPayloadDescriptorStruct *payload;
  struct equipmentDescriptorStruct *equipments;
  void *outPtr;
  struct eventStruct *resultEvent;
  int i;

  /* Initialise the output buffer */
  srandom(0);
  for ( i = 0; i != sizeof( result ); i++ )
    result[i] = random() & 0xff;
  
  /* This is the pointer returned by fifoGetFirst() (or equivalent)
     called to get the next available event */
  startPtr =
    banks[ descriptors[0].elementBankId ] +
    descriptors[0].elementStartOffset;

  /* Initialise some local pointers */
  header = startPtr;
  payload = startPtr + sizeof( *header );
  equipments = startPtr + sizeof( *header ) + sizeof( *payload );
  resultEvent = (struct eventStruct *)result;
  outPtr = result;

  /* Some checks on the event, to be sure it is not corrupted */
  assert( header->eventMagic == EVENT_MAGIC_NUMBER );
  assert( header->eventVersion == EVENT_CURRENT_VERSION );
  if ( TEST_SYSTEM_ATTRIBUTE( header->eventTypeAttribute, ATTR_EVENT_PAGED ) )
    assert( header->eventHeadSize == EVENT_HEAD_BASE_SIZE );

  /* Copy the base header */
  memcpy( outPtr, header, EVENT_HEAD_BASE_SIZE );
  outPtr += EVENT_HEAD_BASE_SIZE;

  /* Override the event size with the size of the streamlined event */
  resultEvent->eventHeader.eventSize = deriveSize( header );

  /* Reset the "paged" attribute */
  CLEAR_SYSTEM_ATTRIBUTE( resultEvent->eventHeader.eventTypeAttribute,
			  ATTR_EVENT_PAGED );

  /* Load the correct headLen and the header extension */
  resultEvent->eventHeader.eventHeadSize =
    payload->eventExtensionVector.eventVectorSize;
  memcpy( outPtr,
	  banks[payload->eventExtensionVector.eventVectorBankId]+
	    payload->eventExtensionVector.eventVectorStartOffset,
	  resultEvent->eventHeader.eventHeadSize );
  outPtr += resultEvent->eventHeader.eventHeadSize;

  /* Load the equipments: equipment header and payload(s) */
  for ( i = 0; i != payload->eventNumEquipments; i++ ) {
    struct equipmentHeaderStruct *eq = outPtr;
    
    memcpy( eq, &equipments[i].equipmentHeader, sizeof(*eq) );
    eq->equipmentSize =
      getPayload( outPtr+sizeof(*eq), &equipments[i].equipmentVector ) -
      outPtr;
    outPtr += eq->equipmentSize;
  }
  assert( outPtr == result + resultEvent->eventHeader.eventSize );
  
  /* As we know the structure of the final event, we can verify that
     the event size matches what we expect to get at the end of the
     streamline */
  assert( header->eventType == PHYSICS_EVENT );
  assert( TEST_SYSTEM_ATTRIBUTE(header->eventTypeAttribute,ATTR_EVENT_PAGED));
  assert( header->eventSize ==
	  EVENT_HEAD_BASE_SIZE +
	  sizeof( struct vectorPayloadDescriptorStruct ) +
	  3 * sizeof( struct equipmentDescriptorStruct ) );
  assert( header->eventSize == PAGED_EVENT_SIZE( header ) );
  
  assert( resultEvent->eventHeader.eventSize ==
	    EVENT_HEAD_BASE_SIZE +	             /* Base header */
	    resultEvent->eventHeader.eventHeadSize + /* Extended header */
	    sizeof( struct equipmentHeaderStruct ) +
	      descriptors[3].elementSize + descriptors[4].elementSize +
	    sizeof( struct equipmentHeaderStruct )+descriptors[5].elementSize +
	    sizeof( struct equipmentHeaderStruct )+descriptors[6].elementSize);

  /* Check the equipments of the paged event */
  for ( i = 0; i != payload->eventNumEquipments; i++ ) {
    int j;
    assert( equipments[i].equipmentHeader.equipmentType == i );
    assert( equipments[i].equipmentHeader.equipmentId == 0 );
    assert( equipments[i].equipmentHeader.equipmentBasicElementSize ==
	    1<<(i-1) );
    for ( j = 0; j != ALL_ATTRIBUTE_WORDS; j++ )
      assert( equipments[i].equipmentHeader.equipmentTypeAttribute[j] == 0 );
  }

  /* Check the equipments of the streamlined event */
  outPtr =
    (void *)resultEvent->eventRawData +
    resultEvent->eventHeader.eventHeadSize;
  for ( i = 0; outPtr != result + resultEvent->eventHeader.eventSize; i++ ) {
    struct equipmentHeaderStruct *eq = outPtr;
    int j;

    assert( eq->equipmentType == i );
    assert( eq->equipmentId == 0 );
    assert( eq->equipmentBasicElementSize == 1<<(i-1) );
    for ( j = 0; j != ALL_ATTRIBUTE_WORDS; j++ )
      assert( eq->equipmentTypeAttribute[j] == 0 );
    outPtr += eq->equipmentSize;
  }
  assert( i == payload->eventNumEquipments );
  
  /* Check the four data blocks: extended header */
  for ( outPtr = resultEvent->eventRawData, i = 0;
	i != descriptors[1].elementSize;
	i++, outPtr++ ) {
    assert( *(char *)outPtr == (0x10 | (i & 0x0f)) );
  }
  outPtr += sizeof( struct equipmentHeaderStruct );

  /* Payload 1A: */
  for ( i = 0; i != descriptors[3].elementSize; i++, outPtr++ ) {
    assert( *(char *)outPtr == (0x30 | (i & 0x0f)) );
  }

  /* Payload 1B: */
  for ( i = 0; i != descriptors[4].elementSize; i++, outPtr++ ) {
    assert( *(char *)outPtr == (0x40 | (i & 0x0f)) );
  }
  outPtr += sizeof( struct equipmentHeaderStruct );

  /* Payload 2: */
  for ( i = 0; i != descriptors[5].elementSize; i++, outPtr++ ) {
    assert( *(char *)outPtr == (0x50 | (i & 0x0f)) );
  }
  outPtr += sizeof( struct equipmentHeaderStruct );

  /* Payload 3: */
  for ( i = 0; i != descriptors[6].elementSize; i++, outPtr++ ) {
    assert( *(char *)outPtr == (0x60 | (i & 0x0f)) );
  }

  /* Check the locations of the result buffer that should not have
     been modified by the streamline process */
  srandom(0);
  for ( i = 0; i != sizeof( result ); i++ ) {
    unsigned char expected = random() & 0xff;
    if ( i >= resultEvent->eventHeader.eventSize ) {
      assert( result[i] == expected );
    }
  }
} /* End of checkTestEvent */

/* Derive the payload of an equipment */
unsigned int deriveEquipmentSize( struct eventVectorStruct *vector ) {
  int i;
  struct eventVectorStruct *ptr;
  unsigned int result;
  
  if ( !vector->eventVectorPointsToVector ) {
    result = vector->eventVectorSize;
  } else {
    ptr = banks[vector->eventVectorBankId] + vector->eventVectorStartOffset;
    for ( result = 0, i = 0; i != vector->eventVectorSize; i++ )
      result += deriveEquipmentSize( &ptr[i] );
  }
  return result;
} /* End of deriveEquipmentSize */

/* Derive the size of the event */
unsigned int deriveSize( struct eventHeaderStruct *header ) {
  unsigned int size;
  
  if ( !TEST_SYSTEM_ATTRIBUTE(header->eventTypeAttribute, ATTR_EVENT_PAGED) ) {
    size = header->eventSize;
  } else {
    struct vectorPayloadDescriptorStruct *payload;
    struct equipmentDescriptorStruct *equipments;
    int e;
  
    payload = (void *)header + EVENT_HEAD_BASE_SIZE;
    equipments = (void *)payload + sizeof( *payload );
    size = EVENT_HEAD_BASE_SIZE+payload->eventExtensionVector.eventVectorSize;
    for ( e = 0; e != payload->eventNumEquipments; e++ )
      size += sizeof( struct equipmentHeaderStruct ) +
	      deriveEquipmentSize( &equipments[e].equipmentVector );
  }
  return size;
} /* End of deriveSize */

/* Get the payload associated to the vector, return updated data pointer */
void *getPayload( void *ptr, struct eventVectorStruct *vector ) {
  if ( vector->eventVectorPointsToVector ) {
    int i;
    struct eventVectorStruct *p =
      banks[vector->eventVectorBankId]+vector->eventVectorStartOffset;

    for ( i = 0; i != vector->eventVectorSize; i++ )
      ptr = getPayload( ptr, &p[i] );
  } else {
    memcpy( ptr,
	    banks[vector->eventVectorBankId]+vector->eventVectorStartOffset,
	    vector->eventVectorSize );
    ptr += vector->eventVectorSize;
  }
  return ptr;
} /* End of getPayload */

/* Load a "sure" data pattern in the data banks */
void initBanks() {
  int b, i;
  
  srandom( 0 );
  for ( b = 0; b != NUM_BANKS; b++ ) {
    unsigned char *d = banks[b];
    for ( i = 0; i != BANK_SIZE; i++ ) {
      d[i] = random() & 0xff;
    }
  }
  checkBanks();
} /* End of initBanks */

/* Validate the data patterns stored in the data banks, skip the
   modules that are actually modified on purpose (vectors/payloads) */
void checkBanks() {
  int b, i;
  
  srandom( 0 );
  for ( b = 0; b != NUM_BANKS; b++ ) {
    unsigned char *d = banks[b];
    for ( i = 0; i != BANK_SIZE; i++ ) {
      unsigned char expected = random() & 0xff;

      if ( d[i] != expected ) {
	int j;
	int ok = FALSE;

	for ( j = 0; descriptors[j].elementSize != 0 && !ok; j++ ) {
	  if ( descriptors[j].elementBankId == b ) {
	    if ( i >= descriptors[j].elementStartOffset &&
		 i <  descriptors[j].elementStartOffset +
		      descriptors[j].elementSize ) {
	      ok = TRUE;
	    }
	  }
	}
	assert( ok );
      }
    }
  }
} /* End of checkBanks */

void checkLong32() {
  long32 l32_1, l32_2;
  unsigned long32 l32_3, l32_4;

  assert( sizeof( long32 ) == 32/8 );
  
  assert( sizeof( l32_1 ) == sizeof( long32 ) );
  assert( sizeof( l32_2 ) == sizeof( long32 ) );
  assert( sizeof( l32_3 ) == sizeof( long32 ) );
  assert( sizeof( l32_4 ) == sizeof( long32 ) );

  l32_1 = 0;
  l32_2 = 0;
  assert( l32_1 == l32_2 );
  assert( !( l32_1 < l32_2 ) );
  assert( !( l32_1 > l32_2 ) );

  l32_1 = 1;
  l32_2 = 0;
  assert( !( l32_1 == l32_2 ) );
  assert( !( l32_1 < l32_2 ) );
  assert( l32_1 > l32_2 );

  l32_1 = -1;
  l32_2 = 0;
  assert( !( l32_1 == l32_2 ) );
  assert( !( l32_1 > l32_2 ) );
  assert( l32_1 < l32_2 );

  /* This will overflow the 31 bits... */
  l32_1 = 0x40000000;
  l32_2 = l32_1 * 2;
  assert( l32_2 == 0x80000000 );

  /* This will overflow the 31 bits... */
  l32_1 = 0x7fffffff;
  l32_2 = l32_1 + 1;
  assert( l32_2 == 0x80000000 );

  l32_1 = 0x80000000;
  l32_2 = l32_1 * 2;
  assert( l32_2 == 0 );

  l32_1 = 0xffffffff;
  l32_2 = l32_1 + 1;
  assert( l32_2 == 0 );
  
  l32_1 = 0;
  l32_2 = l32_1 - 1;
  assert( l32_2 == -1 );
} /* End of checkLong32 */

void checkLong64() {
  long64 l64_1, l64_2;
  unsigned long64 l64_3, l64_4;

  assert( sizeof( long64 ) == 64/8 );
  assert( sizeof( l64_1 ) == sizeof( long64 ) );
  assert( sizeof( l64_2 ) == sizeof( long64 ) );
  assert( sizeof( l64_3 ) == sizeof( long64 ) );
  assert( sizeof( l64_4 ) == sizeof( long64 ) );

  l64_1 = 0;
  l64_2 = 0;
  assert( l64_1 == l64_2 );
  assert( !( l64_1 < l64_2 ) );
  assert( !( l64_1 > l64_2 ) );

  l64_1 = 1;
  l64_2 = 0;
  assert( !( l64_1 == l64_2 ) );
  assert( !( l64_1 < l64_2 ) );
  assert( l64_1 > l64_2 );

  l64_1 = -1;
  l64_2 = 0;
  assert( !( l64_1 == l64_2 ) );
  assert( !( l64_1 > l64_2 ) );
  assert( l64_1 < l64_2 );

  l64_1 = 0xffffffff;
  l64_2 = l64_1+1;
  assert( l64_2 == 0x100000000LL );

  l64_1 = 0x80000000;
  l64_2 = l64_1 * 4;
  assert( l64_2 == 0x200000000LL );

  l64_1 = 0x1000000000LL;
  l64_2 = 0x0800000000LL;
  assert( l64_1 != l64_2 );
  assert( l64_1 > l64_2 );
  assert( !( l64_1 < l64_2 ) );
} /* End of checkLong64 */

void checkPointer() {
#define CHECK_PTR( t ) {                          \
 t *p1;                                           \
 volatile t *p2;                                  \
 unsigned t *p3;                                  \
 volatile unsigned t *p4;                         \
                                                  \
 assert( sizeof( p1 ) == sizeof( datePointer ) ); \
 assert( sizeof( p2 ) == sizeof( datePointer ) ); \
 assert( sizeof( p3 ) == sizeof( datePointer ) ); \
 assert( sizeof( p4 ) == sizeof( datePointer ) ); \
                                                  \
 assert( sizeof( *p1 ) == sizeof( t ) );          \
 assert( sizeof( *p2 ) == sizeof( t ) );          \
 assert( sizeof( *p3 ) == sizeof( t ) );          \
 assert( sizeof( *p4 ) == sizeof( t ) );          \
}
  CHECK_PTR( char );
  CHECK_PTR( int );
  CHECK_PTR( long );
  CHECK_PTR( long long );
  CHECK_PTR( long32 );
  CHECK_PTR( long64 );
  CHECK_PTR( datePointer );
  
  CHECK_PTR( char* );
  CHECK_PTR( int* );
  CHECK_PTR( long* );
  CHECK_PTR( long long* );
  CHECK_PTR( long32* );
  CHECK_PTR( long64* );
  CHECK_PTR( datePointer* );
} /* End of checkPointer */

/* Check the basics principles of the setup of DATE */
void checkBasics() {
  checkLong32();
  checkLong64();
  checkPointer();
} /* End of checkBasics */

/* Check the paged event structure */
void checkPagedEvent() {
  createTestEvent();
  checkTestEvent();
} /* End of checkPagedEvent */

/* Init a common data header struct to a known pattern */
void initCdh( struct commonDataHeaderStruct *s ) {
  unsigned char *t;
  int i;

  for ( t = (char *)s, i = 0; i != CDH_SIZE; i++ ) {
    t[i] = (unsigned char)(0xff - i);
  }
} /* end of initCdh */

/* Check the content of a common data header structure, return
   the number of differences found */
int checkCdh(struct commonDataHeaderStruct *s ) {
  unsigned char *t;
  int i;
  int d;

  for ( t = (char *)s, d = 0, i = 0; i != CDH_SIZE; i++ ) {
    if ( t[i] != (unsigned char)(0xff - i) ) d++;
  }

  return d;
} /* end of checkCdh */

/* Check the Common Data Header structure */
void checkCommonDataHeaderStruct() {
  struct commonDataHeaderStruct cdh;
  unsigned char *c = (unsigned char *)&cdh;

  assert( CDH_VERSION == OUR_CDH_VERSION );
  
  assert( sizeof( struct commonDataHeaderStruct ) == CDH_SIZE );

  initCdh( &cdh );
  assert( checkCdh( &cdh ) == 0 );

  initCdh( &cdh );
  cdh.cdhBlockLength = 0;
  assert( checkCdh( &cdh ) == 4 );

  initCdh( &cdh );
  cdh.cdhVersion = 0;
  assert( checkCdh( &cdh ) == 1 );
  
  initCdh( &cdh );
  cdh.cdhL1TriggerMessage = 0;
  assert( checkCdh( &cdh ) == 2 );
  
  initCdh( &cdh );
  cdh.cdhMBZ0 = 0;
  assert( checkCdh( &cdh ) == 1 );
  
  initCdh( &cdh );
  cdh.cdhEventId1 = 0;
  assert( checkCdh( &cdh ) == 2 );
  
  initCdh( &cdh );
  cdh.cdhMBZ1 = 0;
  assert( checkCdh( &cdh ) == 1 );
  
  initCdh( &cdh );
  cdh.cdhEventId2 = 0;
  assert( checkCdh( &cdh ) == 3 );
  
  initCdh( &cdh );
  cdh.cdhBlockAttributes = 0;
  assert( checkCdh( &cdh ) == 1 );
  
  initCdh( &cdh );
  cdh.cdhParticipatingSubDetectors = 0;
  assert( checkCdh( &cdh ) == 3 );
  
  initCdh( &cdh );
  cdh.cdhMBZ2 = 0;
  assert( checkCdh( &cdh ) == 1 );
  
  initCdh( &cdh );
  cdh.cdhStatusErrorBits = 0;
  assert( checkCdh( &cdh ) == 3 );
  
  initCdh( &cdh );
  cdh.cdhMiniEventId = 0;
  assert( checkCdh( &cdh ) == 2 );
  
  initCdh( &cdh );
  cdh.cdhTriggerClassesLow = 0;
  assert( checkCdh( &cdh ) == 4 );
  
  initCdh( &cdh );
  cdh.cdhRoiLow = 0;
  assert( checkCdh( &cdh ) == 1 );
  
  initCdh( &cdh );
  cdh.cdhMBZ3 = 0;
  assert( checkCdh( &cdh ) == 2 );
  
  initCdh( &cdh );
  cdh.cdhTriggerClassesHigh = 0;
  assert( checkCdh( &cdh ) == 3 );
  
  initCdh( &cdh );
  cdh.cdhRoiHigh = 0;
  assert( checkCdh( &cdh ) == 4 );

  /* The location of each field in the character stream is correct
     given a LE machine. Not sure about BE... */
  initCdh( &cdh );
  cdh.cdhVersion = 0x12;
  assert( c[7] == 0x12 );

  cdh.cdhEventId2 = 0x112233;
  assert( c[ 8] == 0x33 );
  assert( c[ 9] == 0x22 );
  assert( c[10] == 0x11 );
} /* End of checkCommonDataHeaderStruct */

int main( int argc, char **argv ) {
  printf( "%s\n", &validateEventIdent[4] );

  checkBasics();
  checkVersionNumber();
  checkEventHeaderStruct();
  checkEventStruct();
  checkCommonDataHeaderStruct();
  checkPagedEvent();

  printf( "Validation completed\n" );
  exit( 0 );
} /* End of main */
