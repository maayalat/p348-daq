/* ========================================================================= */
/*                                                                           */
/*                             banksManager.c                                */
/*                             ==============                                */
/*                                                                           */
/* ========================================================================= */
/*                                                                           */
/* DATE banks manager to be used for loading, sizing, mapping and            */
/* initialisation of the memory banks (and the entities included             */
/* therein) associate to a running DATE system.                              */
/*                                                                           */
/* Revision history:                                                         */
/*   1.0   6 Dec 2001  RD       First release, based on rcShm.c 2.5          */
/*   1.1  15 May 2002  RD+AV    Added support for edmAgent and hltAgent      */
/*   1.2  30 Apr 2003  RD       Possible problem in loadPatterns fixed       */
/*   1.3   1 Jul 2004  RD       Cleanup for HLT roles                        */
/*   1.4  15 Nov 2004  RD       dateUnmapBanks added                         */
/*   1.5  10 Dec 2004  RD	dateRefreshBanks added                       */
/*   1.6  14 Dec 2004  RD+JCM+FC Full reload forced when dynamic banks have  */
/*                              changed                                      */
/*   1.7  15 Dec 2004  RD+JCM+FC Bug with smartReload changing sizes fixed   */
/*   1.8  17 Dec 2004  RD+KS    Bug with re-open of PHYSMEM fixed            */
/*                                                                           */
/* Compilation symbols:                                                      */
/*   XTRA_CHECKS   	Run extra integrity checks and issue more messages   */
/*   INITIAL_LOGLEVEL	Initial log level (if undefined: 10)                 */
/*                                                                           */
/* Warning: all DEBUG statements are not compiled unless XTRA_CHECKS is      */
/* defined.                                                                  */
/*                                                                           */
/* Keep in mind: we might have to turn with rcShm not loaded or invalid,     */
/* in which case the conditional debug statements (DO_INFO, DO_ERROR and     */
/* so on) cannot be used.                                                    */
/*                                                                           */
/* ========================================================================= */
/*                                                                           */
#define BANKS_MANAGER_VERSION "1.8"
/*                                                                           */
#define DESCRIPTION "DATE banks manager"
#ifdef AIX
static
#endif
char banksManagerIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
      """ """ BANKS_MANAGER_VERSION """ compiled """ __DATE__ """ """ __TIME__;
/*                                                                           */
/* ------------------------------------------------------------------------- */
#include <sys/shm.h>		/* Needed for shmget, shmat, shmdt, ftok...  */
#include <sys/stat.h>		/* Needed for protections: S_*               */
#include <sys/types.h>          /* Needed for stat                           */
#include <errno.h>		/* Needed for errno & E* values              */
#include <stdio.h>		/* Needed for FILE, printf, popen...         */
#include <stdlib.h>		/* Needed for free, getenv                   */
#include <unistd.h>		/* Needed for close() (and not for open()?)  */
#include <limits.h>		/* Needed for INT_MAX                        */
#include <fcntl.h>              /* Needed for open(), close ()               */
#include <sys/mman.h>           /* Needed for mmap()                         */
#include <sys/ioctl.h>          /* Needed for ioctl()                        */       
#include <ctype.h>              /* Needed for isdigit()                      */
/*                                                                           */
#define DATE_IN_BANKS_MODULE
#include "event.h"
#include "dateDb.h"
#include "infoLogger.h"  
#include "rcShm.h"
#include "banksManager.h"
#include "simpleFifo.h"
#include "dateBufferManager.h"
#ifdef Linux
#include "physmem.h"
#endif
#include "dateHlt.h"
/*                                                                           */
/* ------------------------------------------------------------------------- */
/* Static configuration parameters                                           */
/*                                                                           */
/* If a bank contains both control and data buffers, this is the ratio       */
/* between the space allocated for data buffers and control buffers          */
#define BUFFERS_RATIO 0.005
/*                                                                           */
/* Typical number of equipments, used for run-time sizing and checks         */
/* Question: can we get the EXACT number of equipments?                      */
#define TYPICAL_NUM_EQUIPMENTS 3
/*                                                                           */
/* ------------------------------------------------------------------------- */
/* The fields from rcShm we use for our setup                                */
/*                                                                           */
/* Log level: numeric [0..40]                                                */
#define RCSHM_LOG_LEVEL (rcShm->logLevel)
/*                                                                           */
/* rcShm version ID                                                          */
#define RCSHM_RCSHM_ID (rcShm->rcShmId)
/*                                                                           */
/* Event header ID                                                           */
#define RCSHM_EVENT_HEADER_ID (rcShm->eventHeaderId)
/*                                                                           */
/* Master PID                                                                */
#define RCSHM_MASTER_PID (rcShm->masterPid)
/*                                                                           */
/* Full size of the segment where the block is                               */
#define RCSHM_FULL_SHM_SIZE (rcShm->fullShmSize)
/*                                                                           */
/* Size of the shm control block                                             */
#define RCSHM_CONTROL_SHM_SIZE (rcShm->controlShmSize)
/*                                                                           */
/* Maximum event size                                                        */
#define RCSHM_MAX_EVENT_SIZE (rcShm->maxEventSize)
/*                                                                           */
/* Size of the EDM FIFO                                                      */
#define RCSHM_EDMFIFO_SIZE (rcShm->edmFifoSize)
/*                                                                           */
/* Role of the host linked to the control block                              */
#define RCSHM_ROLE_FLAG (rcShm->roleFlag)
/*                                                                           */
/* EDM agent enable (TRUE - non zero - if EDM agent active on this node)     */
#define RCSHM_EDM_AGENT_ENABLE (rcShm->edmEnabled)
/*                                                                           */
/* HLT agent enable (TRUE - non zero - if HLT agent active on this node)     */
#define RCSHM_HLT_AGENT_ENABLE (rcShm->hltEnabled)
/*                                                                           */
/* Paged events flag (TRUE for paged events, FALSE for streamlined events)   */
#define RCSHM_PAGED_EVENTS (rcShm->pagedDataFlag)
/*                                                                           */
/* Minimum and maximum value for the events' pipeline depth                  */
#define RCSHM_PIPELINE_MIN 10
#define RCSHM_PIPELINE_MAX INT_MAX
/*                                                                           */
/* ------------------------------------------------------------------------- */
/*                           Logging control                                 */
/*                                                                           */
#ifndef INITIAL_LOGLEVEL
# define INITIAL_LOGLEVEL 10
#endif
#define LOGFILE       "banksManager"
#define DO_ERROR      if ( logLevel >= LOG_ERROR_TH )
#define DO_INFO       if ( logLevel >= LOG_NORMAL_TH )
#define DO_DETAILED   if ( logLevel >= LOG_DETAILED_TH )
#ifdef  XTRA_CHECKS
# define DO_DEBUG     if ( logLevel >= LOG_DEBUG_TH )
#else
# define DO_DEBUG     if ( FALSE )
#endif
#define DO_LOGBOOK    if ( logLevel != 0 )
#define MSGS_LIMIT    10        /* # identical msgs before going quiet */
/*                                                                           */
/* ------------------------------------------------------------------------- */
/*    Macros for string I/O handling (sized print and sized append)          */
/*                                                                           */
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)
/*                                                                           */
/* ------------------------------------------------------------------------- */
/* The data structures used to describe the entities and the banks. All      */
/* these variables must map onto the corresponding structures given in       */
/* banksManager.h                                                            */
/*                                                                           */
/* Modules including banksManager.h and *using* any of these structures will */
/* have to link with the banks manager library module, which is where the    */
/* data structures will be allocated. If instead none of the structures are  */
/* used, of if they are used "dummy" (e.g. in offline environment) then the  */
/* banks manager library is not needed and the structures below can be       */
/* declared - using the same syntax as in banksManager.h - locally as dummy  */
/* structures.                                                               */
/*                                                                           */
int                 numDateBanks = 0;
void ** volatile    dateBanks = NULL;
long64 *            dateBanksSizes = NULL;
long64 *            dateBanksAllocated = NULL;
dbBankPatternType * dateBanksPatterns = NULL;
dbMemType *         dateBanksSupport = NULL;
int *		    dateBanksId = NULL;
/*                                                                           */
struct daqControlStruct * volatile rcShm = NULL;
long64 rcShmO    = -1;
long64 rcShmSize =  0;
int    rcShmBank = -1;
/*                                                                           */
void * volatile readoutReady = NULL;
long64 readoutReadyO    = -1;
long64 readoutReadySize =  0;
int    readoutReadyBank = -1;
/*                                                                           */
void * volatile readoutFirstLevel = NULL;
long64 readoutFirstLevelO    = -1;
long64 readoutFirstLevelSize =  0;
int    readoutFirstLevelBank = -1;
/*                                                                           */
void * volatile readoutSecondLevel = NULL;
long64 readoutSecondLevelO    = -1;
long64 readoutSecondLevelSize =  0;
int    readoutSecondLevelBank = -1;
/*                                                                           */
void * volatile readoutData = NULL;
long64 readoutDataO    = -1;
long64 readoutDataSize =  0;
int    readoutDataBank = -1;
/*                                                                           */
void * volatile edmReady = NULL;
long64 edmReadyO    = -1;
long64 edmReadySize =  0;
int    edmReadyBank = -1;
void * volatile edmInput = NULL;
/*                                                                           */
void * volatile hltReady = NULL;
long64 hltReadyO    = -1;
long64 hltReadySize =  0;
int    hltReadyBank = -1;
void * volatile hltInput = NULL;
/*                                                                           */
void * volatile hltDataPages = NULL;
long64 hltDataPagesO    = -1;
long64 hltDataPagesSize =  0;
int    hltDataPagesBank = -1;
/*                                                                           */
void * volatile recorderInput = NULL;
/*                                                                           */
void * volatile eventBuilderReady = NULL;
long64 eventBuilderReadyO    = -1;
long64 eventBuilderReadySize =  0;
int    eventBuilderReadyBank = -1;
/*                                                                           */
void * volatile eventBuilderData = NULL;
long64 eventBuilderDataO    = -1;
long64 eventBuilderDataSize =  0;
int    eventBuilderDataBank = -1;
/*                                                                           */
void * physmemAddr = NULL;
int    physmemBank = -1;
/*                                                                           */
/* ------------------------------------------------------------------------- */
/*   		 	       Local variables                               */
/*                                                                           */
int  logLevel = INITIAL_LOGLEVEL;   /* Initial log level (before rcShm's)    */
int  bigphysDeviceFd = -1;          /* File descriptor of Bigphysarea device */
int  bigphysBytes;                  /* Number of allocated Bigphysarea bytes */
int  physmemDeviceFd = -1;	    /* File descriptor for Physmem device    */
long64  physmemBytes;		    /* Size of Physmem memory block          */
int  ourBanks = -1;		    /* The descriptor of our banks           */
int  ourIndex = -1;		    /* The index to the roles DBs            */
char line[1024];		    /* Lined used for all LOGs               */
dbRoleType theRole = dbRoleUndefined; /* Our role                            */
/*                                                                           */
/* ------------------------------------------------------------------------- */
/* Options for IPC shared memory segments' control                           */
/*                                                                           */
#define PROTECTION_PUBLIC \
   ( S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH )
#define CREATE_PUBLIC \
   ( IPC_EXCL | IPC_CREAT | PROTECTION_PUBLIC )
/*                                                                           */
/* ------------------------------------------------------------------------- */
/* Options for BIGPHYS device control                                        */
/*                                                                           */
#define BIGPHYS_ALLOCATE 1                 /* ioctl number for allocation    */
#define BIGPHYS_BYTES    4                 /* ioctl number for page count    */
#define BIGPHYS_CLEAR    5                 /* ioclt number for cleaning      */
/*                                                                           */
/* ------------------------------------------------------------------------- */
/* Got a filename, return the same filename possibly with shell symbols      */
/* properly translated.                                                      */
/*                                                                           */
/* Parameter:                                                                */
/*     inFile   Name of the file to translate                                */
/*                                                                           */
/* Returns:                                                                  */
/*     pointer to static array with translated name of the file              */
/*     NULL in case of error                                                 */
/*                                                                           */
/* Warning: return value lost for consecutive calls, only the last call      */
/* remains valid!                                                            */
/*                                                                           */
static char *translateFilename( const char * const inFile ) {
  FILE *c;
  char  cmd[ FILENAME_MAX+15 ];
  int   status;
  static char res[ FILENAME_MAX ];
  char *date_site;
  int i,l;
  
  /* if it is made only of digits, it is an automatic file name - use DATE_SITE_CONFIG */
  l=strlen(inFile);
  for (i=0;i<l;i++) {
    if (!isdigit(inFile[i])) break;
  }
  if (i==l) {
    date_site=getenv("DATE_SITE_CONFIG");
    if (date_site==NULL) {
      DO_ERROR {
        snprintf( SP(line),
		  "translateFilename: DATE_SITE_CONFIG undefined");
        ERROR_TO( LOGFILE, line );
      }
      return NULL;
    } else {
      snprintf(res,FILENAME_MAX,"%s/membank_%s.key",date_site,inFile);
    }
    return res;
  }

  snprintf( SP(cmd), "/bin/echo \"%s\"", inFile );
  if ( (c = popen(cmd, "r")) == NULL ) {
    DO_ERROR {
      snprintf( SP(line),
		"translateFilename: popen failed errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( LOGFILE, line );
    }
    return NULL;
  }
  if ( fscanf( c, "%s", res ) != 1 ) return NULL;
  if ( (status = pclose(c)) != 0 ) {
    DO_ERROR {
      if ( status == -1 )
	snprintf( SP(line),
		  "translateFilename: pclose failed errno:%d (%s)",
		  errno, strerror(errno) );
      else
	snprintf( SP(line),
		  "translateFilename: command returns status %d (%s)",
		  status, strerror(status) );
      ERROR_TO( LOGFILE, line );
    }
  }
  return res;
} /* End of translateFilename                                               */
/* ------------------------------------------------------------------------ */
/* Deallocate all dynamic structures, invalidate all pointers.              */
/*                                                                          */
static void deallocateAllStructs( void ) {
  if ( dateBanks != NULL ) {
    free( dateBanks );
    dateBanks = NULL;
  }
  if ( dateBanksSizes != NULL ) {
    free( dateBanksSizes );
    dateBanksSizes = NULL;
  }
  if ( dateBanksAllocated != NULL ) {
    free( dateBanksAllocated );
    dateBanksAllocated = NULL;
  }
  if ( dateBanksPatterns != NULL ) {
    free( dateBanksPatterns );
    dateBanksPatterns = NULL;
  }
  if ( dateBanksSupport != NULL ) {
    free( dateBanksSupport );
    dateBanksSupport = NULL;
  }
  if ( dateBanksId != NULL ) {
    free( dateBanksId );
    dateBanksId = NULL;
  }
  numDateBanks = 0;

  rcShm     = NULL;
  rcShmO    = -1;
  rcShmSize =  0;
  rcShmBank = -1;

  readoutReady     = NULL;
  readoutReadyO    = -1;
  readoutReadySize =  0;
  readoutReadyBank = -1;

  readoutFirstLevel     = NULL;
  readoutFirstLevelO    = -1;
  readoutFirstLevelSize =  0;
  readoutFirstLevelBank = -1;

  readoutSecondLevel     = NULL;
  readoutSecondLevelO    = -1;
  readoutSecondLevelSize =  0;
  readoutSecondLevelBank = -1;

  readoutData     = NULL;
  readoutDataO    = -1;
  readoutDataSize =  0;
  readoutDataBank = -1;

  edmReady     = NULL;
  edmReadyO    = -1;
  edmReadySize =  0;
  edmReadyBank = -1;
  edmInput     = NULL;

  hltReady     = NULL;
  hltReadyO    = -1;
  hltReadySize =  0;
  hltReadyBank = -1;
  hltInput     = NULL;

  hltDataPages     = NULL;
  hltDataPagesO    = -1;
  hltDataPagesSize =  0;
  hltDataPagesBank = -1;

  recorderInput = NULL;

  eventBuilderReady     = NULL;
  eventBuilderReadyO    = -1;
  eventBuilderReadySize =  0;
  eventBuilderReadyBank = -1;

  eventBuilderData     = NULL;
  eventBuilderDataO    = -1;
  eventBuilderDataSize =  0;
  eventBuilderDataBank = -1;

  physmemAddr = NULL;
  physmemBank = -1;
} /* End of deallocateAllStructs                                            */
/* ------------------------------------------------------------------------ */
/* Allocate the dateBanks local descriptor structures                       */
/*                                                                          */
/* Return TRUE for OK, FALSE on error                                       */
/*                                                                          */
static int allocateDateBanks( const int b ) {
  int size;
  int i;

  DO_DETAILED {
    snprintf( SP(line), "allocateDateBanks Allocating bank:%d", b );
    INFO_TO( LOGFILE, line );
  }
  
  if ( (numDateBanks = dbBanksDb[b].numBanks) <= 0 ) {
    DO_ERROR {
      snprintf( SP(line),
		"allocateDateBanks Requested to allocate %d banks!",
		numDateBanks );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }

  if ( dateBanks == NULL ) {
    size = sizeof( dateBanks[0] ) * numDateBanks;
    if ( (dateBanks = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "Cannot allocate dateBanks #:%d size:%d errno:%d (%s)",
		  numDateBanks, size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      goto errorExit;
    }
    for ( i = 0; i != numDateBanks; i++ ) dateBanks[i] = NULL;
  }

  if ( dateBanksSizes == NULL ) {
    size = sizeof( dateBanksSizes[0] ) * numDateBanks;
    if ( (dateBanksSizes = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "Cannot allocate dateBanksSizes #:%d size:%d errno:%d (%s)",
		  numDateBanks, size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      goto errorExit;
    }
    for ( i = 0; i != numDateBanks; i++ )
      dateBanksSizes[i] = dbBanksDb[b].banks[i].size;
  }

  if ( dateBanksAllocated == NULL ) {
    size = sizeof( dateBanksAllocated[0] ) * numDateBanks;
    if ( (dateBanksAllocated = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "Cannot allocate dateBanksAllocated #:%d size:%d errno:%d (%s)",
		  numDateBanks, size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      goto errorExit;
    }
    for ( i = 0; i != numDateBanks; i++ ) dateBanksAllocated[i] = 0;
  }

  if ( dateBanksPatterns == NULL ) {
    size = sizeof( dateBanksPatterns[0] ) * numDateBanks;
    if ( (dateBanksPatterns = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "Cannot allocate dateBanksPatterns #:%d size:%d errno:%d (%s)",
		  numDateBanks, size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      goto errorExit;
    }
    for ( i = 0; i != numDateBanks; i++ ) dateBanksPatterns[i] = 0;
  }

  if ( dateBanksSupport == NULL ) {
    size = sizeof( dateBanksSupport[0] ) * numDateBanks;
    if ( (dateBanksSupport = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "Cannot allocate dateBanksSupport #:%d size:%d errno:%d (%s)",
		  numDateBanks, size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      goto errorExit;
    }
    for ( i = 0; i != numDateBanks; i++ ) dateBanksSupport[i] = dbMemUndefined;
  }

  if ( dateBanksId == NULL ) {
    size = sizeof( dateBanksId[0] ) * numDateBanks;
    if ( (dateBanksId = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "Cannot allocate dateBanksId #:%d size:%d errno:%d (%s)",
		  numDateBanks, size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      goto errorExit;
    }
    for ( i = 0; i != numDateBanks; i++ ) dateBanksId[i] = -1;
  }

  return TRUE;

 errorExit:
  deallocateAllStructs();
  return FALSE;
} /* End of allocateDateBanks                                               */
/* ------------------------------------------------------------------------ */
/* Given a hostname and role, find the entitity that corresponds to it      */
/*                                                                          */
/* Return:                                                                  */
/*   -1     not found                                                       */
/*   else   index of description record                                     */
/*                                                                          */
static int findHost( const char * const hostName,
		     const dbRoleType hostRole ) {
  int r;

  if ( hostName == NULL ) return -1;
  if ( hostName[0] == 0 ) return -1;
  for ( r = 0; r != dbSizeRolesDb; r++ ) {
    if ( dbRolesDb[r].role == hostRole ) {
      if ( strcmp( hostName, dbRolesDb[r].hostname ) == 0 ) {
	return r;
      }
    }
  }
  return -1;
} /* End of findHost                                                        */
/* ------------------------------------------------------------------------ */
/* Load (or realod) the configuration databases                             */
/*                                                                          */
/* Input parameters:                                                        */
/*      doReload  TRUE if databases must be reloaded                        */
/*                                                                          */
/* Returns:                                                                 */
/*     Index to banks database if OK                                        */
/*     -1 on error                                                          */
/*                                                                          */
/* Updates:                                                                 */
/*     ourIndex is changed with the index to the ROLES DB for this node     */
/*                                                                          */
static int loadDbs( const int doReload ) {
  int status;
  int r;
  int b;

  if ( doReload ) {
    if ( (status = dbUnloadBanks()) != DB_UNLOAD_OK ) {
      DO_ERROR {
	snprintf( SP(line),
		  "loadDbs Failed to unload BANKS db status:%d (%s)",
		  status, dbDecodeStatus(status) );
	ERROR_TO( LOGFILE, line );
      }
      return -1;
    }
    if ( (status = dbUnloadRoles()) != DB_UNLOAD_OK ) {
      DO_ERROR {
	snprintf( SP(line),
		  "loadDbs Failed to unload ROLES db status:%d (%s)",
		  status, dbDecodeStatus(status) );
	ERROR_TO( LOGFILE, line );
      }
      return -1;
    }
  }
  if ( (status = dbLoadRoles()) != DB_LOAD_OK ) {
    DO_ERROR {
      snprintf( SP(line),
		"loadDbs Failed to load ROLES db status:%d (%s)",
		status, dbDecodeStatus(status) );
      ERROR_TO( LOGFILE, line );
    }
    return -1;
  }
  if ( (status = dbLoadBanks()) != DB_LOAD_OK ) {
    DO_ERROR {
      snprintf( SP(line),
		"loadDbs Failed to load BANKS db status:%d (%s)",
		status, dbDecodeStatus(status) );
      ERROR_TO( LOGFILE, line );
    }
    return -1;
  }
  if ( (r = findHost( getenv( "DATE_HOSTNAME" ), theRole )) == -1 ) {
    DO_ERROR {
      snprintf( SP(line),
		"loadDbs findHost(%s,%s) returns not-found",
		getenv( "DATE_HOSTNAME" ),
		dbDecodeRole( theRole ) );
      ERROR_TO( LOGFILE, line );
    }
    return -1;
  }
  if ( (b = dbRolesDb[r].bankDescriptor) == -1 ) {
    DO_ERROR {
      snprintf( SP(line),
		"loadDbs Host %s role %s has no banks declared",
		getenv( "DATE_HOSTNAME" ),
		dbDecodeRole( theRole ) );
      ERROR_TO( LOGFILE, line );
    }
    return -1;
  }
  ourIndex = r;
  return b;
} /* End of loadDbs                                                         */
/* ------------------------------------------------------------------------ */
/* Internal routine to find out the BANKS descriptor for our machine-role   */
/*                                                                          */
/* Return -1 on error, index to dbBanksDb if OK                             */
/*                                                                          */
static int findOurBanks( const dbRoleType hostRole ) {
  int b;
  
  /* Load the needed databases */
  if ( (b = loadDbs( FALSE )) == -1 ) {
    DO_ERROR
      ERROR_TO( LOGFILE, "findOurBanks Failed to load banks" );
    return -1;
  }
  
  /* Allocate the banks descriptors */
  if ( !allocateDateBanks( b ) ) {
    DO_ERROR
      ERROR_TO( LOGFILE, "findOurBanks Failed to allocate data banks" );
    return -1;
  }

  return b;
} /* End of findOurBanks                                                    */
/* ------------------------------------------------------------------------ */
/* Map to a generic segment (if possible)                                   */
/*                                                                          */
/* Return -1 NULL for error, pointer if OK                                  */
/*                                                                          */
static void * volatile mapTo( const dbMemType support,
			      const char * const name,
			      const long64 size,
			      const int removeIfNeeded,
			      const int bankNum ) {
  char * realName;
  void * volatile p = NULL;
  FILE *keyf;
  struct stat statbuf;

  DO_DETAILED {
    snprintf( SP(line),
	      "mapTo On input support:%s (%d) key:\"%s\" size:%lld remove:%s bankNum:%d\
 dateBanks:%p dateBanksSizes:%lld dateBanksAllocated:%lld dateBanksPattern:%s\
 dateBanksSupport:%s (%d) dateBanksNum:%d",
	      dbMemTypeNames[support], support,
	      name,
	      size,
	      removeIfNeeded ? "TRUE" : "false",
	      bankNum,
	      dateBanks[ bankNum ],
	      dateBanksSizes[ bankNum ],
	      dateBanksAllocated[ bankNum ],
	      dbDecodeBankPattern( dateBanksPatterns[ bankNum ] ),
	      dbMemTypeNames[dateBanksSupport[ bankNum ]],
	      dateBanksSupport[ bankNum ],
	      dateBanksId[bankNum] );
    INFO_TO( LOGFILE, line );
  }
  
  if ( dateBanks[ bankNum ] != NULL ) {
    DO_DEBUG {
      snprintf( SP(line),
		"mapTo Skipping bank %d already mapped to %p",
		bankNum,
		dateBanks[ bankNum ] );
      INFO_TO( LOGFILE, line );
    }
    return dateBanks[ bankNum ];
  }
  
  if ( (realName = translateFilename( name )) == NULL ) {
    DO_ERROR {
      snprintf( SP(line),
		"mapTo Failed to translate name [%s] to filename",
		name );
      ERROR_TO( LOGFILE, line );
    }
    return NULL;
  }

  if ( dateBanksSizes[bankNum] - dateBanksAllocated[bankNum] > size ) {
    DO_ERROR {
      snprintf( SP(line),
		"mapTo Requested bank %d too big for space\
 total:%lld allocated:%lld available:%lld requestedSize:%lld",
		bankNum,
		dateBanksSizes[bankNum],
		dateBanksAllocated[bankNum],
		dateBanksSizes[bankNum] - dateBanksAllocated[bankNum],
		size );
      ERROR_TO( LOGFILE, line );
    }
    return NULL;
  }

  if ( support == dbMemIpc ) {
    key_t key = -1;
    int shmId = -1;
#ifdef XTRA_CHECKS
    struct shmid_ds shmDs;
#endif
    int retry = 0;
    
  againIpc:
    if ( ++retry != 1 ) {
      int status;
      
      DO_DETAILED {
	snprintf( SP(line), "mapTo IPC retry #:%d", retry );
	INFO_TO( LOGFILE, line );
      }
      if ( p != NULL ) shmdt ( p );
      if ( shmId != -1 ) {
	if ( (status = shmctl (shmId, IPC_RMID, NULL)) != 0 ) {
	  DO_ERROR {
	    snprintf( SP(line),
		      "mapTo Failed to IPC_RMID shmId:%d errno:%d (%s)",
		      shmId, errno, strerror(errno) );
	    ERROR_TO( LOGFILE, line );
	  }
	}
      }
    }
    /* create the file if it does not exists already */
    if (stat(realName,&statbuf)==-1) {
      keyf=fopen(realName,"a");
      if (keyf==NULL) {
        DO_ERROR {
	  snprintf( SP(line), "Failed to open/create key file %s - errno:%d (%s)",
		    realName, errno, strerror(errno) );
	  ERROR_TO( LOGFILE, line );
        }
        return NULL;
      } else {
        fclose(keyf);
      }
    }
    
    if ( (key = ftok( realName, 1 )) == -1 ) {
      DO_ERROR {
	snprintf( SP(line), "mapTo Failed to ftok(%s) errno:%d (%s)",
		  realName, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      return NULL;
    }
    if ( (shmId = shmget( key, (int)size, PROTECTION_PUBLIC|IPC_CREAT )) == (-1) ) {
      if ( retry == 1 && removeIfNeeded ) {
	if ( (shmId = shmget( key, 0, PROTECTION_PUBLIC|IPC_CREAT ))==(-1) ) {
	  DO_ERROR {
	    snprintf( SP(line),
		      "mapTo Failed to re-shmget for remove\
 file:%s realName:%s key:%d size:%lld (=%d) protection:%d errno:%d (%s)",
		      name,
		      realName,
		      key,
		      size, (int)size,
		      PROTECTION_PUBLIC,
		      errno,
		      strerror(errno) );
	    ERROR_TO( LOGFILE, line );
	    if ( errno == EINVAL ) {
	      snprintf( SP(line),
			"Error possibly caused by maximum system default\
 values for IPC. Check content of \"/proc/sys/kernel/shmmax\" and\
 \"/proc/sys/kernel/shmall\" (must be > %lld and > %d)",
			size, (int)size );
	      ERROR_TO( LOGFILE, line );
	    }
	  }
	  return NULL;
	}
	goto againIpc;
      }
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo Failed to shmget\
 file:%s realName:%s key:%d size:%lld (=%d) protection:%d errno:%d (%s)\
 retry:%d removeIfNeeded:%s",
		  name,
		  realName,
		  key,
		  size, (int)size,
		  PROTECTION_PUBLIC,
		  errno,
		  strerror(errno),
		  retry,
		  removeIfNeeded ? "TRUE" : "false" );
	ERROR_TO( LOGFILE, line );
      }
      return NULL;
    }
#ifdef XTRA_CHECKS
    if ( shmctl( shmId, IPC_STAT, &shmDs ) == -1 ) {
      DO_ERROR {
	snprintf( SP(line), "mapTo Failed to shmctl(IPC_STAT) errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "mapTo IPC to %s=%s OK #attachments:%ld",
		  name, realName, shmDs.shm_nattch );
	INFO_TO( LOGFILE, line );
      }
    }
#endif
    if ( (p = shmat( shmId, 0, 0 )) == (void *)(-1) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo Failed to attach to segment %d errno:%d (%s)",
		  shmId, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      return NULL;
    }
    dateBanksId[ bankNum ] = shmId;
  } else if ( support == dbMemBigphys ) {
#ifdef Linux 
    int reqMemSize;
    int bigphysBytes;

    /* Skip this if we have already mapped */
    if ( bigphysDeviceFd != -1 ) return dateBanks[bankNum];

    if( (bigphysDeviceFd = open( realName, O_RDWR )) == -1) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo Failed to open BIGPHYS device %s=%s errno:%d (%s)",
		  name, realName, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      close( bigphysDeviceFd );
      return NULL;
    }
    if( (bigphysBytes = ioctl( bigphysDeviceFd, BIGPHYS_BYTES, 0 )) > 0) {
      /* There is a region already allocated */
      if ( size == 0 )
	reqMemSize = bigphysBytes; /* map everything */
      else
	reqMemSize = size; /* map only a portion */
		
      if ( (p = mmap( 0,
		      reqMemSize,
		      PROT_READ|PROT_WRITE,MAP_SHARED,
		      bigphysDeviceFd,
		      0 )) == (void *)(-1) ) {
	DO_ERROR {
	  snprintf( SP(line),
		    "mapTo Failed to mmap to BIGPHYS\
 requested %d bytes in:%lld bigphysBytes:%d fd:%d errno:%d (%s)",
		    reqMemSize,
		    size,
		    bigphysBytes,
		    bigphysDeviceFd,
		    errno, strerror(errno) );
	  ERROR_TO( LOGFILE, line );
	}
	close( bigphysDeviceFd );
	return NULL;
      }
    } else {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo BIGPHYS device %s=%s has no space allocated",
		  name, realName );
	ERROR_TO( LOGFILE, line );
      }
      close( bigphysDeviceFd );
      return NULL;
    }
#else
    DO_ERROR {
      snprintf( SP(line),
		"mapTo BIGPHYS unsupported on the current architecture" );
      ERROR_TO( LOGFILE, line );
    }
    return NULL;
#endif
  } else if ( support == dbMemPhysmem ) {
#ifdef Linux
    int status;
    
    /* Skip this is we have already mapped */
    if ( physmemDeviceFd != -1 ) {
      DO_DETAILED {
	snprintf( SP(line),
		  "mapTo PHYSMEM already mapped fd:%d @:%p",
		  physmemDeviceFd,
		  dateBanks[bankNum] );
	INFO_TO( LOGFILE, line );
      }
      return dateBanks[bankNum];
    }

    if( (physmemDeviceFd = open( realName, O_RDWR )) == -1) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo Failed to open PHYSMEM device %s=%s errno:%d (%s)",
		  name, realName, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      close( physmemDeviceFd );
      return NULL;
    }
    if( ioctl( physmemDeviceFd, PHYSMEM_GETSIZE, &physmemBytes ) < 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo PHYSMEM device %s=%s has no space allocated",
		  name, realName );
	ERROR_TO( LOGFILE, line );
      }
      close( physmemDeviceFd );
      return NULL;
    }
    if ( physmemBytes < size ) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo PHYSMEM device %s=%s has not enough space\
 requested:%lld got:%lld",
		  name, realName,
		  size,
		  physmemBytes );
	ERROR_TO( LOGFILE, line );
      }
      close( physmemDeviceFd );
      return NULL;
    } 
    if ( (p = mmap( 0,
		    physmemBytes,
		    PROT_READ|PROT_WRITE, MAP_SHARED,
		    physmemDeviceFd, 0 )) == MAP_FAILED ) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo PHYSMEM device %s=%s size:%lld physmemBytes:%lld\
 failed errno:%d (%s)",
		  name, realName,
		  size, physmemBytes,
		  errno, strerror( errno ) );
	ERROR_TO( LOGFILE, line );
	if ( errno == ENOMEM ) {
	  snprintf( SP(line),
		    "Check system resources against the total physmem\
 size %lld",
		    physmemBytes );
	  ERROR_TO( LOGFILE, line );
	}
      }
      close( physmemDeviceFd );
      return NULL;
    }
    if ( (status=ioctl(physmemDeviceFd, PHYSMEM_GETADDR, &physmemAddr)) < 0 ) {
      physmemBank = -1;
      DO_ERROR {
	snprintf( SP(line),
		  "GETADDR of PHYSMEM device %s=%s failed errno:%d (%s)",
		  name, realName,
		  errno, strerror( errno ) );
	ERROR_TO( LOGFILE, line );
      }
      close( physmemDeviceFd );
      return NULL;
    }
    physmemBank = bankNum;
#else
    DO_ERROR {
      snprintf( SP(line),
		"mapTo PHYSMEM unsupported on the current architecture" );
      ERROR_TO( LOGFILE, line );
    }
    return NULL;
#endif
  } else if ( support == dbMemHeap ) {
    if ( (p = malloc( size )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "mapTo Failed to malloc for %lld bytes errno:%d (%s)",
		  size, errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
      return NULL;
    }
  } else {
    DO_ERROR {
      snprintf( SP(line),
		"mapTo Don't know how to map to %s (=%d)",
		dbMemTypeNames[ support ],
		support );
      ERROR_TO( LOGFILE, line );
    }
    return NULL;
  }
  
  dateBanks[ bankNum ] = p;
  dateBanksSizes[ bankNum ] = size;
  dateBanksAllocated[ bankNum ] = 0;
  dateBanksSupport[ bankNum ] = support;

  DO_DETAILED {
    snprintf( SP(line),
	      "mapTo Succeded to map to %s=%s\
 bank:%d size:%lld support:%s address:%p id:%d",
	      name,
	      realName,
	      bankNum,
	      size,
	      dbMemTypeNames[ dateBanksSupport[ bankNum ] ],
	      p,
	      dateBanksId[ bankNum ] );
    INFO_TO( LOGFILE, line );
  }
  
  return p;
} /* End of mapTo                                                           */
/* ------------------------------------------------------------------------ */
/* Check the parameters received from the calling level                     */
/*                                                                          */
/* Return: TRUE if parameters are OK, FALSE on error.                       */
/*                                                                          */
static int checkPars( const char     * const where,
		      const dbRoleType       hostRole,
		      const long32           eventId,
		      const long32           rcShmId ) {
  if ( eventId != EVENT_CURRENT_VERSION
    || rcShmId != RCSHM_H_ID ) {
    DO_ERROR {
      snprintf( SP(line),
		"%s IDs mismatch\
 (eventId ours:%08x received:%08x)\
 (rcShmId ours:%08x received:%08x)",
		where,
		EVENT_CURRENT_VERSION,
		eventId,
		RCSHM_H_ID,
		rcShmId );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }
  if ( hostRole != dbRoleGdc
    && hostRole != dbRoleLdc
    && hostRole != dbRoleEdmHost
    && hostRole != dbRoleDdg
    && hostRole != dbRoleFilter
    && hostRole != dbRoleTriggerHost ) {
    DO_ERROR {
      snprintf( SP(line),
		"%s Invalid input role %d %s",
		where,
		hostRole,
		dbDecodeRole( hostRole ) );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }
  if ( theRole != dbRoleUndefined ) {
    if ( theRole != hostRole ) {
      DO_ERROR {
	snprintf( SP(line),
		  "%s Invalid input role old:%d=%s new:%d=%s",
		  where,
		  theRole, dbDecodeRole( theRole ),
		  hostRole, dbDecodeRole( hostRole ) );
	ERROR_TO( LOGFILE, line );
      }
      return FALSE;
    }
  } else {
    theRole = hostRole;
  }
  return TRUE;
} /* End of checkPars                                                       */
/* ------------------------------------------------------------------------ */
/* Internal entry to map the control bank                                   */
/*                                                                          */
/* Updates: pointers to the bank that incldues the control block and the    */
/* pointer to rcShm.                                                        */
/*                                                                          */
static void mapControlBank( const dbRoleType hostRole,
			    const int b,
			    const int createTheBank ) {
  int l;
  void * volatile p = NULL;
#ifdef XTRA_CHECKS
  int i;
#endif

  /* If already loaded, do nothing */
  if ( rcShm != NULL ) {
    DO_DETAILED
      INFO_TO( LOGFILE,
	       "mapControlBank Control block already loaded: nothing to do" );
    return;
  }

  /* Find out which bank contains the control block */
  for ( l = 0; l != dbBanksDb[b].numBanks; l++ )
    if ( DB_TEST_BIT( &dbBanksDb[b].banks[l].pattern, dbBankControl ) ) break;
  if ( l == dbBanksDb[b].numBanks ) {
    DO_ERROR {
      snprintf( SP(line),
		"mapControlBank Host %s role %s has no control bank defined",
		getenv( "DATE_HOSTNAME" ),
		dbDecodeRole( hostRole ) );
      ERROR_TO( LOGFILE, line );
    }
    return;
  }
#ifdef XTRA_CHECKS
  for ( i = l+1; i != dbBanksDb[b].numBanks; i++ )
    if ( DB_TEST_BIT( &dbBanksDb[b].banks[i].pattern, dbBankControl ) ) break;
  if ( i != dbBanksDb[b].numBanks ) {
    DO_ERROR {
      snprintf( SP(line),
		"mapControlBanks Host %s role %s\
 has multiple control banks (%d+%d) defined",
		getenv( "DATE_HOSTNAME" ),
		dbDecodeRole( hostRole ),
		l,
		i );
      ERROR_TO( LOGFILE, line );
    }
    return;
  }
#endif

  /* Do the mapping */
  if ( dateBanksSizes[l] == -1 )
    dateBanksSizes[l] = sizeof( struct daqControlStruct );
  if ( dateBanksSizes[l] < sizeof( struct daqControlStruct ) ) {
    DO_ERROR {
      snprintf( SP(line),
		"mapControlBank Unable to create DAQ control structure\
 bank:%d size:%lld structure size:%d",
		l,
		dateBanksSizes[l],
		(int)sizeof( struct daqControlStruct ) );
      ERROR_TO( LOGFILE, line );
    }
    return;
  }
  switch ( dbBanksDb[b].banks[l].support ) {
  case dbMemIpc:
  case dbMemBigphys:
  case dbMemPhysmem:
    p = mapTo( dbBanksDb[b].banks[l].support,
	       dbBanksDb[b].banks[l].name,
	       dateBanksSizes[l],
	       createTheBank,
	       l );
    break;
  default:
    DO_ERROR {
      snprintf( SP(line),
		"mapControlBank Cannot map control region for\
 host %s role %s unsupported memory type %d %s bank:[%d].[%d]",
		getenv( "DATE_HOSTNAME" ),
		dbDecodeRole( hostRole ),
		dbBanksDb[b].banks[l].support,
		dbMemTypeNames[ dbBanksDb[b].banks[l].support ],
		b,
		l );
      ERROR_TO( LOGFILE, line );
    }
    return;
  }
  if ( p == NULL ) {
    DO_ERROR
      ERROR_TO( LOGFILE, "mapControlBank Failed to map control segment" );
    return;
  }

  /*
  snprintf( SP(line),
	    ">>> (A1) Bank:%d support DB:%s (%d) our:%s (%d)\
 size:%lld allocated:%lld id:%d pattern:%s",
	    l,
	    dbMemTypeNames[dbBanksDb[b].banks[l].support],
	    dbBanksDb[b].banks[l].support,
	    dbMemTypeNames[dateBanksSupport[l]],
	    dateBanksSupport[l],
	    dateBanksSizes[l],
	    dateBanksAllocated[l],
	    dateBanksId[l],
	    dbDecodeBankPattern( dateBanksPatterns[l] ) );
  INFO_TO( LOGFILE, line );
  */
  
  rcShm = p;
  if ( dateBanksSupport[l] == dbMemUndefined )
    dateBanksSupport[l] = dbBanksDb[b].banks[l].support;

  if ( createTheBank ) {
    /* Initialise the control structure */
    RCSHM_LOG_LEVEL = logLevel;
    RCSHM_EVENT_HEADER_ID = EVENT_CURRENT_VERSION;
    RCSHM_RCSHM_ID = RCSHM_H_ID;
    /*
      The step below is left to the caller level...

    RCSHM_MASTER_PID = 0
    */
    RCSHM_FULL_SHM_SIZE = dbBanksDb[b].banks[l].size;
    RCSHM_CONTROL_SHM_SIZE = sizeof(struct daqControlStruct);
    RCSHM_EDMFIFO_SIZE = EDMFIFOSIZE;
    RCSHM_ROLE_FLAG = hostRole;
  } else {
    logLevel = RCSHM_LOG_LEVEL;
  }

  rcShmO = BV2O( l, p );
  rcShmSize = sizeof( struct daqControlStruct );
  rcShmBank = l;
  dateBanksAllocated[l] += rcShmSize;

  /*
  snprintf( SP(line),
	    ">>> (0) Bank:%d Allocated:%lld Size:%lld",
	    l,
	    dateBanksAllocated[l],
	    dateBanksSizes[l] );
  INFO_TO( LOGFILE, line );
  */

  /* Check the content of the control structure */
  if ( RCSHM_EVENT_HEADER_ID  != EVENT_CURRENT_VERSION
    || RCSHM_RCSHM_ID         != RCSHM_H_ID
    || RCSHM_FULL_SHM_SIZE    != (typeof RCSHM_FULL_SHM_SIZE)dbBanksDb[b].banks[rcShmBank].size
    || RCSHM_CONTROL_SHM_SIZE != sizeof(struct daqControlStruct)
    || RCSHM_EDMFIFO_SIZE     != EDMFIFOSIZE
    || RCSHM_LOG_LEVEL        != logLevel ) {
    /* Better not do DO_ERROR as the control block is foobar! */ {
      snprintf( SP(line),
		"mapControlBank control block configuration error\
 rcShm:%p\
 event header id memory:%08x ours:%08x,\
 control block id memory:%08x ours:%08x,\
 bankdId:%d full bank size memory:%d ours:%lld,\
 control block size memory:%d ours:%d,\
 EDM FIFO size memory:%d ours:%d",
		rcShm,
		RCSHM_EVENT_HEADER_ID,
		EVENT_CURRENT_VERSION,
		RCSHM_RCSHM_ID,
		RCSHM_H_ID,
		rcShmBank,
		RCSHM_FULL_SHM_SIZE,
		dbBanksDb[b].banks[rcShmBank].size,
		RCSHM_CONTROL_SHM_SIZE,
		(int)sizeof(struct daqControlStruct),
		(int)RCSHM_EDMFIFO_SIZE,
		(int)EDMFIFOSIZE );
      ERROR_TO( LOGFILE, line );
    }
    rcShm = NULL;
    return;
  }
} /* End of mapControlBank                                                  */
/* ------------------------------------------------------------------------ */
/* Create and initialise the DATE banks system. Create and initialise the   */
/* control bank. Do not create the other banks.                             */
/*                                                                          */
/* Return: TRUE for OK, FALSE on error.                                     */
/*                                                                          */
int dateInitControlRegion( const dbRoleType hostRole,
			   const long32     eventId,
			   const long32     rcShmId ) {
  /* Check the input parameters */
  if ( !checkPars( "dateInitControlRegion", hostRole, eventId , rcShmId ) )
    return FALSE;
  
  /* Check if we have been already called */
  if ( rcShm != NULL ) {
    DO_ERROR {
      snprintf( SP(line),
		"dateInitControlRegion called again? rcShm=%p", rcShm );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }
  
  /* Find the description of our banks */
  if ( (ourBanks = findOurBanks(hostRole)) == -1 ) {
    DO_ERROR
      ERROR_TO( LOGFILE, "dateInitControlRegion Failed to load the banks DB" );
    return FALSE;
  }

  /* Map the control bank */
  mapControlBank( hostRole, ourBanks, TRUE );
  if ( rcShm == NULL ) {
    DO_ERROR
      ERROR_TO( LOGFILE,
		"dateInitCOntrolRegion Failed to load control block rcShm" );
  }

  return rcShm != NULL;
} /* End of dateInitControlRegion                                           */
/* ------------------------------------------------------------------------ */
/* Initialise the entities contained in the given bank.                     */
/*                                                                          */
/* Return TRUE if OK, FALSE on error.                                       */
/*                                                                          */
static int initBank( const int b,
		     const dbRoleType hostRole,
		     const dbBankPatternType pattern ) {
  dbBankPatternType p = pattern;
  int numSmallBuffers;
  int numBigBuffers;
  long64 sizeAvailable;
  long64 sizeSmallBuffers = 0;
  long64 sizeBigBuffers = 0;
  long64 sizeOfPipelineElement;
  long64 sizeOfFirstLevelVector;
  int i;

  DO_DETAILED {
    snprintf( SP(line),
	      "initBank On entry bank:%d role:%s\
 entities:%s size:%lld allocated:%lld",
	      b,
	      dbDecodeRole( hostRole ),
	      dbDecodeBankPattern( pattern ),
	      dateBanksSizes[b],
	      dateBanksAllocated[b] );
    INFO_TO( LOGFILE, line );
  }

  /* Skip the control block that has been already done */
  DB_CLEAR_BIT( &p, dbBankControl );
  if ( p == 0 ) {
    DO_DETAILED
      INFO_TO( LOGFILE, "initBank Control bank only: initBank returning" );
    return TRUE;
  }

  /* Find out the summary of all buffers to implement in the bank */
  for ( numSmallBuffers = 0, numBigBuffers = 0, i = 0;
	i != DB_NUM_BANK_TYPES;
	i++ ) {
    if ( DB_TEST_BIT( &p, i ) ) {
      switch (i) {
      case dbBankReadoutReadyFifo:
	if ( readoutReady == NULL )
	  numSmallBuffers++;
	break;
      case dbBankReadoutFirstLevelVectors :
	if ( readoutFirstLevel == NULL )
	  numSmallBuffers++;
	break;
      case dbBankReadoutSecondLevelVectors :
	if ( readoutSecondLevel == NULL )
	  numSmallBuffers++;
	break;
      case dbBankEventBuilderReadyFifo :
	if ( eventBuilderReady == NULL )
	  numSmallBuffers++;
	break;
      case dbBankEdmReadyFifo :
	if ( edmReady == NULL )
	  numSmallBuffers++;
	break;
      case dbBankHltReadyFifo :
	if ( hltReady == NULL )
	  numSmallBuffers++;
	break;
      case dbBankHltDataPages :
	if ( hltDataPages == NULL )
	  numSmallBuffers++;
	break;
      case dbBankReadoutDataPages :
	if ( readoutData == NULL )
	  numBigBuffers++;
	break;
      case dbBankEventBuilderDataPages :
	if ( eventBuilderData == NULL )
	  numBigBuffers++;
	break;
      case dbBankReadout :
      case dbBankEventBuilder :
	break;
      default :
	DO_ERROR {
	  snprintf( SP(line),
		    "initBank Unexpected buffer in small/big choice: %s",
		    dbDecodeBankPattern( p ) );
	}
	ERROR_TO( LOGFILE, line );
	return FALSE;
      }
    }
  }
  if ( numSmallBuffers == 0 && numBigBuffers == 0 ) {
    DO_DETAILED
      INFO_TO( LOGFILE,
	       "initBank No small/big buffers: initBank returning" );
    return TRUE;
  }

  /* Calculate base size for small buffers and big buffers */
  sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
  if ( numBigBuffers == 0 ) {
    sizeSmallBuffers = sizeAvailable / numSmallBuffers;
  } else if ( numSmallBuffers == 0 ) {
    sizeBigBuffers = sizeAvailable / numBigBuffers;
  } else {
    int s = sizeAvailable * BUFFERS_RATIO;
    sizeSmallBuffers = s / numSmallBuffers;
    sizeBigBuffers = (sizeAvailable - s) / numBigBuffers;
  }

  /* Calculate sizes of various elements */
  sizeOfPipelineElement = sizeof( void * );
  sizeOfFirstLevelVector = EVENT_HEAD_BASE_SIZE +
    sizeof( struct vectorPayloadDescriptorStruct ) +
    TYPICAL_NUM_EQUIPMENTS * sizeof( struct equipmentDescriptorStruct );

  /*
  snprintf( SP(line),
	    ">>> (A2) Bank:%d support:%s (%d) size:%lld allocated:%lld id:%d pattern:%s\
 readoutData:%p test:%s numBigBuffers:%d numSmallBuffers:%d",
	    b,
	    dbMemTypeNames[dateBanksSupport[b]],
	    dateBanksSupport[b],
	    dateBanksSizes[b],
	    dateBanksAllocated[b],
	    dateBanksId[b],
	    dbDecodeBankPattern( dateBanksPatterns[b] ),
	    readoutData,
	    DB_TEST_BIT( &p, dbBankReadoutDataPages ) ? "TRUE" : "FALSE",
	    numBigBuffers,
	    numSmallBuffers );
  INFO_TO( LOGFILE, line );
  */

  /* Size all entities defined in the pattern */
  if ( DB_TEST_BIT( &p, dbBankReadoutReadyFifo ) ) {
    DB_CLEAR_BIT( &p, dbBankReadoutReadyFifo );
    if ( readoutReady == NULL ) {
      readoutReadyBank = b;
      sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
      if ( p == 0 ) readoutReadySize = sizeAvailable;
      else readoutReadySize = sizeSmallBuffers;
      if ( RCSHM_PIPELINE_MIN > 0 &&
	   (int)(readoutReadySize / sizeOfPipelineElement) < RCSHM_PIPELINE_MIN )
	readoutReadySize = RCSHM_PIPELINE_MIN * sizeOfPipelineElement;
      if ( RCSHM_PIPELINE_MAX > 0 &&
	   (int)(readoutReadySize / sizeOfPipelineElement) > RCSHM_PIPELINE_MAX )
	readoutReadySize = RCSHM_PIPELINE_MAX * sizeOfPipelineElement;
      if ( readoutReadySize > sizeAvailable ) {
	DO_ERROR {
	  snprintf( SP(line),
		    "initBank Bank %d too small for readoutReadyFifo\
 size:%lld allocated:%lld available:%lld needed:%lld\
 pipeline:%d (min:%d max:%d element:%lld),\
 numSmallBuffers:%d sizeSmallBuffers:%lld numBigBuffers:%d sizeBigBuffers:%lld\
 pattern:%s",
		    b,
		    dateBanksSizes[b],
		    dateBanksAllocated[b],
		    sizeAvailable,
		    readoutReadySize,
		    (int)(readoutReadySize / sizeOfPipelineElement),
		    RCSHM_PIPELINE_MIN,
		    RCSHM_PIPELINE_MAX,
		    sizeOfPipelineElement,
		    numSmallBuffers,
		    sizeSmallBuffers,
		    numBigBuffers,
		    sizeBigBuffers,
		    dbDecodeBankPattern( p ) );
	  ERROR_TO( LOGFILE, line );
	}
	return FALSE;
      }
      readoutReadyO = dateBanksAllocated[b];
      readoutReady = BO2V( b, readoutReadyO );
      dateBanksAllocated[b] += readoutReadySize;

      DO_DEBUG {
	snprintf( SP(line),
		  ">>> (1) Bank:%d Allocated:%lld Size:%lld",
		  b,
		  dateBanksAllocated[b],
		  dateBanksSizes[b] );
	INFO_TO( LOGFILE, line );
      }
    }
  }

  if ( DB_TEST_BIT( &p, dbBankReadoutFirstLevelVectors ) ) {
    DB_CLEAR_BIT( &p, dbBankReadoutFirstLevelVectors );
    if ( readoutFirstLevel == NULL ) {
      readoutFirstLevelBank = b;
      sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
      if ( p == 0 ) readoutFirstLevelSize = sizeAvailable;
      else readoutFirstLevelSize = sizeSmallBuffers;
      if ( RCSHM_PIPELINE_MIN > 0 &&
	   (int)(readoutFirstLevelSize / sizeOfFirstLevelVector) <
	   RCSHM_PIPELINE_MIN )
	readoutFirstLevelSize = RCSHM_PIPELINE_MIN * sizeOfFirstLevelVector;
      if ( RCSHM_PIPELINE_MAX > 0 &&
	   (int)(readoutFirstLevelSize / sizeOfFirstLevelVector) >
	   RCSHM_PIPELINE_MAX )
	readoutFirstLevelSize = RCSHM_PIPELINE_MAX * sizeOfFirstLevelVector;
      if ( readoutFirstLevelSize > sizeAvailable ) {
	DO_ERROR {
	  snprintf( SP(line),
		    "initBank Bank %d too small for readoutFirstLevelVector\
 size:%lld allocated:%lld available:%lld needed:%lld\
 pipeline:%d (min:%d max:%d element:%lld TYPICAL_NUM_EQUIPMENTS:%d),\
 numSmallBuffers:%d sizeSmallBuffers:%lld numBigBuffers:%d sizeBigBuffers:%lld\
 pattern:%s",
		    b,
		    dateBanksSizes[b],
		    dateBanksAllocated[b],
		    sizeAvailable,
		    readoutFirstLevelSize,
		    (int)(readoutFirstLevelSize / sizeOfFirstLevelVector),
		    RCSHM_PIPELINE_MIN,
		    RCSHM_PIPELINE_MAX,
		    sizeOfFirstLevelVector,
		    TYPICAL_NUM_EQUIPMENTS,
		    numSmallBuffers,
		    sizeSmallBuffers,
		    numBigBuffers,
		    sizeBigBuffers,
		    dbDecodeBankPattern( p ) );
	  ERROR_TO( LOGFILE, line );
	}
	return FALSE;
      }
      readoutFirstLevelO = dateBanksAllocated[b];
      readoutFirstLevel = BO2V( b, readoutFirstLevelO );
      dateBanksAllocated[b] += readoutFirstLevelSize;
      DO_DEBUG {
	snprintf( SP(line),
		  ">>> (2) Bank:%d Allocated:%lld Size:%lld",
		  b,
		  dateBanksAllocated[b],
		  dateBanksSizes[b] );
	INFO_TO( LOGFILE, line );
      }
    }
  }

  if ( DB_TEST_BIT( &p, dbBankReadoutSecondLevelVectors ) ) {
    DB_CLEAR_BIT( &p, dbBankReadoutSecondLevelVectors );
    if ( readoutSecondLevel == NULL ) {
      readoutSecondLevelBank = b;
      sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
      if ( p == 0 ) readoutSecondLevelSize = sizeAvailable;
      else readoutSecondLevelSize = sizeSmallBuffers;
      if ( readoutSecondLevelSize < sizeof( struct eventVectorStruct ) ) {
	DO_ERROR {
	  snprintf( SP(line),
		    "initBank Bank %d too small for readoutSecondLevelVector\
 size:%lld allocated:%lld available:%lld got:%lld\
 numSmallBuffers:%d sizeSmallBuffers:%lld numBigBuffers:%d sizeBigBuffers:%lld\
 pattern:%s",
		    b,
		    dateBanksSizes[b],
		    dateBanksAllocated[b],
		    sizeAvailable,
		    readoutSecondLevelSize,
		    numSmallBuffers,
		    sizeSmallBuffers,
		    numBigBuffers,
		    sizeBigBuffers,
		    dbDecodeBankPattern( p ) );
	  ERROR_TO( LOGFILE, line );
	}
	return FALSE;
      }
      readoutSecondLevelO = dateBanksAllocated[b];
      readoutSecondLevel = BO2V( b, readoutSecondLevelO );
      dateBanksAllocated[b] += readoutSecondLevelSize;
      DO_DEBUG {
	snprintf( SP(line),
		  ">>> (3) Bank:%d Allocated:%lld Size:%lld",
		  b,
		  dateBanksAllocated[b],
		  dateBanksSizes[b] );
	INFO_TO( LOGFILE, line );
      }
    }
  }

  if ( DB_TEST_BIT( &p, dbBankEdmReadyFifo ) ) {
    DB_CLEAR_BIT( &p, dbBankEdmReadyFifo );
    if ( edmReady == NULL ) {
      edmReadyBank = b;
      sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
      if ( p == 0 ) edmReadySize = sizeAvailable;
      else edmReadySize = sizeSmallBuffers;
      if ( RCSHM_PIPELINE_MIN > 0 &&
	   (int)(edmReadySize / sizeOfPipelineElement) < RCSHM_PIPELINE_MIN )
	edmReadySize = RCSHM_PIPELINE_MIN * sizeOfPipelineElement;
      if ( RCSHM_PIPELINE_MAX > 0 &&
	   (int)(edmReadySize / sizeOfPipelineElement) > RCSHM_PIPELINE_MAX )
	edmReadySize = RCSHM_PIPELINE_MAX * sizeOfPipelineElement;
      if ( edmReadySize > sizeAvailable ) {
	DO_ERROR {
	  snprintf( SP(line),
		    "initBank Bank %d too small for edmReadyFifo\
 size:%lld allocated:%lld available:%lld needed:%lld\
 pipeline:%d (min:%d max:%d element:%lld),\
 numSmallBuffers:%d sizeSmallBuffers:%lld numBigBuffers:%d sizeBigBuffers:%lld\
 pattern:%s",
		    b,
		    dateBanksSizes[b],
		    dateBanksAllocated[b],
		    sizeAvailable,
		    edmReadySize,
		    (int)(edmReadySize / sizeOfPipelineElement),
		    RCSHM_PIPELINE_MIN,
		    RCSHM_PIPELINE_MAX,
		    sizeOfPipelineElement,
		    numSmallBuffers,
		    sizeSmallBuffers,
		    numBigBuffers,
		    sizeBigBuffers,
		    dbDecodeBankPattern( p ) );
	  ERROR_TO( LOGFILE, line );
	}
	return FALSE;
      }
      edmReadyO = dateBanksAllocated[b];
      edmReady = BO2V( b, edmReadyO );
      dateBanksAllocated[b] += edmReadySize;
      DO_DEBUG {
	snprintf( SP(line),
		  ">>> (4) Bank:%d Allocated:%lld Size:%lld",
		  b,
		  dateBanksAllocated[b],
		  dateBanksSizes[b] );
	INFO_TO( LOGFILE, line );
      }
    }
  }

   if ( DB_TEST_BIT( &p, dbBankHltReadyFifo ) ) {
     DB_CLEAR_BIT( &p, dbBankHltReadyFifo );
     if ( hltReady == NULL ) {
       hltReadyBank = b;
       sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
       if ( p == 0 ) hltReadySize = sizeAvailable;
       else hltReadySize = sizeSmallBuffers;
       if ( RCSHM_PIPELINE_MIN > 0 &&
	    (int)(hltReadySize / sizeOfPipelineElement) < RCSHM_PIPELINE_MIN )
	 hltReadySize = RCSHM_PIPELINE_MIN * sizeOfPipelineElement;
       if ( RCSHM_PIPELINE_MAX > 0 &&
	    (int)(hltReadySize / sizeOfPipelineElement) > RCSHM_PIPELINE_MAX )
	 hltReadySize = RCSHM_PIPELINE_MAX * sizeOfPipelineElement;
       if ( hltReadySize > sizeAvailable ) {
	 DO_ERROR {
	   snprintf( SP(line),
		     "initBank Bank %d too small for hltReadyFifo\
  size:%lld allocated:%lld available:%lld needed:%lld\
  pipeline:%d (min:%d max:%d element:%lld),\
  numSmallBuffers:%d sizeSmallBuffers:%lld numBigBuffers:%d sizeBigBuffers:%lld\
  pattern:%s",
		     b,
		     dateBanksSizes[b],
		     dateBanksAllocated[b],
		     sizeAvailable,
		     hltReadySize,
		     (int)(hltReadySize / sizeOfPipelineElement),
		     RCSHM_PIPELINE_MIN,
		     RCSHM_PIPELINE_MAX,
		     sizeOfPipelineElement,
		     numSmallBuffers,
		     sizeSmallBuffers,
		     numBigBuffers,
		     sizeBigBuffers,
		     dbDecodeBankPattern( p ) );
	   ERROR_TO( LOGFILE, line );
	 }
	 return FALSE;
       }
       hltReadyO = dateBanksAllocated[b];
       hltReady = BO2V( b, hltReadyO );
       dateBanksAllocated[b] += hltReadySize;
       DO_DEBUG {
	 snprintf( SP(line),
		   ">>> (5) Bank:%d Allocated:%lld Size:%lld",
		   b,
		   dateBanksAllocated[b],
		   dateBanksSizes[b] );
	 INFO_TO( LOGFILE, line );
       }
     }
   }

   if ( DB_TEST_BIT( &p, dbBankHltDataPages ) ) {
     DB_CLEAR_BIT( &p, dbBankHltDataPages );
     if ( hltDataPages == NULL ) {
       hltDataPagesBank = b;
       sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
       if ( p == 0 ) hltDataPagesSize = sizeAvailable;
       else hltDataPagesSize = sizeSmallBuffers;
       if ( hltDataPagesSize < 10 * sizeof( struct hltDecisionStruct ) ) {
	 DO_ERROR {
	   snprintf( SP(line),
		     "initBank Bank %d too small for hltDataPages\
 size:%lld allocated:%lld available:%lld got:%lld\
 numSmallBuffers:%d sizeSmallBuffers:%lld numBigBuffers:%d sizeBigBuffers:%lld\
 pattern:%s",
		     b,
		     dateBanksSizes[b],
		     dateBanksAllocated[b],
		     sizeAvailable,
		     readoutSecondLevelSize,
		     numSmallBuffers,
		     sizeSmallBuffers,
		     numBigBuffers,
		     sizeBigBuffers,
		     dbDecodeBankPattern( p ) );
	   ERROR_TO( LOGFILE, line );
	 }
	 return FALSE;
       }
       hltDataPagesO = dateBanksAllocated[b];
       hltDataPages = BO2V( b, hltDataPagesO );
       dateBanksAllocated[b] += hltDataPagesSize;

       DO_DEBUG {
	 snprintf( SP(line),
		   ">>> (6) Bank:%d Allocated:%lld Size:%lld",
		   b,
		   dateBanksAllocated[b],
		   dateBanksSizes[b] );
	 INFO_TO( LOGFILE, line );
       }
     }
   }

   if ( DB_TEST_BIT( &p, dbBankEventBuilderReadyFifo ) ) {
     DB_CLEAR_BIT( &p, dbBankEventBuilderReadyFifo );
     if ( eventBuilderReady == NULL ) {
       eventBuilderReadyBank = b;
       sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
       if ( p == 0 ) eventBuilderReadySize = sizeAvailable;
       else eventBuilderReadySize = sizeSmallBuffers;
       eventBuilderReadyO = dateBanksAllocated[b];
       eventBuilderReady = BO2V( b, eventBuilderReadyO );
       dateBanksAllocated[b] += eventBuilderReadySize;
       
       DO_DEBUG {
	 snprintf( SP(line),
		   ">>> (7) Bank:%d Allocated:%lld Size:%lld",
		   b,
		   dateBanksAllocated[b],
		   dateBanksSizes[b] );
	 INFO_TO( LOGFILE, line );
       }
     }
   }

   if ( DB_TEST_BIT( &p, dbBankReadoutDataPages ) ) {
     if ( readoutData == NULL ) {
       DB_CLEAR_BIT( &p, dbBankReadoutDataPages );
       readoutDataBank = b;
       sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
       if ( p == 0 ) readoutDataSize = sizeAvailable;
       else readoutDataSize = sizeBigBuffers;
       readoutDataO = dateBanksAllocated[b];
       readoutData = BO2V( b, readoutDataO );
       dateBanksAllocated[b] += readoutDataSize;

       DO_DEBUG {
	 snprintf( SP(line),
		   ">>> (8) Bank:%d Allocated:%lld Size:%lld",
		   b,
		   dateBanksAllocated[b],
		   dateBanksSizes[b] );
	 INFO_TO( LOGFILE, line );
       }
     }
   }

   if ( DB_TEST_BIT( &p, dbBankEventBuilderDataPages ) ) {
     DB_CLEAR_BIT( &p, dbBankEventBuilderDataPages );
     if ( eventBuilderData == NULL ) {
       eventBuilderDataBank = b;
       sizeAvailable = dateBanksSizes[b] - dateBanksAllocated[b];
       if ( p == 0 ) eventBuilderDataSize = sizeAvailable;
       else eventBuilderDataSize = sizeBigBuffers;
       eventBuilderDataO = dateBanksAllocated[b];
       eventBuilderData = BO2V( b, eventBuilderDataO );
       dateBanksAllocated[b] += eventBuilderDataSize;

       DO_DEBUG {
	 snprintf( SP(line),
		   ">>> (9) Bank:%d Allocated:%lld Size:%lld",
		   b,
		   dateBanksAllocated[b],
		   dateBanksSizes[b] );
	 INFO_TO( LOGFILE, line );
       }
     }
   }
  
   return TRUE;
} /* End of initBank                                                        */
/* ------------------------------------------------------------------------ */
/* Take a pattern of entities and remove all those who are not needed       */
/* for the current configuration.                                           */
/*                                                                          */
static void loadPattern( dbBankPatternType * const pattern ) {
  if ( RCSHM_ROLE_FLAG != dbRoleLdc && RCSHM_ROLE_FLAG != dbRoleFilter ) {
    /* Not a LDC: remove all resources relative to readout */
    DB_CLEAR_BIT( pattern, dbBankReadoutReadyFifo );
    DB_CLEAR_BIT( pattern, dbBankReadoutFirstLevelVectors );
    DB_CLEAR_BIT( pattern, dbBankReadoutSecondLevelVectors );
    DB_CLEAR_BIT( pattern, dbBankReadoutDataPages );
  }
  if ( !RCSHM_EDM_AGENT_ENABLE || RCSHM_ROLE_FLAG != dbRoleLdc ) {
    /* Not a EDM AGENT or not a LDC: remove all resources relative to
       the EDM AGENT */
    DB_CLEAR_BIT( pattern, dbBankEdmReadyFifo );
  }
  if ( !RCSHM_HLT_AGENT_ENABLE || RCSHM_ROLE_FLAG != dbRoleLdc ) {
    /* Not a HLT AGENT or not a LDC: remove all resources relative to
       the HLT AGENT */
    DB_CLEAR_BIT( pattern, dbBankHltAgent );
    DB_CLEAR_BIT( pattern, dbBankHltReadyFifo );
    DB_CLEAR_BIT( pattern, dbBankHltDataPages );
  } else {
    if ( dbRolesDb[ourIndex].hltRole != dbHltRoleHltLdc ) {
      DB_CLEAR_BIT( pattern, dbBankHltDataPages );
    }
  }
  if ( RCSHM_ROLE_FLAG != dbRoleGdc ) {
    /* Not a GDC: remove all recources relative to the event builder */
    DB_CLEAR_BIT( pattern, dbBankEventBuilder );
    DB_CLEAR_BIT( pattern, dbBankEventBuilderReadyFifo );
    DB_CLEAR_BIT( pattern, dbBankEventBuilderDataPages );
  }
  if ( !RCSHM_PAGED_EVENTS ) {
    /* Streamlined events: remove all resources related to paged events */
    DB_CLEAR_BIT( pattern, dbBankReadoutFirstLevelVectors );
    DB_CLEAR_BIT( pattern, dbBankReadoutSecondLevelVectors );
  }
} /* End of loadPattern                                                     */
/* ------------------------------------------------------------------------ */
/* For the given host, loop on all banks and find out which entities are    */
/* actually needed for the given role and the current configuration.        */
/*                                                                          */
/* Return TRUE for OK, FALSE for error.                                     */
/*                                                                          */
static int loadPatterns( const int b ) {
  int l;
  
  for ( l = 0; l != numDateBanks; l++ ) {
    dateBanksPatterns[l] = dbBanksDb[b].banks[l].pattern;
    loadPattern( &dateBanksPatterns[l] );
    DO_DETAILED {
      snprintf( SP(line), "loadPatterns bank:%d pattern:%s",
		l,
		dbDecodeBankPattern( dbBanksDb[b].banks[l].pattern ) );
      snprintf( AP(line), " active:%s",
		dbDecodeBankPattern( dateBanksPatterns[l] ) );
      INFO_TO( LOGFILE, line );
    }
  }
  return TRUE;
} /* End of loadPatterns                                                    */
/* ------------------------------------------------------------------------ */
/* Do the actual initialization of the entities contained in the banks      */
/*                                                                          */
/* Return: TRUE if OK, FALSE on error                                       */
/*                                                                          */
static int doInitTheBanks( void ) {
  int e = 0;

  if ( readoutReady != NULL ) {
    if ( fifoDeclare( readoutReady, readoutReadySize ) != 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to declare readoutReady @%p size:%lld",
		  readoutReady,
		  readoutReadySize );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks FIFO readoutReady declared OK @%p size:%lld",
		  readoutReady,
		  readoutReadySize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( readoutFirstLevel != NULL ) {
    if ( !bmInit( readoutFirstLevel, readoutFirstLevelSize ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to initialize readoutFirstLevel\
 @%p size:%lld error:%s",
		  readoutFirstLevel,
		  readoutFirstLevelSize,
		  bmGetError() );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks Buffer readoutFirstLevel\
 initialized OK @%p size:%lld",
		  readoutFirstLevel,
		  readoutFirstLevelSize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( readoutSecondLevel != NULL ) {
    if ( !bmInit( readoutSecondLevel, readoutSecondLevelSize ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to initialize readoutSecondLevel\
 @%p size:%lld error:%s",
		  readoutSecondLevel,
		  readoutSecondLevelSize,
		  bmGetError() );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks Buffer readoutSecondLevel\
 initialized OK @%p size:%lld",
		  readoutSecondLevel,
		  readoutSecondLevelSize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( readoutData != NULL ) {
    if ( !bmInit( readoutData, readoutDataSize ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to initialize\
 readoutData @%p size:%lld error:%s",
		  readoutData,
		  readoutDataSize,
		  bmGetError() );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks Buffer readoutData\
 initialized OK @%p size:%lld",
		  readoutData,
		  readoutDataSize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( edmReady != NULL ) {
    if ( fifoDeclare( edmReady, edmReadySize ) != 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to declare\
 edmReady @%p size:%lld",
		  edmReady,
		  edmReadySize );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks FIFO edmReady declared OK @%p size:%lld",
		  edmReady,
		  edmReadySize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( hltReady != NULL ) {
    if ( fifoDeclare( hltReady, hltReadySize ) != 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to declare hltReady @%p size:%lld",
		  hltReady,
		  hltReadySize );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks FIFO hltReady declared OK @%p size:%lld",
		  hltReady,
		  hltReadySize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( hltDataPages != NULL ) {
    if ( !bmInit( hltDataPages, hltDataPagesSize ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to initialize hltDataPages\
 @%p size:%lld error:%s",
		  hltDataPages,
		  hltDataPagesSize,
		  bmGetError() );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks Buffer hltDataPages initialized OK @%p size:%lld",
		  hltDataPages,
		  hltDataPagesSize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( eventBuilderReady != NULL ) {
    if ( fifoDeclare( eventBuilderReady, eventBuilderReadySize ) != 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to declare eventBuilderReady @%p size:%lld",
		  eventBuilderReady,
		  eventBuilderReadySize );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks FIFO eventBuilderReady declared OK @%p size:%lld",
		  eventBuilderReady,
		  eventBuilderReadySize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  
  if ( eventBuilderData != NULL ) {
    if ( !bmInit( eventBuilderData, eventBuilderDataSize ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doInitTheBanks Failed to initialize eventBuilderData\
 @%p size:%lld error:%s",
		  eventBuilderData,
		  eventBuilderDataSize,
		  bmGetError() );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doInitTheBanks Buffer eventBuilderData\
 initialized OK @%p size:%lld",
		  eventBuilderData,
		  eventBuilderDataSize );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  return e == 0;
} /* End of doInitTheBanks                                                  */ 
/* ------------------------------------------------------------------------ */
/* Create and map all the banks defined and needed for the given role.      */
/*                                                                          */
/* If requested, create and initialise all entities within the              */
/* banks. Load the base addresses and sizes of all the entities within      */
/* the banks.                                                               */
/*                                                                          */
/* This entry assumes that dateCreateBanks() has been already called, by    */
/* this or by another process...                                            */
/*                                                                          */
/* Return TRUE if OK, FALSE on error.                                       */
/*                                                                          */
int dateMapBanks( const dbRoleType hostRole,
		  const int        eventId,
		  const int        rcShmId,
		  const int        initTheBanks ) {
  int b;
  int l;
  int e;

  /* If possible, load our log level as very first thing */
  if ( rcShm != NULL ) {
    if ( RCSHM_EVENT_HEADER_ID == EVENT_CURRENT_VERSION
      && RCSHM_RCSHM_ID == RCSHM_H_ID ) {
      logLevel = RCSHM_LOG_LEVEL;
    }
  }

  DO_DETAILED
    INFO_TO( LOGFILE, "dateMapBanks Mapping Banks" );

  if ( !checkPars( "dateMapBanks", hostRole, eventId,  rcShmId ) )
    return FALSE;

  /* Find the description of our banks */
  if ( (b = findOurBanks(hostRole)) == -1 ) return FALSE;

  /* Check if we have banks to declare */
  if ( numDateBanks == 0 ) {
    DO_ERROR {
      snprintf( SP(line),
		"dateMapBanks Host %s role %s has no banks declared",
		getenv( "DATE_HOSTNAME" ),
		dbDecodeRole( hostRole ) );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }

  /* If needed, map the control bank. This will load all the control fields
     needed for the next phase */
  mapControlBank( hostRole, b, FALSE /* Do not remove */ );
  if ( rcShm == NULL ) {
    DO_ERROR
      ERROR_TO( LOGFILE, "dateMapBanks failed to load control structure" );
    return FALSE;
  }

  DO_DETAILED
    INFO_TO( LOGFILE,
	     "dateMapBanks control segment mapped, loaded and validated" );

  /* Loop on all banks and find out which banks are actually needed */
  if ( !loadPatterns( b ) ) {
    DO_ERROR
      ERROR_TO( LOGFILE, "dateMapBanks Failed to load banks pattern" );
    return FALSE;
  }

  /* Loop on all banks needed,map and initialise them */
  for ( e = 0, l = 0; l != numDateBanks; l++ ) {
    void * volatile p;

    DO_DETAILED {
      snprintf( SP(line),
		"dateMapBanks bank:%d patterns:%d (%s)",
		l,
		dateBanksPatterns[l],
		dbDecodeBankPattern( dateBanksPatterns[l] ) );
      INFO_TO( LOGFILE, line );
    }
    
    /* Skip banks that are not needed or that are already loaded */
    if ( dateBanksPatterns[l] == 0 ) continue;

    /* Map the bank */
    if ( (p = mapTo( dbBanksDb[b].banks[l].support,
		     dbBanksDb[b].banks[l].name,
		     dbBanksDb[b].banks[l].size,
		     initTheBanks, /* Remove only if during initialisation phase */
		     l )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "dateMapBanks Failed to map host:%s role:%s\
 bank:%d support:%s name:%s size:%lld",
		  getenv( "DATE_HOSTNAME" ),
		  dbDecodeRole( hostRole ),
		  l,
		  dbMemTypeNames[ dbBanksDb[b].banks[l].support ],
		  dbBanksDb[b].banks[l].name,
		  dbBanksDb[b].banks[l].size );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "dateMapBanks Host:%s role:%s\
 bank:%d) %s %s size:%lld mapped OK @ %p\
 dateBanks:%p dateBanksSizes:%lld dateBanksAllocated:%lld dateBanksPatterns:%s\
 dateBanksSupport:%s (%d) dateBanksId:%d",
		  getenv( "DATE_HOSTNAME" ),
		  dbDecodeRole( hostRole ),
		  l,
		  dbMemTypeNames[ dbBanksDb[b].banks[l].support ],
		  dbBanksDb[b].banks[l].name,
		  dbBanksDb[b].banks[l].size,
		  p,
		  dateBanks[l],
		  dateBanksSizes[l],
		  dateBanksAllocated[l],
		  dbDecodeBankPattern( dateBanksPatterns[l] ),
		  dbMemTypeNames[ dateBanksSupport[l] ],
		  dateBanksSupport[l],
		  dateBanksId[l] );
	INFO_TO( LOGFILE, line );
      }
    }
    /* Init the entities belonging to the bank */
    if ( !initBank( l, hostRole, dateBanksPatterns[l] ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "dateMapBanks Failed to init bank %d role:%s for %s",
		  l,
		  dbDecodeRole( hostRole ),
		  dbDecodeBankPattern( dateBanksPatterns[l] ) );
	ERROR_TO( LOGFILE, line );
      }
      e++;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "dateMapBanks Host:%s role:%s bank:%d) %s %s %lld initialised OK",
		  
		  getenv( "DATE_HOSTNAME" ),
		  dbDecodeRole( hostRole ),
		  l,
		  dbMemTypeNames[ dbBanksDb[b].banks[l].support ],
		  dbBanksDb[b].banks[l].name,
		  dbBanksDb[b].banks[l].size );
	INFO_TO( LOGFILE, line );
      }
    }
  }
  if ( e != 0 ) {
    DO_ERROR {
      snprintf( SP(line),
		"dateMapBanks Failed to load %d bank(s)", e );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }

  /* Initialise all structures and buffers */
  if ( initTheBanks ) {
    if ( !doInitTheBanks() ) {
      DO_ERROR
	ERROR_TO( LOGFILE,
		  "dateMapBanks Failed to initialise banks content(s)" );
      return FALSE;
    }
  }

  if ( RCSHM_ROLE_FLAG == dbRoleLdc ) {
    /* Prepare the chain of input/output pointers */
    recorderInput = readoutReady;
    if ( RCSHM_EDM_AGENT_ENABLE ) {
      edmInput = recorderInput;
      recorderInput = edmReady;
    }
    if ( RCSHM_HLT_AGENT_ENABLE ) {
      hltInput = recorderInput;
      recorderInput = hltReady;
    }
  }

  DO_DETAILED
    INFO_TO( LOGFILE, "dateMapBanks: all OK" );

  return TRUE;
} /* End of dateMapBanks                                                    */
/* ------------------------------------------------------------------------ */
/* Free the given memory bank                                               */
/*                                                                          */
static void freeBank( const int b ) {
  DO_DETAILED {
    snprintf( SP(line),
	      "freeBank Freeing bank:%d address:%p suppport:%s (%d)",
	      b,
	      dateBanks[b],
	      dbMemTypeNames[dateBanksSupport[b]],
	      dateBanksSupport[b] );
    INFO_TO( LOGFILE, line );
  }
  if ( dateBanks[b] == NULL )
    /* No banks, nothing to do */
    return;
  
  switch ( dateBanksSupport[b] ) {
  case dbMemHeap :
    free( dateBanks[b] );
    break;
  case dbMemIpc :
    if ( shmdt( dateBanks[b] ) != 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "freeBank shmdt for bank\
 num:%d address:%p failed errno:%d (%s)",
		  b, dateBanks[b], errno, strerror(errno) );
	ERROR_TO( LOGFILE, line );
      }
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "freeBank shmdt for bank num:%d address:%p succeeded",
		  b, dateBanks[b] );
	INFO_TO( LOGFILE, line );
      }
    }
    if ( shmctl( dateBanksId[b], IPC_RMID, NULL ) != 0 ) {
      DO_ERROR {
	snprintf( SP(line),
		  "freeBank shmctl for bank num:%d id:%d failed errno:%d (%s)",
		  b, dateBanksId[b], errno, strerror( errno ) );
	ERROR_TO( LOGFILE, line );
      }
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "freeBank shmctl for bank num:%d id:%d succeeded",
		  b, dateBanksId[b] );
	INFO_TO( LOGFILE, line );
      }
    }
    /* This could be a good place to check the number of attachments... */
    break;
  case dbMemBigphys :
    if ( bigphysDeviceFd != -1 ) {
      close( bigphysDeviceFd );
      bigphysDeviceFd = -1;
      DO_DETAILED
	INFO_TO( LOGFILE, "freeBank BIGPHYS closed" );
    }
    break;
  case dbMemPhysmem :
    if ( physmemDeviceFd != -1 ) {
      if ( munmap( dateBanks[physmemBank], physmemBytes ) != 0 ) {
	DO_ERROR {
	  snprintf( SP(line),
		    "freeBank munmap for physmem @:%p size:%lld failed\
 errno:%d (%s)",
		    dateBanks[physmemBank],
		    physmemBytes,
		    errno, strerror(errno) );
	  ERROR_TO( LOGFILE, line );
	}
      }
      close( physmemDeviceFd );
      physmemDeviceFd = -1;
      DO_DETAILED
	INFO_TO( LOGFILE, "freeBank PHYSMEM closed" );
    }
    break;
  case dbMemUndefined :
    break;
  }
  dateBanks[b] = NULL;
  dateBanksSizes[b] = 0;
  dateBanksAllocated[b] = 0;
  dateBanksPatterns[b] = 0;
  dateBanksSupport[b] = dbMemUndefined;
  dateBanksId[b] = -1;
} /* End of freeBank                                                        */
/* ------------------------------------------------------------------------ */
/* Unmap all banks and invalidate all the pointers.                         */
/*                                                                          */
/* Return TRUE if OK, FALSE on error.                                       */
/*                                                                          */
int dateUnmapBanks( void ) {
  int b;

  for ( b = 0; b < numDateBanks; b++ ) {
    freeBank( b );
  }

  deallocateAllStructs();
  
  return TRUE;
} /* End of dateUnmapBanks                                                  */
/* ------------------------------------------------------------------------ */
/* Save the current value of a rcShm block (if possible).                   */
/*                                                                          */
static inline void saveRcShm( struct daqControlStruct * const oldRcShm ) {
  DO_DETAILED {
    snprintf( SP(line),
	      "saveRcShm Saving rcShm:%p => copy:%p\
 eventHeaderId:%08x\
 controlBlockId:%08x\
 fullBankSize:%d\
 controlBlockSize:%d (expected:%d)\
 edmFifoSize:%d",
	      rcShm,
	      oldRcShm,
	      RCSHM_EVENT_HEADER_ID,
	      RCSHM_RCSHM_ID,
	      RCSHM_FULL_SHM_SIZE,
	      RCSHM_CONTROL_SHM_SIZE, (int)sizeof(struct daqControlStruct),
	      (int)RCSHM_EDMFIFO_SIZE );
    INFO_TO( LOGFILE, line );
  }
  if ( rcShm != NULL )
    memcpy( oldRcShm, rcShm, sizeof( struct daqControlStruct ) );
  else
    memset( oldRcShm, 0xdeadbeef, sizeof( struct daqControlStruct ) );

  rcShm = oldRcShm;
} /* End of saveRcShm                                                       */
/* ------------------------------------------------------------------------ */
/* Restore the old value of a rcShm block as if it had not been realloc'd.  */
/*                                                                          */
static inline void restoreRcShm( struct daqControlStruct * const oldRcShm ) {
  if ( rcShm != NULL ) {
    oldRcShm->fullShmSize = dbBanksDb[ourBanks].banks[rcShmBank].size;
    memcpy( rcShm, oldRcShm, sizeof( struct daqControlStruct ) );
    DO_DETAILED {
      snprintf( SP(line),
		"restoreRcShm Restored copy:%p => rcShm:%p\
 eventHeaderId:%08x\
 controlBlockId:%08x\
 fullBankSize:%d\
 controlBlockSize:%d (expected:%d)\
 edmFifoSize:%d",
		oldRcShm,
		rcShm,
		RCSHM_EVENT_HEADER_ID,
		RCSHM_RCSHM_ID,
		RCSHM_FULL_SHM_SIZE,
		RCSHM_CONTROL_SHM_SIZE, (int)sizeof(struct daqControlStruct),
		(int)RCSHM_EDMFIFO_SIZE );
      INFO_TO( LOGFILE, line );
    }
  } else {
    DO_ERROR ERROR_TO( LOGFILE, "restoreRcShm Requested to restore NULL" );
  }
} /* End of restoreRcShm                                                    */
/* ------------------------------------------------------------------------ */
/* Full, blind reload. Unmap everything, remap and restore the rcShm found  */
/* (hoping nobody else changed it in the mean while)                        */
/*                                                                          */
/* Returns: TRUE if OK, FALSE on error                                      */
/*                                                                          */
static int doFullReload( void ) {
  struct daqControlStruct oldRcShm;
  int stat;

  saveRcShm( &oldRcShm );

  if ( !(stat = dateUnmapBanks()) ) {
    DO_ERROR ERROR_TO( LOGFILE, "doFullReload dateUnmapBanks failed" );
  }

  rcShm = NULL;
  if ( !dateInitControlRegion( theRole, EVENT_CURRENT_VERSION, RCSHM_H_ID ) ) {
    DO_ERROR ERROR_TO( LOGFILE, "doFullReload dateInitControlRegion failed" );
    stat = FALSE;
  }

  if ( rcShm != NULL ) {
    restoreRcShm( &oldRcShm );
  } else {
    stat = FALSE;
    
    DO_ERROR
      ERROR_TO( LOGFILE,
		"doFullReload Cannot restore rcShm (rcShm == NULL)" );
  }

  if ( !dateMapBanks( theRole, EVENT_CURRENT_VERSION, RCSHM_H_ID, TRUE ) ) {
    DO_ERROR ERROR_TO( LOGFILE, "doFullReload dateMapBanks failed" );
    stat = FALSE;
  }

  DO_INFO {
    snprintf( SP(line),
	      "doFullReload Full reload of DATE banks completed %s",
	      stat ? "OK" : "WITH ERROR" );
    if ( stat )
      INFO_TO( LOGFILE, line );
    else
      ERROR_TO( LOGFILE, line );
  }
  
  return stat;
} /* End of doFullReload                                                    */
/* ------------------------------------------------------------------------ */
/* doSmartReload: try to reload the memory banks without touching banks     */
/* that have not changed.                                                   */
/*                                                                          */
/* Return: TRUE if OK, FALSE on error.                                      */
/*                                                                          */
static int doSmartReload( const dbSingleBankDescriptor * const oldBanks ) {
  const dbSingleBankDescriptor * const newBanks =
                            dbBanksDb[ ourBanks ].banks;
  int i;
  struct daqControlStruct oldRcShm;
  int rcShmCopied = FALSE;
  int stat = TRUE;
  int numReloaded = 0;

  for ( i = 0; i < numDateBanks; i++ ) {
    int mustRestoreRcShm = FALSE;
    dbBankPatternType oldPattern, newPattern;
    void * volatile p;
    int j;
    
    if ( newBanks[i].support == oldBanks[i].support
      && newBanks[i].size    == oldBanks[i].size
      && newBanks[i].pattern == oldBanks[i].pattern
      && strcmp( newBanks[i].name, oldBanks[i].name ) == 0 )
      /* Unchanged */
      continue;
    numReloaded++;
    DO_DETAILED {
      snprintf( SP(line),
		"smartReload ourBanks:%d numDateBanks:%d Reloading bank %d",
		ourBanks,
		numDateBanks,
		i );
      snprintf( AP(line),
		" support:%s (old:%s ours:%s)",
		dbMemTypeNames[ newBanks[i].support ],
		dbMemTypeNames[ oldBanks[i].support ],
		dbMemTypeNames[ dateBanksSupport[i] ] );
      snprintf( AP(line),
		" size:%lld (old:%lld ours:%lld)",
		newBanks[i].size,
		oldBanks[i].size,
		dateBanksSizes[i] );
      snprintf( AP(line),
		" pattern:%s",
		dbDecodeBankPattern( newBanks[i].pattern ) );
      snprintf( AP(line),
		" (old:%s",
		dbDecodeBankPattern( oldBanks[i].pattern ) );
      snprintf( AP(line),
		" ours:%s)",
		dbDecodeBankPattern( dateBanksPatterns[i] ) );
      snprintf( AP(line),
		" name:\"%s\" (old:\"%s\" cmp:%d)",
		newBanks[i].name,
		oldBanks[i].name,
		strcmp( oldBanks[i].name, newBanks[i].name ) );
      INFO_TO( LOGFILE, line );
    }
    
    /* Check if this bank had the control block and eventually
       save its content */
    if ( DB_TEST_BIT( &oldBanks[i].pattern, dbBankControl )
      && !rcShmCopied ) {
      saveRcShm( &oldRcShm );
      rcShmCopied = TRUE;
    }

    /* If this bank will have the control block set the "must restore" flag */
    if ( DB_TEST_BIT( &newBanks[i].pattern, dbBankControl ) ) {
      mustRestoreRcShm = TRUE;

      /* If control block has not yet been copied, look for it and copy it */
      if ( !rcShmCopied ) {
	int j;

	for ( j = i+1; j != numDateBanks && !rcShmCopied; j++ ) {
	  if ( DB_TEST_BIT( &oldBanks[j].pattern, dbBankControl ) ) {
	    saveRcShm( &oldRcShm );
	    rcShmCopied = TRUE;
	  }
	}
	if ( !rcShmCopied ) {
	  /* We should never get here. If we do, it means that the
	     control block was not declared in the old banks setup
	     and it has been declared in the new banks setup. We
	     issue an error message and load the control block with
	     whatever we want */
	  saveRcShm( &oldRcShm );
	  stat = FALSE;
	  
	  DO_ERROR
	    ERROR_TO( LOGFILE,
		      "doSmartReload: no rcShm in old banks?" );
	}
      }
    }

    /* Free the old memory block and invalidate the entities it supported */
    newPattern = dateBanksPatterns[i];
    freeBank( i );
    dateBanksPatterns[i] = newPattern;
    oldPattern = oldBanks[i].pattern;
    if ( DB_TEST_BIT( &oldPattern, dbBankControl ) ) {
      DB_CLEAR_BIT( &oldPattern, dbBankControl );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankReadout )
      || DB_TEST_BIT( &oldPattern, dbBankReadoutReadyFifo ) ) {
      readoutReady = NULL;
      readoutReadyO = -1;
      readoutReadySize = 0;
      readoutReadyBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankReadoutReadyFifo );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankReadout )
      || DB_TEST_BIT( &oldPattern, dbBankReadoutFirstLevelVectors ) ) {
      readoutFirstLevel     = NULL;
      readoutFirstLevelO    = -1;
      readoutFirstLevelSize =  0;
      readoutFirstLevelBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankReadoutFirstLevelVectors );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankReadout )
      || DB_TEST_BIT( &oldPattern, dbBankReadoutSecondLevelVectors ) ) {
      readoutSecondLevel     = NULL;
      readoutSecondLevelO    = -1;
      readoutSecondLevelSize =  0;
      readoutSecondLevelBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankReadoutSecondLevelVectors );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankReadout )
      || DB_TEST_BIT( &oldPattern, dbBankReadoutDataPages ) ) {
      readoutData     = NULL;
      readoutDataO    = -1;
      readoutDataSize =  0;
      readoutDataBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankReadoutDataPages );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankReadout )
      || DB_TEST_BIT( &oldPattern, dbBankEdmReadyFifo ) ) {
      edmReady     = NULL;
      edmReadyO    = -1;
      edmReadySize =  0;
      edmReadyBank = -1;
      edmInput     = NULL;
      DB_CLEAR_BIT( &oldPattern, dbBankEdmReadyFifo );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankReadout ) ) {
      DB_CLEAR_BIT( &oldPattern, dbBankReadout );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankHltAgent )
      || DB_TEST_BIT( &oldPattern, dbBankHltReadyFifo ) ) {
      hltReady     = NULL;
      hltReadyO    = -1;
      hltReadySize =  0;
      hltReadyBank = -1;
      hltInput     = NULL;
      DB_CLEAR_BIT( &oldPattern, dbBankHltReadyFifo );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankHltAgent )
      || DB_TEST_BIT( &oldPattern, dbBankHltSecondLevelVectors ) ) {
      DB_CLEAR_BIT( &oldPattern, dbBankHltSecondLevelVectors );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankHltAgent )
      || DB_TEST_BIT( &oldPattern, dbBankHltDataPages ) ) {
      hltDataPages     = NULL;
      hltDataPagesO    = -1;
      hltDataPagesSize =  0;
      hltDataPagesBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankHltDataPages );
    }
    /* Next block unused: if uncommented, it must be filled in!
    if ( DB_TEST_BIT( &oldPattern, dbBankHltAgent ) ) {
      DB_CLEAR_BIT( &oldPattern, dbBankHltAgent );
    }
    */
    if ( DB_TEST_BIT( &oldPattern, dbBankEventBuilder )
      || DB_TEST_BIT( &oldPattern, dbBankEventBuilderReadyFifo ) ) {
      eventBuilderReady     = NULL;
      eventBuilderReadyO    = -1;
      eventBuilderReadySize =  0;
      eventBuilderReadyBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankEventBuilderReadyFifo );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankEventBuilder )
      || DB_TEST_BIT( &oldPattern, dbBankEventBuilderDataPages ) ) {
      eventBuilderData     = NULL;
      eventBuilderDataO    = -1;
      eventBuilderDataSize =  0;
      eventBuilderDataBank = -1;
      DB_CLEAR_BIT( &oldPattern, dbBankEventBuilderDataPages );
    }
    if ( DB_TEST_BIT( &oldPattern, dbBankEventBuilder ) ) {
      DB_CLEAR_BIT( &oldPattern, dbBankEventBuilder );
    }

    for ( j = 0; j != DB_NUM_BANK_TYPES; j++ )
      if ( DB_TEST_BIT( &oldPattern, j ) )
	break;
    if ( j != DB_NUM_BANK_TYPES ) {
      /* There are some bank(s) that we have not handled. The above tests */
      /* are not complete, some more branches have to be filled in!       */
      DO_ERROR {
	snprintf( SP(line),
		  "doSmartReload oldPattern not cleared residue: %s",
		  dbDecodeBankPattern( oldPattern ) );
	ERROR_TO( LOGFILE, line );
      }
      stat = FALSE;
    }
  
    dateBanksSizes[i] = dbBanksDb[ourBanks].banks[i].size;
    if ( dateBanksSizes[i] == -1 )
      dateBanksSizes[i] = sizeof( struct daqControlStruct );
    dateBanksAllocated[i] = 0;
    
    DO_DETAILED {
      snprintf( SP(line),
		"doSmartReload bank:%d pattern:%s",
		i,
		dbDecodeBankPattern( dbBanksDb[ourBanks].banks[i].pattern ) );
      snprintf( AP(line), " active:%s",
		dbDecodeBankPattern( dateBanksPatterns[i] ) );
      snprintf( AP(line),
		" mustRestoreRcShm:%s",
		mustRestoreRcShm ? "TRUE" : "false" );
      INFO_TO( LOGFILE, line );
    }

    /* Skip banks that are not needed */
    if ( dateBanksPatterns[i] == 0 ) continue;

    /* Map the bank */
    if ( (p = mapTo( dbBanksDb[ourBanks].banks[i].support,
		     dbBanksDb[ourBanks].banks[i].name,
		     dateBanksSizes[i],
		     TRUE,
		     i )) == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doSmartReload Failed to map host:%s role:%s\
 bank:%d support:%s name:%s size:%lld",
		  getenv( "DATE_HOSTNAME" ),
		  dbDecodeRole( theRole ),
		  i,
		  dbMemTypeNames[ dbBanksDb[ourBanks].banks[i].support ],
		  dbBanksDb[ourBanks].banks[i].name,
		  dbBanksDb[ourBanks].banks[i].size );
	ERROR_TO( LOGFILE, line );
      }
      stat = FALSE;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doSmartReload Host:%s role:%s\
 bank:%d) %s %s %lld mapped OK @ %p",
		  getenv( "DATE_HOSTNAME" ),
		  dbDecodeRole( theRole ),
		  i,
		  dbMemTypeNames[ dbBanksDb[ourBanks].banks[i].support ],
		  dbBanksDb[ourBanks].banks[i].name,
		  dbBanksDb[ourBanks].banks[i].size,
		  p );
	INFO_TO( LOGFILE, line );
      }
    }
    /* Init the entities belonging to the bank */
    if ( mustRestoreRcShm ) {
      rcShm = p;
      rcShmO = 0;
      rcShmSize = sizeof( struct daqControlStruct );
      rcShmBank = i;
      dateBanksAllocated[i] += sizeof( struct daqControlStruct );
      DO_DEBUG {
	snprintf( SP(line),
		  ">>> (10) Bank:%d Allocated:%lld Size:%lld",
		  i,
		  dateBanksAllocated[i],
		  dateBanksSizes[i] );
	INFO_TO( LOGFILE, line );
      }
      if ( rcShm != NULL ) {
	restoreRcShm( &oldRcShm );
      } else {
	stat = FALSE;
	DO_ERROR
	  ERROR_TO( LOGFILE,
		    "doSmartReload Cannot restore rcShm (pointer is NULL)" );
      }
    }
    if ( !initBank( i, theRole, dateBanksPatterns[i] ) ) {
      DO_ERROR {
	snprintf( SP(line),
		  "doSmartReload Failed to init bank %d role:%s for %s",
		  i,
		  dbDecodeRole( theRole ),
		  dbDecodeBankPattern( dateBanksPatterns[i] ) );
	ERROR_TO( LOGFILE, line );
      }
      stat = FALSE;
    } else {
      DO_DETAILED {
	snprintf( SP(line),
		  "doSmartReload Host:%s role:%s bank:%d) %s %s %lld initialised OK",
		  
		  getenv( "DATE_HOSTNAME" ),
		  dbDecodeRole( theRole ),
		  i,
		  dbMemTypeNames[ dbBanksDb[ourBanks].banks[i].support ],
		  dbBanksDb[ourBanks].banks[i].name,
		  dbBanksDb[ourBanks].banks[i].size );
	INFO_TO( LOGFILE, line );
      }
    }
  }

  if ( stat ) {
    DO_INFO {
      if ( numReloaded == 0 ) {
	DO_DETAILED {
	  INFO_TO( LOGFILE,
		   "doSmartReload No changes detected" );
	}
      } else {
	snprintf( SP(line),
		  "doSmartReload %d bank%s reloaded OK",
		  numReloaded,
		  numReloaded == 1 ? "" : "s" );
	INFO_TO( LOGFILE, line );
      }
    }
  } else {
    DO_ERROR {
      snprintf( SP(line),
		"doSmartReload %d bank%s reloaded with error",
		numReloaded,
		numReloaded == 1 ? "" : "s" );
      ERROR_TO( LOGFILE, line );
    }
  }
  
  if ( !doInitTheBanks() ) {
    DO_ERROR
      ERROR_TO( LOGFILE,
		"doSmartReload: failed to initialise banks content(s)" );
    stat = FALSE;
  }

  return stat;
} /* End of doSmartReload                                                   */
/* ------------------------------------------------------------------------ */
/* Refresh (if needed) the banks.                                           */
/*                                                                          */
/* This entry assumes that the banks are in a quiescent state. If other     */
/* processes try to use/update the content of the banks, we are not         */
/* responsible for the consequences. We also assume that no other processes */
/* are currently mapped to the banks: if this is not true, we might not     */
/* be able to remove the banks *and* the other processes will end up with   */
/* pointers to areas no longer in use                                       */
/*                                                                          */
/* Return TRUE if OK, FALSE on error.                                       */
/*                                                                          */
int dateRefreshBanks( void ) {
  int b;
  int oldIndex;
  dbSingleBankDescriptor *oldBanks;
  int size;
  int stat;
  int oldNumBanks;
  int smartReload;

  if ( ourBanks == -1 || ourIndex == -1 ) {
    DO_ERROR {
      ERROR_TO( LOGFILE,
		"dateRefreshBanks No banks loaded yet" );
    }
    return FALSE;
  }

  /* Make a copy of the previous BANKS setup for this node */
  size = sizeof( dbSingleBankDescriptor ) * numDateBanks;
  if ( (oldBanks = malloc( size )) == NULL ) {
    DO_ERROR {
      snprintf( SP(line),
		"dateRefreshBanks Failed to malloc for copy of banks\
 numDateBanks:%d size:%d (%d) errno:%d (%s)",
		numDateBanks,
		size,
		(int)sizeof( dbSingleBankDescriptor ),
		errno, strerror( errno ) );
      ERROR_TO( LOGFILE, line );
    }
    return FALSE;
  }
  memcpy( oldBanks, dbBanksDb[ourBanks].banks, size );
  oldIndex = ourIndex;
  oldNumBanks = numDateBanks;
  for ( b = 0; b != numDateBanks; b++ ) {
    if ( (oldBanks[b].name = strdup( dbBanksDb[ourBanks].banks[b].name))
	 == NULL ) {
      DO_ERROR {
	snprintf( SP(line),
		  "dateRefreshBanks Failed to malloc for copy of name\
 numDateBanks:%d size:%d (%d) name:%s errno:%d (%s)",
		  numDateBanks,
		  size,
		  (int)sizeof( dbSingleBankDescriptor ),
		  dbBanksDb[ourBanks].banks[b].name,
		  errno, strerror( errno ) );
	ERROR_TO( LOGFILE, line );
      }
      return FALSE;
    }
  }

  /* Reload the databases */
  if ( (b = loadDbs( TRUE )) == -1 ) {
    DO_ERROR
      ERROR_TO( LOGFILE,
		"dateRefreshBanks Failed to reload configuration DBs" );
    return FALSE;
  }

  /* See if things have radically changed */
  DO_DETAILED {
    snprintf( SP(line),
	      "dateRefreshBanks\
 (ourIndex old:%d new:%d %s)\
 (ourBanks old:%d new:%d %s)\
 (numDateBanks old:%d new:%d %s) ",
	      ourIndex, oldIndex, ourIndex == oldIndex ? "TRUE" : "FALSE",
	      b, ourBanks, b == ourBanks ? "TRUE" : "FALSE",
	      numDateBanks, dbBanksDb[b].numBanks,
	      numDateBanks == dbBanksDb[b].numBanks ? "TRUE" : "FALSE" );
  }

  smartReload =
       ourIndex == oldIndex
    && numDateBanks == dbBanksDb[b].numBanks
    && ourBanks == b;
  if ( smartReload ) {
    int i;

    for ( i = 0; smartReload && (i != numDateBanks); i++ ) {
      dbBankPatternType oldPattern = dateBanksPatterns[i];
      dateBanksPatterns[i] = dbBanksDb[ourBanks].banks[i].pattern;
      loadPattern( &dateBanksPatterns[i] );
      smartReload = (oldPattern == dateBanksPatterns[i] );
      DO_DETAILED {
	snprintf( SP(line),
		  "dateRefreshBanks #:%d oldPattern:%s",
		  i,
		  dbDecodeBankPattern( oldPattern ) );
	snprintf( AP(line), " newPattern:%s", dbDecodeBankPattern( dateBanksPatterns[i] ) );
	snprintf( AP(line), " smartReload:%s ", smartReload ? "TRUE" : "false" );
	INFO_TO( LOGFILE, line );
      }
    }
  }

  if ( smartReload ) {
    /* Thinks look quite similar. We can try to reload only the bits
     * that have actually changed (if any). */
    DO_DETAILED {
      snprintf( AP(line), "Smart reload in progress" );
      INFO_TO( LOGFILE, line );
    }
    stat = doSmartReload( oldBanks );
  } else {
    /* Things are changed a lot: reload everything */
    DO_DETAILED {
      snprintf( AP(line),
		"Smart reload NOT POSSIBLE: full reload in progress" );
      INFO_TO( LOGFILE, line );
    }
    stat = doFullReload();
  }
  for ( b = 0; b != oldNumBanks; b++ )
    free( oldBanks[b].name );
  free( oldBanks );

  DO_DETAILED {
    snprintf( SP(line),
	      "dateRefreshBanks returning %s",
	      stat ? "TRUE" : "false" );
    INFO_TO( LOGFILE, line );
  }
  
  return stat;
} /* End of dateRefreshBanks                                                */
/* ------------------------------------------------------------------------ */
