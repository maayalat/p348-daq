/*      dbInput.c
 *      =========
 *
 * Module to input a definition stream.
 *
 * Revision history:
 *  1.00  19 Nov 01  RD  First release
 */

#define VID "V 1.00"

#define DESCRIPTION "DATE DBs input handler"
#ifdef AIX
static
#endif
char dbInputIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                       """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <sys/param.h>
#include <errno.h>

#include "infoLogger.h"
#include "dateDb.h"
#define DB_INPUT
#include "dbConfig.h"

/* ------------------------------------------------------------------------- */
char *streamName = NULL;	/* Name of the current stream                */
char fileName[PATH_MAX] = {0};  /* Current stream translated                 */
int lineNo = 0;			/* Number of the line currently parsed       */
int inCs = 0;			/* Number of characters read so far          */
char lastLine[5000];		/* Last line parsed                          */

/* --------------------------------------------------------------------------
    Macros for string I/O handling (sized print and sized append)
*/
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)
/* ------------------------------------------------------------------------- */
/* Initialise the input handler, eventually unload previous loaded information

   Returns:
     DB status (DB_LOAD_OK for success)
 */
int dbInitInput() {
  int i;

  for ( i = 0; dbSections[i].keyword != NULL; i++ ) {
    int l;

    for ( l = 0; l != dbSections[i].numLines; l++ ) {
      int t;
      for ( t = 0; dbSections[i].lines[l].tokens[t].value != NULL; t++ ) {
	free(dbSections[i].lines[l].tokens[t].keyword);
	free(dbSections[i].lines[l].tokens[t].value);
      }
      free( dbSections[i].lines[l].tokens );
      free( dbSections[i].lines[l].line );
    }
    free( dbSections[i].lines );
    dbSections[i].lines = NULL;
    dbSections[i].numLines = 0;
  }

  return DB_UNLOAD_OK;
} /* End of dbInitInput */

/* ------------------------------------------------------------------------- */
/* Open the given stream using the appropriate method.

   Input:
     stream  Name of the stream

   Return:
     NULL  error
     else  FILE handle
*/
static FILE *doOpenStream( const char * const stream ) {
  FILE *cmdPipe;
  char cmdString[PATH_MAX+20];
  char dummy[1];

  /* Save the original name for later use */
  free( streamName );
  streamName = strdup( stream );

  /* Echo the name of the stream, therefore translating all environment
     variables eventually part of the name */
  snprintf( cmdString, sizeof(cmdString), "/bin/echo \"%s\"", stream );
  if ( (cmdPipe = popen( cmdString, "r" )) == NULL ) {
    int e = errno;
    ERROR_TO( "db", "doOpenStream/popen failed " );
    perror( "db/doOpenStream/popen failed " );
    errno = e;
    return NULL;
  }
  if ( fgets( fileName, sizeof(fileName), cmdPipe ) == NULL ) {
    int e = errno;
    ERROR_TO( "db", "doOpenStream/fgets(1) on pipe failed " );
    perror( "db/doOpenStream/fgets(1) on pipe failed " );
    pclose( cmdPipe );
    errno = e;
    return NULL;
  }
  if ( fgets( dummy, sizeof(dummy), cmdPipe ) != NULL ) {
    int e = errno;
    ERROR_TO( "db", "openStream/fgets(2) on pipe failed " );
    ERROR_TO( "db", fileName );
    ERROR_TO( "db", dummy );
    perror( "db/doOpenStream/fgets(2) on pipe failed " );
    pclose( cmdPipe );
    errno = e;
    return NULL;
  }
  pclose( cmdPipe );

  /* Remove trailing CR */
  if ( fileName[strlen(fileName)-1] == '\n' )
    fileName[strlen(fileName)-1] = 0;

  /* Open the stream. The actual mechanism depends on the type of stream */
  return fopen( fileName, "r" );
} /* End of doOpenStream */

/* ------------------------------------------------------------------------- */
/* Open the given stream using the appropriate method. This is simply a
   wrapper for the actual openStream, just to repeat the same in case it
   fails due to intermittent errors.

   Input:
     stream  Name of the stream

   Return:
     NULL  error
     else  FILE handle
*/
static FILE *openStream( const char * const stream ) {
  int ctr;
  int e;

  for ( ctr = 0; ctr != 3; ctr++ ) {
    char line[1024];
    FILE *result = doOpenStream( stream );
    if ( result != NULL ) {
      if ( ctr != 0 ) {
	snprintf( SP(line),
		  "db/openStream succeeded retry:%d",
		  ctr );
	fprintf( stderr, line );
	ERROR_TO( "db", line );
      }
      return result;
    }
    e = errno;
    snprintf( SP(line),
	      "db/openStream failed retry:%d errno:%d (%s)",
	      ctr, e, strerror( e ) );
    ERROR_TO( "db", line );
    fprintf( stderr, line );
  }
  ERROR_TO( "db", "db/openStream: too many errors, bailing out" );
  fprintf( stderr, "db/openStream: too many errors, bailing out" );
  errno = e;
  return NULL;
} /* End of openStream */

/* ------------------------------------------------------------------------- */
/* Close the input stream.

   Parameter:
     stream   The input stream

   Returns:
     TRUE    for OK
     FALSE   for error
*/
int closeStream( FILE *stream ) {
  return fclose( stream ) == 0;
} /* End of closeStream */

/* ------------------------------------------------------------------------- */
/* Find out if the given stream is complete.
   
   Parameter:
     stream   The input stream

   Returns:
     TRUE   Stream complete
     FALSE  More data expected
*/
static int endOfStream ( FILE *stream ) {
  /* File: return eof */
  return feof( stream );
} /* End of endOfStream */

/* ------------------------------------------------------------------------- */
/* Get the next line from the input stream.

   Parameter:
     stream   The input stream
     line     Address of buffer
     maxLen   Maximum lenght of line
   
   Return:
     TRUE    All OK
     FALSE   Error(s)
*/
static int getLine( FILE *stream, char *line, int maxLen ) {
  int i;
  int inQuotes;

  lineNo++;
  if ( fgets( line, maxLen, stream ) == NULL ) {
    line[0] = 0;
    return endOfStream( stream );
  }
  inCs += strlen(line);
  if ( strlen(line) == maxLen-1 ) {
    fprintf( stderr,
	     "Stream \"%s\"=\"%s\" line %d too long (max %d chars)\n",
	     streamName,
	     fileName,
	     lineNo,
	     maxLen );
    return FALSE;
  }
  if ( line[strlen(line)-1] == '\n' ) line[strlen(line)-1] = 0;

  /* Remove comments */
  for ( inQuotes = FALSE, i = 0; line[i] != 0; ) {
    if ( line[i] == '#' && !inQuotes ) {
      line[i] = 0;
    } else {
      if ( line[i++] == '"' ) inQuotes = !inQuotes;
    }
  }

  /* Remove trailing spaces and tabs */
  for ( i = strlen(line)-1; i >= 0; i-- ) {
    if ( line[i] == ' ' || line[i] == '\t' ) {
      line[i] = 0;
    } else break;
  }

  /* If the last character is a continuation mark, continue reading */
  if ( line[strlen(line)-1] == '\\' ) {
    line[strlen(line)-1] = 0;
    return getLine( stream, &line[strlen(line)], maxLen - strlen(line) );
  }

  return TRUE;
} /* End of getLine */

/* ------------------------------------------------------------------------- */
/* Load and tokenize the last line on input.

   Parameters:
     startLine   The number of the first source line
     lastLine    The most recent line read from the input stream
     inLine      The inLineStruct to load

   Return:
     TRUE    All OK
     FALSE   Problems, errors
*/
static int loadAndTokenize( const int startLine,
			    const char * const lastLine,
			    struct inLineStruct * inLine ) {
  char *toTokenize;
  char *s, *v, *k;
  int   token;
  char  c;
  
  inLine->lineNo = startLine;
  inLine->tokens = NULL;
  inLine->line   = NULL;

  if ( (inLine->line = strdup(lastLine)) == NULL ) {
    perror( "DB load: malloc for input line failed " );
    return FALSE;
  }

  if ( (toTokenize = strdup(lastLine)) == NULL ) {
    perror( "DB load: malloc for tokenizer failed " );
    return FALSE;
  }
  for ( token = 0, s = toTokenize;; token++ ) {
    if ( (inLine->tokens = realloc( inLine->tokens,
				    sizeof(struct inTokenStruct)*(token+1))) ==
	 NULL ) {
      perror( "DB load: malloc for inLine->tokens failed " );
      return FALSE;
    }
    inLine->tokens[token].keyword = NULL;
    inLine->tokens[token].value = NULL;
    while ( *s == ' ' || *s == '\t' || *s == ',' ) s++;
    if ( *s == 0 ) break;
    if ( *s == '"' || *s == '\'' ) {
      v = s;
      for ( s = s+1; *s != 0 && *s != *v; s++ );
      if ( *s == 0 ) {
	fprintf( stderr, "DB load: unbalanced '%c'\n", *v );
	return FALSE;
      }
      c = *++s;
      *s = 0;
      if ( (inLine->tokens[token].value = strdup(v)) == NULL ) {
	perror( "DB load: malloc for token value (quoted) failed " );
	return FALSE;
      }
      if ( c != 0 ) s++;
    } else {
      char *e = NULL;
      k = s;
      while ( *s != ' ' && *s != '\t' && *s != ',' && *s != 0 ) {
	if ( *s == '=' || *s == ':' ) {
	  if ( e != NULL ) {
	    fprintf( stderr, "DB load: double '%c'?\n", *s );
	    return FALSE;
	  }
	  e = s;
	}
	s++;
      }
      c = *s;
      *s = 0;
      if ( e == NULL ) {
	if ( (inLine->tokens[token].value = strdup(k)) == NULL ) {
	  perror( "DB load: malloc for token value (alone) failed " );
	  return FALSE;
	}
      } else {
	*e = 0;
	if ( *(e+1) == 0 ) {
	  fprintf( stderr, "DB load: = what?\n" );
	  return FALSE;
	}
	if ( (inLine->tokens[token].keyword = strdup(k)) == NULL
	  || (inLine->tokens[token].value = strdup(e+1)) == NULL ) {
	  perror( "DB load: malloc for token key/value failed " );
	  return FALSE;
	}
      }
      if ( c != 0 ) s++;
    }
  }
  free( toTokenize );

  return TRUE;
} /* End of loadAndTokenize */

/* ------------------------------------------------------------------------- */
/* Open and parse the given stream.

   Parameter:
     stream  The input stream name

   Returns:
     DB status (DB_LOAD_OK for success)
*/
static int openAndParse( const char * const stream ) {
  FILE *in;
  int section = -1;

  if ( (in = openStream( stream )) == NULL ) {
    char line[1024];
    
    snprintf( SP(line),
	      "Failed to open input stream \"%s\"=\"%s\": %s (%d)\n",
	      streamName, fileName, strerror( errno ), errno );
    fprintf( stderr, line );
    ERROR_TO( "db", line );
    return DB_LOAD_ERROR;
  }

  do {
    int s;
    int startLine = lineNo+1;
    int n;

    if ( !getLine( in, lastLine, sizeof(lastLine) ) ) return DB_LOAD_ERROR;
    if ( lastLine[0] != 0 ) {
      for ( s = 0; dbSections[s].keyword != NULL; s++ )
	if ( strcmp( dbSections[s].keyword, lastLine ) == 0 )
	  break;
      if ( dbSections[s].keyword != NULL ) {
	section = s;
      } else {
	if ( section == -1 ) {
	  fprintf( stderr,
		   "DB parse error stream \"%s\"=\"%s\" line:%d NO SECTION\n",
		   streamName,
		   fileName,
		   lineNo );
	  return DB_PARSE_ERROR;
	}
	n = dbSections[section].numLines;
	if ( (dbSections[section].lines =
	        realloc( dbSections[section].lines,
			 sizeof(struct inLineStruct) * (n+1) )) == NULL ) {
	  perror( "DB load: realloc for Section lines failed " );
	  return DB_LOAD_ERROR;
	}
	if ( !loadAndTokenize( startLine,
			       lastLine,
			       &dbSections[section].lines[n] ) )
	  return DB_LOAD_ERROR;
	dbSections[section].numLines++;
      }
    }
  } while ( !endOfStream(in) );

  closeStream( in );

  return DB_LOAD_OK;
} /* End of openAndParse */

/* ------------------------------------------------------------------------- */
/* Input the given database.

   Parameter:
     inName   Name of the database to input

   Returns:
     DB status (DB_LOAD_OK for success)
*/   
int dbInput( const char * const inName ) {
  int db;
  char *tokens;
  int status;
  char *p;

  /* Get the index of the database to load */
  for ( db = 0; dbsTable[db].name != NULL; db++ )
    if ( strcmp( dbsTable[db].name, inName ) == 0 ) break;
  if ( dbsTable[db].name == NULL ) {
    fprintf( stderr, "Cannot load \"%s\": not found in dbsTable\n", inName );
    return DB_INTERNAL_ERROR;
  }

  /* Load (if any) the required databases */
  if ( dbsTable[db].requiredDbs != NULL ) {
    if ( ( tokens = strdup( dbsTable[db].requiredDbs )) == NULL ) {
      perror( "Malloc failed " );
      return DB_INTERNAL_ERROR;
    }
    for ( status=DB_LOAD_OK, p=tokens; status == DB_LOAD_OK && p != NULL; ) {
      if ( ( p = strtok( p, " \t" ) ) != NULL ) {
	int toLoad;
	
	for ( toLoad = 0; dbsTable[toLoad].name != NULL; toLoad++ )
	  if ( strcmp( p, dbsTable[toLoad].name ) == 0 ) break;
	if ( dbsTable[toLoad].name == NULL ) {
	  fprintf( stderr, "Database \"%s\" needed to load \"%s\" not found\n",
		   p, inName );
	  return DB_INTERNAL_ERROR;
	} else {
	  status = (dbsTable[toLoad].parser)();
	}
	
	p = NULL;
      }
    }
    free( tokens );
    if ( status != DB_LOAD_OK ) return status;
  }

  /* Load the given sources */
  if ( ( tokens = strdup( dbsTable[db].sources )) == NULL ) {
    perror( "Malloc failed " );
    return DB_INTERNAL_ERROR;
  }
  for ( status = DB_LOAD_OK, p = tokens; status == DB_LOAD_OK && p != NULL; ) {
    if ( ( p = strtok( p, " \t" ) ) != NULL ) {
      status = openAndParse( p );
      p = NULL;
    }
  }
  free( tokens );

  dbSetLastLine( -1, NULL );

  return status;
} /* End of dbInput */

/* ------------------------------------------------------------------------- */
/* Dump the content of dbSections[] to stdout. */
void dumpSections() {
  int s;

  for ( s = 0; dbSections[s].keyword != NULL; s++ ) {
    printf( "Section:%s numLines:%d",
	    dbSections[s].keyword,
	    dbSections[s].numLines );
    if ( dbSections[s].maxId != NULL )
      printf( " maxId:%d", *dbSections[s].maxId );
    printf( " id range:[%d..%d]",
	    dbSections[s].minIdValue,
	    dbSections[s].maxIdValue );
    printf( "\n" );
    if ( dbSections[s].numLines != 0 ) {
      int l;

      for ( l = 0; l != dbSections[s].numLines; l++ ) {
	printf( "%4d=%4d) |%s| ",
		l,
		dbSections[s].lines[l].lineNo,
		dbSections[s].lines[l].line );
	if ( dbSections[s].lines[l].tokens == NULL ) {
	  printf( "No tokens?" );
	} else {
	  int t;
	  printf( "Tokens:" );
	  for ( t = 0; dbSections[s].lines[l].tokens[t].value != NULL; t++ ) {
	    printf( "%s[%s][%s]",
		    t == 0 ? "" : "+",
		    dbSections[s].lines[l].tokens[t].keyword == NULL ?
		      "" : dbSections[s].lines[l].tokens[t].keyword,
		    dbSections[s].lines[l].tokens[t].value );
	  }
	}
	printf( "\n" );
      }
    }
  }
} /* End of dumpSections */

/* ------------------------------------------------------------------------- */
/* Set the last input line

   Parameter:
     line   Address of line (NULL: invalidate the lastLine)
*/
void dbSetLastLine( const int lineNo, const char * const line ) {
  if ( line == NULL )
    lastLine[0] = 0;
  else
    strncpy( lastLine, line, sizeof(lastLine) );
} /* End of dbSetLastLine */

/* ------------------------------------------------------------------------- */
/* Get the last input line

   Returns:
     address of last parsed line
*/
const char * const dbGetLastLine() {
  return lastLine;
} /* End of dbGetLastline */
