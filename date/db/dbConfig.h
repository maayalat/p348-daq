/* ============================================================================
**			       dbConfig.h
**			       ==========
**
** Configuration file for DATE db package. This file contains all configuration
** parameters required by the DATE database internal modules.
*/

#ifndef __db_config_h__
#define __db_config_h__

/* ------------------------------------------------------------------------- */
/* Formats for the various fields */
#define DB_KEYWORD     ">"		/* Prefix for all keywords           */
#define ID_FORMAT      "%ui"		/* Format used to parse a DATE ID    */
#define TOPLEVEL_LABEL "topLevel" 	/* Label for topLevel indicator      */
#define ACTIVE_LABEL   "active"		/* Label for active indicator        */
#define HOSTNAME_LABEL "hostname"       /* Label for hostname indicator      */
#define HLT_ROLE_LABEL "hltRole"        /* Label for HLT role indicator      */

/* ------------------------------------------------------------------------- */
/* The following structure describes all the databases:

   - the name of the database;
   - the parser for the database;
   - the list of required databases;
   - the list of sources for the given database;

   The list of databases must be terminated by a record with all fields NULL.
*/
#ifndef DB_INPUT
extern
#endif
const struct {
  const char  * const name;
        int  (* const parser)();
        int  (* const unloader)();
  const char  * const requiredDbs;
  const char  * const sources;
} dbsTable[]
#ifdef DB_INPUT
  = {
    
  { "roles",
    dbLoadRoles,
    dbUnloadRoles,
    NULL,
    "${DATE_DB_ROLES}"
  },
  
  { "triggers",
    dbLoadTriggers,
    dbUnloadTriggers,
    "roles",
    "${DATE_DB_TRIGGERS}"
  },
  
  { "detectors",
    dbLoadDetectors,
    dbUnloadDetectors,
    "roles",
    "${DATE_DB_DETECTORS}"
  },
  
  { "banks",
    dbLoadBanks,
    dbUnloadBanks,
    "roles",
    "${DATE_DB_BANKS}"
  },
  
  { "eventBuildingControl",
    dbLoadEventBuildingControl,
    dbUnloadEventBuildingControl,
    "roles",
    "${DATE_DB_EVENT_BUILDING_CONTROL}"
  },
  
  { NULL, NULL, NULL, NULL, NULL } /* Terminator: always leave as last */
}
#endif
;
#define DB_TABLE_ROLES                  0
#define DB_TABLE_TRIGGER                (DB_TABLE_ROLES+1)
#define DB_TABLE_DETECTORS              (DB_TABLE_TRIGGER+1)
#define DB_TABLE_BANKS                  (DB_TABLE_DETECTORS+1)
#define DB_TABLE_EVENT_BUILDING_CONTROL (DB_TABLE_BANKS+1)

/* ------------------------------------------------------------------------- */
/* Structures used to transfer a (set of) databases from the global to
   a specific parser
*/
struct inTokenStruct {
  char *keyword;
  char *value;
};

struct inLineStruct {
  int   lineNo;
  char *line;
  struct inTokenStruct *tokens;
};

struct dbSectionStruct {
  const  char         * const keyword;
  const  dbRoleType           role;
         int          * const maxId;
  const  int                  minIdValue;
  const  int                  maxIdValue;
         int                  numLines;
  struct inLineStruct *       lines;
};


#ifndef DB_INPUT
extern
#endif
  struct dbSectionStruct dbSections[]
#ifdef DB_INPUT
  = {
  { DB_KEYWORD "LDC",
    dbRoleLdc,
    &dbMaxLdcId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "GDC",
    dbRoleGdc,
    &dbMaxGdcId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "DETECTORS",
    dbRoleDetector,
    &dbMaxDetectorId,
    EVENT_DETECTOR_ID_MIN,
    EVENT_DETECTOR_ID_MAX,
    0, NULL },

  { DB_KEYWORD "SUBDETECTORS",
    dbRoleSubdetector,
    &dbMaxSubdetectorId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "TRIGGER_HOST",
    dbRoleTriggerHost,
    &dbMaxTriggerHostId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "TRIGGER_MASK",
    dbRoleTriggerMask,
    &dbMaxTriggerMaskId,
    EVENT_TRIGGER_ID_MIN,
    EVENT_TRIGGER_ID_MAX,
    0, NULL },

  { DB_KEYWORD "EDM",
    dbRoleEdmHost,
    &dbMaxEdmHostId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "DDG",
    dbRoleDdg,
    &dbMaxDdgId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "FILTER",
    dbRoleFilter,
    &dbMaxFilterId,
    HOST_ID_MIN,
    HOST_ID_MAX,
    0, NULL },

  { DB_KEYWORD "BANKS",
    dbRoleUnknown,
    NULL,
    0,
    -1,
    0, NULL },

  { DB_KEYWORD "EVENT_BUILDING_CONTROL",
    dbRoleUnknown,
    NULL,
    0,
    -1,
    0, NULL },

  /* -------------------------------------------------- */
  { NULL, dbRoleUndefined, NULL, 0, 0, 0, NULL }
}
#endif
;
#define DB_SECTION_LDC                    0
#define DB_SECTION_GDC                    (DB_SECTION_LDC+1)
#define DB_SECTION_DETECTORS              (DB_SECTION_GDC+1)
#define DB_SECTION_SUBDETECTORS           (DB_SECTION_DETECTORS+1)
#define DB_SECTION_TRIGGER_HOST           (DB_SECTION_SUBDETECTORS+1)
#define DB_SECTION_TRIGGER_MASK           (DB_SECTION_TRIGGER_HOST+1)
#define DB_SECTION_EDM                    (DB_SECTION_TRIGGER_MASK+1)
#define DB_SECTION_DDG                    (DB_SECTION_EDM+1)
#define DB_SECTION_FILTER                 (DB_SECTION_DDG+1)
#define DB_SECTION_BANKS                  (DB_SECTION_FILTER+1)
#define DB_SECTION_EVENT_BUILDING_CONTROL (DB_SECTION_BANKS+1)
#define DB_SECTION_LAST_SECTION           (DB_SECTION_EVENT_BUILDING_CONTROL)

int dbInitInput();		 /* Init structures descarding previous info */
int dbInput( const char * const stream );    /* Input from the given stream  */
void dumpSections();		 /* Dump the content of dbSections[]         */
void dbSetLastLine( const int lineNo, const char * const line );

/* ------------------------------------------------------------------------- */
/* Structure and getter routine to find out if an object of a given role
   belongs to a LDC mask, a GDC mask or to none of the two */
typedef enum {
  inLdcMask,
  inGdcMask,
  notInMask
} superRoleType;

superRoleType dbGetSuperRole( dbRoleType role );

/* ------------------------------------------------------------------------- */
#endif
