/* This program creates the SQL DATE DB from scratch and feed it with the
information stored in ASCII files */
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include "dateDb.h"
#include "dbConfig.h"
#include "dbSql.h"

void print_usage(){
  printf("This program creates DATE SQL configuration database\nWARNING: existing database is lost\nOptions: \n \
   -h  help \n \
   -c  create configuration from ASCII files \n \
   -e  create empty tables only\n");

  return;
}

int main(int argc, char **argv){
   int status,i;
   char msg[1024];
   int create_only=1;
   int option_ok=0;
   char *exec_argv[3];
   char path[1024];
   char *dir;
  
  for (i=1;i<argc;i++) {
    if (!strcmp(argv[i],"-h")) {
      print_usage();
      return 0;
    }
    if (!strcmp(argv[i],"-c")) {
      create_only=0;
      option_ok=1;
    }
    if (!strcmp(argv[i],"-e")) {
      create_only=1;    
      option_ok=1;
    }
  }
  if (!option_ok) {
      print_usage();
      return -1;
  }

  /* Check if DATE SQL configured */
  if (!dbsql_is_configured()) {
    printf ("DATE SQL database not selected\n");
    return -1;
  }


  /* create tables */
  printf("Opening database\n");
  dbsql_open();
  printf("Destroy existing tables\n");
  dbsql_destroy();
  printf("Create new tables\n");
  dbsql_create();



  if (!create_only) {
    printf ("Load configuration from ASCII files\n");
    
    /* disable SQL to load ASCII files parameters */
    setenv("DATE_DB_MYSQL","FALSE",1);

    /* Load configuration */  
    if ( (status = dbLoadRoles()) != DB_LOAD_OK ) {
      snprintf(
        msg,
        sizeof(msg),
        "Error during dbLoadRolesDb:%d \"%s\". System-dependent status ",
        status,
        dbDecodeStatus( status )
      );
      perror( msg );
      exit(1);
    }
    if ( (status = dbLoadBanks()) != DB_LOAD_OK ) {
      snprintf(
        msg,
        sizeof(msg),
        "Error during dbLoadBanksDb:%d \"%s\". System-dependent status ",
        status,
        dbDecodeStatus( status )
      );
      perror( msg );
      exit(1);
    }
    if ( (status = dbLoadDetectors()) != DB_LOAD_OK ) {
      snprintf(
        msg,
        sizeof(msg),
        "Error during dbLoadDetectorsDb:%d \"%s\". System-dependent status ",
        status,
        dbDecodeStatus( status )
      );
      perror( msg );
      exit(1);
    }
    if ( (status = dbLoadTriggers()) != DB_LOAD_OK ) {
      snprintf(
        msg,
        sizeof(msg),
        "Error during dbLoadTriggersDb:%d \"%s\". System-dependent status ",
        status,
        dbDecodeStatus( status )
      );
      perror( msg );
      exit(1);
    }
    if ( (status = dbLoadEventBuildingControl()) != DB_LOAD_OK ) {
      snprintf(
        msg,
        sizeof(msg),
        "Error during dbLoadEventBuildingControlDb:%d \"%s\". System-dependent status ",
        status,
        dbDecodeStatus( status )
      );
      perror( msg );
      exit(1);
    }

    /* re-enable SQL */
    setenv("DATE_DB_MYSQL","TRUE",1);

    dbsql_update_roles();
    dbsql_update_membanks();
    dbsql_update_detectors();
    dbsql_update_triggers();
    dbsql_update_eventBuildingControl();

    /* clear configuration in memory */
    dbUnloadAll();
  }

  /* Close database */
  dbsql_close();
  printf("Database closed\n");

  if (!create_only) {
    /* populate equipment configuration */
    printf("Calling equipmentDump\n");
    exec_argv[0]="equipmentDump";
    exec_argv[1]="createdb";
    exec_argv[2]=NULL;
    dir=getenv("DATE_READLIST_BIN");
    if (dir==NULL) {
      printf("Error, DATE_READLIST_BIN undefined\n");
    } else {
      snprintf(path,sizeof(path),"%s/equipmentDump",dir);
      execv(path,exec_argv);
      printf("Error %d : %s\n",errno,strerror(errno));
    }
  }
  
  return 0;
}
