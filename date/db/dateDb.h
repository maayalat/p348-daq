/* ============================================================================
**			       dateDb.h
**			       ========
**
** DB API for access to DATE database information.
**
** Requires:
**    DATE event.h
**
** Standard conventions:
**  TRUE: executes a block with "if (TRUE)" or "TRUE ? (true):(false)"
**
**  DB_ status/error codes: should NOT be used as booleans, e.g.:
**  "if (DB_LOAD_ERROR)" is NOT correct
**
**  DB_ status errors can be decoded with dbDecodeStatus()
**  PATTERN: a set of bits with any combination of one and zeroes
**  MASK: a pattern with one and only one bit set
**  ID: the number of the bit set in a mask
*/

#ifndef __date_db_h__
#define __date_db_h__

#include <stddef.h>
#include "event.h"

#ifndef TRUE
# define TRUE (0 == 0)
#endif
#ifndef FALSE
# define FALSE (0 == 1)
#endif

/* =============== Common definitions ============= */
#define DB_LOAD_OK          0	/* DB load completed OK          */
#define DB_UNLOAD_OK        0	/* DB unload completed OK        */
#define DB_LOAD_ERROR       1	/* DB could not be opened        */
#define DB_PARSE_ERROR      2	/* DB content invalid            */
#define DB_INTERNAL_ERROR   3	/* Internal error during parsing */
#define DB_BAD_SIZING       4	/* Invalid dynamic sizing        */
#define DB_PAR_ERROR        5	/* Invalid input parameter       */
#define DB_UNKNOWN_ID       6	/* Unknown input ID              */
#define DB_LAST_ERROR       6

/* ========== Bit-handling macros, all based on 32-bits arithmetic ========= */
#define DB_ID_TO_NUM(bit)       ((bit)>>5)
#define DB_ID_TO_BIT(bit)       ((long32)(1<<((bit)&0x1f)))
#define DB_SET_BIT(mask, bit)   \
   (mask)[ DB_ID_TO_NUM(bit) ] |= DB_ID_TO_BIT(bit)
#define DB_FLIP_BIT(mask, bit)  \
   (mask)[ DB_ID_TO_NUM(bit) ] ^= DB_ID_TO_BIT(bit)
#define DB_CLEAR_BIT(mask, bit) \
   (mask)[ DB_ID_TO_NUM(bit) ] &= ~(DB_ID_TO_BIT(bit))
#define DB_TEST_BIT(mask, bit)  \
   (((mask)[ DB_ID_TO_NUM(bit) ] & DB_ID_TO_BIT(bit))!=0)

/* =============== Types and fields ============= */

/* Generic ID: must hold all types of IDs */
typedef int dbIdType;

/* The LDC pattern type */
#define DB_WORDS_IN_LDC_MASK (int)((HOST_ID_MAX+31)/32)
typedef long32 dbLdcPatternType[ DB_WORDS_IN_LDC_MASK ];

/* Undefined event type, used to signal unloaded values or errors */
#define DB_UNDEFINED_EVENT_TYPE (EVENT_TYPE_MIN-1)

/* Base type to describe the role of an item */
typedef enum {
  dbRoleUndefined   = -1,
  dbRoleUnknown     =  0,
  dbRoleGdc         =  1,
  dbRoleLdc         =  2,
  dbRoleEdmHost     =  3,
  dbRoleDetector    =  4,
  dbRoleSubdetector =  5,
  dbRoleTriggerHost =  6,
  dbRoleTriggerMask =  7,
  dbRoleDdg         =  8,
  dbRoleFilter      =  9
} dbRoleType;

typedef enum {
  dbHltRoleUndefined = -1,
  dbHltRoleHltLdc = 0,
  dbHltRoleDetectorLdc = 1
} dbHltRoleType;

/* Structure of a record coming from the Roles database.
 */
typedef struct {
  char         *name;		/* Entity name (unique)               */
  char         *hostname;	/* Hostname (not necessarily unique)  */
  char         *description;	/* Entity description                 */
  dbIdType      id;		/* Entity ID (unique within its role) */
  dbRoleType    role;		/* The role of the entity             */
  dbHltRoleType hltRole;       	/* Role within the HLT data flow      */
  unsigned      topLevel:1;	/* Top level entity flag              */
  unsigned      active:1;	/* Active flag                        */
  dbRoleType    madeOf;		/* What the entity is made of         */
  int           bankDescriptor;	/* The bank descriptor for this role  */
} dbRoleDescriptor;

/* Structure of a record coming from the triggers database.
 * Each entry is made of a trigger ID (supposed to have a matching record
 * in the RC static database) and a pattern of detectors associated with
 * the given trigger.
 */
typedef struct {
  dbIdType                 id;
  eventDetectorPatternType detectorPattern;
} dbTriggerDescriptor;

/* Structure of a record coming from the detectors database.
 * Each entry is made of a detector ID (supposed to have a matching record
 * in the RC static database) plus a pattern of LDCs/subdetectors who compose
 * the given detector.
 */
typedef struct {
  dbIdType          id;
  dbRoleType        role;
  dbLdcPatternType  componentPattern;
} dbDetectorDescriptor;

/* Structure of a record coming from the Event Building control database.
 * Each entry describes an event building rule, where for a given event
 * type and associated pattern, a building rule is given. The pattern
 * controlling the process can be either a detector pattern or a trigger
 * pattern, according to the experimental setup.
 */
typedef struct {
  eventTypeType eventType;	   /* The event type associated to the rule */
  unsigned build : 1;		   /* The build/nobuild flag                */
  unsigned hltDecision : 1;	   /* The HLT decision needed flag          */
  enum {			   /* What pattern is used for the rule     */
    fullBuild,			   /* Type of rule: full build, based on    */
    useDetectorPattern,            /* a DETECTOR or based on a TRIGGER      */
    useTriggerPattern
  } type;
  union {			   /* The pattern used for the rule         */
    eventDetectorPatternType detectorPattern;
    eventTriggerPatternType  triggerPattern;
  } pattern;
} dbEventBuildingRule;

/* Base type to describe the type of shared memory used on an item */
typedef enum {
  dbMemUndefined         = 0,
  dbMemIpc               = 1,
  dbMemBigphys           = 2,
  dbMemHeap              = 3,
  dbMemPhysmem           = 4
#define DB_NUM_MEM_TYPES   5
} dbMemType;

/* The name of each support, also used to parse the BANKS definition DB */
#ifndef DB_IN_DB_BANKS
extern
#endif
const char * const dbMemTypeNames[ DB_NUM_MEM_TYPES ]
#ifdef DB_IN_DB_BANKS
 = {
  "undefined",
  "ipc",
  "bigphys",
  "heap",
  "physmem"
}
#endif
;

/* Base type to define the memory banks */
typedef enum {
  dbBankControl				= 0,
  
  dbBankReadout 			= 1,
    dbBankReadoutReadyFifo 		= 2,
    dbBankReadoutFirstLevelVectors	= 3,
    dbBankReadoutSecondLevelVectors	= 4,
    dbBankReadoutDataPages 		= 5,
    dbBankEdmReadyFifo                  = 6,

  dbBankHltAgent 			= 7,
    dbBankHltReadyFifo 			= 8,
    dbBankHltSecondLevelVectors 	= 9,
    dbBankHltDataPages 			= 10,

  dbBankEventBuilder 			= 11,
    dbBankEventBuilderReadyFifo 	= 12,
    dbBankEventBuilderDataPages 	= 13
#define DB_NUM_BANK_TYPES                 14
} dbBankType;

/* The name of each bank, used to parse the BANKS definition database */
#ifndef DB_IN_DB_BANKS
extern
#endif
const char * const dbBankNames[ DB_NUM_BANK_TYPES ]
#ifdef DB_IN_DB_BANKS
 = {
  "control",
  
  "readout",
    "readoutReadyFifo",
    "readoutFirstLevelVectors",
    "readoutSecondLevelVectors",
    "readoutDataPages",
    "edmReadyFifo",

  "hltAgent",
    "hltReadyFifo",
    "hltSecondLevelVectors",
    "hltDataPages",

  "eventBuilder",
    "eventBuilderReadyFifo",
    "eventBuilderDataPages"
}
#endif
;

/* The components each bank: either zero (no components, the bank is final)
   or non-zero with a bit set for each of the components */
typedef long32 dbBankPatternType;
#ifndef DB_IN_DB_BANKS
extern
#endif
const dbBankPatternType dbBankComponents[ DB_NUM_BANK_TYPES ]
#ifdef DB_IN_DB_BANKS
 = {
    0,
  
    DB_ID_TO_BIT( dbBankReadoutReadyFifo )
  | DB_ID_TO_BIT( dbBankReadoutFirstLevelVectors )
  | DB_ID_TO_BIT( dbBankReadoutSecondLevelVectors )
  | DB_ID_TO_BIT( dbBankReadoutDataPages )
  | DB_ID_TO_BIT( dbBankEdmReadyFifo ),
      0,
      0,
      0,
      0,
      0,
    
    DB_ID_TO_BIT( dbBankHltReadyFifo )
  | DB_ID_TO_BIT( dbBankHltSecondLevelVectors )
  | DB_ID_TO_BIT( dbBankHltDataPages ),
      0,
      0,
      0,
    
    DB_ID_TO_BIT( dbBankEventBuilderReadyFifo )
  | DB_ID_TO_BIT( dbBankEventBuilderDataPages ),
    0,
    0
}
#endif
;

/* The descriptor for a memory bank */
typedef struct {
  dbMemType          support; /* Support for the bank (IPC, BIGPHYS...) */
  char              *name;    /* Name of bank/device/keyword            */
  long64             size;    /* Size of the bank (-1: wild)            */
  dbBankPatternType  pattern; /* What the bank is used for              */
} dbSingleBankDescriptor;

/* The descriptor of the banks used for a role */
typedef struct {
  int                     numBanks;
  dbSingleBankDescriptor *banks;
} dbBankDescriptor;

/* ==================== Static global variables and API ====================

   All DBs are described as pointer to arrays of elements, NULL if database
   not loaded (dbLoad... not called, dbLoad... failed, after dbUnload...).

   Each database has dbSize... + 1 elements, the last element has all fields
   void or invalid.

   Empty databases have exactly one (void) element.

   Unloaded databases have their array pointing to NULL.
*/

extern dbRoleDescriptor        *dbRolesDb;
extern int                      dbSizeRolesDb;

extern dbTriggerDescriptor     *dbTriggersDb;
extern int                      dbSizeTriggersDb;

extern dbDetectorDescriptor    *dbDetectorsDb;
extern int                      dbSizeDetectorsDb;

extern dbEventBuildingRule     *dbEventBuildingControlDb;
extern int                      dbSizeEventBuildingControlDb;

extern dbBankDescriptor        *dbBanksDb;
extern int                      dbSizeBanksDb;

/* The maximum value for the given IDs. These values are given for run-time
   checks & optimization. They are guaranteed to be < then their static
   maximum values. WARNING: to scan all values in a given range, do:

   for ( index = 0; index <= dbMax...Id; index++ )

   A termination test condition such as (index < dbMax...Id) is NOT correct.

   A dbMax...Id == -1 means that no relative entries have been loaded.
*/
extern dbIdType dbMaxLdcId;
extern dbIdType dbMaxGdcId;
extern dbIdType dbMaxTriggerMaskId;
extern dbIdType dbMaxDetectorId;
extern dbIdType dbMaxSubdetectorId;
extern dbIdType dbMaxHltProxyId;
extern dbIdType dbMaxHltProducerId;
extern dbIdType dbMaxHltRootId;
extern dbIdType dbMaxTriggerHostId;
extern dbIdType dbMaxEdmHostId;
extern dbIdType dbMaxDdgId;
extern dbIdType dbMaxFilterId;

/* The API */

int dbOpen(void);       /* Open database (to avoid init time on Load) */
int dbClose(void);      /* Close database */

int dbLoadRoles(void);		      /* Entry to load the database        */
int dbUnloadRoles(void);		      /* Entry to unload the database      */
int dbRolesFind( const char * const name, /* Find given name and role      */
		   const dbRoleType role ); /* (role = unknown: all roles) */
int dbRolesFindNext(void);                /* Find next matching record         */

/* Returns the index of the role entry with the given name in the roles      */
/* database, or -1 if none found. Can use rolesFind and hash table instead   */
int dbRolesFindName(const char *name);

/* Find given role/id in Roles DB.                                         */
/* Returns the index of corresponding entry, or -1 if not found.           */
int dbRolesFindId(const dbIdType id, const dbRoleType role); 
                                   

int dbLoadTriggers(void);		      /* Entry to load the database        */
int dbUnloadTriggers(void);               /* Entry to unload the database      */
int dbGetLdcsInTriggerPattern( const eventTriggerPatternType triggerPattern,
			       dbLdcPatternType ldcPattern );

int dbLoadDetectors(void);		       /* Entry to load the database       */
int dbUnloadDetectors(void);	       /* Entry to unload the database     */
int dbGetLdcsInDetector( const dbIdType detectorId,
			 dbLdcPatternType ldcPattern );
int dbGetLdcsInDetectorPattern( const eventDetectorPatternType detectorPattern,
				dbLdcPatternType ldcPattern );
int dbGetDetectorsInTriggerPattern(
	 const eventTriggerPatternType  triggerPattern,
	 eventDetectorPatternType detectorPattern );

int dbLoadEventBuildingControl(void);      /* Entry to load the EVB control    */
int dbUnloadEventBuildingControl(void);    /* Entry to unload the EVB control  */

int dbLoadBanks(void);		       /* Entry to load the BANKS database */
int dbUnloadBanks(void);		       /* Entry to unload the BANKS DB     */

int dbUnloadAll(void);		       /* Entry to unload all DBs */

/* Decode a DB role, return a descriptive string for the given role        */
const char * const dbDecodeRole( const dbRoleType role );

/* Decode a HLT DB role, return a descriptive string for the given role    */
const char * const dbDecodeHltRole( const dbHltRoleType role );

/* Decode a BANK pattern, return a descriptive string for the given patt.  */
const char * const dbDecodeBankPattern( const dbBankPatternType pattern );

/* Decode the status returned by any of the DB routines                    */
const char * const dbDecodeStatus( const int status );

/* Give the last parsed line - where the last PARSE error was issued       */
const char * const dbGetLastLine(void);

/* Encode a DB role, return dbRoleType value for the given string          */
/* Reverse operation of dbDecodeRole()                                     */
dbRoleType dbEncodeRole( const char * role );

/* Encode a DB HLT role, return dbHltRoleType value for the given string   */
/* Reverse operation of dbDecodeHltRole()                                  */
dbHltRoleType dbEncodeHltRole( const char * role );

/* Returns the dbMemTypeNames index corresponding to a given string        */
dbMemType dbEncodeMemType(const char * memtype);

/* Decode event type to string. */
const char *dbDecodeEventType(const eventTypeType t);

/* Encode event type from string. Reverse operation of dbDecodeEventType(). */
eventTypeType dbEncodeEventType( const char *t );

/* Get the table to find LDC id attached to given DDL id
   Based on the list of active equipments and filtered with the provided LDC mask.    
   Parameters:
      - LDC mask: the mask of active LDCs. DDLs of inactive LDCs are filtered out.
      - table: a pointer to the table is stored in this variable.
               table[DDLid] = id of the corresponding LDC, or LDC_ID_INACTIVE if this DDL is not used.
               If size>0, memory allocated should be released after use with free()
      - size: table size (number of entries) is stored in this variable. this corresponds to max DDL id + 1.
  Returns: number of active DDLs found, 0 or -1 on failure.
*/
int dbGetDDLtoLDCtable(dbLdcPatternType *LDCmask, dbIdType **table, int *size);
#define LDC_ID_INACTIVE  HOST_ID_MAX+1


#endif
