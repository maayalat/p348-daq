/*      dbBanks.c
 *      =========
 *
 * Module to implement DB access to the memory banks control database.
 *
 * Revision history:
 *  1.00  01 Sep 2000  RD      First release
 *  1.01  01 Jul 2004  RD      HLT role handling added
 */

#define VID "V 1.01"

#define DESCRIPTION "DATE memory banks control DB access"
#ifdef AIX
static
#endif
char dbBanksIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                       """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>

#define DB_IN_DB_BANKS
#include "dateDb.h"
#include "dbConfig.h"

#include "dbSql.h"

/* ------------------------------------------------------------------------- */
dbBankDescriptor *dbBanksDb     = NULL;
int               dbSizeBanksDb = 0;

/* ------------------------------------------------------------------------- */
static int dbBanksSyntaxError( const int lineNo, const char * const line ) {
  dbSetLastLine( lineNo, line );
  fprintf( stderr,
	   "DB load: syntax error handling DATE banks configuration\n" );
  return DB_PARSE_ERROR;
} /* End of dbTriggerSyntaxError */

/* ------------------------------------------------------------------------- */
static int allocateNextSingleBank( dbBankDescriptor *n ) {
  n->numBanks++;
  
  if ( (n->banks =
	  realloc( n->banks,
		   sizeof(dbSingleBankDescriptor)*(n->numBanks+1) )) ==
       NULL ) {
    perror( "DB load: cannot allocate dbBanksDb (singleBank) " );
    return DB_INTERNAL_ERROR;
  }
  
  n->banks[ n->numBanks ].support = dbMemUndefined;
  n->banks[ n->numBanks ].name = NULL;
  n->banks[ n->numBanks ].size = 0;
  n->banks[ n->numBanks ].pattern = 0;
  
  return DB_LOAD_OK;
} /* End of allocateNextSingleBank */

/* ------------------------------------------------------------------------- */
static int allocateNextBank() {
  dbSizeBanksDb++;
  
  if ( (dbBanksDb =
	  realloc( dbBanksDb,
		   sizeof(dbBankDescriptor)*dbSizeBanksDb )) == NULL ) {
    perror( "DB load: cannot allocate dbBanksDb " );
    return DB_INTERNAL_ERROR;
  }
  
  dbBanksDb[dbSizeBanksDb-1].numBanks = -1;
  dbBanksDb[dbSizeBanksDb-1].banks = NULL;
  
  return allocateNextSingleBank( &dbBanksDb[dbSizeBanksDb-1] );
} /* End of allocateNextBank */

/* ------------------------------------------------------------------------- */
static inline dbMemType getSupport( const char * const keyword ) {
  int s;

  for ( s = 0; s != DB_NUM_MEM_TYPES; s++ )
    if ( strcasecmp( keyword, dbMemTypeNames[s] ) == 0 )
      return s;

  return dbMemUndefined;
} /* End of getSupport */

/* ------------------------------------------------------------------------- */
static inline int getBankType ( const char * const keyword ) {
  int t;

  for ( t = 0; t != DB_NUM_BANK_TYPES; t++ )
    if ( strcasecmp( keyword, dbBankNames[t] ) == 0 )
      return t;
  
  return -1;
} /* End of getBankType */

/* ------------------------------------------------------------------------- */
static inline long64 getBankSize( char * k ) {
  int l = strlen(k);
  float factor = 1.0;
  float v;
  long64 r;

  if ( l == 1 && k[0] == '*' ) return -1;
  
  if ( toupper(k[l-1]) == 'K' ) factor = 1024;
  if ( toupper(k[l-1]) == 'M' ) factor = 1024 * 1024;
  if ( toupper(k[l-1]) == 'G' ) factor = 1024 * 1024 * 1024;
  if ( toupper(k[l-1]) == 'T' ) factor = 1024 * 1024 * 1024 * 1024.0;

  if ( factor != 1.0 ) k[l-1] = 0;
  if ( sscanf( k, "%f", &v ) != 1 ) {
    return 0;
  }

  r = (long64)(v * factor );
  
  if ( r < 0 ) return 0;
  return r;
} /* End of getBankSize */

/* ------------------------------------------------------------------------- */
static int handleBanks( struct dbSectionStruct *s ) {
  int l;
  int status;

  for ( l = 0; l != s->numLines; l++ ) {
    dbBankDescriptor *n;
    int c;
    int t;
    dbBankPatternType currPattern = 0;
    dbSingleBankDescriptor *curr = NULL;
    
    if ( (status = allocateNextBank()) != DB_LOAD_OK ) return status;
    
    n = &dbBanksDb[dbSizeBanksDb-1];

    /* The first token must be the name of a LDC, EDM, HLT or GDC */
    if ( s->lines[l].tokens[0].keyword != NULL )
      return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    if ( (c = dbRolesFind( s->lines[l].tokens[0].value, 
			   dbRoleUnknown )) == -1 ) {
      fprintf( stderr,
	       "DB load: unknown role \"%s\" in banks configuration\n",
	       s->lines[l].tokens[0].value );
      return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    }
    if ( dbGetSuperRole( dbRolesDb[c].role ) != inLdcMask
      && dbGetSuperRole( dbRolesDb[c].role ) != inGdcMask
      && dbRolesDb[c].role != dbRoleTriggerHost
      && dbRolesDb[c].role != dbRoleDdg
      && dbRolesDb[c].role != dbRoleFilter
      && dbRolesDb[c].role != dbRoleEdmHost ) {
      fprintf( stderr,
	       "DB load: invalid role \"%s\" in banks configuration\n",
	       s->lines[l].tokens[0].value );
      return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    }
    dbRolesDb[c].bankDescriptor = dbSizeBanksDb-1;

    /* Now scan all banks and what they contain */
    for ( t = 1; s->lines[l].tokens[t].value != NULL; t++ ) {
      dbMemType support;
      dbBankType bankType;
      
      if ( s->lines[l].tokens[t].keyword == NULL
	&& (support = getSupport( s->lines[l].tokens[t].value )) !=
	   dbMemUndefined ) {
	
	/* Begin of a bank definition */
	if ( (status = allocateNextSingleBank(n)) != DB_LOAD_OK )
	  return status;

	if ( s->lines[l].tokens[t+1].keyword != NULL
	  || s->lines[l].tokens[t+1].value == NULL ) {
	  fprintf( stderr,
		   "DB load: incomplete definition for bank \"%s\"\
 in banks configuration (expecting bank name)\n",
		   s->lines[l].tokens[t].value );
	  return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}

	if ( s->lines[l].tokens[t+2].keyword != NULL
	  || s->lines[l].tokens[t+2].value == NULL ) {
	  fprintf( stderr,
		   "DB load: incomplete definition for bank \"%s\"\
 in banks configuration (expecting bank size)\n",
		   s->lines[l].tokens[t].value );
	  return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}

	curr = &n->banks[n->numBanks-1];

	curr->support = support;
	curr->name = s->lines[l].tokens[t+1].value;
	s->lines[l].tokens[t+1].value = NULL;

	if ( (curr->size = getBankSize( s->lines[l].tokens[t+2].value )) ==
	     0 ) {
	  fprintf( stderr,
		   "DB load: invalid definition for bank \"%s\"\
 in banks configuration (bank size: \"%s\")\n",
		   s->lines[l].tokens[t].value,
		   s->lines[l].tokens[t+2].value );
	  return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
	
	t += 2;
	
      } else if ( s->lines[l].tokens[t].keyword == NULL
	       && (bankType = getBankType( s->lines[l].tokens[t].value )) !=
		  -1 ) {

	/* Component of a bank */
	if ( curr == NULL ) {
	  fprintf( stderr,
		   "DB load: unexpected keyword \"%s\"\
 in banks configuration (expecting bank definition)\n",
		   s->lines[l].tokens[t].value );
	  return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}

	if ( (currPattern & DB_ID_TO_BIT(bankType)) != 0 ) {
	  fprintf( stderr,
		   "DB load: multiply defined bank \"%s\"\n",
		   s->lines[l].tokens[t].value );
	  return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
	}
	curr->pattern |=
	  (DB_ID_TO_BIT(bankType) | dbBankComponents[bankType]) & ~currPattern;
	currPattern |= DB_ID_TO_BIT(bankType) | dbBankComponents[bankType];
	
	
      } else {
	fprintf( stderr,
		 "DB load: invalid keyword \"%s\" in banks configuration\n",
		 s->lines[l].tokens[t].value );
	return dbBanksSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
    }
  }
  
  if ( (status = allocateNextBank()) != DB_LOAD_OK ) return status;
  dbSizeBanksDb--;

  return DB_LOAD_OK;
} /* End of handleBanks */

/* ------------------------------------------------------------------------- */
static int checkBanks() {
  int b;

  for ( b = 0; b != dbSizeBanksDb; b++ ) {
    int l;

    if ( dbBanksDb[b].numBanks == 0 ) return DB_LOAD_ERROR;
    
    for ( l = 0; l != dbBanksDb[b].numBanks; l++ ) {
      if ( dbBanksDb[b].banks[l].support == dbMemUndefined ) {
	fprintf( stderr,
		 "DB load: internal error for Banks, support undefined\n" );
	return DB_LOAD_ERROR;
      }
      if ( dbBanksDb[b].banks[l].name == NULL ) {
	fprintf( stderr,
		 "DB load: internal error for Banks, name undefined\n" );
	return DB_LOAD_ERROR;
      }
      if ( dbBanksDb[b].banks[l].size == 0 ) {
	fprintf( stderr,
		 "DB load: internal error for Banks, size zero\n" );
	return DB_LOAD_ERROR;
      }
      if ( dbBanksDb[b].banks[l].size == -1 &&
	   dbBanksDb[b].banks[l].pattern != DB_ID_TO_BIT( dbBankControl ) ) {
	fprintf( stderr,
		 "DB load: invalid bank with free (\"*\") size\n" );
	return DB_LOAD_ERROR;
      }
      if ( dbBanksDb[b].banks[l].pattern == 0 ) {
	fprintf( stderr,
		 "DB load: bank without components?\n" );
	return DB_LOAD_ERROR;
      }
    }
    if ( dbBanksDb[b].banks[l].support != dbMemUndefined
      || dbBanksDb[b].banks[l].name != NULL
      || dbBanksDb[b].banks[l].size != 0
	 || dbBanksDb[b].banks[l].pattern != 0 ) {
      fprintf( stderr,
	       "DB load: internal error for Banks, inner terminator\n" );
      return DB_LOAD_ERROR;
    }
  }
  if ( dbBanksDb[b].numBanks != 0 ) {
    fprintf( stderr,
	     "DB load: internal error for Banks,\
 terminator dbBanksDb[%d].numBanks %d != 0\n",
	     b,
	     dbBanksDb[b].numBanks );
    return DB_LOAD_ERROR;
  }
  if ( dbBanksDb[b].banks[0].support != dbMemUndefined
    || dbBanksDb[b].banks[0].name != NULL
    || dbBanksDb[b].banks[0].size != 0
       || dbBanksDb[b].banks[0].pattern != 0 ) {
    fprintf( stderr,
	     "DB load: internal error for Banks, outer terminator\n" );
    return DB_LOAD_ERROR;
  }
  
  return DB_LOAD_OK;
} /* End of checkBanks */

/* ------------------------------------------------------------------------- */
int dbLoadBanks() {
  int status;

  if ( dbBanksDb != NULL ) return DB_LOAD_OK;

  /* Switch for SQL database */
  if (dbsql_is_configured()) {
    if ( dbsql_open()!=0 ) return DB_LOAD_ERROR;
    if ( (status = dbsql_load_membanks()) != DB_LOAD_OK ) return status;
    dbsql_close();
  } else {

  if ( (status = dbInitInput()) != DB_UNLOAD_OK ) return status;
  if ( (status = dbInput( dbsTable[ DB_TABLE_BANKS ].name )) != DB_LOAD_OK )
    return status;
  if ( (status = handleBanks( &dbSections[DB_SECTION_BANKS] )) !=
       DB_LOAD_OK ) {
    dbUnloadBanks();
    return status;
  }
  
  } /* end of Switch for SQL database */

  if ( (status = checkBanks()) != DB_LOAD_OK ) {
    dbUnloadBanks();
    return status;
  }
  return status;
} /* End of dbLoadBanks */

/* ------------------------------------------------------------------------- */
int dbUnloadBanks() {
  int i;

  if ( dbBanksDb != NULL ) {
    for ( i = 0; i != dbSizeBanksDb; i++ ) {
      if ( dbBanksDb[i].banks != NULL ) {
	int j;

	for ( j = 0; j != dbBanksDb[i].numBanks; j++ ) {
	  free( dbBanksDb[i].banks[j].name );
	}
	free( dbBanksDb[i].banks );
      }
    }
    free( dbBanksDb );
    dbBanksDb = NULL;
  }
  dbSizeBanksDb = 0;

  if ( dbRolesDb != NULL ) {
    for ( i = 0; i != dbSizeRolesDb; i++ ) {
      dbRolesDb[i].bankDescriptor = -1;
    }
  }
  
  return DB_LOAD_OK;
} /* End of dbUnloadBanks */

/* ------------------------------------------------------------------------- */
