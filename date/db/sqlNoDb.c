/*      sqlNoDb.c
 *      =======
 *
 * DATE SQL database routines (cf dbSql.h) implemented but empty. To be used
 * when no SQL database support is required.
 *
 * Revision history:
 *  1.00  5 Jul 2004  SC      First release
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "infoLogger.h"
#include "dateDb.h"

/* Routine to check configuration.
   Program exits if a SQL component is configured, because functions not
   implemented. Function returns 0 (NO) if no DATE SQL database is configured.
*/

int dbsql_is_configured(){  
  char *v;
   
  /* SQL DB activated? */
  v=getenv("DATE_DB_MYSQL");
  if (v==NULL) return 0;
  if (strcmp(v,"TRUE")) return 0;

  /* SQL DB activated but DATE compiled without this support => exit */
  infoLog_f("db", LOG_FATAL, "DATE_DB_MYSQL = TRUE but db package not compiled with MYSQL - exiting");
  fprintf(stderr,"DATE_DB_MYSQL = TRUE but db package not compiled with MYSQL - exiting\n");
  exit(-1);

  /* usually, this function would return 1 (YES). */
  return 1;
}



/* Open/Close functions : do nothing */
int dbOpen(){
  return 0;
}
int dbClose(){
  return 0;
}



/* Following functions left empty and return errors. */

int dbsql_open(){  
  return -1;
}
int dbsql_close(){  
  return -1;
}

int dbsql_create(){  
  return -1;
}
int dbsql_destroy(){  
  return -1;
}

int dbsql_source(char * file){
  return -1;
}

int dbsql_load_roles(){  
  return -1;
}
int dbsql_update_roles(){  
  return -1;
}
int dbsql_load_membanks(){  
  return -1;
}
int dbsql_update_membanks(){  
  return -1;
}
int dbsql_load_detectors(){  
  return -1;
}
int dbsql_update_detectors(){  
  return -1;
}
int dbsql_load_triggers(){  
  return -1;
}
int dbsql_update_triggers(){  
  return -1;
}
int dbsql_load_eventBuildingControl(){  
  return -1;
}
int dbsql_update_eventBuildingControl(){  
  return -1;
}
int dbGetDDLtoLDCtable(dbLdcPatternType *LDCmask, dbIdType **table, int *size){
  *table=NULL;
  *size=0;
  return -1;
}
