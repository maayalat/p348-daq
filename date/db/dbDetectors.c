/*      dbDetectors.c
 *      =============
 *
 * Module to implement DB access to the detectors database.
 *
 * Revision history:
 *  1.00  25 Aug 2000  RD      First release
 *  1.01  01 Jul 2004  RD      HLT role added
 */

#define VID "V 1.01"

#define DESCRIPTION "DATE detectors DB access"
#ifdef AIX
static
#endif
char dbDetectorsIdent[]="@(#)""" __FILE__ """: """ DESCRIPTION \
                       """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>

#include "dateDb.h"
#include "dbConfig.h"

#include "dbSql.h"

/* ------------------------------------------------------------------------- */
dbDetectorDescriptor *dbDetectorsDb     = NULL;
int                   dbSizeDetectorsDb = 0;

/* ------------------------------------------------------------------------- */
static int dbDetectorSyntaxError( const int lineNo, const char * const line ) {
  dbSetLastLine( lineNo, line );
  fprintf( stderr,
	   "DB load: syntax error handling DATE detectors configuration\n" );
  return DB_PARSE_ERROR;
} /* End of dbDetectorSyntaxError */

/* ------------------------------------------------------------------------- */
static int allocateNextDetector() {
  dbSizeDetectorsDb++;
  
  if ( (dbDetectorsDb =
	  realloc(dbDetectorsDb,
		  sizeof(dbDetectorDescriptor)*dbSizeDetectorsDb )) == NULL) {
    perror( "DB load: cannot allocate dbDetectorsDb " );
    return DB_INTERNAL_ERROR;
  }
  
  dbDetectorsDb[dbSizeDetectorsDb-1].id = -1;
  dbDetectorsDb[dbSizeDetectorsDb-1].role = dbRoleUndefined;
  memset( dbDetectorsDb[dbSizeDetectorsDb-1].componentPattern,
	  0,
	  sizeof(dbLdcPatternType) );
  
  return DB_LOAD_OK;
} /* End of allocateNextDetector */

/* ------------------------------------------------------------------------- */
static int handleDetectors( struct dbSectionStruct *s,
			    const dbRoleType expectedRole ) {
  int l;
  int status;
  int c;
  int r;

  for ( l = 0; l != s->numLines; l++ ) {
    dbDetectorDescriptor *n;
    int k;

    if ( (status = allocateNextDetector()) != DB_LOAD_OK ) return status;
    
    n = &dbDetectorsDb[dbSizeDetectorsDb-1];
    
    if ( s->lines[l].tokens[0].keyword != NULL )
      return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    if ( (r = dbRolesFind( s->lines[l].tokens[0].value,
			   expectedRole )) == -1 ) {
      fprintf( stderr,
	       "DB load: unknown %s \"%s\" in Detectors configuration\n",
	       dbDecodeRole( expectedRole ),
	       s->lines[l].tokens[0].value );
      return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
    }

    n->id = dbRolesDb[r].id;
    n->role = expectedRole;

    for ( c = 0, k = 1; s->lines[l].tokens[k].value != NULL; k++ ) {
      int d;
      
      if ( s->lines[l].tokens[k].keyword != NULL ) 
	return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      
      d = dbRolesFind( s->lines[l].tokens[k].value, dbRoleUnknown );
      if ( d == -1 ) {
	fprintf( stderr,
		 "DB load: unknown detector element %s\
 for detector %s in Detectors configuration\n",
		 s->lines[l].tokens[k].value,
		 dbRolesDb[r].name );
	return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
      if ( dbRolesDb[d].role != dbRoleLdc
	&& dbRolesDb[d].role != dbRoleSubdetector ) {
	fprintf( stderr,
		 "DB load: illegal detector element %s (%s)\
 for detector %s in Detectors configuration\n",
		 s->lines[l].tokens[k].value,
		 dbDecodeRole( dbRolesDb[d].role ),
		 dbRolesDb[r].name );
	return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }
      
      if ( dbRolesDb[r].madeOf != dbRoleUndefined
	&& dbRolesDb[r].madeOf != dbRolesDb[d].role ) {
	  fprintf( stderr,
		   "DB load: detector \"%s\" first declared made of\
 %s and then made of %s\n",
		   dbRolesDb[r].name,
		   dbDecodeRole( dbRolesDb[r].madeOf ),
		   dbDecodeRole( dbRolesDb[d].role ) );
	  return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
      }

      dbRolesDb[r].madeOf = dbRolesDb[d].role;
      DB_SET_BIT( n->componentPattern, dbRolesDb[d].id );

      c++;
    }
    if ( c == 0 )
      return dbDetectorSyntaxError(s->lines[l].lineNo, s->lines[l].line);
  }
  
  if ( (status = allocateNextDetector()) != DB_LOAD_OK ) return status;
  dbSizeDetectorsDb--;

  return DB_LOAD_OK;
} /* End of handleDetectors */

/* ------------------------------------------------------------------------- */
static int checkDetectors() {
  return DB_LOAD_OK;
} /* End of checkDetectors */

/* ------------------------------------------------------------------------- */
/* Load the detectors database. Return DB_LOAD_OK on success.                */
int dbLoadDetectors() {
  int status;

  if ( dbDetectorsDb != NULL ) return DB_LOAD_OK;
  
  /* Switch for SQL database */
  if (dbsql_is_configured()) {
    if ( dbsql_open()!=0 ) return DB_LOAD_ERROR;
    if ( (status = dbsql_load_detectors()) != DB_LOAD_OK ) return status;
    dbsql_close();
  } else {
  
  if ( (status = dbInitInput()) != DB_UNLOAD_OK ) return status;
  if ( (status = dbInput( dbsTable[DB_TABLE_DETECTORS].name )) != DB_LOAD_OK )
    return status;
  if ( (status = handleDetectors( &dbSections[DB_SECTION_DETECTORS],
				  dbRoleDetector )) !=
       DB_LOAD_OK ) {
    dbUnloadDetectors();
    return status;
  }
  if ( (status = handleDetectors( &dbSections[DB_SECTION_SUBDETECTORS],
				  dbRoleSubdetector )) !=
       DB_LOAD_OK ) {
    dbUnloadDetectors();
    return status;
  }
  
  } /* end of Switch for SQL database */
  
  if ( (status = checkDetectors()) != DB_LOAD_OK ) {
    dbUnloadDetectors();
    return status;
  }
  return status;
} /* End of dbLoadDetectors */

/* ------------------------------------------------------------------------- */
/* Unload the Detectors database */
int dbUnloadDetectors() {
  free( dbDetectorsDb );
  dbDetectorsDb = NULL;
  dbSizeDetectorsDb = 0;

  return DB_UNLOAD_OK;
} /* End of dbUnloadDetectors */

/* ------------------------------------------------------------------------- */
static int getLdcs( dbLdcPatternType pattern, const int r ) {
  int status = DB_LOAD_OK;
  int i;
  int d;

  for ( d = 0; d != dbSizeDetectorsDb; d++ )
    if ( dbRolesDb[r].role == dbDetectorsDb[d].role &&
	 dbRolesDb[r].id == dbDetectorsDb[d].id ) break;
  if ( d == dbSizeDetectorsDb ) return DB_LOAD_OK;

  if ( dbRolesDb[r].madeOf == dbRoleLdc ) {

    for ( i = 0; i != DB_WORDS_IN_LDC_MASK; i++ )
      pattern[i] |= dbDetectorsDb[d].componentPattern[i];
    return status;
  }
  for ( i = 0; i != dbMaxSubdetectorId+1; i++ ) {
    if ( DB_TEST_BIT( dbDetectorsDb[d].componentPattern, i ) ) {
      int rr;

      for ( rr = 0; rr != dbSizeRolesDb; rr++ )
	if ( dbRolesDb[rr].id == i &&
	     dbRolesDb[rr].role == dbRolesDb[r].madeOf ) break;
      if ( rr == dbSizeRolesDb ) return DB_INTERNAL_ERROR;

      if ( (status = getLdcs( pattern, rr )) != DB_LOAD_OK ) return status;
    }
  }
  return status;
} /* End of getLdcs */

/* ------------------------------------------------------------------------- */
int dbGetLdcsInDetector( const dbIdType id,
			 dbLdcPatternType pattern ) {
  int status;
  int r;

  if ( pattern == NULL ) return DB_PAR_ERROR;

  if ( dbDetectorsDb == NULL )
    if ( (status = dbLoadDetectors()) != DB_LOAD_OK ) return status;

  memset( pattern, 0, DB_WORDS_IN_LDC_MASK*4 );

  for ( r = 0; r != dbSizeRolesDb; r++ )
    if ( dbRolesDb[r].role == dbRoleDetector && dbRolesDb[r].id == id )
      break;
  if ( r == dbSizeRolesDb ) return DB_UNKNOWN_ID;

  return getLdcs( pattern, r );
} /* End of dbGetLdcsInDetector */

/* ------------------------------------------------------------------------- */
int dbGetLdcsInDetectorPattern( const eventDetectorPatternType detPat,
				dbLdcPatternType ldcPat ) {
  int status = DB_LOAD_OK;
  int d;

  if ( detPat == NULL || ldcPat == NULL ) return DB_PAR_ERROR;

  if ( !DETECTOR_PATTERN_OK( detPat ) ) return DB_PAR_ERROR;

  if ( dbDetectorsDb == NULL )
    if ( (status = dbLoadDetectors()) != DB_LOAD_OK ) return status;

  memset( ldcPat, 0, DB_WORDS_IN_LDC_MASK*4 );

  if ( DETECTOR_PATTERN_VALID( detPat ) ) {
    for ( d = EVENT_DETECTOR_ID_MIN; d <= EVENT_DETECTOR_ID_MAX; d++ ) {
      if ( TEST_DETECTOR_IN_PATTERN( detPat, d ) ) {
	int r;
	
	for ( r = 0; r != dbSizeRolesDb; r++ ) {
	  if ( dbRolesDb[r].role == dbRoleDetector && dbRolesDb[r].id == d ) {
	    break;
	  }
	}
	if ( r == dbSizeRolesDb ) return DB_UNKNOWN_ID;
    
	if ( (status = getLdcs( ldcPat, r )) != DB_LOAD_OK )
	  return status;
      }
    }
  }
  return status;
} /* End of dbGetLdcsInDetectorPattern */
