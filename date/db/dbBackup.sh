#!/bin/sh

# this script dumps content of DATE configuration database to specified directory
# (only if changes since last backup)

# usage:
# -d : destination directory
# -s : DATE_SITE
# -q : quiet, no output

# S.C. 11 may 06 - File created


DESTDIR=`pwd`
QUIET=0
while getopts 'd:s:q' OPTION; do
 case $OPTION in
  d)
   DESTDIR="${OPTARG}" ;;
  s)
   DATE_SITE="${OPTARG}" 
   export DATE_SITE ;;
  q)
    QUIET=1 ;; 
 esac
done


T=`date +"%Y-%m-%d_%H:%M:%S"`
FILE="${DESTDIR}/${T}.sql"
LASTFILE="$DESTDIR/last.sql"
if [ $QUIET == 0 ]; then 
  echo "Backup of configuration database for DATE_SITE=$DATE_SITE"
fi
if [ -f $FILE ]; then
  echo "Error - backup file $FILE exists"
  exit 1
fi


# do backup
. /date/setup.sh
mysqldump -u $DATE_DB_MYSQL_USER --password=$DATE_DB_MYSQL_PWD -h $DATE_DB_MYSQL_HOST $DATE_DB_MYSQL_DB > $FILE


# keep file only if different
diff $FILE $LASTFILE -q
if [ $? == 0 ]; then
  if [ $QUIET == 0 ]; then 
    echo "No change found since last backup"
  fi
  rm $FILE
else 
  echo "Changes found since last backup - saved in $FILE"
  ln -s -f $FILE $LASTFILE
fi
