/*****************************************************************************/
/* Declaration of access functions for sql-based DATE configuration database */
/*****************************************************************************/

/* History: 
  - 1.00    5 Jul 2004   SC      First release
*/


/* check if the DATE database is configured (env. var.)
  Returns: 1 if configured, 0 if not
*/
int dbsql_is_configured();


/* open/close DATE sql database - necessary before calling following calls
  Return value: 0 on success, -1 on failure.
*/
int dbsql_open();
int dbsql_close();


/*  execute the queries listed in a given file
    returns: 0 on success, -1 on failure
*/
int dbsql_source(char * file);


/* create/destroy DATE configuration tables in connected database
  Return value: 0 on success, -1 on failure.
*/
int dbsql_create();
int dbsql_destroy();


/*
  Functions to read/write configurations to/from memory from/to database :
   - 'update' writes the configuration in memory to database - should be done in
     empty tables only
   - 'load' reads the configuration stored in database and create the associated
     memory structures.
     
  The target databases are: roles, memory banks, detectors, triggers, event
  building rules.

  Return value: 0 on success, -1 on failure.
*/
int dbsql_load_roles();
int dbsql_update_roles();
int dbsql_load_membanks();
int dbsql_update_membanks();
int dbsql_load_detectors();
int dbsql_update_detectors();
int dbsql_load_triggers();
int dbsql_update_triggers();
int dbsql_load_eventBuildingControl();
int dbsql_update_eventBuildingControl();


/* low level functions */
int dbsql_query(const char *query,...);
int dbsql_query_new();
int dbsql_query_append(const char *s,...);
int dbsql_query_exec();

