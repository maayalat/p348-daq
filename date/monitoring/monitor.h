#ifndef __monitor_h__
#define __monitor_h__

/*
 * DATE monitor V 3.xx definitions
 * ===============================
 */

/* The error codes */
#define ERR_BASE                 0x80000000

	/* Entry not yet implemented */
#define MON_ERR_NOT_IMPLEMENTED  (ERR_BASE|0x001)

	/* Monitor internal error */
#define MON_ERR_INTERNAL         (ERR_BASE|0x002)

	/* Too many clients */
#define MON_ERR_TOO_MANY_CLIENTS (ERR_BASE|0x003)

	/* setDataSource not yet called */
#define MON_ERR_NO_DATA_SOURCE   (ERR_BASE|0x004)

	/* Bad event format detected */
#define MON_ERR_BAD_EVENT        (ERR_BASE|0x008)

	/* Event to monitor too big */
#define MON_ERR_EVENT_TOO_BIG    (ERR_BASE|0x010)

	/* Operation interrupted */
#define MON_ERR_INTERRUPTED      (ERR_BASE|0x020)

	/* Cannot connect to host */
#define MON_ERR_NOCONNECT        (ERR_BASE|0x040)

	/* Cannot malloc for event or other structures */
#define MON_ERR_MALLOC           (ERR_BASE|0x080)

	/* Monitor table too big */
#define MON_ERR_TABLE_TOO_BIG    (ERR_BASE|0x100)

	/* Error parsing monitor policy table */
#define MON_ERR_PARSE_ERROR      (ERR_BASE|0x200)

	/* Error parsing data source string */
#define MON_ERR_BAD_DATA_SOURCE  (ERR_BASE|0x400)

	/* Key file not found */
#define MON_ERR_NO_KEY_FILE      (ERR_BASE|0x800)

	/* System-dependent error (see errno) */
#define MON_ERR_SYS_ERROR        (ERR_BASE|0x1000)

	/* The client has been signed out by force */
#define MON_ERR_SIGNED_OUT       (ERR_BASE|0x2000)

	/* Configuration line too complex */
#define MON_ERR_LINE_TOO_COMPLEX (ERR_BASE|0x4000)

	/* Parse error within configuration file */
#define MON_ERR_CONFIG_ERROR     (ERR_BASE|0x8000)

	/* Cleanup of monitor clients/server needed */
#define MON_ERR_CLEANUP_NEEDED   (ERR_BASE|0x10000)

        /* Monitor buffer locked: manual intervention needed */
#define MON_ERR_LOCKFILE         (ERR_BASE|0x20000)

        /* Bad input parameter(s) */
#define MON_ERR_BAD_INPUT        (ERR_BASE|0x40000)

        /* End of input file */
#define MON_ERR_EOF              (ERR_BASE|0x80000)

        /* Illegal operation for this host (offline host) */
#define MON_ERR_OFFLINE_HOST     (ERR_BASE|0x100000)

        /* Operation unsupported for this version of the monitor library */
#define MON_ERR_UNSUPPORTED      (ERR_BASE|0x200000)

        /* A get event call could not transfer all the event data */
#define MON_ERR_EVENT_TRUNCATED  (ERR_BASE|0x400000)

        /* Input data file not found */
#define MON_ERR_NO_SUCH_FILE     (ERR_BASE|0x800000)

/* Monitoring not allowed for this host */
#define MONITOR_NOT_ALLOWED      (ERR_BASE|0x1000000)

#ifndef __CINT__
#ifdef __cplusplus
extern "C" {
#endif
#endif

/* The clients' interface */
int   monitorSetDataSource( char* );            /* 0 => OK, else => error    */
int   monitorDeclareTable( char** );		/* 0 => OK, else => error    */
int   monitorDeclareTableWithAttributes(        /* 0 => OK, else => error    */
				      char** );
int   monitorDeclareMp( char * );               /* 0 => OK, else => error    */
char *monitorDecodeError( int error ); 		/* NULL => error, else => OK */
int   monitorGetEvent( void*, long32 );	        /* 0 => OK, else => error    */
int   monitorGetEventDynamic( void** );		/* 0 => OK, else => error    */
int   monitorSetWait(void);		        /* 0 => OK, else => error    */
int   monitorSetNowait(void);			/* 0 => OK, else => error    */
int   monitorControlWait( int );		/* 0 => OK, else => error    */
int   monitorSetSwap( int, int );               /* O => OK, else => error */
int   monitorFlushEvents(void);			/* 0 => OK, else => error    */
int   monitorLogout(void);			/* 0 => OK, else => error    */

/* The "old" deprecated entries: */
int   SetDataSource( char* ); 			/* 0 => OK, else => error    */
void  SetGetEventInteractive(void);		/* 0 => OK, else => error    */
void  SetGetEventBatch(void);			/* 0 => OK, else => error    */
void  FlushGetEventBuffer(void);		/* 0 => OK, else => error    */
int   getevent( char*, long32 );	        /* >0   => OK (byte count)
						   else => error             */

#ifndef __CINT__
#ifdef __cplusplus
}
#endif
#endif

#endif
