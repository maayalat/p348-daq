/*		mpDaemon.h
 *		==========
 */

#ifndef __mp_daemon_h__
#define __mp_daemon_h__

/* The constants used for exchange of messages/replies: */

   /* The outgoing/incoming commands */
#define SEND_SITE        0	        /* Send DATE_SITE */
#define OPEN_LINK     1234		/* Open monitor link                */
#define FLUSH_EVENTS  1001		/* Flush events from monitor buffer */
#define DECLARE_TABLE 2002		/* Declare monitor policy table     */
#define DECLARE_ID    3003              /* Declare mp ID */

   /* The incoming/outgoing replies */
#define REPLY_FLAG    0x80112280	/* Reply follows */

   /* The first reply, used to select endianness */
#define FIRST_REPLY   0x80da1e00

/* All the entries return 0 for OK, else for error (unless otherwise stated) */
int   monitorSendInt( int theInt );
int   monitorSendString( char *theString );
int   monitorReceiveInt( int *theInt );
char *monitorReceiveString();	       /* Returns: NULL => error, else => OK */
int   monitorNetConnectTo( char *hostName,
			   char *bufferName,
			   monitorRecordPolicy * );
int   monitorNetDisconnect();
int   monitorXmitMp( char * );
int   monitorCleanReservationsNetwork();
int   monitorSendPolicyTable(
		monitorRecordPolicy * );
int   monitorNetNextEvent( int, void*, int );

void  monitorSwapBuffer( void*, int, int, int );

#endif
