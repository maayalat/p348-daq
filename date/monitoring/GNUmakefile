# 			=============================
# 			Package "monitoring" makefile
# 			=============================

ROOTSYS                := /usr
DATE_SYS               := Linux
DATE_ROOT              := ..
DATE_MAKEFILES         := $(DATE_ROOT)/makefiles
DATE_COMMON_DEFS       := $(DATE_ROOT)/commonDefs
DATE_BANKS_MANAGER_DIR := $(DATE_ROOT)/banksManager
DATE_INFOLOGGER_DIR    := $(DATE_ROOT)/infoLogger
DATE_DB                := $(DATE_ROOT)/db
INFO_DIR               := $(DATE_INFOLOGGER_DIR)
BINS_DIR               := ./$(DATE_SYS)

DATE_POINTER           := "uint64_t"

CFLAGS    := -g -DPACKAGE_NAME=\"${package}\" ${DATE_CFLAGS} -D$(DATE_SYS) -Dlong32="int" -Dlong64="long" \
	-I${DATE_COMMON_DEFS} -I${DATE_INFOLOGGER_DIR} -I${DATE_DB}

LDFLAGS   := -lpthread
LOADLIBES := $(subst |,\",${LOADLIBES})

${OBJS_DIR}/%.o : %.c
	${CC} ${CFLAGS} -c $< -o $@

${BINS_DIR}/% : ${OBJS_DIR}/%.o
	${CC} ${LDFLAGS} $^ -o $@ ${LOADLIBES}

${BINS_DIR}/% : %
	cp $< $@

${BINS_DIR}/% : %.${DATE_SYS}
	cp $< $@

${BINS_DIR}/%.tcl : %.${DATE_SYS}.tcl
	cp $< $@

${BINS_DIR}/% : %.${DATE_SYS}
	cp $< $@

INCS         := monitor.h \
	        monitorLinkDef.h \
                monitorInternals.h \
                queues.h \
		attributesHandler.h \
                fileHandling.h \
                monitorParams.h \
                monitorBuffer.h \
                clientInterface.h \
		shellInterface.h \
                clientRegistry.h \
                reservationHandler.h \
                eventHandler.h \
                fileHandler.h \
                mpDaemon.h \
                testLib.h
SRCS         := queuesSupport.c \
		attributesHandler.c \
                fileHandling.c \
                monitorParams.c \
                monitorBuffer.c \
                clientInterface.c \
		fortranInterface.c \
		shellInterface.c \
                clientRegistry.c \
                reservationHandler.c \
                eventHandler.c \
                fileHandler.c \
                mpDaemon.c \
                mpDaemonMain.c \
                testLogging.c \
                scanTest.c \
                validateQueues.c \
                monitorBufferTest.c \
                lockingTest.c \
	 	setDataSourceTest.c \
                producer.c \
                consumer.c \
                simpleConsumer.c \
                changingConsumer.c \
                testConfiguration \
                sigTest.c \
                testLib.c \
                eventDump.c \
                monitorSpy.c \
		monitorClients.c \
                mpDaemon.sh \
		eventDumpFtn.f \
		monitorDict.cxx monitorDict.h \
		installationNotes.txt
OBJS         :=
LIBS         := libmonitor.a libmonitorstdalone.a libproducer.a libmonitor.so
BINS         := mpDaemon \
                validateQueues \
                testLogging \
                scanTest \
                monitorBufferTest \
                lockingTest \
		setDataSourceTest \
                producer \
                consumer \
                simpleConsumer \
                changingConsumer \
                eventDump \
                sigTest \
                monitorSpy \
		monitorClients \
                mpDaemon.sh
JAVA_SRCS    := $(wildcard *.java)
JAVA_BINS    := $(patsubst %.java,%.class,$(JAVA_SRCS))
PRINTABLES   := ${INCS} ${SRCS} ${JAVA_SRCS} GNUmakefile

#include ${DATE_MAKEFILES}/moveComponents
#include ${DATE_MAKEFILES}/compileRules
#include ${DATE_MAKEFILES}/defineTargets

MON_INCS := monitor.h \
            monitorInternals.h \
	    attributesHandler.h \
            queues.h \
            fileHandling.h \
            monitorParams.h \
            monitorBuffer.h \
	    clientInterface.h \
	    shellInterface.h \
            clientRegistry.h \
            reservationHandler.h \
            eventHandler.h \
            fileHandler.h \
            mpDaemon.h \
            ${DATE_COMMON_DEFS}/event.h \
	    ${DATE_BANKS_MANAGER_DIR}/banksManager.h \
            ${INFO_DIR}/infoLogger.h
MON_LIBS := ${BINS_DIR}/libmonitor.a
MON_PRODUCER_LIB := ${BINS_DIR}/libproducer.a

CXXFLAGS := ${CFLAGS}
ifeq (${CC},gcc)
  CFLAGS := ${CFLAGS} -fPIC
  LDFLAGS := ${LDFLAGS} -shared-libgcc
endif

CFLAGS := ${CFLAGS} -g -Wall -fPIC

ifeq ($(shell [ \! -r /usr/local/lib/libshift.a ] && echo 1),)
  SHIFTLIBES := -L/usr/local/lib -lshift
else
 ifeq ($(shell [ \! -r /usr/lib64/libshift.so ] && echo 1),)
  SHIFTLIBES := -L/usr/lib64 -lshift
 else
  SHIFTLIBES :=
  CFLAGS := ${CFLAGS} -DNO_RFIO
 endif
endif
LOADLIBES := ${SHIFTLIBES} -lm
ifeq (${DATE_SYS},SunOS)
  POSIX_LIBS := -lposix4
  LOADLIBES := ${LOADLIBES} ${POSIX_LIBS} -lsocket -lnsl
endif
ifeq (${DATE_SYS},Linux)
  LOADLIBES := ${LOADLIBES} -lpthread -lnsl
endif

CFLAGS := ${CFLAGS} -DdatePointer="$(DATE_POINTER)" -I${DATE_BANKS_MANAGER_DIR} -I${DATE_DB_DIR} -I${INFO_DIR} -I..

${BINS_DIR}/libmonitor.a : \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/queuesSupport.o)      \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/attributesHandler.o)  \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/fileHandling.o)       \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/monitorParams.o)      \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/monitorBuffer.o)      \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/clientInterface.o)    \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/fortranInterface.o)   \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/shellInterface.o)     \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/clientRegistry.o)     \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/reservationHandler.o) \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/eventHandler.o)       \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/fileHandler.o)        \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/mpDaemon.o)           \
    ${BINS_DIR}/libmonitor.a(${BINS_DIR}/libInfoBundle.o)

${BINS_DIR}/libmonitorstdalone.a : \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/queuesSupport.o)      \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/attributesHandler.o)  \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/fileHandling.o)       \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/monitorParams.o)      \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/monitorBuffer.o)      \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/clientInterface.o)    \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/fortranInterface.o)   \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/shellInterface.o)     \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/clientRegistry.o)     \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/reservationHandler.o) \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/eventHandler.o)       \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/fileHandlerStdalone.o)\
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/mpDaemon.o)           \
    ${BINS_DIR}/libmonitorstdalone.a(${BINS_DIR}/libInfoBundle.o)

${BINS_DIR}/libproducer.a : \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/queuesSupport.o)      \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/attributesHandler.o)  \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/fileHandling.o)       \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/monitorParams.o)      \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/monitorBuffer.o)      \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/clientInterfaceProducer.o)    \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/shellInterface.o)     \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/clientRegistry.o)     \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/reservationHandler.o) \
    ${BINS_DIR}/libproducer.a(${BINS_DIR}/eventHandlerProducer.o)

${BINS_DIR}/libmonitor.so : \
    ${BINS_DIR}/queuesSupport.o       \
    ${BINS_DIR}/attributesHandler.o   \
    ${BINS_DIR}/fileHandling.o        \
    ${BINS_DIR}/monitorParams.o       \
    ${BINS_DIR}/monitorBuffer.o       \
    ${BINS_DIR}/clientInterface.o     \
    ${BINS_DIR}/fortranInterface.o    \
    ${BINS_DIR}/shellInterface.o      \
    ${BINS_DIR}/clientRegistry.o      \
    ${BINS_DIR}/reservationHandler.o  \
    ${BINS_DIR}/eventHandler.o        \
    ${BINS_DIR}/fileHandler.o \
    ${BINS_DIR}/mpDaemon.o            \
    ${BINS_DIR}/libInfoBundle.o             \
    ${BINS_DIR}/monitorDict.o
ifeq (${ROOTSYS},)
	@ echo "*** ROOT not available ***"
else
	@ rm -f $@
	${CC} -shared -o $@ -fPIC $^
endif

ifeq (${ROOTSYS},)
${BINS_DIR}/monitorDict.o :
	@ echo "*** ROOT not available ***"
else
${BINS_DIR}/monitorDict.o : monitorDict.cxx
	@ touch $@
	g++ ${CXXFLAGS} -fPIC -I$(ROOTSYS)/include/root -c $^ -o $@

monitorDict.cxx : monitor.h monitorLinkDef.h
	rootcint -f $@ -c $^
endif

# === The monitor internals' modules ===
${BINS_DIR}/queuesSupport.o            : queuesSupport.c      ${MON_INCS}
${BINS_DIR}/attributesHandler.o        : attributesHandler.c  ${MON_INCS}
${BINS_DIR}/fileHandling.o             : fileHandling.c       ${MON_INCS}
${BINS_DIR}/monitorParams.o            : monitorParams.c      ${MON_INCS}
${BINS_DIR}/monitorBuffer.o            : monitorBuffer.c      ${MON_INCS}
${BINS_DIR}/clientInterface.o          : clientInterface.c    ${MON_INCS}
${BINS_DIR}/fortranInterface.o         : fortranInterface.c   ${MON_INCS}
${BINS_DIR}/clientInterfaceProducer.o  : clientInterface.c    ${MON_INCS}
	${CC} ${CFLAGS} -DPRODUCER -c $< -o $@
${BINS_DIR}/shellInterface.o           : shellInterface.c     ${MON_INCS}
${BINS_DIR}/clientRegistry.o           : clientRegistry.c     ${MON_INCS}
${BINS_DIR}/reservationHandler.o       : reservationHandler.c ${MON_INCS}
${BINS_DIR}/eventHandler.o             : eventHandler.c       ${MON_INCS}
${BINS_DIR}/eventHandlerProducer.o     : eventHandler.c       ${MON_INCS}
	${CC} ${CFLAGS} -DPRODUCER -c $< -o $@
${BINS_DIR}/fileHandler.o              : fileHandler.c        ${MON_INCS}
	${CC} ${CFLAGS} -c $< -o $@ -I/usr/local/include
${BINS_DIR}/fileHandlerStdalone.o      : fileHandler.c        ${MON_INCS}
	${CC} ${CFLAGS} -DNO_RFIO -c $< -o $@
${BINS_DIR}/mpDaemon.o                 : mpDaemon.c           ${MON_INCS}
${BINS_DIR}/mpDaemonMain.o             : mpDaemonMain.c       ${MON_INCS}
${BINS_DIR}/libInfoBundle.o                  : ${INFO_DIR}/libInfoBundle.c \
                                         ${INFO_DIR}/infoLogger.h
	${CC} ${CFLAGS} -DLOG_TO_FILE -c $< -o $@

# The mpDaemon
${BINS_DIR}/mpDaemon : ${BINS_DIR}/mpDaemonMain.o ${MON_LIBS}
${BINS_DIR}/mpDaemonMain.o : mpDaemonMain.c ${MON_INCS}

# === The test/validation utilities ===
${BINS_DIR}/validateQueues.o    : validateQueues.c ${MON_INCS}
${BINS_DIR}/validateQueues      : ${BINS_DIR}/validateQueues.o ${MON_LIBS}
${BINS_DIR}/testLogging.o       : testLogging.c ${MON_INCS}
${BINS_DIR}/testLogging         : ${BINS_DIR}/testLogging.o ${MON_LIBS}
${BINS_DIR}/scanTest.o          : scanTest.c ${MON_INCS}
${BINS_DIR}/scanTest            : ${BINS_DIR}/scanTest.o ${MON_LIBS}
${BINS_DIR}/monitorBufferTest.o : monitorBufferTest.c ${MON_INCS}
${BINS_DIR}/monitorBufferTest   : ${BINS_DIR}/monitorBufferTest.o ${MON_LIBS}
${BINS_DIR}/lockingTest         : ${BINS_DIR}/lockingTest.o ${MON_LIBS}
${BINS_DIR}/setDataSourceTest   : ${BINS_DIR}/setDataSourceTest.o ${MON_LIBS}
${BINS_DIR}/testLib.o           : testLib.c ${MON_INCS} testLib.h
${BINS_DIR}/producer            : ${BINS_DIR}/producer.o \
                                  ${BINS_DIR}/testLib.o \
                                  ${MON_PRODUCER_LIB} \
				  ${DATE_BANKS_MANAGER_BIN}/libBanksManager.a \
				  ${DATE_DB_BIN}/libDb.a \
				  ${DATE_BM_BIN}/libDateBufferManager.a \
				  ${DATE_SIMPLEFIFO_BIN}/libFifo.a \
				  ${DATE_INFOLOGGER_DIR}/${DATE_SYS}/libInfo.a
	${CC} -o ${BINS_DIR}/producer ${BINS_DIR}/producer.o ${BINS_DIR}/testLib.o -L${DATE_SYS} -lproducer ${DATE_BANKS_MANAGER_BIN}/libBanksManager.a ${DATE_DB_BIN}/libDb.a ${DATE_BM_BIN}/libDateBufferManager.a ${DATE_SIMPLEFIFO_BIN}/libFifo.a ${POSIX_LIBS} ${DATE_LOADLIBES} ${DATE_SYSLOADLIBES} -lm
${BINS_DIR}/producer.o          : producer.c monitor.h ${MON_INCS}
${BINS_DIR}/consumer            : ${BINS_DIR}/consumer.o \
                                  ${BINS_DIR}/testLib.o ${MON_LIBS}
${BINS_DIR}/consumer.o          : consumer.c monitor.h ${MON_INCS}
${BINS_DIR}/simpleConsumer      : ${BINS_DIR}/simpleConsumer.o \
                                  ${BINS_DIR}/testLib.o ${MON_LIBS}
${BINS_DIR}/simpleConsumer.o    : simpleConsumer.c monitor.h ${MON_INCS}
${BINS_DIR}/changingConsumer    : ${BINS_DIR}/changingConsumer.o \
                                  ${BINS_DIR}/testLib.o ${MON_LIBS}
${BINS_DIR}/changingConsumer.o  : changingConsumer.c monitor.h ${MON_INCS}
${BINS_DIR}/sigTest             : ${BINS_DIR}/sigTest.o \
                                  ${BINS_DIR}/testLib.o ${MON_LIBS}
${BINS_DIR}/sigTest.o           : sigTest.c monitor.h ${MON_INCS}
${BINS_DIR}/monitorSpy          : ${BINS_DIR}/monitorSpy.o ${MON_LIBS}
${BINS_DIR}/monitorSpy.o        : monitorSpy.c ${MON_INCS}
${BINS_DIR}/monitorClients      : ${BINS_DIR}/monitorClients.o ${MON_LIBS}
${BINS_DIR}/monitorClients.o    : monitorClients.c ${MON_INCS}
${BINS_DIR}/eventDump           : ${BINS_DIR}/eventDump.o ${MON_LIBS}
${BINS_DIR}/eventDump.o         : eventDump.c ${MON_INCS}
${BINS_DIR}/eventDumpFtn.o      : eventDumpFtn.f
	${FTN} -c $< -o $@
${BINS_DIR}/eventDumpFtn        : ${BINS_DIR}/eventDumpFtn.o ${MON_LIBS}
	${FTN} -o $@ $< ${MON_LIBS} -L/usr/local/lib ${SHIFTLIBES} ${FORTRAN_LOADLIBES} ${POSIX_LIBS}

# ===== JAVA classes and modules ===

%.class : %.java
	javac -g $<

java : monitor.class ${JAVA_BINS} ${BINS_DIR}/libmonitorJNI.so
${BINS_DIR}/libmonitorJNI.so : ${BINS_DIR}/monitorJNI.o monitorJNI.exp
	@ rm -f $@
ifeq (${DATE_SYS},AIX)
	ld -o $@ $< \
	   -bnoentry -bM:SRE -bE:monitorJNI.exp \
	   -L${BINS_DIR} -lmonitor -L/usr/local/lib ${SHIFTLIBES} \
	   -blibpath:/lib:/usr/lib -lc_r
endif
ifeq (${DATE_SYS},SunOS)
	${CC} -G -o $@ $< \
	  -L${BINS_DIR} -lmonitor -L/usr/local/lib ${SHIFTLIBES} -lsocket -lnsl -lc
endif

monitorJNI.exp : monitorJNI.h
	@ rm -f $@
	@ grep JNIEXPORT $< | \
	  while read a b c d; do echo $$d >> $@; done;

monitorJNI.h : monitor.java monitor.class
	javah -jni -o $@ monitor
	@ touch monitorJNI.h

JAVA_HOME       := ${DATE_ROOT}/java/${DATE_SYS}/currentRelease
JNI_INCLUDE     := ${JAVA_HOME}/include
ifeq (${DATE_SYS},AIX)
  JNI_INCLUDE_SYS := ${JAVA_HOME}/include/aix
endif
ifeq (${DATE_SYS},SunOS)
  JNI_INCLUDE_SYS := ${JAVA_HOME}/include/solaris
endif

${BINS_DIR}/monitorJNI.o : monitorJNI.c
	${CC} ${CFLAGS} -c -I. -I${JNI_INCLUDE}  -I${JNI_INCLUDE_SYS} -o $@ $<
