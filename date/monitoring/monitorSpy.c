/* monitorSpy.c
 * ============
 *
 * Utility to spy the content of a local monitor buffer
 *
 */

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

#include "monitorInternals.h"

key_t  key;
extern int monitorSharedMemoryId;
char   keyFile[ 1024 ];
volatile int error = FALSE;

void sigHandler( int sigNum ) {
  error = TRUE;
  signal( SIGSEGV, sigHandler );
  fflush( stdout );
  fflush( stderr );
  printf( "\nSEGV catched: aborting\n" );
  exit( 1 );
}

void initVars( int argc, char **argv ) {
  initLogLevel();
  
  if ( argc == 1 ) {
    strcpy( keyFile, getenv( "DATE_SITE" ) );
    strcpy( &keyFile[ strlen( keyFile ) ], "/" );
    strcpy( &keyFile[ strlen( keyFile ) ], getenv( "DATE_HOSTNAME" ) );
    strcpy( &keyFile[ strlen( keyFile ) ], "/monitoring.config" );
  } else {
    strcpy( keyFile, argv[1] );
  }
  signal( SIGSEGV, sigHandler );
}

#define PROTECTION_PUBLIC \
   ( S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH )

extern void monitorInitMonitorBufferPointers();
extern int  monitorInitSemaphores( key_t );

void initPtrs( char *keyFile ) {
  int status;
  
  if ( (key = ftok( keyFile, 1 )) == (key_t)(-1) ) {
    perror( "Cannot get key to key file " );
    fprintf( stderr, "Key file: \"%s\"\n", keyFile );
    exit( 1 );
  }
  if ( (monitorSharedMemoryId = shmget( key, 0, PROTECTION_PUBLIC )) == -1 ) {
    perror( "Cannot get shared memory segment " );
    exit( 1 );
  }
  if ( (monitorBuffer = shmat( monitorSharedMemoryId, 0, SHM_R|SHM_W )) ==
       (void *)(-1) ) {
    perror( "Cannot attach to shared memory segment " );
    exit( 1 );
  }
  if ( ( status = monitorParseConfigFile( keyFile ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot parse monitoring configuration file. Status: %d\n",
	     status );
    exit( 1 );
  }
  monitorInitMonitorBufferPointers();
  if ( monitorInitSemaphores( key ) != 0 ) {
    perror( "Cannot initialise control semaphores " );
    exit( 1 );
  }
}

void separator() {
  printf( "--------------------------------------------------------------------------------\n" );
}

char *indent() { return "                                 "; }

extern int monitorGetMonitorBufferDescriptor();
extern struct shmid_ds monitorBufferDescriptor;


char *dumpEventType( eventTypeType eventType ) {
  static char msg[ 1024 ];
  
  switch ( eventType ) {
    case START_OF_RUN :       return "SOR";
    case END_OF_RUN :         return "EOR";
    case START_OF_RUN_FILES : return "SORF";
    case END_OF_RUN_FILES :   return "EORF";
    case START_OF_BURST :     return "SOB";
    case END_OF_BURST :       return "EOB";
    case PHYSICS_EVENT :      return "PHYSICS";
    case CALIBRATION_EVENT :  return "CALIBRATION";
    case START_OF_DATA :      return "SOD";
    case END_OF_DATA :        return "EOD";
    case SYSTEM_SOFTWARE_TRIGGER_EVENT :
                              return "SST";
    case DETECTOR_SOFTWARE_TRIGGER_EVENT :
                              return "DST";
    case EVENT_FORMAT_ERROR : return "FORMATERROR";
  }
  snprintf( SP(msg), "UNKNOWN x%08x", eventType );
  return msg;
}


void dumpBufferSize() {
  printf( "mbMonitorBufferSize: 0x%08lx  %12d bytes\n",
	  (long)mbMonitorBufferSize, *mbMonitorBufferSize );
  if ( *mbMonitorBufferSize != monitorBufferDescriptor.shm_segsz )
    printf( "   --- WARNING --- Mismatch detected --- WARNING ---\n" );
  separator();
}


void dumpEventRecord( int ix ) {
  struct queuePtr *ptr;
  int ctr;
  int total;
  monitorEventDescriptor *d = EVENT( ix );
  printf( "   %3d) event:%d @%lx type:%s, ",
	  ix, d->eventNumber, (long)d, dumpEventType( d->eventType ) );
  if ( d->monitored )
    printf( "monitored, " );
  else
    printf( "unmonitored, " );
  if ( d->numMust != 0 )
    printf( "MUST:%d, ", d->numMust );
  if ( d->numYes != 0 )
    printf( "SHOULD:%d, ", d->numYes );
  if ( d->numMust + d->numYes == 0 )
    printf( " --- WARNING --- NO DESTINATIONS --- WARNING --- " );
  printf( "BirthTime:%s", ctime( &(d->birthTime) ) );
  
  printf( "        data:0x%08x -> ", (long32)(d->eventData ));
  fflush( stdout );
  ctr = 0;
  total = 0;
  ptr = ADD(mbFreeEvents, d->eventData);
  do {
#define LIMIT 4
    if ( ctr < LIMIT || ptr->next == 0 ) {
      printf( "(%d) %d ", ctr+1, ptr->size );
      fflush( stdout );
    } else {
      if ( ctr == LIMIT ) printf( "..." ); fflush( stdout );
    }
    ctr++;
    total += ptr->size;
    if ( ptr->next != 0 )
      ptr = ADD(ptr, ptr->next);
    else
      ptr = NULL;
  } while ( ptr != NULL && error != TRUE );
  printf( "\n        Total: %d bytes in %d chunk(s)\n", total, ctr );
  fflush( stdout );
}


void dumpParametersDatabase() {
  printf( "\
mbMaxClients:        0x%08lx  %12d clients max\n\
mbMaxEvents:         0x%08lx  %12d events max\n\
mbEventSize:         0x%08lx  %12d bytes/event\n\
mbEventBufferSize:   0x%08lx  %12d bytes event data\n",
	  (long)mbMaxClients, *mbMaxClients,
	  (long)mbMaxEvents, *mbMaxEvents,
	  (long)mbEventSize, *mbEventSize,
	  (long)mbEventBufferSize, *mbEventBufferSize );
  separator();
  
  printf( "\
mbNumSignIn:         0x%08lx  %12d clients\n\
mbNumSignOut:        0x%08lx  %12d clients\n\
mbForcedExits:       0x%08lx  %12d clients\n\
mbTooManyClients:    0x%08lx  %12d clients\n\
mbEventsInjected:    0x%08lx  %12d events\n\
mbBytesInjected:     0x%08lx  ",
	  (long)mbNumSignIn, *mbNumSignIn,
	  (long)mbNumSignOut, *mbNumSignOut,
	  (long)mbForcedExits, *mbForcedExits,
	  (long)mbTooManyClients, *mbTooManyClients,
	  (long)mbEventsInjected, *mbEventsInjected,
	  (long)mbBytesInjected );
  if ( mbBytesInjected[0] != 0 )
    printf( "%12d*10^9 + ", mbBytesInjected[0] );
  printf( "%12d bytes\n", mbBytesInjected[1] );
  separator();
}


void dumpEventsDatabase() {
  printf( "monitorEvents:       0x%08lx\n",
	  (long)monitorEvents );
  printf( "mbOldestEvent:       0x%08lx  %12d (index)\n\
mbFirstFreeEvent:    0x%08lx  %12d (index)\n",
	  (long)mbOldestEvent, *mbOldestEvent,
	  (long)mbFirstFreeEvent, *mbFirstFreeEvent );
  printf( "mbCurrentEventNumber:0x%08lx  %12d (sequential number)\n",
	  (long)mbCurrentEventNumber, *mbCurrentEventNumber );
  if ( *mbOldestEvent < 0 ) {
    if ( *mbFirstFreeEvent >= 0 ) {
      printf( "   --- WARNING --- *mbFirstFreeEvent >= 0 --- WARNING ---\n" );
    }
  } else {
    int ix = *mbOldestEvent;
    int ct = 0;
    do {
      int newIx;
      
      newIx = ix+1 == *mbMaxEvents ? 0 : ix+1;
      if ( ( ct < 4 ) || ( ct == 4 && newIx == *mbFirstFreeEvent ) ) {
	dumpEventRecord( ix );
      } else {
	if ( ct == 4 ) printf( "   .........\n" );
      }
      ix = newIx;
    } while ( ix != *mbFirstFreeEvent );
  }
  separator();
}


char *dumpMonitorType( monitorType type ) {
  switch( type ) {
  case yes: return "monitor";
  case all: return "100%";
  case no:  return "ignore";
  default:  return "--- WARNING --- UNKNOWN --- WARNING ---";
  }
}


void dumpClientsDatabase() {
  int ix;
  
  printf( "monitorClients:      0x%08lx  %12d clients [ 0 .. %d ]\n",
	  (long)monitorClients,
	  *mbMaxClients, *mbMaxClients - 1 );
  printf( "mbNumClients:        0x%08lx  %12d client(s), %ld process(es) attached\n",
	  (long)mbNumClients,
	  *mbNumClients,
	  (long int)monitorBufferDescriptor.shm_nattch );

  ix = 0;
  do {
    if ( CLIENT( ix )->inUse ) {
      int ev;
      
      printf( "   %3d@%lx..%lx: ",
	      ix,
	      (long)CLIENT( ix ), (long)CLIENT((ix+1)-1) );
      printf( "PID:%ld ", (long int)CLIENT( ix )->pid );
      printf( "reg#:%d ", CLIENT( ix )->registrationNumber );
      printf( "mp:\"%s\" ", CLIENT( ix )->mpId );
      printf( "lastEvent:%d ", CLIENT( ix )->lastEventNumber );
      printf( "events:%d ", CLIENT( ix )->eventsRead );
      printf( "bytes:" );
      if ( CLIENT( ix )->bytesRead[0] != 0 ) {
	printf( "%d*10^9 + ", CLIENT( ix )->bytesRead[0] );
      }
      printf( "%d ", CLIENT( ix )->bytesRead[1] );
      if ( CLIENT( ix )->waitingForNewEvents ) printf( "WAITING " );
      printf( "\n      Monitor policy:\n" );
      for ( ev = 0; ev <= EVENT_TYPE_MAX-EVENT_TYPE_MIN; ev++ ) {
	printf( "         %-12s:%s ",
		dumpEventType( (eventTypeType)ev+EVENT_TYPE_MIN ),
		dumpMonitorType( CLIENT( ix )->monitorPolicy[ ev ] ) );
	monitorPrintAttributesMask( &(CLIENT( ix )->monitorAttributes[ ev ]),
				    stdout  );
	printf( "\n" );
      }
      ix++;
    } else {
      int last = ix;
      while ( last != *mbMaxClients && !CLIENT( last )->inUse ) last++;
      if ( ix == last || ix == last-1 )
	printf( "   %3d: ", ix );
      else
	printf( "   %3d .. %d: ", ix, last-1 );
      printf( "unused\n" );
      ix = last;
    }
  } while ( ix != *mbMaxClients );
  separator();
}


void dumpEventsBuffer() {
  int i;
  struct queuePtr *ptr;
  
  printf( "mbFreeEvents:        0x%08lx", (long)mbFreeEvents );
  if ( checkQueue( mbFreeEvents ) != 0 ) printf( " !CORRUPTED!" );
  printf( "," );
  i = getNumOfFragments( mbFreeEvents );
  printf( " %d frag%s,", i, i == 1 ? "" : "s" );
  printf( " size: %d tot,", mbFreeEvents->size );
  printf( " %d avg\n", (int)getAverageFragmentSize( mbFreeEvents ) );

  printf( "  Free list:\n" );
  ptr = ADD(mbFreeEvents, mbFreeEvents->next);
  i = 0;
  while ( ptr != mbFreeEvents ) {
    if ( i < 4 || ADD(ptr, ptr->next) == mbFreeEvents ) {
      int j = 0;
      int *d = (int*)((long)ptr + QUEUE_HEAD_LEN);
      printf( "    0x%08lx (%d, 0x%08x) ", (long)ptr, ptr->size, ptr->size );
      for ( j = 0; j != 4; j++ ) {
	printf( " 0x%08x", *d );
	d++;
      }
      printf( "\n" );
    }
    ptr = ADD(ptr, ptr->next);
    i++;
    if ( i == 4 && ptr != mbFreeEvents && ADD(ptr, ptr->next) != mbFreeEvents )
      printf( "     ...\n" );
  }
  printf( "\n" );

  if ( checkQueue( mbFreeEvents ) != 0 )
    printf( "   --- WARNING --- Free Events list does NOT validate --- WARNING ---\n" );
  
  printf( "monitorEventsData:   0x%08lx\n",
	  (long)monitorEventsData );
  separator();
}


void dumpBuffer() {
#ifdef LOCK_BUFFER
  if ( monitorLockBuffer() != 0 ) {
    printf( "Cannot lock monitor buffer...\n" );
    return;
  }
#endif
  if ( monitorGetMonitorBufferDescriptor() != 0 ) {
    printf( "Cannot get buffer descriptor...\n" );
  }
  separator();
  dumpBufferSize();
  dumpParametersDatabase();
  dumpEventsDatabase();
  dumpClientsDatabase();
  dumpEventsBuffer();
#ifdef LOCK_BUFFER
  if ( monitorUnlockBuffer() ) {
    printf( "Cannot unlock monitor buffer...\n" );
  }
#endif
}

int usage( int argc, char **argv ) {
  fprintf( stderr, "Usage: %s keyFile\n", argv[0] );
  return 1;
} /* End of usage */

int main( int argc, char **argv ) {
  if ( argc != 1 && argc != 2 ) return usage( argc, argv );

  initVars( argc, argv );
  initPtrs( keyFile );
  dumpBuffer();
  return 0;
} /* End of main */
