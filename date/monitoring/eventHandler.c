/* Monitor V 3.xx events handler module
 * ====================================
 *
 * This is the module responsible for the handling of the events' data.
 *
 * Module history:
 *  1.00 29-Jul-98 RD   Created
 *  1.01 24-Aug-98 RD   First public release
 *  1.02 19-Nov-98 RD   Counters for events & bytes added
 *  1.03  2-Jun-99 RD   Make space feature added
 *  1.04  4-Dec-00 RD   Adapted to DATE 4.x
 *  1.05  8-Nov-04 RD   Bug with copy of events payload fixed & checks added
 */
#define VID "1.05"

#include <stdio.h>
#include <stdlib.h>

#include "monitorInternals.h"
#include "banksManager.h"

#define DESCRIPTION "DATE V3 monitoring events data handler"
#ifdef AIX
static
#endif
char eventHandlerIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                           """ """ VID """ """ \
                           """ compiled """ __DATE__ """ """ __TIME__;


#ifdef NOTDEFD
/* Entry to check the status of the free list. Use only when
 * nothing else is available. */
#define NEXT_EVENT( index ) ((index)+1 == (*mbMaxEvents) ? 0 : (index)+1)
void checkFreeList( monitorEventDescriptor *ed ) {
  struct queuePtr *ptr;

  int limit = (int)ADD(mbFreeEvents, (*mbEventBufferSize)+QUEUE_HEAD_LEN);

  assert ( monitorNumLocks > 0 );

  for( ptr = ADD(mbFreeEvents, QUEUE_HEAD_LEN); (int)ptr < limit; ) {
    struct queuePtr *p;
    int e;

    for( p = ADD(mbFreeEvents, mbFreeEvents->next);
	 p != mbFreeEvents;
	 p = ADD(p, p->next ) )
      if ( p == ptr ) goto next;

    if ( *mbOldestEvent >= 0 ) {
      e = *mbOldestEvent;
      do {
	p = ADD(EVENT(e)->eventData,mbFreeEvents);
	while ( p != NULL ) {
	  if ( ptr == p ) goto next;
	  p = p->next == 0 ? NULL : ADD(p, p->next);
	}
	e = NEXT_EVENT( e );
      } while ( e != *mbFirstFreeEvent );
    }
    if ( ed != NULL ) {
      for ( p = ADD(ed->eventData,mbFreeEvents); p != NULL; ) {
	if ( ptr == p ) goto next;
	p = p->next == 0 ? NULL : ADD(p, p->next );
      }
    }
    printf( "Lost event @ 0x%08x\n", (int)ptr );
    assert( 0 == 1 );
next:
    ptr = ADD( ptr, ptr->size+QUEUE_HEAD_LEN );
  }
} /* End of checkFreeList */
#endif


#ifdef PRODUCER
/* Entries to handle event store (needed only for event producers) */

/* Find enough free space to store the new event.
 *
 * Parameter:
 *   evSize	The needed buffer space
 *   mustStore  TRUE if this event needs to be stored ("must" event)
 *
 * Return:
 *    0   => OK
 *   -1   => no free space
 *   else => error
 */
int monitorFindFreeSpace( int evSize, int mustStore ) {
  while ( mbFreeEvents->size < evSize ) {
    int status = monitorFreeOne( mustStore );
    if ( status == -1 ) {
      if ( !mustStore ) {
	return -1;
      }
      if ( monitorWaitForFreeSpace() != 0 ) return -2;
    } else if ( status != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorFindFreeSpace: monitorFreeOne failed" );
      return status;
    }
  }
  return 0;
} /* End of monitorFindFreeSpace */

/* Our I/O vector, used for output of events */
int ourIovSize = 0;
int currIovSize;
struct iovec *ourIov = NULL;
struct eventHeaderStruct ourHeader;

/* Routine to scan through an equipment tree and build the relative
   I/O vector */
static void buildEquipmentIov( struct eventVectorStruct *vector ) {
  if ( vector->eventVectorSize == 0 ) return;
  if ( vector->eventVectorPointsToVector ) {
    int i;
    struct eventVectorStruct *ptr = BO2V( vector->eventVectorBankId,
					  vector->eventVectorStartOffset );
      
    for ( i = 0; i != vector->eventVectorSize; i++ )
      buildEquipmentIov( &ptr[i] );
  } else {
    ourIov[currIovSize].iov_base = BO2V( vector->eventVectorBankId,
					 vector->eventVectorStartOffset );
    ourIov[currIovSize].iov_len  = vector->eventVectorSize;
    currIovSize++;
  }
} /* End of buildEquipmentIov */

/* Routine to build our I/O vector. Returns TRUE for OK. Assumes that
   the variable thisEventSize and thisEventPages are properly
   loaded. */
static int buildOurIov( const struct eventHeaderStruct * const event ) {
  int total;
  int e;

  /* Check if the I/O currently allocated (if any) has enough
     entries. If not, deallocate the current I/O vector and allocate a
     new one */
  if ( (thisEventPages+1) > ourIovSize ) {
    if ( ourIov != NULL ) free( ourIov );
    if ( (ourIov =
	   (struct iovec *)malloc((thisEventPages+1)*sizeof(struct iovec))) ==
	 NULL ) {
      LOG_NORMAL ERROR_TO( MON_LOG, "buildOurIov: base malloc failed" );
      return FALSE;
    }
    ourIovSize = thisEventPages+1;
  }
  /* Indicate the end of the vector */
  ourIov[thisEventPages].iov_len = 0;

  /* Single paged or multi-page vector? */
  if ( thisEventPages == 1 ) {

    /* Single page: single entry I/O vector */
    ourIov[0].iov_base = (void *)event;
    ourIov[0].iov_len = event->eventSize;
    currIovSize = 1;
    
  } else {
    
    /* Multi-page: load the preamble and go through the equipments */
    struct vectorPayloadDescriptorStruct *payload;
    struct equipmentDescriptorStruct *equipments;

    /* Copy & update the event header */
    bcopy( event, &ourHeader, EVENT_HEAD_BASE_SIZE );
    ourHeader.eventHeadSize = EVENT_HEAD_BASE_SIZE;
    CLEAR_SYSTEM_ATTRIBUTE( ourHeader.eventTypeAttribute, ATTR_EVENT_PAGED );

    ourIov[0].iov_base = &ourHeader;
    ourIov[0].iov_len = EVENT_HEAD_BASE_SIZE;
    currIovSize = 1;

    payload = (void *)event + EVENT_HEAD_BASE_SIZE;
    equipments = (void *)payload + sizeof( *payload );

    /* Extended vector? */
    if ( payload->eventExtensionVector.eventVectorSize != 0 ) {
      ourIov[currIovSize].iov_base =
	BO2V( payload->eventExtensionVector.eventVectorBankId,
	      payload->eventExtensionVector.eventVectorStartOffset );
      ourIov[currIovSize].iov_len =
	payload->eventExtensionVector.eventVectorSize;
      currIovSize++;
      ourHeader.eventHeadSize += payload->eventExtensionVector.eventVectorSize;
    }
  
    for ( e = 0; e != payload->eventNumEquipments; e++ ) {
      ourIov[currIovSize].iov_base = &equipments[e].equipmentHeader;
      ourIov[currIovSize].iov_len = sizeof( struct equipmentHeaderStruct );
      currIovSize++;
      buildEquipmentIov( &equipments[e].equipmentVector );
    }
  }

  /* A couple of checks on the result of our work */
  if ( currIovSize != thisEventPages ) {
    LOG_NORMAL {
      char msg[1024];
      snprintf( SP(msg),
		"buildOurIov: vector length mismatch\
 (currIovSize:%d thisEventPages:%d)",
	       currIovSize, thisEventPages );
      ERROR_TO( MON_LOG, msg );
    }
    return FALSE;
  }
  if ( ourIov[thisEventPages].iov_len != 0 ) {
    LOG_NORMAL {
      char msg[1024];
      snprintf( SP(msg),
		"buildOurIov: no terminator thisEventPages:%d",
		thisEventPages );
    }
    return FALSE;
  }
  /* The test below can be skipped if performance is an issue */
  if ( TRUE ) {
    for ( total = 0, e = 0;
	  e!=currIovSize && ourIov[e].iov_base!=NULL && ourIov[e].iov_len!=0;
	  e++ )
      total += ourIov[e].iov_len;
    if ( total != thisEventSize || e != thisEventPages ) {
      LOG_NORMAL {
	char msg[1024];
	snprintf( SP(msg),
		  "buildOurIov: paged event size mismatch\
 total:%d thisEventSize:%d thisEventPages:%d e:%d",
		  total, thisEventSize, thisEventPages, e );
	ERROR_TO( MON_LOG, msg );
      }
      return FALSE;
    }
  }

  /* Update the event size field of the header */
  ourHeader.eventSize = thisEventSize;

  return TRUE;
} /* End of buildOurIov */

/* Routine at exclusive use of the event builder for debugging purposes */
static void dumpIov( const struct iovec *iov, const char * const where ) {
  if ( iov != NULL ) {
    char msg[1024];
    int i;

    snprintf( SP(msg), "%s:", where );
    for ( i = 0;; i++ ) {
      struct eventHeaderStruct *h;
      
      snprintf( AP(msg),
		" [%d]:{base:%p len:%d}",
		i, iov[i].iov_base, (int)iov[i].iov_len );
      if ( iov[i].iov_base == NULL ) break;
      h = (struct eventHeaderStruct *)iov[i].iov_base;
      if ( h->eventMagic != EVENT_MAGIC_NUMBER )
	snprintf( AP(msg), "<<< MAGIC:%08x", h->eventMagic );
    }
    INFO_TO( "eventBuilder", msg );
  }
} /* End of dumpIov */

/* Store (copy) an event into the events' buffer.
 *
 * Parameters:
 *   event     Pointer to the event header
 *   rawData   Pointer to buffer containing the event
 *   iov       Pointer to I/O vector pointing to the event buffer
 *   mustStore Boolean: TRUE if the event must be stored (blocking the
 *             producer if there is no space available)
 *
 * Warning: rawData & iov cannot be != NULL at the same time!
 *
 * Assumes: event header (*event) cannot be fragmented!
 *
 * Return:
 *   0    => error
 *   else => offset to the first data block
 */
int monitorStoreEvent( const struct eventHeaderStruct * const event,
		       const        void              * const rawData,
		       const struct iovec             *       iov,
		       const        int                       mustStore ) {
  int    remaining;		/* Number of bytes remaining to store      */
  void  *evData;		/* Pointer to free data block              */
  struct queuePtr *previous;	/* Pointer used to speed up searches       */
  const struct iovec *theIov;	/* The IO vector to use for the store      */
  int    iovIndex;		/* Index to the theIov                     */
  void  *currAddress;		/* Address of the current chunk of data    */
  void  *to;			/* Destination address                     */
  int    currSize;		/* Size of the current chunk of data       */
  int    left;			/* Bytes left in the current chunk of data */
  int    chunkSize = 0;		/* Size of the last moved chunk            */
  int    srcDelta = 0;
  int    status;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorStoreEvent(event:%p rawData:%p iov:%p mustStore:%s)",
	      event,
	      rawData,
	      iov,
	      mustStore ? "TRUE" : "FALSE" );
    INFO_TO( MON_LOG, msg );
  }

  /* We cannot accept both a raw data and a I/O vector */
  if ( rawData != NULL && iov != NULL ) {
    LOG_NORMAL {
      char msg[1024];
      snprintf( SP(msg),
		"monitorStoreEvent event:%p iov:%p, event AND iov?",
		event, iov );
      ERROR_TO( MON_LOG, msg );
    }
    return 0;
  }

  if ( FALSE ) dumpIov( iov, "Injecting" );

  /* Load and check the byte count */
  remaining = thisEventSize;
  if ( remaining > (*mbEventBufferSize) / 2 || remaining <= 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf(SP(msg), "monitorStoreEvent: event size %d invalid", remaining);
      ERROR_TO( MON_LOG, msg );
    }
    return 0;
  }

  /* Load, check and initialise the I/O vector */
  if ( iov != NULL ) {
    if ( TEST_SYSTEM_ATTRIBUTE(event->eventTypeAttribute,ATTR_EVENT_PAGED) ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorStoreEvent: IO/V+paged event not supported" );
      return 0;
    }
    theIov = iov;
  } else {
    if ( !buildOurIov( event ) ) return 0;
    theIov = ourIov;
  }
  iovIndex = 0;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorStoreEvent: size:%d free space:%d method:%s",
	      remaining, mbFreeEvents->size,
	      theIov == iov ? "I/O vector" : "raw data" );
    INFO_TO( MON_LOG, msg );
  }

  /* If this is not a "must" event and if there is not enough space to store
   * it, try to make space */
  if ( (!mustStore) && (mbFreeEvents->size < remaining) ) {
    int giveUp;
    do {
      giveUp = ( monitorMakeSpace( event ) != 0 );
    } while ( !giveUp && mbFreeEvents->size < remaining );
  }
  if ( ( status = monitorFindFreeSpace( remaining, mustStore ) ) != 0 ) {
    if ( status == -1 ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "monitorStoreEvent: no free space available" );
    } else {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorStoreEvent: monitorFindFreeSpace failed" );
    }
    return 0;
  }

  /* Update the stats counters */
  (*mbEventsInjected)++;
  monitorAddToCounter( remaining, mbBytesInjected );

  /* Go through the chunks of free data and the pages of the event */
  evData = NULL;
  previous = NULL;
  while ( remaining != 0 ) {
    struct queuePtr *current;
    int toMove;

    current = (struct queuePtr *)extractFirst( mbFreeEvents );
    if ( current == NULL ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorStoreEvent: extractFirst returns NULL" );
      monitorDropEvent( SUBTRACT(evData, mbFreeEvents) );
      return 0;
    }
    if ( current->size > remaining ) {
      /* Chunk too big: do we split it into two chunks? */
      int sizeChunk;
      int sizeLeftover;

      /* This is where we split the chunk: either the size of the record
       * or the size of the minimum chunk. We also try to keep a bit of
       * alignment for the chunks...
       */
      sizeChunk = MAX( remaining, MINIMUM_QUEUE_ELEMENT_SIZE );
      sizeChunk = ALIGN_PTR( sizeChunk );
      sizeLeftover = current->size - sizeChunk - QUEUE_HEAD_LEN;
      if ( sizeLeftover >= MINIMUM_QUEUE_ELEMENT_SIZE ) {
	struct queuePtr *left;
	
	left = ADD(current, QUEUE_HEAD_LEN + sizeChunk);
	left->size = sizeLeftover;
	current->size = sizeChunk;
	if ( insertAndGather( mbFreeEvents, left ) != 0 ) {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorStoreEvent: insertAndGather 4 leftover failed" );
	  monitorDropEvent( SUBTRACT(evData, mbFreeEvents) );
	  insertAndGather( mbFreeEvents, current );
	  return 0;
	}
      }
    }

    /* Handle a single-pointer linked list */
    current->next = 0;
    if ( previous == NULL ) evData = current;
    else previous->next = SUBTRACT(current, previous);
    previous = current;

    /* Copy the event data that we can copy: move from the source
     * buffer (raw data or I/O vector) to current (skipping the
     * list header) for the size toMove, smaller between the size
     * of the current monitor buffer chunk and the leftover of the
     * event data */
    toMove = MIN( current->size, remaining );
    to = ADD( current, QUEUE_HEAD_LEN );
    
    currAddress = ADD( theIov[iovIndex].iov_base, srcDelta );
    currSize = theIov[iovIndex].iov_len - srcDelta;
    if ( currAddress == NULL || currSize == 0 ) {
      LOG_NORMAL ERROR_TO( MON_LOG, "monitorStoreEvent: invalid I/O vector" );
      return 0;
    }
    for ( left = toMove; left != 0; ) {

      /* Move the next chunk of data, update pointers and counters */
      chunkSize = MIN( currSize, left );
      memcpy( to, currAddress, chunkSize );
      left -= chunkSize;
      to = ADD( to, chunkSize );
      currAddress = ADD( currAddress, chunkSize );
      remaining -= chunkSize;
      srcDelta += chunkSize;

      /* If needed, increment the I/O vector index */
      currSize -= chunkSize;
      LOG_DEVELOPER {
	char msg[1024];
	snprintf( SP(msg),
		  "Inject move loop currSize:%d chunkSize:%d\
 toMove:%d left:%d remaining:%d iovIndex:%d",
		  currSize, chunkSize, toMove, left, remaining, iovIndex );
	INFO_TO( MON_LOG, msg );
      }
      if ( currSize == 0 ) {
	iovIndex++;
	currAddress = theIov[iovIndex].iov_base;
	currSize = theIov[iovIndex].iov_len;
	chunkSize = 0;
	srcDelta = 0;
	if ( ( currSize == 0 || currAddress == NULL ) &&
	     ( remaining != 0 ) ) {
	  LOG_NORMAL {
	    char msg[1024];
	    
	    snprintf( SP(msg),
		      "monitorStoreEvent: unexpected end of I/O vector\
 iov@%p iovIndex:%d currAddress:%p currSize:%d\
 chunkSize:%d remaining:%d thisEventSize:%d",
		      theIov,
		      iovIndex,
		      currAddress,
		      currSize,
		      chunkSize,
		      remaining,
		      thisEventSize );
	    ERROR_TO( MON_LOG, msg );
	  }
	  return 0;
	}
      }
    }
  }
  /* Add the following line to enable run-time checks on payload

#define DO_CHECK

  */
#ifdef  DO_CHECK
  /* Check the injected data according to the data pattern created
     by the DDL data generator:

     header
     equipment header
     garbage (4 bytes)
     0
     1
     2
     ...

     For simplicity, we'll assume that only one equipment is active,
     that we have paged events and that we are running on the LDC
     that created the event.
  */
  {
    int toCheck = thisEventSize;
    int checked = 0;
    long32 *pSrc = NULL;
    int sSrc = 0;
    int iSrc = -1;
    long32 *pDst = (long32 *)ADD( evData, QUEUE_HEAD_LEN );
    struct queuePtr *dDst = evData;
    int sDst = dDst->size;
    int nDst = 0;
    long32 pattern = 0;
    char line[1000];
    struct eventHeaderStruct *ev = theIov[0].iov_base;
    int firstErr = TRUE;
#define LOGF "monitorCheck"

    if ( ev->eventType == PHYSICS_EVENT ) {
      while ( toCheck != 0 ) {
	int error = FALSE;
      
	if ( sSrc == 0 ) {
	  iSrc++;
	  pSrc = theIov[iSrc].iov_base;
	  sSrc = theIov[iSrc].iov_len;
	}
	if ( sDst == 0 ) {
	  dDst = (struct queuePtr *)ADD(dDst, dDst->next);
	  sDst = dDst->size;
	  pDst = (long32 *)ADD( dDst, QUEUE_HEAD_LEN );
	  nDst++;
	}

	if ( *pSrc != *pDst ) {
	  if ( firstErr ) {
	    firstErr = FALSE;
	    ERROR_TO( LOGF, "-----------------------------------" );
	  }
	  snprintf( SP(line),
		    "Data check error iSrc:%d nDst:%d B:%d/%d\
 sSrc:%d/%d sDst:%d/%d",
		    iSrc,
		    nDst,
		    checked,
		    thisEventSize,
		    sSrc,
		    theIov[iSrc].iov_len,
		    sDst,
		    dDst->size );
	  snprintf( AP(line),
		    " in:%08x", *pSrc );
	  snprintf( AP(line),
		    " out:%08x", *pDst );
	  error = TRUE;
	}

	if ( checked >
	     sizeof( struct eventHeaderStruct ) +
	     sizeof( struct equipmentHeaderStruct ) ) {
	  if ( pattern != *pSrc || pattern != *pDst ) {
	    if ( !error ) {
	      snprintf( SP(line),
			"Data pattern error iSrc:%d nDst:%d B:%d/%d sSrc:%d sDst:%d",
			iSrc,
			nDst,
			checked,
			thisEventSize,
			sSrc,
			sDst );
	    }
	    snprintf( AP(line),
		      " exp:%08x", pattern );
	    if ( !error ) {
	      snprintf( AP(line),
			" got:%08x", *pDst );
	    }
	    error = TRUE;
	  }
	  pattern++;
	}

	if ( error ) ERROR_TO( LOGF, line );

	pSrc++;
	pDst++;
	sSrc -= 4;
	sDst -= 4;
	checked += 4;
	toCheck -= 4;
      }
    }
  }
#endif
  if ( FALSE ) dumpIov( iov, "After injection" );
  return SUBTRACT(evData, mbFreeEvents);
} /* End of monitorStoreEvent */
#endif

/* Drop an event from the events' buffer,
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorDropEvent( int rawData ) {
  int status = 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorDropEvent(%d)", rawData );
    INFO_TO( MON_LOG, msg );
  }

  if ( rawData != 0 ) {
    struct queuePtr *curr = ADD(mbFreeEvents, rawData);
    do {
      struct queuePtr *next = ADD(curr, curr->next);
      int status1;
      
      if ( ( status1 = insertAndGather( mbFreeEvents, curr ) ) != 0 ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorDropEvent insertAndGather(\
mbFreeEvents:%p curr:%p) failed, status:(%d x%08x) next:%p",
		    mbFreeEvents, curr, status1, status1, next );
	  ERROR_TO( MON_LOG, msg );
	}
	status = MON_ERR_INTERNAL;
      } else {
	LOG_DEVELOPER {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorDropEvent: insertAndGather( %p %p ) OK, next:%p",
		    mbFreeEvents, curr, next );
	  INFO_TO( MON_LOG, msg );
	}
      }
      curr = curr != next ? next : NULL;
    } while ( status == 0 && curr != NULL );
  } else {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorDropEvent: parameter is NULL" );
    status = MON_ERR_INTERNAL;
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorDropEvent: returning (%d x%08x)",
	      status, status );
    INFO_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorDropEvent */


/* Copy the given event into the given buffer for a given max size.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorCopyEvent( void *inTo, int inFrom, int maxSize ) {
  int                       evSize;
  struct queuePtr          *curr;
  struct eventHeaderStruct *event;
  int                       status = 0;
  char                     *to = inTo;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorCopyEvent(%p,%d,%d)", inTo, inFrom, maxSize );
    INFO_TO( MON_LOG, msg );
  }

  curr = ADD(mbFreeEvents, inFrom);
  event = (struct eventHeaderStruct *)ADD(curr,QUEUE_HEAD_LEN);
  if ( event->eventMagic != EVENT_MAGIC_NUMBER ||
       (evSize = event->eventSize) <= 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorCopyEvent Bad event size:%d magic:x%08x",
		event->eventSize, event->eventMagic );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_BAD_EVENT;
  } else {
    if ( maxSize > 0 ) {
      int copied = MIN( evSize, maxSize );
      int toCopy = copied;
      do {
	int size = MIN( curr->size, toCopy );

	memcpy( to, (char *)ADD(curr, QUEUE_HEAD_LEN), size );
	toCopy -= size;
	to += size;
	curr = ( curr->next != 0 ) ? ADD( curr, curr->next ) : NULL;
      } while ( status == 0 && curr != NULL && toCopy != 0 );
      if ( curr != NULL && status == 0 && copied == evSize )
	LOG_NORMAL ERROR_TO( MON_LOG, "monitorCopyEvent: short event buffer" );
      if ( copied != evSize ) status = MON_ERR_EVENT_TRUNCATED;
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCopyEvent TO:%p FROM:%d maxSize:%d, status:(%d x%08x)",
	     inTo, inFrom, maxSize, status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorCopyEvent */
