/* Monitor V 3.xx client interface module
 * ======================================
 *
 * This is the module that contains all the monitor clients' interface
 * calls.
 *
 * Compilation switches:
 *  OFFLINE    Do not include code for online monitoring (non-DAQ hosts only)
 *  PRODUCER   Include only code for DAQ producers
 *
 * Module history:
 *  1.00 14-Jul-98 RD    Created
 *  1.01 24-Aug-98 RD    First public release
 *  1.02 27-Aug-98 RD    Fix for client-gone-during-insert added
 *  1.03  1-Sep-98 RD    Split PRODUCER-CONSUMER
 *                        Split OFFLINE-ONLINE
 *                        Porting to HP-UX
 *  1.04  5-Oct-98 RD    Return code from legacy calls cleaned up
 *  1.05  7-Oct-98 RD    Monitoring hosts and must monitoring hosts lists added
 *  1.06  8-Oct-98 RD    Access list for relayed monitoring added
 *  1.07 28-Oct-98 RD    Bug with monitorSetDataSource discarding table fixed
 *  1.08 28-Oct-98 RD    Bug with injection of small events slowing down fixed
 *  1.09 30-Oct-98 RD    FORTRAN support improved
 *  1.10 05-Nov-98 RD    FORTRAN monitor_declare_table added
 *  1.11 11-Nov-98 RD    FORTRAN interface moved to separate module
 *  1.12  2-Jun-99 RD    Make space feature added
 *  1.13  8-Jul-99 RD    Event attribute handling added
 *  1.14 21-Oct-99 RD    No clients shortcut added to monitorDoInject
 *  1.15 16-May-00 RD    Set data source now does a Logout (if needed)
 *  1.16 17-May-00 RD    Bug with free of monitorRecordPolicy fixed
 *  1.17 18-Aug-00 RD    Bug with injection + no key file fixed
 *  1.18  4-Dec-00 RD    Adapted to DATE 4.x
 *  1.19 12-Mar-01 RD    Extra checks on event size during inject added
 *  1.20 15-May-01 RD    Bug with injection + no key file fixed
 *  1.21  5-Jul-01 RD    Handling of "&" and "|" for attributes added
 *  1.22 30-Apr-03 RD+OC monitorLogout modified to detach from memory
 *  1.23 30-Apr-03 RD+OC monitorLogout modified to detach from file
 *  1.24 16-May-03 RD+OC GetEventDynamic modified to NULLify the pointer
 *  1.25 17-Aug-05 RD    Added Vanguard and Rearguard events
 *  1.26  5-Sep-05 RD    Added SST and DST events
 *  1.27 14-Sep-05 RD    VAN/REARGUARD events changed into SOD/EOD
 */
#define VID "1.27"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>

#include "monitorInternals.h"
#include "banksManager.h"

#define DESCRIPTION "DATE V3 monitor client interface"
#ifdef OFFLINE
# define TYPE " OFFLINE"
#else
# define TYPE " ONLINE"
#endif
#ifdef PRODUCER
# define ROLE " PRODUCER"
#else
# define ROLE " CONSUMER"
#endif
#ifdef AIX
static
#endif
char monitorClientInterfaceIdent[] = \
   "@(#)""" __FILE__ """: """ DESCRIPTION TYPE ROLE \
   """ """ VID """ compiled """ __DATE__ """ """ __TIME__;


/* The default monitor policy table */
char *monitorDefaultTable[3] = {
  "All events", "yes",
  0
};

#ifndef OFFLINE
/* The following externals are available only for online monitoring */
# ifndef PRODUCER
/* Consumer only: our ID */
extern int monitorOurId;
# endif
# ifdef PRODUCER
/* Producer only: lock released during inject & last injected event */
extern int monitorLockReleased;
extern int monitorLastInsertedEvent;
# endif
#endif

#ifdef PRODUCER
/* Producer only: time of the last age check on the monitoring buffer events */
static time_t timeOfLastAgeCheck = 0;
#endif

/* The monitoring channel flags */
int monitorChannelIsOpen = FALSE;
static enum {
  undefined,			/* channel not yet defined           */
  memory,			/* memory (online DAQ) monitoring    */
  network,			/* network monitoring (via mpDaemon) */
  file				/* local file monitoring             */
} monitorChannel;

#ifndef PRODUCER
/* Consumer only: wait/nowait control flag */
static int monitorWaitForEvents = DEFAULT_WAIT_STATE;
#endif

#ifdef PRODUCER
# ifndef OFFLINE
/* Producer only: the size and the number of pages of the event to be
   injected */
int thisEventSize;
int thisEventPages;
# endif
#endif

/* The monitoring buffer, monitoring host and our host name */
static char monitorBufferName  [ 256 ] = { 0 };
static char monitorHostName    [ 256 ] = { 0 };
static char monitorOurHostName [ 256 ] = { 0 };

/* The remote hostname */
char *remoteHostname = NULL;

#ifndef PRODUCER
/* Consumer only: our monitoring policy table */
static monitorRecordPolicy *monitorPolicyTable = NULL;
static int monitorPolicyTableDeclared = FALSE;
#endif

#ifndef PRODUCER
/* Some entries declared later on in the file... */
int monitorParseTable( int getLock );
int monitorCheckRelayed( monitorRecordPolicy *table );
int monitoringAllowed( monitorType type );
#endif

#ifndef PRODUCER
/* Consumer only: our monitoring program description string */
static char ourId[ MP_MAX_CHARS ] = { 0 };
#endif

#ifndef PRODUCER
/* Consumer only: the swapping control flags */
extern int monitorByteSwappingRequested;
extern int monitorWordSwappingRequested;
#endif

/* The monitoring hosts ACLs */
monitoringHostDescriptor *monitoringHostsList = NULL;
monitoringHostDescriptor *mustMonitoringHostsList = NULL;


/* Test if the monitor channel is from memory.
 *
 * Returns:
 *   TRUE    if monitoring from memory (online DAQ)
 *   FALSE   otherwise
 */
int monitorFromMemory() {
  return monitorChannel == memory;
} /* End of monitorFromMemory */


/* Dump the content of the eventId */
const char * const
   monitorDumpEventId( const struct eventHeaderStruct * const e ) {
  static char result[256];

  if ( e == NULL ) return "=>NULL";
  if ( TEST_SYSTEM_ATTRIBUTE( e->eventTypeAttribute, ATTR_ORBIT_BC ) ) {
    snprintf( SP(result),
	      "(p:%d o:%d bc:%d)",
	      EVENT_ID_GET_PERIOD( e->eventId ),
	      EVENT_ID_GET_ORBIT( e->eventId ),
	      EVENT_ID_GET_BUNCH_CROSSING( e->eventId ) );
  } else {
    snprintf( SP(result),
	      "(nbInRun:%d burstNb:%d nbInBurst:%d)",
	      EVENT_ID_GET_NB_IN_RUN(   e->eventId ),
	      EVENT_ID_GET_BURST_NB(    e->eventId ),
	      EVENT_ID_GET_NB_IN_BURST( e->eventId ) );
  }
  return result;
} /* End of monitorDumpEventId */


/* Dump the event type field into ASCII string */
char *monitorDumpEventType( eventTypeType type ) {
  static char msg[ 1024 ];
  
  switch ( type ) {
  case START_OF_RUN :       return "SOR";
  case END_OF_RUN :         return "EOR";
  case START_OF_RUN_FILES : return "SORF";
  case END_OF_RUN_FILES :   return "EORF";
  case START_OF_BURST :     return "SOB";
  case END_OF_BURST :       return "EOB";
  case PHYSICS_EVENT :      return "PHYSICS";
  case CALIBRATION_EVENT :  return "CALIBRATION";
  case EVENT_FORMAT_ERROR : return "FORMATERROR";
  case START_OF_DATA :      return "SOD";
  case END_OF_DATA :        return "EOD";
  case SYSTEM_SOFTWARE_TRIGGER_EVENT :
                            return "SST";
  case DETECTOR_SOFTWARE_TRIGGER_EVENT :
                            return "DST";
  }
  snprintf( SP(msg), "UNKNOWN 0x%08x", type );
  return msg;
} /* End of monitorDumpEventType */


#ifndef PRODUCER
/* Record a given mp name in the descriptor record (if any).
 *
 * Parameter:
 *   mpName   The name of the MP (ID descriptor string)
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorRecordMp( char *mpName ) {
  int status = 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorRecordMp(%s)", mpName );
    INFO_TO( MON_LOG, msg );
  }
  
  if ( !monitorChannelIsOpen ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorRecordMp: channel is closed" );
    status = MON_ERR_INTERNAL;
  }

  if ( status == 0 ) {
    if ( monitorChannel == memory ) {
#ifdef OFFLINE
      status = MON_ERR_OFFLINE_HOST;
#else
      status = monitorRecordMpInMemory( mpName );
#endif

    } else if ( monitorChannel == network ) {
      status = monitorXmitMp( mpName );
      
    } else if ( monitorChannel == file ) {
      /* File: nothing to do */
      LOG_NORMAL
	INFO_TO( MON_LOG, "monitorRecordMp: channel is file" );
      
    } else {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorRecordMp: channel is unknown" );
      status = MON_ERR_INTERNAL;
    }
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorRecordMp return (%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorRecordMp */
#endif


/* Print an error message following a parse error on a data source
 * specifier.
 *
 * Parameter:
 *   inputValue		The input data source specifier
 *
 * Return:
 *   Always error status
 */
int monitorBadDataSourceSyntax( char *inputValue ) {
  fprintf( stderr, "monitorSetDataSource: bad syntax \"%s\"\n\
   Valid syntax: ([keyFile]|dataFile)[@host]*[:] e.g.:\n\
\t:                  => local online\n\
\t@host:             => remote online \n\
\tfile               => local file\n\
\tfile@host          => remote file\n\
\t@host1@host2:      => online on host1 via host2\n\
\tfile@host1@host2   => file on host1 via host2\n",
	   inputValue );
  return MON_ERR_BAD_DATA_SOURCE;
} /* End of monitorBadDataSourceSyntax */


/* This entry is used to dump the current status of the data source
 * specifier. */
void printDataSource() {
  printf( "Current data source: %s, host:\"%s\", buffer:\"%s\"\n",
	  monitorChannel == memory ? "localDAQ" :
	    monitorChannel == network ? "network" :
	      monitorChannel == file ? "localFile" :
	        monitorChannel == undefined ? "undefined" : "ERROR",
	  monitorHostName,
	  monitorBufferName );
} /* End of printDataSource */


/* Open a monitor channel. This entry can be called at any time to
 * open or re-open a monitor channel.
 *
 * Parameter:
 *   isClient	TRUE if this request comes from a client
 *
 * Return:
 *   0    => OK
 *   else => error (bit-coded)
 */
int monitorOpenChannel( int isClient ) {
  int status = 0;

  initLogLevel();		/* Just in case it hasn't been done */

  if ( monitorChannel == undefined ) {
    if ( ( status = monitorSetDataSource( ":" ) ) != 0 ) {
      LOG_DEVELOPER
	ERROR_TO( MON_LOG,
		  "monitorOpenChannel: failed to monitorSetDataSource" );
      return status;
    }
  }

  if ( monitorChannel == memory ) {
#ifndef OFFLINE    
    char configFile[ 1024 ];
    
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "monitorOpenChannel: opening memory channel" );

    /* First step: parse the global (SITE-wide) monitoring configuration
     * file: translate the configuration file name (converting all symbols
     * in their actual value), test the file readibility and parse it. Please
     * note that - if the file is not there - we just ignore the error
     * (the file is optional) */
    snprintf( SP(configFile), DEFAULT_CONFIGURATION_FILE );
    if ( ( status = monitorTranslate( configFile,
				      sizeof( configFile ) ) ) != 0 )  {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorOpenChannel: cannot translate DEFAULT \
configuration file name \"%s\"",
		  configFile );
	ERROR_TO( MON_LOG, msg );
      }
    }
    if ( status == 0 ) {
      if ( monitorTestFileReadable( configFile ) ) {
	if ( ( status = monitorParseConfigFile( configFile ) ) != 0 ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorOpenChannel: parse of monitoring \
global configuration file \"%s\" failed",
		      configFile );
	    ERROR_TO( MON_LOG, msg );
	  }
	}
      } else {
	LOG_DETAILED {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: global configuration file \"%s\"\
 not present or unreadable",
		    configFile );
	  INFO_TO( MON_LOG, msg );
	}
      }
    }

    /* Second step: do the same as above, this time on the specific monitor
     * buffer configuration (and key) file. The file MUST be there and
     * it must be readable */
    if ( status == 0 ) {
      if ( strlen( monitorBufferName ) > 0 )
	strcpy( configFile, monitorBufferName );
      else
	strcpy( configFile, DEFAULT_MONITOR_BUFFER );
      if ( ( status = monitorTranslate( configFile,
					sizeof( configFile ) ) ) != 0 ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: cannot translate KEY \
configuration file name \"%s\"",
		    configFile );
	  ERROR_TO( MON_LOG, msg );
	}
      }
    }
    if ( status == 0 ) {
      if ( !monitorTestFileReadable( configFile ) ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: cannot access KEY \
configuration file \"%s\" errno:%d (%s)",
		    configFile,
		    errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	status = MON_ERR_NO_KEY_FILE;
      }
    }
    if ( status == 0 ) {
      if ( ( status = monitorParseConfigFile( configFile ) ) != 0 ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: parse of monitoring KEY \
configuration file \"%s\" failed",
		    configFile );
	  ERROR_TO( MON_LOG, msg );
	}
      }
    }

    /* Now we can attach ourselves to the monitoring buffer */
    if ( status == 0 ) {
      if ( ( status = monitorAttachToBuffer( configFile ) ) != 0 ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: attach to shared memory \
segment \"%s\" failed",
		    configFile );
	  ERROR_TO( MON_LOG, msg );
	}
      }
    }

#ifndef PRODUCER
    /* Consumers can sign in at this point */
    if ( isClient && status == 0 ) {
      if ( ( status = monitorLockBuffer() ) == 0 ) {
	int status1;
	
	if ( ( status = monitorSignIn() ) != 0 ) {
	  LOG_NORMAL ERROR_TO( MON_LOG, "monitorOpenChannel: signIn failed" );
	}
	if ( status == 0 ) {
	  if ( ( status = monitorParseTable( FALSE ) ) != 0 ) {
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorOpenChannel: \
table parsing after sign in failed" );
	  }
	}
	if ( ( status1 = monitorUnlockBuffer() ) != 0 ) {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorOpenChannel: buffer unlock for signIn failed" );
	}
	status = status == 0 ? status1 : status;
      } else {
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorOpenChannel: buffer lock for signIn failed" );
      }
    }
#endif

    monitorChannelIsOpen = ( status == 0 );
    
#ifndef PRODUCER
    /* Consumers can declare their mp ID at this point */
    if ( monitorChannelIsOpen ) {
      if ( ourId[ 0 ] != 0 ) {
	int status1 = monitorRecordMp( ourId );
	if ( status1 != 0 ) {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG, "monitorOpenChannel: monitorRecordMp failed" ); 
	  if ( status == 0 ) status = status1;
	}
      } else {
	LOG_DEVELOPER
	  INFO_TO( MON_LOG, "monitorOpenChannel: mp ID not declared" );
      }
    } else {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorOpenChannel: cannot declare ID (channel not open)" );
    }
#endif

    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorOpenChannel local open returns x%08x (channel:%s)",
		status,
		monitorChannelIsOpen ? "open" : "CLOSED" );
      if ( status == 0 )
	INFO_TO( MON_LOG, msg );
      else
	ERROR_TO( MON_LOG, msg );
    }
    return status;
#else
    /* Offline host: we cannot open memory DAQ channels */
    return MON_ERR_OFFLINE_HOST;
#endif
  }

#ifndef PRODUCER
  if ( monitorChannel == network ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG,
	       "monitorOpenChannel: opening networked channel" );

    if ( remoteHostname != NULL ) {
      /* Relayed monitoring: check if this monitoring is allowed */
      status = monitorCheckRelayed( monitorPolicyTable );
    }

    if ( status == 0 ) {
      if ( ( status = monitorNetConnectTo( monitorHostName,
					   monitorBufferName,
					   monitorPolicyTable ) ) == 0 ) {
	LOG_DEVELOPER {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: network open to \
\"%s\"@\"%s\" completed OK",
		    monitorBufferName, monitorHostName );
	  INFO_TO( MON_LOG, msg );
	}
	if ( ourId[0] != 0 ) {
	  if ( ( status = monitorXmitMp( ourId ) ) != 0 ) {
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorOpenChannel: failed to transmit our id" );
	  }
	}
      } else {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorOpenChannel: failed to open network connection to\
 %s@%s status:x%08x errno:%d (%s)",
		    monitorBufferName, monitorHostName, status,
		    errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
      }
    }
    monitorChannelIsOpen = ( status == 0 );
    return status;
  }
#endif

#ifndef PRODUCER
  if ( monitorChannel == file ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "monitorOpenChannel: opening local file channel" );
    if ( ( status = monitorFileOpenChannel( monitorBufferName ) ) == 0 ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "monitorOpenChannel: channel opened OK" );
      monitorChannelIsOpen = TRUE;
      if ( ( status = monitorFileDeclareTable( monitorPolicyTable ) ) == 0 ) {
	LOG_DEVELOPER
	  INFO_TO( MON_LOG, "monitorOpenChannel: table declared OK" );
      } else {
	LOG_DEVELOPER
	  INFO_TO( MON_LOG, "monitorOpenChannel: failed to declare table" );
      }
    } else {
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "monitorOpenChannel: failed to open local file" );
    }
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorOpenChannel returning status:(%d x%08x)",
		status, status );
      if ( status == 0 ) INFO_TO( MON_LOG, msg );
      else ERROR_TO( MON_LOG, msg );
    }
    return status;
  }
#endif
  LOG_NORMAL
    ERROR_TO( MON_LOG, "monitorOpenChannel: uncovered channel type" );
  return MON_ERR_UNSUPPORTED;
} /* End of monitorOpenChannel */


#ifndef PRODUCER
/* Discard the content of the monitor policy table */
void monitorDiscardPolicyTable() {
  while ( monitorPolicyTable != NULL ) {
    monitorRecordPolicy *p;

    p = monitorPolicyTable;
    monitorPolicyTable = p->next;
    free( p->eventType );
    free( p->monitorType );
    free( p->monitorAttributes );
    free( p );
  }
} /* End of monitorDiscardPolicyTable */
#endif


#ifndef PRODUCER
/* Parse one monitor policy entry. We'll try to allow for "shortened"
 * versions.
 *
 * Parameters:
 *  eventType	The type of event associated to the policy
 *  monitorType	The policy to follow for the given event type
 *
 * Changed/set:
 *  thisEventType		The event type pointed by the entry
 *  thisEventMonitorType	The monitor policy given by the user
 *  thisEventMonitorAttributes	The monitor attributes given by the user
 *
 * Returns:
 *  0    => OK
 *  else => error
 */
int thisEventType;
monitorType thisEventMonitorType;
int thisEventMonitorAnd;
eventTypeAttributeType thisEventMonitorAttributes;
int monitorParsePolicyEntry( char *eventType,
			     char *monitorType,
			     char *monitorAttributes ) {
  int i, nAttr;
  
  if ( eventType == NULL || monitorType == NULL )
    return MON_ERR_PARSE_ERROR;

  /* This is our macro used for "shortened" strings tests */
#define MATCHES( t, s ) ( strncasecmp( t, s, strlen( s ) ) == 0 )

  /* Please note that the order of the tests IS important */

  if ( MATCHES( "monitor", monitorType ) ||
       MATCHES( "yes",     monitorType ) ) {
    thisEventMonitorType = yes;
  } else if ( MATCHES( "ignore", monitorType ) ||
	      MATCHES( "no",     monitorType ) ||
	      MATCHES( "dont",   monitorType ) ) {
    thisEventMonitorType = no;
  } else if ( MATCHES( "must",  monitorType ) ||
	      MATCHES( "all",   monitorType ) ||
	      MATCHES( "100%",  monitorType ) ||
	      MATCHES( "100 %", monitorType ) ) {
    thisEventMonitorType = all;
  } else return MON_ERR_PARSE_ERROR;

  if ( MATCHES( "all events", eventType ) ) {
    thisEventType = -1;
  } else if ( ( MATCHES( "start of run files", eventType ) &&
		!MATCHES( "start of run", eventType ) ) ||
	      ( MATCHES( "start_of_run_files", eventType ) &&
		!MATCHES( "start_of_run", eventType ) ) ||
	      ( MATCHES( "sorf", eventType ) &&
		!MATCHES( "sor", eventType ) ) ) {
    thisEventType = START_OF_RUN_FILES;
  } else if ( ( MATCHES( "end of run files", eventType ) &&
		!MATCHES( "end of run", eventType ) ) ||
	      ( MATCHES( "end_of_run_files", eventType ) &&
		!MATCHES( "end_of_run", eventType ) ) ||
	      ( MATCHES( "eorf", eventType ) &&
		!MATCHES( "eor", eventType ) ) ) {
    thisEventType = END_OF_RUN_FILES;
  } else if ( MATCHES( "start of burst", eventType ) ||
	      MATCHES( "start_of_burst", eventType ) ||
	      MATCHES( "sob", eventType ) ) {
    thisEventType = START_OF_BURST;
  } else if ( MATCHES( "end of burst", eventType ) ||
	      MATCHES( "end_of_burst", eventType ) ||
	      MATCHES( "eob", eventType ) ) {
    thisEventType = END_OF_BURST;
  } else if ( MATCHES( "physics event", eventType ) ||
	      MATCHES( "physics_event", eventType ) ) {
    thisEventType = PHYSICS_EVENT;
  } else if ( MATCHES( "calibration event", eventType ) ||
	      MATCHES( "calibration_event", eventType ) ) {
    thisEventType = CALIBRATION_EVENT;
  } else if ( MATCHES( "start of run", eventType ) ||
	      MATCHES( "start_of_run", eventType ) ||
	      MATCHES( "sor", eventType ) ) {
    thisEventType = START_OF_RUN;
  } else if ( MATCHES( "end of run", eventType ) ||
	      MATCHES( "end_of_run", eventType ) ||
	      MATCHES( "eor", eventType ) ) {
    thisEventType = END_OF_RUN;
  } else if ( MATCHES( "event format error", eventType ) ||
	      MATCHES( "event_format_error", eventType ) ||
	      MATCHES( "ferr", eventType ) ) {
    thisEventType = EVENT_FORMAT_ERROR;
  } else if ( MATCHES( "start of data", eventType ) ||
	      MATCHES( "start_of_data", eventType ) ||
	      MATCHES( "sod", eventType ) ) {
    thisEventType = START_OF_DATA;
  } else if ( MATCHES( "end of data", eventType ) ||
	      MATCHES( "end_of_data", eventType ) ||
	      MATCHES( "eod", eventType ) ) {
    thisEventType = END_OF_DATA;
  } else if ( MATCHES( "system_software_trigger_event", eventType ) ||
	      MATCHES( "system software trigger event", eventType ) ||
	      MATCHES( "sst", eventType ) ) {
    thisEventType = SYSTEM_SOFTWARE_TRIGGER_EVENT;
  } else if ( MATCHES( "detector_software_trigger_event", eventType ) ||
	      MATCHES( "detector software trigger event", eventType ) ||
	      MATCHES( "dst", eventType ) ) {
    thisEventType = DETECTOR_SOFTWARE_TRIGGER_EVENT;
  } else return MON_ERR_PARSE_ERROR;

  /* Check the monitor attributes field */
  if ( monitorAttributes == NULL ) return MON_ERR_PARSE_ERROR;

  nAttr = 0;
  /* "*" means all attributes selected */
  if ( monitorAttributes[0] != '*' || monitorAttributes[1] != 0 ) {
    /* Must contain numbers or '&'s or '|'s */
    int numAnds = 0;
    int numOrs  = 0;
    
    for ( i = 0; monitorAttributes[i] != 0; i++ ) {
      if ( monitorAttributes[i] != '&' &&
	   monitorAttributes[i] != '|' &&
	   !isdigit( (int)monitorAttributes[i] ) ) {
	return MON_ERR_PARSE_ERROR;
      }
      if ( monitorAttributes[i] == '&' ) numAnds++;
      if ( monitorAttributes[i] == '|' ) numOrs++;
    }
    /* Must contain only '&'s or '|'s (no mixtures allowed) */
    if ( numAnds != 0 && numOrs != 0 ) return MON_ERR_PARSE_ERROR;
    /* The numbers must be within the allowed range */
    RESET_ATTRIBUTES( thisEventMonitorAttributes );
    for ( i = 0; monitorAttributes[i] != 0; ) {
      int num;
      
      if ( monitorAttributes[i] == '&' || monitorAttributes[i] == '|' ) {
	i++;
      } else {
	if ( sscanf( &monitorAttributes[i], "%d", &num ) != 1 )
	  return MON_ERR_PARSE_ERROR;
	if ( num < 0 || num >= ALL_ATTRIBUTE_BITS )
	  return MON_ERR_PARSE_ERROR;
	nAttr++;
	SET_ANY_ATTRIBUTE( thisEventMonitorAttributes, num );
	while ( monitorAttributes[i] != '&' &&
		monitorAttributes[i] != '|' &&
		monitorAttributes[i] != 0 ) i++;
      }
    }
    thisEventMonitorAnd = ( numAnds != 0 );
  }
  /* If no attributes where specified, select all attributes */
  if ( nAttr == 0 )
    monitorAssertAttributesMask( &thisEventMonitorAttributes );
  return 0;
} /* End of monitorParsePolicyEntry */
#endif


#ifndef PRODUCER
int monitorCheckRelayed( monitorRecordPolicy *table ) {
  int status = 0;
  
  LOG_DETAILED INFO_TO( MON_LOG, "monitorCheckRelayed( <table> ): start" );

  for( ; status == 0 && table != NULL; table = table->next  ) {
    if ( ( status = monitorParsePolicyEntry( table->eventType,
					     table->monitorType,
					     table->monitorAttributes ) ) != 0 ) {
      LOG_DETAILED {
	char msg[1024];
	snprintf( SP(msg),
		  "monitorCheckRelayed: parse error on \"%s\":\"%s\"",
		  table->eventType, table->monitorType );
	ERROR_TO( MON_LOG, msg );
      }
    } else {
      if ( ( status = monitoringAllowed( thisEventMonitorType ) ) != 0 ) {
	LOG_DETAILED {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorCheckRelayed: policy \"%s\":\"%s\" \
not allowed from host \"%s\"",
		    table->eventType,
		    table->monitorType,
		    remoteHostname );
	  ERROR_TO( MON_LOG, msg );
	}
      }
    }
  }

  LOG_DETAILED {
    char msg[1024];
    snprintf( SP(msg),
	      "monitorCheckRelayed return:(%d x%08x)",
	     status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else               ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorCheckRelayed */
#endif


#ifndef PRODUCER
int monitorDoCheck( monitoringHostDescriptor *d ) {
  char *toCompare = remoteHostname != NULL ? remoteHostname : "localhost";
  if ( d == NULL ) return -1;
  while ( d != NULL ) {
    if ( strncasecmp( d->hostname, toCompare, strlen( d->hostname ) ) == 0  ||
	 strcmp( d->hostname, "*" ) == 0 ) {
      LOG_DETAILED {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorDoCheck: \"%s\" validates \"%s\"",
		  toCompare, d->hostname );
	INFO_TO( MON_LOG, msg );
      }
      return -1;
    }
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorDoCheck: no match between \"%s\" and \"%s\"",
		toCompare, d->hostname );
      INFO_TO( MON_LOG, msg );
    }
    d = d->next;
  }
  return 0;
} /* End of monitorDoCheck */


/* Check if a given monitoring type is allowed from this source */
int monitoringAllowed( monitorType type ) {
  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitoringAllowed(%s) hostname:\"%s\"",
	      type == yes ? "yes" : (
		      type == no ? "no" : (
			      type == all ? "all " :
			                    "UNKNOWN" ) ),
	     remoteHostname == NULL ? "<empty>" : remoteHostname );
    INFO_TO( MON_LOG, msg );
  }
  switch ( type ) {
    int status;
    
  case yes:
    status = monitorDoCheck( monitoringHostsList );
    if ( status == 0 && mustMonitoringHostsList != NULL )
      status = monitorDoCheck( mustMonitoringHostsList );
    if ( status == -1 ) {
      LOG_DETAILED INFO_TO( MON_LOG, "monitoringAllowed: allowed (yes)" );
      return 0;
    }
    if ( status != 0 ) return status;
    return MONITOR_NOT_ALLOWED;
  case all:
    status = monitorDoCheck( mustMonitoringHostsList );
    if ( status == -1 ) {
      LOG_DETAILED INFO_TO( MON_LOG, "monitoringAllowed: allowed (must)" );
      return 0;
    }
    if ( status != 0 ) return status;
    LOG_DETAILED INFO_TO( MON_LOG, "monitoringAllowed: not allowed" );
    return MONITOR_NOT_ALLOWED;
  case no:
    LOG_DETAILED INFO_TO( MON_LOG, "monitoringAllowed: allowed (no)" );
    return 0;
  }
  LOG_DETAILED ERROR_TO( MON_LOG, "monitoringAllowed: allowed (notreached)" );
  return MON_ERR_INTERNAL;
} /* End of monitorAllowed */
#endif


#ifndef PRODUCER
#ifndef OFFLINE
/* Parse the content of the monitor table
 *
 * Parameter:
 *   getLock	TRUE if monitor buffer needs to be locked/unlocked
 *
 * Return:
 *   0    => OK
 *   else => error (bit-coded value)
 */
int monitorParseTable( int getLock ) {
  int status;
  int unlockBuffer = FALSE;
  int l;
  monitorRecordPolicy *p;
  
  for ( status = 0, l = 0, p = monitorPolicyTable; p != NULL; p = p->next ) {
    if ( ( status = monitorParsePolicyEntry( p->eventType,
					     p->monitorType,
					     p->monitorAttributes ) ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorParseTable cannot validate policy table \
entry \"%s\":\"%s\"",
		  p->eventType, p->monitorType );
	ERROR_TO( MON_LOG, msg );
      }
    }
  }

  if ( status == 0 ) {
    if ( getLock ) {
      status = monitorLockBuffer();
      if ( status != 0 )
	LOG_NORMAL
	  ERROR_TO( MON_LOG, "monitorParseTable: failed to lock buffer" );
      unlockBuffer = ( status == 0 );
    }
  }
  if ( status == 0 ) status = monitorCheckRegistration();
  if ( status == 0 ) status = monitorCleanReservations( monitorOurId );
  if ( status == 0 ) {
    /* Reset the monitor policy/attribute for all type of events */
    for ( l = 0; l != EVENT_TYPE_RANGE; l++ ) {
      CLIENT( monitorOurId )->monitorPolicy[l] = DEFAULT_MONITOR_POLICY;
      monitorAssertAttributesMask(&(CLIENT(monitorOurId)->monitorAttributes[l]));
    }
    
    /* Parse all entries in the table and load the monitor policy records */
    for ( l = 0, p = monitorPolicyTable;
	  status == 0 && p != NULL;
	  p = p->next ) {
      if ( ( status = monitorParsePolicyEntry( p->eventType,
					       p->monitorType,
					       p->monitorAttributes )) != 0 ) {
	/* In principle, we should never get here. The policy entry has been
	 * validated before: it should re-validate now! Is it changed in the
	 * mean while?
	 */
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		   "monitorParseTable: cannot re-validate entry \
\"%s\":\"%s\" \"%s\"",
		    p->eventType, p->monitorType, p->monitorAttributes );
	  ERROR_TO( MON_LOG, msg );
	}
      } else {
	/* Now we check if we are allowed to perform monitoring */
	if ( ( status = monitoringAllowed( thisEventMonitorType ) ) == 0 ) {

	  /* All OK: we can add the entry */
	  if ( thisEventType == (-1) ) {
	  
	    /* This policy applies to all events */
	    for ( l = 0; l != EVENT_TYPE_RANGE; l++ ) {
	      CLIENT( monitorOurId )->monitorPolicy[l] = thisEventMonitorType;
	      COPY_ALL_ATTRIBUTES( thisEventMonitorAttributes,
				   CLIENT(monitorOurId)->monitorAttributes[l]);
	      CLIENT(monitorOurId)->monitorAttributesAnd[l] =
		thisEventMonitorAnd;
	    }
	  } else {
	    /* This policy applies to one event type only */
	    int ix = thisEventType-EVENT_TYPE_MIN;
	    CLIENT( monitorOurId )->monitorPolicy[ ix ] = thisEventMonitorType;
	    COPY_ALL_ATTRIBUTES( 
			 thisEventMonitorAttributes,
			 CLIENT(monitorOurId)->monitorAttributes[ix] );
	    CLIENT(monitorOurId)->monitorAttributesAnd[ix] =
	      thisEventMonitorAnd;
	  }
	}
      }
    }
  }
  if ( getLock && unlockBuffer ) {
    int status1 = monitorUnlockBuffer();
    if ( status1 != 0 )
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorParseTable: failed to unlock buffer" );
    status = status == 0 ? status1 : status;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorParseTable return (%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorParseTable */
#endif
#endif


#ifndef PRODUCER
/* Copy a monitor policy table into our local buffer.
 *
 * Parameter:
 *  table	        The input monitor policy table
 *  attributesDeclared  The table includes monitor attributes
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorCopyPolicyTable( char **table, int attributesDeclared ) {
  int l;
  int status = 0;
  int nEntries;
  int items;

  for ( nEntries = 0; table[nEntries] != 0; nEntries++ )
    if ( table[nEntries][0] == 0 )
      break;

  if ( nEntries == 0 )
    /* Zero entries: use the default monitor table */
    return monitorCopyPolicyTable( monitorDefaultTable, FALSE );

  if ( attributesDeclared ) items = 3;
  else items = 2;
  if ( ( nEntries % items ) != 0 ) {
    fprintf( stderr,
	     "Monitor policy table format error\
 (%d entr%s found, modulo %d number expected)\n",
	     nEntries,
	     nEntries == 1 ? "y" : "ies",
	     items );
    status = MON_ERR_PARSE_ERROR;
  }

  if ( status == 0 ) {
    /* Parse the entry and validate them */
    for ( l = 0; l != nEntries; l += items ) {
      if ( monitorParsePolicyEntry( table[ l ],
				    table[ l+1 ],
				    items == 2 ? "" : table[ l+2 ] ) != 0 ) {
	fprintf( stderr,
		 "Parse error on monitor policy %d) \"%s\":\"%s\" \"%s\"\n",
		 (int)((l+items) / items),
		 table[l],
		 table[l+1],
		 items == 2 ? "" : table[l+2] );
	status = MON_ERR_PARSE_ERROR;
      }
    }
  }
  
  if ( status == 0 ) {
    monitorRecordPolicy *lastP = NULL;

    monitorDiscardPolicyTable();
    for ( l = 0; l != nEntries; ) {
      monitorRecordPolicy *p;

      p = (monitorRecordPolicy *)malloc( (size_t)sizeof(monitorRecordPolicy) );
      if ( p == NULL ) {
	status = MON_ERR_MALLOC;
      } else {
	p->next = NULL;
	if ( monitorPolicyTable == NULL )
	  monitorPolicyTable = p;
	else
	  lastP->next = p;
	lastP = p;
      }
      
      if ( status == 0 )
	if ( ( p->eventType = strdup( table[l++] ) ) == NULL )
	  status = MON_ERR_MALLOC;
      if ( status == 0 )
	if ( ( p->monitorType = strdup( table[l++] ) ) == NULL )
	  status = MON_ERR_MALLOC;
      if ( status == 0 )
	if ( ( p->monitorAttributes =
	         strdup( attributesDeclared ? table[l++] :
					      "" ) ) == NULL )
	  status = MON_ERR_MALLOC;
      
      if ( status != 0 )
	monitorDiscardPolicyTable();
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorCopyPolicyTable %d entr%s parsed status:(%d x%08x)",
	      nEntries, nEntries == 1 ? "y" : "ies", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorCopyPolicyTable */
#endif


#ifndef PRODUCER
/* Declare our default monitor table */
int monitorDeclareDefaultMonitorTable() {
  int status;
  
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorDeclareDefaultMonitorTable: starting" );
  status = monitorCopyPolicyTable( monitorDefaultTable, FALSE );
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorDeclareDefaultMonitorTable return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorDeclareDefaultMonitorTable */
#endif


#ifndef PRODUCER
/* Internal entry to declare a monitor table.
 *
 * Parameters:
 *   table	          Monitor policy table, set of entries event type +
 *                        monitor type + (optional) monitor attributes,
 *                        null terminated
 *   attributesDeclared   TRUE if the table uses monitor attributes
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorDeclareInternal( char **table, int attributesDeclared ) {
  int status;

  if ( ( status = monitorCopyPolicyTable( table,
					  attributesDeclared ) ) == 0 ) {
    if ( monitorChannelIsOpen ) {
      if ( monitorChannel == memory ) {
#ifdef OFFLINE
	status = MON_ERR_OFFLINE_HOST;
#else
	status = monitorParseTable( TRUE );
#endif
      } else if ( monitorChannel == network ) {
	if ( remoteHostname != NULL ) {
	  /* Relayed monitoring: check if this monitoring is allowed */
	  status = monitorCheckRelayed( monitorPolicyTable );
	}
	if ( status == 0 )
	  status = monitorSendPolicyTable( monitorPolicyTable );
      } else if ( monitorChannel == file ) {
	status = monitorFileDeclareTable( monitorPolicyTable );
      } else {
	FATAL_TO( MON_LOG, "Unknown monitor buffer type" );
	status = MON_ERR_INTERNAL;
      }
    } else {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorDeclareInternal: channel still closed, \
cannot declare the table" );
    }
  }
  if ( status == 0 ) {
    monitorPolicyTableDeclared = TRUE;
  } else {
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "monitorDeclareInternal: reverting to default table" );
    monitorDeclareDefaultMonitorTable();
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorDeclareInternal return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorDeclareInternal */


/* Declare a monitor policy table (with monitor attributes)
 *
 * Parameter:
 *   table	Monitor policy table, set of entries event type + monitor
 *		type + monitor attributes, null terminated
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorDeclareTableWithAttributes( char **table ) {
  int status;

  LOG_DEVELOPER {
    char msg[ 20*100 ];
    int l;
    
    snprintf( SP(msg), "monitorDeclareTableWithAttributes:\n" );
    for ( l=0; table[ l ] != 0; ) {
      snprintf( AP(msg),
		"   -> \"%s\"",
		table[ l++ ] );
      if ( table[l] != 0 )
	snprintf( AP(msg),
		  ": \"%s\"",
		  table[ l++ ] );
      if ( table[l] != 0 )
	snprintf( AP(msg),
		  "+\"%s\"",
		  table[ l++ ] );
      snprintf( AP(msg), "\n" );
    }
    INFO_TO( MON_LOG, msg );
  }
  status = monitorDeclareInternal( table, TRUE );
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorDeclareTableWithAttributes return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorDeclareTableWithAttributes */


/* Declare a monitor policy table (without monitor attributes)
 *
 * Parameter:
 *   table	Monitor policy table, set of entries event type + monitor
 *		type, null terminated
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorDeclareTable( char **table ) {
  int status;

  LOG_DEVELOPER {
    char msg[ 20*100 ];
    int l;
    
    snprintf( SP(msg), "monitorDeclareTable:\n" );
    for ( l=0; table[ l ] != 0; ) {
      snprintf( AP(msg),
		"   -> \"%s\"",
		table[ l++ ] );
      if ( table[l] != 0 )
	snprintf( AP(msg),
		  " : \"%s\"",
		  table[ l++ ] );
      snprintf( AP(msg), "\n" );
    }
    INFO_TO( MON_LOG, msg );
  }

  status = monitorDeclareInternal( table, FALSE );
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorDeclareTable return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorDeclareTable */
#endif


/* Public entry used to set the monitor data source.
 *
 * Parameter:
 *   dataSource		The inout data source specifier
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorSetDataSource( char *dataSource ) {
  int length;
  int at = -1;
  int dots = -1;
  int status = 0;
#ifndef PRODUCER
  int status1;
#endif

  initLogLevel();

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorSetDataSource(%s)", dataSource );
    INFO_TO( MON_LOG, msg );
  }
#ifndef PRODUCER
  if ( monitorChannelIsOpen )
    if ( (status = monitorLogout()) != 0 )
      return status;
#endif
  monitorBufferName[0] = 0;
  if ( ( length = strlen( dataSource ) ) == 0 ) {
    /* DEFAULT setup */
    monitorChannel = memory;
    strcpy( monitorBufferName, DEFAULT_MONITOR_BUFFER );
  } else {
    int i;

    for ( i=0, at = -1, dots = -1; i != length ; i++ ) {
      if ( dataSource[ i ] == '@' ) at = i;
      if ( dataSource[ i ] == ':' ) dots = i;
    }
    /* RFIO uses ':' in the middle of the file name. We will ignore
     * all ':'s excepted when at the very end of the file name */
    if ( dots != -1 && dots != length-1 ) dots = -1;
    if ( dots == (-1) && at == (-1) ) {

      /* No ':'s and no '@'s: this must be a local file name */
      monitorChannel = file;
      strcpy( monitorBufferName, dataSource );
      
    } else if ( at == (-1) ) {

      /* No '@'s and one ':': this is a local memory address */
      monitorChannel = memory;
      strncpy( monitorBufferName, dataSource, length-1 );
      
    } else {

      /* '@' and ':': we don't accept '@' at the end of the string */
      if ( ( dots != -1 && at == dots - 1 ) ||
	   ( dots == -1 && at == length - 1 ) )
	return monitorBadDataSourceSyntax( dataSource );
      
      monitorChannel = network;
      strncpy( monitorBufferName, dataSource, at );
      strncpy( monitorHostName, &dataSource[ at+1 ],
	       length - ( dots == (-1) ? 0 : at+2 ) );
      if ( dots != (-1) ) {
	monitorBufferName[ at ] = ':';
	monitorBufferName[ at + 1 ] = 0;
      }
    }
  }

  /* Now we try to translate all logical names eventually part of the
   * host and buffer names */
  monitorTranslate( monitorBufferName, sizeof( monitorBufferName ) );
  monitorTranslate( monitorHostName, sizeof( monitorHostName ) );
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSetDataSource after transl buffer:\"%s\" host:\"%s\"",
	      monitorBufferName, monitorHostName );
    INFO_TO( MON_LOG, msg );
  }

  /* Now we try to figure out if the hostname points to the local host */
  if ( monitorChannel == network ) {
    char fullOurHostName[ 100 ];
    char fullMonitorHostName[ 100 ];

    if ( ( status = monitorGetHostname( monitorOurHostName,
					sizeof(monitorOurHostName ))) != 0 )
      return status;
    if ( ( status = monitorTranslateAddress( fullOurHostName,
					     sizeof( fullOurHostName ),
					     monitorOurHostName )) != 0 ) {
      /* We have two choices: generate an error or simply ignore the
       * error condition. If we decide to ignore it, we might have
       * problems translating hosts aliases or hosts with/without
       * domain name. We think we can survive with it... */
      LOG_DEVELOPER
	ERROR_TO( MON_LOG,
		  "monitorSetDataSource: error translating our hostName" );
    }
    if ( ( status = monitorTranslateAddress( fullMonitorHostName,
					     sizeof( fullMonitorHostName ),
					     monitorHostName )) != 0 ) {
      /* Same considerations as before */
      LOG_DEVELOPER
	ERROR_TO( MON_LOG,
		  "monitorSetDataSource: error translating monitor hostName" );
    }
    LOG_DEVELOPER {
      char msg[ 1024 ];

      snprintf( SP(msg),
		"monitorSetDataSource/network after name resolution, \
our host:\"%s\" monitorHost:\"%s\"",
		fullOurHostName, fullMonitorHostName );
      INFO_TO( MON_LOG, msg );
    }
    if ( strcmp( fullOurHostName, fullMonitorHostName ) == 0 ||
	 strcmp( "localhost",     monitorHostName ) == 0 ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "Same local and monitor host: reverting..." );
      monitorChannel = dots == (-1) ? file : memory;
    }
  }

  if ( monitorChannel == memory &&
        ( monitorBufferName[0] == ':' || monitorBufferName[0] == 0 ) ) {
    LOG_DEVELOPER
      INFO_TO( MON_LOG,
	       "monitorSetDataSource: empty key file name: assuming default" );
    strcpy( monitorBufferName, DEFAULT_MONITOR_BUFFER );
    if ( monitorBufferName[ strlen( monitorBufferName ) -1 ] == ':' )
      monitorBufferName[ strlen( monitorBufferName ) -1 ] = 0;
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSetDataSource source is %s, \
buffer:\"%s\" host:\"%s\"",
	      monitorChannel == memory ? "localDAQ" :
	        monitorChannel == network ? "network" :
	          monitorChannel == file ? "localFile" :
	            monitorChannel == undefined ? "undefined" :
	                                          "INVALID",
	     monitorBufferName, monitorHostName );
    INFO_TO( MON_LOG, msg );
  }
#ifndef PRODUCER
  if ( !monitorPolicyTableDeclared ) {
    status1 = monitorDeclareDefaultMonitorTable();
    status = status == 0 ? status1 : status;
  }
#endif

  /* Small sanity check on the pointer type */
  if ( sizeof( datePointer ) != sizeof( void * ) ) {
    char msg[ 1024 ];
    snprintf( SP(msg),
	     "DATE configuration error: incorrect pointer size, \
pointer:%d void*:%d",
	     (int)sizeof( datePointer ),
	     (int)sizeof( void* ) );
    ERROR_ALL( MON_LOG, msg );
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSetDataSource return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorSetDataSource */


#ifdef PRODUCER
# ifndef OFFLINE
/* Internal routine to follow a paged event blocks */
static void deriveEquipmentSize( struct eventVectorStruct *vector ) {
  if ( vector->eventVectorSize == 0 ) return;
  if ( vector->eventVectorPointsToVector ) {
    int i;
    struct eventVectorStruct *ptr = BO2V( vector->eventVectorBankId,
					  vector->eventVectorStartOffset );
    
    for ( i = 0; i != vector->eventVectorSize; i++ )
      deriveEquipmentSize( &ptr[i] );
  } else {
    thisEventSize += vector->eventVectorSize;
    thisEventPages++;
  }
} /* End of deriveEquipmentSize */

/* Internal routine to calculate the "real" size of an event */
static void calculateEventSize( const struct eventHeaderStruct* const event ) {
  if ( !TEST_SYSTEM_ATTRIBUTE( event->eventTypeAttribute,ATTR_EVENT_PAGED ) ) {
    thisEventSize = event->eventSize;
    thisEventPages = 1;
  } else {
    struct vectorPayloadDescriptorStruct *payload =
      (void *)event + event->eventHeadSize;
    struct equipmentDescriptorStruct *equipments;
    int e;
  
    thisEventSize = EVENT_HEAD_BASE_SIZE;
    thisEventPages = 1;
  
    payload = (void *)event + event->eventHeadSize;
    equipments = (void *)payload + sizeof( *payload );

    if ( payload->eventExtensionVector.eventVectorSize != 0 ) {
      thisEventSize += payload->eventExtensionVector.eventVectorSize;
      thisEventPages++;
    }
  
    for ( e = 0; e != payload->eventNumEquipments; e++ ) {
      thisEventSize += sizeof( struct equipmentHeaderStruct );
      thisEventPages++;
      deriveEquipmentSize( &equipments[e].equipmentVector );
    }
  }
} /* End of calculateEventSize */
# endif
#endif

#ifdef PRODUCER
# ifndef OFFLINE
/* The "internal" injection routine, used for both inject and inject vector
 *
 *
 * Return:
 *   0    => OK
 *   else => error
 */
static int monitorDoInject( const void * const rawData,
			    const struct iovec * const iov,
			    const struct eventHeaderStruct * const event ) {
  int mustUnlock = FALSE;
  int status = 0;

  if ( !monitorChannelIsOpen ) {
    if ( monitorChannel == undefined ) status = monitorSetDataSource( ":" );
    if ( status == 0 ) status = monitorOpenChannel( FALSE );
  }

  if ( status == 0 ) {
    if ( event->eventMagic != EVENT_MAGIC_NUMBER ||
	 event->eventSize  <= 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorDoInject BAD RAW DATA \
size:%d magic:x%08x type:%s id:%s",
		  event->eventSize,
		  event->eventMagic,
		  monitorDumpEventType( event->eventType ),
		  monitorDumpEventId( event ) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_BAD_EVENT;
    } else {
      calculateEventSize( event );
      LOG_DEVELOPER {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorDoInject size:%d (total:%d pages:%d) magic:x%08x \
type:%s id:%s",
		  event->eventSize,
		  thisEventSize,
		  thisEventPages,
		  event->eventMagic,
		  monitorDumpEventType( event->eventType ),
		  monitorDumpEventId( event ) );
	INFO_TO( MON_LOG, msg );
      }
    }
  }

  /* If there is no key file, abort the next phase */
  if ( mbNumClients == NULL ) {
    LOG_DEVELOPER INFO_TO( MON_LOG, "monitorDoInject No key file found" );
    status = MON_ERR_NO_KEY_FILE;
  }
  
  /* Proceed only if no clients are registered. Note that the test
   * below is made without the mandatory lock. This means that any
   * other process may change the value of the variable on the fly
   * and implies that we will have to re-check the value if we want
   * to be sure. However, the test below can give "wrong" result
   * without changing the semantic of the next block: it is simply
   * a shortcut to speed up things in case no clients are registered.
   */
  if ( status == 0 ) {
    if ( *mbNumClients != 0 ) {

      if ( status == 0 ) {
	mustUnlock = (status = monitorLockBuffer()) == 0;
	if ( status != 0 )
	  LOG_NORMAL ERROR_TO( MON_LOG,
			       "monitorDoInject: failed to lock buffer" );
      }
  
      if ( status == 0 ) {
	if ( thisEventSize > (int)((*mbEventBufferSize) / 2) ) {
	  status = MON_ERR_EVENT_TOO_BIG;
	  LOG_NORMAL
	    ERROR_TO( MON_LOG, "monitorDoInject: event too big to handle" );
	}
      }

      /* This is the right moment to perform the age check on the events
       * currently in the monitor buffer */
      if ( status == 0 ) {
	time_t now = time( NULL );
	if ( now == (-1) ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorDoInject cannot get time errno:%d (%s)",
		      errno, strerror(errno) );
	    ERROR_TO( MON_LOG, msg );
	  }
	} else {
	  if ( ( now - timeOfLastAgeCheck ) >= TIME_BETWEEN_CHECKS ) {
	    timeOfLastAgeCheck = now;
	    if ( ( status = monitorCheckEventsAge( now ) ) != 0 )
	      LOG_NORMAL
		ERROR_TO( MON_LOG,
			  "monitorDoInject: failed to perform age check" );
	  }
	}
      }
    }
  
    if ( status == 0 ) {
      if ( *mbNumClients != 0 ) {
	monitorEventDescriptor evDescriptor;

	/* All parameters are OK and we have registered clients: build the
	 * event reservation descriptor. Then, if the event is needed for
	 * monitoring, try to store the event data and to insert the
	 * event data in the monitor buffer. */
	if ( (status = monitorBuildReservation( &evDescriptor,event )) == 0 ) {
	  /* If this is not a "must" event and if there are no free
	   * descriptors, try to make space for this event. This may
	   * free a descriptor... */
	  if ( evDescriptor.numMust == 0 && evDescriptor.numYes != 0 ) {
	    if ( !monitorFreeReservations() ) {
	      monitorMakeSpace( event );
	    }
	  }
	  if ( evDescriptor.numMust != 0 ||
	       ( evDescriptor.numYes != 0 && monitorFreeReservations())) {
	    monitorLockReleased = FALSE;
	    evDescriptor.eventNumber = ++(*mbCurrentEventNumber);
	    evDescriptor.eventData =
	      monitorStoreEvent( event,
				 rawData,
				 iov,
				 ( evDescriptor.numMust != 0 ) );
	    if ( evDescriptor.eventData != 0 ) {
	      status = monitorInsertReservation( &evDescriptor );
	      if ( status == 0 ) {
		/* All OK so far. During the above operations, the lock might
		 * have been released (to wait for free space or to wait for
		 * free event descriptors). In this case, we must re-check the
		 * clients who requested the event (some of them might have
		 * signed out). */
		if ( monitorLockReleased ) {
		  status = monitorBuildReservation(
				EVENT( monitorLastInsertedEvent ),
				event );
		  if ( status == 0 ) {
		    status = monitorCheckReservations(
				monitorLastInsertedEvent );
		  }
		}
	      }
	      if ( status == 0 ) status = monitorSignalNewEvent();
	    }
	  }
	}
      }
    }
    if ( mustUnlock ) {
      int status1 = monitorUnlockBuffer();
      status = status == 0 ? status1 : status;
      if ( status1 != 0 )
	ERROR_TO( MON_LOG, "monitorDoInject: failed to unlock buffer" );
    }
    if ( status == 0 )
      status = monitorPeriodicClientsCheck( event->eventType );
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorDoInject return:(%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorDoInject */
# endif
#endif


#ifdef PRODUCER
# ifndef OFFLINE
/* Inject an event in the monitor buffer.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorInjectEvent( void *rawData ) {
  return monitorDoInject( rawData, NULL, rawData );
} /* End of monitorInjectEvent */


/* Inject an event in the monitor buffer.
 * The event is given as an I/O vector.
 *
 * Return:
 *   0    => OK
 *   else => error
 */
int monitorInjectEventVector( const struct iovec * const iov ) {
  struct eventHeaderStruct event;
  int   size;
  int   index;
  char *to = (char *)(&event);

  /* Rebuild the event header */
  for ( index = 0, size = sizeof( event ); size != 0; ) {
    int toMove;
    if ( ( toMove = iov[ index ].iov_len ) > size ) toMove = size;
    memcpy( to, iov[ index ].iov_base, toMove );
    to += toMove;
    index++;
    size -= toMove;
  }
  return monitorDoInject( NULL, iov, &event );
} /* End of monitorInjectEventVector */
# endif
#endif


#ifndef PRODUCER
/* Control the behaviour of get event in case no events are available
 *
 * Parameter:
 *    flag   Boolean TRUE if wait is desired, FALSE if get event returns
 *           immediately when no events are available
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorControlWait( int flag ) {
  monitorWaitForEvents = flag;
  return 0;
} /* End of monitorControlWait */
#endif


#ifndef PRODUCER
/* Set wait behaviour in case get event has no available events
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorSetWait() {
  return monitorControlWait( TRUE );
} /* End of monitorSetWait */
#endif


#ifndef PRODUCER
/* Set nowait behaviour in case get event has no available events
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorSetNowait() {
  return monitorControlWait( FALSE );
} /* End of monitorSetNowait */
#endif


#ifndef PRODUCER
/* Control the byte/word swapping scheme
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorSetSwap( int doByteSwap, int doWordSwap ) {
  LOG_DETAILED {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSetSwap(doByteSwap:%s doWordSwap:%s)",
	      doByteSwap ? "TRUE" : "FALSE",
	      doWordSwap ? "TRUE" : "FALSE" );
    INFO_TO( MON_LOG, msg );
  }
  monitorByteSwappingRequested = doByteSwap;
  monitorWordSwappingRequested = doWordSwap;
  return 0;
} /* End of monitorSetSwap */
#endif


#ifndef PRODUCER
/* Flush all available events from the input channel (memory or network)
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorFlushEvents() {
  int status = 0;

  if ( !monitorChannelIsOpen ) {
    status = monitorOpenChannel( TRUE );
  } else {
    if ( monitorChannel == memory ) {
#ifdef OFFLINE
      status = MON_ERR_OFFLINE_HOST;
#else
      status = monitorCleanReservations( monitorOurId );
#endif
    } else if ( monitorChannel == network ) {
      status = monitorCleanReservationsNetwork() ;
    } else {
      /* Nothing to do... */
      status = 0;
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf(SP(msg), "monitorFlushEvents return:(%d x%08x)", status, status);
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorFlushEvents */
#endif


#ifndef PRODUCER
/* Internal entry to get the next event
 *
 * Parameters:
 *    buffer        address of user buffer (isDyn == FALSE)
 *                  address of user pointer to buffer (isDyn == TRUE)
 *    sizeOfBuffer  size of user buffer (isDyn == FALSE)
 *                  0 (isDyn == TRUE)
 *    isDyn         TRUE if dynamic memory allocation is requested
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorGetEventInternal( void *buffer,
			     long32 sizeOfBuffer,
			     int isDyn ) {
  int status = 0;
  if ( !monitorChannelIsOpen ) status = monitorOpenChannel( TRUE );
  if ( ( !isDyn && sizeOfBuffer < EVENT_HEAD_BASE_SIZE ) ||
       ( isDyn && sizeOfBuffer != 0 ) ){
    return MON_ERR_BAD_INPUT;
  }
  
  if ( status == 0 ) {
    if ( monitorChannel == memory ) {
#ifdef OFFLINE
      status = MON_ERR_OFFLINE_HOST;
#else
      int mustUnlock = ( status = monitorLockBuffer() ) == 0;
      if ( status != 0 ) {
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorGetEventInternal: failed to lock buffer" );
      } else {
	//printf("here1 %i %i\n",buffer,sizeOfBuffer);
	status = monitorFindNextEvent( monitorOurId,
				       monitorWaitForEvents,
				       buffer,
				       sizeOfBuffer );
      }
      if ( mustUnlock ) {
	int status1 = monitorUnlockBuffer();
	if ( status1 != 0 )
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorGetEventInternal: failed to unlock buffer" );
	status = status == 0 ? status1 : status;
      }
#endif
    } else if ( monitorChannel == network ) {
      
      /* We try to apply a simple network recovery algorithm to handle
       * hangups. The parameter NETWORK_MAX_RETRIES controls how many
       * times we try to re-open the channel */
      int retries = 0;
      int again;
      do {
	again = FALSE;
	status = monitorNetNextEvent( monitorWaitForEvents,
				      buffer,
				      sizeOfBuffer );
	if ( status == MON_ERR_INTERNAL && retries++ != NETWORK_MAX_RETRIES ) {
	  LOG_NORMAL {
	    ERROR_TO( MON_LOG,
		      "TCP/IP link failure: attempting to reconnect" );
	  }
	  if ( ( status = monitorNetDisconnect() ) == 0 ) {
	    if ( ( status = monitorOpenChannel( TRUE ) ) == 0 ) {
	      again = TRUE;
	    }
	  }
	}
      } while ( again );
      
    } else if ( monitorChannel == file ) {
      status = monitorFileNextEvent( buffer, sizeOfBuffer );
    } else {
      status = MON_ERR_NO_DATA_SOURCE;
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorGetEventInternal return:(%d x%08x)",
	      status, status );  
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorGetEventInternal */


/* Get next event (public entry).
 *
 * Parameter:
 *    buffer        pointer to user buffer
 *    sizeOfBuffer  size (in bytes) of user buffer
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorGetEvent( void *buffer, long32 sizeOfBuffer ) {
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorGetEvent(%p,%d)",buffer, sizeOfBuffer );
    INFO_TO( MON_LOG, msg );
  }
  if ( buffer == NULL ||
       sizeOfBuffer < EVENT_HEAD_BASE_SIZE ) return MON_ERR_BAD_INPUT;
  
  return monitorGetEventInternal( buffer, sizeOfBuffer, FALSE );
} /* End of monitorGetEvent */


/* Get next event via dynamic memory (public entry).
 *
 * Parameter:
 *   buffer   address of the user pointer to the event buffer
 *
 * Returns:
 *    0    => OK
 *    else => error
 */
int monitorGetEventDynamic( void **buffer ) {
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorGetEventDynamic(%p)", buffer );
    INFO_TO( MON_LOG, msg );
  }
  if ( buffer == NULL ) return MON_ERR_BAD_INPUT;

  *buffer = NULL;
  return monitorGetEventInternal( buffer, 0, TRUE );
} /* End of monitorGetEventDynamic */
#endif


#ifndef PRODUCER
int monitorDeclareMp( char *mpName ) {
  int status = 0;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorDeclareMp(%s)", mpName );
    INFO_TO( MON_LOG, msg );
  }

  if ( ourId[ 0 ] == 0 ) {
    strncpy( ourId, mpName, MP_MAX_CHARS );
    ourId[ MP_MAX_CHARS-1 ] = 0;
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg), "monitorDeclareMp new ID:\"%s\"", ourId );
      INFO_TO( MON_LOG, msg );
    }
  } else {
    if ( strncmp( ourId, mpName, strlen( mpName ) ) == 0 ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "monitorDeclareMp: already declared" );
    } else {
      /* Concatenate the two IDs, old one first */
      int lenOld = strlen( mpName );
      char newId[ MP_MAX_CHARS ];
      
      strncpy( newId, mpName, MP_MAX_CHARS );
      if ( lenOld < MP_MAX_CHARS ) {
	strncpy( &newId[ lenOld ], ourId, MP_MAX_CHARS-lenOld );
      }
      strncpy( ourId, newId, MP_MAX_CHARS );
      ourId[ MP_MAX_CHARS-1 ] = 0;
      LOG_DEVELOPER {
	char msg[ 1024 ];
	  snprintf(SP(msg), "monitorDeclareMp concatenated ID:\"%s\"", ourId);
	  INFO_TO( MON_LOG, msg );
      }
    }
  }
  if ( monitorChannelIsOpen ) {
    status = monitorRecordMp( ourId );
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorDeclareMp return:(%d x%08x)", status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  
  return status;
} /* End of monitorDeclareMp */
#endif


#ifndef PRODUCER
/* Close the monitor channel, remain ready for next event.
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorLogout() {
  if ( !monitorChannelIsOpen ) return 0;
  
  monitorChannelIsOpen = FALSE;
  
  if ( monitorChannel == memory ) {
#ifdef OFFLINE
    return MON_ERR_OFFLINE_HOST;
#else
    int status1, status2;
    
    status1 = monitorSignOut();
    status2 = monitorDetachFromBuffer();

    if ( status1 != 0 ) return status1;
    return status2;
#endif
  }

  if ( monitorChannel == network ) return monitorNetDisconnect();

  if ( monitorChannel == file ) return monitorFileDisconnect();

  return MON_ERR_INTERNAL;
} /* End of monitorLogout */
#endif


/* Decode an error code, return an error description string */
char *monitorDecodeError( int errorCode ) {
  static char theMsg[ 1024 ];
  
  switch ( errorCode ) {
  case 0 :
    return "Normal completion";
  case (int)MON_ERR_NOT_IMPLEMENTED :
    return "Call not implemented for this type of monitoring";
  case (int)MON_ERR_INTERNAL :
    return "Monitor internal error";
  case (int)MON_ERR_TOO_MANY_CLIENTS :
    return "Too many clients registered";
  case (int)MON_ERR_NO_DATA_SOURCE :
    return "Call monitorSetDataSource first...";
  case (int)MON_ERR_BAD_EVENT :
    return "Event data corrupted";
  case (int)MON_ERR_EVENT_TOO_BIG :
    return "Event to monitor too big";
  case (int)MON_ERR_INTERRUPTED :
    return "Operation interrupted by external signal";
  case (int)MON_ERR_NOCONNECT :
    return "Cannot connect to remote host (see error log for more details)";
  case (int)MON_ERR_MALLOC :
    return "Cannot malloc for dynamic memory";
  case (int)MON_ERR_TABLE_TOO_BIG :
    return "Monitor policy table too big (zero terminated?)";
  case(int)MON_ERR_PARSE_ERROR :
    return "Parse error on monitor policy table";
  case(int)MON_ERR_BAD_DATA_SOURCE :
    return "Parse error on data source parameter";
  case (int)MON_ERR_NO_KEY_FILE :
    return "Monitor buffer key file (see monitorSetDataSource) not found";
  case (int)MON_ERR_SYS_ERROR :
    return "System-dependent error (see errno for more details)";
  case (int)MON_ERR_SIGNED_OUT :
    return "Client has been disconnected from monitor buffer";
  case (int)MON_ERR_LINE_TOO_COMPLEX :
    return "Configuration line too complex";
  case (int)MON_ERR_CONFIG_ERROR :
    return "Parse error while reading configuration file";
  case (int)MON_ERR_CLEANUP_NEEDED :
    return "Monitor scheme needs cold restart to reconfigure: \
stop all client(s), all producer(s) and retry";
  case (int)MON_ERR_LOCKFILE :
    return "Monitor scheme failure, operator intervention required\
 (see operator message)";
  case (int)MON_ERR_BAD_INPUT :
    return "Invalid input parameter(s), check routine specs";
  case (int)MON_ERR_EOF :
    return "End of input file reached";
  case (int)MON_ERR_OFFLINE_HOST :
    return "Operation not allowed on OFFLINE hosts";
  case (int)MON_ERR_UNSUPPORTED :
    return "Operation not supported for this version of the monitor library";
  case (int)MON_ERR_EVENT_TRUNCATED :
    return "User buffer too small, event truncated";
  case (int)MON_ERR_NO_SUCH_FILE :
    return "Data file not found or not accessible";
  case (int)MONITOR_NOT_ALLOWED :
    return "Host not authorized for this type of monitoring";
  }
  if ( ( errorCode & ERR_BASE ) == ERR_BASE ) {
    snprintf( SP(theMsg), "Monitor-dependent error x%08x", errorCode );
    return theMsg;
  }
  if ( errorCode > 0 ) {
    snprintf( SP(theMsg), "Routine-dependent error:%d", errorCode );
    return theMsg;
  }
  snprintf( SP(theMsg), "Unknown error code:x%08x", errorCode );
  return theMsg;
} /* End of monitorDecodeError */


#ifndef PRODUCER
/****** OLD (legacy) calls *******/
#define SPY_NO_EV_AVAIL      -1
#define SPY_TOO_BIG          -2
#define SPY_NO_GLOBAL        -3
#define SPY_BAD_LUCK         -4
#define SPY_SITE_NOT_DEFINED -5
#define SPY_NO_FILE          -6

int SetDataSource( char *source ) {
  return monitorSetDataSource( source );
}
void SetGetEventInteractive() {
  monitorSetWait();
}
void SetGetEventBatch() {
  monitorSetNowait();
}
void FlushGetEventBuffer() {
  monitorFlushEvents();
}
int getevent( char * ptr, long32 size ) {
  int status = monitorGetEvent( (void *)ptr, size );
  if ( status == 0 ) {
    struct eventHeaderStruct *event = (struct eventHeaderStruct *)ptr;
    
    if ( event->eventSize == 0 ) return SPY_NO_EV_AVAIL;
    return (int)(event->eventSize);
  } else {
    if ( status == MON_ERR_EVENT_TRUNCATED ) return SPY_TOO_BIG;
    if ( status == MON_ERR_NO_SUCH_FILE ) return SPY_NO_FILE;
    if ( status == MON_ERR_NO_KEY_FILE ) return SPY_NO_GLOBAL;
    if ( status == MON_ERR_NO_DATA_SOURCE ) return SPY_SITE_NOT_DEFINED;
    return SPY_NO_GLOBAL;
  }
}
#endif
