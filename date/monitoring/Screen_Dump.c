/*
  void Screen_Dump( int, int *); 

  Version: 4.5.2001 FHH
*/

#include <stdlib.h>
#include <stdio.h>
/*
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
*/

#define min(x,y) ((x)>(y)?(y):(x))
#define max(x,y) ((x)>(y)?(x):(y))

int nrh=0;
int nrd=0;

void Screen_Dump(int evsize, int *data_buf) {
  int dsize;
  int ctrl_beg,ctrl_end;
  int err;
  int ev_type;
  int src_id;
  int ev_size;
  int stat;
  int spill_nr;
  int event_nr;
  int format_nr, nr_err, tcs_err, status_nr;
  int bit[6];
  int trg_time,setup,chip_adr,chn_adr,mes_time,geo_id;
  int j,i;
  int head, data;
  int undecode_data;
  int scount=0;
  int tdc_error=0;
  int adc_size, apv_size, apv_format;
  double dtime,stime;
  char* etext[4] = {"         ","TBO      ","FIFO Full","Hotl.Err."};
  char* etextg[16] = 
  {"ID wrong or         ","data/setup:no header","timeout (no data)   ",
   "event# low          ","no CMC connected    ","data overfl/no trail",
   "FIFO full (port off)","event# higher       ","undefined error 8   ",
   "undefined error 9   ","hotlink error       ","undefined error 11  ",
   "data skipped (trail)","error 13            ","error 14            ",
   "error 15            "};


  /* Dump data to the screen */
  dsize=0;
  i=0;
  head=0;
  data=0;
      printf("\n");
      printf("SLINK Header: %d Words\n",evsize/4);
  /* check for s-link multiplexer */
      src_id=(data_buf[0] >>16 & (1<< 10)-1);
      if ((src_id>=0x380) & (src_id<1024)) {
	err=(data_buf[0] >>31 & (1<< 1)-1);
	ev_type=(data_buf[0] >>26 & (1<< 5)-1);
	src_id=(data_buf[0] >>16 & (1<< 10)-1);
	ev_size=(data_buf[0] >>0 & (1<< 16)-1);
	stat=(data_buf[1] >>31 & (1<< 1)-1);
	spill_nr=(data_buf[1] >>20 & (1<< 11)-1);
	event_nr=(data_buf[1] >>0 & (1<< 20)-1);
	if (evsize/4 >= 1)
        printf("\n");
	printf("S-Link multiplexer: Warning data not yet correctly printed!\n");
	  printf("  0: %08x  Error: %d, Event Type: 0x%02x, Source ID: %4i, Size: %d\n",data_buf[0],err,ev_type,src_id,ev_size);
	if (evsize/4 >= 2)
	  printf("  1: %08x  Status: %d, Spill Number: %d, Event Number: %d\n",data_buf[1],stat,spill_nr,event_nr);
	i += 2;dsize+=8;
      }


      err=(data_buf[i] >>31 & (1<< 1)-1);
      ev_type=(data_buf[i] >>26 & (1<< 5)-1);
      src_id=(data_buf[i] >>16 & (1<< 10)-1);
      ev_size=(data_buf[i] >>0 & (1<< 16)-1);
      stat=(data_buf[i+1] >>31 & (1<< 1)-1);
      spill_nr=(data_buf[i+1] >>20 & (1<< 11)-1);
      event_nr=(data_buf[i+1] >>0 & (1<< 20)-1);
      format_nr=(data_buf[i+2]>>24 & (1<<8)-1);
      nr_err   =(data_buf[i+2]>>16 & (1<<8)-1);
      tcs_err  =(data_buf[i+2]>> 8 & (1<<8)-1);
      status_nr=(data_buf[i+2]>> 0 & (1<<8)-1);
      if (evsize/4 >= i) {
         printf("\n");
         printf("%3d: %08x  Error: %d, Event Type: 0x%02x, Source ID: %4i, Size: %d\n",i,data_buf[i],err,ev_type,src_id,ev_size); 
      }
	 i++;dsize+=4;
	 if (evsize/4 >= i) {
      printf("%3d: %08x  Status: %d, Spill Number: %d, Event Number: %d\n",i,data_buf[i],stat,spill_nr,event_nr);
	 }
    i++;dsize+=4;
    if (evsize/4 >= i) {
      printf("%3d: %x  Format: 0x%02x, #Err: %3d, TCS-Err: 0x%02x, Status: 0x%02x",i,data_buf[i],format_nr,nr_err,tcs_err,status_nr);
    }
    i++;dsize+=4;
      printf("\n");

    if ((((format_nr>>4) & 7) != 0)||((format_nr&1) == 1)) { /* non APV format */
  if (format_nr & 0x80) {
    printf("%3d: %08x  CATCH S/N=%8d #CMC=%02i\n",i,data_buf[i],data_buf[i]>>16,(data_buf[i]>>12)&0xF);
    i++;dsize+=4;
    printf("%3d: %08x  FPGA: M=%3i, F=%3i, S=%3i, T=%3i\n",i,data_buf[i],data_buf[i]>>24,(data_buf[i]>>16)&0xFF,(data_buf[i]>>8)&0xFF,data_buf[i]&0xFF);
    i++;dsize+=4;
    printf("%3d: %08x  CMC 1 S/N %04i  CMC 2 S/N %04i\n",i, data_buf[i], (data_buf[i]>>16)&0xFFFF, data_buf[i]&0xFFFF);
    i++;dsize+=4;
    printf("%3d: %08x  CMC 3 S/N %04i  CMC 4 S/N %04i\n",i, data_buf[i], (data_buf[i]>>16)&0xFFFF, data_buf[i]&0xFFFF);
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("%3d: %08x Port= %2i geogr. ID= %4i front end S/N = %5i\n",i, data_buf[i],(data_buf[i]>>28)&0xF,(data_buf[i]>>16)&0x3FF,(data_buf[i]&0xFFFF));
    i++;dsize+=4;
    printf("\n");
  }

    printf("SLINK Data:\n\n");

    scount = 0; /* scaler counter */

  while(dsize<evsize) 
    {

      printf("%3d: %08x ",i, data_buf[i]);
      bit[6]=(data_buf[i] >>31 & (1<< 1)-1);
      bit[5]=(data_buf[i] >>30 & (1<< 1)-1);

      /*      switch (format_nr & 0x7F) { */
      switch (format_nr & 0x1) {
      case 0:
        /* 32 bit data */

	switch ((format_nr>>4) & 7) {
	case 3: /* scaler */
	  if (!bit[6]) {
	    if ((data_buf[i] >>20 & (1<<12)-1)==0x7FF) 
	      { /* error word */
		printf(" Error: %s, %s geo.id=%03x        port=%2i",
		       etextg[data_buf[i] >>0 & (1<<4)-1],
		       etext[data_buf[i] >>4 & (1<<2)-1],
		       data_buf[i] >>10 & (1<<10)-1,
		       data_buf[i] >>6 & (1<<4)-1);
	      }
	    else
	      {
		head+=1;
		scount=0;
		if (!bit[5]) {
		  printf(" Header ");
		}else {
		  printf(" Trailer");
		}
		printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
	      }
	  }
	  else {
	    data+=1;
	    if (scount<=31) 
	    printf(" Scaler %3i = %10i",scount,data_buf[i]&0x7FFFFFFF);
	    if (scount==32) {
	      printf(" Scaler LSB time = %5i, pattern = 0x%04X",(data_buf[i]>>16)&0x7FFF,data_buf[i]&0xFFFF);
	      stime = (data_buf[i]>>16)&0x7FFF;
	    }
	    if (scount==33) {
	      dtime = stime + (((data_buf[i]>>16)&0x7FFF)<<15);
	      dtime = dtime /38.88/1000;
	      printf(" Scaler MSB time = %5i, pattern = 0x%04X time =%13.6f ms ",(data_buf[i]>>16)&0x7FFF,data_buf[i]&0xFFFF, dtime);
	    }
	    if (scount>33) 
	    printf(" Scaler (error) = %10i",data_buf[i]&0x7FFFFFFF);
	    scount++;
	  }
	  printf("\n");
	  break;
	case 4: /* FI-ADC */
	  if (!bit[6]) {
	    if ((data_buf[i] >>20 & (1<<12)-1)==0x7FF) 
	      { /* error word */
		printf(" Error: %s, %s geo.id=%03x        port=%2i",
		       etextg[data_buf[i] >>0 & (1<<4)-1],
		       etext[data_buf[i] >>4 & (1<<2)-1],
		       data_buf[i] >>10 & (1<<10)-1,
		       data_buf[i] >>6 & (1<<4)-1);
	      }
	    else
	      {
		head+=1;
		if (!bit[5]) {
		  printf(" Header ");
		}else {
		  printf(" Trailer");
		}
		printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
	      }
	  }
	  else {
	    data+=1;
	    printf(" ADC ");
	    printf("Pedsub=%1i, chan=%2i, ofl=%1i, otr=%1i, val=%4i",bit[5],(data_buf[i]>>14)&0x3F,(data_buf[i]>>13)&1,(data_buf[i]>>12)&1,data_buf[i]&0xFFF);
	  }
	  printf("\n");
	  break;
	case 5: /* RICH */
	  if (!bit[6]) {
	    if ((data_buf[i] >>20 & (1<<12)-1)==0x7FF) 
	      { /* error word */
		printf(" Error: %s, %s geo.id=%03x        port=%2i",
		       etextg[data_buf[i] >>0 & (1<<4)-1],
		       etext[data_buf[i] >>4 & (1<<2)-1],
		       data_buf[i] >>10 & (1<<10)-1,
		       data_buf[i] >>6 & (1<<4)-1);
	      }
	    else
	      {
		head+=1;
		if (!bit[5]) {
		  printf(" Header ");
		}else {
		  printf(" Trailer");
		}
		printf(" geographic id= %d, event= %d",(data_buf[i]>>20)&0x3FF,data_buf[i]&0xFFFFF);
	      }
	  }
	  else {
	    data+=1;
	    printf(" RICH  spare=%1i chamber=%1i bora=%2i channel=%4i ADC=%4i",(data_buf[i]>>28)&7,(data_buf[i]>>25)&7,(data_buf[i]>>20)&0x1F,(data_buf[i]>>10)&0x3FF,(data_buf[i])&0x3FF);
	  }
	  printf("\n");
	  break;

	default:
	  printf("\n");
	  break;
	}
	break;
      
      case 1:
	/******************** 24 bit TDC data ******************/

	if ((((format_nr>>1) & 1) == 0) || (tdc_error==1)) {

	  tdc_error=0; /* if tdc_error in sparsified mode, next word is in debug mode */
	  bit[6]=(data_buf[i] >>31 & (1<< 1)-1);
	  bit[5]=(data_buf[i] >>30 & (1<< 1)-1);
	  if (!bit[6]) 
	    { /* header word */
	      head+=1;
	      event_nr=(data_buf[i] >>24 & (1<< 6)-1);
	      trg_time=(data_buf[i] >>15 & (1<< 9)-1);
	      setup=(data_buf[i] >>14 & (1<< 1)-1);
	      chip_adr=(data_buf[i] >>11 & (1<< 3)-1);
	      chn_adr=(data_buf[i] >>8 & (1<< 3)-1);
	      for (j=0;j<4;j++) 
		{
		  bit[j]=(data_buf[i] >>j & (1<< 1)-1);
		}
	      printf(" Header");
	      printf(" event# %2d tbo=%1d time= %3d",event_nr,bit[5],trg_time);
	      printf(" xor=%1d chip %x / %x ",setup,chip_adr,chn_adr); 

	      if (((format_nr>>6)&1)==0) { /* TDC-CMC */
		if (chip_adr<4 & bit[chip_adr]==1) 
		  printf("locked");
		else
		  printf("      ");
	      } else {
		  printf("      ");
	      }
	      printf(" port=%2i",data_buf[i]>>4 & (1<< 4)-1);
	      printf("\n");
	    } 
	  else if ((data_buf[i] >>20 & (1<<12)-1)==0xFFF) 
	    { /* error word */
	      /*	  printf(" Error: %06x\n",data_buf[i] >>0 & (1<<20)-1); */
	      printf(" Error: %s, %s geo.id=%03x        port=%2i\n",
		     etextg[data_buf[i] >>0 & (1<<4)-1],
		     etext[data_buf[i] >>4 & (1<<2)-1],
		     data_buf[i] >>10 & (1<<10)-1,
		     data_buf[i] >>6 & (1<<4)-1);
	    }
	  else 
	    { /* data word */
	      data+=1;
	      chip_adr=(data_buf[i] >>27 & (1<< 3)-1);
	      chn_adr=(data_buf[i] >>24 & (1<< 3)-1);
	      mes_time=(data_buf[i] >>8 & (1<< 16)-1);
	      for (j=0;j<4;j++) 
		{
		  bit[j]=(data_buf[i] >>j & (1<< 1)-1);
		}
	      printf(" Data ");
	      if ((format_nr>>5) & 1) { /* lead & trail */
		printf("%s",(mes_time&1)?"leading ":"trailing");
	      }else{
		printf("        ");
	      }
	      printf("    time= %5d ",mes_time);
	      if ((format_nr>>3) & 1) { /* latch mode */
		printf("wire %1s%1s%1s%1s",(mes_time>>3)&1?"3":" ",(mes_time>>2)&1?"2":" ",(mes_time>>1)&1?"1":" ",(mes_time>>0)&1?"0":" ");
	      }else{
		printf("         ");
	      }
	      
	      printf(" chip %x / %x ",chip_adr,chn_adr);
	      if (((format_nr>>6)&1)==0) { /* TDC-CMC */
		if (chip_adr<4 & bit[chip_adr]==1) 
		  printf("locked");
		else
		  printf("      ");
	      } else {
		  printf("      ");
	      }
	      printf(" port=%2i",data_buf[i]>>4 & (1<< 4)-1);
	      printf("\n");
	    }
	}


	else { /* sparsified TDC data */
	  tdc_error=0;
	  if ((data_buf[i] >>20 & (1<<12)-1)==0xFFF) 
	    { /* error word */
	      printf(" Error: %s, %s geo.id=%03x        port=%2i\n",
		     etextg[data_buf[i] >>0 & (1<<4)-1],
		     etext[data_buf[i] >>4 & (1<<2)-1],
		     data_buf[i] >>10 & (1<<10)-1,
		     data_buf[i] >>6 & (1<<4)-1);
	      tdc_error=1;
	    }
	  else 
	    { /* data word */
	      data+=1;
	      geo_id  =(data_buf[i] >>22 & (1<<10)-1);
	      chip_adr=(data_buf[i] >>19 & (1<< 3)-1);
	      chn_adr =(data_buf[i] >>16 & (1<< 3)-1);
	      mes_time=(data_buf[i] >>0 & (1<< 16)-1);
	      printf(" Data  ");
	      printf("           time= %5d       ",mes_time);
	      printf("    chip %x / %x ",chip_adr,chn_adr);
	      printf(" id=%4i",geo_id);
	      printf("\n");
	    }

	}
      break;
      default:
	  printf("\n");
      }
      dsize+=4;
      i+=1;
    }
    printf("\n %d Header words, %d Data words\n",head, data);

    } else { /******** APV chip decode ******/

      while(dsize<evsize) {
	adc_size = (data_buf[i] >>0 & (1<<16)-1);
	apv_format = (data_buf[i] >>24 & (1<< 8)-1);
	printf("%3d: %08x ",i, data_buf[i]);
	printf(" Format=%02x      ADC id=%3i         Size=%7i\n",(data_buf[i] >>24 & (1<< 8)-1),(data_buf[i] >>16 & (1<< 8)-1),adc_size);
	dsize+=4;
	i++;

	printf("%3d: %08x ",i, data_buf[i]);
	printf(" Dummy = %02x         time tag = %9i\n",(data_buf[i] >>24 & (1<< 8)-1),(data_buf[i] >>0 & (1<<24)-1));
	dsize+=4;
	i++;
	adc_size-=2;

	while(dsize<evsize && adc_size>0) {
	  
	  apv_size = (data_buf[i] >>0 & (1<<16)-1);
	    printf("%3d: %08x ",i, data_buf[i]);
	  printf("    APV id= %01x       event=%5i    size=%7i\n",(data_buf[i] >>28 & (1<< 4)-1),(data_buf[i] >>16 & (1<<12)-1),apv_size);
	  dsize+=4;
	  i++;

	  printf("%3d: %08x ",i, data_buf[i]);
	  printf("    Dummy=%02x            APV3= %03x APV2= %03x APV1= %03x\n",(data_buf[i] >>27 & (1<< 5)-1),(data_buf[i] >>18 & (1<<9)-1),(data_buf[i] >>9 & (1<<9)-1),(data_buf[i] >>0 & (1<<9)-1));
	  dsize+=4;
	  i++;
	  apv_size-=2;
	  adc_size-=2;

	  while(dsize<evsize && apv_size>0) {
	    printf("%3d: %08x ",i, data_buf[i]);
	    if ((apv_format & 2)==2) {
	      /* Latch ALL */
	      printf("       ALLdata chan=%02x  ADC3= %03x ADC2= %03x ADC1= %03x\n",(data_buf[i] >>30 & (1<< 2)-1),(data_buf[i] >>20 & (1<<10)-1),(data_buf[i] >>10 & (1<<10)-1),(data_buf[i] >>0 & (1<<10)-1));
	    } else {
	      /* sparse data */
	      printf("       Sparse  chan=%02x  ADC3= %03x ADC2= %03x ADC1= %03x\n",(data_buf[i] >>25 & (1<< 7)-1),(data_buf[i] >>16 & (1<<9)-1),(data_buf[i] >>8 & (1<<8)-1),(data_buf[i] >>0 & (1<<8)-1));
	    }
	    
	    dsize+=4;
	    i++;
	    apv_size--;
	    adc_size--;
	  }

	}


      }

    }
} 

