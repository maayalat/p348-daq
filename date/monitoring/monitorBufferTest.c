#include <stdio.h>
#include <stdlib.h>
#include "monitorInternals.h"

int main( int argc, char **argv ) {
  int status;

  initLogLevel();
  
  status = monitorAttachToBuffer( "/tmp/keyFile" );
  printf( "monitorAttachToBuffer returns %d\n", status );
  
  exit( 0 );
}
