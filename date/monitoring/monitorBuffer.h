/* Monitor buffer definitions
 * ==========================
 */
#ifndef __monitor_buffer_h__
#define __monitor_buffer_h__

/* The mask used to create a temporary lock file using the key as ID */
#define LOCK_FILE_MASK "/tmp/lock.%d"


/* The maximum number of retries to sleep before giving up grabbing the
 * lock to initialise the shared memory buffer */
#define MAX_NUM_RETRIES 10


/* The semaphores used for synchronisation between actors */
#define LOCK_SEMAPHORE 0
#define FREE_SPACE_SEMAPHORE 1
#define NEW_EVENTS_SEMAPHORE 2
#define MAX_SEMAPHORES 3


/* The public entries to the monitor buffer handler */
char *monitorBufferGetVid();	    /* Get the version ID of the module      */
void monitorAddToCounter( int size, /* Increment the given counter           */
			  long32 *counter );

int   monitorAttachToBuffer( char *keyFile ); /* Attach to the monitor
					       * buffer.
					       * 0    => OK
					       * else => ERROR(s)         */
int monitorDetachFromBuffer(); /* Detach from the buffer (return: 0/error) */

/* All the following routines return 0 for OK, non-zero for error */
int monitorLockBuffer();
int monitorUnlockBuffer();
int monitorWaitForFreeSpace();
int monitorSignalFreeSpace();
int monitorWaitForNewEvents();
int monitorSignalNewEvent();
int monitorStopWaitingForNewEvents();

/* The pointers to the monitor buffer and its structures */
extern void            *monitorBuffer;
extern int             *mbMonitorBufferSize;
extern int             *mbMaxClients;
extern int             *mbMaxEvents;
extern int             *mbEventSize;
extern int             *mbEventBufferSize;
extern int             *mbNumClients;
extern int             *mbOldestEvent;
extern int             *mbFirstFreeEvent;
extern int             *mbCurrentEventNumber;
extern int             *mbNumSignIn;
extern int             *mbNumSignOut;
extern int             *mbForcedExits;
extern int             *mbTooManyClients;
extern int             *mbEventsInjected;
extern long32          *mbBytesInjected;
extern void            *monitorClients;
extern void            *monitorEvents;
extern struct queuePtr *mbFreeEvents;
extern void            *monitorEventsData;


/* The pointer to the monitor control semaphores */
extern int monitorControlSemaphores;

#endif
