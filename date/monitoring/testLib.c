/* Monitor V 3.xx test library module
 * ==================================
 *
 * Module history:
 * 1.00 30-Jul-98  RD  Created
 * 1.01 24-Aug-98  RD  First public release
 * 1.02  8-Jul-99  RD  Handling of event attribute added
 * 1.03 13-Aug-99  RD  Equipment bit removed
 * 1.04  5-Jul-01  RD  Bug fixed with generation of attributes
 * 1.05 17-Aug-05  RD  Added Vanguard and Rearguard events
 * 1.06  5-Sep-05  RD  Added DST and SST events
 * 1.07 14-Sep-05  RD  VAN/REARGUARD events changed into SOD/EOD
 */
#define VID "1.07"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <math.h>
#include <unistd.h>

#include "testLib.h"
#include "monitorInternals.h"
#include "event.h"

#define DESCRIPTION "DATE V3 monitor test library module"
#ifdef AIX
static
#endif
char monitorTestLibraryIdent[] = \
   "@(#)""" __FILE__ """: """ DESCRIPTION \
   """ """ VID """ compiled """ __DATE__ """ """ __TIME__;


int         timerMin, timerMax;
int         debug = FALSE;
int         seed  = 0;
int         numLoops = 1;
double      randRange;
int         currEvent = 0;
eventIdType eventId;
int         mustConsume = FALSE;
int         useIOV = FALSE;

char  *dataSource = NULL;
char  *consumerName = NULL;
char  *consumerName2 = NULL;

int    evSerialNum = 0;

int usage( char **argv ) {
  fprintf( stderr,
	   "Usage: %s configFile [-d] [-m] [-c name] [-n secondName] [-l loops#] [-s seed] [-f]\n",
	   argv[ 0 ] );
  return 1;
}

int monParseFlags( int argc, char **argv, int client ) {
  int flag = 2;

  while ( flag < argc ) {
    if ( strcmp( argv[ flag ], "-d" ) == 0 ) {
      debug = TRUE;
    } else if ( strcmp( argv[ flag ], "-s" ) == 0 ) {
      if ( ++flag == argc ) {
	fprintf( stderr, "-s what?\n" );
	return -1;
      }
      if ( sscanf( argv[ flag ], "%d", &seed ) != 1 ) {
	fprintf( stderr,
		 "Cannot convert \"%s %s\" to int\n",
		 argv[flag-1], argv[flag] );
	return -1;
      }
    } else if ( strcmp( argv[ flag ], "-l" ) == 0 ) {
      if ( ++flag == argc ) {
	fprintf( stderr, "-l what?\n" );
	return -1;
      }
      if ( sscanf( argv[ flag ], "%d", &numLoops ) != 1 ) {
	fprintf( stderr,
		 "Cannot convert \"%s %s\" to int\n",
		 argv[flag-1], argv[flag] );
	return -1;
      }
    } else if ( strcmp( argv[ flag ], "-c" ) == 0 ) {
      if ( ++flag == argc ) {
	fprintf( stderr, "-c what?\n" );
	return -1;
      }
      consumerName = strdup( argv[ flag ] );
    } else if ( strcmp( argv[ flag ], "-n" ) == 0 ) {
      if ( ++flag == argc ) {
	fprintf( stderr, "-n what?\n" );
	return -1;
      }
      consumerName2 = strdup( argv[ flag ] );
    } else if ( strcmp( argv[ flag ], "-m" ) == 0 ) {
      if ( !client ) {
	fprintf( stderr,
		 "%s: MustConsume flag valid for simple consumer only\n",
		 argv[0] );
	return -1;
      }
      mustConsume = TRUE;
    } else if ( strcmp( argv[ flag ], "-f" ) == 0 ) {
      useIOV = TRUE;
    } else {
      fprintf( stderr, "%s: unknown flag \"%s\"\n", argv[0], argv[flag] );
      return -1;
    }
    flag++;
  }

  return 0;
}

char **parse( char **config ) {
  int ix = 0;

  if ( config == NULL ) return NULL;
  
  /* First line: timers */
  if ( config[ ix ] == 0 ) return NULL;
  if ( sscanf( config[ ix ], "%d %d %d", &timerMin, &timerMax, &seed ) != 3 ) {
    fprintf( stderr, "Parse error on first line \"%s\"\n", config[ ix ] );
    return NULL;
  }
  ix++;

  /* Second line: data source */
  if ( config[ ix ] == 0 ) return NULL;
  dataSource = config[ ix ];
  ix++;
  
  /* Rest: monitor table */
  return &config[ ix ];
}

char **createConfigTable( char **table ) {
  int    nLine = 0;
  int    i;
  char **result;
  
  while ( table[ nLine ] != 0 ) nLine++;
  if ( nLine == 0 ) return NULL;
  if ( (result = (char **) malloc((size_t)((nLine*3 + 1)*sizeof(char*)))) ==
       NULL ) {
    fprintf( stderr,
	     "Not enough memory to create monitor configuration table" );
    return NULL;
  }
  /* Parse the input table whose format is:
   * (space)* eventType (space)* policy [(space)* attributes]
   */
  for ( i = 0, nLine = 0; table[ nLine ] != 0; nLine++ ) {
    char *p = table[ nLine ];
    /* Skip the initial spaces leading to the event type */
    while ( *p == ' ' && *p != 0 ) p++;
    /* This is the event type */
    result[i++] = p;
    while ( *p != ' ' && *p != 0 ) p++;
    if ( *p != 0 ) *p++ = 0;
    /* This is the monitoring policy */
    result[i++] = p;
    while ( *p != ' ' && *p != 0 ) p++;
    if ( *p != 0 ) *p++ = 0;
    /* Here we might have the attributes */
    if ( *p != 0 ) result[i++] = p;
    else result[i++] = "";
  }
  result[ i ] = NULL;
  return result;
}

static char line[ 4000 ];
char *getLine( FILE *inFile ) {
  int i, j;
  
  if ( fgets( line, sizeof( line ), inFile ) == NULL ) return NULL;

  for ( i=0; i != strlen( line ); i++ )
    if ( line[i] == '\t' ) line[i] = ' ';
  
  for ( i = strlen( line ); i != -1; i-- )
    if ( line[i] == 0 ||
	 line[i] == '\n' ||
	 line[i] == '#' ) line[i] = 0;

  for ( i = strlen( line ) - 1; i != -1; i-- ) {
    if ( line[i] != ' ' ) break;
    line[i] = 0;
  }

  for ( j = 0, i = 0; i != strlen( line ); ) {
    if ( line[i] != ' ' ) line[j++] = line[i++];
    else {
      if ( j != 0 ) {
	if ( line[j-1] != ' ' ) line[j++] = ' ';
      }
      i++;
    }
  }
  line[j] = 0;
  return line;
}

#define MAX_RECORD 1000

char **monParseConfig( char *whoAmI, char *configFile ) {
  FILE  *inFile;
  int    paragraphBreak = TRUE;
  int    inRecord = FALSE;
  int    lineN = -1;
  char **theRecord;

  if ( whoAmI == 0 ) {
    fprintf( stderr, "monParseConfig: whoAmI == NULL?\n" );
    return NULL;
  }
  if ( strlen( whoAmI ) == 0 ) {
    fprintf( stderr, "monParseConfig: empty whoAmI?\n" );
    return NULL;
  }

  if ( (theRecord = (char **)malloc((size_t)(sizeof(char *[ MAX_RECORD ])))) ==
       NULL ) {
    fprintf( stderr, "Not enough memory to parse configuration file" );
    return NULL;
  }
    
  if ( (inFile = fopen( configFile, "r")) == NULL ) {
    perror( "Cannot open configuration file " );
    return NULL;
  }
  
  while ( !feof( inFile ) ) {
    char *line;

    if ( ( line = getLine( inFile ) ) != NULL ) {
      if ( strlen( line ) > 0 ) {
	if ( paragraphBreak ) {
	  paragraphBreak = FALSE;
	  if ( strcasecmp( whoAmI, line ) == 0 ) inRecord = TRUE;
	}
	if ( inRecord ) {
	  if ( lineN >= 0 ) {
	    theRecord[ lineN ] = (char*)malloc( (size_t)(strlen( line )+1) );
	    strcpy( theRecord[ lineN ], line );
	  }
	  if ( ++lineN == MAX_RECORD ) {
	    fprintf( stderr, "Record too complex..." );
	    fclose( inFile );
	    return NULL;
	  }
	}
      } else {
	paragraphBreak = TRUE;
	if ( inRecord ) {
	  fclose( inFile );
	  theRecord[ lineN ] = NULL;
	  return theRecord;
	}
      }
    }
  }
  if ( fclose( inFile ) != 0 )
    perror( "fclose on configuration file failed " );
  if ( lineN < 0 ) return NULL;
  theRecord[ lineN ] = NULL;
  return theRecord;
}

#ifdef SunOS
#define TIME_STRUCT timespec
#define SLEEP_CALL  nanosleep
#endif
#ifdef AIX
#define TIME_STRUCT timestruc_t
#define SLEEP_CALL  nsleep
#endif
#ifdef HPUX
#define TIME_STRUCT timespec
#define SLEEP_CALL  nanosleep
#endif
#ifdef Linux
#define TIME_STRUCT timespec
#define SLEEP_CALL  nanosleep
#endif
#ifdef OSF1
#define TIME_STRUCT timespec
#define SLEEP_CALL  nanosleep_d9
#endif

int doWait() {
  struct TIME_STRUCT sleepTime, remTime;
  int status;

  sleepTime.tv_nsec = timerMin + randRange * rand();
  if ( sleepTime.tv_nsec < 1000000 ) {
    sleepTime.tv_sec = 0;
  } else {
    sleepTime.tv_sec = sleepTime.tv_nsec / 1000000;
    sleepTime.tv_nsec -= sleepTime.tv_sec * 1000000;
  }
  sleepTime.tv_nsec *= 1000;
  if ( debug )
    printf( "%08lx: sleeping for %ld s, %ld msec\n",
	    (unsigned long int)getpid(),
	    sleepTime.tv_sec,
	    sleepTime.tv_nsec/1000 );
  status = SLEEP_CALL( &sleepTime, &remTime );
  if ( status != 0 )
    perror( "Error during sleep " );
  
  return status;
}


char theEvent[ 100000 ];
struct eventHeaderStruct *ev = (struct eventHeaderStruct *)theEvent;
int *evData = (int *)((long)theEvent + EVENT_HEAD_BASE_SIZE);

int initVars() {
  int i;
  int *ptr;
  
  randRange = (timerMax - timerMin + 1) / pow( 2.0, 15.0 );

  srand( seed );

  ptr = (int *)ADD(theEvent, EVENT_HEAD_BASE_SIZE);
  for ( i = 0; (long)ptr != (long)ADD(theEvent, sizeof( theEvent )); )
    *(ptr++) = i++;
  
  LOAD_EVENT_ID( eventId, 0, 0, 0 );
  
  return 0;
}

void *createNextEvent( char **eventsList ) {
  int  size;
  char type[ 100 ];
  char attributes[ 100 ];
  int  i;
  int  incrNbInBurst = FALSE;

  i = sscanf( eventsList[ currEvent ],
	      "%d %s %s",
	      &size,
	      type,
	      attributes );
  if ( i != 2 && i != 3 ) {
    printf( "%08lx Invalid record %d) \"%s\"\n",
	    (unsigned long int)getpid(), currEvent, eventsList[ currEvent ] );
    return NULL;
  }
  if ( i == 2 ) attributes[0] = 0;

  if ( size > sizeof( theEvent ) ) {
    printf( "Event too big. Requested: %d, available: %d\n",
	    size, (int)sizeof( theEvent ) );
    return NULL;
  }
  if ( size <= 0 ) {
    printf( "Invalid event size %d\n", size );
    return NULL;
  }
    
  if ( eventsList[ ++currEvent ] == 0 )
    currEvent = 0;
  
#define MATCHES( t, s ) ( strncasecmp( t, s, strlen( t ) ) == 0 )
  if ( MATCHES( "SORF", type ) ||
       MATCHES( "start of run files", type ) )
    ev->eventType = START_OF_RUN_FILES;
  else if ( MATCHES( "EORF", type ) ||
	    MATCHES( "end of run files", type ) )
    ev->eventType = END_OF_RUN_FILES;
  else if ( MATCHES( "SOR", type ) ||
	    MATCHES( "start of run", type ) ) {
    ev->eventType = START_OF_RUN;
  } else if ( MATCHES( "EOR", type ) ||
	    MATCHES( "end of run", type ) )
    ev->eventType = END_OF_RUN;
  else if ( MATCHES( "SOB", type ) ||
	    MATCHES( "start of burst", type ) ) {
    ev->eventType = START_OF_BURST;
  } else if ( MATCHES( "EOB", type ) ||
	    MATCHES( "end of burst", type ) )
    ev->eventType = END_OF_BURST;
  else if ( MATCHES( "phy", type )) {
    ev->eventType = PHYSICS_EVENT;
    incrNbInBurst = TRUE;
  } else if ( MATCHES( "calib", type )) {
    ev->eventType = CALIBRATION_EVENT;
    incrNbInBurst = TRUE;
  } else if ( MATCHES( "sod", type )) {
    ev->eventType = START_OF_DATA;
  } else if ( MATCHES( "eod", type )) {
    ev->eventType = END_OF_DATA;
  } else if ( MATCHES( "SST", type )) {
    ev->eventType = SYSTEM_SOFTWARE_TRIGGER_EVENT;
  } else if ( MATCHES( "DST", type )) {
    ev->eventType = DETECTOR_SOFTWARE_TRIGGER_EVENT;
  } else if ( MATCHES( "event format error", type )) {
    ev->eventType = EVENT_FORMAT_ERROR;
    SET_SYSTEM_ATTRIBUTE( ev->eventTypeAttribute, ATTR_EVENT_ERROR );
  } else {
    printf( "%08lx Unknown event type: \"%s\"\n",
	    (unsigned long int)getpid(), type );
    return NULL;
  }
  ev->eventSize = size;
  ev->eventMagic = EVENT_MAGIC_NUMBER;
  ev->eventHeadSize = EVENT_HEAD_BASE_SIZE;
  ev->eventVersion = EVENT_CURRENT_VERSION;
  COPY_EVENT_ID( eventId, ev->eventId );
  EVENT_ID_SET_BUNCH_CROSSING( eventId,
			       EVENT_ID_GET_BUNCH_CROSSING( eventId ) + 1 );
  if ( EVENT_ID_GET_BUNCH_CROSSING( eventId ) == 0 )
    EVENT_ID_SET_ORBIT( eventId, EVENT_ID_GET_ORBIT( eventId ) + 1 );
  ZERO_TRIGGER_PATTERN( ev->eventTriggerPattern );
  RESET_ATTRIBUTES( ev->eventTypeAttribute );
  ev->eventLdcId = VOID_ID;
  ev->eventGdcId = VOID_ID;

  for ( i = 0; attributes[i] != 0; ) {
    while ( attributes[i] == '+' && attributes[i] != 0 ) i++;
    if ( attributes[i] != 0 ) {
      int attribute;
      if ( sscanf( &attributes[i], "%i", &attribute ) != 1 ) {
	printf( "%08lx Invalid attributes list: \"%s\"\n",
		(unsigned long int)getpid(), attributes );
	return NULL;
      }
      if ( attribute < 0 || attribute >= ALL_ATTRIBUTE_BITS ) {
	printf( "%08lx Invalid attributes list: \"%s\"\n",
		(unsigned long int)getpid(), attributes );
	return NULL;
      }
      SET_ANY_ATTRIBUTE( ev->eventTypeAttribute, attribute );
      while ( attributes[i] != '+' && attributes[i] != 0 ) i++;
    }
  }
  
  return ev;
}


int dumpEvent( void *rawData ) {
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)rawData;
  int  i;
  int *ptr;
  int  max;

  printf( " Type: " );
  switch ( ev->eventType ) {
  case START_OF_RUN :       printf( "StartOfRun" );      break;
  case END_OF_RUN :         printf( "EndOfRun" );        break;
  case START_OF_RUN_FILES : printf( "StartOfRunFiles" ); break;
  case END_OF_RUN_FILES :   printf( "EndOfRunFiles" );   break;
  case START_OF_BURST :     printf( "StartOfBurst" );    break;
  case END_OF_BURST :       printf( "EndOfBurst" );      break;
  case PHYSICS_EVENT :      printf( "PhysicsEvent" );    break;
  case CALIBRATION_EVENT :  printf( "CalibrEvent" );     break;
  case EVENT_FORMAT_ERROR : printf( "EventFormatErr" );  break;
  case START_OF_DATA :      printf( "StartOfData" );     break;
  case END_OF_DATA :        printf( "EndOfData" );       break;
  default :                 printf( "Unknown 0x%08x", ev->eventType );
  }
  
  printf( " Attributes:" );
  monitorPrintAttributes( ev, stdout );
  printf( "\n" );
  printf( " Size:%d", ev->eventSize );
  printf( " headLen:%d", ev->eventHeadSize );
  printf( " Magic:0x%08x", ev->eventMagic );
  printf( " Version:0x%08x", ev->eventVersion );
  printf( "\n" );

  printf( " EventId orbit:%d bunchCrossing:%d",
	  EVENT_ID_GET_ORBIT( ev->eventId ),
	  EVENT_ID_GET_BUNCH_CROSSING( ev->eventId ) );
  printf( " TriggerPattern:0x%08x-0x%08x",
	  ev->eventTriggerPattern[0],
	  ev->eventTriggerPattern[1] );
  printf( " ldcId:" );
  if ( ev->eventLdcId == VOID_ID ) printf( "VOID" );
  else printf( "%d", ev->eventLdcId );
  printf( " gdcId:" );
  if ( ev->eventLdcId == VOID_ID ) printf( "VOID" );
  else printf( "%d", ev->eventGdcId );
  printf( "\n" );

  if ( ev->eventSize > ev->eventHeadSize ) {
    printf( " Data: " );
    max = ( (ev->eventSize - ev->eventHeadSize) >> 2 ) - 1;
    ptr = (int *)((long)rawData + ev->eventHeadSize);
    for ( i = 0; i != 4; i++ ) {
      if ( i >= max ) break;
      printf( "%08x ", *(ptr++) );
    }
    if ( max != 0 ) printf( ".. " );
    ptr = (int *)((long)rawData + ev->eventSize - 4);
    ptr = (int *)ALIGN_PTR( ptr );
    printf( "%08x", *ptr );
  } else {
    printf( " No data available" );
  }
  printf( "\n" );
  
  return checkEvent( rawData );
}

int checkEvent( void *rawData ) {
  int ok = 0;
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)rawData;

  ok |= ev->eventSize > 0 ? 0 : 1;
  /* ok |= ((int)(ev->eventSize >> 2) << 2) == ev->eventSize ? 0 : 2; */
  ok |= ev->eventMagic == EVENT_MAGIC_NUMBER ? 0 : 4;
  ok |= ( ev->eventType >= EVENT_TYPE_MIN &&
	  ev->eventType <= EVENT_TYPE_MAX ) ? 0 : 8;
  ok |= ev->eventHeadSize == EVENT_HEAD_BASE_SIZE ? 0 : 0x10;
  ok |= ev->eventVersion == EVENT_CURRENT_VERSION ? 0 : 0x20;

  return ok;
}
