/* DATEV3 events handler definition
 * ================================
 *
 */

#ifndef __event_handler_h__
#define __event_handler_h__

#include <sys/uio.h>

int monitorStoreEvent( const struct eventHeaderStruct * const,
		       const void * const,
		       const struct iovec *,
		       const int );       /* Return: 0 => error, else ptr    */
int monitorDropEvent( int );              /* Return: 0 => OK, else => error  */
int monitorCopyEvent( void *,      	  /* Return: 0 => OK, else => error  */
		      int,
		      int );
#endif
