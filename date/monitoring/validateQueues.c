/* Facility to validate the DATEV3 monitoring queues support module
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "monitorInternals.h"

struct queuePtr theQueue;

int  numCalls;
int  numLoops;
int  numElements;
int  elementSize;
int *theBuffer;
int  bufferSize;

/* Init the memory chunks to be used later for the tests */
void initChunks() {
  int i;

  for ( i = 0; i != numElements; i++ ) {
    struct chunkStruct *p =
      (struct chunkStruct *)&theBuffer[ i*elementSize ];
    p->next = -1;
    p->prev = -1;
    p->size = elementSize * 4 - QUEUE_HEAD_LEN;
    p->data[0] = i;		/* Index, used for checking */
    p->data[1] = 0;		/* Flag, used for random tests */
  }
} /* initChunks */

/* Check a supposely full list */
void checkFullList() {
  numCalls++; assert( getNumOfFragments( &theQueue ) == numElements );
  numCalls++; assert( getAverageFragmentSize( &theQueue ) ==
		      elementSize * 4 - QUEUE_HEAD_LEN );
  numCalls++; assert( checkQueue( &theQueue ) == 0 );
} /* End of checkFullList */

/* Check a supposely empty list */
void checkEmptyList( ) {
  numCalls++; assert( getNumOfFragments( &theQueue ) == 0 );
  numCalls++; assert( getAverageFragmentSize( &theQueue ) == 0.0 );
  numCalls++; assert( checkQueue( &theQueue ) == 0 );
  numCalls++; assert( getNext( &theQueue ) == &theQueue );
  numCalls++; assert( getPrev( &theQueue ) == &theQueue );
} /* End of checkEmptyList */

/* Validation phase one: initialise an empty queue and test the basic
 * utilities */
void phaseOne() {
  numCalls++; assert( initQueue( &theQueue ) == 0 );
  checkEmptyList();
} /* End of phase one */

/* Phase two: some simple operations on the list: insert first/last,
 * extract first/last
 */
void phaseTwo() {
  int   i;
  void *ptr;
  
  numCalls++; assert( initQueue( &theQueue ) == 0 );

  /* Fill the queue adding chunks at the head of the queue in growing
   * order: this should result in a reverse ordered list
   */
  for ( i = 0; i != numElements; i++ ) {
    struct chunkStruct *p =
      (struct chunkStruct *)&theBuffer[ i*elementSize ];
    numCalls++; assert( insertFirst( &theQueue, p ) == 0 );
  }
  checkFullList();

  /* As we kept inserting at the head of the queue, we must have as first
   * element of the queue the last chunk of the array and as last element
   * of the queue the first chunk of the array. Is this true? We will check
   * this in two ways: getting the first/last elements and validating
   * their address and scanning the whole list and checking the element
   * number
   */
  numCalls++; assert( getNext( &theQueue ) ==
		      &theBuffer[ (numElements-1) * elementSize ] );
  numCalls++; assert( getPrev( &theQueue ) == theBuffer );
  for ( ptr = &theQueue, i = 0; i != numElements; i++ ) {
    numCalls++; assert( (ptr = getPrev( ptr )) != NULL );
    assert( ((struct chunkStruct *)ptr)->data[0] == i);
  }
  numCalls++; assert( getPrev( ptr ) == &theQueue );

  /* Now we extract from the list, first element first. We should get all
   * the chunks, in reversed order */
  for ( i = 0; i != numElements; i++ ) {
    numCalls++; assert( (ptr = extractFirst( &theQueue )) != NULL );
    assert( ((struct chunkStruct *)ptr)->data[0] == numElements - i - 1 );
  }
  numCalls++; assert( extractFirst( &theQueue ) == NULL );

  checkEmptyList( );

  /* We repeat the same exercise as above, only in reverse order */
  for ( i = 0; i != numElements; i++ ) {
    struct chunkStruct *p =
      (struct chunkStruct *)&theBuffer[ i*elementSize ];
    numCalls++; assert( insertLast( &theQueue, p ) == 0 );
  }
  
  checkFullList();

  /* Now the list elements should be in growing order. We repeat the same
   * checks as before, only the order must be reversed */
  numCalls++; assert( getNext( &theQueue ) == theBuffer );
  numCalls++; assert( getPrev( &theQueue ) ==
		      &theBuffer[ (numElements-1) * elementSize ] );
  for ( ptr = &theQueue, i = 0; i != numElements; i++ ) {
    numCalls++; assert( (ptr = getNext( (struct queuePtr *)ptr )) != NULL );
    assert( ((struct chunkStruct *)ptr)->data[0] == i );
  }
  numCalls++; assert( getNext( ptr ) == &theQueue );

  /* Now we empty the list again, last element first. We should get all the
   * chunks in reversed order (last element first) */
  for ( i = 0; i != numElements; i++ ) {
    numCalls++; assert( (ptr = extractLast( &theQueue )) != NULL );
    assert( ((struct chunkStruct *)ptr)->data[0] == numElements - i - 1 );
  }
  numCalls++; assert( extractLast( &theQueue ) == NULL );
  
  checkEmptyList();
} /* End of phase two */

  /* Phase three: insert after/before, extract
   */
void phaseThree() {
  int   i;
  void *ptr;
  
  numCalls++; assert( initQueue( &theQueue ) == 0 );

  /* First test: insert all elements after the head-of-the-queue */
  for ( i = 0; i != numElements; i++ ) {
    struct chunkStruct *p =
      (struct chunkStruct *)&theBuffer[ i*elementSize ];
    numCalls++; assert( insertBefore( &theQueue, &theQueue, p ) == 0 );
  }
  for ( ptr = &theQueue, i = 0; i != numElements; i++ ) {
    numCalls++; assert( (ptr = getNext( ptr )) != NULL );
    assert( ((struct chunkStruct *)ptr)->data[0] == i);
  }
  checkFullList();

  /* Now remove all elements in strict order */
  for ( i = 0; i != numElements; i++ ) {
    numCalls++; assert( extractElement( &theQueue,
					&theBuffer[ i*elementSize ] ) ==
			0 );
    numCalls++; assert( extractElement( &theQueue,
					&theBuffer[ i*elementSize ] ) !=
			0 );
  }
  assert( extractFirst( &theQueue ) == NULL );

  checkEmptyList();

  /* Second test: insert all elements in strict order */
  numCalls++; assert( insertFirst( &theQueue, theBuffer ) == 0 );
  for ( i = 1; i != numElements; i++ ) {
    struct chunkStruct *p =
      (struct chunkStruct *)&theBuffer[ i*elementSize ];
    struct chunkStruct *p1 =
      (struct chunkStruct *)&theBuffer[ (i - 1) * elementSize ];
    numCalls++; assert( insertAfter( &theQueue,
				     p1,
				     p ) == 0 );
  }
  for ( ptr = &theQueue, i = 0; i != numElements; i++ ) {
    numCalls++; assert( (ptr = getNext( ptr )) != NULL );
    assert( ((struct chunkStruct *)ptr)->data[0] == i);
  }
  checkFullList();

  /* Now remove all elements in reverse order */
  for ( i = numElements-1; i != -1; i-- ) {
    numCalls++; assert( extractElement( &theQueue,
					&theBuffer[ i*elementSize ] ) ==
			0 );
    numCalls++; assert( extractElement( &theQueue,
					&theBuffer[ i*elementSize ] ) !=
			0 );
  }
  assert( extractFirst( &theQueue ) == NULL );

  checkEmptyList();

  /* Add elements at random via "insertAfter" */
  numCalls++; assert( initQueue( &theQueue ) == 0 );
  numCalls++; assert( insertFirst( &theQueue, theBuffer ) == 0 );
  for ( i = 1; i != numElements; i++ ) {
    void *ptr = &theQueue;
    void *newEl = &theBuffer[ i * elementSize ];
    int   walk = ( rand() % (2*numElements) ) + 1;
    do {
      numCalls++; ptr = getNext( ptr ); 
    } while ( walk-- > 0 );
    numCalls++; assert( insertAfter( &theQueue, ptr, newEl ) == 0 );
  }
  checkFullList();
  
  /* Same via "insertBefore", but this time we also
   * extract the elements at random
   */
  numCalls++; assert( initQueue( &theQueue ) == 0 );
  numCalls++; assert( insertFirst( &theQueue, theBuffer ) == 0 );
  for ( i = 1; i != numElements; i++ ) {
    void *ptr = &theQueue;
    void *newEl = &theBuffer[ i * elementSize ];
    int   walk = ( rand() % (2*numElements) ) + 1;
    do {
      numCalls++; ptr = getNext( ptr );
    } while ( walk-- > 0 );
    numCalls++; assert( insertBefore( &theQueue, ptr, newEl ) == 0 );
  }
  checkFullList();

  for ( i = 0; i != numElements; i++ ) {
    int walk = ( rand() % (2*numElements) ) + 1;
    ptr = &theQueue;
    do {
      numCalls++; ptr = getNext( ptr );
    } while ( walk-- > 0 );
    if ( ptr == &theQueue ) {
      numCalls++; assert( (ptr = getNext( ptr )) != &theQueue );
    }
    numCalls++; assert( extractElement( &theQueue, ptr ) == 0 );
  }
  assert( extractFirst( &theQueue ) == NULL );
  checkEmptyList();
} /* End of phase three */

/* Phase four: check the ordered insertion + chunk gathering */
void phaseFour() {
  int i;
  struct chunkStruct *ptr;
    
  numCalls++; assert( initQueue( &theQueue ) == 0 );
  for ( i = 0; i != numElements; i++ ) {
    int nEl = rand();
    do {
      nEl = (nEl + 1) % numElements;
      ptr = (struct chunkStruct *)(&theBuffer[ nEl * elementSize ]);
    } while ( ptr->data[1] == 1 );
    ptr->data[1] = 1;
    numCalls++; assert( insertOrdered( &theQueue, ptr ) == 0 );
  }
  checkFullList();
  for ( i = 0, numCalls++, ptr = getNext( &theQueue );
	ptr != (void *)&theQueue;
	numCalls++, ptr = getNext( (struct queuePtr *)ptr ), i++) {
    assert( ptr->data[1] == 1 ); /* All chunks must have been used */
    assert( ptr->data[0] == i ); /* Chunks must be in good order   */
  }
    
  numCalls++; gatherOrderedQueue( &theQueue );
  numCalls++; assert( getNumOfFragments( &theQueue ) == 1 );
  numCalls++; assert( getAverageFragmentSize( &theQueue ) ==
		      bufferSize - QUEUE_HEAD_LEN );
    
  initChunks();			/* Re-initialise the chunks */
} /* End of phase four */

/* Phase five: check the chunk gathering of unordered lists */
void phaseFive() {
  int i;
  struct chunkStruct *ptr;
    
  numCalls++; assert( initQueue( &theQueue ) == 0 );
  for ( i = 0; i != numElements; i++ ) {
    int nEl = rand();
    do {
      nEl = (nEl + 1) % numElements;
      ptr = (struct chunkStruct *)(&theBuffer[ nEl * elementSize ]);
    } while ( ptr->data[1] == 1 );
    ptr->data[1] = 1;
    numCalls++; assert( insertFirst( &theQueue, ptr ) == 0 );
  }
  checkFullList();
  for ( i = 0, numCalls++, ptr = getNext( &theQueue );
	ptr != (void *)&theQueue;
	numCalls++, ptr = getNext( (struct queuePtr *)ptr ), i++) {
    assert( ptr->data[1] == 1 ); /* All chunks must have been used */
  }
    
  numCalls++; gatherQueue( &theQueue );
  numCalls++; assert( getNumOfFragments( &theQueue ) == 1 );
  numCalls++; assert( getAverageFragmentSize( &theQueue ) ==
		      bufferSize - QUEUE_HEAD_LEN );
    
  initChunks();			/* Re-initialise the chunks */
} /* End of phase five */

/* Phase six: check the insertion with chunk gathering */
void phaseSix() {
  int i;
  struct chunkStruct *ptr;
    
  numCalls++; assert( initQueue( &theQueue ) == 0 );
  for ( i = 0; i != numElements; i++ ) {
    int nEl = rand();
    do {
      nEl = (nEl + 1) % numElements;
      ptr = (struct chunkStruct *)(&theBuffer[ nEl * elementSize ]);
    } while ( ptr->data[1] == 1 );
    ptr->data[1] = 1;
    numCalls++; assert( insertAndGather( &theQueue, ptr ) == 0 );
  }
  numCalls++; assert( getNumOfFragments( &theQueue ) == 1 );
  numCalls++; assert( getAverageFragmentSize( &theQueue ) ==
		      bufferSize - QUEUE_HEAD_LEN );
    
  initChunks();			/* Re-initialise the chunks */
} /* End of phase six */

void usage( char **argv ) {
  fprintf( stderr, "Usage: %s [numLoops [numElements [elementsSize]]]\n",
	   argv[0] );
  exit( 1 );
} /* End of usage */

void initVars( int argc, char **argv ) {
  srand( 1 );			/* Init the random number generator */

  if ( argc > 4 ) usage( argv );

  numLoops = 1;
  if ( argc > 1 )
    if ( sscanf(argv[1], "%d", &numLoops ) != 1 ) usage( argv );

  numElements = 100;
  if ( argc > 2 )
    if ( sscanf( argv[2], "%d", &numElements ) != 1 ) usage( argv );

  elementSize = 1000;
  if ( argc > 3 )
    if ( sscanf( argv[3], "%d", &elementSize ) != 1 ) usage( argv );

  printf( "Test parameters: %d random loops, %d elements %d bytes each\n",
	  numLoops,
	  numElements,
	  elementSize );

  bufferSize = numElements * elementSize * 4;
  assert( (theBuffer = (int *)malloc( (size_t)bufferSize )) != NULL );

  numCalls = 0;
} /* End of initVars */

int main( int argc, char **argv ) {
  int nLoops;
  
  printf( "DATEV3 monitor queues validation %s starting\n",
	  queueSupportGetVid() );

  initVars( argc, argv );

  for ( nLoops = 0; nLoops++ != numLoops; ) {
    printf( "\r%d", nLoops ); fflush( stdout );
    
    initChunks();
    
    phaseOne();
    phaseTwo();
    phaseThree();
    phaseFour();
    phaseFive();
    phaseSix();
  }
  
  printf( "\nDATEV3 monitor queues validation normal completion after %d library calls\n",
	  numCalls );
  exit( 0 );
} /* End of main */
