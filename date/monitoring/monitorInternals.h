/* DATE monitor V3.xx internal common definitions
 * ==============================================
 *
 * In this module you will find all the definitions that are common to
 * all "internal" modules of the monitoring packages, including the
 * required include statements.
 *
 * Declarations that must be shared with producers and consumers must
 * go in the file monitor.h
 *
 * If the symbol NO_LOGGING is define prior to include this file, then
 * the logging facilities will not be included in the object file. This
 * is useful for modules that do not need the "modified" monitoring
 * logging facilities (avoids compilation warnings).
 */

#ifndef __monitor_internals_h__
#define __monitor_internals_h__

#include <sys/types.h>
#include <time.h>
#include <stdint.h>

#include "event.h"


/* The DATE site configuration file */
#define DEFAULT_CONFIGURATION_FILE "${DATE_SITE_CONFIG}/monitoring.config"


/* The default local monitor buffer configuration & key file (DATE hosts) */
#define DEFAULT_MONITOR_BUFFER "${DATE_SITE}/${DATE_HOSTNAME}/monitoring.config:"


/* The configuration file specific for mpDaemon */
#define MONITOR_MPDAEMON_CONFIGURATION "/etc/mpDaemon.config"


/* The default wait/nowait state */
#define DEFAULT_WAIT_STATE TRUE


/* The default maximum number of clients allowed */
#define DEFAULT_MAX_CLIENTS 10


/* The default maximum number of events allowed */
#ifdef SunOS
#define DEFAULT_MAX_EVENTS 10
#else
#define DEFAULT_MAX_EVENTS 100
#endif


/* The default maximum age (in seconds) for events recorded in the buffer */
#define DEFAULT_MAX_AGE 30


/* The default (typical) size of each event. This value is used to
 * compute the size of the events data buffer (unless explicitely
 * overridden via the eventBufferSize variable). */
#ifdef SunOS
#define DEFAULT_EVENT_SIZE ( 1024 * 2 )
#else
#define DEFAULT_EVENT_SIZE ( 1024 * 128 )
#endif


/* The default size of the events data buffer. If the value is > 0, this
 * value will be used to size the events data buffer. If the value is <= 0
 * the data buffer size will be sized using the maxEvents and the eventSize
 * values. */
#define DEFAULT_EVENT_BUFFER_SIZE ( -1 )


/* The minimum size of the data portion (payload) for queue elements */
#define MINIMUM_QUEUE_ELEMENT_SIZE 100


/* The default monitor policy. This policy is used in case a user policy
 * table does not cover all event types; this is the policy used for the
 * "uncovered" event types. */
#define DEFAULT_MONITOR_POLICY no


/* The minimum time (in seconds) between checks on the status of the
 * registered clients. The higher the time, the less the overhead but
 * the longer it takes to detect abnormal terminations. */
#define TIME_BETWEEN_CHECKS 5


/* The number of times we try to re-open a network connection in case of
 * link failure */
#define NETWORK_MAX_RETRIES 5


/* The log file used for local logging. If the symbol DATE_SITE_LOGS is
 * defined, the log file will reside there, otherwise a default location
 * (/tmp) will be choosen */
#define MONITOR_LOG_FILE PACKAGE_NAME


/* The environment variable that can be used to set an initial log level */
#define MONITOR_LOG_ENV_VAR "MON_LOG"


/* The maximum number of characters for the mp name */
#define MP_MAX_CHARS 256


/* Macros for log strings handling */
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)


#ifndef NO_LOGGING
/* The following declarations are used only if logging features are
 * not explicitly disabled via the NO_LOGGING flag (to avoid compilation
 * warnings comcerning variables declared and never used).
 */

#include "infoLogger.h"

extern int monitoringLogLevel;	/* The logging level */

#define DEFAULT_LOG_LEVEL   0   /* Default logging level: no log */

#define LOG_NORMAL    if ( monitoringLogLevel >= LOG_NORMAL_TH )
#define LOG_DETAILED  if ( monitoringLogLevel >= LOG_DETAILED_TH )
#define LOG_DEVELOPER if ( monitoringLogLevel >= LOG_DEBUG_TH )

#define MON_LOG "monitoring"
#endif

#ifndef TRUE
 #define TRUE (0 == 0)
#endif
#ifndef FALSE
 #define FALSE (0 == 1)
#endif

#ifndef MIN
# define MIN( a, b ) (((a)<(b))?(a):(b))
#endif
#ifndef MAX
# define MAX( a, b ) (((a)>(b))?(a):(b))
#endif

/* The name of the environment variable we use to get the socket number */
#define MONITOR_SOCKET_ENVIRONMENT "DATE_SOCKET_MON"


/* The default value for the socket number (can be overridden using the
 * above environment variable */
#define MONITOR_DEFAULT_SOCKET 6601


/* The bias for the top part of a byte counter */
#define MONITOR_TOP_COUNTER 1000000000


/* The allowed range of event types (for tables) */
#define EVENT_TYPE_RANGE (EVENT_TYPE_MAX-EVENT_TYPE_MIN+1)


/* The macro used to align pointers/sizes */
#define ALIGN_PTR( p ) \
  (((((datePointer)(p))%4)==0)? \
    (datePointer)(p):           \
    ((datePointer)(p))-(((datePointer)(p))%4)+4)


/* The arithmetic on pointers, used to calculate relative pointers */
#define ADD( ptr, d ) \
  (struct queuePtr *)((datePointer)(ptr) + (datePointer)(d))
#define SUBTRACT( ptr1, ptr2 ) \
  (datePointer)((datePointer)(ptr1) - (datePointer)(ptr2))


/* The macros used for dynamic arrays handling */
#define CLIENT( id ) \
  ((monitorClientDescriptor*)(monitorClients + \
                              sizeof(monitorClientDescriptor) * id ))
#define EVENT( id ) \
  ((monitorEventDescriptor*)(monitorEvents + \
                             sizeof(monitorEventDescriptor) * id ))


/* Our monitoring host type structure */
typedef struct {
  void *next;
  char *hostname;
} monitoringHostDescriptor;


/* Our monitor type record structure */
typedef struct {
  void *next;
  char *eventType;
  char *monitorType;
  char *monitorAttributes;
} monitorRecordPolicy;


/* The monitor policy controller */
typedef enum { all = 1, yes = 2, no = 3 } monitorType;


/* The client descriptor record */
typedef struct {
  int   inUse;	            /* TRUE if record in use, FALSE if record free   */
  pid_t pid;	            /* The PID of the client                         */
  int   registrationNumber; /* Unique registration number, used to detect
			     * re-use of a record due to forced sign outs    */
  int   lastEventNumber;    /* The number of the last monitored event        */
  int   waitingForNewEvents;/* TRUE if client is waiting for new events      */
  char  mpId[MP_MAX_CHARS]; /* The ID of the monitoring program              */
  monitorType		    /* The policy for each event type                */
         monitorPolicy[EVENT_TYPE_RANGE];
  eventTypeAttributeType
         monitorAttributes[EVENT_TYPE_RANGE];
  int    monitorAttributesAnd[EVENT_TYPE_RANGE];
  int    eventsRead;        /* The number of events read by this client      */
  long32 bytesRead[ 2 ];    /* The number of bytes read by this client       */
} monitorClientDescriptor;


/* The event descriptor record */
typedef struct {
  int           eventNumber; /* The ID of this event                         */
  eventTypeType eventType;   /* The event type of this event                 */
  int           numMust;     /* How many "must" monitor clients have not yet
			      * used this event                              */
  int           numYes;	     /* How many "monitor" clients have not yet used
			      * this event                                   */
  time_t        birthTime;   /* When this event has been created             */
  int           monitored;   /* TRUE: at least 1 client monitored the event  */
  datePointer   eventData;   /* The pointer to the event data                */
} monitorEventDescriptor;


/* The various include files */
#include "monitor.h"		/* Public monitor declarations       */
#include "queues.h"		/* Queue handler declarattions       */
#include "attributesHandler.h"  /* Events attributes handler         */
#include "fileHandling.h"	/* File handling declarations        */
#include "monitorParams.h"	/* Monitor parameters handler        */
#include "monitorBuffer.h"	/* Monitor buffer handler            */
#include "shellInterface.h"	/* The interface to the system shell */
#include "clientRegistry.h"	/* The clients registry              */
#include "clientInterface.h"	/* The client interface              */
#include "reservationHandler.h" /* The reservations handler          */
#include "eventHandler.h"       /* The event buffer handler          */
#include "fileHandler.h"        /* The file handler                  */
#include "mpDaemon.h"		/* The network daemon library        */

#endif
