/* Events attributes handler definitions
 * =====================================
 */
#ifndef __attributes_handler_h__
#define __attributes_handler_h__

int  monitorValidateAttribute( int );
void monitorAssertAttributes( void * );
void monitorAssertAttributesMask( eventTypeAttributeType* );
int  monitorSelectedByAttributes( void *, eventTypeAttributeType*, int );
void monitorPrintAttributes( void *, FILE * );
void monitorPrintAttributesMask( eventTypeAttributeType*, FILE * );
void monitorDumpAttributes( void *, char *, int );
void monitorDumpAttributesMask( eventTypeAttributeType*, char *, int );
#endif
