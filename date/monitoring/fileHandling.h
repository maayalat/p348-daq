/* File handling definitions
 * =========================
 */
#ifndef __file_handling_h__
#define __file_handling_h__

char *monitorFileHandlingGetVid();
int   monitorTestFileReadable( char* );
int   monitorParseConfigFile( char* );
#endif
