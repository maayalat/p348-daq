/* Monitor client interface definitions
 * ====================================
 */
#ifndef __shell_interface__h__
#define __shell_interface__h__

/* Translate all SHELL symbols eventually contained in a string */
int monitorTranslate( char *string, int length );

/* Get the local host name */
int monitorGetHostname( char *string, int length );

/* Translate as much as possible a host name, adding (if necessary) the
 * domain name and resolving all name aliases */
int monitorTranslateAddress( char *name, int length, char *in );
#endif

