/* Monitor V 3.xx buffer handler module
 * =====================================
 *
 * Module history:
 *  1.00  7-Jul-98 RD    Created
 *  1.01 24-Aug-98 RD    First public release
 *  1.02 31-Aug-98 RD    Timeout on wait for free space added
 *  1.03 11-Sep-98 RD    Mods for Linux & OSF1 added
 *  1.04 19-Nov-98 RD    Counters for events & bytes added
 *  1.05 18-May-99 RD    Problem in WaitForNextEvent fixed
 *                       Bug with NumLocks counter handling fixed
 *  1.06 19-May-99 RD	 Lock/Unlock buffer protected against incoming signals
 *  1.07 30-Nov-99 RD	 Ported on Linux 2.2.12-20
 *  1.08 30-Apr-03 RD+OC Function to detach from shared memory added
 *
 * Compilation symbols:
 *   NEED_SEMUN		Define if semaphore operations (semctl) need semun as
 *			value parameter (seen on some versions of Linux)
 */
#define VID "1.08"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

#include "monitorInternals.h"

#define DESCRIPTION "DATE V3 monitor buffer handler"
#ifdef AIX
static
#endif
char monitorBufferIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                            """ """ VID """ """ \
                            """ compiled """ __DATE__ """ """ __TIME__;


/* The way we pass the parameter to "semctl" */
#ifdef SunOS
#define SEMCTL_PAR( value ) &value
#else
#define SEMCTL_PAR( value ) value
#endif


/* The protection used to create a public accessible segment */
#define PROTECTION_PUBLIC \
   ( S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH )


/* The shared memory identifier used for shared memory segment control */
int monitorSharedMemoryId;


/* A descriptor of the parameters of the monitor buffer */
struct shmid_ds monitorBufferDescriptor;


/* The pointers to the monitor buffer and its elements */
void *monitorBuffer       = NULL; /* The anonymous pointer to the buffer */

  /* The following parameters can be used either to validate the configuration
   * of the monitoring system or to connect to a monitor buffer without
   * knowledge on its structure */
int *mbMonitorBufferSize = NULL; /* The size of the complete monitor buffer  */
int *mbMaxClients        = NULL; /* The max number of clients                */
int *mbMaxEvents         = NULL; /* The max number of events available       */
int *mbEventSize         = NULL; /* The average size of one event            */
int *mbEventBufferSize   = NULL; /* The size of the events buffer            */

  /* These are counters used for the control of the monitoring scheme        */
int *mbNumClients        = NULL; /* Number of clients currently registered   */

  /* The indexes used for the events reservation scheme */
int *mbOldestEvent       = NULL; /* Pointer to the oldest available event    */
int *mbFirstFreeEvent    = NULL; /* Pointer to the first free event descrip. */
int *mbCurrentEventNumber= NULL; /* Number of the current event              */

  /* Counters used for statistics/error identification */
int *mbNumSignIn         = NULL;
int *mbNumSignOut        = NULL;
int *mbForcedExits       = NULL;
int *mbTooManyClients    = NULL;
int *mbEventsInjected    = NULL;
long32 *mbBytesInjected  = NULL;

  /* The buffer describing the clients currently registered */
void *monitorClients;

  /* The buffer describing the events currently available */
void *monitorEvents;

/* The events buffer */
struct queuePtr *mbFreeEvents  = NULL;
void *monitorEventsData = NULL;

/* The first free location in the monitor buffer, used to compute the
 * various pointers and to find out how big the monitor buffer should be */
void *monitorFirstFree = (void*)(-1);

/* The semaphores used to control the monitor buffer */
int monitorControlSemaphores = -1;


/* The external variables defining the monitor buffer and its elements */
extern int maxClients;
extern int maxEvents;
extern int eventSize;
extern int eventBufferSize;


/* The monitor buffer lock counter, used for nested lock/unlock */
int monitorNumLocks = 0;


/* Flag used to signal that the wait for new events was aborted */
static int waitWasAborted = FALSE;


/* Flag used to signal that the lock was released during an inject operation */
int monitorLockReleased;


/* Flag used to signal that our signal handler has been installed */
static int sigHandlerInstalled = FALSE;


/* Get the version ID of this module */
char *monitorBufferGetVid() {
  return VID;
} /* End of monitorBufferGetVid */


/* Add a given amount to a byte counter */
void monitorAddToCounter( int size, long32 *counter ) {
  while ( size > MONITOR_TOP_COUNTER ) {
    monitorAddToCounter( MONITOR_TOP_COUNTER, counter );
    size -= MONITOR_TOP_COUNTER;
  }
  counter[1] += size;
  if ( counter[1] >= MONITOR_TOP_COUNTER ) {
    counter[1] -= MONITOR_TOP_COUNTER;
    counter[0] += 1;
  }
} /* End of monitorAddToCounter */


/* Initialise the monitor buffer data pointers */
void monitorInitMonitorBufferPointers() {
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorInitMonitorBufferPointers monitorBuffer:x%08lx",
	     (long)monitorBuffer );
    INFO_TO( MON_LOG, msg );
  }

  monitorFirstFree = (void*)monitorBuffer;
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbMonitorBufferSize = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbMaxClients        = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbMaxEvents         = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbEventSize         = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbEventBufferSize   = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbNumClients        = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbOldestEvent       = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbFirstFreeEvent    = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbCurrentEventNumber= (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );
  
  mbNumSignIn         = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbNumSignOut        = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbForcedExits       = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbTooManyClients    = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbEventsInjected    = (int*)monitorFirstFree;
  monitorFirstFree   += sizeof( int );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbBytesInjected     = (long32*)monitorFirstFree;
  monitorFirstFree   += sizeof( long32[2] );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  monitorClients      = (void*)monitorFirstFree;
  monitorFirstFree   += maxClients * sizeof( monitorClientDescriptor );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  monitorEvents       = (void*)monitorFirstFree;
  monitorFirstFree   += maxEvents * sizeof( monitorEventDescriptor );
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  mbFreeEvents        = (struct queuePtr*)monitorFirstFree;
  monitorFirstFree   += QUEUE_HEAD_LEN;
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  monitorEventsData   = (void*)monitorFirstFree;
  monitorFirstFree   += eventBufferSize > 0 ?
                          eventBufferSize :
                          maxEvents * eventSize;
  monitorFirstFree    = (void*)ALIGN_PTR( monitorFirstFree );

  LOG_DEVELOPER {
    char msg[ 50*100 ];
    snprintf( SP(msg),
	      "monitorInitMonitorBufferPointers Pointers initialised.\n\
   mbMonitorBuffer:x%08lx\n\
   ----------------------------------------\n\
   mbMonitorBufferSize:x%08lx\n\
   ----------------------------------------\n\
   mbMaxClients:x%08lx\n\
   mbMaxEvents:x%08lx\n\
   mbEventSize:x%08lx\n\
   mbEventBufferSize:x%08lx\n\
   ----------------------------------------\n\
   mbNumClients:x%08lx\n\
   ----------------------------------------\n\
   mbOldestEvent:x%08lx\n\
   mbFirstFreeEvent:x%08lx\n\
   mbCurrentEventNumber:x%08lx\n\
   ----------------------------------------\n\
   mbNumSignIn:x%08lx\n\
   mbNumSignOut:x%08lx\n\
   mbForcedExits:x%08lx\n\
   mbTooManyClients:x%08lx\n\
   mbEventsInjected:x%08lx\n\
   mbBytesInjected:x%08lx\n\
   ----------------------------------------\n\
   monitorClients:x%08lx\n\
   ----------------------------------------\n\
   monitorEvents:x%08lx\n\
   ----------------------------------------\n\
   mbFreeEvents:x%08lx\n\
   monitorEventsData:x%08lx\n\
   ----------------------------------------\n\
   --- monitorFirstFree:x%08lx (delta:x%08lx, %ld)\n\
   ----------------------------------------",
	     (long)monitorBuffer,
	     (long)mbMonitorBufferSize,
	     (long)mbMaxClients,
	     (long)mbMaxEvents,
	     (long)mbEventSize,
	     (long)mbEventBufferSize,
	     (long)mbNumClients,
	     (long)mbOldestEvent,
	     (long)mbFirstFreeEvent,
	     (long)mbCurrentEventNumber,
	     (long)mbNumSignIn,
	     (long)mbNumSignOut,
	     (long)mbForcedExits,
	     (long)mbTooManyClients,
	     (long)mbEventsInjected,
	     (long)mbBytesInjected,
	     (long)monitorClients,
	     (long)monitorEvents,
	     (long)mbFreeEvents,
	     (long)monitorEventsData,
	     (long)monitorFirstFree,
	     (long)monitorFirstFree-(long)monitorBuffer,
	     (long)monitorFirstFree-(long)monitorBuffer );
    INFO_TO( MON_LOG, msg );
  }
} /* End of monitorInitMonitorBufferPointers */


/* Return the amount of memory needed for the monitor buffer */
int monitorGetMonitorBufferSize() {
  int value;
  
  if ( (long)monitorFirstFree == (-1) ) {
    /* Initialise the pointers starting from a zero value. This will return
     * us the amount of memory wee need for the monitor buffer. The pointers
     * will all be useless, as the buffer has not been allocated yet.
     */
    LOG_DEVELOPER
      INFO_TO( MON_LOG,
	       "monitorGetMonitorBufferSize: \
calling monitorInitMonitorBufferPointers" );
    monitorBuffer = 0;
    monitorInitMonitorBufferPointers();
  }
  value = (long)monitorFirstFree - (long)monitorBuffer;
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorGetMonitorBufferSize Buffer size:(%d x%08x)",
	      value, value );
    INFO_TO( MON_LOG, msg );
  }
  return value;
} /* End of monitorGetMonitorBufferSize */


/* Initialise the monitor buffer data structures */
void monitorInitMonitorBuffer() {
  int i;
  struct queuePtr *first;
  
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorInitMonitorBuffer: start" );
  
  *mbMonitorBufferSize = monitorGetMonitorBufferSize();
  *mbMaxClients = maxClients;
  *mbMaxEvents = maxEvents;
  *mbEventSize = eventSize;
  *mbEventBufferSize = eventBufferSize > 0 ?
    eventBufferSize :
    (*mbMaxEvents) * (*mbEventSize);
  
  *mbNumClients = 0;
  
  *mbOldestEvent = -1;
  *mbFirstFreeEvent = -1;
  *mbCurrentEventNumber = -1;
  
  *mbNumSignIn = 0;
  *mbNumSignOut = 0;
  *mbForcedExits = 0;
  *mbTooManyClients = 0;
  *mbEventsInjected = 0;
  mbBytesInjected[0] = 0;
  mbBytesInjected[1] = 0;

  for ( i = 0; i != maxClients; i++ ) {
    monitorClientDescriptor *ptr = CLIENT( i );
    ptr->inUse = FALSE;
    ptr->registrationNumber = 0;
  }

  /* The initialisation of monitorEvents is NOT necessary */
  for ( i = 0; i != maxEvents; i++ ) {
    monitorEventDescriptor *ptr = EVENT( i );
    ptr->eventNumber = -1;
    ptr->numMust = -1;
    ptr->numYes = -1;
    ptr->eventData = -1;
  }

  /* Initialise the events data buffer as for one unique big event */
  first = (struct queuePtr *)monitorEventsData;
  first->size = *mbEventBufferSize - QUEUE_HEAD_LEN;

  /* Initialise the queue and insert the whole buffer as first event */
  initQueue( mbFreeEvents );
  insertFirst( mbFreeEvents, first );

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorInitMonitorBuffer: completed" );
} /* End of monitorInitMonitorBuffer */


/* Get the pointer and initialise the control semaphores */
int monitorInitSemaphores( int key ) {
  int status = 0;
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorInitSemaphores(%d)", key );
    INFO_TO( MON_LOG, msg );
  }
  
  monitorControlSemaphores =
    semget( key, MAX_SEMAPHORES, PROTECTION_PUBLIC );
  if ( monitorControlSemaphores == (-1) ) {
    if ( errno == ENOENT ) {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorInitSemaphores: creating semaphores" );
      monitorControlSemaphores =
	semget( key, MAX_SEMAPHORES, PROTECTION_PUBLIC | IPC_CREAT );
      if ( monitorControlSemaphores == (-1) ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorInitSemaphores Failed to create \
control semaphores for key:%d errno:%d (%s)",
		    key, errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	status = MON_ERR_SYS_ERROR;
      } else {
	LOG_DEVELOPER
	  INFO_TO( MON_LOG,
		   "monitorInitSemaphores: control semaphores created OK" );
      }
    } else {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		 "monitorInitSemaphores Failed to attach to \
control semaphores key:%d errno:%d (%s)",
		 key, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
  }

  if ( status == 0 ) {
    if ( monitorBufferDescriptor.shm_nattch == 1 ) {
      int semNum;
      
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorInitSemaphores: initialising control semaphores" );

      for ( semNum = 0; semNum != MAX_SEMAPHORES; semNum++ ) {
	int value;
#ifdef NEED_SEMUN
	union semun valueUnion;
#endif

	if ( semNum == LOCK_SEMAPHORE ) value = 1;
	else if ( semNum == FREE_SPACE_SEMAPHORE ) value = 0;
	else if ( semNum == NEW_EVENTS_SEMAPHORE ) value = 0;
	else {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorInitSemaphores Don't know how \
to set semaphore %d",
		      semNum );
	    ERROR_TO( MON_LOG, msg );
	  }
	  status = MON_ERR_INTERNAL;
	  value = -1;
	}
#ifdef NEED_SEMUN
	valueUnion.val = value;
#endif
	if ( semctl( monitorControlSemaphores,
		     semNum,
		     SETVAL,
#ifdef NEED_SEMUN
		     SEMCTL_PAR( valueUnion )
#else
		     SEMCTL_PAR( value )
#endif
		     ) != 0 ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorInitSemaphores Cannot reset semaphore %d \
key:%d SETVAL:%d errno:%d (%s)",
		      semNum, key, SETVAL, errno, strerror(errno) );
	    ERROR_TO( MON_LOG, msg );
	  }
	  status = MON_ERR_SYS_ERROR;
	} else {
	  int newValue;

	  if ( ( newValue = semctl( monitorControlSemaphores,
				    semNum,
				    GETVAL,
#ifdef NEED_SEMUN
				    valueUnion
#else
				    0
#endif
				    ) ) < 0 ) {
	    LOG_NORMAL {
	      char msg[ 1024 ];
	      snprintf( SP(msg),
			"monitorInitSemaphores Cannot read semaphore %d \
key:%d GETVAL:%d errno:%d (%s)",
			semNum, key, GETVAL, errno, strerror(errno) );
	      ERROR_TO( MON_LOG, msg );
	    }
	    status = MON_ERR_SYS_ERROR;
	  } else {
#ifdef NEED_SEMUN
	    newValue = valueUnion.val;
#endif
	    if ( value != newValue ) {
	      LOG_NORMAL {
		char msg[ 1024 ];
		snprintf( SP(msg),
			  "monitorInitSemaphores Semaphore %d validation \
failed expected:%d got:%d",
			  semNum,
			  value,
			  newValue );
		ERROR_TO( MON_LOG, msg );
	      }
	      status = MON_ERR_SYS_ERROR;
	    } else {
	      LOG_DEVELOPER {
		char msg[ 1024 ];
		snprintf( SP(msg),
			  "monitorInitSemaphores Semaphore %d validated OK",
			  semNum );
		INFO_TO( MON_LOG, msg );
	      }
	    }
	  }
	}
      }
      if ( status == 0 ) {
	LOG_DEVELOPER
	  INFO_TO( MON_LOG,
		   "monitorInitSemaphores: control semaphores reset OK" );
      }
    } else {
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorInitSemaphores: cannot reset semaphores (we are \
not alone)" );
    }
  }
  
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorInitSemaphores Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorInitSemaphores */


/* Lock the monitor buffer.
 *
 * Return:
 *   0    => ok
 *   else => error
 */
struct sembuf grabSemaphoreOp[1] = { { LOCK_SEMAPHORE, -1, SEM_UNDO } };
int monitorLockBuffer() {
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorLockBuffer Locking buffer currentLockLevel:%d",
	      monitorNumLocks );
    INFO_TO( MON_LOG, msg );
  }
  if ( monitorNumLocks == 0 ) {
    int status;
    
    do {
      status = semop( monitorControlSemaphores, grabSemaphoreOp, 1 );
    } while ( status != 0 && errno == EINTR );
    if ( status != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorLockBuffer Cannot semop to lock buffer \
errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_SYS_ERROR;
    }
    LOG_DEVELOPER INFO_TO( MON_LOG, "monitorLockBuffer: buffer locked OK" );
  }
  monitorNumLocks++;
  return 0;
} /* End of monitorLockBuffer */


/* Unlock the monitor buffer.
 *
 * Return:
 *   0    => ok
 *   else => error
 */
struct sembuf ungrabSemaphoreOp[1] = { { LOCK_SEMAPHORE, 1, SEM_UNDO } };
int monitorUnlockBuffer() {
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorUnlockBuffer unlocking buffer currentLockLevel:%d",
	      monitorNumLocks );
    INFO_TO( MON_LOG, msg );
  }
  if ( monitorNumLocks == 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorUnlockBuffer: cannot unlock..." );
    return MON_ERR_INTERNAL;
  }
  if ( monitorNumLocks == 1 ) {
    int status;

    do {
      status = semop( monitorControlSemaphores, ungrabSemaphoreOp, 1 );
    } while ( status != 0 && errno == EINTR );
    if ( status != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorUnlockBuffer cannot semop/ungrab errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_SYS_ERROR;
    }
    monitorLockReleased = TRUE;
    LOG_DEVELOPER
      INFO_TO( MON_LOG, "monitorUnlockBuffer: buffer unlocked OK" );
  }
  monitorNumLocks--;
  return 0;
} /* End of monitorUnlockBuffer */


/* Timeout handler, used to catch alarams during wait for free space */
void monitorTimeoutHandler( int sigNum ) {
  sigHandlerInstalled = TRUE;
  signal( SIGALRM, monitorTimeoutHandler );
} /* End of monitorTimeoutHandler */


struct sembuf waitForSpaceOp[1] = { { FREE_SPACE_SEMAPHORE, 0, 0 } };
/* Wait for free space. This entry is supposed to be called with the
 * buffer locked ONCE (it must be released) and will return in the
 * same state (with the buffer locked once).
 *
 * Return:
 *   0    => ok
 *   else => error
 */
int monitorWaitForFreeSpace() {
  int status = 0;
#ifndef NEED_SEMUN
  int value = 1;
#else
  union semun value;

  value.val = 1;
#endif

  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorWaitForFreeSpace: starting" );

  if ( monitorNumLocks != 1 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorWaitForFreeSpace Cannot unlock now count:%d",
		monitorNumLocks );
      ERROR_TO( MON_LOG, msg );
    }
    status = MON_ERR_INTERNAL;
  }

  if ( status == 0 ) {
    if ( !sigHandlerInstalled ) monitorTimeoutHandler( 0 );
  
    if ( semctl( monitorControlSemaphores,
		 FREE_SPACE_SEMAPHORE, SETVAL, SEMCTL_PAR( value ) ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorWaitForFreeSpace failed to set semaphore \
errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
  }

  if ( status == 0 ) {
    if ( ( status = monitorUnlockBuffer() ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorWaitForFreeSpace: failed to unlock monitor buffer" );
    }
  }
  if ( status == 0 ) {
    alarm( TIME_BETWEEN_CHECKS );
    status = semop( monitorControlSemaphores, waitForSpaceOp, 1 );
    alarm( 0 );
    if ( status != 0 && errno == EINTR ) {
      if ( ( status = monitorValidateRegistrations() ) != 0 ) {
	LOG_NORMAL
	  ERROR_TO( MON_LOG,
		    "monitorWaitForFreeSpace: \
monitorValidateRegistrations failed" );
      } else {
	time_t now = time( NULL );
	if ( now == (time_t)(-1) ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorWaitForFreeSpace time() failed errno:%d (%s)",
		      errno, strerror(errno) );
	    ERROR_TO( MON_LOG, msg );
	  }
	  status = MON_ERR_SYS_ERROR;
	} else {
	  if ( ( status = monitorCheckEventsAge( now ) ) != 0 ) {
	    LOG_NORMAL
	      ERROR_TO( MON_LOG,
			"monitorWaitForFreeSpace: \
monitorCheckEventsAge failed" );
	  }
	}
      }
    } else if ( status != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorWaitForFreeSpace semop failed errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
  }
  if ( status == 0 ) {
    if ( ( status = monitorLockBuffer() ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG, "monitorWaitForFreeSpace: cannot re-lock buffer" );
    }
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorWaitForFreeSpace Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorWaitForFreeSpace */


struct sembuf setSemaphoreOp[1] = { { FREE_SPACE_SEMAPHORE, -1, IPC_NOWAIT } };
/* Signal the presence of free space.
 *
 * Return:
 *   0    => ok
 *   else => error
 */
int monitorSignalFreeSpace() {
  int status;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorSignalFreeSpace: starting" );
  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorSignalFreeSpace: lock failed" );
  } else {
    int status1;
    status = semop( monitorControlSemaphores, setSemaphoreOp, 1 );
    if ( status != 0 && errno == EAGAIN ) status = 0;
    if ( status != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorSignalFreeSpace cannot semop \
status:%d errno:%d (%s)",
		  status, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
    if ( ( status1 = monitorUnlockBuffer() ) != 0 )
      LOG_NORMAL ERROR_TO( MON_LOG, "monitorSignalFreeSpace: unlock failed" );
    status = status == 0 ? status1 : status;
  }

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorSignalFreeSpace return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorSignalFreeSpace */


/* Wait for availability of new events
 *
 * WARNING: this routine must be called owning the lock of the
 * monitor buffer and will re-lock the monitor buffer on success.
 * It will release the monitor buffer on failure.
 *
 * Return:
 *   0                   => ok, monitor buffer locked
 *   MON_ERR_INTERRUPTED => wait interrupted, monitor buffer locked
 *   else                => error, monitor buffer unlocked
 */
struct sembuf waitOp[1] = { { NEW_EVENTS_SEMAPHORE, 0, 0 } };
int monitorWaitForNewEvents() {
  int status;
#ifndef NEED_SEMUN
  int value = 1;
#else
  union semun value;

  value.val = 1;
#endif

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorWaitForNewEvents: starting" );
  
  if ( monitorNumLocks != 1 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorWaitForNewEvents Cannot release buffer now count:%d",
		monitorNumLocks );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_INTERNAL;
  }
  waitWasAborted = FALSE;
  if ( semctl( monitorControlSemaphores,
	       NEW_EVENTS_SEMAPHORE,
	       SETVAL,
	       SEMCTL_PAR( value ) ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorWaitForNewEvents failed to set semaphore \
errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
  CLIENT( monitorOurId )->waitingForNewEvents = TRUE;
  if ( ( status = monitorUnlockBuffer() ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorWaitForNewEvents: failed to unlock buffer" );
    CLIENT( monitorOurId )->waitingForNewEvents = FALSE;
    return status;
  }
  LOG_DEVELOPER
    INFO_TO( MON_LOG,
	     "monitorWaitForNewEvents: waiting on new events semaphore" );

  status = semop( monitorControlSemaphores, waitOp, 1 );
  CLIENT( monitorOurId )->waitingForNewEvents = FALSE;
  if ( status < 0 ) {
    if ( errno == EINTR ) {
      waitWasAborted = TRUE;
    } else {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorWaitForNewEvents cannot semop to wait \
errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      return MON_ERR_SYS_ERROR;
    }
  }

  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorWaitForNewEvents: failed to lock monitor buffer" );
    return status;
  }
  if ( waitWasAborted ) {
    LOG_DEVELOPER INFO_TO( MON_LOG, "monitorWaitForNewEvents: interrupted" );
    return MON_ERR_INTERRUPTED;
  }
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorWaitForNewEvents: normal completion" );
  return 0;
} /* End of monitorWaitForNewEvents */


/* Signal the availability of a new event
 *
 * This entry can be called with the monitor buffer locked or unlocked.
 *
 * Return:
 *   0
 *   else => error
 */
int monitorSignalNewEvent() {
  int status;
  int repeat;
  
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorSignalNewEvents: starting" );
  
  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorSignalNewEvent: cannot lock" );
    return status;
  }
  do {
    int client;
    for ( repeat = FALSE, client = 0;
	  !repeat && client != *mbMaxClients;
	  client++ ) {
      if ( ( repeat = CLIENT( client )->inUse &&
	              CLIENT( client )->waitingForNewEvents ) ) {
#ifndef NEED_SEMUN
	int value = 0;
#else
	union semun value;

	value.val = 0;
#endif
	if ( semctl( monitorControlSemaphores,
		     NEW_EVENTS_SEMAPHORE,
		     SETVAL,
		     SEMCTL_PAR( value ) ) != 0 ) {
	  LOG_NORMAL {
	    char msg[ 1024 ];
	    snprintf( SP(msg),
		      "monitorSignalNewEvents Cannot semctl errno:%d (%s)",
		      errno, strerror(errno) );
	    ERROR_TO( MON_LOG, msg );
	  }
	  return MON_ERR_SYS_ERROR;
	}
	if ( monitorPeriodicClientsCheck( PHYSICS_EVENT ) != 0 ) {
	  LOG_NORMAL
	    ERROR_TO( MON_LOG,
		      "monitorSignalNewEvent: error(s) in monitorPeriodicClientsCheck (ignored)" );
	}
      }
    }
  } while ( repeat );
  if ( ( status = monitorUnlockBuffer() ) != 0 ) {
    LOG_NORMAL ERROR_TO( MON_LOG, "monitorSignalNewEvent: cannot unlock" );
    return status;
  }
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorSignalNewEvents: normal completion" );
  return 0;
} /* End of monitorSignalNewEvent */


/* Abort any pending wait for new events.
 *
 * Return:
 *   0
 *   else => error
 */
int monitorStopWaitingForNewEvents() {
  int status;
  int status1;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorStopWaitingForNewEvents: starting" );
  if ( ( status = monitorLockBuffer() ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorStopWaitingForNewEvents Failed to lock buffer \
status:%d",
		status );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }

  waitWasAborted = TRUE;

  if ( ( status = monitorSignalNewEvent() ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorStopWaitingForNewEvents: failed to signal" );
  }
  if ( ( status1 = monitorUnlockBuffer() ) != 0 )
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorStopWaitingForNewEvents: failed to unlock buffer" );
  status = status == 0 ? status1 : status;
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorStopWaitingForNewEvents Return:(%d x%08x)",
	      status, status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorStopWaitingForNewEvents */


/* Get from the system the descriptor of the monitor buffer */
int monitorGetMonitorBufferDescriptor() {
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorGetMonitorBufferDescriptor: starting" );
  
  if ( shmctl( monitorSharedMemoryId,
	       IPC_STAT,
	       &monitorBufferDescriptor ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorGetMonitorBufferDescriptor \
Cannot get monitor buffer descriptor errno:%d (%s)",
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
  LOG_DEVELOPER {
    char msg[ 5000 ];
    snprintf( SP(msg),
	      "Monitor buffer descriptor:\n\
  shm_perm:\n\
    Owner user ID:%ld\n\
    Owner group ID:%ld\n\
    Creator user ID:%ld\n\
    Creator group ID:%ld\n\
    Access modes:x%lx\n\
    Slot usage seq:%ld\n\
    Key:x%0x\n\
  shm_segsz:%d\n\
  shm_lkcnt:%d\n\
  shm_lpid:%ld\n\
  shm_cpid:%ld\n\
  shm_nattch:%ld\n\
  shm_cnattch:%ld",
	     (long int)monitorBufferDescriptor.shm_perm.uid,
	     (long int)monitorBufferDescriptor.shm_perm.gid,
	     (long int)monitorBufferDescriptor.shm_perm.cuid,
	     (long int)monitorBufferDescriptor.shm_perm.cgid,
	     (long int)monitorBufferDescriptor.shm_perm.mode,
#ifdef Linux
	     (long int)-1, -1,
#else
	     monitorBufferDescriptor.shm_perm.seq,
	     monitorBufferDescriptor.shm_perm.key,
#endif
	     (int)monitorBufferDescriptor.shm_segsz,
#ifdef SunOS
	     monitorBufferDescriptor.shm_lkcnt,
#else
	     (int)-1,
#endif
	     (long int)monitorBufferDescriptor.shm_lpid,
	     (long int)monitorBufferDescriptor.shm_cpid,
	     (long int)monitorBufferDescriptor.shm_nattch,
#ifdef Linux
	     (long int)-1
#else
#ifdef OSF1
	     -1
#else
	     monitorBufferDescriptor.shm_cnattch
#endif
#endif
	     );
    INFO_TO( MON_LOG, msg );
  }
  LOG_DEVELOPER
    INFO_TO( MON_LOG, "monitorGetMonitorBufferDescriptor: normal completion" );
  return 0;
} /* End of monitorGetMonitorBufferDescriptor */


/* Validate the monitor buffer
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorValidateMonitorBuffer() {
  int    status = 0;

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorValidateMonitorBuffer: starting" );

  if ( monitorBufferDescriptor.shm_segsz != monitorGetMonitorBufferSize() ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorValidateMonitorBuffer\
Monitor buffer validation error \
sharedMemorySegmentSize:(%d x%08x) expected:(%d x%08x)",
	       (int)monitorBufferDescriptor.shm_segsz,
	       (int)monitorBufferDescriptor.shm_segsz,
	       monitorGetMonitorBufferSize(),
	       monitorGetMonitorBufferSize() );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x80000000;
  }
  if ( *mbMonitorBufferSize != monitorGetMonitorBufferSize() ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorValidateMonitorBuffer \
Monitor buffer validation error size:%d expected:%d",
		*mbMonitorBufferSize, monitorGetMonitorBufferSize() );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x00000001;
  }
  if ( *mbMaxClients != maxClients ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorValidateMonitorBuffer \
Monitor buffer validation error maxClients:%d expected:%d",
	       *mbMaxClients, maxClients );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x00000002;
  }
  if ( *mbMaxEvents != maxEvents ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorValidateMonitorBuffer \
Monitor buffer validation error maxEvents:%d expected:%d",
	       *mbMaxEvents, maxEvents );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x00000004;
  }
  if ( *mbEventSize != eventSize ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorValidateMonitorBuffer \
Monitor buffer validation error eventSize:%d expected:%d",
	       *mbEventSize, eventSize );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x00000008;
  }
  if ( eventBufferSize > 0 && *mbEventBufferSize != eventBufferSize ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
	       "monitorValidateMonitorBuffer \
Monitor buffer validation error eventBufferSize:%d expected:%d",
		*mbEventBufferSize, eventBufferSize );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x00000010;
  }  if ( eventBufferSize <= 0 &&
	  *mbEventBufferSize != (*mbMaxEvents) * (*mbEventSize) ) {
    LOG_DEVELOPER {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorValidateMonitorBuffer \
Monitor buffer validation error eventBufferSize:%d expected:%d",
	       *mbEventBufferSize, (*mbMaxEvents) * (*mbEventSize) );
      ERROR_TO( MON_LOG, msg );
    }
    status |= 0x00000010;
  }
  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	     "monitorValidateMonitorBuffer \
Monitor buffer %s validated status:x%08x",
	      status == 0 ? "has been" : "NOT",
	      status );
    if ( status == 0 ) INFO_TO( MON_LOG, msg );
    else ERROR_TO( MON_LOG, msg );
  }
  return status;
} /* End of monitorValidateMonitorBuffer */


/* Remove the monitor buffer
 *
 * Returns:
 *   0    => OK
 *   else => error
 */
int monitorRemoveMonitorBuffer() {
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorRemoveMonitorBuffer: starting" );
  if ( shmctl( monitorSharedMemoryId, IPC_RMID, NULL ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorRemoveMonitorBuffer \
Cannot remove shared memory ID:%d errno:%d (%s)",
		monitorSharedMemoryId, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
  LOG_DEVELOPER INFO_TO( MON_LOG, "Shared memory segment removed OK" );
  return 0;
} /* End of monitorRemoveMonitorBuffer */


/* Create a lock file using the local filesystem. This lock will be
 * effective ONLY if multiple processes are trying to initialise the
 * shared memory segment at the same time. For "normal" use of the
 * monitor buffer, locking should be done using the control
 * semaphores.
 *
 * Return:
 *	0    => OK
 *	else => error
 *
 * Warning: after monitorLockFile the caller should always call
 * monitorUnlockFile
 */
int monitorLockFile( int key ) {
  char lockFile[ 1024 ];
  int  retries;
  int  fileId;
  int  retry;
  int  tryAgain = FALSE;

  snprintf( SP(lockFile), LOCK_FILE_MASK, key );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorLockFile(%d) lockFile:\"%s\"",
	      key, lockFile );
    INFO_TO( MON_LOG, msg );
  }
  
  do {
    retries = MAX_NUM_RETRIES;
    do {
      fileId = open( lockFile, O_RDWR | O_CREAT | O_EXCL, 0x666 );
      retry = ( fileId == (-1) && errno == EEXIST );
      if ( retry ) sleep( 1 );
    } while ( retry && --retries != 0 );
    
    if ( fileId == (-1) ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorLockFile failed to lock \"%s\" errno:%d (%s-%s)",
		  lockFile,
		  errno, strerror(errno),
		  errno == EEXIST ? "file exists" : "check errno.h" );
	ERROR_TO( MON_LOG, msg );
      }
      if ( errno == EEXIST ) {
	if ( tryAgain ) {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "Monitoring scheme failure. Please remove the file \"%s\"",
		    lockFile );
	  ERROR( msg );
	  return MON_ERR_LOCKFILE;
	} else {
	  /* The file is there and we cannot lock it. Probably it's lost
	   * from a previous attempt. We remove the file and try again
	   */
	  if ( unlink( lockFile ) != 0 ) {
	    LOG_NORMAL {
	      char msg[ 1024 ];
	      snprintf( SP(msg),
			"monitorLockFile: cannot remove \"%s\" \
errno:%d (%s)",
			lockFile, errno, strerror(errno) );
	      ERROR_TO( MON_LOG, msg );
	    }
	  }
	  tryAgain = TRUE;
	}
      } else {
	return MON_ERR_SYS_ERROR;
      }
    }
  } while ( tryAgain );
  
  if ( fchmod( fileId, 0x0777 ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorLockFile Failed chmod \"%s\" errno:%d (%s)",
		lockFile, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    close( fileId );
    return MON_ERR_SYS_ERROR;
  }
 
  if ( close( fileId ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorLockFile: cannot close \"%s\" errno:%d (%s)",
		lockFile, errno, strerror(errno) );
    }
    return MON_ERR_SYS_ERROR;
  }

  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorLockFile: lock acquired OK" );
  return 0;
} /* End of monitorLockFile */


/* Unlock a previously locked file.
 *
 * Return value:
 *	0    => all OK
 *	else => error
 */
int monitorUnlockFile( int key ) {
  char lockFile[ 1024 ];

  snprintf( SP(lockFile), LOCK_FILE_MASK, key );

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg),
	      "monitorUnlockFile(%d) lockFile:\"%s\"",
	      key, lockFile );
    INFO_TO( MON_LOG, msg );
  }

  if ( unlink( lockFile ) != 0 ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorUnlockFile Cannot remove \"%s\" errno:%d (%s)",
		lockFile, errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }
  LOG_DEVELOPER INFO_TO( MON_LOG, "monitorUnlockFile: normal completion" );
  return 0;
} /* End of monitorUnlockFile */


/* Attach to the shared memory segment containing the buffer (if possible).
 *
 * Parameters:
 *	keyFile		File used as key to the buffer
 *
 * Return value:
 *	0    => all OK
 *	else => error
 *
 * There are three basic situations possible:
 *
 *   1) the buffer does not exist yet
 *	=> we must create it and initialise it (watch out for multiple
 *         attempts!)
 *   2) the buffer already exists and has the correct parameters (total
 *      size, internal parameters)
 *	=> attach to the buffer
 *   3) the buffer already exists and has wrong parameters
 *	=> if no other processes are attached to it, destroy the buffer and
 *	   re-create it using the correct parameters
 *	=> if other processes are using it, fail
 */
int monitorAttachToBuffer( char *keyFile ) {
  int   status;
  key_t key;
  int   repeatGet = FALSE;

  LOG_DEVELOPER {
    char msg[ 1024 ];
    snprintf( SP(msg), "monitorAttachToBuffer(%s)", keyFile );
    INFO_TO( MON_LOG, msg );
  }
  
  key = ftok( keyFile, 1 );
  if ( key == (key_t)(-1) ) {
    LOG_NORMAL {
      char msg[ 1024 ];
      snprintf( SP(msg),
		"monitorAttachToBuffer Cannot get key to \"%s\" errno:%d (%s)",
		keyFile,
		errno, strerror(errno) );
      ERROR_TO( MON_LOG, msg );
    }
    return MON_ERR_SYS_ERROR;
  }

  if ( ( status = monitorLockFile( key ) ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorAttachToBuffer: failed to acquire lock file" );
    return status;
  }

  do {
    int size;
    
    monitorSharedMemoryId = shmget( key, 0, PROTECTION_PUBLIC );
    if ( monitorSharedMemoryId == (-1) ) {
      /* We could not get the shared memory ID: there can be two
       * explanations:
       *   1) the shared memory segment does not exist
       *   2) we could not get the ID for other reasons
       */
      if ( errno != ENOENT && errno != 0 ) {
	/* The shared memory segment exists but we cannot get the ID */
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorAttachToBuffer Cannot get shared memory ID \
key:%d errno:%d (%s) protectionMask:x%08x",
		    key, errno, strerror(errno), PROTECTION_PUBLIC );
	  ERROR_TO( MON_LOG, msg );
	}
	monitorUnlockFile( key );
	return MON_ERR_SYS_ERROR;
      }
      /* The shared memory segment does not exist */
      LOG_DEVELOPER
	INFO_TO( MON_LOG,
		 "monitorAttachToBuffer: buffer does not exist, \
trying to create it" );

      monitorFirstFree = (void*)-1;	/* This will reset all pointers */
      monitorSharedMemoryId = shmget( key,
				      (size = monitorGetMonitorBufferSize()),
				      PROTECTION_PUBLIC | IPC_CREAT );
      if ( monitorSharedMemoryId == (-1) ) {
	/* If it does not work, we give up */
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorAttachToBuffer Cannot create buffer segment \
key:%d size:%d errno:%d (%s)",
		   key, size, errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	monitorUnlockFile( key );
	return MON_ERR_SYS_ERROR;
      }
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "monitorAttachToBuffer: monitor buffer created OK" );
    } else {
      LOG_DEVELOPER
	INFO_TO( MON_LOG, "monitorAttachToBuffer: monitor buffer ID got OK" );
    }

    /* Now we attach to the shared memory segment: the system should give
     * us a pointer we can use to access the memory block:
     */
    monitorBuffer = shmat( monitorSharedMemoryId, 0, SHM_R|SHM_W );
    if ( (long)monitorBuffer == (-1) ) {
      /* Problems: invalidate the pointer, unlock and return */
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorAttachToBuffer Cannot attach to memory \
key:%d id:%d errno:%d (%s)",
		  key, monitorSharedMemoryId, errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      monitorBuffer = NULL;
      monitorUnlockFile( key );
      return MON_ERR_SYS_ERROR;
    }

    /* Get the characteristics of the shared memory block */
    if ( ( status = monitorGetMonitorBufferDescriptor() ) != 0 ) {
      LOG_NORMAL
	ERROR_TO( MON_LOG,
		  "monitorAttachToBuffer: \
monitorGetMonitorBufferDescriptor failed" );
      monitorUnlockFile( key );
      return status;
    }
    /* Initialise the pointers we will use to access the buffer */
    monitorInitMonitorBufferPointers();

    /* If no other processes are attached to the segment, we can initialise
     * its internal data structures
     */
    if ( monitorBufferDescriptor.shm_nattch == 1 )
      monitorInitMonitorBuffer();

    /* Now we can validate the content of the buffer */
    if ( monitorValidateMonitorBuffer() == 0 ) {

      /* All OK: reset the repeat flag */
      repeatGet = FALSE;
    } else {

      /* The buffer does not validate: detach from it */
      if ( shmdt( monitorBuffer ) != 0 ) {
	LOG_NORMAL {
	  char msg[ 1024 ];
	  snprintf( SP(msg),
		    "monitorAttachToBuffer Cannot detach segment \
errno:%d (%s)",
		    errno, strerror(errno) );
	  ERROR_TO( MON_LOG, msg );
	}
	monitorBuffer = NULL;
	monitorUnlockFile( key );
	return MON_ERR_SYS_ERROR;
      }
      monitorBuffer = NULL;

      /* If we are the only users of the buffer, we can try to destroy it
       * and re-create it
       */
      if ( monitorBufferDescriptor.shm_nattch == 1 ) {
	if ( (repeatGet = !repeatGet) ) {
	  LOG_DEVELOPER
	    INFO_TO( MON_LOG,
		     "monitorAttachToBuffer: buffer does not validate; \
try to re-create it" );
	  if ( ( status = monitorRemoveMonitorBuffer() ) != 0 ) {
	    monitorUnlockFile( key );
	    return status;
	  }
	} else {
	  LOG_DEVELOPER
	    INFO_TO( MON_LOG,
		     "monitorAttachToBuffer: buffer already re-created, \
giving up" );
	}
      } else {
	LOG_DEVELOPER
	  INFO_TO( MON_LOG,
		   "monitorAttachToBuffer: buffer does not validate \
and currently in use" );
	monitorUnlockFile( key );
	return MON_ERR_CLEANUP_NEEDED;
      }
    }
  } while ( repeatGet );

  /* Get and init the control semaphores */
  if ( ( status = monitorInitSemaphores( key ) ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG,
		"monitorAttachToBuffer: monitorInitSemaphores failed" );
    return status;
  }

  /* All done: unlock the buffer */
  if ( ( status = monitorUnlockFile( key ) ) != 0 ) {
    LOG_NORMAL
      ERROR_TO( MON_LOG, "monitorAttachToBuffer: monitorUnlockFile failed" );
    return status;
  }

  return 0;
} /* End of monitorAttachToBuffer */

/* Detach from the buffer (if needed) */
int monitorDetachFromBuffer( void ) {
  if ( monitorBuffer != NULL ) {
    int status;

    if ( shmdt( monitorBuffer ) != 0 ) {
      LOG_NORMAL {
	char msg[ 1024 ];
	snprintf( SP(msg),
		  "monitorDetachFromBuffer Cannot detach segment \
errno:%d (%s)",
		  errno, strerror(errno) );
	ERROR_TO( MON_LOG, msg );
      }
      status = MON_ERR_SYS_ERROR;
    }
    monitorBuffer = NULL;
  }
  return 0;
} /* End of monitorDetachFromBuffer */
