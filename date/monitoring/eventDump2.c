/* Event dump DATE utility program
 * ===============================
 *
 * Module history:
 *  1.00  20-Aug-98  RD  Created
 *  1.01  15-Sep-98  RD  MP declare added
 *  1.02  27-Oct-98  RD  Silent switch added
 *  1.03  28-Oct-98  RD  Silent cursor made function of time
 *  1.04  15-Jan-99  RD  Bug with Asynch sampling fixed
 *  1.05  18-Jan-99  RD  Re-structured
 *  1.06  19-Feb-99  RD  Swapping added
 *  1.07   1-Mar-99  RD  Data dumped as 32 bits rather then 8 bits
 *  1.08   7-Jul-99  RD  Dump of monitor attributes added
 *  1.09   6-Dec-99  RD  Small problem with "old type" events fixed
 *  1.10   6-Apr-00  RD  Other problem with "old type" events fixed
 *  1.11  12-May-00  LS  Version to write selected events in DATE raw file
 *  1.12  12-Jul-00  RD  Added "print after" switches
 *  1.13   1-Dec-00  RD  Adapted for DATE 4.x
 *  1.14   5-Jul-01  RD  Changed handling of attributes (and-or)
 *  1.15   1-Nov-04  RD  Added dump of PERIOD
 *  1.16   8-Nov-04  RD  Data checks changed for new test equipment
 *  1.17  17-Aug-05  RD  Added Vanguard and Rearguard events
 *  1.18   5-Sep-05  RD  Added SST and DST events
 *  1.19  14-Sep-05  RD  VAN/REARGUARD events changed into SOD/EOD
 */
#define VID "1.19"

#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "event.h"
#include "monitor.h"
#include "attributesHandler.h"

#define DESCRIPTION "DATE V3 event dump"
#ifdef AIX
static
#endif
char mpDaemonMainIdent[] = "@(#)""" __FILE__ """: """ DESCRIPTION \
                        """ """ VID """ compiled """ __DATE__ """ """ __TIME__;

#ifndef TRUE
# define TRUE ( 0 == 0 )
#endif
#ifndef FALSE
# define FALSE ( 0 == 1 )
#endif

/* Macros for log strings handling */
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

unsigned char *dataBuffer;
char *dataSource;
char filename[128];
int terminate;
int briefOutput;
int noOutput;
int checkData;
int useStatic;
int asynchRead;
int interactive;
int must;
int wfile;
int fd;
int maxGetEvents;
int useTable;
int useAttributes;
int numErrors;
int numChecked;
unsigned int lastEventType;
int numHeaders;
int bytesHeaders;
int numSuperEvents;
int numEventData;
int bytesEventData;
int staticDataBuffer[ 100000 ];
int doOutput;
int startBunchCrossingLoaded;
int startBunchCrossing;
int startOrbitLoaded;
int startOrbit;
int startEventIdLoaded;
eventIdType startEventId;
int startSerialNbLoaded;
int startSerialNb;
int numEvents;
int gotSuperEvents = FALSE;
int dumpEquipmentHeader;
int dumpCommonDataHeader;

char *mustTable[3] = { "all", "all", 0 };

/* Check the data content against the defined test sequence */
void doCheckData( const unsigned char * const buffer,
		  int from,
		  int to,
		  const int scream ) {
  int i;
  struct equipmentHeaderStruct *eh;
  int ehBase;
  unsigned int expectedPattern;
  
  if ( lastEventType != PHYSICS_EVENT ) return;

  ehBase = 0;
  eh = (struct equipmentHeaderStruct *)&buffer[ehBase];

  for ( i = from,
	  expectedPattern = ( from -
			      sizeof( struct equipmentHeaderStruct ) -
			      4 ) >> 2;
	i < to;
	i += 4 ) {
    unsigned int gotPattern;

    /* Re-synch with the equipment headers (if necessary) */
    if ( i >= ehBase + eh->equipmentSize ) {
      do {
	ehBase += eh->equipmentSize;
	eh = (struct equipmentHeaderStruct *)&buffer[ehBase];
      } while ( i >= ehBase + eh->equipmentSize );
      expectedPattern = (i -
			 ehBase -
			 sizeof( struct equipmentHeaderStruct ) -
			 4 ) >> 2;
    }

    /* Skip data from the equipment header & first data word */
    if ( i < ehBase + sizeof( struct equipmentHeaderStruct ) + 4 ) {
      expectedPattern = 0;
    } else {
      gotPattern = *(unsigned int *)&buffer[i];
      if ( expectedPattern != gotPattern ) {
	if ( scream )
	  printf( " <<<(E:%08x G:%08x)", expectedPattern, gotPattern );
	numErrors++;
      }
      numChecked++;
      expectedPattern++;
    }
  }
} /* End of checkData */


/* Dump a data buffer in ASCII form (if possible) */
void dumpAscii( unsigned char *buffer,
		int from,
		int to ) {
  register int i;

  if ( from == to ) return;
  for ( i = to; ; i++ ) {
    if ( i % 16 == 0 ) break;
    printf( "  " );
    if ( i % 4 == 3 ) printf( " " );
  }
  printf( "| " );
  for ( i = from; i != to; i++ ) {
    if ( isprint( buffer[ i ] ) )
      printf( "%c", buffer [ i ] );
    else
      printf( "." );
    if ( i % 4 == 3 ) printf( " " );
  }
  printf( "|" );
} /* End of dumpAscii */


void doDump( unsigned char *buffer, int size, int check ) {
  int i, start;

  if ( size <= 0 ) return;
  if ( !briefOutput ) {
    for ( start = 0, i = 0; i != size; ) {
      if ( i % 16 == 0 ) {
	dumpAscii( buffer, start, i );
	if ( check && checkData )
	  doCheckData( buffer, start, i, TRUE );
	start = i;
	if ( i != 0 ) printf( "\n" );
	printf( "%4d) ", i );
      }
      if ( ( size - i ) >= 4 ) {
	printf( "%08x ", *(long32 *)(&buffer[i]));
	i += 4;
      } else {
	printf( "%02x", buffer[ i ] );
	if ( i % 4 == 3 ) printf( " " );
	i++;
      }
    }
    dumpAscii( buffer, start, i );
    if ( check && checkData )
      doCheckData( buffer, start, i, TRUE );
    printf( "\n" );
  } else {
    if ( check && checkData )
      doCheckData( buffer, 0, size, FALSE );
  }
} /* End of doDump */

/* Dump a buffer in HEX and ASCII format. Eventually check the content */
void dumpData( struct eventHeaderStruct *event ) {
  lastEventType = event->eventType;
  doDump( (void*)event + event->eventHeadSize,
	  event->eventSize - event->eventHeadSize,
	  TRUE );
} /* End of dumpData */


/* Swap an event header. We assume that the first LW of the event
 * contains the header length.
 */
void swapHeader( void *inHeader ) {
  long32 *ptr = (long32 *)inHeader;
  long32  i = 0;
  long32  size = -1;

  i = 0;
  ptr = (long32 *)inHeader;
  do {
    long32 word = *ptr;
    *ptr = ((word << 24) & 0xff000000) |
           ((word <<  8) & 0x00ff0000) |
           ((word >>  8) & 0x0000ff00) |
           ((word >> 24) & 0x000000ff);
    if ( i == 0 ) size = *ptr;
    i += 4;
    ptr++;
  } while ( i < size );
} /* End of swapHeader */

/* Dump a Common Data Header block attributes field */
void printCdhBlockAttributes( unsigned long32 ba ) {
  int i, n;

  for ( n = 0, i = 0; i != 7; i++ ) {
    if ( ((1 << i) & ba) != 0 ) {
      if ( n == 0 ) printf( "=" );
      else          printf( "+" );
      n++;
      printf( "%d", i );
      ba ^= (1 << i);
    }
  }
  if ( ba != 0 ) {
    if ( n == 0 ) printf( "=" );
    else          printf( "+" );
    n++;
    printf( "RESIDUE:%08x!", ba );
  }
} /* End of printCdhBlockAttributes */

void testCdhBit( unsigned long32 *se,
		 int bit,
		 char *text,
		 int *n ) {
  int mask = 1 << bit;
  if ( (*se & mask) != 0 ) {
    if ( (*n)++ != 0 ) printf( "+" );
    printf( text );
    *se ^= mask;
  }
} /* End of testCdhBit */

/* Dump a Common Data Header Status & Error bits field */
void printCdhStatusErrorBits( unsigned long32 se ) {
  int n = 0;

  testCdhBit( &se,
	      CDH_TRIGGER_OVERLAP_ERROR_BIT,
	      "triggerOverlapError",
	      &n );
  testCdhBit( &se,
	      CDH_TRIGGER_MISSING_ERROR_BIT,
	      "triggerMissingError",
	      &n );
  testCdhBit( &se,
	      CDH_DATA_PARITY_ERROR_BIT,
	      "dataParityError",
	      &n );
  testCdhBit( &se,
	      CDH_CONTROL_PARITY_ERROR_BIT,
	      "controlParityError",
	      &n );
  testCdhBit( &se,
	      CDH_FEE_ERROR_BIT,
	      "feeError",
	      &n );
	      
  testCdhBit( &se,
	      CDH_TRIGGER_INFORMATION_UNAVAILABLE_BIT,
	      "noTriggerInfo",
	      &n );

  if ( se != 0 ) {
    if ( n++ != 0 ) printf( "+" );
    printf( "RESIDUE:x%x!", se );
  }
} /* End of printCdhStatusErrorBits */

/* Dump a Event header (if possible). Returns TRUE if "good" event header */
int dumpHeader( struct eventHeaderStruct *header,
		int scream ) {
  int swapped = FALSE;

  if ( header->eventMagic == EVENT_MAGIC_NUMBER_SWAPPED ) {
    swapped = TRUE;
    swapHeader( header );
  }
  
  if ( header->eventMagic != EVENT_MAGIC_NUMBER ) {
    if ( scream ) {
      printf( "!!! WARNING !!! MAGIC: %08x (expected: %08x)\n",
	      header->eventMagic,
	      EVENT_MAGIC_NUMBER );
    } else {
      return( FALSE );
    }
  }
  printf( "Size:%d (header:%d)", header->eventSize, header->eventHeadSize );
  printf( " " );
  
  printf( "Version:0x%08x", header->eventVersion );
  printf( " " );
  
  printf( "Type:" );
  switch ( header->eventType ) {
    case START_OF_RUN :       printf( "StartOfRun" ); break;
    case END_OF_RUN :         printf( "EndOfRun" ); break;
    case START_OF_RUN_FILES : printf( "StartOfRunFiles" ); break;
    case END_OF_RUN_FILES :   printf( "EndOfRunFiles" ); break;
    case START_OF_BURST :     printf( "StartOfBurst" ); break;
    case END_OF_BURST :       printf( "EndOfBurst" ); break;
    case PHYSICS_EVENT :      printf( "PhysicsEvent" ); break;
    case CALIBRATION_EVENT :  printf( "CalibrationEvent" ); break;
    case EVENT_FORMAT_ERROR : printf( "EventFormatError" ); break;
    case START_OF_DATA :      printf( "StartOfData" ); break;
    case END_OF_DATA :        printf( "EndOfData" ); break;
    case SYSTEM_SOFTWARE_TRIGGER_EVENT :
                              printf( "SystemSoftwareTriggerEvent" ); break;
    case DETECTOR_SOFTWARE_TRIGGER_EVENT :
                              printf( "DetectorSoftwareTriggerEvent" ); break;
    default :                 printf( "UNKNOWN TYPE %d 0x%08x",
				      header->eventType, header->eventType );
  }
  if ( swapped )
    printf( " SWAPPED" );

  printf( "\n" );

  printf( "RunNb:%d", header->eventRunNb );
  printf( " " );

  if ( TEST_SYSTEM_ATTRIBUTE( header->eventTypeAttribute,
			      ATTR_ORBIT_BC ) ) {
    printf( "Period:%d Orbit:%d BunchCrossing:%d",
	    EVENT_ID_GET_PERIOD( header->eventId ),
	    EVENT_ID_GET_ORBIT( header->eventId ),
	    EVENT_ID_GET_BUNCH_CROSSING( header->eventId ) );
  } else {
    printf( "nbInRun:%d burstNb:%d nbInBurst:%d",
	    EVENT_ID_GET_NB_IN_RUN( header->eventId ),
	    EVENT_ID_GET_BURST_NB( header->eventId ),
	    EVENT_ID_GET_NB_IN_BURST( header->eventId ) );
  }
  printf( " " );
  
  printf( "ldcId:" );
  if ( header->eventLdcId == VOID_ID ) printf( "VOID" );
  else printf( "%d", header->eventLdcId );
  printf( " " );

  printf( "gdcId:" );
  if ( header->eventGdcId == VOID_ID ) printf( "VOID" );
  else printf( "%d", header->eventGdcId );
  printf( " " );

  {
    time_t t = header->eventTimestamp;
    
    printf( "time:%s", ctime( &t ) );
  } /* Note that ctime will add a "\n" !!! */
  
  printf( "Attributes:" );
  monitorPrintAttributes( header, stdout );
  if ( !SYSTEM_ATTRIBUTES_OK( header->eventTypeAttribute ) )
    printf( "{NOT OK}" );
  printf( "\n" );
  
  printf( "triggerPattern:%08x-%08x%s%s",
	  header->eventTriggerPattern[0],
	  header->eventTriggerPattern[1],
	  TRIGGER_PATTERN_VALID(header->eventTriggerPattern) ? "":"[invalid]",
	  TRIGGER_PATTERN_OK(header->eventTriggerPattern) ? "" : "{NOT OK}" );
  printf( " " );

  printf( "detectorPattern:%08x%s%s",
	  header->eventDetectorPattern[0],
	  DETECTOR_PATTERN_VALID(header->eventDetectorPattern) ?
	    "" : "[invalid]",
	  DETECTOR_PATTERN_OK(header->eventDetectorPattern) ?
	    "" : "{NOT OK}" );
  printf( "\n" );

  if ( header->eventHeadSize > EVENT_HEAD_BASE_SIZE ) {
    if ( !briefOutput )
      printf( "Header extension (%d byte(s)):\n",
	      header->eventHeadSize - EVENT_HEAD_BASE_SIZE );
    doDump( ((void*)header) + EVENT_HEAD_BASE_SIZE,
	    header->eventHeadSize - EVENT_HEAD_BASE_SIZE,
	    FALSE );
    if ( !briefOutput )
      printf( "Payload:\n" );
  }

  if ( dumpEquipmentHeader ) {
    if ( (header->eventType == PHYSICS_EVENT
       || header->eventType == CALIBRATION_EVENT)
      && !TEST_SYSTEM_ATTRIBUTE( header->eventTypeAttribute,
				 ATTR_SUPER_EVENT) ){
      void *p;

      for ( p = (void *)header + header->eventHeadSize;
	    p < (void *)header + header->eventSize; ) {
	struct equipmentHeaderStruct *eh = p;

	printf( " - Equipment: size:%d type:%d id:%d basicElementSize:%d attributes:",
		eh->equipmentSize,
		eh->equipmentType,
		eh->equipmentId,
		eh->equipmentBasicElementSize );
	monitorPrintAttributesMask( &eh->equipmentTypeAttribute, stdout );
	printf( "\n" );
	if ( dumpCommonDataHeader ) {
	  struct commonDataHeaderStruct *h =
	    (struct commonDataHeaderStruct *)(p + sizeof( struct equipmentHeaderStruct ));
	  
	  printf( "   > Length:%d", h->cdhBlockLength );
	  if ( h->cdhBlockLength == 0xffffffff ) {
	    printf( "=<VOID>" );
	  } else {
	    if ( eh->equipmentSize - sizeof( struct equipmentHeaderStruct ) !=
		 h->cdhBlockLength ) {
	      printf( "!=equipmentSize-sizeof(struct equipmentHeaderStruct):%d",
		      eh->equipmentSize - (int)sizeof( struct equipmentHeaderStruct ) );
	    }
	  }
	  printf( " version:%d", h->cdhVersion );
	  if ( CDH_VERSION != h->cdhVersion
	    && h->cdhVersion != 0 ) {
	    printf( "(!=CDH_VERSION:%d!)", CDH_VERSION );
	  }
	  /* Skip the other fields if version numbers differ. Exception:
	     version 0 (development phase) */
	  if ( 0           == h->cdhVersion
	    || CDH_VERSION == h->cdhVersion ) {
	    printf( " l1TriggerMsg:%x", h->cdhL1TriggerMessage );
	    printf( " blockAttr:x%x", h->cdhBlockAttributes );
	    if ( h->cdhBlockAttributes != 0 )
	      printCdhBlockAttributes( h->cdhBlockAttributes );

	    printf( "\n    " );
	    
	    printf( " eventId1(bunch crossing):%d", h->cdhEventId1 );
	    printf( " eventId2(orbit counter):%d", h->cdhEventId2 );	    
	    printf( " miniEventId(bunch crossing):%d", h->cdhMiniEventId );

	    printf( "\n    " );
	    
	    printf( " participatingSubDetectors:x%x", h->cdhParticipatingSubDetectors );
	    printf( " Status/Errors:" );
	    if ( h->cdhStatusErrorBits == 0 ) {
	      printf( "<NONE>" );
	    } else {
	      printCdhStatusErrorBits( h->cdhStatusErrorBits );
	    }
    
	    printf( "\n    " );
	    
	    printf( " triggerClassesHigh/Low:x%x-x%x",
		    h->cdhTriggerClassesHigh,
		    h->cdhTriggerClassesLow );
	    printf( " ROIhigh/low:x%x-x%x",
		    h->cdhRoiHigh,
		    h->cdhRoiLow );

	    if ( h->cdhMBZ0 != 0
	      || h->cdhMBZ1 != 0
	      || h->cdhMBZ2 != 0
	      || h->cdhMBZ3 != 0 ) {
	      printf( "\n    " );
	      printf( " MBZ0:x%x!", h->cdhMBZ0 );
	      printf( " MBZ1:x%x!", h->cdhMBZ1 );
	      printf( " MBZ2:x%x!", h->cdhMBZ2 );
	      printf( " MBZ3:x%x!", h->cdhMBZ3 );
	    }
	  }
	  printf( "\n" );
	}
	p += eh->equipmentSize;
	if ( eh->equipmentSize < sizeof( struct equipmentHeaderStruct ) ) {
	  printf( " >>> Equipment size too small (%d < %d): skipping\n",
		  eh->equipmentSize,
		  (int)sizeof( struct equipmentHeaderStruct ) );
	  p = (void *)header + header->eventSize;
	}
      }
    }
  }
  
  return( header->eventMagic == EVENT_MAGIC_NUMBER );
} /* End of dumpHeader */


/* Decode and store a monitoring policy table */
#define MAX_ENTRIES 15
char *monitorTable[ MAX_ENTRIES ] = { 0 };
int decodeTable( char *declaration ) {
  char *p = declaration;
  int nEntries = 0;
  while ( *p != 0 ) {
    while ( *p == ' ' ) p++;
    if ( *p != 0 ) {
      char *s = p;
      char *r;
      while ( *p != 0 && *p != ' ' ) p++;
      if ( ( r = (char*)malloc( p-s+1 ) ) == NULL ) {
	perror( "malloc failed " );
	return 1;
      }
      strncpy( r, s, p-s );
      r[ p-s ] = 0;
      if ( nEntries == MAX_ENTRIES-1 ) {
	fprintf( stderr, "Table too complex, cannot store...\n" );
	return( 1 );
      }
      monitorTable[ nEntries++ ] = r;
      /* This part not needed any more (change in syntax of attributes')
      for (; *r != 0; r++ )
	if ( *r == '+' ) *r = ' ';
      */
    }
  }
  return 0;
} /* End of decodeTable */

int decodeTableWithAttributes( char *declaration ) {
  useAttributes = TRUE;
  return decodeTable( declaration );
} /* End of decodeTableWithAttributes */


/* Print the tool's usage instructions */
void usage( char *ourName ) {
  fprintf( stderr,
	   "Usage: %s [-b][-c][-s][-a][-i][-f \"filename\"][-n number][-t \"table\"][-T \"table\"][-# [b|t|n|e]number] dataSource\n\
\t-b: brief output (skip long events)\n\
\t-S: silent\n\
\t-c: check event data\n\
\t-s: use static data buffer\n\
\t-a: use asynchronous reads\n\
\t-i: interactive\n\
\t-f: write selected events to raw file\n\
\t-n: maximum number of events to process\n\
\t-t: monitoring table to be used (e.g. -t \"ALL yes SOB no\")\n\
\t-T: as \"-t\" but the table has monitoring attributes included\n\
\t\t(e.g. -T \"All yes 1 Phy y 1|2 SOB NO 1&5\")\n\
\t-e: dump content of equipment header\n\
\t-D: dump content of common data header (implies \"-e\")\n\
\t-#: wait for given event\n\
\t\t(b:bunchCrossing o:orbit e:orbit-bunchCrossing <nothing>:serial number)\n",
	   ourName );
} /* End of usage */


/* Check the current configuration */
void checkConfig() {
  if ( sizeof( long32 ) != 4 )
    fprintf( stderr, "ERROR; sizeof(long32):%d\n", (int)sizeof(long32) );
  if ( sizeof( long64 ) != 8 )
    fprintf( stderr, "ERROR; sizeof(long64):%d\n", (int)sizeof(long64) );
  if ( sizeof( datePointer ) != sizeof( char* ) ||
       sizeof( datePointer ) != sizeof( void* ) )
    fprintf( stderr,
	     "ERROR: \
sizeof(datePointer):%d \
sizeof(char*):%d \
sizeof(void*):%d\n",
	     (int)sizeof( datePointer ),
	     (int)sizeof( char* ),
	     (int)sizeof( void* ) );
} /* End of checkConfig */


/* Initialise our variables */
void initVars() {
  dataSource = NULL;
  briefOutput = FALSE;
  noOutput = FALSE;
  checkData = FALSE;
  useStatic = FALSE;
  asynchRead = FALSE;
  interactive = FALSE;
  useTable = FALSE;
  useAttributes = FALSE;
  must = FALSE;
  wfile = FALSE;
  fd = 0;
  maxGetEvents = 0;
  numErrors = 0;
  numChecked = 0;
  numHeaders = 0;
  bytesHeaders = 0;
  numSuperEvents = 0;
  numEventData = 0;
  bytesEventData = 0;
  doOutput = TRUE;
  dumpEquipmentHeader = FALSE;
  dumpCommonDataHeader = FALSE;
  startBunchCrossingLoaded = FALSE;
  startOrbitLoaded = FALSE;
  startEventIdLoaded = FALSE;
  startSerialNbLoaded = FALSE;
  numEvents = 0;
} /* End of initVars */


/* Handle all command line arguments */
void handleArgs( int argc, char **argv ) {
  int   optNum;
  
  for ( optNum = 1; optNum != argc; ) {
    if ( strcmp( argv[optNum], "-?" ) == 0 ) {
      usage( argv[0] );
      exit( 0 );
    }
    if ( strcmp( argv[optNum], "-b" ) == 0 ) {
      briefOutput = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-S" ) == 0 ) {
      noOutput = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-c" ) == 0 ) {
      checkData = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-s" ) == 0 ) {
      useStatic = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-a" ) == 0 ) {
      asynchRead = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-i" ) == 0 ) {
      interactive = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-M" ) == 0 ) {
      must = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-e" ) == 0 ) {
      dumpEquipmentHeader = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-D" ) == 0 ) {
      dumpEquipmentHeader = TRUE; dumpCommonDataHeader = TRUE; optNum++;
    } else if ( strcmp( argv[optNum], "-f" ) == 0 ) {
      wfile = TRUE; optNum++;
      snprintf( SP(filename), "%s", argv[optNum]); optNum++;
    } else if ( strcmp( argv[optNum], "-n" ) == 0 ) {
      optNum++;
      sscanf( argv[optNum], "%d", &maxGetEvents );
      optNum++;
      printf("Taking max. %d events\n",maxGetEvents);
    } else if ( strcmp( argv[optNum], "-t" ) == 0 ) {
      if ( !useTable ) {
	useTable = TRUE; optNum++;
	if ( decodeTable( argv[optNum++] ) != 0 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
      } else {
	fprintf( stderr, "%s: only one \"-t\" and/or \"-T\"\n", argv[0] );
	exit( 1 );
      }
    } else if ( strcmp( argv[optNum], "-T" ) == 0 ) {
      if ( !useTable ) {
	useTable = TRUE; optNum++;
	if ( decodeTableWithAttributes( argv[optNum++] ) != 0 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
      } else {
	fprintf( stderr, "%s: only one \"-t\" and/or \"-T\"\n", argv[0] );
	exit( 1 );
      }
    } else if ( strcmp( argv[optNum], "-#" ) == 0 ) {
      char *s;
      void *i;
      
      optNum++;
      switch ( argv[optNum][0] ) {
      case 'o' :
	s = &argv[optNum][1];
	i = &startOrbit;
	startOrbitLoaded = TRUE;
	break;
      case 'e' :
	s = &argv[optNum][1];
	i = &startEventId;
	startEventIdLoaded = TRUE;
	break;
      case 'b' :
	s = &argv[optNum][1];
	i = &startBunchCrossing;
	startBunchCrossingLoaded = TRUE;
	break;
      default :
	s = &argv[optNum][0];
	i = &startSerialNb;
	startSerialNbLoaded = TRUE;
	break;
      }
      if ( !isdigit( (int)*s ) ) {
	usage( argv[0] );
	exit( 1 );
      }
      if ( i == &startEventId ) {
	int orbit;
	int bunchCrossing;
	char *separator;

	if ( (separator = strchr( s, '-' )) == NULL ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	if ( strrchr( s, '-' ) != separator ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	*separator = 0;
	if ( sscanf( s, "%d", &orbit ) != 1 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	if ( sscanf( separator+1, "%d", &bunchCrossing ) != 1 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
	LOAD_EVENT_ID( startEventId, 0, orbit, bunchCrossing );
      } else {
	if ( sscanf( s, "%d", (int *)i ) != 1 ) {
	  usage( argv[0] );
	  exit( 1 );
	}
      }
      doOutput = FALSE;
      optNum++;
    } else if ( dataSource == NULL && argv[optNum][0] != '-' ) {
      dataSource = argv[optNum++];
    } else {
      usage( argv[0] );
      exit( 1 );
    }
  }

  if ( briefOutput && noOutput ) {
    fprintf( stderr, "%s: -i and -S switches incompatible\n", argv[0] );
    exit( 1 );
  }
  if ( briefOutput && noOutput ) {
    fprintf( stderr, "%s: -b and -S switches incompatible\n", argv[0] );
    exit( 1 );
  }
  if ( checkData && noOutput ) {
    fprintf( stderr, "%s: -c and -S switches incompatible\n", argv[0] );
    exit( 1 );
  }
  if ( must && useTable ) {
    fprintf( stderr, "%s: -t and -M switches incompatible\n", argv[0] );
    exit( 1 );
  }
} /* End of handleArgs */


/* Setup monitoring source, parameters and characteristics */
void monitorSetup( char *ourName ) {
  int status;
  
  if ( ( status = monitorDeclareMp( DESCRIPTION """ V""" VID ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot declare MP, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( dataSource == NULL ) {
    usage( ourName );		/* We must have a data source! */
    exit( 1 );
  }

  if ( ( status = monitorSetDataSource( dataSource ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot attach to monitor scheme, status: %s\n",
	     monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( ( status = monitorSetSwap( FALSE, FALSE ) ) != 0 ) {
    fprintf( stderr,
	     "Cannot set swpping mode, status: %d (0x%08x), %s",
	     status, status, monitorDecodeError( status ) );
    exit( 1 );
  }

  if ( asynchRead ) {
    if ( ( status = monitorSetNowait() ) != 0 ) {
      fprintf( stderr,
	       "Cannot set Asynchronous mode, status: %s\n",
	       monitorDecodeError( status ) );
      exit( 1 );
    }
  }

  if ( must ) {
    if ( ( status = monitorDeclareTable( mustTable ) ) != 0 ) {
      fprintf( stderr,
	       "Cannot set must-monitor mode, status: %s\n",
	       monitorDecodeError( status ) );
      exit( 1 );
    }
    printf( "Entering must-monitor mode. \
WARNING: may stop data acquisition!\n" );
  }

  if ( useTable ) {
    if ( useAttributes )
      status = monitorDeclareTableWithAttributes( monitorTable );
    else
      status = monitorDeclareTable( monitorTable );
    if ( status != 0 ) {
      fprintf( stderr,
	       "Error during monitoring table declaration%s, status: %s\n",
	       useAttributes ? " with attributes" : "",
	       monitorDecodeError( status ) );
      exit( 1 );
    }
  }

  if ( useStatic ) dataBuffer = (unsigned char *)staticDataBuffer;
} /* End of monitorSetup */


/* Get the next event (if possible) */
#define DOTS_AT 5000
int getNextEvent() {
  int status;
  int again;
  int tries = 0;
  do {
    again = FALSE;
    if ( useStatic )
      status = monitorGetEvent( staticDataBuffer, sizeof(staticDataBuffer) );
    else
      status = monitorGetEventDynamic( (void**)&dataBuffer );
    if ( asynchRead && status == 0 ) {
      if ( ( useStatic && *(long32 *)staticDataBuffer == 0 ) ||
	   ( !useStatic && dataBuffer == NULL ) ) {
	if ( ((++tries) % DOTS_AT ) == 0 ) {
	  printf( "*" );
	  fflush( stdout );
	}
	again = TRUE;
      }
    }
  } while ( again );
  if ( asynchRead && tries >= DOTS_AT ) printf( "\n" );
  return status;
} /* End of getNextEvent */


/* Prompt for user action */
void promptUser() {
  int status;
  int action = '\n';
  do {
    if ( numErrors != 0 )
      printf( "%d ERROR%s: ",
	      numErrors,
	      numErrors != 1 ? "S" : "" );
    printf( "n[ext event], q[uit], f[lush], l[ogout]: " );
    fflush( stdout );
    if ( (action = getchar() ) != '\n' ) {
      int c;
      do { c = getchar(); } while ( c != '\n' && c != EOF);
    } else {
      action = 'n';
    }
    if ( action == ' ' ) action = 'n';
    if ( action == EOF ) action = 'q';
    if ( action == 'q' ) {
      printf( "Quitting\n" );
      terminate = TRUE;
    } else if ( action == 'f' ) {
      if ( (status = monitorFlushEvents()) != 0 ) {
	printf( "Error flushing: %d (0x%08x): %s\n",
		status, status,
		monitorDecodeError( status ) );
      }
    } else if ( action == 'l' ) {
      if ( (status = monitorLogout()) != 0 ) {
	printf( "Error logging out: %d (0x%08x): %s\n",
		status, status,
		monitorDecodeError( status ) );
      }
    } else if ( action != 'n' ) {
      printf( "Unrecognized command \'%c\'\n", action );
    }
  } while ( action != 'n' && !terminate );
} /* End of promptUser */

/* Write the event we recieved to a raw file */
int writeEvent() {
  int status;

  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;
  int dataSize = ev->eventSize;
  if ( fd == 0 ) {
    printf("Opening file %s\n",filename);
    fd=open( filename, O_CREAT|O_WRONLY|O_TRUNC, 0x001a4 );
    if(fd<=0) {
      printf("Error opening file %s\n",filename);
      terminate=TRUE;
      return(-1);
    }
  }
  status=write( fd, dataBuffer, dataSize );
  if ( !briefOutput )
    printf("Wrote %d/%d bytes to %s\n",status,dataSize,filename);
  if(status==dataSize) {
    return(0);
  } else {
    return(status);
  }
} /* End of writeEvent */

/* Decode the event we have received */
void decodeEvent() {
  char *ptr = dataBuffer;
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;
  if ( TEST_SYSTEM_ATTRIBUTE( ev->eventTypeAttribute, ATTR_SUPER_EVENT ) ) {
    /* Superevent */
    int totSize = ev->eventHeadSize;
    int dataSize = ev->eventSize;
    gotSuperEvents = TRUE;
    printf( "===============================================================================\n" );
    dumpHeader( ev, TRUE );
    while ( totSize != dataSize ) {
	printf( "...............................................................................\n" );
      ptr = &dataBuffer[ totSize ];
      ev = (struct eventHeaderStruct *)ptr;
      if ( dumpHeader( ev, TRUE ) ) dumpData( ev );
      else return;
      totSize += ev->eventSize;
    }
  } else {
    /* Normal event */
    if ( gotSuperEvents )
	printf( "===============================================================================\n" );
    else
      printf( "...............................................................................\n" );
    if ( dumpHeader( ev, TRUE ) ) dumpData( ev );
  }
} /* End of decodeEvent */


/* Check the various numbers to see if it is time to start the output */
int checkNumbers() {
  struct eventHeaderStruct *ev = (struct eventHeaderStruct *)dataBuffer;

  if ( startSerialNbLoaded )
    if ( numEvents == startSerialNb )
      return TRUE;

  if ( startBunchCrossingLoaded )
    if ( EVENT_ID_GET_BUNCH_CROSSING( ev->eventId ) == startBunchCrossing )
      return TRUE;

  if ( startOrbitLoaded )
    if ( EVENT_ID_GET_ORBIT( ev->eventId ) == startOrbit )
      return TRUE;

  if ( startEventIdLoaded )
    if ( EQ_EVENT_ID( ev->eventId, startEventId ) )
	 return TRUE;
	 
  return FALSE;
} /* End of checkNumbers */


/* The main monitor loop */
void monitoringLoop() {
  int  status;
  int  timeOfLastPrint = -1;
  int  outCtr = 0;
  char outFlg[] = { '|', '/', '-', '\\', 0 };
  
  terminate = FALSE;
  do {
    status = getNextEvent();
    numEvents++;
    if ( status == MON_ERR_EOF ) {
      /* End-of-file: terminate the monitoring loop */
      terminate = TRUE;
      
    } else if ( status != 0 ) {
      /* Error: print the corresponding message */
      fprintf( stderr,
	       "Error getting next event: %s\n",
	       monitorDecodeError( status ) );
      
    } else {
      /* Event received OK */
      if ( !doOutput ) doOutput = checkNumbers();
      if ( doOutput ) {
	if ( noOutput ) {
	  /* No output: print a "progress" cursor */
	  int now = (int)time( NULL );
	  if ( timeOfLastPrint == -1 ) timeOfLastPrint = now;
	  if ( timeOfLastPrint != -1 ) {
	    if ( now - timeOfLastPrint >= 1 ) {
	      timeOfLastPrint = now;
	      printf( "\r%c", outFlg[ outCtr++ ] );
	      if ( outFlg[ outCtr ] == 0 ) outCtr = 0;
	      fflush( stdout );
	    }
	  }
	} else if ( wfile ) {
	  if ( writeEvent() ) {
	    terminate = TRUE;
	    break;
	  }
	} else {
	  /* Normal output: print the content of the event */
	  decodeEvent();
	  if ( interactive ) promptUser();
	}
      }
      
      /* If Dynamic mode, free the data buffer and invalidate the pointer */
      if ( !useStatic ) {
	free( dataBuffer );
	dataBuffer = NULL;
      }
    }
    
    if( maxGetEvents > 0 && numEvents >= maxGetEvents )
      terminate = TRUE;

    if ( !interactive )	/* Terminate on error (unless interactive) */
      terminate = terminate | ( numErrors != 0 );
    
  } while ( status == 0 && !terminate );
  if ( noOutput ) printf( "\n" );
  if ( wfile && fd != 0 ) {
    status=close(fd);
  }
} /* End of monitoringLoop */


/* Our main */
int main( int argc, char **argv ) {
  checkConfig();
  initVars();
  handleArgs( argc, argv );
  monitorSetup( argv[0] );
  monitoringLoop();
  if ( checkData && numErrors != 0 )
    fprintf( stderr,
	     "ERROR%s: %d\n", numErrors != 1 ? "s" : "", numErrors );
  exit( 0 );
} /* End of main */
