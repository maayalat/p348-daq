/*
** logfunc.c: Client side C-library for logging to infoLoggerReader
** 
** @author: Simon Lord
**
** $Id: libInfo.c,v 1.15 2006/05/22 14:24:37 chapelan Exp $
**
** Last changed by: $Author: chapelan $
**
** Revision History:
** 05/10/2004 Created
** 29/10/2004 Connect is now attempted 5 times over 5 seconds
** 01/11/2004 Now using SC's slog functions for error logging
** 02/11/2004 Bug fix in connectNow(), changed message format
**            added infoOpen()
** 08/11/2004 Added recovery code for when infoLoggerReader dies
** 23/11/2004 Added microsecond time precision for message timestamp
** 02/12/2004 Commenting and squigglies for everyone!!
** 16/12/2004 S.C.  Log only to file when LOG_TO_FILE defined at compile time.
** 10/01/2005 S.C.  Added printf-like formatted log function: infolog_f().
** 14/01/2005 Client now execs reader if it cannot connect to unix socket.
** 07/04/2005 Added runnumber to data flow when available
** 20/05/2005 Changed socket name from $DATE_SITE to 'infoLogger-$DATE_SITE'
** 17/11/2005 S.C.  Added infoLogTo_f : same as infoLog_f with stream.
              output logs to STDOUT only when DATE_INFOLOGGER_STDOUT=ONLY
              (nothing transmitted to infoLoggerReader). If TRUE, copy.
** 02/12/2005 S.C.  Added "system" to log fields, defined on startup by env. DATE_INFOLOGGER_SYSTEM
**
*/

#define LOG_FACILITY "libInfo"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/times.h>
#include <sys/time.h>
#include <linux/limits.h>
#include <time.h>
#include <fcntl.h>
#include <assert.h>
#include <pwd.h>
#include <stdarg.h>
#include <sys/wait.h>
#include "simplelog.h"
#include "infoLogger.h"

#define MAXRETRY 200
#define VERSION "$Revision: 1.15 $"

#ifndef FALSE
# define FALSE (0 == 1)
#endif
#ifndef TRUE
# define TRUE (0 == 0)
#endif

#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

extern struct tm *localtime_r(const time_t *, struct tm *);

/* Where the log files go in case DATE_SITE_LOGS is not defined */
#define DEFAULT_LOGS_ROOT "/tmp"

/* Where the logDef should write in case no default has been set */
#define DEFAULT_LOG_FILE "defaultLog"

/* Begin of definition for format of the log records */
#define LOG_S         "#%1c"
#define LOG_TIMESTAMP "#%f"
#define LOG_HOSTID    "#%s"
#define LOG_PID       "#%d"
#define LOG_USER      "#%s"
#define LOG_SYSTEM    "#%s"
#define LOG_SOURCE    "#%s"
#define LOG_STREAM    "#%s"
#define LOG_RUNNUMBER "#%d#"

#define LOG_RECORD      LOG_S         \
                        LOG_TIMESTAMP \
                        LOG_HOSTID    \
                        LOG_PID       \
                        LOG_USER      \
                        LOG_SYSTEM    \
                        LOG_SOURCE    \
                        LOG_STREAM    \
                        LOG_RUNNUMBER \

#define LOG_CR '\f' /* Character used to replace CRs for multi-line msgs */

/* End of declaration for format of the log records */

static char  *hostId = NULL;            /* This pointer will also be used for 
					 * static initialisations' control */
static char   hostName[256];            /* Our host name */
static pid_t  myPid;                    /* Our process ID */
static char   panicLog[PATH_MAX];       /* The panic log file */
static char   localLogFile[PATH_MAX];   /* Where our messages go if infoLoggerReader is dead */
static char   defaultLogFile[PATH_MAX]  /* The package default log file */
                               = { 0 };
static const char *unknownHost = "unknownHost";

static int                 logChannel;  /* Channel used for I/O */
static int                 logToStdout=0; /* Set to 1 if log messages should be copied to stdout - set when connecting, depending on DATE_INFOLOGGER_STDOUT */

static char * systemId = "";          /* name of the system originating log messages : DAQ, ECS, ... */

#ifdef LOG_TO_FILE
static int logLocal = 1;    /* If set to 1 then we log to a file */
#else
static int logLocal = 0;    /* If set to 1 then we log to a file */
#endif

static char *facilityName;  /* Pointer to facility name          */
#define USERNAME_UNDEF "unknown user"
static char *username = USERNAME_UNDEF;      /* Pointer to username               */
static time_t start; /* used for calculating when to retry connecting to reader */

/* pointers to various env variables */
static char *dateSite;
static char *dateRoot;
static char *dateInfoLoggerDir;


/* Prototypes for functions */
static int     connectNow(int);
static int     writeMessage(int, char *);
static int     getHostName();
static int     setNonBlocking(int);
static char   *getUserName();
static void    infoPanic(const char * const);

void           infoOpen();
void           infoClose(void);

char   *createRecord(const char * const,
		     const char * const,
		     const char,
		     const char * const);

static void    doLog     (int, int,
			  const char * const,
			  const char * const,
			  const char,
			  const char * const);

void           infoLog   (const char * const,
			  const char,
			  const char * const);

void           infoLogTo (const char * const,
			  const char * const,
			  const char,
			  const char * const);

void           infoLogAll(const char * const,
			  const char * const,
			  const char,
			  const char *);

void           infoLogDef(const char * const,
			  const char,
			  const char * const);



/*
** Print version information into passed buffer pointer
*/
void getLogfuncVersion(char *buf, size_t size){
  snprintf(buf,size,VERSION);
}




/*
** Get users unix username and return it
*/
static char * getUserName(){
  struct passwd *passent;
  passent = getpwuid(getuid());
  if(passent == NULL){
    slog(SLOG_WARNING,"Could not get username: %s\n",strerror(errno));
    return NULL;
  }
  return passent->pw_name;
}




/*
** Get hostname from environment variable HOST and return.
** as we'll want the hostname in each message header.
*/
static int getHostName(){
  int status = 0;
  memset(hostName,0,sizeof hostName);
  status = gethostname(hostName,sizeof hostName);
  if(status == -1){
    memset(hostName,0,sizeof hostName);
    strncat(hostName,unknownHost,sizeof hostName);
    infoPanic("Can't find hostname, using \"unkownhost\" instead");
    return -1;
  }
  else
    return 0;
}


/*
** Get current DATE run number
** returns run number, or -1 if unknown
*/
static int getRunNumber(){
  int runnumber=-1;
  char *run;
  
  run=getenv("DATE_RUN_NUMBER");
  if (run==NULL) return -1;
  if (sscanf(run,"%d",&runnumber)!=1) return -1;
  return runnumber;   
}


/*
** Initialize all static variable other functions require
*/
static void initStaticVars(){
  char * siteLogs;
  /* Ignore SIGPIPE so that we catch them on write(2) */
  signal(SIGPIPE,SIG_IGN);

  /*
  ** Register infoClose() function to be ran when client exits, allowing
  ** for a more graceful shutdown.
  */
  atexit(infoClose);
  umask(0);
  logChannel = -1;

  myPid = getpid();
  start = time(NULL);

  /* Find out our hostname */
  getHostName();
  hostId = hostName;

  /* Find out our username */
  infoSetUserName(NULL);

  /* Find out system name */
  systemId = getenv("DATE_INFOLOGGER_SYSTEM");
  if (systemId==NULL) {
    systemId="";
  } 

  /* Set default log file name */
  if(defaultLogFile[0] == 0){
    snprintf(SP(defaultLogFile),"%s",DEFAULT_LOG_FILE);
  }

  /* Init local logs names */
  if((siteLogs = getenv("DATE_SITE_LOGS"))!=NULL){
    snprintf(SP(localLogFile),"%s/libInfo.msg@%s",siteLogs,hostId);

    /* Set panicLog filename */
    snprintf(SP(panicLog),"%s/libInfo.err-%d@%s",siteLogs,(int)getpid(),hostId);
  }
  else {
    snprintf(SP(localLogFile),"%s/libInfo.msg@%s",DEFAULT_LOGS_ROOT,hostId);

    /* Set panicLog filename */
    snprintf(SP(panicLog),"%s/libInfo.err-%d@%s",DEFAULT_LOGS_ROOT,(int)getpid(),hostId);
  }

  slog_set_file(panicLog,SLOG_FILE_OPT_DONT_CREATE_NOW + SLOG_FILE_OPT_IF_UNSET);

  /* Get DATE_SITE for use an env variable to pass to execve of infoLoggerReader.sh */
  dateSite = getenv("DATE_SITE");
  if(dateSite == NULL||strlen(dateSite)<1){
    slog(SLOG_WARNING,"DATE_SITE not defined, may not be able to launch infoLoggerReader if required.");
  }

  /* Get DATE_ROOT for use an env variable to pass to execve of infoLoggerReader.sh */
  dateRoot = getenv("DATE_ROOT");
  if(dateRoot == NULL||strlen(dateRoot)<1){
    slog(SLOG_WARNING,"DATE_ROOT not defined, may not be able to launch infoLoggerReader if required.");
  }

  /* Get DATE_INFOLOGGER_DIR for use an env variable to pass to execve of infoLoggerReader.sh */
  dateInfoLoggerDir = getenv("DATE_INFOLOGGER_DIR");
  if(dateInfoLoggerDir == NULL||strlen(dateInfoLoggerDir)<1){
    slog(SLOG_WARNING,"DATE_INFOLOGGER_DIR not defined, may not be able to launch infoLoggerReader if required.");
  }

  /* Copy log messages to stdout? */
  char *e;
  e=getenv("DATE_INFOLOGGER_STDOUT");
  logToStdout=0;
  if (e!=NULL) {
    if (!strcmp("TRUE",e)) {logToStdout=1;}
    if (!strcmp("ONLY",e)) {logToStdout=2;}
  }

}






/* 
** Written originally by RD, modifed by SL
** Entry used to create a log record using the fields given by the caller
** plus some standard fields that we found out ourselves...
**
** The pointer returned needs to be freed when finished with!!!
*/
char *createRecord(const char * const facility,
		   const char * const stream,
		   const char         severity,
		   const char * const message){
  
  int     i;           /* Counter                               */
  int     headerLen;   /* Length of header                      */
  int     messageLen;  /* Length of message                     */
  int     totalLen;    /* Total length                          */
  int     status = 0;  /* Holds return from system calls        */

  char   *record;      /* Pointer to the formatted message      */
  char    header[150]; /* NOTE: might need to make this bigger  */

  double  time = 0.0;  /* Holds microsecond accuracy time stamp */
  double  sec  = 0.0;  /* Holds seconds                         */
  double  usec = 0.0;  /* Holds microseconds                    */

  struct timeval tv;   /* Filled in by gettimeofday             */


  if(hostId == NULL){
    initStaticVars();
  }

  /* Get microsecond timestamp */
  status = gettimeofday(&tv,NULL);
  if(status == -1){
    infoPanic("gettimeofday failed in doLog()");
  }
  sec = (double)tv.tv_sec;
  usec = (double)tv.tv_usec/1000000;
  time = sec + usec;

  /* Write header */
  snprintf(SP(header),
	   LOG_RECORD,
	   severity,
	   time,
	   hostId,
	   (int)myPid,
	   username,
           systemId,
	   facility,
	   stream,
           getRunNumber()
           );
  headerLen = strlen(header);
  messageLen = strlen(message);
  totalLen = headerLen + messageLen;
  if ((record = malloc(totalLen+2)) == NULL) {
    slog(SLOG_WARNING,"Cannot create log record: %s malloc(2)\n",
	 strerror(errno));
    return NULL;
  }
  strcpy(record, header);
  strcpy(record+headerLen, message);
  record[totalLen] = '\n';
  record[totalLen+1] = 0;

  /* Sanity check for no CRs in the middle of the message: */
  for (i = 0; i != totalLen ; i++){
    if (record[i] == '\n'){
      record[i] = LOG_CR;
    }
  }

  return record;
} /* End of createRecord */






/*
** Main log function for parsing log messages to
** infoLoggerReader process
*/
static void doLog(int                toDefault,
		  int                toFile,
		  const char * const facility,
		  const char * const fileName,
		  const char         severity,
		  const char * const message){
  
  int         status = 0;         /* Holds return from system/function calls */
  char       *msg = NULL;         /* Pointer to the formatted message record */

  /* Initialize if we haven't already */
  if(hostId == NULL){
    initStaticVars();
  }

  /* copy log messages to stdout? */
  if (logToStdout) {
    switch (severity) {
      case 'E':
        fprintf(stdout,"Error - ");
        break;
      case 'F':
        fprintf(stdout,"Fatal - ");
        break;
      default:      
        break;
    }
    fprintf(stdout,"%s\n",message);
    fflush(stdout);
    if (logToStdout==2) return;
  }  

#ifndef LOG_TO_FILE
  /* If we are logging locally try and reconnect to reader */
  if(logLocal){
    /* Retry every 60 seconds or more */
    if(difftime(time(NULL),start)>=60.0){
      status = connectNow(1);
      start = time(NULL); /* Reset for next attempt */
    }
  }
#endif

  /* Connect to server if we're not already connected */
  if (logChannel == -1){
    facilityName = (char *)facility;
    logChannel = connectNow(0);
  }

  /* Send message to desired place */
  if(toDefault){
  /* Create our complete message including header */ 
    msg = createRecord(facility,defaultLogFile,severity,message);
    if(msg == NULL){
      infoPanic(message);
      return;
    }
    status = writeMessage(logChannel,msg);
    if(status == -1){
      infoPanic(msg);
    }
  }

  if(toFile){
    /* 
    ** This should rarely be needed since the infoLoggerReader should always be running
    ** and now handles local writing to disk if infoLoggerServer dies.
    */

    /*
    ** It's upto the infoLoggerServer to check for non default steam
    ** in message header and take the appropriate action.
    */
    if(fileName != NULL){
      /* Create our complete message including header */ 
      msg = createRecord(facility,fileName,severity,message);
    }
    else{
      return;
    }

    if(msg == NULL){
      infoPanic(message);
      return;
    }

    status = writeMessage(logChannel,msg);
    if(status == -1){
      infoPanic(msg);
    }
  }

  free(msg);
  return;
}







/*
** Sets the passed fd to non-blocking io
*/
static int setNonBlocking(int s){

  long opts;

  opts = fcntl(s,F_GETFL);
  if (opts == -1){
    slog(SLOG_WARNING,"F_GETFL: %s: fcntl(2)\n",strerror(errno));
    return -1;
  }
  opts = (opts | O_NONBLOCK);
  if (fcntl(s,F_SETFL,opts) == -1){
    slog(SLOG_WARNING,
	 "Cannot set socket into non blocking mode: %s: fcntl(2)\n",
	 strerror(errno));
    return -1;
  }
  return 0;
}





/*
** Open connection to socket or file on disk if that fails
**
** if option not zero we will attempt to connect to reader
** without any sort of connect time out.
*/
static int connectNow(int option){

  int                 status =  0;  /* Holds return from system calls */
  int                 s      = -1;  /* Socket File descriptor         */
  int                 count;        /* Counter                        */
  int                 tmp_pid = 0;
  int                 forked = 0;

  char buffer[500];
  char scriptPath[256]; /* Path to infoLoggerReader.sh */
  char dateSiteEnv[256];
  char dateRootEnv[256];
  char socketName[256];

  char *myargv[2];
  char *myenv[3];

  static char        *path = NULL;  /* Pointer to the abstract path for connect */
  static struct sockaddr_un  addr1;        /* Holds details for connecting to infoLoggerReader */


  memset(scriptPath,0,sizeof scriptPath);
  memset(dateSiteEnv,0,sizeof dateSiteEnv);
  memset(dateRootEnv,0,sizeof dateRootEnv);
  /* Values required for starting infoLoggerReader if it's not running */
  snprintf(SP(scriptPath),"%s/infoLoggerReader.sh",dateInfoLoggerDir);
  snprintf(SP(dateSiteEnv),"DATE_SITE=%s",dateSite);
  snprintf(SP(dateRootEnv),"DATE_ROOT=%s",dateRoot);
  myargv[0] = scriptPath;
  myargv[1] = 0;
  myenv[0] = dateSiteEnv;
  myenv[1] = dateRootEnv;
  myenv[2] = 0;


  if(path == NULL||strlen(path)<1){
    memset(socketName,0,sizeof socketName);
    snprintf(SP(socketName),"infoLogger-%s",dateSite);
    
    path = socketName;
  }

  if(option){
    /* create a socket to work with */
    s = socket(PF_LOCAL,SOCK_STREAM,0);
    if(s == -1){
      slog(SLOG_ERROR,"%s: socket(2)\n",strerror(errno));
      return -1;
    }
    /* Connect */
    status = connect(s, (struct sockaddr *)&addr1, sizeof(addr1));
    if(!status){
      //      fflush(logChannel);
      close(logChannel);
      logChannel = -1;
      logLocal = 0;
      setNonBlocking(s);
      logChannel = s;
      slog(SLOG_INFO,"We have connected to infoLoggerReader after logging locally");
      /* Let reader know we have local messages stored */
      memset(buffer,0,sizeof buffer);
      snprintf(buffer,sizeof buffer,
	       "We have just connected and have messages stored locally, please retrieve from %s",localLogFile);
      doLog( TRUE, FALSE, LOG_FACILITY, NULL,'I', buffer);
    }
    return status;
  }

  if(!logLocal && (logChannel == -1)){
    /* Get the env var to tell us where to connect to */
    if(path == NULL){
      memset(socketName,0,sizeof socketName);
      snprintf(SP(socketName),"infoLogger-%s",dateSite);      
      path = socketName;
      if(path == NULL||strlen(path)<1){
	slog(SLOG_WARNING,"DATE_SITE not defined");

      }
    }

    /* Create a socket to work with */
    s = socket(PF_LOCAL,SOCK_STREAM,0);
    if(s == -1){
      slog(SLOG_ERROR,"%s: socket(2)\n",strerror(errno));
      status=-1;
    } 
    else {
      /* Init sockaddr */
      bzero(&addr1, sizeof(addr1));
      assert(sizeof path < sizeof addr1.sun_path);
      addr1.sun_family = PF_LOCAL;
      /* +1 so that we use abstract address, see man 7 unix for details!*/
      strncpy(addr1.sun_path+1, path, strlen(path)+1);
      
      /* Try and connect every second for 5 seconds */
      for(count = 0;count < 5;count++){    
        status = connect(s, (struct sockaddr *)&addr1, sizeof(addr1));
	if(status == -1 && !forked){
	  /* infoLoggerReader is not running, so we'll start it */
	  if((tmp_pid = fork()) == -1){
	  /* fork failed */
	  }
	  else if(tmp_pid == 0){
	    /* Child */
	    forked = 1;
	    execve(scriptPath,myargv,myenv);
	    slog(SLOG_ERROR,"infoLoggerReader exec failed: %s",strerror(errno));
	    exit(0); /* This might be too drastic! */
	  }
	  else{
	    wait(NULL); /* wait for script to exit before continuing */
	    forked = 1; /* so we don't fork again on the next iteration of the for loop */
	    sleep(2); /* Script ends before infoLoggerReader is ready to receive messages */
	  }
	}
	else if(status == -1){
	  sleep(1);
	}
        else {
	  /*slog(SLOG_INFO,"Connected to %s, fd: %d\n",addr1.sun_path+1,s);*/
	  
	  break;
        }
      }
    } 
  }
  
  /*
  ** If we haven't connected to infoLoggerReader then
  ** start logging locally.
  */
  if((status == -1) || logLocal){
    if(status == -1){
      slog(SLOG_WARNING, "%s: connect(2)\n",strerror(errno));
      slog(SLOG_WARNING,"Couldn't open socket to infoLogger Reader\n");
    }
    /* No infoLoggerReader running, log to file */
    logLocal = 1;
    
    s = open(localLogFile,
	     O_CREAT|O_WRONLY|O_APPEND,
	     S_IROTH|S_IWOTH|S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
    
    if(s == -1){
      slog(SLOG_ERROR,"%s: open(2): %s\n",strerror(errno),localLogFile);
    }

    /*Let people know where the messages are going. */
    #ifndef LOG_TO_FILE
    slog(SLOG_INFO,"(pid %d) Outputing to: %s\n",getpid(),localLogFile);
    #endif
  }

  setNonBlocking(s);
  return s;
}






/*
** Set default logfile
*/
void infoSetDef(const char * const fileName ){
  strncpy(defaultLogFile,fileName,sizeof defaultLogFile);
  defaultLogFile[(sizeof defaultLogFile)-1]=0; /* ensure NULL terminated */
}




/*
** Check if default log file is set
*/
int infoDefIsSet(){
  return defaultLogFile[0] == 0;
}





/* 
** Log a message to the default log file. 
*/
void infoLog( const char * const facility,
              const char         severity,
              const char * const message ) {
  doLog( TRUE, FALSE, facility, NULL, severity, message );
}





/*
** Log a message to a given log file. 
*/
void infoLogTo( const char * const facility,
                const char * const fileName,
                const char         severity,
                const char * const message ) {
  doLog( FALSE, TRUE, facility, fileName, severity, message );
}





/* 
** Log a message to the default log file and to a given log file.
*/
void infoLogAll( const char * const facility,
                 const char * const fileName,
                 const char         severity,
                 const char * const message ) {
  doLog( TRUE, TRUE, facility, fileName, severity, message );
}





/* 
** Log a message to the default log file.
*/
void infoLogDef( const char * const facility,
                 const char         severity,
                 const char * const message ) {
  doLog( FALSE, TRUE, facility, defaultLogFile, severity, message );
}






/*
** Write the passed message to the passed fd
*/
static int writeMessage(int fd, char *msg){

  int status; /* Holds return form system calls */
  int count;  /* Counter                        */

  for(count = 0; count < MAXRETRY; count++){
    errno = 0;
    status = write(fd, msg, strlen(msg));
    if(status > 0){
      return 0;
    }
    else if((status == -1) && (errno == EAGAIN)){
      usleep(300);
    }
    else if((status == -1) && (errno == EPIPE)){
      slog(SLOG_WARNING,
	   "(pid %d) %s:InfoLoggerReader died, logging to disk.\n",
	   getpid(),
	   strerror(errno));
      close(logChannel);
      logChannel = -1;
      logLocal = 1;
      logChannel = connectNow(0);
      status = writeMessage(logChannel,msg);
      return status;
    }
    else{
      slog(SLOG_WARNING,"%d:%s: write(2)\n",errno,strerror(errno));
      return -1;
    }
  }
  return -1;
}/* End of writeMessage */






/*
** Initialize and open communications with infoLoggerReader
*/
void infoOpen(){
  initStaticVars();
  logChannel = connectNow(0);
}





/*
** Close open fd if possible
*/
void infoClose(){

  int status = 0; /* Holds return from system calls */

  /* delete username */
  if ((username!=NULL) && (username!=USERNAME_UNDEF)) {
    free(username);
  }
  username=USERNAME_UNDEF;

  if(logChannel != (-1)){
    if(!logLocal){
      status = shutdown(logChannel, SHUT_WR);
    }
    if(status == -1){
      printf("%s : infoClose shutdown(2)",strerror(errno));
    }
    status = close(logChannel);
    if(status == -1){
      printf("%s : infoClose close(2)",strerror(errno));
    }
    logChannel = -1;
  }

}




/*
** Print the description message plus the relevant data into our
** "private" log file
*/
static void infoPanic( const char * const msg ) {
  slog(SLOG_WARNING,
       "PANIC: \"%s\" :: logChannel:%d panicLog=\"%s\"\n",
       msg,logChannel,panicLog);

#ifdef DEBUG
  fprintf(stderr,msg);
#endif

} /* End of infoPanic */



#define LOG_MAX_RECORD_SIZE 4096
/* Log a message to the default log stream. */
/* printf-like message formatting, variable number of arguments */
void infoLog_f( const char * const facility,
	      const char         severity,
	      const char * const message,
              ... ) {

  char buffer[LOG_MAX_RECORD_SIZE];         /* buffer to store formatted message */
  va_list     ap;                           /* list of additionnal params */

  va_start(ap, message);
  vsnprintf(buffer,LOG_MAX_RECORD_SIZE,message,ap);
  va_end(ap);

  doLog( TRUE, FALSE, facility, NULL, severity, buffer );
} /* End of infoLog_f */


/* Log a message to the specified log stream */
/* printf-like message formatting, variable number of arguments */
void infoLogTo_f( const char * const stream,
                  const char * const facility,
                  const char         severity,
                  const char * const message,
                  ... ) {

  char buffer[LOG_MAX_RECORD_SIZE];         /* buffer to store formatted message */
  va_list     ap;                           /* list of additionnal params */

  va_start(ap, message);
  vsnprintf(buffer,LOG_MAX_RECORD_SIZE,message,ap);
  va_end(ap);

  doLog( FALSE, TRUE, facility, stream, severity, buffer );
} /* End of infoLogTo_f */


/* set username - if NULL, take current user */
void infoSetUserName( const char * name) {
 
  /* first delete previous one */
  if ((username!=NULL) && (username!=USERNAME_UNDEF)) {
    free(username);
  }

  /* assign new user name */  
  if (name==NULL) {
    name=getUserName();
  } else {
    if (strlen(name)==0) {
      name=getUserName();
    }
  }
  if (name==NULL) {
    username=USERNAME_UNDEF;
  } else {
    username=strdup(name);
  } 
  if (username==NULL) {
    username=USERNAME_UNDEF;
  }
} /* End of infoSetUserName */
