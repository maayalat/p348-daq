// infoLogger.i : SWIG interface for infoLogger library.
// 
// This exports infoLogger functions to scripting languages as loadable modules.
// To load the module from Tcl: load $env(DATE_INFOLOGGER_BIN)/libInfo_tcl.so infoLogger
//
// S.C.  8 Jul 2005  Interface created

%module infoLogger
%{
#include "infoLogger.h"
%}

/* open infoLoggerReader connection */
void infoOpen();

/* close infoLoggerReader connection */
void infoClose();

/* Log message to given facility/stream with given severity */
void infoLogTo(const char *Facility, const char *logStream, const char Severity,const char *Message);

/* Log message to given facility with given severity */
void infoLog(const char *Facility, const char Severity,const char *Message);

/* set user Name (optionnal) */
void infoSetUserName(const char * username);
