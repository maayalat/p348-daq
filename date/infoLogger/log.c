#define ENVIRONMENT_FACILITY "DATE_FACILITY"
#define LOG_FACILITY facility

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/errno.h>
#include "infoLogger.h"

#ifdef Linux
char * cuserid ( char *string );
#endif

#ifndef FALSE
# define FALSE (0 == 1)
#endif
#ifndef TRUE
# define TRUE (0 == 0)
#endif
#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

void usage( char *myName ) {
  fprintf( stderr,
	   "Usage: %s [-?] [-i][-e][-f] [\"Log message\"]\n\
\t-?  This message\n\
\t-i  Sends a \"information\" message (default)\n\
\t-e  Sends a \"error\" message\n\
\t-f  Sends a \"fatal\" message\n\n\
Define the environment variable \"%s\" to set the originator's facility\n",
	   myName,
	   ENVIRONMENT_FACILITY );
}

char * createMsg( const char * const header ) {
#define BUNCH 1000
  int   resultSize = strlen( header ) + BUNCH;
  char *result = malloc( resultSize );
  char *curr;

  if ( result == NULL ) {
    perror( "Cannot malloc. System-dependent status " );
    exit( 1 );
  }

  strcpy( result, header );
  curr = &result[strlen(result)];
  
  while (!feof(stdin)) {
    if ( curr == &result[ resultSize-2 ] ) {
      if ((result = realloc( result, resultSize += BUNCH )) == NULL ) {
	perror( "Cannot malloc. System-dependent status " );
	exit( 1 );
      }
      curr = &result[resultSize-2-BUNCH];
    }
    *curr++ = getchar();
  }
  *--curr = 0;
  for ( curr--; *curr == '\n'; *curr-- = 0 );
  return result;
} /* End of createMsg */

int main( int argc, char **argv ) {
  char *message;
  int   severity;
  char *facility;
  char  header[100];
  int   i;
  int   useStdin = TRUE;

  severity = LOG_INFO;
  for ( i = 1; i < argc; i++ ) {
    if ( argv[i][0] == '-' ) {
      if ( strcmp( argv[i], "-i" ) == 0 ) {
	severity = LOG_INFO;
      } else if ( strcmp( argv[i], "-e" ) == 0 ) {
	severity = LOG_ERROR;
      } else if ( strcmp( argv[i], "-f" ) == 0 ) {
	severity = LOG_FATAL;
      } else if ( strcmp( argv[i], "-help" ) == 0
	       || strcmp( argv[i], "-h" ) == 0
	       || strcmp( argv[i], "-H" ) == 0
	       || strcmp( argv[i], "-?" ) == 0 ) {
	usage( argv[0] );
	exit( 0 );
      }
    } else if ( i < argc - 1 ) {
      usage( argv[0] );
      exit( EINVAL );
    } else useStdin = FALSE;
  }
  
  facility = getenv( ENVIRONMENT_FACILITY );
  if ( facility == NULL || strlen( facility ) == 0 )
    facility = "operator";

/*  snprintf( header, sizeof(header), "=%s=", cuserid( NULL ) );*/
/*  snprintf( header, sizeof(header), "");*/
  header[0]=0;
  
  if ( useStdin ) {
    message = createMsg( header );
  } else {
    if ( (message=malloc( strlen(header)+strlen(argv[argc-1])+1 )) == NULL ) {
      perror( "Malloc failed. System-dependent status " );
      exit( 1 );
    }
    strcpy( message, header );
    strcpy( &message[strlen(message)], argv[argc-1] );
  }
  LOG( severity, message );
  free( message );
  infoClose();
  exit( 0 );
}
