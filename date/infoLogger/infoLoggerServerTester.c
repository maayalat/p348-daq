#include <unistd.h>

#include "transport_client.h"
#include "simplelog.h"
#include "utility.c"


/*
gcc -Wall infoLoggerServerTester.c Linux/libInfo.a -o Linux/log  -L/usr/lib/mysql -lmysqlclient -lcrypt -lnsl -lm -lz -lc -lnss_files -lnss_dns -lresolv -lc -lnss_files -lnss_dns -lresolv -lc -ldl -L/usr/X11R6/lib -lm -lpthread -lTransport -L./Linux -o logToServer
*/

void usage() {
  printf("usage: \n\
  -s server name \n\
  -p port number \n\
  -m message \n\
  -n number of times to send messages\n\
  ");
}


int main(int argc,char **argv){
  TR_client_configuration cfg;
  TR_client_handle h;
  int i;
  TR_file* f;
  TR_blob* b;
  int j,k;
  int option;

  int n_msg=1;
  char *msg="#I#12345.12345#testhost#12345#testuser#testclient#defaultLog#12345#Message";

  cfg.server_name="localhost";
  cfg.server_port=6301;
  cfg.queue_length=1000000;
  cfg.msg_queue_path=NULL;
  cfg.client_name=NULL;
  cfg.proxy_state=TR_PROXY_CAN_NOT_BE_PROXY;
  
  
    /* parse command line parameters */
    while((option = getopt(argc, argv, "s:m:p:n:h")) != -1){
      switch(option){
      case 's': 
        cfg.server_name=optarg;
        break;
      case 'm': 
        msg=optarg;
        break;
      case 'n':
        sscanf(optarg,"%d",&n_msg);
        break;
      case 'p':
        sscanf(optarg,"%d",&cfg.server_port);
        break;

      case 'h':
      case '?':    
        usage();
        return 0;

      default:
        usage();
        return -1;
      }
    }

for (k=0;k<1;k++) {
  h=TR_client_start(&cfg);

  for(j=0;j<n_msg;j++){
   f=TR_file_new();

   b=checked_malloc(sizeof(*b));
   b->value=checked_strdup(msg);

   b->size=strlen(msg);
   b->next=NULL;
   f->id.minId=j;
   f->id.majId=1;
   f->size=strlen(msg);
   f->first=b;

   // TR_file_dump(f);
   TR_client_queueAddFile(h,f);
   TR_file_dec_usage(f);
  }

  for(i=0;i!=1;){
    f=TR_client_getLastFileSent(h);
    if (f!=NULL) {
      // TR_file_dump(f);
      if (f->id.minId%10000==0) {slog(SLOG_INFO,"%d.%d ok\n",f->id.majId,f->id.minId);}
      if ((f->id.minId==n_msg-1)) {i=1;}
      TR_file_dec_usage(f);
    } else {
      usleep(1000);
    }
  }

  TR_client_stop(h);
}
  slog(SLOG_INFO,"%d items left",checked_memstat());

  return 0;
}
