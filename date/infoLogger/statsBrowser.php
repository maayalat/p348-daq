<HTML>
<BODY bgcolor="#000033" text="#ffffff" link="#ffff00" vlink="#cccc00" alink="#ff0000">
<FONT face="Helvetica">
<IMG src="date_logo_logbook_600_75.png" width="600" height="75" alt="DATE logbook">


<?php

function echo_prompt_date_site($date_site){
  echo "
  <form action=\"$this_page\">
  Please provide DATE_SITE:
  <input type=\"text\" name=\"date_site\" value=\"$date_site\">
  <input type=\"submit\" value=\"Select\">
  </form>
  </BODY></HTML>
  ";
}


/* read params from URL
     -date_site       if not defined, entry box
     -run             if not defined, show all
     -qmaxn           number of runs to display. if not defined, auto limit (20). if 0, show all.
     -qoffset         first run to display. if not defined, show latest
     -refresh         autorefresh rate
*/
parse_str($_SERVER['QUERY_STRING'], $params);

$run=$params["run"];
$date_site=$params["date_site"];
$this_page=$_SERVER['PHP_SELF'];
$qoffset=$params["qoffset"];
$qmaxn=$params["qmaxn"];
$p_refresh=$params["refresh"];

/* set defaults */
if (strlen($params["qoffset"])==0) {
  $qoffset=-1;
}
if (strlen($params["qmaxn"])==0) {
  $qmaxn=10;
}
if (strlen($params["refresh"])==0) {
  $p_refresh=0;
}

if ($p_refresh>0) {
  echo "<meta http-equiv=\"refresh\" content=\"${p_refresh}\">\n\n";
}


/* table header */
echo "<TABLE cellspacing=0 cellpadding=20 border=0 align=\"center\"><TR><TD>\n";



/* DATE_SITE definition */
if (strlen($params["date_site"])==0) {
  echo_prompt_date_site($date_site);
  exit(0);
}

/* Setup environment */
$filename="$date_site/tmp/cache.infoLogger.config";
/* use cache file if file is recent */
if (file_exists($filename)) {
  $mt=filemtime($filename);
} else {
  $mt=FALSE;
}
if (($mt==FALSE)||(time()-$mt>60)) {
//  echo "Read configuration from db ...<BR>";
  putenv("DATE_SITE=$date_site");
  exec("/date/infoLogger/infoLoggerConfig.sh -f $filename",$i);
} else {
//  echo "Read configuration from cache ...<BR>";
}
$fd=@fopen($filename,"r");
if ($fd==FALSE) {
  echo "Failed to load infoLogger configuration from ${date_site}<BR><BR>";
  echo_prompt_date_site($date_site);
  exit(0);
};
$file_content = explode( "\n", fread($fd, filesize($filename)));
fclose($fd);

$date_setup=array();
foreach ($file_content as $line) {
  list($key, $value) = sscanf($line, "%s %s");
  if ((strlen($key)==0)  || (ereg("#",$key)!=FALSE)) {
    continue;
  }
  $date_setup[$key]=$value;
}

if ($date_setup[DATE_INFOLOGGER_MYSQL]!="TRUE") {
  echo "DATE_SITE: DATE_INFOLOGGER_MYSQL not TRUE";
  exit(0);
}

if (strlen($date_setup[DATE_INFOLOGGER_MYSQL_USER])==0) {
  echo "DATE_SITE: DATE_INFOLOGGER_MYSQL_USER undefined";
  exit(0);
}
if (strlen($date_setup[DATE_INFOLOGGER_MYSQL_PWD])==0) {
  echo "DATE_SITE: DATE_INFOLOGGER_MYSQL_PWD undefined";
  exit(0);
}
if (strlen($date_setup[DATE_INFOLOGGER_MYSQL_HOST])==0) {
  echo "DATE_SITE: DATE_INFOLOGGER_MYSQL_HOST undefined";
  exit(0);
}
if (strlen($date_setup[DATE_INFOLOGGER_MYSQL_DB])==0) {
  echo "DATE_SITE: DATE_INFOLOGGER_MYSQL_DB undefined";
  exit(0);
}


/*
echo "DATE_SITE=$date_site<BR>";
*/

/*
echo "Connecting $date_setup[DATE_INFOLOGGER_MYSQL_HOST]:$date_setup[DATE_INFOLOGGER_MYSQL_DB]...";
flush(stdout);
*/

/* Connecting, selecting database */
$link = @mysql_connect($date_setup[DATE_INFOLOGGER_MYSQL_HOST],$date_setup[DATE_INFOLOGGER_MYSQL_USER],$date_setup[DATE_INFOLOGGER_MYSQL_PWD])
   or die("Failed to connect to database $date_setup[DATE_INFOLOGGER_MYSQL_HOST]:$date_setup[DATE_INFOLOGGER_MYSQL_DB] : " . mysql_error());
mysql_select_db("$date_setup[DATE_INFOLOGGER_MYSQL_DB]") or die("Could not select database $date_setup[DATE_INFOLOGGER_MYSQL_DB]");

/*
echo "ok!<BR><BR>";
flush(stdout);
*/

if (strlen($run)==0) {

  $query = "select COUNT(*) as nruns,round(avg(time_end-time_start)/60) as avg_run from logbook;";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    // echo "$line[nruns] runs available, average run duration is $line[avg_run] minutes.";
    $nruns=$line[nruns];
  }
  mysql_free_result($result);

  /* limit query */
  if ($qmaxn==0) {
    $l2=$nruns;
  } else {
    $l2=$qmaxn;
  }
  if ($qoffset==-1) {
    $l1=$nruns-$l2;
  } else {
    $l1=$qoffset;
  }
  if ($l1<0) {$l1=0;}

  $lim1=$l1+1;
  $lim2=$l1+$l2;
  $lim3=$l1-$l2;
  if ($lim3<0) $lim3=0;
  $lim4=$l1+$l2;
  if ($lim4+$qmaxn>$nruns) {
    $lim4=$nruns-$qmaxn;
    if ($lim4<0) $lim4=0;
  }
  
  $t_first="<font size=\"-1\"><A href=\"${this_page}?date_site=${date_site}&qoffset=0&qmaxn=${qmaxn}\">&lt;&lt;</A></font>";
  $t_end="<font size=\"-1\"><A href=\"${this_page}?date_site=${date_site}&qoffset=-1&qmaxn=${qmaxn}\">&gt;&gt;</A></font>";
  $t_previous="<font size=\"-1\"><A href=\"${this_page}?date_site=${date_site}&qoffset=${lim3}&qmaxn=${qmaxn}\">&lt;</A></font>";
  $t_next="<font size=\"-1\"><A href=\"${this_page}?date_site=${date_site}&qoffset=${lim4}&qmaxn=${qmaxn}\">&gt;</A></font>";
  
  echo "<BR>${t_first} &nbsp; ${t_previous} &nbsp; $lim1-$lim2 of ${nruns} &nbsp;  ${t_next} &nbsp; ${t_end}";
  if ($qmaxn!=0) {
    echo "&nbsp; <font size=\"-2\"><A href=\"${this_page}?date_site=${date_site}&qmaxn=0\">Show all</A></font>";
  } else {
    echo "&nbsp; <font size=\"-2\"><A href=\"${this_page}?date_site=${date_site}&qmaxn=10\">Limit view</A></font>";
  }
  echo "<BR><BR>\n\n";

  $query = " select run,from_unixtime(time_start,\"%d/%m/%Y\") as \"start
  date\",from_unixtime(time_start,\"%H:%i:%s\") as \"start
  time\",from_unixtime(time_end,\"%H:%i:%s\") as \"end time\", numberOfLDCs, numberOfGDCs, totalEvents, totalData, averageDataRate, averageEventsPerSecond,
  averageDuration, numberOfStreams from logbook order by time_start limit $l1,$l2;";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());

  /* Printing results in HTML */
  echo
  "<table cellpadding=4 border=1>
 
<TR><TH>Run</TH><TH>Date</TH><TH>Start</TH><TH>End</TH><TH>LDCs</TH><TH>GDCs</TH><TH>Total<BR>events</TH><TH>Total<BR>data (GB)</TH><TH>GDC data<BR>rate (MB/s)</TH><TH>GDC Events/s</TH><TH>Duration<BR>(s)</TH><TH>Streams</TH>\n";
  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
     echo "\t<tr>\n";
     $i=1;
     foreach ($line as $col_value) {
       if (strlen($col_value)==0) {
          $fill_cell="&nbsp";
       } else {
          $fill_cell="";
       }
       if ($i==1) {
         $text="<A href=\"${this_page}?date_site=${date_site}&run=${col_value}\">${col_value}</A>";
       } else {
         $text="$col_value";
       }
       echo "\t\t<td>${text}${fill_cell}</td>\n";
       $i=$i+1;
     }
     echo "\t</tr>\n";
  }
  echo "</table>\n";

  /* Free resultset */
  mysql_free_result($result);

} else {

  $query = " select run,from_unixtime(time_start,\"%d/%m/%Y %H:%i:%s\") as \"start\",from_unixtime(time_end,\"%d/%m/%Y %H:%i:%s\") as \"end\",log from logbook where run=\"$run\";";
  $result = mysql_query($query) or die("Query failed : " . mysql_error());

  /* Printing results in HTML */

  while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
    echo "<br>Run $line[run]";
    echo "<br>From $line[start] to $line[end]\n";
    echo "<br><br><pre>$line[log]</pre>\n";
  }
  echo "</table>\n";

  /* Free resultset */
  mysql_free_result($result);


}

/* Closing connection */
mysql_close($link);

echo "<BR><BR><font size=\"-3\">\n";
echo "DATE site is $date_site (<A href=\"${this_page}\">change</A>), using database $date_setup[DATE_INFOLOGGER_MYSQL_DB]@$date_setup[DATE_INFOLOGGER_MYSQL_HOST].<BR>\n";
echo "<A href=\"${this_page}?date_site=${date_site}&refresh=60\">Autorefresh</A> latest runs.<BR>\n";


/* table header */
echo "\n</TD></TR></TABLE>";


?> 

</BODY>
</HTML>
