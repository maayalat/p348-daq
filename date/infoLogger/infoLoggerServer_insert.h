/* Log message interface */
/* 07 Apr 2005 - Added run number to fields */
/* 02 Dec 2005 - Added system to fields */

#include <pthread.h>


#define INSERT_TH_QUEUE_SIZE 100

/* structure used to communicate with an insertion thread */
typedef struct _insert_th{
  struct ptFIFO *     queue;            /**< The queue of messages to be inserted */
  pthread_t           thread;           /**< Handle to the thread */  

  int                 shutdown;         /**< set to 1 to stop thread */
  pthread_mutex_t     shutdown_mutex;   /**< lock on shutdown variable */

} insert_th;


int insert_th_start(insert_th * t);
int insert_th_stop(insert_th * t);
int insert_th_loop(void* arg);
int insert_th_loop_nosql(void* arg);

#include <sys/types.h>

typedef struct _infoLog_msg_t{
  /* all char* are NULL terminated */

  char *  severity;   /* message severity */
  double  timestamp;  /* time message issued. Unix time + fraction of second*/
  char *  hostname;   /* pointer to hostname in 'data' */
  pid_t   pid;        /* pid of sending process */
  char *  username;   /* user running the process logging the message */
  char *  system;     /* origin system of the message (DAQ, ECS, ...) */
  char *  facility;   /* facility sending process belongs to */
  char *  dest;       /* destination of the message (stream) */
  int     run;        /* DATE run number, -1 if undefined */
  char *  message;    /* message */

  void *  data;                 /* data containing all above data - the one to be freed */
  struct _infoLog_msg_t *next;  /* next message */
} infoLog_msg_t;
