/*
**   writetest.c : Utility to test mass logging of 
**                 messages to the infoLoggerReader
**
**   Author : Simon Lord
*/

#define LOG_FACILITY "writetest"
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <getopt.h>
#include "infoLogger.h"

int main(int argc, char **argv){
  int status = 0,count = 0,numToSend = 0,useRandomLength = 0, forkTimes = 0;
  struct timeval  tvStart;
  struct timeval  tvFinish;
  char msg[4096];
  char msg2[4096];
  char originalMsg[4096];
  char option;


  while((option = getopt(argc,argv,"m:n:f:rh"))!=-1){
    switch(option){
    case 'm':
      strncpy(originalMsg,optarg,sizeof originalMsg);
      break;
    case 'n':
      numToSend = atoi(optarg);
      break;
    case 'r':
      useRandomLength = 1;
      break;
    case 'f':
      forkTimes = atoi(optarg);
      break;
    case 'h':
    default:
      usage(argv[0]);
    }
  }

  if(numToSend == 0 || strlen(originalMsg) == 0){
    usage(argv[0]);
  }

  srand(13);

  if(forkTimes){
    for(count = 0; count < (forkTimes - 1) ; count++){
      if(fork()){
	break;
      }
    }
  }

  gettimeofday(&tvStart,NULL);
  for(count = 0; count < numToSend; count++){
    if(useRandomLength){
      snprintf(msg2,(rand() % strlen(originalMsg))+1,"%s",originalMsg);
    }
    else {
      sprintf(msg2,"%s",originalMsg);
    }
    sprintf(msg,"%s(%d)",msg2,count);
    LOG(LOG_INFO,msg);
  }
  gettimeofday(&tvFinish,NULL);
  printf("%s : %d seconds to write %d messages\n",
	 argv[0],
	 (tvFinish.tv_sec - tvStart.tv_sec),
	 numToSend);
  return 0;
}

int usage(char *name) {
  printf("Usage: %s\n\t -m <msg>\n\t -n <number to send>\n\t[-f <n>] Number of instances of %s to run\n\t[-r] use a randomlength of msg\n",name,name);
  exit(0);
}
