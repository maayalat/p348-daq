#pragma once

#include "MManager.h"

unsigned int TrueHits(const vector<MChit>& hits)
{
 unsigned int counter (0);
 for(auto p = hits.begin();p != hits.end(); ++p)
  counter += (int)( (*p).id != -1);

 return counter;
}

bool IsDimuon(const RecoEvent& e)
{
  //search dimuon in first gem
  std::vector<MChit> mmhits = e.mc->GEM1truth;
  bool foundmup(false),foundmun(false);
  for(auto p = mmhits.begin();p != mmhits.end(); ++p)
    {
      if((*p).particle == 13)foundmun = true;
      if((*p).particle == -13)foundmup = true;
    }
  return foundmun && foundmup;
}

bool FindIdInGem(const vector<MChit>& mmhits,const int& ID = 13)
{
  bool found(false);
  for(auto p = mmhits.begin();p != mmhits.end(); ++p)
    {
      if((*p).particle == ID){found = true;break;}
    }
  return found;
}

bool FindIdInEvent(const RecoEvent& e,const int& ID = 13)
{
  bool found(false);
  found = FindIdInGem(e.mc->GEM1truth,ID);
  found = found && FindIdInGem(e.mc->GEM2truth,ID);
  found = found && FindIdInGem(e.mc->GEM3truth,ID);
  found = found && FindIdInGem(e.mc->GEM4truth,ID);
//return true if either one of the two is found
  return found;
}

int GetKShortAcceptance(const RecoEvent& e)
{
  //search p+ or pi- in all GEMS, and require DM to be present in the MCtruth
  std::vector<MChit> mmhits = e.mc->GEM1truth;
  
  const bool foundp = FindIdInEvent(e,-211);
  const bool foundm = FindIdInEvent(e,211);
  
  //return number of tracks found in the decay volume
  if(foundp && foundm)
    return 2;
  else if(foundp || foundm)
    return 1;
  else
    return 0;
}

std::vector<TVector3> TakeHits(const vector<MChit>& hits)
{
  std::vector<TVector3> result;
  unsigned int counter (0);
  for(auto p = hits.begin();p != hits.end(); ++p)
    {
      if((*p).id < 0 )continue;
      TVector3 thisvec((*p).pos);
      result.push_back(thisvec);      
    }
  
  return result;
}

bool CheckNumber(const vector<MChit>& hits,const unsigned int Maxhits)
{
 unsigned int counter (0);
 for(auto p = hits.begin();p != hits.end(); ++p)
  counter += (int)( (*p).id != -1);

 //cout << "counter: " << counter << endl;
 return counter <= Maxhits && counter >= 2;
}

bool CheckHitsSim(const RecoEvent& e,const unsigned int Maxhits)
{
 const bool Splash1 = CheckNumber(e.mc->GEM1truth,Maxhits);
 const bool Splash2 = CheckNumber(e.mc->GEM1truth,Maxhits);
 const bool Splash3 = CheckNumber(e.mc->GEM1truth,Maxhits);
 const bool Splash4 = CheckNumber(e.mc->GEM1truth,Maxhits);

 return Splash1 && Splash2 && Splash3 && Splash4;
}

bool CheckHitsData(const RecoEvent& e,const unsigned int Maxhits)
{
  //count them
  const unsigned int GEM1hits = gem::CountGEMHits(e.GM01);
  const unsigned int GEM2hits = gem::CountGEMHits(e.GM02);
  const unsigned int GEM3hits = gem::CountGEMHits(e.GM03);
  const unsigned int GEM4hits = gem::CountGEMHits(e.GM04);
  
  const bool Splash1 = GEM2hits < Maxhits;
  const bool Splash2 = GEM4hits < Maxhits;
  const bool Splash3 = GEM3hits < Maxhits;
  const bool Splash4 = GEM1hits < Maxhits;

  const bool Min1 = GEM2hits > 1;
  const bool Min2 = GEM4hits > 1;
  const bool Min3 = GEM3hits > 1;
  const bool Min4 = GEM1hits > 1;
  
  return Splash1 && Splash2 && Splash3 && Splash4 && Min1 && Min2 && Min3 && Min4;
}

bool CheckHits(const RecoEvent& e,const unsigned int Maxhits)
{
  if(e.mc)
    return CheckHitsSim(e, Maxhits);
  else
    return CheckHitsData(e, Maxhits);
}
