#pragma once
/* Straw reconstruction code
 *
 * Originally implemented by P.Degen
 * RT additions from V.Volkov's branch
 * Modified and improved by A. Karneyeu, H. Sieber and B. Banto
 *
 */

//Generic function to find if an element of any type exists in list
template <typename T>
bool contains(std::list<T> & listOfElements, const T & element) {
        // Find the iterator if element in list
        auto it = std::find(listOfElements.begin(), listOfElements.end(), element);
        //return if iterator points to end or not. It points to end then it means element
        // does not exists in list
        return it != listOfElements.end();
}


/*
Struct for the straw trackers that contains the channel data (wire number and TDC time) 
for the X and Y planes, as well as declares members that are later going to store the
identified hit clusters for both planes and a member to store the final 2D tracker hits.
*/

struct StWire {
  int wire;
  int time;

  // Identify StWire based on channel
  bool operator==(const StWire &other) const {
    return (wire == other.wire);
  }

  void flipTime() {
    time = -time;
  }
};

struct StCluster {
  std::vector<StWire> wires;

  void clear() { wires.clear(); }

  void add_wire(const StWire& new_wire)
  {
    wires.push_back(new_wire);
  }

  bool has_wire(const int wire) const
  {
    const StWire wt = {wire, 0};
    // Find StWire with same wire channel number
    auto it = find(wires.begin(), wires.end(), wt);
    return (it != wires.end());
  }

  void pop_back()
  {
    wires.pop_back();
  }

  // Return cluster position for 1d plane hit
  double position(double tube2tube) const {
    if (wires.empty()) return nan("");

    // Average position of hits
    double pos = 0.;
    for(const auto &w : wires) pos += w.wire * tube2tube + rtStraw6(w.time);
    pos /= wires.size();

    return pos;
  }
  
  // cluster position with the origin at the plane center
  double position(const GEMGeometry* geom) const {
    // Positions in mm
    const double tube2tube = 0.5 * geom->pitch;
    double pos = position(tube2tube);
    
    // translate into center of surface
    const double center = (geom->size/2.0 - 0.5) * tube2tube;
    pos -= center;
    
    // each coordiante plane is built with two layers of straw tubes:
    //           _   _   _   _ 
    //  . . .   / \ / \ / \ / \    . . .  <- layer1
    //          \_/_\_/_\_/_\_/_
    //   . . .    / \ / \ / \ / \   . . . <- layer2
    //            \_/ \_/ \_/ \_/
    //
    // in result the transversal distance between tubes is 0.5 * tube diameter:
    
    return pos;
  }

  double wire() const {
    if (wires.empty()) return nan("");

    double w = 0.;
    for(const auto &stw : wires) w += stw.wire;
    w /= wires.size();

    return w;
  }

  // Return time of cluster
  double time() const {
    if (wires.empty()) return nan("");

    double t = 0.;
    for(const auto &wire : wires) t += wire.time;
    t /= wires.size();

    return t;
  }

  size_t size() const { return wires.size(); }

  double prob() const {
    return (wires.size() == 1)? 0.5 : 1.0;
  }

};

struct StHit {
  StCluster x;
  StCluster y;
  const ST_calib *calib;
  const GEMGeometry* geom;

  // hit position in untransformed coordinate system
  // TODO: not in use, to be removed?
  double U() const { return x.position(geom); }
  double V() const { return y.position(geom); }
  
  // hit position in local coordinate system
  TVector3 pos() const {
    // Positions in mm
    double wire_x = x.position(geom);
    double wire_y = y.position(geom);
    
    // XY station:
    double xpos = wire_x;
    double ypos = wire_y;

    // UV stations: change coordinates accordingly
    // See also git issue: https://gitlab.cern.ch/P348/p348-daq/-/work_items/103#note_7474944
    if (calib->isUV()) {
      const double stereoAngle = calib->stereoAngle;
      const double u = wire_x;
      const double v = wire_y;
      const double s = (v-u*cos(2*stereoAngle))/sin(2*stereoAngle); // "y" component in U plane; must be within [-300,300]
      const double t = (v*cos(2*stereoAngle)-u)/sin(2*stereoAngle); // "y" component in V plane; must be within [-300,300]
      bool physical = (t>-300 && t<300) && (s>-300 && s<300);
      xpos = physical ? v*cos(stereoAngle)-t*sin(stereoAngle) : nan("");
      ypos = physical ? v*sin(stereoAngle)+t*cos(stereoAngle) : nan("");
    }
    
    if (calib->invertAxis) {
      xpos = -xpos;
    }

    TVector3 pos(xpos, ypos, 0);
    // rotate, no use for the moment and disabled
    //pos.RotateZ(geom->angle);
    
    return pos;
  }
  
  TVector3 abspos() const {return (pos() + geom->pos); }
  
  // check if the hit coordinate is within acceptance region
  // (signaled by non-NaN components)
  bool isValid() const
  {
    const TVector3 p = pos();
    return !(std::isnan(p.X()) || std::isnan(p.Y()));
  }

  double prob() const {return (x.prob() * y.prob()); }
};


ostream& operator<<(ostream& os, const StWire& x)
{
  os << "wire=" << x.wire << " time=" << x.time;
  return os;
}

ostream& operator<<(ostream& os, const StCluster& x)
{
  os << "wire=" << x.wire() << " time=" << x.time() << " size=" << x.size() << " pos=" << x.position(3.);
  return os;
}

ostream& operator<<(ostream& os, const StHit& h)
{
  os << "x=(" << h.x << ") y=(" << h.y << ")";
  return os;
}

bool byWireAsc(const StWire& l, const StWire& r)
{
  if (l.wire == r.wire)
    return (l.time < r.time);
  
  return (l.wire < r.wire);
}

struct Straw {
  // digits data - {wire, time}
  std::vector<StWire> XChannels; // X plane
  std::vector<StWire> YChannels; // Y plane
  
  // clusters following digits grouping - {average wire, average time, cluster size}
  std::vector<StCluster> XClusters; // X plane
  std::vector<StCluster> YClusters; // Y plane
  
  // hits followig X and Y clusters matching - {x cluster, y cluster}
  std::vector<StHit> Hits;

  const GEMGeometry* geom;
  const ST_calib* calib;

  // empty contructor
  Straw(): geom(0), calib(0) {}
  
  bool hasHit() const { return (!Hits.empty()); }
  
  void Init(const ST_calib* _calib, const GEMGeometry* _geom)
  {
    calib = _calib;
    geom  = _geom;
  }

};

/*
 * Straw reconstruction method
 *
 * Originally implemented by P. Degen and E. Depero (ETHZ)
 * and improved on by A. Karneyeu, H. Sieber and B. Banto
 *
The plane reconstruction function implements the algorithm to find clusters of adjacent straw tube hits with in-time TDC measurements. 
This function contains two nested for loops, the first one simply iterating over the (wire, time) tuples, which are sorted by wire in 
ascending order. The second loop looks ahead of the current iterated tuple (the one from the outer loop) and tries to fill the vector 
clusterCandidates with adjacent wires that lie within ± timeWindow ns of the original tuple. Once all hits have been assigned to 
clusterCandidates, the candidates are trimmed down to definite clusters.
*/
void DoStrawRecoPlane(Straw& s, float phase, bool verbose, char plane) {
  
  const bool isX = (plane == 'X' || plane == 'U');
  if (!s.calib->hasTcalib()) return;

  const double pitch = s.geom->pitch;

  // Get the data vector from the correct straw plane
  const std::vector<StWire>& channels = isX ? s.XChannels : s.YChannels;
  
  // apply time shift and time window cut
  std::vector<StWire> vec;
  for (StWire stw : channels) {
    stw.time = s.calib->t0 + phase - stw.time;
    const bool inTime = (stw.time >= 0) && (stw.time <= (int)s.calib->timeWindow);
    if (inTime) vec.push_back(stw);
  }

  if (vec.empty()) return;

  // Sort vector of (channel, time) tuples by channel in ascending order
  sort(vec.begin(), vec.end(), byWireAsc);

  if (verbose)
    for (size_t i = 0; i < vec.size(); ++i)
      cout << s.calib->name << plane << ": " << vec[i] << " pos: " << rtStraw6(vec[i].time) << endl;

  std::vector<StCluster> Clusters; // store here the clusters
  std::list<int> matched; // used to remember index of tuples that have already been clustered

  // Iterate over all hits in the plane
  for (uint i = 0; i < vec.size()-1; i++) {

    const int Channel = vec[i].wire;
    const int Time = vec[i].time;
    const double Dist = rtStraw6(Time);

    // Skip tuples that have already been clustered
    if (contains(matched,Channel)) continue;

    // Define vector to store hits that may belong to a cluster (clusterCandidates)
    StCluster cand;

    // Remember the current channel and reconstructed distance to wire
    int oldChannel = Channel;
    int oldTime = Time;
    double oldDist = Dist;
    double totalDist = Dist;

    // Push back the current hit
    cand.add_wire(vec[i]);

    // For this hit iterate forward looking for cluster candidates
    // i.e. adjacent channel until new channel > channel+3
    for (uint j = i+1; j < vec.size(); j++) {
      const int newChannel = vec[j].wire;
      const int newTime = vec[j].time;
      const double newDist = rtStraw6(newTime);

      // Break when channel[j] too far from channel[i] or not adjacent to previous channel
      if (newChannel > Channel+3 || newChannel - oldChannel != 1) break;
      // Break when total distance between wires is more that threshold
      // TODO: optimize this cut
      if (fabs(newDist) + fabs(oldDist) > pitch) break;

      cand.add_wire(vec[j]);
      oldChannel = newChannel;
      oldTime = newTime;
      oldDist = newDist;
      totalDist += newDist;
    }

    // Now trim down the cluster candidate to a definite cluster

    // Trivial case
    if (cand.size() == 1) {
      Clusters.push_back(cand);
      if (verbose) cout << s.calib->name << plane << " cluster (1-1): " << Clusters.back() << "\n";
      cand.wires.back().flipTime();
      Clusters.push_back(cand);
      if (verbose) cout << s.calib->name << plane << " cluster (1-1): " << Clusters.back() << "\n";
    }
    // For clusters of size > 1, return "average" channel and time
    else if (cand.size() == 2) {
      // Flip time of the last wire, so that rt is negative
      cand.wires.back().flipTime();
      Clusters.push_back(cand);
      if (verbose) cout << s.calib->name << plane << " cluster (2-2): " << Clusters.back() << "\n";
      matched.push_back(cand.wires.back().wire);
    }
/*
    // For clusters of size > 1, return "average" channel and time
    else if (cand.size() == 3) {
      // Push back triple cluster if all three distances are greater than the diameter of a straw
      if (totalDist-Dist-oldDist < 0.25*pitch && oldDist > 0.25*pitch && Dist > 0.25*pitch) {
        // Flip time of the last wire, so that rt is negative
        cand.wires.back().flipTime();
        Clusters.push_back(cand);
        if (verbose) cout << s.calib->name << plane << " cluster (3-3): " << Clusters.back() << "\n";
        matched.push_back(cand.wires.at(1).wire);
        matched.push_back(cand.wires.at(2).wire);
      }
      // Else push a cluster with the first and second hit
      else {
        // Remove third wire
        cand.pop_back();
        // Flip time of the last wire, so that rt is negative
        cand.wires.back().flipTime();
        Clusters.push_back(cand);
        if (verbose) cout << s.calib->name << plane << " cluster (2-2): " << Clusters.back() << "\n";
        matched.push_back(cand.wires.back().wire);
      }
    }
*/
    // Throw away larger clusters because of increasing complexity
    else if (cand.size() > 2) {
      if (verbose) {
        cout << "Cluster of size: " << cand.size() << " detected\n";
        for (const auto &stw : cand.wires)
          cout << stw << endl;
        cout <<endl;
      }
    }
  }

  // Check if last element is not yet part of a cluster
  if (!contains(matched,vec.back().wire)) {
    StCluster cand;
    cand.add_wire(vec.back());
    Clusters.push_back(cand);
    if (verbose) cout << s.calib->name << plane << " cluster (1-1): " << Clusters.back() << "\n";
    cand.wires.back().flipTime();
    Clusters.push_back(cand);
    if (verbose) cout << s.calib->name << plane << " cluster (1-1): " << Clusters.back() << "\n";
  }

  // Store the clusters in correct plane
  if (isX) s.XClusters = Clusters;
  else     s.YClusters = Clusters;
}


/*
 * Previous implementation for large cluster sizes
 *

     else if (cand.size() == 3) {
       // Push back double cluster if second and third hits are not in time window
       if (std::fabs((vec[cand[1]].time-vec[cand[2]].time)) > timeWindow) {
         Clusters.push_back(mkcluster(vec[cand[0]], vec[cand[1]]));
         if (verbose) cout << plane << " cluster (2-3): " << Clusters.back() << "\n";
         matched.push_back(cand[1]);
       }
       else {
         // Else push back triple cluster
         Clusters.push_back(mkcluster(vec[cand[0]], vec[cand[1]], vec[cand[2]]));
         if (verbose) cout << plane << " cluster (3-3): " << Clusters.back() << "\n";
           
         matched.push_back(cand[1]);
         matched.push_back(cand[2]);
       }
     }
    

     else if (cand.size() == 4) {
       // First check if can be split into two double clusters: (1+2) and (3+4)
       if (vec[cand[1]].wire - Channel == 1 && fabs(vec[cand[1]].time - Time) < timeWindow // (1+2)
           && vec[cand[3]].wire - vec[cand[2]].wire == 1 && fabs(vec[cand[3]].time - vec[cand[2]].time) < timeWindow) // (3+4)
         {
           // If so only push back first double cluster (1+2) because the second one may belong to a triple cluster (3+4+5)
           Clusters.push_back(mkcluster(vec[cand[0]], vec[cand[1]]));
           if (verbose) cout << plane << " cluster (2-4): " << Clusters.back() << "\n";
           matched.push_back(cand[1]);
         }
       // Else check if triple cluster (1+2+3)
       else if (vec[cand[1]].wire - Channel == 1 && vec[cand[2]].wire - vec[cand[1]].wire == 1
                && fabs(max(Time, (double)max(vec[cand[2]].time,vec[cand[1]].time)) - min(Time,(double)min(vec[cand[2]].time,vec[cand[1]].time))) < timeWindow) {
         Clusters.push_back(mkcluster(vec[cand[0]], vec[cand[1]], vec[cand[2]]));
         if (verbose) cout << plane << " cluster (3-4): " << Clusters.back() << "\n";
         matched.push_back(cand[1]);
         matched.push_back(cand[2]);
       }
       // Else push back double cluster (1+2)
       else {
         Clusters.push_back(mkcluster(vec[cand[0]], vec[cand[1]]));
         if (verbose) cout << plane << " cluster (2-4): " << Clusters.back() << "\n";
         matched.push_back(cand[1]);
       }
     }

    // Throw away larger clusters because of increasing complexity
     else if (cand.size() > 4) {
       if (verbose) {
         cout << "Cluster of size: " << cand.size() << " detected\n";
         for (UInt_t k = 0; k < cand.size(); k++)
           cout << vec[cand[k]] << endl;
         cout <<endl;
       }
     }
*/

/*
This function takes the clusters from both planes and pairs them into definite 2D tracker hits.
The pairing is done by minimizing the time difference between the two clusters.
 */
void MatchStrawClusters(Straw& s, bool verbose) {
  
  std::list<uint> matched; // store here clusters that have already been matched

  // Determine which plane has more clusters
  auto smallCluster = s.YClusters;
  auto largeCluster = s.XClusters;
  bool yfirst = true;
  if (s.XClusters.size() < s.YClusters.size()) {
    smallCluster = s.XClusters;
    largeCluster = s.YClusters;
    yfirst = false;
  }

  // Iterate over plane with fewer clusters
  for (uint i = 0; i < smallCluster.size(); i++) {

    // 2D hits are paired by minimizing their time difference
    CS::int16 timeDiff = 2000; // some large number
    CS::int16 smallTime = smallCluster[i].time();
    uint tmp = 1000;
    bool found = false;

    // Look for cluster in other plane with closest time difference
    for (uint j = 0; j < largeCluster.size(); j++) {
      CS::int16 largeTime = largeCluster[j].time();
      if (std::fabs(smallTime-largeTime) < timeDiff && !contains(matched,j)) {
        timeDiff = std::fabs(smallTime-largeTime);
        tmp = j;
        found = true;
      }
    }

    if (found) {
      matched.push_back(tmp);
      if (yfirst) {
        const StHit sh = {largeCluster[tmp], smallCluster[i], s.calib, s.geom};
        if (sh.isValid()) s.Hits.push_back(sh);
      }
      else {
        const StHit sh = {smallCluster[i], largeCluster[tmp], s.calib, s.geom};
        if (sh.isValid()) s.Hits.push_back(sh);
      }
      
      if (verbose) cout << s.calib->name << " - MatchStrawClusters() hit found: " << s.Hits.back() << "\n";
    }
  }
}

// Wrapper function for straw reco
void DoStrawReco(Straw& s, float phase=0., bool verbose=false)
{
  DoStrawRecoPlane(s, phase, verbose, 'X');
  DoStrawRecoPlane(s, phase, verbose, 'Y');
  //FindStrawClusters(s, verbose);
  MatchStrawClusters(s, verbose);
}
