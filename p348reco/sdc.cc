#include "sdc.h"

namespace p348 {

sdc::Documents<RunNo_t>
instantiate_default_docs(const std::string & rootpath) {
    // create calibration document index by run number
    sdc::Documents<RunNo_t> docs;
    // Create a loader object and add it to the index for automated binding.
    // This type of loader (ExtCSVLoader) is pretty generic one and implies
    // a kind of "extended" grammar for CSV-like files.
    auto extCSVLoader = std::make_shared< sdc::ExtCSVLoader<RunNo_t> >();
    docs.loaders.push_back(extCSVLoader);
    // ^^^ one can customize this loader's grammar by modifying its public
    //     `grammar` attribute or its defaults. For instance, we assume all the
    //     discovered files to have `CaloCellCalib` data type by default:
    extCSVLoader->defaults.dataType = "p348reco/CaloCellCalib";

    // create filesystem iterator to walk through all the dirs and their
    // subdirs looking for files matching certain wildcards and size criteria
    sdc::aux::FS fs( rootpath
                   , "*.txt" // (opt) accept patterns
                   , "*.swp:*.swo:*.bak:*.BAK:*.bck:~*:*-orig.txt:*.dev" // (opt) reject patterns
                   , 10  // (opt) min file size, bytes
                   , 128*1024*1024  // (opt) max file size, bytes
                   );
    // use this iterator to fill documents index by recursively traversing FS
    // subtree and pre-parsing all matching files
    docs.add_from(fs);
    return docs;
}

std::vector<CaloCellCalib>
load_calib(std::string rootpath, int run) {
    // 1. SETUP
    auto docs = instantiate_default_docs(rootpath);

    #if 0  // XXX
    for(const auto & indexItem : docs.validityIndex.entries()) {
        std::cout << indexItem.first << ":" << std::endl;
        for(const auto & subIndexItem : indexItem.second) {
            std::cout << "    #" << subIndexItem.first.v << ", "
                << subIndexItem.second.docID << " - #" << subIndexItem.second.validTo.v
                << std::endl;
        }
    }
    #endif
    #if 0  // XXX
    auto updates = docs.validityIndex.updates("p348reco/CaloCellCalib", {(size_t) run});
    std::cout << "Updates for #" << run << ":" << std::endl;
    for(const auto & upd : updates) {
        std::cout << "   " << upd.first.v << "-" << upd.second->validTo.v
            << ": " << upd.second->docID << std::endl;
    }
    #endif


    // 2. LOAD DATA FOR CERTAIN RUN ID
    // We now load the data into collection.
    // Usually, when no origin info is required for the data being fetched,
    // one can simply call `docs.load<CustomDataType>(runNo)` and that's it.
    // However, for `CaloCellCalib` it is requested to apply additional check
    // on the origin, so we use a templated wrapper `SrcInfo<T>` here to
    // gain some info on the source document for every entry.
    auto l_ = docs.load< sdc::SrcInfo<CaloCellCalib> >({(size_t) run});

    // assure we have no same "name" defined in distinct files (specification
    // policy addendum)
    std::list<CaloCellCalib> l;
    // collection of file sources by name
    std::unordered_map<std::string, std::string> locs;
    {
        for(const sdc::SrcInfo<CaloCellCalib> & cloc : l_) {
            const auto p = p348::split_name(cloc.data.name);
            auto ir = locs.emplace(p.first, cloc.srcDocID);
            if((!ir.second) && ir.first->second != cloc.srcDocID) {
                char errbf[1024];
                snprintf(errbf, sizeof(errbf), "Multiple definitions of"
                        " calibration data entry `CaloCellCalib' for same"
                        " detector kin (%s) revealed for run %d; current"
                        " definition \"%s\" at %s:%zu is conflicting with"
                        " previous one of same kin defined in %s for same run"
                        , p.first.c_str()
                        , run
                        , cloc.data.name.c_str()
                        , cloc.srcDocID.c_str()
                        , cloc.lineNo
                        , ir.first->second.c_str()
                        );
                throw std::runtime_error(errbf);
            }
            // ___ XXX
            //if(cloc.data.name == "ECAL1" && cloc.data.x == 2 && cloc.data.y == 2) {  // XXX
            //    std::cout << "Loading " << cloc.data.name
            //        << "-" << cloc.data.x << "-" << cloc.data.y
            //        << " from " << cloc.srcDocID << ":" << cloc.lineNo
            //        << ", time=" << cloc.data.time
            //        << std::endl;
            //}
            // ^^^ XXX
            l.push_back(cloc.data);
        }
    }

    // filter out duplicating entries (spcification policy addendum)
    // From specification:
    //  For every individual entry only the last cell must be taken, so we
    //  have to remove the previous entries. Unique entry is identified by
    //  name(z)+x+y
    typedef std::tuple<std::string, int, int> CellKey;  // cell key
    // key hashing operator
    struct CellKeyHash
        {
        std::size_t operator()( const CellKey & k ) const {
            return std::hash<std::string>{}(std::get<0>(k))
                 ^ (std::get<1>(k) << 1)
                 ^ (std::get<1>(k) << 2)
                 ;
        }
    };
    // collection of unique cells
    std::unordered_map< CellKey
                      , CaloCellCalib
                      , CellKeyHash
                      > uniqEntries;
    // Set data avoiding NaNs to provide incremental assignment
    for( auto eIt = l.rbegin(); eIt != l.rend(); ++eIt) {
        const CaloCellCalib & cur = *eIt;
        auto ir = uniqEntries.emplace(CellKey{eIt->name, eIt->x, eIt->y}, cur);
        if( ir.second ) continue;
        // insertion failed => this is aduplicate, set attributes of already
        // inserted instance to duplicate's ONLY if they're filling missing
        // data (as we iterate backwards by overriding chain)
        CaloCellCalib & dest = ir.first->second;
        assert(dest.name == cur.name && dest.x == cur.x && dest.y == cur.y);
        // ^^^ guaranteed by map
        if( std::isnan(dest.peakpos) ) dest.peakpos = cur.peakpos;  // append peakpos (from prev)
        if( (!dest.have_time) && cur.have_time ) {  // append time calib (from prev)
            assert(!std::isnan(cur.time));
            //assert(!std::isnan(cur.sigma));
            dest.time = cur.time;
            dest.timesigma = cur.timesigma;
            dest.have_time = true;
        }
        if( (!dest.have_ledref) && cur.have_ledref ) {  // append ledref (from prev)
            assert(!std::isnan(cur.ledref));
            dest.ledref = cur.ledref;
            dest.have_ledref = true;
        }
        if( std::isnan(dest.factor) ) {  // use factor from prev
            dest.factor = cur.factor;
        }
        // ... add here other fields to be overriden!
    }
    // nice side effect of collecting entries by detector kin -- since current
    // p348 policy explicitly restricts one file per single detector kin, we
    // can provide user with brief info on calibration files used
    for( const auto & loc : locs) {
        size_t nEntriesOfKin = 0;
        for( auto e : uniqEntries ) {
            if(0 == std::get<0>(e.first).rfind(loc.first, 0)) {
                ++nEntriesOfKin;
            }
        }
        std::cout << "Info: sdc(): run=" << run << ", file=\""
            << loc.second << "\", "
            << loc.first
            << " (" << nEntriesOfKin << " entries)."
            << std::endl;
    }
    std::vector<CaloCellCalib> calibsList;
    std::transform( uniqEntries.begin(), uniqEntries.end()
                  , std::back_inserter(calibsList)
                  , [](const decltype(uniqEntries)::value_type & p) { return p.second; } );

    // == Addendum: per-part specification as discussed here: =================
    //  https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/347
    // Iterate over calib entries collection looking for factors specified per
    // parts. These per-part factors shall take precedence over the ones given
    // in document's metadata
    std::vector<CaloCellCalib> perPartCalibs;
    for(auto it = calibsList.begin(); it != calibsList.end(); ++it) {
        CaloCellCalib & item = *it;
        if( item.x != std::numeric_limits<decltype(CaloCellCalib::x)>::max()
         && item.y != std::numeric_limits<decltype(CaloCellCalib::y)>::max()
          ) continue;
        // otherwise it is per-part item; has no x/y
        perPartCalibs.push_back(item);
    }
    // remove per-part specifications from calibs list
    calibsList.erase( std::remove_if(calibsList.begin(), calibsList.end(),
        [](const CaloCellCalib & item){ return item.x == std::numeric_limits<decltype(CaloCellCalib::x)>::max()
                                            && item.y == std::numeric_limits<decltype(CaloCellCalib::y)>::max();
                                      })
            , calibsList.end() );
    // apply per-part specifications to calibs list
    for( CaloCellCalib & src : calibsList ) {
        assert(!src.name.empty());
        const CaloCellCalib * thisPartSpec = nullptr;
        // For given item, look for per-part specification and use values for it
        // if own were not specified
        for(const auto & perPartItem : perPartCalibs) {
            if(perPartItem.name != src.name) continue;  // no per-part
            if(!thisPartSpec) {
                thisPartSpec = &perPartItem;
                continue;
            }
            // Prefer one with most detailed specification
            if( thisPartSpec->x == std::numeric_limits<decltype(CaloCellCalib::x)>::min() )
                continue;
            if( thisPartSpec->y == std::numeric_limits<decltype(CaloCellCalib::y)>::min() )
                continue;
            thisPartSpec = &perPartItem;
        }
        if(thisPartSpec) {
            // override with per-part spec
            src.factor = thisPartSpec->factor;
        }
    }
    return calibsList;
}  // load_calib()

}  // namespace p348
