#include "led.h"
#include "conddb.h"

#include <fstream>

  bool LED_spill_id_t::operator < (const LED_spill_id_t& r) const {
    if (run == r.run) return (spill < r.spill);
    return (run < r.run);
  }
  
  bool LED_spill_id_t::operator != (const LED_spill_id_t& r) const {
    return (run != r.run) || (spill != r.spill);
  }

ostream& operator<<(ostream& os, const LED_spill_id_t& id)
{
  os << id.run << "-" << id.spill;
  return os;
}


  // merge taking into account stats
  void LED_event_t::merge(const LED_event_t& a)
  {
    const int ntot = stat + a.stat;
    const float w = ((float)stat) / ntot;
    const float aw = ((float)a.stat) / ntot;
    
    for (int d = 0; d < 2; d++)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal[d][x][y] = w*ecal[d][x][y] + aw*a.ecal[d][x][y];
    
    for (int d = 0; d < 4; d++)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal[d][x][y] = w*hcal[d][x][y] + aw*a.hcal[d][x][y];
    
    for (int d = 0; d < 2; d++)
      for (int x = 0; x < 4; ++x)
        for (int y = 0; y < 4; ++y)
          vhcal[d][x][y] = w*vhcal[d][x][y] + aw*a.vhcal[d][x][y];

    for (int d = 0; d < 3; d++)
          wcal[d] = w*wcal[d] + aw*a.wcal[d];
    
    stat = ntot;
  }

ostream& operator<<(ostream& os, const LED_event_t& led)
{
  os << "stat = " << led.stat << "\n";
  
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; ++x) {
      os << "ecal[d=" << d << ",x=" << x << "] = ";
      for (int y = 0; y < 6; ++y)
        os << led.ecal[d][x][y] << " ";
      os << "\n";
    }
  
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; ++x) {
      os << "hcal[d=" << d << ",x=" << x << "] = ";
      for (int y = 0; y < 3; ++y)
        os << led.hcal[d][x][y] << " ";
      os << "\n";
    }
  
  os << "wcal = ";
  for (int d = 0; d < 3; d++)
        os << led.wcal[d] << " ";
  os << "\n";
  
  return os;
}


map<LED_spill_id_t, LED_event_t> LEDcalib;


LED_event_t ledref;    // Reference run data
LED_event_t ledcurr;   // Current run data
LED_spill_id_t ledcurrid; // Current run data ID (run, spill)

void Set_LED_spill(int nrun, int nspill)
{
  //cout << "Set_LED_spill(): run " << nrun << " spill " << nspill << endl;
  const LED_spill_id_t eid = {nrun, nspill};
  
  // skip if data is set already
  if (!(eid != ledcurrid)) return;
  
  // load LED corrections data for a new spill
  
    // check corrections are available
    LED_spill_id_t loadid = eid;
    bool hasData = false;
    
    // for some of spills LED amplitudes are missing,
    // try to load LED data from the a few of previous spills or from run average
    const int ntry = 3;
    for (int i = 0; i <= ntry; ++i) {
      hasData = (LEDcalib.find(loadid) != LEDcalib.end());
      if (hasData) break;
      
      cout << "WARNING: missing LED corrections data for spill " << loadid << endl;
      
      // navigate to previous spill
      loadid.spill -= 1;
      
      // fallback to run average
      if (i == ntry-1) loadid = eid.getRunId();
    }
    
    if (!hasData) {
      throw std::runtime_error("Missing LED corrections data");
    }
    
    if (eid != loadid) {
      cout << "WARNING: using LED data from the previous spill " << loadid << endl;
    }
    
    ledcurr = LEDcalib[loadid];
    ledcurrid = eid;
    
    // notify
    const int iX=ECAL0BEAMCELL.ix;
    const int iY=ECAL0BEAMCELL.iy;
    const double eref = ledref.ecal[1][iX][iY];
    const double ecurr = ledcurr.ecal[1][iX][iY];
    const double ratio = eref / ecurr;
    cout << "INFO: Set_LED_spill(): loaded LED corrections for"
         << " spill " << ledcurrid << " stat=" << ledcurr.stat
         << " | ECAL1-"<<iX<<"-"<<iY<< " ref=" << eref << " led=" << ecurr << " ratio=" << ratio
         << endl;
}

// resolve run number into the led data file name
// TODO: move the table run range -> filename into SDC .txt
string ledpath(const int run)
{
  char path[100];
  
  const bool is2023A = (8459 <= run && run <= 9717);
  if (is2023A) {
    sprintf(path, "led-2023A-2023dec11/run_%d", run);
    return conddbpath() + path;
  }
  
  const bool is2022B = (6035 <= run && run <= 8458);
  if (is2022B) {
    sprintf(path, "led-2022B-2023dec5/run_%d", run);
    return conddbpath() + path;
  }
  
  const bool is2018 = (3574 <= run && run <= 4310);
  if (is2018) {
    return conddbpath() + "ledspill-2018-v3.txt";
  }
  
  // no check for actual file existence,
  // it will be done in the following call Load_LED_calibrations_xxx()
  return "";
}

string ledpath(const LED_spill_id_t id) { return ledpath(id.run); }

void Load_LED_calibrations(const string fname)
{
  cout << "INFO: Load_LED_calibrations() loading file " << fname << endl;
  LEDcalib.clear();
  
  ifstream f(fname);
  
  if (!f) {
    throw std::runtime_error("Error to open LED calibrations data file");
  }
  
  int nread = 0;
  int nmerge = 0;
  int nruns = 0;
  
  while (true) {
    // Donskov LED record format - 2 lines for each spill, example:
    // stat 00160 RUN     3809 Spill     1 Time 1526594765 Fri May 18 00:06:05 2018
    // [111 float values] = ECAL0,1 (36+36 values) HCAL0,1,2,3 (9+9+9+9 values) WCAL (3 values)
    
    string tmp;
    LED_spill_id_t id = {0, 0};
    
    getline(f, tmp, ' '); // "stat"
    
    int stat;
    f >> stat;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // "RUN"
    
    f >> id.run;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // "Spill"
    
    f >> id.spill;
    f.ignore(1000, '\n'); // "Time ..."
    
    LED_event_t led;
    led.stat = stat;
    
    for (int d = 0; d < 2; d++)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          f >> led.ecal[d][x][y];
    
    for (int d = 0; d < 4; d++)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          f >> led.hcal[d][x][y];
    
    for (int d = 0; d < 3; d++)
          f >> led.wcal[d];
    
    f.ignore(1000, '\n');
    
    // end of file or IO error
    if (!f) break;
    
    // accumulate run average
    const LED_spill_id_t runid = id.getRunId();
    const bool hasRunData = (LEDcalib.find(runid) != LEDcalib.end());
    LED_event_t& runled = LEDcalib[runid];
    if (!hasRunData) {
      runled.stat = 0;
      nruns++;
    }
    runled.merge(led);
    
    const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
    if (hasData) {
      // There are a few duplicate entries in the data file
      // which happens when calib. sequence is split between two .dat files.
      // Merge to single entry:
      led.merge(LEDcalib[id]);
      nmerge++;
      
      //cout << id << " " << led << endl;
      
      // TODO: fix calibration data file
    }
    
    LEDcalib[id] = led;
    nread++;
  }
  
  // check file state
  if (!f.eof()) {
    // file read stop due to format or IO error
    //std::cout << "eof()=" << f.eof() << " fail()=" << f.fail() << endl;
    throw std::runtime_error("Error to read LED calibrations data file - file format or IO error");
  }
  
  const int nload = LEDcalib.size();
  const int nspills = nload - nruns;
  const LED_spill_id_t& id1run = LEDcalib.cbegin()->first;
  const LED_spill_id_t& id1 = LEDcalib.lower_bound(id1run.getNextId())->first;
  const LED_spill_id_t& idN = (--LEDcalib.cend())->first;
  
  cout << "INFO: Load_LED_calibrations()"
       << " total records = " << nload
       << " total runs = " << nruns
       << " total spills = " << nspills
       << " range = " << id1 << " " << idN
       << endl;
  
  if (nread != nspills) {
    cout << "WARNING: Load_LED_calibrations() duplicate records detected,"
         << " nread=" << nread
         << " nspills=" << nspills
         << " ndup=" << (nread - nspills)
         << " nmerge=" << nmerge
         << endl;
  }
}

void Init_LED_calibrations_2018()
{
  // the run number 4310 is the last run of the session 2018,
  // all LED data of 2018 is a single file,
  // and any run number of the session 2018 is good for ledpath() here
  Load_LED_calibrations(ledpath(4310));
  
  // source: /afs/cern.ch/user/d/donskov/public/H4/calibrs/2018.xml
  // ECAL.xml, HCAL.xml, WCAL.xml @ 20May2019
  const LED_event_t refdata = {
    {
      { // ECAL0
        {1330,  950, 2325, 2100, 1725,   85},
        {1190, 1080, 1605, 1170, 1680, 2500},
        { 505, 1430, 1820, 2300, 1775, 2430},
        {1450, 2000, 1485, 2360, 2100, 2970},
        {2350, 1870, 1540, 2340, 1575, 2070},
        {1950, 1495, 1380, 1100, 1975, 2440}
      },
      { // ECAL1
        { 900, 1020,  760, 1020, 1070,  930},
        { 760, 1075,  590,  765, 1320,  243},
        {1700,  720, 1180, 1040,  955, 1200},
        {1310, 1225, 1100,  935, 1340,  730},
        { 285, 1020, 1000, 1080, 1300, 1060},
        { 200,  365,  625,  800,  870, 1555}
      }
    },
    
    { // HCAL 0, 1, 2, 3
      {{1365.6, 1426.4, 1717.8}, {1445.8, 1649.9, 1689.9}, {1703.3, 1327.2, 1756.0}},
      {{ 905.8, 1093.4,  922.2}, {1140.8, 1272.3, 1217.2}, {1553.1,  821.5, 1021.7}},
      {{ 798.2,  914.9,  432.3}, {1094.7,  855.0,  878.0}, { 982.1, 1214.0,  896.3}},
      {{ 920.6,  663.2,  822.1}, { 999.7, 1452.8, 1236.6}, { 929.0,  880.7, 1113.1}}
    },
    
    {0},
    
    // WCAL
    {255.9, 62.7, 407.},
    
    1 // dummy stat
  };
  
  ledref = refdata;
  
  // normalisation for ECAL0-1
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 6; ++y) {
        ledref.ecal[d][x][y] *= 100./111.;
      }
  
  // normalisation HCAL0-3
  // NOTE: the 80./95. factor is extracted only from HCAL0 distribution, and assuming same factor for HCAL1-3
  for (int d = 0; d < 4; d++)
    for (int x = 0; x < 3; ++x)
      for (int y = 0; y < 3; ++y) {
        ledref.hcal[d][x][y] *= 80./95.;
      }
  
  // normalisation WCAL[0]
  ledref.wcal[0] *= 8./5.;
  
  // TODO: WCAL[1] and WCAL[2]
  
  ledcurr = LED_event_t();
  ledcurrid = {-1, -1};
}


void Load_LED_calibrations_Format2022B(const string fname)
{
  cout << "INFO: Load_LED_calibrations_Format2022B() fname=" << fname << endl;
  LEDcalib.clear();
  
  ifstream f(fname);
  
  if (!f) {
    throw std::runtime_error("Error to open LED calibrations data file");
  }
  
  int nread = 0;
  int nruns = 0;
  
  // skip first line
  f.ignore(1000, '\n');
  
  while (true) {
    // Format - up to 5 lines for each spill, example:
    // stat_off  1185 stat_on   143 RUN     7645 Spill     1 Time 1662714055 Fri Sep  9 11:00:55 2022
    // [106 float numbers] 49.5  50.7  ...  49.7  50.4  49.6  50.3
    // [104 float numbers] 49.4  49.8  ...  50.7  50.0
    // [105 float numbers] 1399.5 1072.4 ...    3.3    3.5
    // [105 float numbers] 1399.5 1072.4 ...    3.3    3.5
    
    string tmp;
    LED_spill_id_t id = {0, 0};
    
    getline(f, tmp, ' '); // string "stat_off"
    
    int stat_off;
    f >> stat_off;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "stat_on"
    
    int stat_on;
    f >> stat_on;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "RUN"
    
    f >> id.run;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "Spill"
    
    f >> id.spill;
    f.ignore(1000, '\n'); // skip the rest of line
    
    LED_event_t led = {{0},{0},{0},{0},0};
    led.stat = stat_off;
    
    if (stat_off) {
      // skip the line of pedestal0
      f.ignore(1000, '\n');
      
      // skip the line of pedestal1
      f.ignore(1000, '\n');
      
      // read the line with LEDs OFF_BURST, total 105 numbers
      
      // ECAL is nx=5 x ny=6:  2 * 5*6 = 60 numbers
      for (int d = 0; d < 2; d++)
        for (int x = 0; x < 5; ++x)
          for (int y = 0; y < 6; ++y)
            f >> led.ecal[d][x][y];
      
      // HCAL: 4 * 3*3 = 36 numbers
      for (int d = 0; d < 4; d++)
        for (int x = 0; x < 3; ++x)
          for (int y = 0; y < 3; ++y)
            f >> led.hcal[d][x][y];
      
      // ignore the rest of line with 3 SRD + 6 VETO = 9 numbers
      f.ignore(1000, '\n');
    }
    
    if (stat_on) {
      // skip the line of LEDS IN_BURST
      f.ignore(1000, '\n');
    }
    
    // end of file or IO error
    if (!f) break;
    
    // skip blocks without LEDs OFF_BURST
    if (!stat_off) continue;
    
    // integrity check for duplicate spill id
    const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
    if (hasData) {
      throw std::runtime_error("ERROR: duplicate spill id");
    }
    
    // accumulate run average
    const LED_spill_id_t runid = id.getRunId();
    const bool hasRunData = (LEDcalib.find(runid) != LEDcalib.end());
    LED_event_t& runled = LEDcalib[runid];
    if (!hasRunData) {
      runled.stat = 0;
      nruns++;
    }
    runled.merge(led);
    
    LEDcalib[id] = led;
    nread++;
  }
  
  // check file state
  if (!f.eof()) {
    // file read stop due to format or IO error
    //std::cout << "eof()=" << f.eof() << " fail()=" << f.fail() << endl;
    throw std::runtime_error("Error to read LED calibrations data file - file format or IO error");
  }
  
  const int nload = LEDcalib.size();
  const int nspills = nload - nruns;
  const LED_spill_id_t& id1run = LEDcalib.cbegin()->first;
  const LED_spill_id_t& id1 = LEDcalib.lower_bound(id1run.getNextId())->first;
  const LED_spill_id_t& idN = (--LEDcalib.cend())->first;
  
  cout << "INFO: Load_LED_calibrations_Format2022B()"
       << " total records = " << nload
       << " total runs = " << nruns
       << " total spills = " << nspills
       << " range = " << id1 << " " << idN
       << endl;
  
  if (nread != nspills) {
    cout << "WARNING: Load_LED_calibrations_Format2022B() duplicate records detected,"
         << " nread=" << nread
         << " nspills=" << nspills
         << " ndup=" << (nread - nspills)
         << endl;
  }
}

void Init_LED_calibrations_2022B(const int run)
{
  Load_LED_calibrations_Format2022B(ledpath(run));
  
  //A.C. 2022B-specific corrections for some runs, some cells
  //See: https://gitlab.cern.ch/P348/p348-daq/-/issues/20#note_6276499

  //1: from 7914 to run 7932 (included)
  //   cells HCAL0-0-2, HCAL0-1-0, HCAL1-0-0, HCAL1-0-2, HCAL2-0-2 have a "double-peak" structure.
  //   fix them to the average of run 7913
  if ((run>=7914)&&(run<=7923)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg=LEDcalib;

    LED_spill_id_t refId = { 7913, 0 };
    Load_LED_calibrations_Format2022B(ledpath(refId));

    float hcal002=LEDcalib[refId].hcal[0][0][2];
    float hcal010=LEDcalib[refId].hcal[0][1][0];
    float hcal100=LEDcalib[refId].hcal[1][0][0];
    float hcal102=LEDcalib[refId].hcal[1][0][2];
    float hcal202=LEDcalib[refId].hcal[2][0][2];

    //re-fix LEDcalib
    LEDcalib=LEDcalibOrg;
    for (int ispill=0;ispill<=200;ispill++){ //no more than 200 spills/run. Fix also average (spill==0)
         const LED_spill_id_t id = {run, ispill};
         const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
         if (hasData) {
           LEDcalib[id].hcal[0][0][2]=hcal002;
           LEDcalib[id].hcal[0][1][0]=hcal010;
           LEDcalib[id].hcal[1][0][0]=hcal100;
           LEDcalib[id].hcal[1][0][2]=hcal102;
           LEDcalib[id].hcal[2][0][2]=hcal202;
         }
    }

  }
  //2: from run 7798 to 7927 (included), cell HCAL3-2-2 shows a decrease in the LED response
  //   that is not reproduced looking at the average HCA3L-2-2 energy (same for in-spill and out-of-spill)
  //   7798 is just after the long MD. To solve, scale the cell amplitude by the ratio, so that
  //   for run 7927(id1) the LEDcalib resembles 7929.
  if ((run >= 7798) && (run <= 7927)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 7927, 0 };
    Load_LED_calibrations_Format2022B(ledpath(refId1));
    float hcal322_id1 = LEDcalib[refId1].hcal[3][2][2];

    LED_spill_id_t refId2 = { 7929, 0 };
    Load_LED_calibrations_Format2022B(ledpath(refId2));
    float hcal322_id2 = LEDcalib[refId2].hcal[3][2][2];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      const LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].hcal[3][2][2] *= hcal322_id2 / hcal322_id1;
      }
    }

  }

  //3: from run 7090 to 7795 (included), cell HCAL2-0-0 shows an increase in the LED response
  //   that is not reproduced looking at the average HCAL2-0-0 energy (same for in-spill and out-of-spill)
  //   7799 is just after the long MD. To solve, scale the cell amplitude by the ratio, so that
  //   for run 7090 (id2) the LEDcalib resembles 7088
  if ((run >= 7090) && (run <= 7795)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 7088, 0 };
    Load_LED_calibrations_Format2022B(ledpath(refId1));
    float hcal200_id1 = LEDcalib[refId1].hcal[2][0][0];

    LED_spill_id_t refId2 = { 7090, 0 };
    Load_LED_calibrations_Format2022B(ledpath(refId2));
    float hcal200_id2 = LEDcalib[refId2].hcal[2][0][0];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      const LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].hcal[2][0][0] *= hcal200_id1 / hcal200_id2;
      }
    }
  }
  
  // reset unused fields, althrough it might be not needed
  ledref.wcal[0] = 0;
  ledref.wcal[1] = 0;
  ledref.wcal[2] = 0;
  ledref.stat = 1;
  
  // reset the current led data to enforce load of proper table
  // on the next call to Set_LED_spill()
  ledcurr = LED_event_t();
  ledcurrid = {-1, -1};
}


void Load_LED_calibrations_Format2023A(const string fname)
{
  /*
   * LED calibrations for 2023A
   *
   * Main difference to previous years:
   * - Addition of the VHCAL: This adds 16 more entries for each line of each spill
   *
   */
  cout << "INFO: Load_LED_calibrations_Format2023A() fname=" << fname << endl;
  LEDcalib.clear();
  
  ifstream f(fname);
  
  if (!f) {
    throw std::runtime_error("Error to open LED calibrations data file");
  }
  
  int nread = 0;
  int nruns = 0;
  
  // skip first line
  f.ignore(1000, '\n');
  
  while (true) {
    // Format - up to 5 lines for each spill, example:
    // stat_off   270 stat_on   137 RUN     9201 Spill     1 Time 1686606222 Mon Jun 12 23:43:42 2023
    // [122 float numbers] 354.0 360.9  ...  339.0 354.2 359.4 357.2
    // [120 float numbers] 323.7 331.5  ...  329.1
    // [121 float numbers] 861.4  727.4 ...    2.9    3.3
    // [121 float numbers] 861.1  728.6 ...    13.6   13.3
    
    string tmp;
    LED_spill_id_t id = {0, 0};
    
    getline(f, tmp, ' '); // string "stat_off"
    
    int stat_off;
    f >> stat_off;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "stat_on"
    
    int stat_on;
    f >> stat_on;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "RUN"
    
    f >> id.run;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "Spill"
    
    f >> id.spill;
    f.ignore(1000, '\n'); // skip the rest of line
    
    LED_event_t led = {{0},{0},{0},{0},0};
    led.stat = stat_off;
    
    if (stat_off) {
      // skip the line of pedestal0
      f.ignore(1000, '\n');
      
      // skip the line of pedestal1
      f.ignore(1000, '\n');
      
      // read the line with LEDs OFF_BURST, total 121 numbers
      
      // ECAL is nx=5 x ny=6:  2 * 5*6 = 60 numbers
      for (int d = 0; d < 2; d++)
        for (int x = 0; x < 5; ++x)
          for (int y = 0; y < 6; ++y)
            f >> led.ecal[d][x][y];
      
      // HCAL: 4 * 3*3 = 36 numbers
      for (int d = 0; d < 4; d++)
        for (int x = 0; x < 3; ++x)
          for (int y = 0; y < 3; ++y)
            f >> led.hcal[d][x][y];

      // VHCAL: 1 * 4*4 = 16 numbers
      for (int x = 0; x < 4; ++x)
        for (int y = 0; y < 4; ++y)
          f >> led.vhcal[0][x][y];

      // ignore the rest of line with 3 SRD + 6 VETO = 9 numbers
      f.ignore(1000, '\n');
    }
    
    if (stat_on) {
      // skip the line of LEDS IN_BURST
      f.ignore(1000, '\n');
    }
    
    // end of file or IO error
    if (!f) break;
    
    // skip blocks without LEDs OFF_BURST
    if (!stat_off) continue;
    
    // integrity check for duplicate spill id
    const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
    if (hasData) {
      throw std::runtime_error("ERROR: duplicate spill id");
    }
    
    // accumulate run average
    const LED_spill_id_t runid = id.getRunId();
    const bool hasRunData = (LEDcalib.find(runid) != LEDcalib.end());
    LED_event_t& runled = LEDcalib[runid];
    if (!hasRunData) {
      runled.stat = 0;
      nruns++;
    }
    runled.merge(led);
    
    LEDcalib[id] = led;
    nread++;
  }
  
  // check file state
  if (!f.eof()) {
    // file read stop due to format or IO error
    //std::cout << "eof()=" << f.eof() << " fail()=" << f.fail() << endl;
    throw std::runtime_error("Error to read LED calibrations data file - file format or IO error");
  }
  
  const int nload = LEDcalib.size();
  const int nspills = nload - nruns;
  const LED_spill_id_t& id1run = LEDcalib.cbegin()->first;
  const LED_spill_id_t& id1 = LEDcalib.lower_bound(id1run.getNextId())->first;
  const LED_spill_id_t& idN = (--LEDcalib.cend())->first;
  
  cout << "INFO: Load_LED_calibrations_Format2023A()"
       << " total records = " << nload
       << " total runs = " << nruns
       << " total spills = " << nspills
       << " range = " << id1 << " " << idN
       << endl;
  
  if (nread != nspills) {
    cout << "WARNING: Load_LED_calibrations_Format2023A() duplicate records detected,"
         << " nread=" << nread
         << " nspills=" << nspills
         << " ndup=" << (nread - nspills)
         << endl;
  }
}


void Load_LED_calibrations_Format2023h(const string fname)
{
  /*
   * LED calibrations for 2023 hadron runs
   *
   * Main difference:
   * - No ECAL
   * - Addition of the VHCAL: This adds 16 more entries for each line of each spill
   *
   */
  cout << "INFO: Load_LED_calibrations_Format2023h() fname=" << fname << endl;
  LEDcalib.clear();
  
  ifstream f(fname);
  
  if (!f) {
    throw std::runtime_error("Error to open LED calibrations data file");
  }
  
  int nread = 0;
  int nruns = 0;
  
  // skip first line
  f.ignore(1000, '\n');
  
  while (true) {
    // Format - up to 5 lines for each spill, example:
    // channel sequance ->  HCAL:36,VHCAL:16,VETO:6,LTGT:5,WCAT,V2,CHCNT:2
    // stat_off  1054 stat_on   139 RUN     9592 Spill     1 Time 1688165732 Sat Jul  1 00:55:32 2023
    // [68 float numbers]  356.7 358.8 ... 359.6 398.9
    // [66 float numbers]  355.5 405.2 ... 338.7 388.2
    // [67 float numbers]  803.0 1103.6 ... 2.0    2.3
    // [67 float numbers]    0.0    0.0 ... 4.3   21.8
    
    string tmp;
    LED_spill_id_t id = {0, 0};
    
    getline(f, tmp, ' '); // string "stat_off"
    
    int stat_off;
    f >> stat_off;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "stat_on"
    
    int stat_on;
    f >> stat_on;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "RUN"
    
    f >> id.run;
    f.ignore(100, ' ');
    
    getline(f, tmp, ' '); // string "Spill"
    
    f >> id.spill;
    f.ignore(1000, '\n'); // skip the rest of line
    
    LED_event_t led = {{0},{0},{0},{0},0};
    led.stat = stat_off;
    
    if (stat_off) {
      // skip the line of pedestal0
      f.ignore(1000, '\n');
      
      // skip the line of pedestal1
      f.ignore(1000, '\n');
      
      // read the line with LEDs OFF_BURST, total 67 numbers
     
      // HCAL: 4 * 3*3 = 36 numbers
      for (int d = 0; d < 4; d++)
        for (int x = 0; x < 3; ++x)
          for (int y = 0; y < 3; ++y)
            f >> led.hcal[d][x][y];

      // VHCAL: 1 * 4*4 = 16 numbers
      for (int x = 0; x < 4; ++x)
        for (int y = 0; y < 4; ++y)
          f >> led.vhcal[0][x][y];

      // ignore the rest of line with 6 VETO + 5 LTGT + WCAT + V2 + 2 CHCNT = 15 numbers
      f.ignore(1000, '\n');
    }
    
    if (stat_on) {
      // skip the line of LEDS IN_BURST
      f.ignore(1000, '\n');
    }
    
    // end of file or IO error
    if (!f) break;
    
    // skip blocks without LEDs OFF_BURST
    if (!stat_off) continue;
    
    // integrity check for duplicate spill id
    const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
    if (hasData) {
      throw std::runtime_error("ERROR: duplicate spill id");
    }
    
    // accumulate run average
    const LED_spill_id_t runid = id.getRunId();
    const bool hasRunData = (LEDcalib.find(runid) != LEDcalib.end());
    LED_event_t& runled = LEDcalib[runid];
    if (!hasRunData) {
      runled.stat = 0;
      nruns++;
    }
    runled.merge(led);
    
    LEDcalib[id] = led;
    nread++;
  }
  
  // check file state
  if (!f.eof()) {
    // file read stop due to format or IO error
    //std::cout << "eof()=" << f.eof() << " fail()=" << f.fail() << endl;
    throw std::runtime_error("Error to read LED calibrations data file - file format or IO error");
  }
  
  const int nload = LEDcalib.size();
  const int nspills = nload - nruns;
  const LED_spill_id_t& id1run = LEDcalib.cbegin()->first;
  const LED_spill_id_t& id1 = LEDcalib.lower_bound(id1run.getNextId())->first;
  const LED_spill_id_t& idN = (--LEDcalib.cend())->first;
  
  cout << "INFO: Load_LED_calibrations_Format2023h()"
       << " total records = " << nload
       << " total runs = " << nruns
       << " total spills = " << nspills
       << " range = " << id1 << " " << idN
       << endl;
  
  if (nread != nspills) {
    cout << "WARNING: Load_LED_calibrations_Format2023h() duplicate records detected,"
         << " nread=" << nread
         << " nspills=" << nspills
         << " ndup=" << (nread - nspills)
         << endl;
  }
}


void Init_LED_calibrations_2023h(const int run)
{
  Load_LED_calibrations_Format2023h(ledpath(run));

  // reset unused fields, althrough it might be not needed
  ledref.wcal[0] = 0;
  ledref.wcal[1] = 0;
  ledref.wcal[2] = 0;
  ledref.stat = 1;

  // reset ECAL
  for (int d = 0; d < 2; ++d)
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 6; ++y)
        ledref.ecal[d][x][y] = 0.;

  // reset VHCAL[1]
  for (int x = 0; x < 4; ++x)
    for (int y = 0; y < 4; ++y)
      ledref.vhcal[1][x][y] = 0.;

  // reset the current led data to enforce load of proper table
  // on the next call to Set_LED_spill()
  ledcurr = LED_event_t();
  ledcurrid = {-1, -1};
}


void Init_LED_calibrations_2023A(const int run)
{
  const bool isPeriodHadron = (9592 <= run && run <= 9714);
  if (isPeriodHadron) {
    Init_LED_calibrations_2023h(run);
    return;
  }

  Load_LED_calibrations_Format2023A(ledpath(run));
  
  //A.C. 2023A-specific corrections for some runs, some cells
  //See:  https://gitlab.cern.ch/P348/p348-daq/-/issues/86

  //1: from 8719 to 8722 (included)
  //   cells HCAL1-0-0 and HCAL1-2-2 have a jump and the data does not reproduce this/
  //   fix them so that on average 8719 is equal to 8717, the first run before the jump
  if ((run>=8719)&&(run<=8722)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg=LEDcalib;


    LED_spill_id_t refId1 = {8717, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float hcal100_id1=LEDcalib[refId1].hcal[1][0][0];
    float hcal122_id1=LEDcalib[refId1].hcal[1][2][2];

    LED_spill_id_t refId2 = {8719, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float hcal100_id2=LEDcalib[refId2].hcal[1][0][0];
    float hcal122_id2=LEDcalib[refId2].hcal[1][2][2];


    //re-fix LEDcalib
    LEDcalib=LEDcalibOrg;
    for (int ispill=0;ispill<=200;ispill++){ //no more than 200 spills/run. Fix also average (spill==0)
         LED_spill_id_t id = {run,ispill};
         const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
         if (hasData) {
           LEDcalib[id].hcal[1][0][0]*=hcal100_id1/hcal100_id2;
           LEDcalib[id].hcal[1][2][2]*=hcal122_id1/hcal122_id2;
         }
    }
  }

  //2: from run 8719, cell HCAL1-0-2, HCAL1-1-1, HCAL1-1-2 show an increase in the LED response
  //   that is not reproduced looking at the average energy.
  //   To solve, scale the cell amplitude by the ratio, so that
  //   for run 8719 (id2) the LEDcalib resembles 8717
  //   For cell HCAL1-1-1 there is another jump between 8881 and 8883, that is again not real.
  if ((run >= 8719) && (run <= 9588)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;


    LED_spill_id_t refId1 = { 8717, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float hcal102_id1 = LEDcalib[refId1].hcal[1][0][2];
    float hcal111_id1 = LEDcalib[refId1].hcal[1][1][1];
    float hcal112_id1 = LEDcalib[refId1].hcal[1][1][2];

    LED_spill_id_t refId2 = { 8719, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float hcal102_id2 = LEDcalib[refId2].hcal[1][0][2];
    float hcal111_id2 = LEDcalib[refId2].hcal[1][1][1];
    float hcal112_id2 = LEDcalib[refId2].hcal[1][1][2];

    LED_spill_id_t refId3 = { 8881, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId3));
    float hcal111_id3 = LEDcalib[refId3].hcal[1][1][1];

    LED_spill_id_t refId4 = { 8883, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId4));
    float hcal111_id4 = LEDcalib[refId4].hcal[1][1][1];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = {run,ispill};
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].hcal[1][0][2] *= hcal102_id1 / hcal102_id2;
        LEDcalib[id].hcal[1][1][1] *= hcal111_id1 / hcal111_id2;
        LEDcalib[id].hcal[1][1][2] *= hcal112_id1 / hcal112_id2;
        if (run>=8883){
          LEDcalib[id].hcal[1][1][1] *= hcal111_id3 / hcal111_id4;
        }
      }
    }
  }

  //3: HCAL1-2-0 shows a first jump between 8705 and 8706, and then another in the oppoosite direction from 8717 to 8719.
  //   These are not reproduced looking at the average energy.
  //   To solve, scale the cell amplitude by the ratio, so that
  //   for run 8706 (id2) the LEDcalib resembles 8105
  if ((run >= 8706) && (run <= 8717)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 8705, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float hcal120_id1 = LEDcalib[refId1].hcal[1][2][0];

    LED_spill_id_t refId2 = { 8706, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float hcal120_id2 = LEDcalib[refId2].hcal[1][2][0];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].hcal[1][2][0] *= hcal120_id1 / hcal120_id2;
      }
    }
  }

  //4: HCAL2-1-1 shows some jumps between 8663 and 8706, the bigger being the one between 8705 and 8706.
  //I correct these to compensate for the jump between 8705 and 8706
  if ((run >= 8663) && (run <= 8705)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 8705, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float hcal211_id1 = LEDcalib[refId1].hcal[2][1][1];

    LED_spill_id_t refId2 = { 8706, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float hcal211_id2 = LEDcalib[refId2].hcal[2][1][1];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].hcal[2][1][1] *= hcal211_id2 / hcal211_id1;
      }
    }
  }

  //5: ECAL0-1-1 shows a jump between 8654 and 8656, and another between 8705 and 8706.
  //Both are not seen in data: correct
  if ((run >= 8656)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 8654, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float ecal011_id1 = LEDcalib[refId1].ecal[0][1][1];

    LED_spill_id_t refId2 = { 8656, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float ecal011_id2 = LEDcalib[refId2].ecal[0][1][1];

    LED_spill_id_t refId3 = { 8705, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId3));
    float ecal011_id3 = LEDcalib[refId3].ecal[0][1][1];

    LED_spill_id_t refId4 = { 8706, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId4));
    float ecal011_id4 = LEDcalib[refId4].ecal[0][1][1];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].ecal[0][1][1] *= ecal011_id1 / ecal011_id2;
        if (run >= 8706)
          LEDcalib[id].ecal[0][1][1] *= ecal011_id3 / ecal011_id4;
      }
    }
  }

  //6: ECAL0-1-3 shows a jump between 8683 and 8684, and another between 8705 and 8706.
  //Both are not seen in data: correct
  if ((run >= 8683)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 8683, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float ecal013_id1 = LEDcalib[refId1].ecal[0][1][3];

    LED_spill_id_t refId2 = { 8684, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float ecal013_id2 = LEDcalib[refId2].ecal[0][1][3];

    LED_spill_id_t refId3 = { 8705, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId3));
    float ecal013_id3 = LEDcalib[refId3].ecal[0][1][3];

    LED_spill_id_t refId4 = { 8706, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId4));
    float ecal013_id4 = LEDcalib[refId4].ecal[0][1][3];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].ecal[0][1][3] *= ecal013_id1 / ecal013_id2;
        if (run >= 8706)
          LEDcalib[id].ecal[0][1][3] *= ecal013_id3 / ecal013_id4;
      }
    }
  }

  //7: ECAL0-1-4 shows a jump between 9059 and 9060, not seen by data. Correct
  if ((run >= 9060)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 9059, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float ecal014_id1 = LEDcalib[refId1].ecal[0][1][4];

    LED_spill_id_t refId2 = { 9060, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float ecal014_id2 = LEDcalib[refId2].ecal[0][1][4];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].ecal[0][1][4] *= ecal014_id1 / ecal014_id2;
      }
    }
  }

  //8: ECAL0-3-2 shows a jump between 8654 and 8656, not seen by data. Correct
  if ((run >= 8656)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 8654, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float ecal032_id1 = LEDcalib[refId1].ecal[0][3][2];

    LED_spill_id_t refId2 = { 8656, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float ecal032_id2 = LEDcalib[refId2].ecal[0][3][2];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].ecal[0][3][2] *= ecal032_id1 / ecal032_id2;
      }
    }
  }

  //9: ECAL0-4-2 shows a jump between 8705 and 8706 then 8718 to 8719. Probably, cell was switched off. Led amplitude is very small (~40), hence one would have a very big correction.
  if ((run >= 8706) && (run <= 8718)) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 8705, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float ecal042_id1 = LEDcalib[refId1].ecal[0][4][2];

    LED_spill_id_t refId2 = { 8706, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float ecal042_id2 = LEDcalib[refId2].ecal[0][4][2];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].ecal[0][4][2] *= ecal042_id1 / ecal042_id2;
      }
    }
  }

  //9: ECAL1-0-4 shows a jump between 9234 and 9235. I fix all later runs >= 9252 (after the calib) with the data from 9161 (last before calib)
  if (run >= 9252) {
    //save the original LED calib
    map<LED_spill_id_t, LED_event_t> LEDcalibOrg = LEDcalib;

    LED_spill_id_t refId1 = { 9161, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId1));
    float ecal104_id1 = LEDcalib[refId1].ecal[1][0][4];

    LED_spill_id_t refId2 = { 9252, 0 };
    Load_LED_calibrations_Format2023A(ledpath(refId2));
    float ecal104_id2 = LEDcalib[refId2].ecal[1][0][4];

    //re-fix LEDcalib
    LEDcalib = LEDcalibOrg;
    for (int ispill = 0; ispill <= 200; ispill++) { //no more than 200 spills/run. Fix also average (spill==0)
      LED_spill_id_t id = { run, ispill };
      const bool hasData = (LEDcalib.find(id) != LEDcalib.end());
      if (hasData) {
        LEDcalib[id].ecal[1][0][4] *= ecal104_id1 / ecal104_id2;
      }
    }
  }

  // reset unused fields, althrough it might be not needed
  ledref.wcal[0] = 0;
  ledref.wcal[1] = 0;
  ledref.wcal[2] = 0;
  ledref.stat = 1;
  
  // reset VHCAL[1]
  for (int x = 0; x < 4; ++x)
    for (int y = 0; y < 4; ++y)
      ledref.vhcal[1][x][y] = 0.;

  // reset the current led data to enforce load of proper table
  // on the next call to Set_LED_spill()
  ledcurr = LED_event_t();
  ledcurrid = {-1, -1};
}
