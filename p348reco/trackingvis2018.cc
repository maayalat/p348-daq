// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TF1.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TStyle.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "MManager.h"

/*
Demonstrating tracking in 2018 using four upstream micromegas and MManager as object
*/

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./trackingvis2018.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();

  MManager man;
  
  // stats bar format
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(111);
  gStyle->SetLineScalePS(1);
  
  TFile* file = new TFile("trackingvis2018.root", "RECREATE");

  //two histograms for colloquim
  TH1I momentum_advanced("momentum_advanced", "Reconstructed momentum for electron using advanced algorithm;Energy(GeV);Entries", 300, 0, 300);
  TH1I momentum_simple("momentum_simple", "Reconstructed momentum for electron using simple algorithm;Energy(GeV);Entries", 300, 0, 300);
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // process only "physics" events
    if (!e.isPhysics) continue;
    
    // classic "simple" algorithm
    const SimpleTrack simpletrack = simple_tracking_4MM(e);
    momentum_simple.Fill(simpletrack.momentum);
    
    // advanced algorithm
    man.Reset();
    man.AddMicromega(e.MM3,NOCUT);
    man.AddMicromega(e.MM4,NOCUT);
    man.AddMicromega(e.MM5,NOCUT);
    man.AddMicromega(e.MM6,NOCUT);
    const SimpleTrack track = man.PointDeflectionTracking();
    momentum_advanced.Fill(track.momentum);
  }
  
  TCanvas c("trackingvis2018", "trackingvis2018");
  c.SetGrid();
  c.Print(".pdf[");
  
  c.Clear();
  momentum_simple.Draw();
  c.Print(".pdf");

  c.Clear();
  momentum_advanced.Draw();
  // NOTE: fit range assume 150 GeV track
  momentum_advanced.Fit("gaus", "", "", 120., 180.);
  TF1* fit2 = momentum_advanced.GetFunction("gaus");
  if (fit2) fit2->SetNpx(300);
  c.Print(".pdf");
 
  c.Print(".pdf]");
 
  // save all histograms to .root file
  file->Write();
  file->Close();

  return 0;
}
