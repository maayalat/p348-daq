#pragma once
/* 

header containing the function to process hit and search for vertices in NA64 visible mode
a simple explanation of the algorithm can be found in: https://indico.cern.ch/event/771609/contributions/3206110/attachments/1749970/2835049/pres.pdf

The code was used for the analysis of dimuon sample in 2018, the note regarding that analysis can be found at: https://gitlab.cern.ch/P348/na64-papers/tree/master/NA64-NOTE-19-03
 */
//ROOT
#include <TDatabasePDG.h>
#include <TEveManager.h>
#include <TGeoManager.h>
#include <TRandom.h>
#include <TRandom3.h>
#include <TVector3.h>
#include <TROOT.h>
#include <TClonesArray.h>
#include <TFile.h>
#include <TTree.h>
#include <TDatabasePDG.h>
#include <TMath.h>
#include <TList.h>
//GENFIT
#include <ConstField.h>
#include <Exception.h>
#include <FieldManager.h>
#include <KalmanFitterRefTrack.h>
#include <DAF.h>
#include <StateOnPlane.h>
#include <Track.h>
#include <TrackPoint.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <MaterialEffects.h>
#include <RKTrackRep.h>
#include <AbsTrackRep.h>
#include <TGeoMaterialInterface.h>
#include <EventDisplay.h>
#include <PlanarMeasurement.h>
#include <KalmanFittedStateOnPlane.h>
#include <KalmanFitterInfo.h>
#include "mySpacepointDetectorHit.h"
#include "mySpacepointMeasurement.h"
//GENFIT FIELDS
#include "ConstFieldBox.h"
#include "ConstField2Box.h"
#include "ConstFieldBox.cc"
#include "ConstField2Box.cc"
//NA64
#include "p348reco.h"
#include "conddb.h"
#include "simu.h"

//GLOBALS
const bool ApplyPrePattern = true;
const bool ApplyRefit = true;
//converstion mm to cm
const double mm2cm = 0.1;
//tolerance to hit on the same plan
const double tolerance = 2; //cm
//Magfield in decay volume, to move in conddb.h?
const double DecayVolumeMagField = 0;
//hit error
const double HitError = mm2cm*GEMxres; // in cm
//genfit objects
genfit::Track* fitTrack = nullptr;
genfit::AbsTrackRep* rep = nullptr;
genfit::AbsMeasurement* measurement = nullptr;
// init event display
genfit::EventDisplay* vertexdisplay = nullptr;
//Matrix for the fitting
TMatrixDSym covM(6); //covariance matrix
TMatrixDSym cov(3); // covariance for spacepoint
TVector3 momM; //vector for initial condition

const int pdg = 11;               // particle pdg code,

//FUNCTION TO PREPARE INITIAL CONDITION FOR GENFIT
void InitializeMatrices(const int nMeasurements,const double detectorResolution = 0.1)
{
 // approximate covariance for spacepoint
 for (int ic = 0; ic < 3; ++ic)
  covM(ic,ic) = detectorResolution*detectorResolution;
 for (int ic = 3; ic < 6; ++ic)
  covM(ic,ic) = pow(detectorResolution / nMeasurements / sqrt(3), 2);
 
 // covariance for spacepoint
 for (int ic = 0; ic < 3; ++ic)
  cov(ic,ic) = detectorResolution*detectorResolution;
}

/*
back propagate genfit tracks until the nearest until the point
of minimal distance is reached
 */
double TrackProximity(genfit::Track * track1, genfit::Track * track2, TVector3 * POCA, TVector3 * mom1, TVector3 * mom2) {
    
  // Start from first measurement point
  genfit::StateOnPlane msop1 = track1->getFittedState();
  genfit::StateOnPlane msop2 = track2->getFittedState();

  TVector3 xpos1 = msop1.getPos();
  TVector3 xpos2 = msop2.getPos();
  //  std::cout << "Start pos 1: " << xpos1.X() << ", " << xpos1.Y() << ", " << xpos1.Z() << " cm " <<std::endl;
  //  std::cout << "Start pos 2: " << xpos2.X() << ", " << xpos2.Y() << ", " << xpos2.Z() << " cm " <<std::endl;

  // First determine which direction the tracks will get closer

  double h=0.1;
  int steps=0;
  double d = 99999999;
  double oldd=999999;
  double posd=99999;
  double negd=99999;
  oldd = (xpos1-xpos2).Mag();
  //std::cout << "old distance: " << oldd << std::endl;
  // step with positive h
  try{
    track1->getCardinalRep()->extrapolateBy(msop1,h);
    track2->getCardinalRep()->extrapolateBy(msop2,h);
  }
  catch(genfit::Exception& e){
    std::cerr << e.what();
    std::cerr << "Exception, next track" << std::endl;
  }
  xpos1 = msop1.getPos();
  xpos2 = msop2.getPos();
  //  std::cout << "Step pos 1: " << xpos1.X() << ", " << xpos1.Y() << ", " << xpos1.Z() << " cm " <<std::endl;
  //  std::cout << "Step pos 2: " << xpos2.X() << ", " << xpos2.Y() << ", " << xpos2.Z() << " cm " <<std::endl;
  posd = (xpos1-xpos2).Mag();
  //  std::cout << "new + distance: " << posd << std::endl;

  // step back to original position
  try{
    track1->getCardinalRep()->extrapolateBy(msop1,-h);
    track2->getCardinalRep()->extrapolateBy(msop2,-h);
  }
  catch(genfit::Exception& e){
    std::cerr << e.what();
    std::cerr << "Exception, next track" << std::endl;
  }
  d = (xpos1-xpos2).Mag();

  // step with negative h
  try{
    track1->getCardinalRep()->extrapolateBy(msop1,-h);
    track2->getCardinalRep()->extrapolateBy(msop2,-h);
  }
  catch(genfit::Exception& e){
    std::cerr << e.what();
    std::cerr << "Exception, next track" << std::endl;
  }
  xpos1 = msop1.getPos();
  xpos2 = msop2.getPos();
  //  std::cout << "Step pos 1: " << xpos1.X() << ", " << xpos1.Y() << ", " << xpos1.Z() << " cm " <<std::endl;
  //  std::cout << "Step pos 2: " << xpos2.X() << ", " << xpos2.Y() << ", " << xpos2.Z() << " cm " <<std::endl;
  negd = (xpos1-xpos2).Mag();
  //  std::cout << "new - distance: " << negd << std::endl;

  if(negd < posd){
    h = -1.0*h;
    d = negd;
  }else{
    d = posd;
  }

  while(d < oldd){
    oldd = d;
    try{
      track1->getCardinalRep()->extrapolateBy(msop1,h);
      track2->getCardinalRep()->extrapolateBy(msop2,h);
    }
    catch(genfit::Exception& e){
      std::cerr << e.what();
      std::cerr << "Exception, next track" << std::endl;
    } 
    xpos1 = msop1.getPos();
    xpos2 = msop2.getPos();
    //    std::cout << "Step1: " << steps  << xpos1.X() << ", " << xpos1.Y() << ", " << xpos1.Z() << " cm " <<std::endl;
    //    std::cout << "Step2: " << steps << xpos2.X() << ", " << xpos2.Y() << ", " << xpos2.Z() << " cm " <<std::endl;
    d = (xpos1-xpos2).Mag();
    //    std::cout << "current distance: " << d << std::endl;    
    steps++;
  }

  // step back once 
  try{
    track1->getCardinalRep()->extrapolateBy(msop1,-h);
    track2->getCardinalRep()->extrapolateBy(msop2,-h);
  }
  catch(genfit::Exception& e){
    std::cerr << e.what();
    std::cerr << "Exception, next track" << std::endl;
  } 

  *mom1 = msop1.getMom();
  *mom2 = msop2.getMom();
  
  TVector3 poca = (xpos1+xpos2)*0.5;
  *POCA = poca;
  
  return d;
}

//delete array in genfit
void deleteAll(vector<genfit::Track *>& theElement ) {
  if (theElement.size() == 0)return;
  for(auto it =theElement.begin();it!=theElement.end();++it)
    {
      delete *it;   
    }
  theElement.clear();
}

//boolean to order based on chisq
bool ChiSort(genfit::Track* a,genfit::Track* b)
{
  const double chi2a = a->getFitStatus()->getChi2();
  const double chi2b = b->getFitStatus()->getChi2();
  return chi2a > chi2b;
}

//PREPATTERN CONDITION
bool CheckTrack(const TVector3& M1,const TVector3& M2,const TVector3& M3,const TVector3& M4)
{
 //require the points to lie on the same Y plane
 const double SamePlaneTol = 2; // cm
 //require angle between two line in plane 1-2 and 3-4 to be low
 //const double AngleTol =0.002;
 const double AngleTol =0.009;

 //Check that tracks are on the same Y plane 
 const bool SamePlane = ( (fabs(M1.Y() - M2.Y()) < SamePlaneTol) && (fabs(M2.Y() - M3.Y()) < SamePlaneTol) && (fabs(M3.Y() - M4.Y()) < SamePlaneTol) ) ;
 //cout << "Same Plane: " << SamePlane << endl;
 //check that tracks are in a line to a good degree
 const bool GoodAngle = ((M2-M1).Angle(M4-M3) < AngleTol)
                        &&
                        ((M2-M1).Angle(M3-M2) < AngleTol)
                        &&
                        ((M3-M2).Angle(M4-M3) < AngleTol);
 //cout << "Good Angle: " << GoodAngle << endl;

 //return value
 return (GoodAngle && SamePlane);
}

bool CheckVertex(const vector<TVector3>& hits1,const vector<TVector3>& hits2)
{
 //for double comparison
 const double dtol = mm2cm*MergingDistance;
 bool SameHits = (fabs(hits1[0].X() - hits2[0].X()) > dtol)
                 &&
                 (fabs(hits1[1].X() - hits2[1].X()) > dtol)
                 &&
                 (fabs(hits1[2].X() - hits2[2].X()) > dtol)
                 &&
                 (fabs(hits1[3].X() - hits2[3].X()) > dtol);
 return SameHits;
}


//for box field
void InitBoxField(const double Xoffset,const double MZ1min,const double MZ1max, const double MagField = 0)
{

 //FIELD IMPLEMENTATION, NOT USED IN NA64 AT THE MOMENT FOR VERTEXIN 
 cout << "Start setting the fields" << endl;
 genfit::ConstFieldBox* field = new genfit::ConstFieldBox(0.,10*MagField, 0.0, 
                                                          -1900.0*0.1/2.0 + Xoffset*0.1, 1900.0*0.1/2.0 + Xoffset*0.1, -1000*0.1/2.0, 1000*0.1/2.0, MZ1min, MZ1max
                                                          );//kGauss
  genfit::FieldManager::getInstance()->init(field);
  cout << "GENFIT FIELD SET:" << endl;
}

//for zero field

void InitGeometry(const string geofile)
{
 if(gGeoManager)
  return;
 TGeoManager::SetVerboseLevel(-1);
 TGeoManager::Import(geofile.c_str());
 genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface);

}

void InitUniformField(const double MagField = 0)
{

 //FIELD IMPLEMENTATION, NOT USED IN NA64 AT THE MOMENT FOR VERTEXIN 
 cout << "Start setting the fields" << endl;
 genfit::ConstField* field = new genfit::ConstField(0.,10*MagField, 0.0);
 genfit::FieldManager::getInstance()->init(field);
 cout << "GENFIT FIELD SET Uniform in Y direction: " << MagField << " Tesla" << endl;
}

void GenfitReset()
{
 delete gGeoManager;
}

void GenfitInit(const string geofile)
{
  if(gGeoManager)
   return;

  //Init Geometry
  InitGeometry(geofile);
  //init field, dummy field
  InitUniformField(DecayVolumeMagField);
}

void VertexDisplayOn() { vertexdisplay = genfit::EventDisplay::getInstance(); }

void VertexDisplayShow()
{
 if(vertexdisplay)
  {
   vertexdisplay->open();   
  }
 else
  {
   cout << "display not initialized, cannot show event" << endl;
  }
   
}

void FitTrack(genfit::Track* track)
{
 track->checkConsistency();

 //FIT TRACKS
 genfit::KalmanFitterRefTrack fitter;
 //extra to improve the fit
 fitter.setRefitAll(true);
 fitter.setMinIterations(4);
 fitter.setMaxIterations(10);
 
 try{          
  fitter.processTrack(track);
  
 }catch(genfit::Exception& e){
  std::cerr << e.what();
  std::cerr << "Exception, not possible to complete the fit" << std::endl;
  return;
 }
 track->checkConsistency();
}


void RefitTrack(genfit::Track* track,const TVector3 newpoint)
{
 //prepare coviariance matrix (code copypaste for now)
 double detectorResolution(0.1); // resolution of planar detectors

 
 // setup covariance matries
 InitializeMatrices(5);
 

 //NEW CANDIDATE POINT
 genfit::mySpacepointDetectorHit spointhit5(newpoint, cov);
 genfit::TrackCandHit mycand5(1, 4);

 //NEW MEASUREMENTS
 measurement = new genfit::mySpacepointMeasurement(&spointhit5, &mycand5);
 track->insertPoint(new genfit::TrackPoint(measurement, track));
 
 //REFIT
 FitTrack(track);
 
}

genfit::Track* DoTracking(const TVector3& posM1,const TVector3& posM2,const TVector3& posM3,const TVector3& posM4, const double resolution = 0.1)
{
 //set momentum initial condition
    momM.SetXYZ(0.,0.,15.);

     //prepare coviariance matrix
    InitializeMatrices(4,resolution);
     

     
     // trackrep     
     genfit::AbsTrackRep* rep = new genfit::RKTrackRep(pdg);
     genfit::MeasuredStateOnPlane stateSmeared(rep);
     rep->setPosMomCov(stateSmeared, posM4, momM, covM);
     
     genfit::Track* fitTrack = new genfit::Track(rep, posM4, momM);
     
     //      fitTrack->set_verbosity(1);
     
     // create measurement points
     genfit::mySpacepointDetectorHit spointhit1(posM1, cov);
     genfit::TrackCandHit mycand1(1, 0);
     genfit::mySpacepointDetectorHit spointhit2(posM2, cov);
     genfit::TrackCandHit mycand2(1, 1);
     genfit::mySpacepointDetectorHit spointhit3(posM3, cov);
     genfit::TrackCandHit mycand3(1, 2);
     genfit::mySpacepointDetectorHit spointhit4(posM4, cov);
     genfit::TrackCandHit mycand4(1, 3);
   
   
     genfit::AbsMeasurement* measurement = new genfit::mySpacepointMeasurement(&spointhit1, &mycand1);
     fitTrack->insertPoint(new genfit::TrackPoint(measurement, fitTrack));
     measurement = new genfit::mySpacepointMeasurement(&spointhit2, &mycand2);
     fitTrack->insertPoint(new genfit::TrackPoint(measurement, fitTrack));
     measurement = new genfit::mySpacepointMeasurement(&spointhit3, &mycand3);
     fitTrack->insertPoint(new genfit::TrackPoint(measurement, fitTrack));
     measurement = new genfit::mySpacepointMeasurement(&spointhit4, &mycand4);
     fitTrack->insertPoint(new genfit::TrackPoint(measurement, fitTrack));
     
     fitTrack->checkConsistency();

     //FIT TRACKS
     FitTrack(fitTrack);
     
     return fitTrack;
}

bool ApplyTracking(const TVector3& posM1,const TVector3& posM2,const TVector3& posM3,const TVector3& posM4,const double chicut,const double resolution = 0.012)
{

 
 //check for prepattern condition
 if(ApplyPrePattern)
  {
    const bool IsGoodTrack = CheckTrack(posM1,posM2,posM3,posM4);
   if(!IsGoodTrack) return false;
  }

 //if passed pre-pattern, apply genfit fitting
 fitTrack = DoTracking(posM1,posM2,posM3,posM4,resolution);

 //DECIDE IF TRACK IS VALID OR NOT
 double chisq = fitTrack->getFitStatus()->getChi2() / fitTrack->getFitStatus()->getNdf();
 bool trackstatus = fitTrack->getFitStatus()->isFitConverged();
 //cout << "Track is Converged: " << trackstatus << " with chisq: " << chisq << endl;
 return chisq < chicut && trackstatus;  
 
}


namespace NA64
{
 struct VertexTruth
{
 bool IsReal;     // if the vertex originates from two physical tracks
 bool IsDM;       // if the vertex originates from a Dark Matter Decay
 bool IsDiMuon;   // if the vertex originates from a DiMuon events
 TVector3 pos;    // true position of the vertex
 TLorentzVector Mome;   // momentum of the negative charged particle
 TLorentzVector Momp;   // momentum of the positive charged particle
 vector<MChit> track1truth;  //vector with all MChit used to reconstruct the track1
 vector<MChit> track2truth;  //vector with all MChit used to reconstruct the track2

VertexTruth() : IsReal(false), IsDM(false) {}

 double Angle() const
 {
  return Mome.Angle(Momp.Vect());
 }

 double M() const
 {
  return (Mome + Momp).M()*1000; // MeV by default
 }
  
 
 };
 
 
 struct Vertex
 {
  genfit::Track * track1;
  genfit::Track * track2;  
  TVector3 pos;
  double angle;
  double imass;
  double distance;

  vector<TVector3> hittrack1;
  vector<TVector3> hittrack2;

  std::shared_ptr<VertexTruth> vtruth;

 Vertex() : angle(0), distance(-9999)
  {
   track1 = nullptr;
   track2 = nullptr;
  }

  double GetChi1() const
  {
   const genfit::FitStatus* fit1 = track1->getFitStatus();
   return fit1->getChi2() / fit1->getNdf();
  }

  double GetChi2() const
  {
   const genfit::FitStatus* fit2 = track2->getFitStatus();
   return fit2->getChi2() / fit2->getNdf();
  }

  void RefitVertex()
  {
   if(!track1 || !track2)
    {
     cout << "one of the track is not defined, refit impossible" << endl;
     return;
    }
   //Refit tracks
   RefitTrack(track1,0.1*pos);
   RefitTrack(track2,0.1*pos);
  }

  void ShowVertex()
  {
   if(!vertexdisplay)
    {
     cout << "not display was present, initialize display" << endl;
     VertexDisplayOn();
    }
   else
    {
     vertexdisplay->reset();
    }
   //refit vertex with vertex point inside   
   if(ApplyRefit) {RefitVertex();}
   //show the events
   vector<const genfit::Track*> thistracks;
   thistracks.push_back(track1);
   thistracks.push_back(track2);
   vertexdisplay->addEvent(thistracks);
   vertexdisplay->open();
  }

  TVector3 Vres() const
  {
   if(vtruth)
    return (pos - vtruth->pos);
   else
    {
     cout << "no MC truth avaiable, impossible to calculate resolution of vertex" << endl;
     return TVector3(-9999,-9999,-9999);
    }
  }

  double Angleres() const
  {
   if(vtruth)
    return (angle - vtruth->Angle());
   else
    {
     cout << "no MC truth avaiable, impossible to calculate resolution of angle of vertex" << endl;
     return -9999;
    }
  }
  
 };

 void operator<<(ostream& stream,const VertexTruth& vt)
{
 cout << "TRUE INFORMATION FOR THE VERTEX: " << endl;
 if(vt.IsDM)
  cout << "The vertex originates from a Dark Matter decay "<< endl;
 else  if(vt.IsDiMuon)  
  cout << "The Vertex originates from a Dimuon vertex"<< endl;
 else  if(vt.IsReal)  
  cout << "The Vertex originates from two physical tracks"<< endl;
 else
  cout << "The Vertex originates from fake tracks"<< endl;
 cout << "True position of the vertex was: " << endl;
 vt.pos.Print();
 cout << "Momentum of electron at decay was: " << endl;
 vt.Mome.Print();
 cout << "Momentum of positron at decay was: " << endl;
 vt.Momp.Print();
 cout << "True angle of the vertex was: " << vt.Angle() << " rad" << endl;
 cout << "Invariant mass of the vertex was: " << vt.M() << " MeV" <<endl;
 cout << "TRUE HIT OF FIRST TRACK: " << endl;
 cout << vt.track1truth[0];
 cout << vt.track1truth[1];
 cout << vt.track1truth[2];
 cout << vt.track1truth[3];
 cout << "TRUE HIT OF SECOND TRACK: " << endl;
 cout << vt.track2truth[0];
 cout << vt.track2truth[1];
 cout << vt.track2truth[2];
 cout << vt.track2truth[3];
 
} 

 void operator<<(ostream& stream,const Vertex& v)
{
 cout << endl;
 cout << "RECONSTRUCTION OF THE VERTEX: " << endl;
 cout << "Reconstructed position was: " << endl;
 v.pos.Print();
 cout << "Reconstructed angle was: " << v.angle << " rad" << endl;
 cout << "Reconstructed invariant mass was: " << v.angle << " MeV" << endl;
 cout << "Reconstructed distance between the two line was: " << v.distance << " mm" << endl;
 const genfit::FitStatus* fit1 = v.track1->getFitStatus();
 const genfit::FitStatus* fit2 = v.track2->getFitStatus();
 cout << "chisq of the first Reconstructed track was: " << fit1->getChi2() / fit1->getNdf() << endl;
 cout << "chisq of the second Reconstructed track was: " << fit2->getChi2() / fit2->getNdf() << endl;
 cout << "HIT OF THE FIRST TRACK: " << endl;
 for(auto p = v.hittrack1.begin();p != v.hittrack1.end();++p)
  {
   (*p).Print();
  }
 cout << "HIT OF THE SECOND TRACK: " << endl;
 for(auto p = v.hittrack2.begin();p != v.hittrack2.end();++p)
  {
   (*p).Print();
  }
 cout << endl;
 if(v.vtruth)
  {
   cout << *(v.vtruth);
  }
 else
  {
   cout << "No Monte Carlo Truth present" << endl;
  }
 cout << endl << endl << endl;
}
 
} // end namespace


vector<NA64::Vertex> DoVertexingSim(const RecoEvent& e,std::vector<genfit::Track*>& tracks,const double& chicut = 1.5)
{

 //VECTOR WITH ORIGINAL TRUE INFORMATION ABOUT MCHIT
 vector<MChit> mm1hits = e.mc->GEM1truth;
 vector<MChit> mm2hits = e.mc->GEM2truth;
 vector<MChit> mm3hits = e.mc->GEM3truth;
 vector<MChit> mm4hits = e.mc->GEM4truth;
 
 //DOTRACKING
 //vector to save the tracks
 std::vector<genfit::Track*> tracksSel;
 std::vector< vector<MChit> > tracksSelTruth;
 std::vector< vector<TVector3> > tracksSelHits;
 
 // Track fit for all hit combinations   
 for(unsigned int i = 0; i < mm1hits.size(); i++){
  for(unsigned int j = 0; j < mm2hits.size(); j++){
   for(unsigned int k = 0; k < mm3hits.size(); k++){
    for(unsigned int l = 0; l < mm4hits.size(); l++){

     //set TVector3, changed to cm to follow genfit convention
     TVector3 posM1 = mm2cm*mm1hits[i].pos;
     TVector3 posM2 = mm2cm*mm2hits[j].pos;
     TVector3 posM3 = mm2cm*mm3hits[k].pos;
     TVector3 posM4 = mm2cm*mm4hits[l].pos;
   
     //Fit the track and decide if has to be used
     const bool IsGoodTrack = ApplyTracking(posM1,posM2,posM3,posM4,chicut,HitError);     
     if(!IsGoodTrack) continue;
     
     // Add track to list of track candidates
     tracksSel.push_back(fitTrack);
     vector<MChit> thistruth;
     thistruth.push_back(mm1hits[i]);
     thistruth.push_back(mm2hits[j]);
     thistruth.push_back(mm3hits[k]);
     thistruth.push_back(mm4hits[l]);
     tracksSelTruth.push_back(thistruth);
     vector<TVector3> thishits;
     thishits.push_back(posM1);
     thishits.push_back(posM2);
     thishits.push_back(posM3);
     thishits.push_back(posM4);
     tracksSelHits.push_back(thishits);
     
    }
   }
  }
 }// end loop over MChit

 //save tracks into vector
 tracks = tracksSel;

 //USE TRACK VECTOR TO DO THE VERTEXING TODO: add analytical solution?
 vector<NA64::Vertex> result;
 for(unsigned int i = 0; i < tracksSel.size(); i++)
  {
  for(unsigned int j = i+1; j < tracksSel.size(); j++)
   {
    if(i == j)continue;
    const bool IsVertexGood = CheckVertex(tracksSelHits[i],tracksSelHits[j]);    
    if(!IsVertexGood)continue;
    TVector3 *myPOCA = new TVector3();     
    TVector3 *myMom1 = new TVector3();
    TVector3 *myMom2 = new TVector3();
    //apply vertex reconstruction
    const double vertdist = TrackProximity(tracksSel[i], tracksSel[j], myPOCA, myMom1, myMom2);
    
    //COLLECTED INFORMATION OVER THE VERTEX
    double Eex1 = TMath::Sqrt(emass*emass + myMom1->Mag()*myMom1->Mag());
    double Eex2 = TMath::Sqrt(emass*emass + myMom2->Mag()*myMom2->Mag());
    TLorentzVector lex1(*myMom1, Eex1);
    TLorentzVector lex2(*myMom2, Eex2);
    double imassex = (lex1+lex2).M()*1000;// MeV
    const double ComputedAngle = lex1.Angle(lex2.Vect());

    const vector<MChit> track1truth = tracksSelTruth[i];
    const vector<MChit> track2truth = tracksSelTruth[j];
    const int ID1 = track1truth[0].id;
    const int ID2 = track2truth[0].id;

    //SAVE INFORMATION IN STRUCT
    NA64::Vertex vert;
    vert.vtruth = std::shared_ptr<NA64::VertexTruth>(new NA64::VertexTruth);
    
    //save reconstructed information
    vert.pos = 10 * *myPOCA; // conversion back in mm
    vert.imass = imassex;
    vert.angle = ComputedAngle;
    vert.distance = 10 * vertdist; //conversion back in mm
    //save tracks
    vert.track1 = tracksSel[i];
    vert.track2 = tracksSel[j];
    vert.hittrack1 = tracksSelHits[i];
    vert.hittrack2 = tracksSelHits[j];

    //save truth information
    if((track1truth[1].id == ID1 && track1truth[2].id == ID1 && track1truth[3].id == ID1)
       &&
       ID1 != -1
       &&
       (track2truth[1].id == ID2 && track2truth[2].id == ID2 && track2truth[3].id == ID2)
       &&
       ID2 != -1
       )
     {
      vert.vtruth->IsReal = true;
      const int pid1 = track1truth[0].particle;
      const int pid2 = track2truth[0].particle;
      if((ID1 == e.mc->DMTRID1 && ID2 == e.mc->DMTRID2)
         ||
         (ID2 == e.mc->DMTRID1 && ID1 == e.mc->DMTRID2))
       {
        vert.vtruth->IsDM = true;
        vert.vtruth->IsDiMuon = false;
        //save true information of the vertex
        vert.vtruth->pos = e.mc->DMD;
        vert.vtruth->Mome = e.mc->DMPE;
        vert.vtruth->Momp = e.mc->DMPP;
       }
      else if((pid1 == 13 && pid2 == -13)
             ||
             (pid1 == -13 && pid2 == 13))
       {
        vert.vtruth->IsDiMuon = true;
        vert.vtruth->IsDM = false;
       }
      else
       {
        vert.vtruth->IsDM = false;
        vert.vtruth->IsDiMuon = false;
       }
     }
    else
     {
      vert.vtruth->IsReal = false;
      vert.vtruth->IsDM = false;
      vert.vtruth->IsDiMuon = false;
     }
    //save correspondent MChit into truth
    vert.vtruth->track1truth = track1truth;
    vert.vtruth->track2truth = track2truth;

    //garbage collection
    delete myMom1;
    delete myMom2;
    delete myPOCA;

    //save vertex into the vector
    result.push_back(vert);
   }
  }

  return result;
} // end function do vertexing


  vector<NA64::Vertex> DoVertexingData(const RecoEvent& e,std::vector<genfit::Track*>& tracks,const double& chicut = 1.5)
{

 //VECTOR WHERE TO SAVE GEMS HIT
 // save gem hits using coral wrapper
 std::vector<TVector3> GEM1hits = gem::GetHits(e.GM01);
 std::vector<TVector3> GEM2hits = gem::GetHits(e.GM02);
 std::vector<TVector3> GEM3hits = gem::GetHits(e.GM03);
 std::vector<TVector3> GEM4hits = gem::GetHits(e.GM04);

 
 //DOTRACKING
 //vector to save the tracks
 std::vector<genfit::Track*> tracksSel;
 std::vector< vector<TVector3> > tracksSelHits;

 ////cout<<"PRE LOOP"<<endl;
 // Track fit for all hit combinations, ordered from upstream to downstream 2018
 for(unsigned int i = 0; i < GEM2hits.size(); i++){
  for(unsigned int j = 0; j < GEM4hits.size(); j++){
   for(unsigned int k = 0; k < GEM3hits.size(); k++){
    for(unsigned int l = 0; l < GEM1hits.size(); l++){

     //set TVector3, changed to cm to follow genfit convention, ordered from upstream to downstream
     TVector3 posM1 = mm2cm*GEM2hits[i];
     TVector3 posM2 = mm2cm*GEM4hits[j];
     TVector3 posM3 = mm2cm*GEM3hits[k];
     TVector3 posM4 = mm2cm*GEM1hits[l];     

     //Fit the track and decide if has to be used
     const bool IsGoodTrack = ApplyTracking(posM1,posM2,posM3,posM4,chicut,HitError);
     //cout << "Track is good: " << IsGoodTrack << endl;
     if(!IsGoodTrack) continue;
     
     // Add track to list of track candidates
     tracksSel.push_back(fitTrack);
     vector<TVector3> thishits;
     thishits.push_back(posM1);
     thishits.push_back(posM2);
     thishits.push_back(posM3);
     thishits.push_back(posM4);
     tracksSelHits.push_back(thishits);
     
    }
   }
  }
 } // end loop over MChit

 //save tracks into vector
 tracks = tracksSel;

 //USE TRACK VECTOR TO DO THE VERTEXING TODO: add analytical solution?
 vector<NA64::Vertex> result;
 for(unsigned int i = 0; i < tracksSel.size(); i++)
  {
  for(unsigned int j = i+1; j < tracksSel.size(); j++)
   {
    if(i == j)continue;
    const bool IsVertexGood = CheckVertex(tracksSelHits[i],tracksSelHits[j]);
    if(!IsVertexGood)continue;
    TVector3 *myPOCA = new TVector3();     
    TVector3 *myMom1 = new TVector3();
    TVector3 *myMom2 = new TVector3();
    //apply vertex reconstruction
    const double vertdist = TrackProximity(tracksSel[i], tracksSel[j], myPOCA, myMom1, myMom2);
    
    //COLLECTED INFORMATION OVER THE VERTEX
    double Eex1 = TMath::Sqrt(emass*emass + myMom1->Mag()*myMom1->Mag());
    double Eex2 = TMath::Sqrt(emass*emass + myMom2->Mag()*myMom2->Mag());
    TLorentzVector lex1(*myMom1, Eex1);
    TLorentzVector lex2(*myMom2, Eex2);
    double imassex = (lex1+lex2).M()*1000;// MeV
    const double ComputedAngle = lex1.Angle(lex2.Vect());

    //SAVE INFORMATION IN STRUCT
    NA64::Vertex vert;
    
    //save reconstructed information
    vert.pos = 10 * *myPOCA; // conversion back in mm
    vert.imass = imassex;
    vert.angle = ComputedAngle;
    vert.distance = 10 * vertdist; //conversion back in mm
    //save tracks
    vert.track1 = tracksSel[i];
    vert.track2 = tracksSel[j];
    vert.hittrack1 = tracksSelHits[i];
    vert.hittrack2 = tracksSelHits[j];
    
    //garbage collection
    delete myMom1;
    delete myMom2;
    delete myPOCA;

    //save vertex into the vector
    result.push_back(vert);
   }
  }
 
  return result;
} // end function do vertexing


//MASTER FUNCTION

 vector<NA64::Vertex> DoVertexing(const RecoEvent& e,std::vector<genfit::Track*>& tracks,const double& chicut = 1.5)
{
 //first step, decide if event is from simulation or data by lokking at the pointer
 vector<NA64::Vertex> result; 
 if(e.mc)
  {
   //is simulation
   //TODO: Use different gdml as function of geometry in sim
   GenfitInit("NA64_vis150_2018_sim.gdml");
   result = DoVertexingSim(e,tracks,chicut);
  }
 else
  {
   //is data, only 2018 supported for now
   if(4224 <= e.run && e.run <= 4310)
    {
     GenfitInit(conddbpath() + "NA64_vis150_2018.gdml");
     result = DoVertexingData(e,tracks,chicut);
    }
   else
    {
     cout << "RUN: " << e.run << " is not supported yet for vertex reconstruction" << endl;
    }
  }
 return result;
}

//geometry file as input
vector<NA64::Vertex> DoVertexing(const RecoEvent& e,std::vector<genfit::Track*>& tracks,const string geofile,const double& chicut = 1.5)
{
 //first step, decide if event is from simulation or data by lokking at the pointer
 vector<NA64::Vertex> result;
 GenfitInit(geofile);
 if(e.mc)
  {
   //is simulation
   result = DoVertexingSim(e,tracks,chicut);
  }
 else
  {
   //is data, only 2018 supported for now
   result = DoVertexingData(e,tracks,chicut);
  }
 return result;
}

