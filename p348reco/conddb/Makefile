all: ready.txt

ready.txt: Makefile shower-2016B-v4.root shower-2016B-MC-v3.root shower-2022B-v1 shower55-2022B.root shower-2022B-MC-v1.root shower-2022B-MC-v2.root shower-2023A-v1 shower55-MC-v1.root pedestal-v13 led-2023A-2023dec11 led-2022B-2023dec5 ledspill-2018-v3.txt badburst-2018-invis-3898_4201-v2.txt badburst-2022B.txt badburst-2023A.txt badburst-2023B.txt NA64Geom2016B.root NA64Geom2017.root NA64Geom2018.root NA64Geom2018_vis.root NA64Geom2021B.root NA64Geom2022A.root BeamOnly-2023A-2023nov24
	touch ready.txt

clean:
	rm -f ready.txt

# NOTE: the `--no-check-certificate` is a work-around of twiki issue 2023aug16, to be removed in the future
WOPT = -nc -nv --no-check-certificate

# === shower profile database
shower-2016B-v4.root:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@

shower-2016B-MC-v3.root:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@

shower-2022B-v1:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@.tar.gz
	tar zxf $@.tar.gz

shower-2023A-v1:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@.tar.gz
	tar zxf $@.tar.gz

shower55-2022B.root:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@

shower-2022B-MC-v1.root:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@

shower-2022B-MC-v2.root:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@

shower55-MC-v1.root:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/EcalShowerShape/$@

# === tracker pedestal database
# NOTE: the GM03Y was not operational during 2017 session and didn't produce any data, and so calibration data is not available,
#       dummy symlink created to avoid crash due to missing file on the pedestals loading
pedestal-v13:
	ln -sf -T $@ pedestal
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/ApvCalibration/$@.tgz
	tar -zxf $@.tgz
	cd $@/2017 && ln -sf GM03X1__~~start-2017-09-10-16:15:28~~finish-2017-12-31-23:59:59 GM03Y1__~~start-2017-08-01-00:00:00~~finish-2017-12-31-23:59:59
	ls -ld $@.tgz $@


# === BEAM-only corrections data
BeamOnly-2023A-2023nov24:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/BeamOnlyCorrections/$@.tar.gz
	tar jxf $@.tar.gz

# === LED corrections data
# run 8655 - led amplitudes are zero values (LED system issues?), remove the file
led-2023A-2023dec11:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/LedCorrections/$@.tar.bz2
	tar jxf $@.tar.bz2
	rm $@/run_8655

led-2022B-2023dec5:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/LedCorrections/$@.tar.bz2
	tar jxf $@.tar.bz2

ledspill-2018-v3.txt:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/LedCorrections/$@.gz
	gunzip -N -f $@.gz

# === bad spills data
badburst-2018-invis-3898_4201-v2.txt:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/BadBurst/$@

badburst-2022B.txt:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/BadBurst/$@

badburst-2023A.txt:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/BadBurst/$@

badburst-2023B.txt:
	wget $(WOPT) https://twiki.cern.ch/twiki/pub/P348/BadBurst/$@

# === geometry
NA64Geom2016B.root: makeGeomNA642016B.C
	root -b -q -l makeGeomNA642016B.C

NA64Geom2017.root: makeGeomNA642017.C
	root -b -q -l makeGeomNA642017.C

NA64Geom2018.root: makeGeomNA642018.C
	root -b -q -l makeGeomNA642018.C

NA64Geom2018_vis.root: makeGeomNA642018_vis.C
	root -b -q -l makeGeomNA642018_vis.C

NA64Geom2021B.root: makeGeomNA642021B.C
	root -b -q -l makeGeomNA642021B.C

NA64Geom2022A.root: makeGeomNA642022A.C
	root -b -q -l makeGeomNA642022A.C
