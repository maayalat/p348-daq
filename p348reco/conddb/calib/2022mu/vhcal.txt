# VHCAL calibrations for 2022-mu run

# Energy calibration data by Vladimir Samoylenko,
# released 2022 May 31
#   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2022A/vhcal_raw.pdf
#   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2022A/vhcal_clb.pdf
#   https://twiki.cern.ch/twiki/pub/P348/RunsInfo2022A/vhcal_clb.txt
#
# Based on runs ???.
# Fit is Landau Function + Exp.Background in the region of MIP peak.
# The peak position is MPV parameter of the fit.

runs=5575-6034

# The 1.5 GeV is peak position of MIP energy deposition distribution for 160 GeV mu
# (Calculated by Mikhail Kirsanov)
factor.VHCAL0=1.5

# NOTE: only subset of VHCAL cells was calibrated due to the lack of illumination
columns=name,x,y,sigma,sigma_err,peakpos,peakpos_err,norm,norm_err,chi2ndf
#name   x  y       Sigma     Er(Sigma)      MPV      Er(MPV)       Norm.     Er(Norm)     Chi2/NDF
VHCAL0  0  1        13.3      0.4597       94.08      0.6875        6040       241.9       1.499
VHCAL0  0  2       12.57      0.2539       91.24        0.34   1.258e+04       274.5       1.595
VHCAL0  0  3       10.24       0.869       82.07       1.159       614.1       35.18       1.201
VHCAL0  1  1       2.246     0.01682       18.48     0.03889    4.95e+04       739.4        3.58
VHCAL0  1  2        2.39    0.007025       20.68     0.01977   4.062e+05        3168       16.74
VHCAL0  1  3       5.414      0.2269       38.97      0.5569        1262       51.19       1.022
VHCAL0  2  1       1.292     0.01149       10.59     0.02277   2.723e+04         249       8.255
VHCAL0  2  3       1.911      0.1167       13.35      0.2965       642.9       84.48       0.986
VHCAL0  3  2       3.285     0.09984       26.97      0.1701        3657       166.9      0.8366

# NOTE: the rest of VHCAL cells (uncalibrated) are set to "zero" (peakpos=1e10 gives .energy= ~0)
columns=name,x,y,peakpos
#name   x  y   peakpos
VHCAL0  0  0    1e10
VHCAL0  1  0    1e10
VHCAL0  2  0    1e10
VHCAL0  3  0    1e10
VHCAL0  3  1    1e10
VHCAL0  3  3    1e10
VHCAL0  2  2    1e10


# Timing calibrations by Anton Karneyeu
# released 2022 May 30
# preliminary, made by visual inspection of `timing.cc` histograms for run 6030

# name   X    Y    tmean   tsigma
columns=name,x,y,time,timesigma
VHCAL0   0    0    80.     10.
VHCAL0   0    1    80.     10.
VHCAL0   0    2    80.     10.
VHCAL0   0    3    80.     10.
VHCAL0   1    0    80.     10.
VHCAL0   1    1    80.     10.
VHCAL0   1    2    80.     10.
VHCAL0   1    3    80.     10.
VHCAL0   2    0    80.     10.
VHCAL0   2    1    80.     10.
VHCAL0   2    2    80.     10.
VHCAL0   2    3    80.     10.
VHCAL0   3    0    80.     10.
VHCAL0   3    1    80.     10.
VHCAL0   3    2    80.     10.
VHCAL0   3    3    80.     10.

