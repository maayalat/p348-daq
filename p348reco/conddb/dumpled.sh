#!/bin/bash

# 2022B runs 6035 - 8458
# $ ./dumpled.sh led-2022B-2023dec5/run_{6035..8458} > led6035-8458.txt

# 2023A runs 8459 - 9717
# $ ./dumpled.sh led-2023A-2023dec11/run_{8459..9717} > led8459-9717.txt

# Note: by default, the script dumps the off-spill led data,
# to output on-spill data - comment the line 66, and uncomment the line 72

print_header () {
  local session="$1"
  echo "INFO: print_header() session=$session" >&2
  
  echo "run spill time stat_off"

  for d in $(seq 0 1) ; do
    for x in $(seq 0 4) ; do
      for y in $(seq 0 5) ; do
        echo "ECAL$d-$x-$y"
      done
    done
  done

  for d in $(seq 0 3) ; do
    for x in $(seq 0 2) ; do
      for y in $(seq 0 2) ; do
        echo "HCAL$d-$x-$y"
      done
    done
  done

  if [[ "$session" == "2023A" ]] ; then
  for d in $(seq 0 0) ; do
    for x in $(seq 0 3) ; do
      for y in $(seq 0 3) ; do
        echo "VHCAL$d-$x-$y"
      done
    done
  done
  fi

  for d in $(seq 0 2) ; do
    echo "SRD$d"
  done

  for d in $(seq 0 5) ; do
    echo "VETO$d"
  done
}

parse_led_file () {
  read x
  while true ; do
    read x stat_off x stat_on x run x spill x time x || break
    
    leddata=""
    if (( stat_off != 0 )) ; then
      # skip two lines with pedestals
      read x
      read x
      
      # LED off-spill data
      read leddata
      echo $run $spill $time $stat_off $leddata
    fi
    
    if (( stat_on != 0 )) ; then
      # LED on-spill data
      read leddata
      #echo $run $spill $time $stat_on $leddata
    fi
  done
}

if (( $# < 1 )) ; then
  echo "Usage: ./dumpled.sh file1 file2 ..." >&2
  exit 1
fi

nfiles=$#
nmissing=0

# extract datataking session name from directory name of the first file path
# dirname assumption example: led-2023A-2023dec11
session=$(basename $(dirname $1))
session=$(expr $session : 'led-\([^-]*\)-')

print_header "$session" | xargs
for f in `find $*` ; do
  cat $f | parse_led_file
  echo -n "." >&2
done
