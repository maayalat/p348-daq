// Usage example:
// * just show all lines:
//   $ root 'trendview.cxx("led8459-9717.txt")'
//
// * show only lines with "CAL" name match (like ECAL,...) and normalize amplitudes by values of run 8700:
//   $ root 'trendview.cxx("led8459-9717.txt", "ch=CAL normby=8700")'

int mg_search_x0_index(TMultiGraph* mg, const double x0)
{
  TIter next(mg->GetListOfGraphs());
  
  while (TGraph* g = (TGraph*)next()) {
    const int n = g->GetN();
    const double* x = g->GetX();
    for (int j = 0; j < n; ++j) {
      if (x[j] == x0) return j;
    }
  }
  
  return -1;
}

void mg_norm_by_y0(TMultiGraph* mg, const double x0, TString pattern)
{
  const int x0idx = mg_search_x0_index(mg, x0);
  
  TIter next(mg->GetListOfGraphs());
  
  while (TGraph* g = (TGraph*)next()) {
    const TString name = g->GetTitle();
    if (!name.BeginsWith(pattern)) continue;
    
    const int n = g->GetN();
    double* y = g->GetY();
    const double y0 = y[x0idx];
    
    for (int j = 0; j < n; ++j) {
      y[j] /= y0;
    }
  }
}

void mg_scale(TMultiGraph* mg, const vector<double>& factors)
{
  const bool isMatch = (mg->GetListOfGraphs()->GetEntries() == factors.size());
  if (!isMatch) {
    cout << "WARNING: mg_scale() : size mismatch between 'mg' and 'factors'" << endl;
    return;
  }
  
  int idx = 0;
  
  for (TObject* i : *mg->GetListOfGraphs()) {
    TGraph* g = (TGraph*) i;
    const TString name = g->GetTitle();
    const int n = g->GetN();
    double* y = g->GetY();
    const double f = factors[idx];
    
    for (int j = 0; j < n; ++j) {
      y[j] *= f;
    }
    
    idx++;
  }
}

void mg_highline_item(TMultiGraph* mg, TString pattern)
{
  TIter next(mg->GetListOfGraphs());
  
  while (TGraph* g = (TGraph*)next()) {
    const TString name = g->GetTitle();
    if (!name.BeginsWith(pattern)) continue;
    
    g->SetLineWidth(3);
  }
}

void mg_keep_only_match(TMultiGraph* mg, const vector<TRegexp>& keep)
{
  TIter next(mg->GetListOfGraphs());
  vector<TGraph*> del;
  
  // collect list of graphs for remove
  while (TGraph* g = (TGraph*)next()) {
    const TString name = g->GetTitle();
    
    bool match = false;
    for (const auto& j : keep) {
      if (name.Index(j) != -1) {
        match = true;
        break;
      }
    }
    
    if (!match) del.push_back(g);
  }
  
  // run remove
  for (TGraph* g : del) {
    mg->RecursiveRemove(g);
    delete g;
  }
}

// https://root.cern.ch/doc/master/classTColor.html
int NextColor()
{
  static int index = 0;
  const int maxPalette = 255;
  index = (index+1) % maxPalette;
  return gStyle->GetColorPalette(index);
}


struct SpillInfo {
  int run;
  int spill;
  int time;
  int stat;
};

vector <SpillInfo> vs;
vector <TObject*> labels;

void redraw()
{
  if (!gPad) return;
  
  // extract current viewport ranges
  TFrame* frame = gPad->GetFrame();
  const double xmin = frame->GetX1();
  const double xmax = frame->GetX2();
  const double ymin = frame->GetY1();
  const double ymax = frame->GetY2();
  const double ystep = 0.01*(ymax-ymin);
  
  const uint64_t pad_width  = gPad->XtoPixel(gPad->GetX2());
  const uint64_t pad_height = gPad->YtoPixel(gPad->GetY1());
  
  // the "hash-code" of current viewport state
  const uint64_t state = (uint64_t)xmin ^ (uint64_t)xmax ^
                         (uint64_t)ymin ^ (uint64_t)ymax ^
                         pad_width ^ pad_height;
  // skip markers re-plot if viewport is not changed
  static uint64_t lastState = 0;
  if (state == lastState) return;
  lastState = state;
  
  // remove all old labels
  for (auto i : labels) i->Delete();
  labels.clear();
  
  /*
  cout << "redraw()"
       << " xmin=" << xmin
       << " xmax=" << xmax
       << " ymin=" << ymin
       << " ymax=" << ymax
       << endl;
  */
  
  int run0 = 0;
  double pos0 = 0;
  for (const auto& i : vs) {
    // skip spill of the same run
    if (i.run == run0) continue;
    
    // skip if out of the current viewport range
    const bool inRange = (xmin <= i.time) && (i.time <= xmax);
    if (!inRange) continue;
    
    run0 = i.run;
    
    // check if the label is too close to the previous one
    // or the run type is "dry run"
    const bool isDryRun = (i.run > 900000);
    const bool isHideLabel = (i.time < pos0) || isDryRun;
    
    const double alen = isHideLabel ? 3.0*ystep : 6.5*ystep;
    TArrow* a = new TArrow(i.time, ymax+alen, i.time, ymax, 0.01, ">");
    //if (isDryRun) a->SetLineStyle(2);
    a->SetLineColor(isDryRun ? kGray : kBlue);
    a->Draw();
    labels.push_back(a);
    
    // the label is too close to the previous one, skip the display
    if (isHideLabel) continue;
    
    TString label;
    if (i.spill == 1) label.Form("%d", run0);
    else  label.Form("%d-%d", run0, i.spill);
    
    TText* t = new TText(i.time+1, ymax+6.0*ystep, label);
    t->SetTextColor(kGray);
    t->SetTextAlign(13);
    t->SetTextSize(0.03);
    t->Draw();
    labels.push_back(t);
    
    // calc min. boundary for the next label
    UInt_t w, h;
    t->GetBoundingBox(w, h); // label width in pixels
    const double wu = (gPad->PixeltoX(w) - gPad->PixeltoX(0)); // convert pixels to "user" coordinate system
    pos0 = i.time + 1.5*wu;
    
    /*
    TText* t2 = new TText(i.time+1, ymax+3.0*ystep, TString::Format("%d spills", i.spill));
    t2->SetTextColor(kGray);
    t2->SetTextAlign(13);
    t2->SetTextSize(0.022);
    t2->Draw();
    labels.push_back(t2);
    */
  }
  
  // display channels labels
  //map<double,TString> legend;
  TMultiGraph* mg = (TMultiGraph*) gPad->FindObject("trendview");
  {
    // 1 pixel in "user" coordinate system units
    const double w1px = (gPad->PixeltoX(1) - gPad->PixeltoX(0));

    TIter next(mg->GetListOfGraphs());
    
    while (TGraph* g = (TGraph*)next()) {
      const TString name = g->GetTitle();
      const int n = g->GetN();
      const double* x = g->GetX();
      const double* y = g->GetY();
      
      // skip empty graphs (all point removed due to time interval limit)
      if (n == 0) continue;
      
      int lastidx = n-1;
      for (int j = 0; j < n; ++j) {
        if (x[j] > xmax) {lastidx=j-1; break;}
      }
      
      const double x_idx = x[lastidx];
      const double y_idx = y[lastidx];
      
      //cout << "name=" << name << " lastidx=" << lastidx << " y_idx=" << y_idx << endl;
      //cout << "inRange=" << inRange << endl;
      
      const bool inRange = (ymin <= y_idx) && (y_idx <= ymax);
      if (!inRange) continue;
      
      //legend[y_idx] = name;
      
      TArrow* a = new TArrow(xmax+3*w1px, y_idx, x_idx + 3*w1px, y_idx, 0.01, ">");
      a->SetLineColor(kGray);
      a->Draw();
      labels.push_back(a);
      
      TText* t = new TText(xmax+3*w1px, y_idx, name);
      //t->SetTextColor(kBlack);
      t->SetTextColor(g->GetLineColor());
      t->SetTextAlign(12);
      t->SetTextSize(0.020);
      t->Draw();
      labels.push_back(t);
    }
  }
  
  
  gPad->Modified();
  gPad->Update();
}

void CanvasEvent(Int_t event, Int_t, Int_t, TObject*)
{
  cout << "CanvasEvent()"
       << " event=" << event
       << endl;
  
  // event is EEventType:
  // https://root.cern.ch/doc/master/Buttons_8h_source.html#l00015
  
  if (event == kButton1Up) redraw();
}

vector<string> to_words(const string& s)
{
  vector<string> list;
  istringstream iss(s);
  string w;
  while (iss >> w) list.push_back(w);
  return list;
}

vector<TGraph*> read_dump_file(const TString fname)
{
  cout << "INFO: read_dump_file() fname=" << fname << endl;
  
  const int minrun = 0;
  const int maxrun = 999999;
  
  ifstream f(fname);
  
  if (!f) terminate();
  
  // read the header
  string line;
  getline(f, line);
  vector<string> cname = to_words(line);
  cname.erase(cname.begin(), cname.begin() + 4); // erase leading 4 columns
  
  //cout << "ncolumns=" << cname.size() << endl;
  
  vector<TGraph*> vg;
  
  for (auto& i : cname) {
    TGraph* g = new TGraph;
    g->SetTitle(i.c_str());
    vg.push_back(g);
  }
  
  vector<int> remap(cname.size(), 0);
  for (int i = 0; i < remap.size(); ++i)
    remap[i] = i;
  
  
  vs.clear();
  
  while (getline(f, line)) {
    // skip header lines
    const bool isHeader = (line[0] == '#');
    if (isHeader) {
      vector<string>  newcol = to_words(line);
      newcol.erase(newcol.begin(), newcol.begin() + 4);
      //cout << "new header size=" << newcol.size() << endl;
      
      remap.resize(newcol.size());
      
      for (int j = 0; j < newcol.size(); ++j) {
        bool isNew = true;
        for (int k = 0; k < cname.size(); ++k)
          if (newcol[j] == cname[k]) {
            isNew = false;
            
            remap[j] = k;
            /*
            cout << newcol[j] << " idx=" << j
                 << " => " << cname[k] << " idx=" << k
                 << endl;
            */
            break;
          }
      
        if (isNew) {
            cname.push_back(newcol[j]);
            TGraph* g = new TGraph;
            g->SetTitle(newcol[j].c_str());
            vg.push_back(g);
            remap[j] = cname.size()-1;
          
          
            //cout << newcol[j] << " idx=" << j
            //     << endl;
        }
      }
      
      continue;
    }
    
    istringstream iss(line);
    int run,spill,time,stat_off;
    iss >> run >> spill >> time >> stat_off;
    
    if (!iss) break; //  => rise ERROR
    
    if ((run < minrun) || (run > maxrun)) {
      continue;
    }
    
    //cout << run << " " << spill << " " << time << " " << stat_off << endl;
    SpillInfo si = {run, spill, time, stat_off};
    vs.push_back(si);
    
    // print dot each 10k spills for progress indication
    const bool showProgress = (vs.size() % 10000 == 0);
    if (showProgress) {
      //cout << "run=" << run << endl;
      cout << "." << flush;
    }
    
    for (int j = 0; j < remap.size(); ++j) {
      double y;
      iss >> y;
      //cout << y << " ";
      
      // skip corrupted? ADC=0 channels in LED on-spill events
      //if (y < 1) continue;
      
      // magic number, means "no data"
      if (y > 1e99) continue;
      
      TGraph* i = vg[remap[j]];
      i->AddPoint(time, y);
      
      /*
      if ((y < 1) || (y > 1e6))
        cout << run << " " << spill << " " << time << " " << stat_off << " "
             << i->GetTitle() << " " << " y=" << y
             << endl;
      */
    }
    //cout << endl;
    
    // if (!iss) break; => rise ERROR
  }
  
  cout << endl;
  
  cout << "INFO: read_dump_file()"
       << " total channels n=" << vg.size()
       << " total spills n=" << vs.size()
       << endl;
  return vg;
}

void vg_keep_only_match(vector<TGraph*>& vg, const vector<TRegexp>& keep, const vector<TRegexp>& reject)
{
  vector<TGraph*> del;
  
  // collect list of graphs for remove
  for (TGraph* g : vg) {
    const TString name = g->GetTitle();
    
    bool match = false;
    for (const auto& j : keep) {
      if (name.Index(j) != -1) {
        match = true;
        break;
      }
    }
    
    for (const auto& j : reject) {
      if (name.Index(j) != -1) {
        match = false;
        break;
      }
    }
    
    if (!match) del.push_back(g);
  }
  
  // run remove
  for (TGraph* g : del) {
    auto pos = find(vg.begin(), vg.end(), g);
    if (pos != vg.end()) {
      vg.erase(pos);
      delete g;
    }
  }
}

void vg_scale(vector<TGraph*>& vg, const vector<double>& factors)
{
  const bool isMatch = (vg.size() == factors.size());
  if (!isMatch) {
    cout << "WARNING: vg_scale() : size mismatch between 'vg' and 'factors'" << endl;
    return;
  }
  
  for (size_t i = 0; i < vg.size(); ++i) {
    TGraph* g = vg[i];
    //const TString name = g->GetTitle();
    const int n = g->GetN();
    double* y = g->GetY();
    const double f = factors[i];
    
    for (int j = 0; j < n; ++j) {
      y[j] *= f;
    }
  }
}

vector<double> vg_calc_runmean_scale(const vector<TGraph*>& vg, const int runnum)
{
  vector<double> scales(vg.size(), 1.);
  
  // find time range for 'runnum'
  int mintime = 2000000000;
  int maxtime = 0;
  for (const auto& i : vs) {
    if (i.run == runnum) {
      if (mintime > i.time) mintime = i.time;
      if (maxtime < i.time) maxtime = i.time;
    }
  }
  
  cout << "INFO: vg_calc_runmean(), runnum=" << runnum
       << " mintime=" << mintime
       << " maxtime=" << maxtime
       << " duration=" << (maxtime-mintime)
       << endl;
  
  // extra marging to avoid floating-point numerical imprecission issue
  mintime -= 1;
  maxtime += 1;
  
  for (size_t i = 0; i < vg.size(); ++i) {
    TGraph* g = vg[i];
    //const TString name = g->GetTitle();
    const int n = g->GetN();
    double* x = g->GetX();
    double* y = g->GetY();
    
    double S = 0.;
    int nspills = 0;
    
    for (int j = 0; j < n; ++j) {
      const bool inRange = (mintime <= x[j]) && (x[j] <= maxtime);
      if (!inRange) continue;
      
      S += y[j];
      nspills++;
    }
    
    // skip, if no data corresponding to 'runnum'
    if (nspills == 0) continue;
    
    const double m = S / nspills;
    const double f = 1. / m;
    scales[i] = f;
  }
  
  return scales;
}


void SetCustomRainBow(const int ncolors)
{
  // the reference point data is extracted from ROOT TColor.cxx / "Rain Bow" palette
  Double_t stops[9] = { 0.0000, 0.1250, 0.2500, 0.3750, 0.5000, 0.6250, 0.7500, 0.8750, 1.0000};
  Double_t red[9]   = {  0./255.,   5./255.,  15./255.,  35./255., 102./255., 196./255., 208./255., 199./255., 110./255.};
  Double_t green[9] = {  0./255.,  48./255., 124./255., 192./255., 206./255., 226./255.,  97./255.,  16./255.,   0./255.};
  Double_t blue[9]  = { 99./255., 142./255., 198./255., 201./255.,  90./255.,  22./255.,  13./255.,   8./255.,   2./255.};
  TColor::CreateGradientColorTable(9, stops, red, green, blue, ncolors);
}

TMultiGraph* mg_prepare(vector<TGraph*> vg, TString title)
{
  SetCustomRainBow(vg.size());
  //gStyle->SetPalette(kRainBow);
  //gStyle->SetPalette(kVisibleSpectrum);
  
  for (auto g : vg) {
    g->SetFillColor(0);
    g->SetMarkerStyle(kFullDotMedium);
    
    /*
    TString dname = g->GetName();
    dname.subsrt(0, 5); // keep first 5 chars
    Color_t c = kBlack;
    if (dname == "ECAL0") c = kOrange;
    else if (dname == "ECAL1") c = kRed;
    else if (dname == "HCAL0") c = kPink;
    else if (dname == "HCAL1") c = kViolet;
    else if (dname == "HCAL2") c = kAzure;
    else if (dname == "HCAL3") c = kTeal;
    g->SetLineColor(c);
    g->SetMarkerColor(c);
    */
    
    const Color_t c = NextColor();
    g->SetLineColor(c);
    g->SetMarkerColor(c);
  }
  
  TMultiGraph* mg = new TMultiGraph;
  mg->SetName("trendview");
  mg->SetTitle(title + ";local time;value");
  for (auto i : vg) {
      mg->Add(i);
  }
  
  mg->GetXaxis()->SetTimeDisplay(1);
  mg->GetXaxis()->SetNdivisions(505);
  mg->GetXaxis()->SetTickSize(0.015);
  mg->GetXaxis()->SetLabelOffset(0.016);
  mg->GetXaxis()->SetTimeFormat("#splitline{%Y-%m-%d}{%H:%M}");
  mg->GetXaxis()->SetTitleOffset(1.5);
  //mg->GetXaxis()->SetTimeOffset(0,"gmt");
  mg->GetXaxis()->SetTimeOffset(0,"local");
  mg->GetYaxis()->SetTickSize(0.015);
  
  return mg;
}

void trendview(const TString fname = "dump.txt", const TString options = "")
{
  // read database
  vector<TGraph*> vg = read_dump_file(fname);
  int ndays = 0;
  int normby = 0;
  
  // process options
  // in fact currently it could be only one option for channels subset selection
  for (auto i : to_words(options.Data())) {
    if (i == "all") {}
    else if (i == "eff") vg_keep_only_match(vg, {"Eff"}, {"STT0"});
    else if (i == "mm12") vg_keep_only_match(vg, {"MM1_X", "MM1_Y", "MM2_X", "MM2_Y"}, {});
    else if (i == "led-ecal0") vg_keep_only_match(vg, {"LED:ECAL0"}, {});
    else if (i == "led-ecal1") vg_keep_only_match(vg, {"LED:ECAL1"}, {});
    else if (i == "led-hcal") vg_keep_only_match(vg, {"LED:HCAL"}, {});
    else if (i == "led-hcal0") vg_keep_only_match(vg, {"HCAL0"}, {"VHCAL0"});
    else if (i == "led-hcal1") vg_keep_only_match(vg, {"HCAL1"}, {"VHCAL1"});
    else if (i == "led-hcal2") vg_keep_only_match(vg, {"HCAL2"}, {"VHCAL2"});
    else if (i == "led-hcal3") vg_keep_only_match(vg, {"HCAL3"}, {"VHCAL3"});
    else if (i == "led-vhcal") vg_keep_only_match(vg, {"LED:VHCAL"}, {});
    else if (i == "led") vg_keep_only_match(vg, {"LED:ECAL0", "LED:ECAL1", "LED:HCAL", "LED:VHCAL"}, {});
    // TODO: GM03,GM04 - not present at all in setup, the coool groupGEM.cc to be fixed to not include such histograms
    else if (i == "xy") vg_keep_only_match(vg, {"_X$", "_Y$", "ECAL.?_hit.?"}, {"STT0", "GM03", "GM04"});
    else if (i == "scalers") vg_keep_only_match(vg, {"SCALERS"}, {});
    else if (i == "calo") vg_keep_only_match(vg, {".*"}, {"LED:", "SCALERS", "events_per_spill", "^MM", "^GM", "^ST", "ECALSUM", "_hitX", "_hitY"});
    else if (i.substr(0, 3) == "ch=") {
      TString list = i.substr(3);
      list.ReplaceAll(",", " ");
      vector<TRegexp> match;
      for (auto j : to_words(list.Data()))
        match.push_back(j.c_str());
      vg_keep_only_match(vg, match, {});
    }
    else if (i.substr(0, 6) == "ndays=") {
      ndays = TString(i.substr(6)).Atoi();
    }
    else if (i.substr(0, 7) == "normby=") {
      normby = TString(i.substr(7)).Atoi();
    }
    else {
      cout << "ERROR: unknown option: " << i << endl;
      gApplication->Terminate(1);
      return;
    }
  }
  
  if (normby > 0) {
    cout << "INFO: normalising all channels by the corresponding mean value in the run=" << normby << endl;
    vg_scale(vg, vg_calc_runmean_scale(vg, normby));
  }
  
  if (ndays > 0) {
    cout << "INFO: limiting display for last ndays=" << ndays << " days" << endl;
    
    const time_t t0 = time(0) - ndays*24*60*60;
    
    int inew = 0;
    for (auto& i : vs) {
      if (i.time < t0) continue;
      
      vs[inew] = i;
      inew++;
    }
    vs.resize(inew);
    
    
    for (TGraph* g : vg) {
      double* x = g->GetX();
      double* y = g->GetY();
      int n = g->GetN();
      
      inew = 0;
      
      for (int j = 0; j < n; ++j) {
        if (x[j] < t0) continue;
        
        x[inew] = x[j];
        y[inew] = y[j];
        inew++;
      }
      
      g->Set(inew);
    }
    
    cout << "INFO: remaining spills n=" << vs.size() << endl;
  }
  
  
  // print selected channels list
  cout << "Channels list:" << endl;
  for (auto i : vg)
    cout << i->GetTitle() << " ";
  cout << endl;
  cout << "Total channels n=" << vg.size() << endl;
  
  if (vg.empty()) {
      cout << "WARNING: no channels pass the selection option" << endl;
      //gApplication->Terminate(1);
      //return;
  }
  
  // show the picture
  const TString title = options;
  TMultiGraph* mg = mg_prepare(vg, title);
  
  /*
  // scale to reference values
  mg->SetTitle("LED signals;local time;amplitude/reference");
  mg_norm_by_y0(mg, 1660157409, "HCAL"); // 1660157409 is spill 6472-118 of HCAL calibrations
  mg_norm_by_y0(mg, 1660253984, "ECAL"); // 1660253984 is spill 6541-1   of ECAL calibrations
  */
  
  /*
  mg_highline_item(mg, "HCAL2-1-0");
  mg_highline_item(mg, "ECAL1-1-3");
  */
  
  TCanvas* c = new TCanvas(title, title);
  c->SetGrid();
  // disable opaque effects to speedup redraw
  c->MoveOpaque(0);
  c->ResizeOpaque(0);
  mg->Draw("APL");
  //mg->Draw("APL pmc plc");
  //c->BuildLegend();
  
  //c->Modified();
  c->Update(); // trigger graph frame creation
  
  const bool isBatch = gROOT->IsBatch();
  if (isBatch) {
    cout << "INFO: batch mode detected" << endl;
    c->SetCanvasSize(1600, 1200);
    redraw();
    c->Print("trendview.png");
    delete c;
    gApplication->Terminate();
    return;
  }
  
  // exit root on canvas window close
  TRootCanvas* rc = (TRootCanvas*)c->GetCanvasImp();
  rc->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
  // TODO: following commands do not work
  //rc->ShowStatusBar();
  //rc->ShowToolTips();
  
  // plot run start labels
  redraw();
  
  TTimer* timer = new TTimer();
  timer->Connect("Timeout()", 0, 0, "redraw()");
  timer->Start(2000);
  
  // two signal methods, but not always working as expected
  //c->Connect("ProcessedEvent(Int_t,Int_t,Int_t,TObject*)", 0, 0, "CanvasEvent(Int_t,Int_t,Int_t,TObject*)");
  //gPad->AddExec("redraw","redraw()");
  // https://root.cern/doc/master/exec1_8C.html
  // https://root.cern/doc/master/exec2_8C.html
  // https://root.cern/doc/master/exec3_8C.html
  // https://root.cern/doc/master/mandelbrot_8C.html
  // https://root.cern.ch/root/htmldoc/guides/users-guide/ROOTUsersGuide.html#graphical-containers-canvas-and-pad
}
