// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TString.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TF1.h"
#include "TTree.h"
#include "TLine.h"
#include <vector>
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TCanvas.h>


// c++
#include <iostream>
#include <string>
using namespace std;

// P348 reco
#include "p348reco.h"
#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif


/*
 * ST_response
 *
 * Implemented by B. Banto (ETHZ)
 *
 * This application serves for checking the output of the Straw reconstruction
 *
 */


struct ST_histo_plane
{
  TH1D* clus_time;
  TH2D* wire_time;
  TH2D* clus_wire_time;
  TH2D* clus_rt;
  TH2D* track_rt;
  TH2D* clus_pos_time;
  TH1D* mult;
  TH1D* clussize;


  ST_histo_plane()
    :
      clus_time(0),
      wire_time(0),
      clus_wire_time(0),
      clus_rt(0), track_rt(0),
      clus_pos_time(0),
      mult(0),
      clussize(0)
  {;}

  void Init(const char* _name, const ST_calib* calib, const GEMGeometry* geom)
  {
    std::cout << "MESSAGE: initializing plane: " << _name << "\n";
    const unsigned int Nchan     = geom->size;
    const double pitch           = 6.0;
    clus_time     = new TH1D(TString::Format("%s_clus_time", _name),
        TString::Format("cluster time in %s; # Time (ns)", _name),
        200, -100, 100);
    wire_time    = new TH2D(TString::Format("%s_wire_time",_name),
        TString::Format("raw wire vs time in %s; # Wire; Time (ns)", _name),
        Nchan,-0.5,Nchan-0.5,
        200,-100,100);
    clus_wire_time    = new TH2D(TString::Format("%s_clus_wire_time",_name),
        TString::Format("cluster wire vs time in %s; # Cluster Wire; Time (ns)", _name),
        Nchan,-0.5,Nchan-0.5,
        200,-100,100);
    clus_rt    = new TH2D(TString::Format("%s_clus_rt",_name),
        TString::Format("r(t) distribution for cluster wires in %s; # Distance (mm); Wire Time (ns)", _name),
        200,0.,pitch/2.,
        200,-100,100);
    track_rt    = new TH2D(TString::Format("%s_track_rt",_name),
        TString::Format("r(t) distribution for cluster wires using fitted track in %s; # Distance (mm); Wire Time (ns)", _name),
        200,0.,pitch/2.,
        200,-100,100);
    clus_pos_time    = new TH2D(TString::Format("%s_clus_pos_time",_name),
        TString::Format("Position in wire plane vs time of cluster in %s; C.O.M. Wire; Time (ns)", _name),
        Nchan,-0.5,Nchan-0.5,
        200,-100,100);
    mult          = new TH1D(TString::Format("%s_mult",_name),
        TString::Format("Number of clusters in %s ; NClusters", _name),
        30,-0.5,29.5);
    clussize      = new TH1D(TString::Format("%s_clussize",_name),
        TString::Format("Size of clusters in %s ; NWires", _name),
        6,-0.5,5.5);
  }

  void Fill(TString name, const std::vector<StWire>& Channels, const std::vector<StCluster>& Clusters, const ST_calib* calib, const GEMGeometry* geom, float phase, const double &fittedHit)
  {
    const int t0 = calib->t0;
    const unsigned int Nchan = geom->size;
    const double pitch = 6.0;
    // Raw channels information
    for(const auto& wire : Channels)
    {
      const double time = t0 + phase - wire.time;
      wire_time->Fill(wire.wire,time);
    }
    // Reconstructed clusters
    for(const auto& clus : Clusters)
    {
      for(const auto& wire : clus.wires)
      {
        clus_wire_time->Fill(wire.wire,wire.time);
        clus_rt->Fill(rtStraw6(wire.time), wire.time);
        // Obtain distance between fitted track hit and wire center
        const double wire_center = (wire.wire - Nchan/2.0 + 0.5)*0.5*pitch;
        const double distance = fittedHit - wire_center;
        if (abs(distance) < pitch/2.0 && !std::isnan(fittedHit)) track_rt->Fill(abs(distance), wire.time);
      }
      clus_time->Fill(clus.time());
      clus_pos_time->Fill(clus.wire(), clus.time());
      clussize->Fill(clus.size());
    }
    mult->Fill(Clusters.size());
  }

};

struct ST_histo
{
  //name
  const char* name;
  bool IsInitialized;
  TFile* out;
  //general ST profile
  TH2D* profile;
  TH2D* profile_global;
  TH2D* time_corr;
  TH2D* span_corr;

  ST_histo_plane xplane;
  ST_histo_plane yplane;

  ST_histo()
    :
      name("dummy"),
      IsInitialized(false),
      out(0),
      profile(0),
      profile_global(0),
      time_corr(0),
      span_corr(0)
  {}

  void SetOutFile(TFile* file)
  {
    if(file)
      out = file;
    else
    {
      std::cerr << "invalid file, not initialized \n";
    }
  }

  void Init(const ST_calib* calib, const GEMGeometry* geom)
  {
    const unsigned int Nchan     = geom->size;
    const double pitch            = geom->pitch;
    name = calib->name.c_str();
    if(out){out->mkdir(name);out->cd(name);}
    profile = new TH2D(TString::Format("%s_profile",name),
        TString::Format(" XY profile in %s; X [mm]; Y [mm]", name),
        Nchan, -Nchan * pitch / 2, Nchan * pitch / 2,
        Nchan, -Nchan * pitch / 2, Nchan * pitch / 2);
    profile_global = new TH2D(TString::Format("%s_profile_global",name),
        TString::Format(" XY profile in %s in global coordinate; X [mm]; Y [mm]", name),
        480, geom->pos.X()-120, geom->pos.X()+120,
        480, geom->pos.Y()-120, geom->pos.Y()+120);
    time_corr = new TH2D(TString::Format("%s_time_corr",name),
        TString::Format(" Time of clusters in X and Y planes for hits in  %s; X-plane time [ns]; Y-plane time [ns]", name),
        200, -100, 100,
        200, -100, 100);
    span_corr = new TH2D(TString::Format("%s_span_corr",name),
        TString::Format(" Time Span of clusters in X and Y planes for hits in %s; X-plane span [ns]; Y-plane [ns]", name),
        600, 0, 600,
        600, 0, 600);
    TString planenameX(name),planenameY(name),dirname(name);

    if (calib->isUV()) {
      planenameX+="U";
      planenameY+="V";
    } else {
      planenameX+="X";
      planenameY+="Y";
    }

    dirname+="/";
    if(out){
      out->mkdir(dirname+planenameX);out->cd(dirname+planenameX);
    }
    xplane.Init(planenameX.Data(), calib, geom);

    if(out){
      out->mkdir(dirname+planenameY);out->cd(dirname+planenameY);
    }
    yplane.Init(planenameY.Data(), calib, geom);
    //finalize
    if(out)out->cd();
    IsInitialized = true;
  }

  void Fill(const Straw* st, float phase, const TVector3 &fittedHit)
  {
    if(!IsInitialized)
    {
      std::cout << "initialize histogram collection with: " << st->calib->name << "\n";
      Init(st->calib, st->geom);
    }

    if(st->calib->isUV()) {
      xplane.Fill(TString(name)+"U", st->XChannels, st->XClusters, st->calib, st->geom, phase, fittedHit.X());
      yplane.Fill(TString(name)+"V", st->YChannels, st->YClusters, st->calib, st->geom, phase, fittedHit.Y());
    } else {
      xplane.Fill(TString(name)+"X", st->XChannels, st->XClusters, st->calib, st->geom, phase, fittedHit.X());
      yplane.Fill(TString(name)+"Y", st->YChannels, st->YClusters, st->calib, st->geom, phase, fittedHit.Y());
    }

    //fill
    for (const auto& hit : st->Hits) {
      profile->Fill(hit.pos().X(), hit.pos().Y());
      profile_global->Fill(hit.abspos().X(), hit.abspos().Y());
      time_corr->Fill(hit.x.time(), hit.y.time());
      //span_corr->Fill(hit.xcluster->cluster_span(), hit.ycluster->cluster_span());
    }
  }

  //write projection
  void WriteProj(const bool fit = false)
  {
    TString planenameX(name),planenameY(name),dirname(name);
    planenameX+="X";
    planenameY+="Y";
    dirname+="/";

    if(out)out->cd(dirname+planenameX);
    TH1D* px =  profile->ProjectionX();
    TH1D* pxg = profile_global->ProjectionX();
    if(fit){px->Fit("gaus","Q");pxg->Fit("gaus","Q");gStyle->SetOptFit(1);}

    if(out)out->cd(dirname+planenameY);
    TH1D* py =  profile->ProjectionY();
    TH1D* pyg = profile_global->ProjectionY();
    if(fit){py->Fit("gaus","Q");pyg->Fit("gaus","Q");gStyle->SetOptFit(1);}

    if(out)out->cd();
  }
};

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./ST_response.exe file1 [file2 ...]" << endl;
    return 1;
  }

  int event;
  const unsigned int NST = 6;
  const unsigned int NSTUV = 2;
  std::string file(argv[1]);
  std::size_t found_run = file.find("1001-0");
  std::size_t found_dotroot = file.find(".dat");
  std::string run_name = file.substr(found_run,found_dotroot-found_run);
  std::string path = file.substr(0,found_run);

  std::string new_file = "run"+run_name+"_ST_response.root";

  TFile* file_new = new TFile(new_file.c_str(),"RECREATE");


  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();

  std::vector<ST_histo*> histvec;
  std::vector<ST_histo*> histvecUV;

  for(unsigned int i(0); i < NST; ++i){
    histvec.push_back(new ST_histo);
    histvec[i]->SetOutFile(file_new);
  }
  for(unsigned int i(0); i < NSTUV; ++i){
    histvecUV.push_back(new ST_histo);
    histvecUV[i]->SetOutFile(file_new);
  }

  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");

  while (manager.ReadEvent()) {


    const int nevt= manager.GetEventsCounter();

    event=manager.GetEventsCounter();

    if(nevt%1000==1) cout << "Run "<<manager.GetEvent().GetRunNumber()<<"===> Event #" << manager.GetEventsCounter() << endl;
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();

    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }

    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);

    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;

    for(unsigned int i(1); i <= NST; ++i)
    {
      const Straw* st;
      switch(i)
      {
        case 1:
          st = &e.ST01;
          break;
        case 2:
          st = &e.ST02;
          break;
        case 3:
          st = &e.ST03;
          break;
        case 4:
          st = &e.ST04;
          break;
        case 5:
          st = &e.ST05;
          break;
        case 6:
          st = &e.ST06;
          break;
        default:
          std::cerr << "no such Straw implemented in the reco, defaulting to ST01 \n";
          st = &e.ST01;
          break;
      }

      TVector3 hit_pos(nan(""), nan(""), st->geom->pos.Z());
#if 0
    // track reconstruction with Tracking Tools
    const std::vector<Track_t> upstream = GetReconstructedTracks(e, true);
    if(!upstream.empty()) {
      Track_t track = upstream.at(0);
      try {
        hit_pos = upstream.at(0).extrapolateToZ(st->geom->pos);
        hit_pos = hit_pos - st->geom->pos;
      }
      catch (const runtime_error&) {
      }
    }
#endif
      //lowering index by 1, just to match index of ST
      histvec[i-1]->Fill(st, e.tdct0_phase, hit_pos);
    }
    for(unsigned int i(1); i <= NSTUV; ++i)
    {
      const Straw* st;
      switch(i)
      {
        case 1:
          st = &e.ST11;
          break;
        case 2:
          st = &e.ST12;
          break;
        default:
          std::cerr << "no such Straw implemented in the reco, defaulting to ST01 \n";
          st = &e.ST01;
          break;
      }

      TVector3 hit_pos(nan(""), nan(""), st->geom->pos.Z());
      //lowering index by 1, just to match index of ST
      histvecUV[i-1]->Fill(st, e.tdct0_phase, hit_pos);
    }

  }
for(auto p : histvec)p->WriteProj(1);
file_new->Write();
file_new->Close();

return 0;
}
