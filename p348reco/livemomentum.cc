// this is an example of program with a "live" display
// the example is based on live momentum reconstruction
// with a magnetic spectrometer in both the "upstream" and "downstream"
// part of the experiment

// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TApplication.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TText.h"
#include "TH1.h"
#include "TF1.h"
#include "TMath.h"

// c++
#include <iostream>
#include <ctime>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "tracking_new.h"

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./livemomentum.exe file1 [file2 ...]" << endl;
    return 1;
  }

  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();

  TApplication app("momentum", 0, 0);

  // stats bar format
  gStyle->SetOptStat("emruo");
  gStyle->SetOptFit(11);
  gStyle->SetLineScalePS(1);

  TCanvas c1("upmomentum", "upmomentum", 20, 20, 800, 600);
  c1.SetGrid();
  c1.SetLogy();
  //c.Connect("TCanvas", "Closed()", "TApplication", &app, "Terminate()");

  TCanvas c2("downmomentum", "downmomentum", 40, 40, 800, 600);
  c2.SetGrid();
  c2.SetLogy();

  /*
     THStack vetoplots("vetoplots", "VETO;Energy, MeV;#nevents");
     veto0plot.SetLineColor(kRed);
     veto0plot.SetMarkerColor(kRed);
     veto0plot.SetMarkerStyle(kFullSquare);
     vetoplots.Add(&veto0plot);
     vetoplots.Draw("nostack");
     c.BuildLegend(0.5, 0.65, 0.7, 0.85);
     */

  TH1I upmomentum("upmomentum", "Upstream momentum;momentum, GeV;events", 200, 0, 200);
  c1.cd();
  upmomentum.Draw();

  TH1I downmomentum("downmomentum", "Downstream momentum;momentum, GeV;events", 200, 0, 200);
  c2.cd();
  downmomentum.Draw();

  TText* t = new TText(0.01, 0.01, "");
  t->SetNDC();
  //t->SetTextAlign();
  t->SetTextFont(82);
  t->SetTextSize(0.04);
  c1.cd();
  t->Draw();
  c2.cd();
  t->Draw();

  time_t nextUpdate = 0;

  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();

    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;

    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();

    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }

    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);

    std::vector<Track_t> up = GetReconstructedTracks(e, true);
    std::vector<Track_t> down = GetReconstructedTracks(e, false);

    // sort the tracks
    double momup = -1;
    double momdown = -1;
    double chisqup, chisqdown;

    // best tracks selection 
    std::sort(up.begin(), up.end(), 
        [](const Track_t& a, const Track_t& b){return a.chisquare < b.chisquare;}
        );
    std::sort(down.begin(), down.end(), 
        [](const Track_t& a, const Track_t& b){return a.chisquare < b.chisquare;}
        );

    if (!up.empty()) momup = up.front().momentum;
    if (!down.empty()) momdown = down.front().momentum;

    // fill the histograms
    if (momup != -1 && momdown != -1) {
      upmomentum.Fill(momup);
      downmomentum.Fill(momdown);
    }

    const time_t now = time(0);
    if (now >= nextUpdate) {
      time_t evtt = manager.GetEvent().GetTime().first;
      char timestr[100];
      strftime(timestr, 100, "%d %b %Y %H:%M:%S %Z", localtime(&evtt));

      TString eid;
      eid.Form("Last event ID: run %d spill %d event %d, %s", e.run, e.spill, e.spillevent, timestr);
      t->SetTitle(eid);

      {
        const int maxbin = upmomentum.GetMaximumBin();
        const double maxpos = upmomentum.GetXaxis()->GetBinCenter(maxbin);
        upmomentum.Fit("gaus", "QW", "", maxpos-10.0, maxpos+10.0);
        TF1* ft = upmomentum.GetFunction("gaus");
        if (ft) ft->SetNpx(300);
        //pr->SetAxisRange(maxpos-2*50, maxpos+2*50, "X");
        //pr->Draw();
      }
      //upmomentum.Fit("gaus");
      //upmomentum.GetFunction("gaus")->SetNpx(300);

      {
        const int maxbin = downmomentum.GetMaximumBin();
        const double maxpos = downmomentum.GetXaxis()->GetBinCenter(maxbin);
        downmomentum.Fit("gaus", "QW", "", maxpos-10.0, maxpos+10.0);
        TF1* ft = downmomentum.GetFunction("gaus");
        if (ft) ft->SetNpx(300);
      }
      //downmomentum.Fit("gaus");
      //downmomentum.GetFunction("gaus")->SetNpx(300);

      c1.Modified();
      c1.Update();
      c2.Modified();
      c2.Update();
      gSystem->ProcessEvents();
      nextUpdate = now + 1; // next update in 1 second
    }
  }

  app.Run(kTRUE);

  return 0;
}
