# pragma once

// STD Library
#include <stdlib.h>
#include <sys/time.h>
#include <sys/resource.h>

// Tracking Tools
#include <TrackingAnalytical.hh>
#include <TrackingGenFit.hh>
#include <TrackingManager.hh>
#include <DetectorFactory.hh>
#include <MagnetMBPL.hh>
#include <ConfigFileParser.hh>
#include <FieldManager.h>
#include "TError.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Global variables
// static managers
static TrackingManager* trackingMgr1 = nullptr;
static TrackingManager* trackingMgr2 = nullptr;

// useful variables
const bool kVERBOSE = false;

// structure for tracking
struct Hit_t{
  TVector3 pos;
};

// Fitted track point
struct TrackPoint_t{
  TVector3 pos; // position of point in mm
  TVector3 mom; // momentum at point in GeV
  bool isSet;

  TrackPoint_t(): isSet(false) {}

  void set(const TVector3& pos_, const TVector3& mom_) {
    isSet = true;
    pos = pos_;
    mom = mom_;
  }

  TVector3 dir() const {
    return mom.Unit();
  }
  // Get angles with respect to beam line
  double theta() const { return dir().Theta(); }
  double theta_x() const { return TMath::ATan2(dir().x(), dir().z()); }
  double theta_y() const { return TMath::ATan2(dir().y(), dir().z()); }
  // Check that point was set
  operator bool() const { return isSet; }
};

// Fitted track
struct Track_t{
  genfit::Track gfTrack;

  double momentum;
  double chisquare;
  double pvalue;

  TrackPoint_t in;
  TrackPoint_t out;

  Track_t() : gfTrack(), momentum(-1.0), chisquare(-1.0), pvalue(-1.0)
  {}
  Track_t(Tracking::Track &fittedtrack) : gfTrack(*fittedtrack.gfTrack) {
    // Obtain
    momentum = gfTrack.getFittedState().getMomMag();
    chisquare = gfTrack.getFitStatus()->getChi2()/gfTrack.getFitStatus()->getNdf();
    pvalue = gfTrack.getFitStatus()->getPVal();

    // Fill in and out TrackPoints
    const unsigned int last = gfTrack.getNumPoints()-1;
    const genfit::MeasuredStateOnPlane& instate = gfTrack.getFittedState(0);
    in.set(10.*instate.getPos(), instate.getMom()); // factor 10 to convert from cm to mm
    const genfit::MeasuredStateOnPlane& outstate = gfTrack.getFittedState(last);
    out.set(10.*outstate.getPos(), outstate.getMom()); // factor 10 to convert from cm to mm
  }

  double in_out_angle() const { return in.dir().Angle(out.dir()); }

  // .....ooooo00000OOO0O00000ooooo.....ooooo00000OOOOO00000ooooo.....
  // Extrapolation using fitted track by GenFit
  // - Fitted track is obtained from the momentum reconstruction
  // - Returns the point closest to the fitted track as implemented in GenFit
  // .....ooooo00000OOO0O00000ooooo.....ooooo00000OOOOO00000ooooo.....
  // Please refer to https://gitlab.cern.ch/P348/p348-daq/-/merge_requests/353#note_6483235
  // The difference (z_extrap-z_in) was estimated to be less than 1mm for calibration run 7185
  // This corresponds to a difference of at most 1mm * 20mrad = ~0.02mm < MM resolution
  // .....ooooo00000OOO0O00000ooooo.....ooooo00000OOOOO00000ooooo.....
  TVector3 extrapolateToZ(const TVector3 &pos_) const {
    double mm2cm = 0.1;
    double cm2mm = 10.;
    TVector3 endpos(0,0,0);
    // extrapolate to ECAL (only if track has converged)
    if (!gfTrack.getFitStatus()->isFitConvergedFully()) {
      throw std::runtime_error("ERROR: Bad track! No extrapolation possible!");
    }
    // extrapolate
    genfit::StateOnPlane state = gfTrack.getFittedState(0);
    // Translate position to cm for GenFit
    TVector3 pos = mm2cm*pos_;
    double trackLength = gfTrack.getCardinalRep()->extrapolateToPoint(state, pos);
    // Translate back to mm
    endpos = cm2mm*state.getPos();

    // return to previous state
    gfTrack.getCardinalRep()->extrapolateBy(state, -trackLength);

    return endpos;
  }

  // Uses value obtained from extrapolation using GenFit fitted track (tracking-tools)
  TVector3 getECALHit() const {
    TVector3 hit(nan(""), nan(""), nan(""));
    try {
      hit = extrapolateToZ(ECAL_pos.pos);
    }
    catch (const runtime_error &e) {;}
    catch (const genfit::Exception &e) {;}

    return hit;
  }

  // Check existence of TrackPoint_t members
  operator bool() const { return in && out; }
};

// Complete output of Track_t
ostream& operator<<(ostream& os, const Track_t& t)
{
  os << "Track:\n";

  if (!t) return os;

  const double mrad = 1e-3; // radians to milli-radians conversion
  TVector3 extrapolatedPos = t.getECALHit();

  os << "  momentum = " << t.momentum << " GeV/c\n"
     << "  chisquare = " << t.chisquare << "\n"
     << "  p-value = " << t.pvalue << "\n"

     << "  in.theta = " << t.in.theta()/mrad << " mrad\n"
     << "  in.theta_x = " << t.in.theta_x()/mrad << " mrad\n"
     << "  in.theta_y = " << t.in.theta_y()/mrad << " mrad\n"

     << "  out.theta = " << t.out.theta()/mrad << " mrad\n"
     << "  out.theta_x = " << t.out.theta_x()/mrad << " mrad\n"
     << "  out.theta_y = " << t.out.theta_y()/mrad << " mrad\n"

     << "  in_out_angle = " << t.in_out_angle()/mrad << " mrad\n"

     << "  ECAL hit extrapolation = [" << extrapolatedPos.X() << ", " << extrapolatedPos.Y() << "]\n";

  return os;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Useful functions
// ~~~~~~ GEM ~~~~~~
bool FillAbsGEMHits(const gem::GEM* gem,
    std::vector<std::pair<TVector3, double> >& candidates)
{
  if (!(gem->bestHit)) return false;
  
  TVector3 pos = gem->bestHit.abs_pos();
  candidates.push_back(std::make_pair(pos, 0.15));
  return true;
}

// Using position from conddb
// Using position stored in json file
bool FillAbsGEMHits(const gem::GEM* gem,
    std::vector<std::pair<TVector3, double> >& candidates,
    std::string name, ConfigFileParser& parser)
{
  if (!(gem->bestHit)) return false;
  
  const TVector3 abs_pos = parser.GetConfigValueFromListWithATVector3(name, "origin");
  const double res = parser.GetConfigValueFromListWithADouble(name,"resolution");
  TVector3 pos = gem->bestHit.rel_pos() + abs_pos;
  candidates.push_back(std::make_pair(pos, res));
  return true;
}

// Using truth info from MChit
bool FillAbsGEMHitsMC(const std::vector<MChit>& gemhits,
    std::vector<std::pair<TVector3, double> >& candidates)
{
  if (gemhits.empty()) return false;

  for (unsigned int k = 0; k < gemhits.size(); k++) {
    TVector3 pos = gemhits.at(k).pos;
    candidates.push_back(std::make_pair(pos, 0.10));
  }
  return true;
}

// ~~~~~~ MM ~~~~~~
// Using position from conddb
bool FillAbsMMHits(const Micromega& mm,
    std::vector<std::pair<TVector3, double> >& candidates)
{
  if (!(mm.hasHit())) return false;
  
  const std::vector<Hit>& hits = mm.hits;
  for (unsigned int k = 0; k < hits.size(); k++) {
    TVector3 pos = hits.at(k).abspos();
    candidates.push_back(std::make_pair(pos, 0.15));
  }
  return true;
}

// Using position stored in json file
bool FillAbsMMHits(const Micromega& mm,
    std::vector<std::pair<TVector3, double> >& candidates,
    ConfigFileParser& parser)
{
  if (!(mm.hasHit())) return false;
  
  // TODO?: `if(!mm.calib) crash();`
  const std::string& name = mm.calib->name;
  const TVector3 abs_pos = parser.GetConfigValueFromListWithATVector3(name,"origin");
  const double res = parser.GetConfigValueFromListWithADouble(name,"resolution");
  const std::vector<Hit>& hits = mm.hits;
  for (unsigned int k = 0; k < hits.size(); k++) {
    TVector3 pos = hits.at(k).pos() + abs_pos;
    candidates.push_back(std::make_pair(pos, res));
  }
  return true;
}

// Using truth info from MChit
bool FillAbsMMHitsMC(const std::vector<MChit>& mmhits,
    std::vector<std::pair<TVector3, double> >& candidates)
{
  if (mmhits.empty()) return false;
  
  for (unsigned int k = 0; k < mmhits.size(); k++) {
    TVector3 pos = mmhits.at(k).pos;
    candidates.push_back(std::make_pair(pos, 0.15));
  }
  return true;
}

// ~~~~~~ Straw ~~~~~~
// Using position from conddb
bool FillAbsSTHits(const Straw& st,
    std::vector<std::pair<TVector3, double> >& candidates)
{
  if (!(st.hasHit())) return false;

  const std::vector<StHit>& hits = st.Hits;
  bool found = false;
  for (unsigned int k = 0; k < hits.size(); k++) {
    TVector3 pos = hits.at(k).abspos();
    if (hits.at(k).prob() > 0.7) {
      found = true;
      candidates.push_back(std::make_pair(pos, 0.3));
    }
  }
  return found;
}


// Using position stored in json file
bool FillAbsSTHits(const Straw& st,
    std::vector<std::pair<TVector3, double> >& candidates,
    ConfigFileParser& parser)
{
  if (!(st.hasHit())) return false;

  const std::string name = st.calib->name;
  const TVector3 abs_pos = parser.GetConfigValueFromListWithATVector3(name,"origin");
  const double res = parser.GetConfigValueFromListWithADouble(name,"resolution");
  const std::vector<StHit>& hits = st.Hits;
  bool found = false;
  for (unsigned int k = 0; k < hits.size(); k++) {
    TVector3 pos = hits.at(k).pos() + abs_pos;
    if (hits.at(k).prob() > 0.7) {
      found = true;
      candidates.push_back(std::make_pair(pos, res));
    }
  }
  return found;
}

// Using truth info from MChit
bool FillAbsSTHitsMC(const vector<MChit>& sthits,
    std::vector<std::pair<TVector3, double> >& candidates)
{
  if ((sthits.size()<1)) return false;

  for (unsigned int k = 0; k < sthits.size(); k++) {
    TVector3 pos = sthits.at(k).pos;
    candidates.push_back(std::make_pair(pos, 0.3));
  }
  return true;
}




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Initialising the tracking
// 
// Available:
// - General elec MC
// - 2021 muon run
// - 2022 muon run
// - 2022 elec run
// - 2023 elec run
// - 2024 elec run


void InitTrackingMC(const std::string& geofile)
{
  trackingMgr1 = new TrackingManager();

  // init the factory
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("NA64MC");
  factory->SetGDMLFile(geofile, 0);

  // TODO: move all geometry extraction, fallbacks, fixes into the LoadGeometryFromCalo()
  
  // TODO: current MAGU_pos / MAGD_pos variables is insufficient to describe geometry properly
  //       replace with MBPL1U/D, MBPL2U/D, MBPL3U/D ?
  
  double magstart, magend, magwidth, magheight;
  // MBPL01
  try{
    magstart = mcrunoption<double>("Magnet:Magnet1Start");
    magend = mcrunoption<double>("Magnet:Magnet1End");
    magwidth = mcrunoption<double>("Magnet:MagnetGapWidth");
    magheight = mcrunoption<double>("Magnet:MagnetGapHeight");
  }
  catch (std::runtime_error& exc) {
    std::cerr << exc.what() << std::endl;
    std::cerr << "Warning: Old format: Magnet1 defined between (MAGU_pos, MAGU_pos+2000.)." << std::endl;
    magstart = MAGU_pos.pos.Z();
    magend = magstart + 2000.;
    magwidth = 500.; magheight = 500.;
  }

  if (magstart > magend)
    std::swap(magstart, magend);

  Magnet::MBPLDimensions mbpl1(
      -0.5*magwidth,  0.5*magwidth,
      -0.5*magheight, 0.5*magheight,
      magstart, magend);
  const double fieldMag01 = MAG_field;
  const TVector3 fieldDir01(0,-1.,0);

  Magnet::MBPL* magnet01 = new Magnet::MBPL("BEND21");
  magnet01->SetMBPLDimensions(mbpl1);
  magnet01->SetFieldMag(fieldMag01);
  magnet01->SetFieldDir(fieldDir01);

  // MBPL02
  try{
    magstart = mcrunoption<double>("Magnet:Magnet2Start");
    magend = mcrunoption<double>("Magnet:Magnet2End");
    magwidth = mcrunoption<double>("Magnet:MagnetGapWidth");
    magheight = mcrunoption<double>("Magnet:MagnetGapHeight");
  }
  catch (std::runtime_error& exc) {
    std::cerr << exc.what() << std::endl;
    std::cerr << "Warning: Old format: Magnet2 defined between (MAGD_pos-2000., MAGD_pos)." << std::endl;
    magend = MAGD_pos.pos.Z();
    magstart = magend - 2000.;
    magwidth = 500.; magheight = 500.;
  }

  if (magstart > magend)
    std::swap(magstart, magend);

  Magnet::MBPLDimensions mbpl2(
      -0.5*magwidth,  0.5*magwidth,
      -0.5*magheight, 0.5*magheight,
      magstart, magend);
  const double fieldMag02 = MAG_field;
  const TVector3 fieldDir02(0,-1.,0);

  Magnet::MBPL* magnet02 = new Magnet::MBPL("BEND22");
  magnet02->SetMBPLDimensions(mbpl2);
  magnet02->SetFieldMag(fieldMag02);
  magnet02->SetFieldDir(fieldDir02);

  // register the magnets
  std::vector<Magnet::Generic*> magnets = {magnet01, magnet02};
  factory->RegisterListOfMagnets(magnets);
  trackingMgr1->RegisterFactory(factory);

  // init the algorithm
  // upstream
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(NominalBeamPDG);
  genfit->SetNominalEnergy(NominalBeamEnergy);
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::KalmanRefTrack);
  genfit->SetSmearing(0.08);
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr1->SetTrackingAlgorithm(genfit);

}

void InitTracking2021B(const int run)
{

  ConfigFileParser parser;

  if (run >= 5249 && run <= 5276) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2021mu/config-runs5249-5276.json");
  }
  else if (run >= 5277 && run <= 5438) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2021mu/config-runs5277-5438.json");
  }
  else if (run >= 5439 && run <= 5572) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2021mu/config-runs5439-5572.json");
  }

  // init the geometry
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("NA64mu2021");
  factory->SetGDMLFile(conddbpath() + "NA64Geom2021B.gdml", 0);

  // BEND 6
  Magnet::MBPLDimensions dimB6(
      -0.5*parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6_gap").X(), 
      0.5*parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6_gap").X(), 
      -0.5*parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6_gap").Y(), 
      0.5*parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6_gap").Y(), 
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6U_pos").Z(), 
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6D_pos").Z()  
      );

  const double fieldMagB6 = parser.GetConfigValueFromListWithADouble("BEND", "BEND_6_field");
  const TVector3 fieldDirB6 = parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6_dir");

  Magnet::MBPL* magnetB6 = new Magnet::MBPL("BEND6");
  magnetB6->SetMBPLDimensions(dimB6);
  magnetB6->SetFieldMag(fieldMagB6);
  magnetB6->SetFieldDir(fieldDirB6);

  // MBPL04
  Magnet::MBPLDimensions dim04(
      -0.5*parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04_gap").X(), 
      0.5*parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04_gap").X(), 
      -0.5*parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04_gap").Y(), 
      0.5*parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04_gap").Y(), 
      parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04U_pos").Z(), 
      parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04D_pos").Z()  
      );

  const double fieldMag04 = parser.GetConfigValueFromListWithADouble("MBPL","MBPL04_field");
  const TVector3 fieldDir04 = parser.GetConfigValueFromListWithATVector3("MBPL","MBPL04_dir");

  Magnet::MBPL* magnet04 = new Magnet::MBPL("MBPL");
  magnet04->SetMBPLDimensions(dim04);
  magnet04->SetFieldMag(fieldMag04);
  magnet04->SetFieldDir(fieldDir04);

  // register the magnets
  std::vector<Magnet::Generic*> magnets = {magnetB6, magnet04};
  factory->RegisterListOfMagnets(magnets);
  trackingMgr1->RegisterFactory(factory);
  trackingMgr2->RegisterFactory(factory);

  // init the algorithm

  // upstream
  TrackingAnalytical* analytical = new TrackingAnalytical;

  analytical->SetNumberOfPlanes(3);
  analytical->SetSmearing(0.0);
  analytical->SetMagnet(magnetB6);
  if (run >= 5249 && run <= 5438)  analytical->SetBackPropagate(false);
  else if (run >= 5439 && run <= 5572)  analytical->SetBackPropagate(true);

  trackingMgr1->SetTrackingAlgorithm(analytical);

  // downstream
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(-13);
  genfit->SetNominalEnergy(160.);
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::KalmanRefTrack);
  genfit->SetSmearing(0.08);
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr2->SetTrackingAlgorithm(genfit);

}

void InitTracking2022A(const int run = 0)
{

  ConfigFileParser parser;
  parser.SetConfigFile(conddbpath() + "trackingtools/2022mu/config.json");

  // init the geometry
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("NA64mu2022");
  factory->SetGDMLFile(conddbpath() + "trackingtools/2022mu/geometry.gdml", 0);

  // BEND 6
  Magnet::MBPLDimensions dimB6(
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6").X()-520.5,
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6B").X()+520.5,
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6B").Y()-285.5,
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6").Y()+285.5,
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6").Z()-2500.5,
      parser.GetConfigValueFromListWithATVector3("BEND", "BEND_6B").Z()+2500.5);

  const double fieldMagB6 = parser.GetConfigValueFromListWithADouble("BEND", "Mag");
  const TVector3 fieldDirB6 = parser.GetConfigValueFromListWithATVector3("BEND", "Dir");

  Magnet::MBPL* magnetB6 = new Magnet::MBPL("BEND6");
  magnetB6->SetMBPLDimensions(dimB6);
  magnetB6->SetFieldMag(fieldMagB6);
  magnetB6->SetFieldDir(fieldDirB6);

  // MBPL04
  Magnet::MBPLDimensions dim04(
      parser.GetConfigValueFromListWithATVector2("MBPL", "MBPL04_X").X(),
      parser.GetConfigValueFromListWithATVector2("MBPL", "MBPL04_X").Y(),
      parser.GetConfigValueFromListWithATVector2("MBPL", "MBPL04_Y").X(),
      parser.GetConfigValueFromListWithATVector2("MBPL", "MBPL04_Y").Y(),
      parser.GetConfigValueFromListWithATVector2("MBPL", "MBPL04_Z").X(),
      parser.GetConfigValueFromListWithATVector2("MBPL", "MBPL04_Z").Y());

  const double fieldMag04 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir04 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet04 = new Magnet::MBPL("MBPL");
  magnet04->SetMBPLDimensions(dim04);
  magnet04->SetFieldMag(fieldMag04);
  magnet04->SetFieldDir(fieldDir04);

  // register the magnets
  std::vector<Magnet::Generic*> magnets = {magnetB6, magnet04};
  factory->RegisterListOfMagnets(magnets);
  trackingMgr1->RegisterFactory(factory);
  trackingMgr2->RegisterFactory(factory);

  // init the algorithm

  // upstream
  TrackingAnalytical* analytical = new TrackingAnalytical;

  analytical->SetNumberOfPlanes(4);
  analytical->SetSmearing(0.0);
  analytical->SetMagnet(magnetB6);
  analytical->SetBackPropagate(false);

  trackingMgr1->SetTrackingAlgorithm(analytical);

  // downstream
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(-13);
  genfit->SetNominalEnergy(160.);
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::KalmanRefTrack);
  genfit->SetSmearing(0.08);
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr2->SetTrackingAlgorithm(genfit);

}

void InitTracking2022B(const int run)
{

  ConfigFileParser parser;
  if (run >= 6035 && run <= 7089) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config-runs6035-7089.json");
  }
  else if (run>=8297 && run<= 8323){ //positrons
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config-runs8297-8323.json");
  }
  else if (run >= 8396 && run <= 8458) {//hadrons
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config-runs8396-8458.json");
  }
  else {
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config.json");
  }

  // init the geometry
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("NA64elec2022");

  // This file was produced using example106 from NA64 simulation
  // commit hash c59beadc85c1d6b9fbca765c21ab5464420a9c68
  // by Benjamin Banto Oberhauser, 09.12.2022
  factory->SetGDMLFile(conddbpath() + "/trackingtools/2022elec/geometry.gdml", 0);

  // upstream

  // BEND21
  Magnet::MBPLDimensions mbpl1(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").Z());

  const double fieldMag01 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir01 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet01 = new Magnet::MBPL("BEND21");
  magnet01->SetMBPLDimensions(mbpl1);
  magnet01->SetFieldMag(fieldMag01);
  magnet01->SetFieldDir(fieldDir01);

  // BEND22
  Magnet::MBPLDimensions mbpl2(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").Z());

  const double fieldMag02 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir02 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet02 = new Magnet::MBPL("BEND22");
  magnet02->SetMBPLDimensions(mbpl2);
  magnet02->SetFieldMag(fieldMag02);
  magnet02->SetFieldDir(fieldDir02);

  // downstream

  // MBPL03
  Magnet::MBPLDimensions mbpl3(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").Z());

  const double fieldMag03 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir03 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet03 = new Magnet::MBPL("BEND23");
  magnet03->SetMBPLDimensions(mbpl3);
  magnet03->SetFieldMag(fieldMag03);
  magnet03->SetFieldDir(fieldDir03);

  // register the magnets
  std::vector<Magnet::Generic*> magnets = {magnet01, magnet02, magnet03};
  factory->RegisterListOfMagnets(magnets);
  trackingMgr1->RegisterFactory(factory);
  trackingMgr2->RegisterFactory(factory);

  // init the algorithm

  // upstream
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(NominalBeamPDG);
  genfit->SetNominalEnergy(NominalBeamEnergy);
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::DAF);
  genfit->SetSmearing(0.08);
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr1->SetTrackingAlgorithm(genfit);

  // downstream
  trackingMgr2->SetTrackingAlgorithm(genfit);

}


void InitTracking2023A(const int run)
{

  ConfigFileParser parser;
  // Positron runs at 70 GeV
  const bool isPositronRuns = (9557 <= run && run <= 9588);
  // Hadron runs with at 40 GeV
  const bool isHadronRuns = (9592 <= run && run <= 9714);
  if (isPositronRuns)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-positron.json");
  else if (isHadronRuns)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-hadron.json");
  else
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config.json");

  // init the geometry
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("NA64elec2023");

  // -- Geometry for hadron runs --
  // TODO: Replace current empty geometry for hadron runs
  // Created by Benjamin Banto Oberhauser, 28.07.2023
  if (isHadronRuns)
    factory->SetGDMLFile(conddbpath() + "trackingtools/2023elec/geometry_hadron.gdml", 0);
  // -- Geometry for electron and positron runs --
  // This file was produced using example106 from NA64 simulation
  // commit hash 1154d4257bbe1d79da0a5a75a213c2cdc9cb9424
  // Created by Benjamin Banto Oberhauser, 26.06.2023
  else
    factory->SetGDMLFile(conddbpath() + "trackingtools/2023elec/geometry.gdml", 0);

  // upstream

  // BEND21
  Magnet::MBPLDimensions mbpl1(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").Z());

  const double fieldMag01 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir01 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet01 = new Magnet::MBPL("BEND21");
  magnet01->SetMBPLDimensions(mbpl1);
  magnet01->SetFieldMag(fieldMag01);
  magnet01->SetFieldDir(fieldDir01);

  // BEND22
  Magnet::MBPLDimensions mbpl2(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").Z());

  const double fieldMag02 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir02 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet02 = new Magnet::MBPL("BEND22");
  magnet02->SetMBPLDimensions(mbpl2);
  magnet02->SetFieldMag(fieldMag02);
  magnet02->SetFieldDir(fieldDir02);

  // downstream

  // MBPL03
  Magnet::MBPLDimensions mbpl3(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").Z());

  const double fieldMag03 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir03 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet03 = new Magnet::MBPL("BEND23");
  magnet03->SetMBPLDimensions(mbpl3);
  magnet03->SetFieldMag(fieldMag03);
  magnet03->SetFieldDir(fieldDir03);

  // register the magnets
  std::vector<Magnet::Generic*> magnets = {magnet01, magnet02, magnet03};
  factory->RegisterListOfMagnets(magnets);
  trackingMgr1->RegisterFactory(factory);
  trackingMgr2->RegisterFactory(factory);

  // init the algorithm

  // upstream
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(NominalBeamPDG);
  genfit->SetNominalEnergy(NominalBeamEnergy);
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::DAF);
  genfit->SetSmearing(0.08);
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr1->SetTrackingAlgorithm(genfit);

  // downstream
  trackingMgr2->SetTrackingAlgorithm(genfit);

}


void InitTracking2024A(const int run)
{

  ConfigFileParser parser;
  // TODO: Replace with survey measurements
  parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config.json");

  // init the geometry
  DetectorFactory* factory = new DetectorFactory();
  factory->SetFactoryName("NA64elec2024");

  // -- Geometry for electron and positron runs --
  // TODO: Replace with output from simulation after survey measurements are available
  // Created by Benjamin Banto Oberhauser, 21.05.2024
  factory->SetGDMLFile(conddbpath() + "trackingtools/2024elec/empty_geom.gdml", 0);

  // upstream

  // BEND21
  Magnet::MBPLDimensions mbpl1(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND21D").Z());

  const double fieldMag01 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir01 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet01 = new Magnet::MBPL("BEND21");
  magnet01->SetMBPLDimensions(mbpl1);
  magnet01->SetFieldMag(fieldMag01);
  magnet01->SetFieldDir(fieldDir01);

  // BEND22
  Magnet::MBPLDimensions mbpl2(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND22D").Z());

  const double fieldMag02 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir02 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet02 = new Magnet::MBPL("BEND22");
  magnet02->SetMBPLDimensions(mbpl2);
  magnet02->SetFieldMag(fieldMag02);
  magnet02->SetFieldDir(fieldDir02);

  // downstream

  // MBPL03
  Magnet::MBPLDimensions mbpl3(
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").X(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").Y(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23U").Z(),
      parser.GetConfigValueFromListWithATVector3("MBPL", "BEND23D").Z());

  const double fieldMag03 = parser.GetConfigValueFromListWithADouble("MBPL", "Mag");
  const TVector3 fieldDir03 = parser.GetConfigValueFromListWithATVector3("MBPL", "Dir");

  Magnet::MBPL* magnet03 = new Magnet::MBPL("BEND23");
  magnet03->SetMBPLDimensions(mbpl3);
  magnet03->SetFieldMag(fieldMag03);
  magnet03->SetFieldDir(fieldDir03);

  // register the magnets
  std::vector<Magnet::Generic*> magnets = {magnet01, magnet02, magnet03};
  factory->RegisterListOfMagnets(magnets);
  trackingMgr1->RegisterFactory(factory);
  trackingMgr2->RegisterFactory(factory);

  // init the algorithm

  // upstream
  TrackingGenFit* genfit = new TrackingGenFit();

  genfit->SetPDG(NominalBeamPDG);
  genfit->SetNominalEnergy(NominalBeamEnergy);
  genfit->SetGDMLManager(factory->GetGDMLGeoManager());
  genfit->SetMBPLField(true);
  genfit->SetNIter(8);
  genfit->InitFitter(TrackingGenFit::DAF);
  genfit->SetSmearing(0.08);
  genfit->SetMagnets(magnets);
  genfit->InitGeomAndFields();
  genfit->InitMeasurementsFactory();

  trackingMgr1->SetTrackingAlgorithm(genfit);

  // downstream
  trackingMgr2->SetTrackingAlgorithm(genfit);

}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Reconstruct the tracks
// 
// Available:
// - 2021 muon run
// - 2022 muon run
// - 2022 elec run
// - 2023 elec run
// - 2024 elec run


std::vector<Track_t> GetReconstructedTracksFromHits(
    const std::vector<std::pair<TVector3, double> >& hits,
    TrackingManager* tMgr = trackingMgr1)
{
  std::vector<Track_t> tracks;

  if (!tMgr) return tracks;

  // init the tracking and fit
  tMgr->SetInputs(hits);
  tMgr->FindTrackCandidates();
  if (kVERBOSE) tMgr->GetFactory()->PrintListOfRegisteredTrackers(true);

  // Silence ROOT errors
  auto backupErrorIgnoreLevel = gErrorIgnoreLevel;
  if (!kVERBOSE) gErrorIgnoreLevel = kBreak;

  try {
    tMgr->FitTrackCandidates();
  }
  catch (genfit::Exception& e) {
    if (kVERBOSE) {
      std::cerr << e.what();
      std::cerr << "genfit: Exception, next event" << std::endl;
    }

    tMgr->Clean();
    gErrorIgnoreLevel = backupErrorIgnoreLevel;

    return tracks;
  }
  gErrorIgnoreLevel = backupErrorIgnoreLevel;

  // gather results
  std::vector<std::pair<double, double> > fittedTracks = tMgr->GetFittedTracks();
  std::vector<Tracking::Track > recoTracks = ((TrackingGenFit*) tMgr->GetTrackingAlgorithm())->GetReconstructedTracks();

  for (unsigned int k = 0; k < recoTracks.size(); k++) {
    Track_t track(recoTracks.at(k));

    // ...
    if (kVERBOSE) std::cout << track << std::endl;

    tracks.push_back(track);
  }

  tMgr->Clean();

  return tracks;

}



std::vector<Track_t> GetReconstructedTracks2021B(
    const RecoEvent& e, const bool upstream)
{

  ConfigFileParser parser;
  if (e.run >= 5249 && e.run <= 5276) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2021mu/config-runs5249-5276.json");
  }
  else if (e.run >= 5277 && e.run <= 5438) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2021mu/config-runs5277-5438.json");
  }
  else if (e.run >= 5439 && e.run <= 5572) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2021mu/config-runs5439-5572.json");
  }
  std::vector<Track_t> tracks;

  if (!trackingMgr1 && !trackingMgr2) return tracks;

  if (upstream) {

    // TODO: add straws

    // pre-process the hits (using GEMs and MMs)
    std::vector<std::pair<TVector3, double> > hits;
    if (e.run >= 5249 && e.run <= 5438) {
      FillAbsMMHits(e.MM1, hits, parser);
      FillAbsMMHits(e.MM2, hits, parser);
      FillAbsMMHits(e.MM3, hits, parser);
      FillAbsMMHits(e.MM4, hits, parser);
    }
    else {
      FillAbsMMHits(e.MM2, hits, parser);
      FillAbsMMHits(e.MM3, hits, parser);
      FillAbsMMHits(e.MM4, hits, parser);

      // NOTE: physical GEMs from in z ascending order are 
      // - GEMs 4-2-3-1 if run <= 5438
      // - GEMs 2-3-1-4 if run >= 5439
      FillAbsGEMHits(e.GM04, hits, "GEM4", parser);
    }

    // init the tracking and fit
    trackingMgr1->SetInputs(hits);
    trackingMgr1->FindTrackCandidates();
    if (kVERBOSE) trackingMgr1->GetFactory()->PrintListOfRegisteredTrackers(true);

    trackingMgr1->FitTrackCandidates();

    // gather results
    std::vector<std::pair<double, double> > fittedTracks = trackingMgr1->GetFittedTracks();

    for (unsigned int k = 0; k < fittedTracks.size(); k++) {
      Track_t track;
      track.momentum = std::get<0>(fittedTracks.at(k));
      track.chisquare = std::get<1>(fittedTracks.at(k));

      // ...
      if (kVERBOSE) std::cout << "upstream = " << track.momentum << ", " << track.chisquare << std::endl;

      tracks.push_back(track);
    }

    trackingMgr1->Clean();

  }
  else {

    // TODO: add straws

    // NOTE: physical GEMs from in z ascending order are 
    // - GEMs 4-2-3-1 if run <= 5438
    // - GEMs 2-3-1-4 if run >= 5439
    std::vector<std::pair<TVector3, double> > hits;
    if (e.run <= 5438) {
      FillAbsGEMHits(e.GM04, hits, "GEM4", parser);
      FillAbsGEMHits(e.GM02, hits, "GEM2", parser);
      FillAbsGEMHits(e.GM03, hits, "GEM3", parser);
      FillAbsGEMHits(e.GM01, hits, "GEM1", parser);
    }
    else {
      FillAbsGEMHits(e.GM02, hits, "GEM2", parser);
      FillAbsGEMHits(e.GM03, hits, "GEM3", parser);
      FillAbsGEMHits(e.GM01, hits, "GEM1", parser);
      FillAbsGEMHits(e.GM04, hits, "GEM4", parser);
    }
    FillAbsMMHits(e.MM5, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    FillAbsMMHits(e.MM7, hits, parser);

    // init the tracking and fit
    trackingMgr2->SetInputs(hits);
    trackingMgr2->FindTrackCandidates();
    if (kVERBOSE) trackingMgr2->GetFactory()->PrintListOfRegisteredTrackers(true);

    try {
      trackingMgr2->FitTrackCandidates();
    }
    catch (genfit::Exception& e) {
      if (kVERBOSE) {
        std::cerr << e.what();
        std::cerr << "genfit: Exception, next event" << std::endl;
      }

      trackingMgr2->Clean();

      return tracks;
    }

    // gather results
    std::vector<std::pair<double, double> > fittedTracks = trackingMgr2->GetFittedTracks();
    std::vector<Tracking::Track > recoTracks = ((TrackingGenFit*) trackingMgr2->GetTrackingAlgorithm())->GetReconstructedTracks();

    for (unsigned int k = 0; k < recoTracks.size(); k++) {
      Track_t track(recoTracks.at(k));

      // ...
      if (kVERBOSE) std::cout << "downstream =\n" << track << std::endl;

      tracks.push_back(track);
    }

    trackingMgr2->Clean();

  }

  return tracks;

}

std::vector<Track_t> GetReconstructedTracks2022A(
    const RecoEvent& e, const bool upstream)
{

  ConfigFileParser parser;
  parser.SetConfigFile(conddbpath() + "trackingtools/2022mu/config.json");
  std::vector<Track_t> tracks;

  if (!trackingMgr1 && !trackingMgr2) return tracks;

  if (upstream) {

    // TODO: upstream to be improved + add straws

    // pre-process the hits 
    std::vector<std::pair<TVector3, double> > hits;
    FillAbsMMHits(e.MM1, hits, parser);
    FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM3, hits, parser);
    FillAbsMMHits(e.MM4, hits, parser);

    // init the tracking and fit
    trackingMgr1->SetInputs(hits);
    trackingMgr1->FindTrackCandidates();
    if (kVERBOSE) trackingMgr1->GetFactory()->PrintListOfRegisteredTrackers(true);

    trackingMgr1->FitTrackCandidates();

    // gather results
    std::vector<std::pair<double, double> > fittedTracks = trackingMgr1->GetFittedTracks();

    for (unsigned int k = 0; k < fittedTracks.size(); k++) {
      Track_t track;
      track.momentum = std::get<0>(fittedTracks.at(k));
      track.chisquare = std::get<1>(fittedTracks.at(k));

      // ...
      if (kVERBOSE) std::cout << "upstream = " << track.momentum << ", " << track.chisquare << std::endl;

      tracks.push_back(track);
    }

    trackingMgr1->Clean();

  }
  else {

    // TODO: add straws

    // pre-process the hits (using GEMs and MMs)
    std::vector<std::pair<TVector3, double> > hits;
    FillAbsGEMHits(e.GM01, hits, "GEM1", parser);
    FillAbsGEMHits(e.GM02, hits, "GEM2", parser);
    FillAbsGEMHits(e.GM03, hits, "GEM3", parser);
    FillAbsGEMHits(e.GM04, hits, "GEM4", parser);
    FillAbsMMHits(e.MM5, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    FillAbsMMHits(e.MM7, hits, parser);

    // init the tracking and fit
    trackingMgr2->SetInputs(hits);
    trackingMgr2->FindTrackCandidates();
    if (kVERBOSE) trackingMgr2->GetFactory()->PrintListOfRegisteredTrackers(true);

    try {
      trackingMgr2->FitTrackCandidates();
    }
    catch (genfit::Exception& e) {
      if (kVERBOSE) {
        std::cerr << e.what();
        std::cerr << "genfit: Exception, next event" << std::endl;
      }

      trackingMgr2->Clean();

      return tracks;
    }

    // gather results
    std::vector<std::pair<double, double> > fittedTracks = trackingMgr2->GetFittedTracks();
    std::vector<Tracking::Track > recoTracks = ((TrackingGenFit*) trackingMgr2->GetTrackingAlgorithm())->GetReconstructedTracks();

    for (unsigned int k = 0; k < recoTracks.size(); k++) {
      Track_t track(recoTracks.at(k));

      // ...
      if (kVERBOSE) std::cout << "downstream =\n" << track << std::endl;

      tracks.push_back(track);
    }

    trackingMgr2->Clean();

  }

  return tracks;

}


std::vector<Track_t> GetReconstructedTracks2022B(
    const RecoEvent& e, const bool upstream)
{
  ConfigFileParser parser;
  if (e.run >= 6035 && e.run <= 7089) {
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config-runs6035-7089.json");
  }
  else if (e.run>=8297 && e.run<= 8323){ //positrons
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config-runs8297-8323.json");
  }
  else if (e.run >= 8396 && e.run <= 8458) {//hadrons
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config-runs8396-8458.json");
  }
  else {
    parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config.json");
  }
  std::vector<Track_t> tracks;

  if (!trackingMgr1 && !trackingMgr2) return tracks;

  if (upstream) {

    // TODO: upstream to be improved + add straws

    // pre-process the hits
    std::vector<std::pair<TVector3, double> > hits;
    size_t nUpStream = 0;
    size_t nDownStream = 0;
    // Return empty tracks if no upstream hits are found
    if (FillAbsMMHits(e.MM1, hits, parser)) ++nUpStream;
    if (FillAbsMMHits(e.MM2, hits, parser)) ++nUpStream;
    if (nUpStream < 2) return tracks;
    // Return empty tracks if no downstream hits are found
    if (FillAbsMMHits(e.MM3, hits, parser)) ++nDownStream;
    if (FillAbsMMHits(e.MM4, hits, parser)) ++nDownStream;
    if (FillAbsGEMHits(e.GM01, hits, "GEM1", parser)) ++nDownStream;
    if (FillAbsGEMHits(e.GM02, hits, "GEM2", parser)) ++nDownStream;
    if (nDownStream < 2) return tracks;
    //FillAbsSTHits(e.ST01, hits, parser);
    //FillAbsSTHits(e.ST02, hits, parser);
    //FillAbsSTHits(e.ST03, hits, parser);
    //FillAbsSTHits(e.ST04, hits, parser);

    return GetReconstructedTracksFromHits(hits, trackingMgr1);

  } else {

    // TODO: add straws

    // pre-process the hits (using GEMs and MMs)
    std::vector<std::pair<TVector3, double> > hits;
    FillAbsMMHits(e.MM3, hits, parser);
    FillAbsMMHits(e.MM4, hits, parser);
    FillAbsGEMHits(e.GM01, hits, "GEM1", parser);
    FillAbsGEMHits(e.GM02, hits, "GEM2", parser);
    FillAbsMMHits(e.MM5, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    FillAbsMMHits(e.MM7, hits, parser);

    return GetReconstructedTracksFromHits(hits, trackingMgr2);
  }

  return tracks;
}


std::vector<Track_t> GetReconstructedTracks2023A(
    const RecoEvent& e, const bool upstream)
{
  ConfigFileParser parser;
  if (e.run >= 8536 && e.run < 8548)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-runs8536-8547.json"); // MM2, MM4, MM6, MM7 moved
  else if (e.run >= 8548 && e.run < 8677)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-runs8548-8676.json"); // MM2 moved up
  else if (e.run >= 8677 && e.run < 8948)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-runs8677-8947.json"); // MM4 replaced
  else if (e.run >= 8948 && e.run < 9160)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config.json");               // MM3 replaced, survey after run 9059
  else if (e.run >= 9160 && e.run < 9332)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-runs9160-9331.json"); // MM3 fixed
  else if (e.run >= 9332 && e.run < 9557)
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-runs9332-9556.json"); // MM3 replaced
  else if (e.run >= 9557 && e.run < 9589)                                                    // positron runs 9557 - 9588
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-positron.json");
  else if (e.run >= 9589 && e.run < 9714)                                                    // hadron runs 9589 - 9714
    parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config-hadron.json");

  std::vector<Track_t> tracks;
  if (!trackingMgr1 && !trackingMgr2) return tracks;

  if (upstream) {

    // pre-process the hits
    std::vector<std::pair<TVector3, double> > hits;
    size_t nUpStream = 0;
    size_t nDownStream = 0;
    // Return empty tracks if no upstream hits are found
    if (FillAbsMMHits(e.MM1, hits, parser)) ++nUpStream;
    if (FillAbsMMHits(e.MM2, hits, parser)) ++nUpStream;
    if (FillAbsSTHits(e.ST01, hits, parser)) ++nUpStream;
    if (FillAbsSTHits(e.ST02, hits, parser)) ++nUpStream;
    if (nUpStream < 2) return tracks;
    // Return empty tracks if no downstream hits are found
    if (FillAbsMMHits(e.MM3, hits, parser)) ++nDownStream;
    if (FillAbsMMHits(e.MM4, hits, parser)) ++nDownStream;
    if (FillAbsGEMHits(e.GM01, hits, "GEM1", parser)) ++nDownStream;
    if (FillAbsGEMHits(e.GM02, hits, "GEM2", parser)) ++nDownStream;
    if (FillAbsSTHits(e.ST03, hits, parser)) ++nDownStream;
    if (FillAbsSTHits(e.ST04, hits, parser)) ++nDownStream;
    if (nDownStream < 2) return tracks;

    return GetReconstructedTracksFromHits(hits, trackingMgr1);

  } else {

    // TODO: check performance of downstream tracking

    // pre-process the hits (using GEMs and MMs)
    std::vector<std::pair<TVector3, double> > hits;
    FillAbsMMHits(e.MM3, hits, parser);
    FillAbsMMHits(e.MM4, hits, parser);
    FillAbsGEMHits(e.GM01, hits, "GEM1", parser);
    FillAbsGEMHits(e.GM02, hits, "GEM2", parser);
    FillAbsMMHits(e.MM5, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    FillAbsMMHits(e.MM7, hits, parser);

    return GetReconstructedTracksFromHits(hits, trackingMgr2);
  }

  return tracks;
}

std::vector<Track_t> GetReconstructedTracks2024A(
    const RecoEvent& e, const bool upstream)
{
  ConfigFileParser parser;
  parser.SetConfigFile(conddbpath() + "trackingtools/2023elec/config.json");

  std::vector<Track_t> tracks;
  if (!trackingMgr1 && !trackingMgr2) return tracks;

  if (upstream) {

    // pre-process the hits
    std::vector<std::pair<TVector3, double> > hits;
    size_t nUpStream = 0;
    size_t nDownStream = 0;
    // Return empty tracks if no upstream hits are found
    if (FillAbsMMHits(e.MM1, hits, parser)) ++nUpStream;
    if (FillAbsMMHits(e.MM2, hits, parser)) ++nUpStream;
    if (FillAbsSTHits(e.ST01, hits, parser)) ++nUpStream;
    if (FillAbsSTHits(e.ST02, hits, parser)) ++nUpStream;
    if (nUpStream < 2) return tracks;
    // Return empty tracks if no downstream hits are found
    if (FillAbsMMHits(e.MM3, hits, parser)) ++nDownStream;
    if (FillAbsMMHits(e.MM4, hits, parser)) ++nDownStream;
    if (FillAbsGEMHits(e.GM01, hits, "GEM1", parser)) ++nDownStream;
    if (FillAbsGEMHits(e.GM02, hits, "GEM2", parser)) ++nDownStream;
    if (FillAbsSTHits(e.ST03, hits, parser)) ++nDownStream;
    if (FillAbsSTHits(e.ST04, hits, parser)) ++nDownStream;
    if (nDownStream < 2) return tracks;

    return GetReconstructedTracksFromHits(hits, trackingMgr1);

  } else {

    // TODO: check performance of downstream tracking

    // pre-process the hits (using GEMs and MMs)
    std::vector<std::pair<TVector3, double> > hits;
    FillAbsMMHits(e.MM3, hits, parser);
    FillAbsMMHits(e.MM4, hits, parser);
    FillAbsGEMHits(e.GM01, hits, "GEM1", parser);
    FillAbsGEMHits(e.GM02, hits, "GEM2", parser);
    FillAbsMMHits(e.MM5, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    FillAbsMMHits(e.MM7, hits, parser);

    return GetReconstructedTracksFromHits(hits, trackingMgr2);
  }

  return tracks;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....
// Main functions

void InitTracking(const unsigned int runID)
{

  // 2021 muon run
  if (runID >= 5249 && runID <= 5572) {
    if (!trackingMgr1 && !trackingMgr2) {
      trackingMgr1 = new TrackingManager();
      trackingMgr2 = new TrackingManager();
      InitTracking2021B(runID);
    }
  }

  // 2022 muon run
  else if (runID >= 5573 && runID <= 6034) {
    if (!trackingMgr1 && !trackingMgr2) {
      trackingMgr1 = new TrackingManager();
      trackingMgr2 = new TrackingManager();
      InitTracking2022A(runID);
    }
  }

  // 2022 electron run
  else if (runID >= 6035 && runID <= 8458) {
    if (!trackingMgr1 && !trackingMgr2) {
      trackingMgr1 = new TrackingManager();
      trackingMgr2 = new TrackingManager();
      InitTracking2022B(runID);
    }
  }

  // 2023 electron run
  else if (runID >= 8459 && runID <= 9717) {
    if (!trackingMgr1 && !trackingMgr2) {
      trackingMgr1 = new TrackingManager();
      trackingMgr2 = new TrackingManager();
      InitTracking2023A(runID);
    }
  }

  // 2024 electron run
  else if (runID >= 10316) {
    if (!trackingMgr1 && !trackingMgr2) {
      trackingMgr1 = new TrackingManager();
      trackingMgr2 = new TrackingManager();
      InitTracking2024A(runID);
    }
  }

  else {
    std::cerr << "WARNING: (tracking_new) unsupported run number " << runID << std::endl;
  }

}

// Default function for electron mode MC
std::vector<Track_t> GetReconstructedTracksMC(const RecoEvent& e)
{
  // RECO HITS
  std::vector<std::pair<TVector3, double> > hits;
  FillAbsMMHits(e.MM1, hits);
  FillAbsMMHits(e.MM2, hits);
  FillAbsMMHits(e.MM3, hits);
  FillAbsMMHits(e.MM4, hits);
  FillAbsGEMHitsMC(e.mc->GEM1truth, hits);
  FillAbsGEMHitsMC(e.mc->GEM2truth, hits);
  FillAbsGEMHitsMC(e.mc->GEM3truth, hits);
  FillAbsGEMHitsMC(e.mc->GEM4truth, hits);
  FillAbsSTHits(e.ST01, hits);
  FillAbsSTHits(e.ST02, hits);
  FillAbsSTHits(e.ST03, hits);
  FillAbsSTHits(e.ST04, hits);

  return GetReconstructedTracksFromHits(hits);
}

// Convenience function
std::vector<Track_t> GetReconstructedTracks(const RecoEvent& e, const bool upstream=true)
{
  if (e.mc) return GetReconstructedTracksMC(e);

#if 0
  // memory usage
  struct rusage r_usage;
  getrusage(RUSAGE_SELF, &r_usage);
  std::cout << "\033[1;32m ==> check: maxrss = " << r_usage.ru_maxrss << " kb ...\033[0m" << std::endl;
#endif

  InitTracking(e.run);

  // 2021 muon run
  if (e.run >= 5249 && e.run <= 5572) return GetReconstructedTracks2021B(e, upstream);

  // 2022 muon run
  else if (e.run >= 5573 && e.run <= 6034) return GetReconstructedTracks2022A(e, upstream);

  // 2022 electron run
  else if (e.run >= 6035 && e.run <= 8458) return GetReconstructedTracks2022B(e, upstream);

  // 2023 electron run
  else if (e.run >= 8459 && e.run <= 9717) return GetReconstructedTracks2023A(e, upstream);

  // 2024 electron run
  else if (e.run >= 10316) return GetReconstructedTracks2024A(e, upstream);

  // unsupported run number
  else {
    std::cerr << "WARNING: (tracking_new) unsupported run number " << e.run << std::endl;
    return std::vector<Track_t>();
  }
}
