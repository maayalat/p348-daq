#pragma once

#include "p348reco.h"

// zero energy if the cell is out-of-time
inline void timecut(Cell& c, const double master, const double tdevmax)
{
  if (!c.hasDigit) return;
  
  if (fabs((c.t0ns() - c.calib->texpected(master))/c.calib->tsigma) > tdevmax)
    c.energy = 0.;
}

// zero energy of all out-of-time cells
void timecut(RecoEvent& e, const double tdevmax)
{
  const double master = e.masterTime;
  
  for (int d = 0; d < 2; ++d)
    for (int x = 0; x < 12; ++x)
      for (int y = 0; y < 6; ++y)
        timecut(e.ECAL[d][x][y], master, tdevmax);
  
  for (int d = 0; d < 4; ++d)
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 3; ++y)
        timecut(e.HCAL[d][x][y], master, tdevmax);
  
  for (int d = 0; d < 2; ++d)
    for (int x = 0; x < 4; ++x)
      for (int y = 0; y < 4; ++y)
        timecut(e.VHCAL[d][x][y], master, tdevmax);
  
  for (int i = 0; i < 4; ++i) timecut(e.SRD[i], master, tdevmax);
  
  for (int i = 0; i < 6; ++i) timecut(e.VETO[i], master, tdevmax);
  
  for (int i = 0; i < 3; ++i) timecut(e.WCAL[i], master, tdevmax);
  
  timecut(e.WCAT, master, tdevmax);
  timecut(e.VTWC, master, tdevmax);
  timecut(e.VTEC, master, tdevmax);
  timecut(e.S2, master, tdevmax);
  timecut(e.V2, master, tdevmax);
  timecut(e.S4, master, tdevmax);
}
