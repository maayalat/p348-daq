#pragma once

#include <fstream>
//root
#include "TVector3.h"
#include "TRandom.h"
#include "TMath.h"

//na64
#include "p348reco.h"
#include "mm.h"
#include "conddb.h"

#ifdef TRACKINGTOOLS
#include "tracking_new.h"
#endif


//GLOBALS
const double MCsignalCalibrationX = 0.103;
const double MCsignalCalibrationY = 0.164;
const int MCsizeX = 4;
const int MCsizeY = 11;
const bool simulatenoise = true;
const double sigmaLevel = 2.;
//method to compute charge spread
enum DigiMethod {STD,GAUS};
const DigiMethod MM_chargespread = GAUS;
//Maximum threshold for detectors
const double MM_threshold = 22e-6; //22 keV, taken from https://www.sciencedirect.com/science/article/pii/S1875389212018019
const double GEM_threshold = 22e-6; // same, waiting input for Michael Hoesgen
const double ST_threshold = 15e-8; // 0.15 keV, taken from CERN-PPE/94-224, waiting input from straw experts
//gem resolution in mm
double GEMyres = 0.060;
double GEMxres = 0.060;
//maximal distance before the merging
double MergingDistance = 1.75; //mm
//decide if to replicate fake hits
const bool ReplicateHits = true;


/* === SMEARING FUNCTIONS === */

/* This structure includes the parameters necessary to smear a MC cell.
 * It also includes the function to return the sigma value, in energy units.
 * For the moment, the following possibilities are foreseen:
 * - Zero value (kNull) (yes, it is a specific case than the one below, but it is for clarity here)
 * - Constant energy value (kConstant)
 * - Resolution proportional to the energy (kProportional)
 * - Resolution given by two terms, summed in quadrature: constant and proportional to energy (kConstantPlusProportional)
 * A note on the constant energy value: for cells with low energy deposition before smearing, this can result in a negative post-smearing energy for the cell. While, on average, this does not affect mean values, it can be problematic for some analysis.
 * Also, this does not reflect the true meaning of this term, that is possibly associated with noise, whose model is non gaussian (negative energies in real data are not allowed).
 * Therefore, for the moment this is fixed by the prescription of not doing the smearing if the energy (pre-smearing) is less then N*constantVal, where N is provided by user (default:3)
 *
 * Note that it is up to the initialization code below to instantiate the values of the parameters
 *
 * */

struct SmearingCellMC {
    enum {kNull,kConstant,kProportional,kConstantPlusProportional} resType{kNull};
    int kCutVal{3};
    double kConstantVal{0};
    double kProportionalVal{0};


    //set methods
  void setConstant(double constantVal,int Ncut=3){
        resType=kConstant;
        kConstantVal=constantVal;
        kCutVal=Ncut;
    }
    void setProportional(double proportionalVal,int Ncut=3){
        resType=kProportional;
        kProportionalVal=proportionalVal;
        kCutVal=Ncut;
    }
  void setConstantPlusProportional(double constantVal,double proportionalVal,int Ncut=3){
        resType=kConstantPlusProportional;
        kConstantVal=constantVal;
        kProportionalVal=proportionalVal;
        kCutVal=Ncut;
    }

    double getSigma(double energy){
        switch (resType){
        case kNull:
            return 0.;
        case kConstant:
            return kConstantVal;
        case kProportional: //sigma_E/E = kProportionalVal; sigma_E = E * kProportionalVal
            return kProportionalVal * energy;
        case kConstantPlusProportional:
        {//sigma_E = E*kProportionalVal ++ kConstantVal, where ++ is quadratic sum
            const double propV=kProportionalVal * energy;
            const double constV=kConstantVal;
            return sqrt(propV*propV+constV*constV);
        }
        default:
            return 0.;
        }
    }

    //apply smearing, making two checks:
    //1) the energy pre-smearing should be large enough to avoid negative energies; this is particularly relevant for constant smearing
    //2) for safety, post-smearing set the energy to zero if it is negative.
    void applySmearing(double &energy){
        double sigma=getSigma(energy);
        if (energy > kCutVal*sigma){
            energy+=gRandom->Gaus(0.,sigma);
        }
        if (energy<0) energy=0;
    }


};

struct SmearingMC {
    //PMT-based detectors
    SmearingCellMC ECAL[2][12][6];
    SmearingCellMC HCAL[4][6][3];
    SmearingCellMC VETO[3]; //MC handles veto as 3 counters only
    SmearingCellMC SRD[4];



};

//Global var to be used
SmearingMC smearMC;


//functions to init smearing
//Later on we can move this to DB / Calibration params / ...
//Documentation: git issue https://gitlab.cern.ch/P348/p348-daq/-/issues/13
//Slides: https://indico.cern.ch/event/1231090/contributions/5179935/attachments/2566312/4424409/MC%20smearing%20in%20p348-reco.pdf
void Init_Smear_2022B(){

    //A.C. VETO data from h calib runs, looing at central VETO cell.
    smearMC.VETO[1].setConstant(0.6*MeV);//600 keV
    smearMC.VETO[0]=smearMC.VETO[1];
    smearMC.VETO[2]=smearMC.VETO[1];

    //A.C. HCAL data from h calib runs, looking at MIPs in the central cell + hadrons in the total
    for (int iM=0;iM<4;iM++){
        for (int iX=0;iX<6;iX++){
            for (int iY=0;iY<3;iY++){
                if(iM == 0) smearMC.HCAL[iM][iX][iY].setConstantPlusProportional(0.66, 0.0487, 1);
                if(iM > 0) smearMC.HCAL[iM][iX][iY].setConstantPlusProportional(0.55, 0.0487, 1);
            }
        }
    }
    //A.C. ECAL data from h calib runs (MIPS) + e- calib runs. 
    //Work of L. Marsicano. The current resolution model is based on runs 6507-6540.
    double smearPRS[6][6]={ //iX, iY
            0.0679462,0.0346212,0.112978,0.120915,0.130637,0.104252,
            0.0590384,0.0435674,0.0592485,0.0235271,0.113523,0.0499147,
            0.175284,0.092342,0.0288348,0.0686753,0.0459995,0.0556801,
            0.0649371,0.140433,0.0963646,0.0932977,0.127013,0.176526,
            0.105045,0.0685859,0.152378,0.129703,0.076776,0.111668,
            0.,0.,0.,0.,0.,0.};
    double smearECAL[6][6]={
            0.0112563,0.00230075,0.0272729,5.13063e-05,0.0109956,0.0115378,
            0.0248973,0.0497554,0.0293766,0.0135231,0.0550221,7.98965e-05,
            0.00412892,0.0297973,0.0168425,0.00519933,0.00142061,7.72216e-05,
            0.00477331,0.0500511,0.00023775,0.000253945,0.00351778,0.000290025,
            0.000275872,0.000150268,2.01987e-05,0.0018015,0.00124974,7.37535e-05,
            0.,0.,0.,0.,0.,0.};

    for (int iX=0;iX<6;iX++){
            for (int iY=0;iY<6;iY++){
                //PRS: only proportional val
                smearMC.ECAL[0][iX][iY].setProportional(smearPRS[iX][iY]);

                //ECAL: proportional val + 50 meV constant
                smearMC.ECAL[1][iX][iY].setConstantPlusProportional(0.05,smearECAL[iX][iY]);
            }
    }
}

void SmearMC_SRD(RecoEvent &e) {
    return;
}

/*Each VETO counter is read by two PMTs.
 * However, in MC only one energy is reported, copied to both PMT cells.
 * inFile >> e.VETO[0].energy >> e.VETO[2].energy >> e.VETO[4].energy;
 e.VETO[1].energy = e.VETO[0].energy;
 e.VETO[3].energy = e.VETO[2].energy;
 e.VETO[5].energy = e.VETO[4].energy;
 */
void SmearMC_VETO(RecoEvent &e) {
   for (int ii = 0; ii < 3; ii++) {
      smearMC.VETO[ii].applySmearing(e.VETO[2 * ii].energy);
      e.VETO[2 * ii + 1].energy = e.VETO[2 * ii].energy;
    }
    return;
}

void SmearMC_ECAL(RecoEvent &e) {
    for (int iM = 0; iM < 2; iM++) {
        for (int iX = 0; iX < 6; iX++) {
            for (int iY = 0; iY < 6; iY++) { 
                smearMC.ECAL[iM][iX][iY].applySmearing(e.ECAL[iM][iX][iY].energy);
            }
        }
    }
    return;
}

void SmearMC_HCAL(RecoEvent &e) {
    for (int iM = 0; iM < 4; iM++) {
        for (int iX = 0; iX < 6; iX++) {
            for (int iY = 0; iY < 3; iY++) {
                smearMC.HCAL[iM][iX][iY].applySmearing(e.HCAL[iM][iX][iY].energy);
            }
        }
    }
    return;
}





//this code will smear the observables of the input reco event e
void SmearMC(RecoEvent& e){
  if (!e.mc){
    std::cout<<"smearingMC: input event is not MC-type, do nothing"<<std::endl;
    return;
  }

  SmearMC_SRD(e);
  SmearMC_VETO(e);
  SmearMC_ECAL(e);
  SmearMC_HCAL(e);
}


//Given n seeds (n>0), use the first and initialize the random number generator.
//If there are more seeds, use one of the randomly to initialize again the random number generator.
void InitRandom(RecoEvent &e) {
    auto n = e.mc->seeds.size();
    if (n > 0) {
        ULong_t s=(ULong_t)e.mc->seeds[0];
        if (s==0) s=1;
        gRandom->SetSeed(s);

        //A.C. probably useless - I keep this here just to remind myself to think more about this.
        /* if (n > 1) {
            auto u = (n - 1) * (gRandom->Uniform());
            int i = (int)(u)+1;
            gRandom->SetSeed(1+ (ULong_t)e.mc->seeds[i]);
        }*/
    }
}





//geometry definition
void Init_Geometry_MC2017()
{
  Reset_Calib();
  //imported from standard example1
  ECAL_pos.pos = TVector3(     0,   0,     0);
  MAGD_pos.pos = TVector3(     0,   0, -13975.1);
  MAGU_pos.pos = TVector3(     0,   0, -17975.1);
  MAG_field = 1.7;
  MM1_pos.pos  = TVector3(315.,0.,-19375);
  MM2_pos.pos  = TVector3(315.,0.,-18575);
  MM3_pos.pos  = TVector3(0.,0.,-1840);
  MM4_pos.pos  = TVector3(0.,0.,-640);
  //all same angle for simplicity
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM4_pos.Angle = M_PI/4;
  //same pedestal calibration as in 2017
  // timestamp 1506895200 is `2017 Oct 2 0:00 CEST` (last midnight before the end of beam time)
  Init_Calib_2017_MM(1506895200);
  NominalBeamEnergy = 100.;
}

void Init_Geometry_MC2018()
{
  Reset_Calib();
  //imported from standard example1
  ECAL_pos.pos = TVector3(     0,   0,     0);
  MAGU_pos.pos = TVector3(     0,   0, -19530.1);
  //MAGD_pos.pos = TVector3(     0,   0, -15530.1);
  //adjusted down position
  MAGD_pos.pos = TVector3(     0,   0, -16600.1);
  MAG_field = 1.85;
  //position of MM
  MM3_pos.pos  = TVector3(0.,      0., -17593.7);
  MM4_pos.pos  = TVector3(0.,      0., -17226.7);
  MM5_pos.pos  = TVector3(-203.33, 0.,  287.35);
  MM6_pos.pos  = TVector3(-206.667,0.,  387.35);
  //all same angle for simplicity
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM5_pos.Angle = M_PI/4;
  MM6_pos.Angle = M_PI/4;
  //same pedestal calibration as in 2018
  // timestamp 1529445600 is `2018 June 20 0:00 CEST` (last midnight before the end of beam time)
  Init_Calib_2018_MM(1529445600);
  NominalBeamEnergy = 150.;
}

//placeholder for dummy geometry
void Init_Geometry_MC2018_invis()
{
  Reset_Calib();
  //imported from standard example1
  ECAL_pos.pos = TVector3(     0,   0,     0);
  MAGU_pos.pos = TVector3(     0,   0, -19530.1);
  //MAGD_pos.pos = TVector3(     0,   0, -15530.1);
  //adjusted down position
  MAGD_pos.pos = TVector3(     0,   0, -16600.1);
  MAG_field = 1.85;
  //position of MM
  MM1_pos.pos  = TVector3(0.,      0., -16930);
  MM2_pos.pos  = TVector3(0.,      0., -16130);
  MM3_pos.pos  = TVector3(-319.,      0.,  854);
  MM4_pos.pos  = TVector3(-329.,      0.,  1048);
  MM5_pos.pos  = TVector3(-381, 0.,  3309);
  MM6_pos.pos  = TVector3(-397.667,0.,  3521);
  //all same angle for simplicity
  MM1_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM2_pos.Angle = M_PI/4;
  MM3_pos.Angle = M_PI/4;
  MM5_pos.Angle = M_PI/4;
  MM6_pos.Angle = M_PI/4;
  //same pedestal calibration as in 2018
  // timestamp 1529445600 is `2018 June 20 0:00 CEST` (last midnight before the end of beam time)
  Init_Calib_2018_MM(1529445600);
  NominalBeamEnergy = 150.;
}


/* MCRealHit function
 *
 * Originally implemented by E. Depero
 * Adapted by B. Banto
 *
 * Collection of functions for Micromegas and GEM response simulation
 *
 * What this function does:
 *
 * - For multiple energy depositions from the same track, these are merged into one hit per track.
 *
 * - Each track id represents one particle passing through the detector and so the value of the
 * kinetic energy is basically the same for all hits as the energy depositions are small. So the
 * kinetic energy of the first of these hits is taken.
 *
 *  - For the same track id, the merged hit is added or not depending on the energy of the track. This
 * is to avoid saving hits coming from tracks with very little energy (< 22 keV is the current
 * threshold). The problem with taking the sum of the energy deposition instead of the kinetic
 * energy of the track, is that the energy deposition heavily depends on the Geant4 implementation
 * of the trackers. In the case of GEMs, these detectors are very poorly implemented in the
 * simulations, and so it's better to stick to the kinetic energy of the track.
 *
 */
void MCRealHit(vector<MChit>& mchit,const double& EnergyThreshold)
{

  sort(mchit.begin(),mchit.end(),MChitByID);
  vector<MChit> new_hit;

  for(unsigned int i(0);i<mchit.size();)
  {
    // Kinetic energy of particle with trackID == id needs to be above detector sensitivity threshold
    // NOTE: Sum of energy deposit would be better, but would need Geant4 revision of detector
    double kinenergy = mchit[i].energy;
    if(kinenergy < EnergyThreshold) {
      ++i;
      continue;
    }

    int id = mchit[i].id;
    int particle = mchit[i].particle;
    double deposit(0);
    TVector3 pos;
    for (; i < mchit.size(); ++i)
    {
      const bool isSameTrack = (mchit[i].id == id);
      if (!isSameTrack) break;
      deposit += mchit[i].deposit;
      pos += mchit[i].deposit*mchit[i].pos;
    }
    //weighted mean of the hit
    pos *= 1./deposit;
    //merged hit
    new_hit.push_back(MChit (pos,deposit,particle,id,kinenergy));
  }
  //all computation is over, substitute vector with one ordered by energy deposit
  sort(new_hit.begin(),new_hit.end(),MChitByDeposit);
  mchit = new_hit;

}//end MCRealHit





void SimulateMultiplexing(MicromegaPlane& plane,unsigned int strip,const double& signal)
{
  const unsigned int detectedsignal=round(gRandom->Poisson(signal));
  vector<MMStrip>& PlaneStrips = plane.strips;
  const int reverse_index = plane.calib->MM_reverse_map.at(strip);
  const vector<int> strips = plane.calib->MM_map.at(reverse_index);
  for(unsigned int j(0);j<strips.size();++j)
  {
    //limit ADC count to 4095
    if(PlaneStrips[strips[j]].charge + detectedsignal < 4095)
    {
      PlaneStrips[strips[j]].charge += detectedsignal;
    }
    else
    {
      PlaneStrips[strips[j]].charge = 4095;
    }
  }
} ///end SimulateMultiplexing

//method of computation of strip deposit in strip as function of distance from hit point
double ComputeCharge(const double& signal,const double& dist2center,const unsigned int ClusSize)
{
  if( MM_chargespread == STD )
  {
    return signal / pow(2,dist2center+1);
  }
  else if( MM_chargespread == GAUS )
  {
    const double chargespread = ClusSize/4.;
    return signal*TMath::Gaus(dist2center,0,chargespread);
  }
  else
  {
    cout << "WARNING: NO VALID METHOD FOR CHARGE SPREAD OVER STRIPS SELECTED" << endl;
    exit(-1);
  }
}


void SimulateHit(MicromegaPlane& plane,const double center,const unsigned int meansize, const double& signal)
{
  //define size of the cluster
  unsigned int size(0);
  while (size < 2)
  {
    size = gRandom->Poisson(meansize);
  }
  //in case cluster is asymmetric, choose a direction
  int direction(1);
  if(size%2 == 0)
  {
    direction = 1-2*gRandom->Integer(2);
  }
  //substract center
  int strip = round(center);
  if((strip < 0) || (((unsigned int)strip + 1) > plane.strips.size()))
    return;
  double dist2center = fabs(center - strip);
  double stripsignal = ComputeCharge(signal,dist2center,size);
  SimulateMultiplexing(plane,strip,stripsignal);
  unsigned int counter = 1;
  while(counter < size)
  {
    int distance = counter/2 +1;
    strip = round(center+direction*distance);
    dist2center = fabs(center - strip);
    stripsignal = ComputeCharge(signal,dist2center,size);
    if((strip < 0) || (((unsigned int)strip + 1) > plane.strips.size())){++counter;continue;}
    //multiplex the strip
    SimulateMultiplexing(plane,strip,stripsignal);
    ++counter;
    strip = round(center-direction*distance);
    dist2center = fabs(center - strip);
    stripsignal = ComputeCharge(signal,dist2center,size);
    if((strip < 0) || (((unsigned int)strip + 1) > plane.strips.size()) || counter >= (size)){++counter;continue;}
    ++counter;
    SimulateMultiplexing(plane,strip,stripsignal);
  }
}

// For Straws
void MCDoHitAccumulation(Straw& ST,const vector<MChit>& STtruth, const int verbose = 0)
{
  int wire, time;
  for(const auto& hit : STtruth) {
    wire = hit.strawStationWire();
    const uint t0 = ST.calib->t0;
    time = t0 - invRtStraw6(hit.DistWire);
    if(verbose > 0) cout << " hit in channel: " << wire << " and tdc time: " << time << " (true distance = " << hit.DistWire << ")" << endl;

    const StWire wt = {wire, time};
    if (hit.IProj == 0)  ST.XChannels.push_back(wt);
    else                 ST.YChannels.push_back(wt);
  }

} //end MCDoHitAccumulation


// For Micromegas
void MCDoHitAccumulation(Micromega& MM,vector<MChit> MMtruth, const int verbose = 0)
{
  //compute strip hit
  const double angle = MM.geom->Angle;
  const double Kx = cos(angle);
  const double Ky = sin(angle);
  const double xcenter = MM.calib->xcalib.MM_Nstrips/2.0 - 0.5;
  const double ycenter = MM.calib->ycalib.MM_Nstrips/2.0 - 0.5;
  for(unsigned int i(0);i<MMtruth.size();++i)
  {
    double xpos = MMtruth[i].pos.X() - MM.geom->pos.X();
    double ypos = MMtruth[i].pos.Y() - MM.geom->pos.Y();
    const double signal = MMtruth[i].deposit;
    //add resolution and convert to strip size
    static const double StripRes = 1/sqrt(12);
    xpos /= MM.calib->xcalib.MM_strip_step;
    ypos /= MM.calib->ycalib.MM_strip_step;
    xpos = gRandom->Gaus(xpos,StripRes);
    ypos = gRandom->Gaus(ypos,StripRes);
    /*find best strips for the hit and signal 
      invert the function in MMCalculatePos to account 45 degree conversion*/
    // !!! Has to be the inverse of the rotation matrix in mm.h !!!
    const double xstrip = ( Kx*xpos + Ky*ypos + xcenter);
    const double ystrip = (-Ky*xpos + Kx*ypos + ycenter);
    const double signalX = signal*MCsignalCalibrationX;
    const double signalY = signal*MCsignalCalibrationY;
    if(verbose > 0)
      cout << " hit with stripx: " << xstrip << " and stripy: " << ystrip << " and signalX: " << signalX << " and signalY: " << signalY << endl;
    if(xstrip >= 0 && xstrip < MM.calib->xcalib.MM_Nstrips)
    {
      SimulateHit(MM.xplane,xstrip,MCsizeX,signalX);
    }
    if(ystrip >= 0 && ystrip < MM.calib->ycalib.MM_Nstrips)
    {
      SimulateHit(MM.yplane,ystrip,MCsizeY,signalY);
    }
  }
} //end MCDoHitAccumulation

//function to simulate noise
void SimulatePlaneNoise(MicromegaPlane& plane,const vector<double>& sigma)
{
  //loop over channels and add noise
  vector<MMStrip>& PlaneStrips = plane.strips;
  for(unsigned int i(0);i<plane.calib->MM_Nchannels;++i)
  {
    const double noise = gRandom->Gaus(0,sigma[i]);
    const vector<int> strips = plane.calib->MM_map.at(i);
    const double currentsignal = PlaneStrips[strips[0]].charge;
    // If noise is greater than sigma threshold then add it
    if(currentsignal + noise < sigmaLevel*sigma[i]) //sigma used in data
    {
      continue;
    }
    else
    {
      // Add noise to all the strips connected to this channel
      for(unsigned int j(0);j<strips.size();++j)
      {
        PlaneStrips[strips[j]].charge += noise;
      }
    }
  }
}

void SimulateNoise(Micromega& mm)
{
  SimulatePlaneNoise(mm.xplane,mm.calib->xcalib.sigma);
  SimulatePlaneNoise(mm.yplane,mm.calib->ycalib.sigma);
}

void DoSmearing(vector<MChit>& mmhits,const double xres, const double yres)
{
  for(auto& i : mmhits)
    i.pos += TVector3(gRandom->Gaus(0.,xres), gRandom->Gaus(0.,yres), 0.);
}

vector<MChit> DoMerging(vector<MChit> mmhits,const double maxD = MergingDistance)
{
  vector<MChit> mmhitsmerged;
  for(auto p1 = mmhits.begin(); p1 != mmhits.end(); ++p1)
  {
    MChit newhit = *p1;
#if 0
    newhit.pos *= newhit.deposit;
#endif
    for(auto p2 = p1+1; p2 != mmhits.end(); )
    {
      if(abs(newhit.pos.X()-(*p2).pos.X()) < maxD && abs(newhit.pos.Y()-(*p2).pos.Y()) < maxD)
      {
#if 0
        newhit.pos += (*p2).deposit * (*p2).pos;
#endif
        //DISTANCE IS TOO SHORT, DO MERGING
        //TODO: half of distance is taken, use weighted mean instead?
        newhit.pos = 0.5*(newhit.pos + (*p2).pos);
        newhit.deposit = newhit.deposit + (*p2).deposit;
        //TODO: as convention the one with largest energy dominates, should it be deposit instead?
        newhit.energy = newhit.energy > (*p2).energy ? newhit.energy : (*p2).energy;
        newhit.id = newhit.energy > (*p2).energy ? newhit.id : (*p2).id;
        //TODO: Dark photon always take precedence
        if(abs(newhit.particle) == 100)
          newhit.particle = newhit.particle;
        else if(abs((*p2).particle) == 100)
          newhit.particle = (*p2).particle;
        else
          newhit.particle = newhit.energy > (*p2).energy ? newhit.particle : (*p2).particle;
        //erase merged hit
        p2 = mmhits.erase(p2);
      }// merging condition
      else
      {
        ++p2;
      }
    }
#if 0
    if (newhit.deposit > 0.) {
      newhit.pos *= 1./newhit.deposit;
    }
#endif
    mmhitsmerged.push_back(newhit);
  }
  return mmhitsmerged;
} // end do merging function

//CREATE SAME REDUNDANCY OBSERVED IN THE DATA DUE TO THE FAKE HIT MATCHING BETWEEN PLANES
vector<MChit> CreateFakeHits(const vector<MChit>& mmhits)
{
  vector<MChit> mmhitsfake = mmhits;
  for(auto p1 = mmhits.begin(); p1 != mmhits.end(); ++p1)
  {
    MChit newhit;
    for(auto p2 = mmhits.begin(); p2 != mmhits.end(); ++p2)
    {
      //skip true combination
      if(p1 == p2)continue;
      //DISTANCE IS TOO SHORT, DO MERGING
      newhit.pos = (*p1).pos;
      newhit.deposit = 0.5*((*p1).deposit + (*p2).deposit);
      //TODO: as convention the one with largest energy dominates, should it be deposit instead?
      newhit.energy = (*p1).energy > (*p2).energy ? (*p1).energy : (*p2).energy;
      //put Geant ID at -1 to see that it is a fake hits
      newhit.id = -1;
      //TODO: Dark photon always take precedence
      if(abs((*p1).particle) == 100)
        newhit.particle = (*p1).particle;
      else if(abs((*p2).particle) == 100)
        newhit.particle = (*p2).particle;
      else
        newhit.particle = (*p1).energy > (*p2).energy ? (*p1).particle : (*p2).particle;
      mmhitsfake.push_back(newhit);
      /* cout<< "new hit created" << endl;      */
      /* cout << newhit; */
    }
  }
  return mmhitsfake;
} // end do merging function


void DoMCReco (Micromega& mm,vector<MChit>& hits,const MM_calib& calib,const DetGeo& geom)
{
  //eliminate redundant hits
  MCRealHit(hits,MM_threshold);
  //assign calib and geom
  mm.Init(calib, geom);
  //simulate multiplexing and real condition
  MCDoHitAccumulation(mm,hits);
  //simulate noise
  if(simulatenoise)
    SimulateNoise(mm);
  //do the rest of the reconstruction as for data
  mm.DoMMReco();
}

void DoMCRecoGEM(vector<MChit>& hits)
{
  //take only physical hit
  MCRealHit(hits,GEM_threshold);

  //smear the hits
  DoSmearing(hits,GEMxres,GEMyres);

  //MERGED HITS
  hits = DoMerging(hits);

  //create fake hits
  if(ReplicateHits)
  {
    hits = CreateFakeHits(hits);
  }
}

void DoMCRecoStraw(Straw& st,vector<MChit>& hits, const ST_calib& calib, const GEMGeometry& geom)
{
  /* TODO: Eliminate redundant hits from low energy events with energy threshold
  * similar to MCRealHitST(hits, ST_threshold);
  */
  //MCRealHitST(hits, ST_threshold);
  //assign geom and initialize Straw struct
  st.Init(&calib, &geom);
  //accumulate straw hits
  MCDoHitAccumulation(st,hits);
  //do the rest of the reconstruction as for data
  DoStrawReco(st);
}

//DIGITALIZE MC INFORMATION

void DoDigitization(RecoEvent& e)
{

  //do reconstruction in mc for Micromegas
  DoMCReco(e.MM1,e.mc->MM1truth,MM1_calib,MM1_pos);
  DoMCReco(e.MM2,e.mc->MM2truth,MM2_calib,MM2_pos);
  DoMCReco(e.MM3,e.mc->MM3truth,MM3_calib,MM3_pos);
  DoMCReco(e.MM4,e.mc->MM4truth,MM4_calib,MM4_pos);
  DoMCReco(e.MM5,e.mc->MM5truth,MM5_calib,MM5_pos);
  DoMCReco(e.MM6,e.mc->MM6truth,MM6_calib,MM6_pos);
  DoMCReco(e.MM7,e.mc->MM7truth,MM7_calib,MM7_pos);
  DoMCReco(e.MM8,e.mc->MM8truth,MM8_calib,MM8_pos);
  //merge gem hits
  //TODO: Do proper digitization of GEM hits
  DoMCRecoGEM(e.mc->GEM1truth);
  DoMCRecoGEM(e.mc->GEM2truth);
  DoMCRecoGEM(e.mc->GEM3truth);
  DoMCRecoGEM(e.mc->GEM4truth);
  //digitize straw hits in mc
  DoMCRecoStraw(e.ST01, e.mc->ST1truth, ST01_calib, ST01Geometry);
  DoMCRecoStraw(e.ST02, e.mc->ST2truth, ST02_calib, ST02Geometry);
  DoMCRecoStraw(e.ST03, e.mc->ST3truth, ST03_calib, ST03Geometry);
  DoMCRecoStraw(e.ST04, e.mc->ST4truth, ST04_calib, ST04Geometry);

}

void Init_ADC_MC(RecoEvent &e) {
  for (int d = 0; d < 2; d++)
    for (int x = 0; x < 5; x++)
      for (int y = 0; y < 6; y++) {
        e.ECAL[d][x][y].calib = &ECAL_calibration[d][x][y];
      }
  for (int i = 0; i < 4; ++i)
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 3; ++y) {
        e.HCAL[i][x][y].calib = &HCAL_calibration[i][x][y];
      }
  for (int x = 0; x < 4; ++x) {
    e.SRD[x].calib = &SRD_calibration[x];
  }

  for (int i = 0; i < 2; ++i)
    for (int x = 0; x < 4; ++x)
      for (int y = 0; y < 4; ++y) {
        e.VHCAL[i][x][y].calib = &VHCAL_calibration[i][x][y];
      }

  for (int i = 0; i < 6; ++i) {
    e.VETO[i].calib = &VETO_calibration[i];
  }
}





void LoadMCGeometryFromMetaData()
{

  // loop over MC file metadata
  if (mcrunHasOption("ECAL:ECALStart")) {
    ECAL_pos.pos.SetZ(mcrunoption<double>("ECAL:ECALStart"));
  } else {
    std::cout << "ECAL:ECALStart not found, use default" << std::endl;
  }

  if (mcrunHasOption("ECAL:ECALX")) {
    ECAL_pos.pos.SetX(mcrunoption<double>("ECAL:ECALX"));
  } else {
    std::cout << "ECAL:ECALX not found, use default" << std::endl;
  }

  if (mcrunHasOption("ECAL:ECALY")) {
    ECAL_pos.pos.SetY(mcrunoption<double>("ECAL:ECALY"));
  } else {
    std::cout << "ECAL:ECALY not found, use default" << std::endl;
  } 

  if (mcrunHasOption("ECAL:NCellsECALX")) {
    ECAL_pos.Nx = mcrunoption<double>("ECAL:NCellsECALX");
  } else {
    std::cout << "ECAL:NCellsECALX not found, use default" << std::endl;
  }

  if (mcrunHasOption("ECAL:NCellsECAL")) {
    ECAL_pos.Ny = mcrunoption<double>("ECAL:NCellsECAL");
  } else {
    std::cout << "ECAL:NCellsECAL not found, use default" << std::endl;
  }

  if (mcrunHasOption("HCAL:NCellsHCALX")) {
    HCAL_pos.Nx = mcrunoption<double>("HCAL:NCellsHCALX");
  } else {
    std::cout << "HCAL:NCellsHCALX not found, use default" << std::endl;
  }

  if (mcrunHasOption("HCAL:NCellsHCALY")) {
    HCAL_pos.Ny = mcrunoption<double>("HCAL:NCellsHCALY");
  } else {
    std::cout << "HCAL:NCellsHCALY not found, use default" << std::endl;
  }

  if (mcrunHasOption("General:BeamMomentum")) {
    NominalBeamEnergy = mcrunoption<double>("General:BeamMomentum");
  } else {
    std::cout << "General:BeamMomentum not found, use default" << std::endl;
  }

  //A.C. the fabs call is necessary to track both negative-charge and positive-charge particles
  //See: https://gitlab.cern.ch/P348/p348-daq/-/issues/93
  if (mcrunHasOption("Magnet:MagnetMagY")) {
    MAG_field = fabs(mcrunoption<double>("Magnet:MagnetMagY"));
  } else {
    std::cout << "Magnet:MagnetMagY not found, use default" << std::endl;
  }

  if (mcrunHasOption("SRD:ZSandwichBegin")) {
    SRD_pos.pos.SetZ(mcrunoption<double>("SRD:ZSandwichBegin"));
  } else {
    std::cout << "SRD:ZSandwichBegin not found, use default" << std::endl;
  }

  if (mcrunHasOption("BeamNominalEnergy")) {
    NominalBeamEnergy = mcrunoption<double>("BeamNominalEnergy");
  } else {
    std::cout << "BeamNominalEnergy not found, use default" << std::endl;
  }

  if (mcrunHasOption("BeamPDG")) {
    NominalBeamPDG = mcrunoption<double>("BeamPDG");
  } else {
    std::cout << "BeamPDG not found, use default" << std::endl;
  }

  // WCAL_pos is absent on conddb.h for now
  //else if( key == "ECAL:ECAL1Start") WCAL_pos.pos.SetZ(mcrunoption<double>(key));  //ECAL1 is the name of WCAL in the simulation
  //else if( key == "ECAL:ECAL1X") WCAL_pos.pos.SetX(mcrunoption<double>(key)); //ECAL1 is the name of WCAL in the simulation

  //MM part

  /*A.C. to simplify code below.
   in principle would be great to have just DetGeo MM_pos[kNMM],
   but that would require to re-write many things
   */
  const int kNMM = 8;
  DetGeo *MMs_pos[kNMM] = { &MM1_pos, &MM2_pos, &MM3_pos, &MM4_pos, &MM5_pos, &MM6_pos, &MM7_pos, &MM8_pos };

  double d;
  std::string key;
  std::string coord[3] = { "X", "Y", "Z" };
  std::cout << "LoadMCGeometryFromMetaData - set MM" << std::endl;
  for (int ii = 0; ii < kNMM; ii++) {
    std::cout << "MM" << ii + 1 << std::endl;
    for (int jj = 0; jj < 3; jj++) {
      key = Form("MM:MM%i%s", ii + 1, coord[jj].c_str());
      if (mcrunHasOption("Micromegas:MicromegasConstructed")) key = Form("MM:MM%02i%s", ii + 1, coord[jj].c_str());

      if (mcrunHasOption(key)) {
        d = mcrunoption<double>(key);
        MMs_pos[ii]->pos[jj] = d;
        std::cout << "MM" << ii + 1 << " coord " << coord[jj] << "found: "<<d<< std::endl;

      } else {
        std::cerr << "MM" << ii + 1 << " coord " << coord[jj] << "not found" << std::endl;
      }
    }

    key = Form("MM:MM%iAngle", ii + 1);
    if (mcrunHasOption(key)) {
      d = mcrunoption<double>(key);
      MMs_pos[ii]->Angle = d;
      std::cout << "MM" << ii + 1 << " angle found: "<<d<< std::endl;
    }
    else{
      std::cerr << "MM" << ii + 1 << " angle not found." << std::endl;
    }
  }

  const int kNGEM = 4;
  GEMGeometry *GEMs_pos[kNGEM] = { &GM01Geometry, &GM02Geometry, &GM03Geometry, &GM04Geometry};
  std::cout << "LoadMCGeometryFromMetaData - set GEM" << std::endl;
  for (int ii = 0; ii < kNGEM; ii++) {
    std::cout << "GEM" << ii + 1 << std::endl;
    for (int jj = 0; jj < 3; jj++) {
      key = Form("GEM:GEM%02i%s", ii + 1, coord[jj].c_str());
      if (mcrunHasOption(key)) {
        d = mcrunoption<double>(key);
        GEMs_pos[ii]->pos[jj] = d;
        std::cout << "GEM" << ii + 1 << " coord " << coord[jj] << "found: "<<d<< std::endl;

      } else {
        std::cerr << "GEM" << ii + 1 << " coord " << coord[jj] << "not found" << std::endl;
      }
    }

    key = Form("GEM:GEM%iAngle", ii + 1);
    if (mcrunHasOption(key)) {
      d = mcrunoption<double>(key);
      GEMs_pos[ii]->angle = d;
      std::cout << "GEM" << ii + 1 << " angle found: "<<d<< std::endl;
    }
    else{
      std::cerr << "GEM" << ii + 1 << " angle not found." << std::endl;
    }
  }


 //Straw part

  const int kNST = 4;
  GEMGeometry *STs_pos[kNST] = { &ST01Geometry, &ST02Geometry, &ST03Geometry, &ST04Geometry};
  std::cout << "LoadMCGeometryFromMetaData - set ST" << std::endl;
  for (int ii = 0; ii < kNST; ii++) {
    std::cout << "ST" << ii + 1 << std::endl;
    key = Form("Straw%i:1:X", ii + 1);
    if (mcrunHasOption(key)) {
      d = mcrunoption<double>(key);
      key = Form("Straw%i:2:X", ii + 1);
      if (mcrunHasOption(key)) {
        d += mcrunoption<double>(key);
        STs_pos[ii]->pos.SetX(d/2.);
      } else {
        STs_pos[ii]->pos.SetX(d);
      }
      std::cout << "Straw" << ii + 1 << " coord X found: "<<d<< std::endl;
    } else {
      std::cerr << "Straw" << ii + 1 << " coord X not found" << std::endl;
    }
    key = Form("Straw%i:3:Y", ii + 1);
    if (mcrunHasOption(key)) {
      d = mcrunoption<double>(key);
      key = Form("Straw%i:4:Y", ii + 1);
      if (mcrunHasOption(key)) {
        d += mcrunoption<double>(key);
        STs_pos[ii]->pos.SetY(d/2.);
      } else {
        STs_pos[ii]->pos.SetY(d);
      }
      std::cout << "Straw" << ii + 1 << " coord Y found: "<<d<< std::endl;
    } else {
      std::cerr << "Straw" << ii + 1 << " coord Y not found" << std::endl;
    }
    d = 0.;
    for (int jj = 1; jj < 5; jj++) {
      key = Form("Straw%i:%i:Z", ii + 1, jj);
      if (mcrunHasOption(key)) {
        d += mcrunoption<double>(key);
      }
    }
    if (d != 0) {
      STs_pos[ii]->pos.SetZ(d/4.);
      std::cout << "Straw" << ii + 1 << " coord Z found: "<<d<< std::endl;
    } else {
      std::cerr << "Straw" << ii + 1 << " coord Z not found" << std::endl;
    }
    STs_pos[ii]->pitch = 6.145;
    STs_pos[ii]->size = 64;
  }

  // Check if parameters for magnet exists
  try {
    MAGU_pos.pos.SetZ(mcrunoption<double>("Magnet:Magnet1Start"));
    MAGD_pos.pos.SetZ(mcrunoption<double>("Magnet:Magnet2End"));
  }
  // Take older versions for compatibility
  catch (std::runtime_error& exc) {
    std::cerr << exc.what() << std::endl;
    try {
    MAGU_pos.pos.SetZ(mcrunoption<double>("Magnet:Membrane0Z"));
    MAGD_pos.pos.SetZ(mcrunoption<double>("Magnet:Membrane1Z"));
    std::cerr << "Warning: Using old format! Using Magnet:Membrane0Z and Magnet:Membrane1Z instead of Magnet:Magnet1Start and Magnet:Magnet2End." << std::endl;
    } catch (std::runtime_error& exc2) {
      std::cerr << exc2.what() << std::endl;
      std::cerr << "Warning: No Magnet - using defaults!" << std::endl;
    }
  }
}


//this part fo the code handles proper loading of MC constants
void InitMC() {

  int setupNumber, geometryVersion;
  std::string geofile;
  Reset_Calib();
  try {
    setupNumber = mcrunoption<int>("SetupNumber");
    geometryVersion = mcrunoption<int>("General:GeometryVersion");
    std::cout << "Setup-dependent MC parameters, setupNumber: " << setupNumber << " geoVersion: " << geometryVersion << std::endl;

  } catch (std::runtime_error &exc) {
    std::cerr << exc.what() << std::endl;
    std::cerr << "MC: no SetupNumber and/or no General:GeometryVersion. Can't load setup-dependent MC parameters" << std::endl;
    return;
  }

  // Set sorting methods for MM clusters and hits
  MM_cluster_sort = kSort_ByCharge;
  MM_hit_sort = kSort_ByCharge;

  // => session 2018
  const bool is2018 = ((setupNumber == 1 || setupNumber == 2) && geometryVersion == 0);

  // => session 2021A, runs range 4642 - 5187
  const bool is2021A = ((setupNumber == 1 || setupNumber == 2) && geometryVersion == 1);

  // => session 2022B (electron), runs 6035 - 8458
  const bool is2022B = ((setupNumber == 1 || setupNumber == 2) && geometryVersion == 2);

  // => session 2023A (electron), runs 8459 - 9717
  const bool is2023A = ((setupNumber == 1 || setupNumber == 2) && geometryVersion == 3);

  // => session 2024A (electron), runs 10316 - 19999
  const bool is2024A = ((setupNumber == 1 || setupNumber == 2) && geometryVersion == 4);

  //Load run-specific configuration, including a geometry default

  double ECALCellSize = mcrunoption<double>("ECAL:ECALCellSize", 0.);
  //double ECALDepth = mcrunoption<double>("ECAL:ECALEnd", 0.) - mcrunoption<double>("ECAL:ECALStart", 0.);
  if (is2018) { //for 2018,
    std::cout << "MC: 2018-electron" << std::endl;
    //LoadMCGeometryFromMetaData();       // Meta Data is not completely available for 2018 simulations
    Init_Geometry_MC2018_invis();
    Init_Calib_2018_MM(1529445600); // timestamp 1529445600 is `2018 June 20 0:00 CEST` (last midnight before the end of beam time)
    geofile = conddbpath() + "/trackingtools/2018elec/geometry_mc.gdml";
    ECAL0BEAMCELL={3,3}; 
    ECAL0BEAMSPOT_pos = CCellECAL(ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, ECALCellSize); //MK
  } else if (is2021A) {
    std::cout << "MC: 2021-electron" << std::endl;
    LoadMCGeometryFromMetaData();
    Init_Calib_2021_MM(1630922400);    // 1630922400 is "Mon Sep 06 2021 12:00:00 CEST"
    Init_Calib_2022B_ST(6501);
    geofile = conddbpath() + "/trackingtools/2021elec/geometry_mc.gdml";
    ECAL0BEAMCELL={2,2};
    ECAL0BEAMSPOT_pos = CCellECAL(ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, ECALCellSize); //MK
  } else if (is2022B) {
    std::cout << "MC: 2022-electron" << std::endl;
    LoadMCGeometryFromMetaData();
    Init_Calib_2022B_MM(1664928923, 8458);    // 1664928923 is "Wed Oct 05 2022 02:15:23 CEST", the 8458 is to load the proper MM2 design data
    Init_Calib_2022B_ST(6501);
    geofile = conddbpath() + "/trackingtools/2022elec/geometry_mc.gdml";
    ECAL0BEAMCELL={2,3};
    //ECAL0BEAMSPOT_pos = TVector3(-355, 0, 0); //P.Bisio: set by hand looking on 2022B MC beam spot 
    ECAL0BEAMSPOT_pos = CCellECAL(ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, ECALCellSize); //MK
#ifdef TRACKINGTOOLS
    InitTrackingMC(geofile);
#endif
  } else if (is2023A) {
    std::cout << "MC: 2023-electron" << std::endl;
    LoadMCGeometryFromMetaData();
    Init_Calib_2023A_MM(1686157200);    // 1686157200 is "Wed Jun 07 2023 19:00:00 GMT+02", time after survey measurement
    Init_Calib_2023A_ST(9022);
    geofile = conddbpath() + "/trackingtools/2023elec/geometry_mc.gdml";
    ECAL0BEAMCELL={2,2};
    ECAL0BEAMSPOT_pos = CCellECAL(ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, ECALCellSize); //MK
#ifdef TRACKINGTOOLS
    InitTrackingMC(geofile);
#endif
  } else if (is2024A) {
    std::cout << "MC: 2024-electron" << std::endl;
    LoadMCGeometryFromMetaData();
    Init_Calib_2024A_MM(1716292696, 10920);    // 1716292696 is "i 21 Mai 2024 13:58:16 CEST", time just before the survey measurement
    Init_Calib_2024A_ST();
    // TODO: Set this to proper geometry once survey measurements are available
    geofile = conddbpath() + "/trackingtools/2024elec/empty_geom.gdml";
    ECAL0BEAMCELL={2,2};
    ECAL0BEAMSPOT_pos = CCellECAL(ECAL0BEAMCELL.ix, ECAL0BEAMCELL.iy, ECALCellSize); //MK
#ifdef TRACKINGTOOLS
    InitTrackingMC(geofile);
#endif

  } else {
    std::cerr <<" InitMC(): SetupNumber: "<<setupNumber<<" GeometryVersion: "<<geometryVersion<<" notRecognized "<<std::endl;
    return;
  }

  // TODO: Set proper ECAL0BEAMSPOT_pos (coordinates of the 'beam' cell center) for all setups cases,
  //       this value is neccessary for proper calculation of the shower chi2.
  //       The possible solution is to calculate value automaticaly based on
  //          ECAL_pos, ECAL0BEAMCELL, and single cell size
  //          https://gitlab.cern.ch/P348/p348-daq/-/issues/52#note_6580473
  // MK 03.08.2023: Added function to setup ECAL0BEAMSPOT_pos, for the moment only for MC

    //Load smearing parameters. I keep this separate from the code before, since this is only for smearing, before it was for geo.
    if (is2018) { //for 2018,
        std::cout << "MC smearing parameters: 2018-electron" << std::endl;
        std::cout << " NOT IMPLEMENTED " << std::endl;
    } else if (is2021A) {
        std::cout << "MC smearing parameters: 2021-electron" << std::endl;
        std::cout << " NOT IMPLEMENTED " << std::endl;
    } else if (is2022B) {
        std::cout << "MC smearing parameters: 2022-electron" << std::endl;
        Init_Smear_2022B();
    } else if (is2023A) {
        std::cout << "MC smearing parameters: 2023-electron" << std::endl;
        std::cout << " NOT IMPLEMENTED " << std::endl;
        //Init_Smear_2022B();
    }


}

/*
 * The following function takes care of:
 * - Reading MC event from file
 * - Initializing MC parameters
 * - Do digitization for tracking, if requested
 * - Applying, if requested, smearing on ECAL, HCAL, VETO, SRD
 */

RecoEvent RunP348RecoMC(std::ifstream &inFile, bool doDigi = 1, bool doSmear = 0) {
  RecoEvent e = RunP348ReadMC(inFile);

  static bool isFirst = true;
  if (e.mc) {
    if (isFirst) {
      isFirst = false;
      InitMC();
    }
    //Use the event random seeds to initialize the random number generator
    Init_ADC_MC(e);
    InitRandom(e);
    if (doDigi) {
      DoDigitization(e);
    }
    if (doSmear) {
      SmearMC(e);
    }
  }
  return e;
}

