// GenFit
#include <Exception.h>
#include <FieldManager.h>
#include <KalmanFitterRefTrack.h>
#include <DAF.h>
#include <StateOnPlane.h>
#include <Track.h>
#include <MaterialEffects.h>
#include <RKTrackRep.h>
#include <AbsTrackRep.h>
#include <TGeoMaterialInterface.h>
#include <EventDisplay.h>
#include "mySpacepointDetectorHit.h"
#include "mySpacepointMeasurement.h"

#include "ConstFieldBox.h"
#include "ConstFieldBox.cc"

// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TCanvas.h"
#include "TFile.h"
#include "TStyle.h"
#include <TH1.h>
#include "TH2.h"
#include <TVector3.h>
#include <TGeoManager.h>
#include <TClass.h>

// c++
#include <vector>
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "timecut.h"
#include "tracking.h"

// print all global plots to pdf file
void print_global_plots(const TString fname = "plots.pdf")
{
  TCanvas c;
  
  // open output file
  c.Print(fname + "[");
  
  TIter next(gDirectory->GetList());
  while (TObject* o = next()) {
    //cout << "gdir obj = " << o->GetName() << endl;
    c.Clear();
    o->Draw();
    c.Print(fname);
  }
  
  // close output file
  c.Print(fname + "]");
}

int main(int argc, char* argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./tracking.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  // setup input data files list
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i) manager.AddDataSource(argv[i]);
  manager.Print();
  
  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");
  
  // book histograms
  TFile file_histo("tracking.root", "RECREATE");
  
  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  TH2I ehTplot("ehTplot", "ECAL vs HCAL (in-time);ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 150, 0, 150);
  
  TH2I mm1("mm1", "Beam Profile on MM1;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm2("mm2", "Beam Profile on MM2;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm3("mm3", "Beam Profile on MM3;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  TH2I mm4("mm4", "Beam Profile on MM4;x, mm;y, mm", 1000, -100, 100, 1000, -100, 100);
  
  TH1I mom_simple("mom_simple", "Reconstructed momentum (simple); Momentum, GeV/c;#nevents", 300, 0, 150);
  TH1I mom_genfit("mom_genfit", "Reconstructed momentum (GenFit); Momentum, GeV/c;#nevents", 300, 0, 150);
  TH2I mom_simple_genfit("mom_simple_genfit", "Reconstructed momentum (GenFit vs. simple);Momentum (simple), GeV/c;Momentum (GenFit), GeV/c", 150, 0, 150, 150, 0, 150);
  
  TH1I genfit_chi2ndf("genfit_chi2ndf", "GenFit Chi^2/Ndf;Chi^2/Ndf;nevents", 1000, 0, 2000);
  TH2I genfit_mom_vs_chi2ndf("genfit_mom_vs_chi2ndf", "GenFit Momentum vs Chi^2/Ndf;Chi^2/Ndf;Momentum, GeV/c", 1000, 0, 2000, 300, 0, 150);
  
  TH2I EHCAL_mom_simple("EHCAL_mom_simple", "Momentum (simple) vs ECAL+HCAL;ECAL+HCAL, GeV;Momentum, GeV/c", 150, 0, 150, 150, 0, 150);
  TH2I EHCAL_mom_genfit("EHCAL_mom_genfit", "Momentum (GenFit) vs ECAL+HCAL;ECAL+HCAL, GeV;Momentum, GeV/c", 150, 0, 150, 150, 0, 150);
  
  ehplot.SetOption("COL");
  ehTplot.SetOption("COL");
  mm1.SetOption("COL");
  mm2.SetOption("COL");
  mm3.SetOption("COL");
  mm4.SetOption("COL");
  mom_simple_genfit.SetOption("COL");
  EHCAL_mom_simple.SetOption("COL");
  EHCAL_mom_genfit.SetOption("COL");
  
  // setup GenFit tracking
  
  // init event display
  genfit::EventDisplay* display = 0;
  //genfit::EventDisplay* display = genfit::EventDisplay::getInstance();
  // EventDisplay crash on machines with Intel graphics, workaround:
  //   $ export LIBGL_ALWAYS_INDIRECT=1
  //   $ ./tracking.exe file.dat
  // Reference:
  //   https://root.cern.ch/phpBB3/viewtopic.php?t=21274
  
  // set MAGU_pos, MAGD_pos for magnetic field setup
  // TODO: this is valid only for 2016B
  Init_Geometry_2016B();
  
  const double mm2cm = 0.1;
  
  // init geometry and mag. field
  TGeoManager::Import((conddbpath() + "NA64Geom2016B.root").c_str());
  
  genfit::ConstFieldBox field(0, 21.0, 0, -100, 100, -100, 100, mm2cm*min(MAGU_pos.pos.Z(), MAGD_pos.pos.Z()), mm2cm*max(MAGU_pos.pos.Z(), MAGD_pos.pos.Z())); // kGauss
  genfit::FieldManager::getInstance()->init(&field);
  genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface);
  
  // init fitter
  genfit::AbsKalmanFitter* fitter = new genfit::KalmanFitterRefTrack();
  //genfit::AbsKalmanFitter* fitter = new genfit::DAF();
  fitter->setMinIterations(5);
  fitter->setMaxIterations(8);
  cout << "Fitter:"
       << " name = " << fitter->IsA()->GetName()
       << " min iterations = " << fitter->getMinIterations()
       << " max iterations = " << fitter->getMaxIterations()
       << endl;
  
  // init the measurement factory
  const int myDetId = 1;
  typedef genfit::MeasurementProducer<genfit::mySpacepointDetectorHit, genfit::mySpacepointMeasurement> MyMeasurementProducer;
  TClonesArray detHits("genfit::mySpacepointDetectorHit");
  genfit::MeasurementFactory<genfit::AbsMeasurement> factory;
  factory.addProducer(myDetId, new MyMeasurementProducer(&detHits));
  
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // print progress
    if (nevt % 1000 == 1)
      cout << "===> Event #" << nevt << endl;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // check trigger type
    if (!e.isPhysics) continue;
    
    const double ecal = e.ecalTotalEnergy();
    const double hcal = e.hcalTotalEnergy();
    
    ehplot.Fill(ecal,hcal);
    
    RecoEvent eT = e;
    timecut(eT, 4);
    const double ecalT = eT.ecalTotalEnergy();
    const double hcalT = eT.hcalTotalEnergy();
    
    ehTplot.Fill(ecalT, hcalT);
    
    if (e.MM1.hasHit()) mm1.Fill(e.MM1.xpos, e.MM1.ypos);
    if (e.MM2.hasHit()) mm2.Fill(e.MM2.xpos, e.MM2.ypos);
    if (e.MM3.hasHit()) mm3.Fill(e.MM3.xpos, e.MM3.ypos);
    if (e.MM4.hasHit()) mm4.Fill(e.MM4.xpos, e.MM4.ypos);
    
    // simple track reconstruction
    if (e.MM1.hasHit() && e.MM2.hasHit() && e.MM3.hasHit() && e.MM4.hasHit()) {
       // TODO: definition is valid only for 2016B
       const TVector3 mm1_hit = e.MM1.pos() + MM1_pos.pos;
       const TVector3 mm2_hit = e.MM2.pos() + MM2_pos.pos;
       const TVector3 mm3_hit = e.MM3.pos() + MM3_pos.pos;
       const TVector3 mm4_hit = e.MM4.pos() + MM4_pos.pos;
       const double Bfield = MAG_field;
       const double mag_l = fabs(MAGU_pos.pos.Z() - MAGD_pos.pos.Z());
       
       const double mom1 = simple_tracking(mm1_hit, mm2_hit, mm3_hit, mm4_hit, mag_l, Bfield);
       mom_simple.Fill(mom1);
      
      
      // GenFit track reconstruction
      const double detectorResolution = 0.02;
      const int nMeasurements = 4;
      const int pdg = 11;
      
      // setup detector hits
      TMatrixDSym cov(3);
      for (int i = 0; i < 3; ++i) cov(i,i) = detectorResolution*detectorResolution;
      
      detHits.Clear("C");
      new(detHits[0]) genfit::mySpacepointDetectorHit(mm2cm * mm1_hit, cov);
      new(detHits[1]) genfit::mySpacepointDetectorHit(mm2cm * mm2_hit, cov);
      new(detHits[2]) genfit::mySpacepointDetectorHit(mm2cm * mm3_hit, cov);
      new(detHits[3]) genfit::mySpacepointDetectorHit(mm2cm * mm4_hit, cov);
      
      // setup track seed state
      TVector3 momM(0, 0, -10); // GeV/c
      TMatrixDSym covM(6);
      for (int i = 0; i < 3; ++i) covM(i,i) = detectorResolution*detectorResolution;
      for (int i = 3; i < 6; ++i) covM(i,i) = pow(detectorResolution / nMeasurements / sqrt(3), 2);
      
      // setup candidate (hits + seed)
      genfit::TrackCand cand;
      cand.addHit(myDetId, 0);
      cand.addHit(myDetId, 1);
      cand.addHit(myDetId, 2);
      cand.addHit(myDetId, 3);
      cand.setPosMomSeedAndPdgCode(mm2cm * mm1_hit, momM, pdg);
      cand.setCovSeed(covM);
      
      double mom2 = 0;
      try {
      // create track
      genfit::AbsTrackRep* rep = new genfit::RKTrackRep(pdg);
      genfit::Track track(cand, factory, rep);
      fitter->processTrack(&track);
      track.checkConsistency();
      
      const genfit::StateOnPlane state = track.getFittedState();
      mom2 = track.getCardinalRep()->getMomMag(state);
      
      const genfit::FitStatus* fit = track.getFitStatus();
      const double chi2ndf = fit->getChi2() / fit->getNdf();
      
      /*
      cout << "GenFit track"
           << " momentum = " << mom2
           << " fit converged = " << fit->isFitConverged()
           << " fitted charge = " << fit->getCharge()
           << " fit Chi^2 = " << fit->getChi2()
           << " fit Ndf = " << fit->getNdf()
           << " fit Chi^2/Ndf = " << chi2ndf
           << endl;
      */
      
      mom_genfit.Fill(mom2);
      genfit_chi2ndf.Fill(chi2ndf);
      genfit_mom_vs_chi2ndf.Fill(chi2ndf, mom2);
      
      EHCAL_mom_simple.Fill(ecal+hcal, mom1);
      EHCAL_mom_genfit.Fill(ecal+hcal, mom2);
      mom_simple_genfit.Fill(mom1, mom2);
      
      if (display) display->addEvent(&track);
      }
      catch (genfit::Exception& e) {
        std::cerr << e.what();
        std::cerr << "Exception, next track" << std::endl;
        continue;
      }
      
    /*
    cout << "run = " << e.run
         << " spill = " << e.spill
         << " event = " << e.spillevent
         << " ecal = " << ecal
         << " hcal = " << hcal
         << " mom_simple = " << mom1
         << " mom_genfit = " << mom2
         << endl;
    */
    }
    
    //if (nevt > 10000) break;
  }
  
  file_histo.Write();
  
  // open event display
  if (display) display->open();
  
  print_global_plots("tracking.pdf");
  
  return 0;
}
