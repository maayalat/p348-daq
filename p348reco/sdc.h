#pragma once

#include <sdc.hh>

//                                                   _________________________
// ________________________________________________/ NA64-specific definitions

//
// CALIBRATION INFO DATA STRUCTURES

namespace p348 {

/// Run number type
//typedef size_t RunNo_t;
struct RunNo_t {
    /// Actual value of the run number
    size_t v;
    /// Returns true if both run number match
    bool operator==(const RunNo_t & b) const { return v == b.v; }
    /// (trivial) less comparison
    bool operator<(const RunNo_t & b) const { return v < b.v; }
};

/// Standard NA64 SADC calibration entry
struct CaloCellCalib {
    std::string name;  ///< Name of the station + Z/station No
    int z;  ///< z-index/station no. of the entity (cell)
    int x;  ///< x-index of the entity (cell)
    int y;  ///< y-index of the entity (cell)
  
    float time;  ///< timing calibration of the entity (cell)
    float timesigma;  ///< sigma of the timing calibration of the entity (cell)
    bool have_time;  ///< `true` when time calibration is available

    double factor;  ///< multiplication factor for peak position of the entity (cell)
    double peakpos;  ///< peak position of the entity (cell)

    bool is_relative;  ///< `time` is relative to master?
    
    bool have_ledref;
    float ledref;      ///< reference amplitude of LED pulse
    // ... (other fields; NB: append overriding code at `load_calib()` below
    //      whenever new fields are introduced here)
};

//
// NA64 UTILITY TYPES AND FUNCTIONS

/**\brief returns a pair of detector kin name and station number by joint str
 *
 * For instance, "ECAL0" input will yield ("ECAL", 0) pair.
 * */
inline std::pair<std::string, int> split_name(const std::string & nm) {
    // extract detector kin from its name
    size_t n;
    for( n = 0; n < nm.size() && ! isdigit(nm[n]); ++n ) {;}
    if( !n ) {
        // note, that event though file is not specified here in exception
        // constructor, it will be appended to exception body higher on
        // stack
        throw sdc::errors::ParserError( "Detector name expected in first column"
                , nm );
    }
    const std::string kin = nm.substr(0, n);
    if( n != nm.size() ) {
        // remaining suffix is "z"
        const std::string suffix = nm.substr(n);
        return {kin, sdc::aux::lexical_cast<int>(suffix)};
    }
    return {kin, 0};
}

sdc::Documents<RunNo_t> instantiate_default_docs(const std::string & rootpath);

}  // namespace p348

//
// Traits

namespace sdc {

/**\brief Validity traits specialization for `RunNo_t` key type */
template<>
struct ValidityTraits<p348::RunNo_t> {
    static constexpr p348::RunNo_t unset = {0};
    static constexpr char strRangeDelimiter = '-';
    static inline bool is_set( p348::RunNo_t id)
        { return unset.v != id.v; }
    static inline bool less( p348::RunNo_t a
                           , p348::RunNo_t b)
        { return a.v < b.v; }
    static inline std::string to_string(p348::RunNo_t rn)
        { return std::to_string(rn.v); }
    static inline void advance(p348::RunNo_t & rn) { ++rn.v; }
    static inline p348::RunNo_t from_string(const std::string & stre) {
        return {aux::lexical_cast<size_t>(stre)};
    }
    using Less = std::less<p348::RunNo_t>;
};

/** Traits implementing parser logic for `CaloCellCalib` struct
 *
 * This is the data structure from the original Anton's specification. It
 * expects lines provided within a special format:
 * 
 *   <name:str><z-index:int> <x:int> <y:int> ...
 *
 * Special treatment to be applied to interpret first three columns:
 *
 *      HCAL3 1 2 => name="HCAL3" z=3, x=1, y=2
 *
 * meaning that station number goes into data structure as "z-index". The rest
 * of line columns:
 *  - `<Amp 100 GeV:float> <sigma 100 GeV:float>` -- mean and sigma values for
 *      electron calibration peak
 *  - `<Amp 100 GeV:float> <sigma 100 GeV:float>` -- mean and sigma values for
 *      muon calibration peak
 *  - `<run:int>` -- calibration run number
 * 
 * Metadata used (besides of `runs` and `type`:
 *  - `factor.<name>` -- a special factor $f$ used to calculate `K`. The result
 *      is $K=f/peakpos$ where $peakpos$ is taken from column
 *  - `columns` -- denotes meaning of columns (like "name,x,y,peakpos")
 *
 */
template<>
struct CalibDataTraits<p348::CaloCellCalib> {
    /// Type name alias
    static constexpr auto typeName = "p348reco/CaloCellCalib";
    /// A collection type of the parsed entries
    template<typename T=p348::CaloCellCalib>
        using Collection=std::list<T>;
    //typedef std::list<p348::CaloCellCalib> Collection;
    /// An action performed to put newly parsed data into collection
    template<typename T=p348::CaloCellCalib>
    static inline void collect( Collection<T> & col
                              , const T & e
                              , const aux::MetaInfo &
                              , size_t
                              ) { col.push_back(e); }

    /// This way one could define the data type name right in the structure
    /// defining a parser for data line. However, p348reco forbids
    /// doing this because of "header-only" politics that doesn't allow
    /// to define static data members for reentrant usage.
    //static constexpr auto typeName = "CaloCell";

    /// Parses the CSV line into `CaloCellCalib` data object
    ///
    /// Parsing can be affected by the parser state to handle changes within
    /// a file. Namely, metadata "columns=name1,name2..." may denote number of
    /// token with certain semantics. By default, "name,x,y,peakpos" is assumed
    ///
    /// The `lineNo` argument can be used for diagnostic purposes (e.g.
    /// to report a format error).
    static p348 ::CaloCellCalib
            parse_line( const std::string & line
                      , size_t lineNo
                      , const aux::MetaInfo & m
                      , const std::string & filename
                      , sdc::aux::LoadLog * loadLogPtr=nullptr
                      ) {
        // create object and set everything to zero
        p348::CaloCellCalib obj;
        obj.have_time = false;
        obj.have_ledref = false;
        obj.is_relative = m.get<bool>("timeIsRelativeToMaster", false, lineNo);
        // Create and validate columns order
        auto csv = m.get<aux::ColumnsOrder>("columns", lineNo)
                //.validate({"name", "x", "y", "peakpos", "time", "timesigma"})  // TODO: policies
                .interpret(aux::tokenize(line), loadLogPtr);
        // ^^^ note: every CSV block must be prefixed by columns order,
        // according to Anton's specification

        // Get required columns
        obj.name = csv("name");
        assert(!obj.name.empty());
        obj.x = csv("x", std::numeric_limits<decltype(p348::CaloCellCalib::x)>::max());
        obj.y = csv("y", std::numeric_limits<decltype(p348::CaloCellCalib::y)>::max());
        // get name from line, extract z-index and station kin
        auto kinNoPair = p348::split_name(obj.name);
        obj.z = kinNoPair.second;

        // apply arithmetics to peakpos, if some special fields are defined in the
        // metadata for current line. We try first the particular name+Z
        // (e.g. "factor.ECAL0"), otherwise kin (like "factor.WCAL")
        obj.peakpos = csv("peakpos", std::nan("0"));
        // ^^^ TODO: Anton asked K=1 by default, however it is not the same
        //     as `peakpos'. Clarify it.
        obj.factor = csv("factor", std::nan("0"));  // try to retrieve factor from the CSV
        #if 0  // TODO: experimental behaviour, test and cleanup
        if(std::isnan(obj.factor)) {  // factor is not given as the column, look in MD
            // check, if the metadata entry was deined prior to current line
            obj.factor = m.get<double>("factor." + obj.name, std::nan("0"), lineNo );
            if( std::isnan(obj.factor) )
                obj.factor = m.get<float>("factor." + kinNoPair.first, std::nan("0"), lineNo );
        }
        #else
        // if either `factor`, or `peakpos` were given in CSV block
        // For description, see: https://gitlab.cern.ch/P348/p348-daq/-/issues/110
        if(std::isfinite(obj.peakpos) || std::isfinite(obj.factor)) {
            // if factor is not set explicitly, take it from MD
            if(!std::isfinite(obj.factor)) {
                obj.factor = m.get<double>("factor." + obj.name, std::nan("0"), lineNo );
                if( std::isnan(obj.factor) )
                    obj.factor = m.get<float>("factor." + kinNoPair.first, std::nan("0"), lineNo );
                // if load log is provided, push its effective value
                if(loadLogPtr) {
                    char tmpbf[32];
                    snprintf(tmpbf, sizeof(tmpbf), "%.4e", obj.factor);
                    loadLogPtr->add_entry("factor-in-use", tmpbf);
                }
            }
            // if retrieval failed, do
            if(!std::isfinite(obj.factor)) {
                char errbf[512];
                snprintf(errbf, sizeof(errbf), "Error at %s:%zu: `peakpos'"
                        " column is given, but no explicit `factor' is"
                        " specified, nor `factor.*=...' metadata item was"
                        " provided before.", filename.c_str(), lineNo);
                throw std::runtime_error(errbf);
            }
        } 
        #ifndef NDEBUG
        // NOTE: otherwise both fields should be set to NaN/not finite values
        // (yes, global `factor.*=...` ignored)
        else {
            assert(!std::isfinite(obj.peakpos));
            assert(!std::isfinite(obj.factor));
        }
        #endif
        #endif

        // Get other optional columns
        obj.time = csv("time", std::nan("0"));
        obj.timesigma = csv("timesigma", std::nan("0"));
        //if( std::isnan(obj.time) ^ std::isnan(obj.sigma) )
        //    throw errors::DataError( "Time/sigma must be both given or none." );
        if( !std::isnan(obj.time) )
            obj.have_time = true;
        
        obj.ledref = csv("ledref", std::nan("0"));
        if( !std::isnan(obj.ledref) )
            obj.have_ledref = true;
        // ... other columns

        return obj;
    }
};  // struct CalibDataTraits<::CaloCellCalib>

}  // namespace sdc

namespace p348 {

///\brief Function requested by initial specification: straightforward load of
///       calorimeter calibration data
///
/// Highly specialized one. Loads list of `CaloCellCalib` structures for
/// certain run using the path provided here.
std::vector<CaloCellCalib> load_calib(std::string rootpath, int run);

}

using p348::load_calib;

