#!/bin/bash -e

# === functions ===

function batch_list_output_files() {
  local batchid=$1
  
  find -L $batchid/ -path '*/job*result/*' | cut -d / -f 3 | sort -u | grep -vE "LSFJOB|list.txt"
}

function batch_print_output_files() {
  local batchid=$1
  
  local uniqlist=$(batch_list_output_files $batchid)
  local nuniqlist=$(echo "$uniqlist" | wc -w)
  
  if (( $nuniqlist > 10 )) ; then
    echo $(echo "$uniqlist" | head -n 3 | xargs) " ... ($nuniqlist total files)"
  else
    echo $uniqlist
  fi
}

# === main ===

# print usage info
if (( $# < 1 )) ; then
  echo "Usage:"
  echo ""
  echo " Submit jobs:"
  echo "  ./batch.sh submit {prog} {list} {duration} {njobs} [EOSpath]"
  echo "      {prog}   - name of the analysis program executable"
  echo "                   It is possible to specify additional files with the following format of {prog} parameter:"
  echo "                     \"prog file1 file2 ...\""
  echo "                   All additional files will be copied to execution host"
  echo "      {list}   - path to file with list of data files to process"
  echo "      {duration}  - batch job max runtime duration, example: 10 (10 seconds), 5nm (5 minutes), 8nh (8 hours), 3nd (3 days)"
  echo "      {njobs}  - number of jobs or keyword 'byRun' or keyword 'byRunSplit=XXX'"
  echo "                   'byRun' mode splits input {list} to process all files of each run by a single job,"
  echo "                   'byRunSplit=XXX' mode splits input {list} to process XXX files of each run by a single job,"
  echo "                   {list} data file names are expected to be *-NNNNNN.dat for run number NNNNNN extraction"
  echo "      [EOSpath]    - optionally, a path in the EOS filesystem where the output files will be stored. "
  echo "                     the user should have R/W permit to that folder. "
  echo ""
  echo "    Environment variables:"
  echo "      EOS_MGM_URL  - the instance path for eos r/w operations, necessary in case of [EOSpath] option"
  echo "      CONDDB       - the explicit path to the calibration data, to avoid the packaging of p348reco/conddb into the job archive file"
  echo ""
  echo " List batches:"
  echo "  ./batch.sh list"
  echo ""
  echo " Merge results:"
  echo "  ./batch.sh merge {batchid} {fname}"
  echo "      {batchid} - BatchId (see 'list' command output)"
  echo "      {fname}   - file name of job output file (supported .dat and .root files)"
  echo ""
  echo "  ./batch.sh mergeall {batchid}"
  echo "      {batchid} - BatchId (see 'list' command output)"
  echo "                  (all output files will be merged)"
  echo ""
  echo " Collect results:"
  echo "  ./batch.sh collect {batchid} {fname}"
  echo "      {batchid} - BatchId (see 'list' command output)"
  echo "      {fname}   - file name of job output file (supported .dat and .root files)"
  echo ""
  
  exit 1
fi

# run mode
mode="$1"

case "$mode" in
  "submit" )
    
    if (( $# != 5 && $# != 6 ))  ; then
      echo "ERROR: incorrect parameters (submit)"
      exit 1
    fi
    
    jobfiles="$2"
    list="$3"
    durationcli="$4"
    njobs="$5"
    EOSpath="$6"
    
    # translate to seconds
    duration=$durationcli
    duration=${duration/nm/*60}
    duration=${duration/nh/*60*60}
    duration=${duration/nd/*60*60*24}
    duration=$((duration))
    
    # extract the executable file name (first item)
    prog=$(echo "$jobfiles" | awk '{print $1}')
    
    if [[ "$CONDDB" == "" ]] ; then
      # no `CONDDB` env. var., add the "conddb/" files to the job archive
      jobfiles="$jobfiles conddb"
    else
      if [[ ! -d "$CONDDB" ]] ; then
        echo "ERROR: incorrect directory path CONDDB=$CONDDB"
        exit 1
      fi
      # upgrade for the full path:
      export CONDDB=$(cd $CONDDB ; pwd)
      echo "INFO: the calibration data will be loaded from CONDDB=$CONDDB"
    fi
    
    # prepend all file names with 'p348reco/'
    jobfiles=$(echo "$jobfiles" | sed 's,[^ ]* *,p348reco/&,g')
    
    # add maps/ directory
    jobfiles="$jobfiles maps"
    
    echo "Job files list: $jobfiles"
    
    # prepare job submission directory
    batchdir="batch-$(date +%Y%m%d-%H%M%S)"
    echo "Batch dir: $batchdir"
    echo "Packaging job files ..."
    mkdir $batchdir
    cp $0 $batchdir/
    cp $list $batchdir/list.txt
    tar -zc -f $batchdir/prog.tgz -C .. $jobfiles
    
    # prepare for EOS output
    if [[ "$EOSpath" != "" ]] ; then
      # update the EOSpath to point to batch folder
      EOSpath="${EOSpath}/$batchdir"
      
      echo "INFO: the jobs output will be transferred to the EOS directory"
      echo "      (instance) EOS_MGM_URL = $EOS_MGM_URL"
      echo "      (path)     EOSpath = $EOSpath"
      
      # create output directory and
      # check the destination area is available for write:
      echo "Create EOS path: ${EOSpath} ..."
      eos mkdir -p ${EOSpath}
    fi
    
    # prepare jobs
    cd $batchdir
    
    echo "Parsing njobs=$njobs"
    if [[ "$njobs" == "byRun" ]] ; then
      cat list.txt | awk '{fname="job"substr($0, length($0)-9, 6); print > fname}'
    elif [[ "$njobs" == "byRunSplit"* ]] ; then
      nJobsPerRun=${njobs##*=}
      echo $nJobsPerRun
      cat list.txt | awk '{fname="job"substr($0, length($0)-9, 6); print > fname}'
      for fRun in job??????
      do
        split -a 5 -d -l $nJobsPerRun $fRun ${fRun}_
        rm $fRun
      done
    else
      nfiles=$(wc -l < list.txt)
      files_per_job=$((nfiles/njobs))
      if (( files_per_job == 0 )) ; then
        files_per_job="1"
      fi
      split -a 5 -d -l $files_per_job list.txt job
    fi
    
    #A.C. compute disk space required, considering the job with the largest requirement
    #At this point of the script, each job has the list of files reported in the file jobABCD
    #By HTCondor default, 1 job = 1 core = 20 GB space. If more space is requested, it is necessary to ask for more cores
    maxSize=0
    size=0
    for f in ./job*
    do
         size=$(du -m `cat $f` | awk '{i+=$1} END {print i;}' )
         #echo "job files list: $f size: $size MB"
         if (( $size > $maxSize )) ; then
             maxSize=$size
         fi
    done
    maxSize=$((maxSize/1024))
    nCores=$((maxSize/20 + 1))
    
    
    echo "Jobs total: $(find -name 'job*' | wc -w)"
    echo "Largest disk space required (GB): $maxSize"
    echo "Number of cores required (1 core <--> 20 GB): $nCores"
    
    # check nCores limit, never set maxCores value larger than 32
    maxCores=8
    if (( $nCores > $maxCores )) ; then
        echo "ERROR: Number of cores exceeds the limit maxCores=$maxCores"
        echo "To avoid:"
        echo "  - increase the number of jobs (current njobs=$njobs), or"
        echo "  - increase maxCores in the script"
        cd ..
        rm -rf $batchdir
        exit 1
    fi

    {
      echo "Program = $prog"
      echo "Job files = $jobfiles"
      echo "File list = $list"
      echo "MaxRuntime = $durationcli ($duration seconds)"
      echo "Njobs = $njobs"
      echo "Batch directory = $(pwd)"
      echo "Git commit id = $(git rev-parse HEAD)"
      echo "Git status:"
      git status
      echo "Git diff:"
      git diff
    } > info.txt
    
    
    # submit jobs
    jobcmd="$(pwd)/$0"
    
    {
      echo "JobBatchName = $batchdir"
      echo "+MaxRuntime = $duration"
      echo "executable  = $jobcmd"
      echo "arguments   = slave" $(pwd) $prog '$(ClusterId)' '$(ProcId)' '$(jobid)' $EOSpath
      # propagate env. variables
      echo 'getenv      = EOS_MGM_URL CONDDB'
      echo 'output      = task-$(ProcId).out'
      echo 'error       = task-$(ProcId).out'
      echo 'log         = condor.log'
      echo "RequestCpus = $nCores"
      #echo "request_disk = ${maxSize}G"  #A.C. according to LXPLUS support, it is not possible to use this option, they enforce 1 core = 2 GB ram = 20 GB space
      echo 'transfer_output_files = ""'
      echo 'queue jobid matching files job*'
      echo ""
    } > condor.sub
    
   
    # documentation: http://batchdocs.web.cern.ch/batchdocs/
    condor_submit condor.sub
    
    echo "All jobs submitted"
    echo "Jobs progress monitoring:"
    echo "   condor_q"
    echo "   condor_q -nobatch"
    echo "   condor_tail -f <job-id>"
    echo "   condor_ssh_to_job <job-id>"
   
    ;;
  
  "list" )
    
    echo "BatchId                 Done / Total   Output files"
    
    for i in $(find batch-* -maxdepth 0 -type d) ; do
      # total number of jobs
      ntot=$(find $i -maxdepth 1 -name 'job*' | wc -l)
      
      # number of completed jobs
      ndone=$(find $i -maxdepth 1 -name 'job*result' | wc -l)
      
      # output files
      outlist=$(batch_print_output_files $i)
      
      printf "%s   %4d / %5d   %s\n" $i $ndone $ntot "$outlist"
    done
    
    ;;
  
  "merge" )
    
    if (( $# != 3 )) ; then
      echo "ERROR: incorrect parameters"
      exit 1
    fi
    
    bname="$2"
    fname="$3"
    fname_name=${fname%.*}  # name
    fname_ext=${fname##*.}  # extension
    
    ntot=$(find $bname -maxdepth 1 -name 'job*' | wc -l)
    ndone=$(find $bname -maxdepth 1 -name 'job*result' | wc -l)
    outf=$bname-${fname_name}-${ndone}of${ntot}.${fname_ext}
    
    case "$fname_ext" in
      "root" )
        hadd -f $outf $bname/job*result/$fname
        ;;
      
      "dat" )
        cat $bname/job*result/$fname > $outf
        ;;
      
      * )
        echo "ERROR: unknown file type: $fname_ext"
        exit 1
        ;;
    esac
    
    echo "Output files $fname ($ndone of $ntot) are merged into $outf"
    
    ;;
  
  "mergeall" )
    
    if (( $# != 2 )) ; then
      echo "ERROR: incorrect parameters"
      exit 1
    fi
    
    bname="$2"
    
    for fname in $(batch_list_output_files $bname) ; do
      ./batch.sh merge $bname $fname
    done
    
    ;;
  
  "collect" )
    
    if (( $# != 3 )) ; then
      echo "ERROR: incorrect parameters"
      exit 1
    fi
    
    bname="$2"
    fname="$3"
    fname_ext=${fname##*.}  # extension
    
    outd=$bname-$fname
    echo "Output directory = $outd"
    
    if [[ -d $outd ]] ; then
      echo "ERROR: output directory exists already"
      exit 1
    fi
    
    mkdir $outd
    
    find -L $bname -wholename "*/job*result/$fname" | while read f ; do
      r=${f/*job}
      r=${r/result*}
      cp -a $f "$outd/$r.$fname_ext"
    done
    
    echo "Output files $fname are collected into $outd"
    
    ;;
  
  "slave" )
    
    src=$2
    prog=$3
    clusterid=$4
    procid=$5
    jobid=$6
    EOSpath=$7
      
    jobf=$src/$jobid
    dst=$src/${jobid}result
  
    echo "src=$src"
    echo "prog=$prog"
    echo "clusterid=$clusterid"
    echo "procid=$procid"
    echo "jobid=$jobid"
    echo "jobf=$jobf"
    echo "dst=$dst"
    echo "EOSpath=$EOSpath"
    
    echo "[$(date)] Prepare files"
    tar zxf $src/prog.tgz
    cd p348reco
    cp $jobf list.txt
    echo "List of job files:"
    (cd .. ; find)
    
    echo "[$(date)] Run analysis"
    echo "Input files number = $(wc -l < list.txt)"
    
    #A.C. copy files locally 
    #See: https://cern.service-now.com/service-portal?id=kb_article&n=KB0001998
    awk '{print "root://eospublic.cern.ch/" $0}' list.txt > listEOS.txt
    xrdcp -I listEOS.txt ./

    #Prepare a list with the local files
    cat list.txt | xargs -l basename > listLocal.txt
    
    echo "Input files size = $(du -cbm $(cat listLocal.txt) | grep total | cut -f 1) Mb"
    
    ./$prog $(cat listLocal.txt)
    
    echo "[$(date)] Copy results"
    # remove original job files to keep only files generated by job
    # (sort is to ensure files deleted before directories,
    #  sed is to prepend './' to path and ensure only local files are deleted)
    tar -t -f $src/prog.tgz | sort -r | sed 's,^,./,' | (cd .. ; xargs rm -rf)
    # A.C. remove also the files copied previously from eos
    rm `cat listLocal.txt`
    rm listLocal.txt
    rm listEOS.txt
    
    # A.C. transfer the output file. 
    # If the EOSpath is not given, transfer to the $dst folder. 
    # Otherwise, ALSO create the destination folder in $EOSpath and copy the files there, relying on the env. var EOS_MGM_URL that is exported to the slave
    if [[ "${EOSpath}" == "" ]] ; then
      mkdir $dst
      cp -av * $dst/
    else
      EOSresult="${EOSpath}/${jobid}result"
      eos mkdir -p ${EOSresult}
      eos cp * ${EOSresult}
      # point $dst to EOS output directory for compatibility with 'mergeXXX' commands
      ln -s ${EOSresult} ${dst}
    fi
    
    # remove original job file to indicate success
    rm -f $jobf
    
    echo "[$(date)] Job finished successfully"
    ;;
  
  * )
    echo "ERROR: unknown mode: $mode"
    exit 1
    ;;
esac
