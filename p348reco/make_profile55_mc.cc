// ROOT
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <TProfile2D.h>

// c++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"
#include "simu.h"

int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "Usage: ./makeprofile_mc.exe <MC file name> " << std::endl;
    return 1;
  }
  
  std::ifstream inFile(argv[1]);
  
  if (! inFile) {
    std::cerr << "ERROR: can't open file " << argv[1] << std::endl;
    return 1;
  }
  
  const int Nevents = 1e8;
    
  // cuts for selecting profile
  const double srd_cut[] = {1.,70.}; //MeV
  const double HCAL_cut = 1.; //GeV

#if 0
  ECAL:ECALCellSize=38.2
  ECAL:ECALConstructed=1
  ECAL:ECALEnd=3400.86
  ECAL:ECALStart=2950.86
  ECAL:ECALX=-359.854
  ECAL:ECALY=-19.1
  ECAL:NCellsECAL=6
  ECAL:NCellsECALX=5
#endif

  TFile* file_profile = new TFile("shower_profileMC.root", "RECREATE");

  //VARIABLE RELEVANT TO SHOWER PROFILE
  //size of the spot where the profiles are selected
  const double spot_size = 20.; //mm
  //size of the bin of the profile, should not go under 0.3 mm to not exceed MM precision in extrapolate the line
  const double binwidth = 0.666666666666;
  const int Nbin_profile = 2. * spot_size / binwidth;
  // energy fraction in cell in percent
  const double norm = 100.;
  

  /*      define TProfile2D to fill, The profile have their center in the same center of the beam spot on the ECAL surface and they express
          the percentage of the total energy recorded by the ECAL deposited in each cell   */
  ////  chi-square distribution due to the large fluctuation of low energy deposit in the cells ////
  TString name;
  TString title;
  TProfile2D* Shower_Profiles[6][6]; // 6 for compatibility; only 5 will be used
  for(int x=0; x < 6; ++x)
    for(int y=0; y < 6; ++y)
      {
        name.Form("ECAL1-%i-%i", x, y);
        title.Form("ECAL energy fraction deposited in cell Nr.%iX%i",x,y);
        Shower_Profiles[x][y] = new TProfile2D(name, title, Nbin_profile, -spot_size, spot_size, Nbin_profile, -spot_size, spot_size, 0., 100.);
      } 
  
  // event loop, read event by event
  int NAccepted = 0;
  for (int iev = 0; iev < Nevents; iev++) {

    RecoEvent e = RunP348RecoMC(inFile);
    if (!e.mc) break;

    // ECAL smearing (will be moved to central part of p348reco
    for (int d = 0; d < 2; ++d) {
      for (int x = 0; x < ECAL_pos.Nx; ++x) {
        for (int y = 0; y < ECAL_pos.Ny; ++y) {
          e.ECAL[d][x][y].energy *= 1. + gRandom->Gaus(0., 0.04);
        }
      }
    }

    // Calculate total energy in 5x5
    //const double ecal1 = e.ecalTotalEnergy(1);// this is in all ecal
    double ecal1 = 0.;
    for (int x = 0; x < 5; x++)
      for (int y = 0; y < 5; y++)
        ecal1 += e.ECAL[1][x+ECAL0BEAMCELL.ix-2][y+ECAL0BEAMCELL.iy-2].energy;

    // print progress
    if (iev % 10000 == 1)
      std::cout << "===> Event #" << iev << std::endl;

    //Entry point in the coordinate system of the central cell
    const TVector3 P_ecal(e.mc->ECALEntryX, e.mc->ECALEntryY, 0.);
    const TVector3 showerpos = P_ecal - ECAL0BEAMSPOT_pos;

    // Determine central cell by looking at the cell with most energy
    double totalenergy = -1.;
    int xmax=-1, ymax=-1;
    for (int x = 0; x < ECAL_pos.Nx; x++) {
      for (int y = 0; y < ECAL_pos.Ny; y++) {
        double tot = e.ECAL[0][x][y].energy + e.ECAL[1][x][y].energy;
        if (tot > totalenergy) {
          totalenergy = tot;
          xmax = x; ymax = y;
        }
      }
    }

    //prepare booleans of the cuts

    //maxcell
    //const bool maxcellcut = xmax == ECAL0BEAMCELL.ix && ymax == ECAL0BEAMCELL.iy; // Looks like it is not needed
    const bool maxcellcut = true;

    //SRD
    const bool SRDcut = e.SRD[0].energy / MeV > srd_cut[0] && e.SRD[1].energy / MeV > srd_cut[0] && e.SRD[2].energy / MeV > srd_cut[0] &&
                        e.SRD[0].energy / MeV < srd_cut[1] && e.SRD[1].energy / MeV < srd_cut[1] && e.SRD[2].energy / MeV < srd_cut[1];

    //PRS
    const bool PRScut = e.ecalTotalEnergy(0) > 0.4;

    //VETO
    double veto = 0.;
    double a1, a2, aveto;
    for (int iv = 0; iv < 3; ++iv) {
      a1 = e.VETO[2*iv].energy;
      a2 = e.VETO[2*iv+1].energy;
      aveto = sqrt(a1*a2);
      if(a1 < 0.3*a2) aveto = a2;
      if(a2 < 0.3*a1) aveto = a1;
      veto += aveto;
    }
    const bool VETOcut = veto < 0.01;
    //if(!VETOcut) std::cout << "Cut by VETO" << std::endl;

    const bool HCALcut = e.hcalTotalEnergy(0) < HCAL_cut;

    const bool cutAll = maxcellcut && SRDcut && PRScut && VETOcut && HCALcut;

    if (!cutAll) continue;
    NAccepted++;

    int xoffset = ECAL0BEAMCELL.ix - 2;
    int yoffset = ECAL0BEAMCELL.iy - 2;

    // fill the shower profiles
    for (int x = 0; x < 5; x++)
      for (int y = 0; y < 5; y++)
        Shower_Profiles[x][y]->Fill(showerpos.X(), showerpos.Y(), norm*e.ECAL[1][x+xoffset][y+yoffset].energy/ecal1);
  }

  std::cout << "Number of accepted events = " << NAccepted << std::endl;

  int NBadCentral = CheckProfileStatistics(Shower_Profiles);
  //std::cout << "Nbins with poor statistics central part = " << NBadCentral << std::endl;

  DumpProfile(Shower_Profiles);

  TVector3 ProfileCenter = GetProfileCenter(Shower_Profiles);
  cout << "ecal calocoord x = " << ProfileCenter.X() << " y = " << ProfileCenter.Y() << endl;

  // Add list of parameters
  file_profile->cd();
  TList* parlist = new TList();
  parlist->Add(new TParameter<int>("pcenterX", 2)); // It is always (2,2) for the matrix 5x5
  parlist->Add(new TParameter<int>("pcenterY", 2));
  // These parameters are read by LoadMCGeometryFromMetaData() in simu.h
  parlist->Add(new TParameter<int>("dimX", 5)); 
  parlist->Add(new TParameter<int>("dimY", 5));
  parlist->Add(new TParameter<double>("norm", norm));
  parlist->Add(new TParameter<double>("NominalBeamEnergy", NominalBeamEnergy));
  parlist->Write("parameters", TList::kSingleKey);

  file_profile->Write();
  file_profile->Close();

  return 0;
}
