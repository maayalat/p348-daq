// ROOT
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>

// c++
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

// P348 reco
#include "p348reco.h"
#include"shower.h"
#include "simu.h"

int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cerr << "Usage: ./examplemc.exe <MC file name>" << std::endl;
    return 1;
  }
  
  std::ifstream inFile(argv[1]);
  
  if (! inFile) {
    std::cerr << "ERROR: can't open file " << argv[1] << std::endl;
    return 1;
  }
  
  TFile* hOutputFile = new TFile("hist.root", "RECREATE");

  TH2I ehplot("ehplot", "ECAL vs HCAL;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH2I ehplot_sig("ehplot_sig", "ECAL vs HCAL for signal only events;ECAL, GeV;HCAL, GeV;#nevents", 150, 0, 150, 100, 0, 150);
  TH1I bgoplot("bgoplot", "BGO;Energy, MeV;#nevents", 150, 0, 150);

  TH1I mcecalentries("MC_ecalentries", "MC ECAL entry X coordinates in mm;X, mm;#nevents", 100, -200., 200.);
  
  TH1D Chi ("Chi","chi square distribution of shower profile",1000,0,100);
  
  // event loop, read event by event
  for (int iev = 0; true; iev++) {

    const RecoEvent e =RunP348RecoMC(inFile);
    if (!e.mc) break;

    // print progress
    if (iev % 100 == 1)
      std::cout << "===> Event #" << iev << std::endl;

    double ecal = 0;
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y)
          ecal += e.ECAL[d][x][y].energy;
    
    double hcal = 0;
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y)
          hcal += e.HCAL[d][x][y].energy;
    
    double bgo = 0;
    for (int x = 0; x < 8; ++x)
      bgo += e.BGO[x].energy;
    
    bgo /= MeV; // convert to MeV

    double srd = 0;
    for (int x = 0; x < 3; ++x)
      srd += e.SRD[x].energy;

    srd /= MeV; // convert to MeV

    //chi example, put 1 at the end to use MCtruth for the initial position of the shower
    double chi = calcShowerChi2(e,2,2,3,SP_MC);


    double veto = 0.;
    for (int iv = 0; iv < 3; ++iv)
      veto += e.vetoEnergy(iv);

    std::cout << "Event #" << iev
              << " ECAL = " << ecal << " GeV"
              << " HCAL = " << hcal << " GeV"
              << " BGO = " << bgo << " GeV"
              << " Veto = " << veto << " GeV"
              << std::endl;

    ehplot.Fill(ecal, hcal);
    // Tag events with no DM production by checking default values
    if(e.mc->isDM()) ehplot_sig.Fill(ecal, hcal);
    bgoplot.Fill(bgo);
    if(e.mc) mcecalentries.Fill(e.mc->ECALEntryX);
    Chi.Fill(chi);

  }
  
  hOutputFile->Write();

  cout << "-------------------" << endl;
  cout << "MC sample summary:" << endl
     << Form("N_eot     = %6.4e", mcrunoption<double>("General:EventsSimulated", 0)) << endl // the number of primaries (events) simulated
     << Form("N_trg     = %6.4e", mcrunoption<double>("General:EventsTriggered", 0)) << endl // the number of events satisfying the trigger condition, for example "beam telescope"
     << Form("N_file    = %6.4e", ehplot.GetEntries()) << endl // the number of events in the .d file
     << Form("N_sig     = %6.4e", mcrunoption<double>("DarkMatter:NDMsimulated", 0)) << endl // the number of events with signal (DM)
     << Form("N_sig_trg = %6.4e", ehplot_sig.GetEntries()) << endl // the number of events with signal (DM) that passed the trigger condition
     << endl;
  cout << "-------------------" << endl;

  return 0;
}
