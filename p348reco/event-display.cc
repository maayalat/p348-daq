// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TApplication.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TProfile.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "THStack.h"
#include "TLine.h"
#include "TBox.h"
#include "TText.h"
#include "TPaveText.h"
#include "TMarker.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking.h"
#include "shower.h"

struct GeomPlane {
    TVector3 pos;  // front side center point position
    TVector3 dim;  // X,Y,Z dimensions
    
    // calculate geometry of subcell
    // nx, ny - total number of cells
    // ix, iy - particular cell index
    GeomPlane cell(const int nx, const int ny, const int ix, const int iy) const {
      return {
          { pos.X() + dim.X() * (-0.5 + (ix+0.5)/nx),
            pos.Y() + dim.Y() * (-0.5 + (iy+0.5)/ny),
            pos.Z()},
          
          { dim.X()/nx,
            dim.Y()/ny,
            dim.Z()
          }
        };
    }
    
    // coordinate of the back (downstream) side center point
    TVector3 backpos() const { return pos + TVector3(0, 0, dim.Z()); }
};

TLine * projXZ( const GeomPlane & p ) {
    return new TLine( p.pos.Z(), p.pos.X() + p.dim.X()/2,
                      p.pos.Z(), p.pos.X() - p.dim.X()/2 );
}

TLine * projYZ( const GeomPlane & p ) {
    return new TLine( p.pos.Z(), p.pos.Y() + p.dim.Y()/2,
                      p.pos.Z(), p.pos.Y() - p.dim.Y()/2 );
}

TBox* boxXZ(const GeomPlane & p) {
  return new TBox(p.pos.Z(), p.pos.X() + p.dim.X()/2,
                  p.pos.Z() + p.dim.Z(), p.pos.X() - p.dim.X()/2);
}

TBox* boxYZ(const GeomPlane & p) {
  return new TBox(p.pos.Z(), p.pos.Y() + p.dim.Y()/2,
                  p.pos.Z() + p.dim.Z(), p.pos.Y() - p.dim.Y()/2);
}

void scaleByEnergy(TBox* box, const double edep, const double emax) {
  double f = edep / emax;
  
  // sanity
  f = fabs(f);
  if (f > 1.) f = 1.;
  
  // size
  const double x1 = box->GetX1();
  const double x2 = box->GetX2();
  box->SetX2(x1 + f * (x2 - x1));
  
  // color
  if (f > 0.5) {
    box->SetFillColor(kRed);
  }
  else if (f > 0.25) {
    box->SetFillColor(kOrange);
  }
  else {
    box->SetFillColor(kYellow);
  }
}

void set_atributes( TGraph * graph_ ) {
    graph_->SetMarkerColor(2);
    graph_->SetMarkerStyle(5);
    graph_->SetMarkerSize(0.5);
}

void axis_center_labels(TAxis* a)
{
    const int ndiv = a->GetXmax() - a->GetXmin();
    a->SetNdivisions(ndiv);
    a->CenterLabels();
}

void histos_axis_center_labels( TH2D* hist) {
  axis_center_labels(hist->GetXaxis());
  axis_center_labels(hist->GetYaxis());
}

bool cmpTtextByX(TText* a, TText* b) { return a->GetX() < b->GetX(); }

void add_tracker_labels( DetGeo MMs_[12], DetGeo GEMs_[4] )
{
  // create labels
  
  vector<TText*> labels;
  
  for (int i = 0; i < 12+4; ++i) {
    const bool isMM = (i < 12);
    const int idx = isMM ? i : i-12;
    const DetGeo& dg = isMM ? MMs_[idx] : GEMs_[idx];
    const double x = dg.pos.Z();
    const double y = -500; //dg.pos.X();
    
    TString name;
    name.Form("%s%d", (isMM ? "MM" : "GM"), idx+1);
    TText* l = new TText(x, y, name);
    
    l->SetTextAlign(13); // adjustment: 13 = left top
    l->SetTextSize(0.04);
    l->SetTextColor(kGray+3);
    l->Draw();
    
    labels.push_back(l);
  }
  
  // apply Y offset correction to avoid labels overlap
  
  sort(labels.begin(), labels.end(), cmpTtextByX);
  
  const double lw = 600; // label width
  const double lh = 50;  // label height
  double xprev = -1e6;
  double yoff = 0;
  
  for (size_t i = 0; i < labels.size(); ++i) {
    TText* l = labels[i];
    
    const double x = l->GetX();
    const double y = l->GetY();
    
    if ((xprev+lw) < x)
      yoff = 0;    // no overlap - reset offset
    else
      yoff -= lh;  // increase offset to avoid overlap
    
    xprev = x;
    
    //l->SetX(x);
    l->SetY(y+yoff);
  }
}

int main(int argc, char *argv[])
{
  // parse command line
  bool gui = true;
  bool pdf = false;
  bool labelsOn = false;
  // ECAL, HCAL energy deposition display scaling method: absolute (true), relative to total (false)
  const bool absScale = true;
  bool showxy = false;
  int nevery = 1;
  int delay = 0;
  vector<string> list;
  
  for (int i = 1; i < argc; ++i) {
    const string arg = argv[i];
    if (arg == "--gui") { gui = true; pdf = false; continue; }
    if (arg == "--pdf") { gui = false; pdf = true; continue; }
    if (arg == "--guipdf") { gui = true; pdf = true; continue; }
    if (arg == "--labels") { labelsOn = true; continue; }
    if (arg == "--showxy") { showxy = true; continue; }
    if (arg.substr(0, 8) == "--every=") {
      nevery = cast<int>(arg.substr(8));
      continue;
    }
    if (arg.substr(0, 8) == "--delay=") {
      delay = cast<int>(arg.substr(8));
      continue;
    }
    list.push_back(arg);
  }
  
  if (list.empty()) {
    cerr << "Usage: ./event-display.exe [options] file1 [file2 ...]\n"
         << "\n"
         << "options:\n"
         << "  --gui     - graphical user interface mode (default)\n"
         << "  --pdf     - batch mode, save events to file event-display.pdf\n"
         << "  --guipdf  - show GUI and create .pdf\n"
         << "  --labels  - show detector names\n"
         << "\n"
         << "  --every=N - draw only 1 of N events (default N=1)\n"
         << "  --delay=N - delay in ms before next event show (default N=0)\n"
         << endl;
    return 1;
  }
  
  TApplication app("event-display", 0, 0);
  if (!gui) gROOT->SetBatch();
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (size_t i = 0; i < list.size(); ++i)
    manager.AddDataSource(list[i]);
  manager.Print();
  
  // load geometry of the first event
  manager.ReadEvent();
  const bool decoded = manager.DecodeEvent();
  RunP348Reco(manager);
  
  // rewind to the first event
  manager.Clear();
  manager.SetMapsDir("../maps");
  for (size_t i = 0; i < list.size(); ++i)
    manager.AddDataSource(list[i]);
  
  // dimensions: X,Y,Z [mm]
  const TVector3 mmdim(200, 200, 0);
  const TVector3 gemdim(200, 200, 0);
  
  // ECAL
  const TVector3 ecal0dim(229.2, 229.2, 500);  // real Z=50.24
  const TVector3 ecal1dim(229.2, 229.2, 1000); // real Z=420.76
  const GeomPlane ecal[2] = {
    // ECAL0
    {ECAL_pos.pos, ecal0dim},
    // ECAL1, situated right after ECAL0
    {ECAL_pos.pos + TVector3(0,0,ecal0dim.Z()), ecal1dim}
  };
  
  // HCAL, situated right after ECAL1
  // TODO: the definition of gaps geometry as listed in 2017 runs list by Vladimir Poliakov,
  //       to be moved to conddb
  //       https://twiki.cern.ch/twiki/pub/P348/RunsInfo2017/list_runs_Sep_2017_v4.pdf
  const TVector3 hcaldim(600, 600, 1650);
  const TVector3 hcalstart = ecal[1].backpos() + TVector3(0, 0, 300); // +300mm is gap ECAL-HCAL
  const GeomPlane hcal[4] = {
    {hcalstart, hcaldim},
    {hcalstart + TVector3(-50,0,hcaldim.Z()+100), hcaldim},
    {hcalstart + TVector3(-200,0,hcaldim.Z()+100+hcaldim.Z()+100), hcaldim},
    {TVector3(0,0,hcalstart.Z()+hcaldim.Z()+100+hcaldim.Z()+100), hcaldim}
  };
  
  // magnet aperture dimensions - page 7 of
  // https://twiki.cern.ch/twiki/pub/P348/RunsInfo2017/2017.09.19_H4_TEST_NA64_MBPL_3_Flanges.pdf
  const TVector3 magdim(500, 130, MAGD_pos.pos.Z() - MAGU_pos.pos.Z());
  const GeomPlane magnet = {MAGU_pos.pos, magdim};
  
  
  vector<GeomPlane> trackerPlanes;
  // MMs
  DetGeo MMs[12] = {MM1_pos, MM2_pos, MM3_pos, MM4_pos, MM5_pos, MM6_pos, MM7_pos, MM8_pos, MM9_pos, MM10_pos, MM11_pos, MM12_pos};
  for (size_t i = 0; i < 12; ++i) {
    const GeomPlane g = {MMs[i].pos, mmdim};
    trackerPlanes.push_back(g);
  }
  
  // GEMs
  DetGeo GEMs[4] = {GM1_pos, GM2_pos, GM3_pos, GM4_pos};
  for (size_t i = 0; i < 4; ++i) {
    const GeomPlane g = {GEMs[i].pos, gemdim};
    trackerPlanes.push_back(g);
  }
  
  // ECAL
  trackerPlanes.push_back(ecal[0]);
  trackerPlanes.push_back(ecal[1]);

  // ECAL XY projection
  TH2D* ecalXY[2];
  for (int i = 0; i < 2; ++i) {
    char title[8];
    snprintf( title, 8, "e%d", i);
    ecalXY[i] = new TH2D(title, title, 6, 0, 6, 6, 0, 6);
    ecalXY[i]->GetXaxis()->SetLabelOffset(0.01);
    ecalXY[i]->SetMarkerSize(2);
    histos_axis_center_labels(ecalXY[i]);
  }
    // SRD XY projection
  TH2D* srdXY = new TH2D("srd", "srd", 3, 0, 3, 1, 0, 1);
  srdXY->GetXaxis()->SetLabelOffset(0.01);
  srdXY->SetMarkerSize(5);
  histos_axis_center_labels(srdXY);
  // VETO XY projection
  TH2D* vetoXY = new TH2D("veto", "veto", 3, 0, 3, 1, 0, 1);
  vetoXY->GetXaxis()->SetLabelOffset(0.01);
  vetoXY->SetMarkerSize(5);
  histos_axis_center_labels(vetoXY);
  // HCAL
  TH2D* hcalXY[4];
  for (int i = 0; i < 4; ++i) {
    char title[8];
    snprintf( title, 8, "h%d", i);
    hcalXY[i] = new TH2D(title, title, 3, 0, 3, 3, 0, 3);
    hcalXY[i]->GetXaxis()->SetLabelOffset(0.01);
    hcalXY[i]->SetMarkerSize(5);
    histos_axis_center_labels(hcalXY[i]);
    trackerPlanes.push_back(hcal[i]);
  }

  gStyle->SetLineScalePS(1);
  gStyle->SetGridColor(kGray);
  gStyle->SetNdivisions(10, "Y");
  gStyle->SetTickLength(0.015, "Y");
  gStyle->SetPaintTextFormat("4.1f");
  gStyle->SetOptStat(0);
  
  TCanvas* c = new TCanvas("event-display", "event-display");
  const int npads = showxy ? 3 : 2;
  c->Divide(1, npads, 0.0001, 0.0001);
  
  TPad* p1 = (TPad*) c->cd(1);
  p1->SetGrid();
  p1->SetMargin(0.05,0.01,0.05,0.02);
  
  TPad* p2 = (TPad*) c->cd(2);
  p2->SetGrid();
  //p2->SetFillColor(0);
  //p2->SetFillStyle(0);
  //p2->SetFrameFillColor(0);
  //p2->SetFrameFillStyle(0);
  //p2->SetBorderMode(1);
  //p2->SetBorderSize(1);
  p2->SetMargin(0.05,0.01,0.05,0.02);
  
  TPad* cXY = showxy ? (TPad*) c->cd(3) : 0;
  cout << "cXY=" << cXY << endl;
  if (cXY) {
  cXY->Divide(8, 1);
  cXY->SetFrameBorderMode(0);
  cXY->SetMargin(0.05,0.01,0.05,0.02);
  
  for (int i = 1; i <= 8; ++i) {
    TPad* p = (TPad*) cXY->cd(i);
    p->SetGrid();
    p->SetMargin(0.05, 0.01, 0.05, 0.02);
    //p->SetBorderMode(0);
  }
  }
  
  // move pads to free space for event ID string
  const double padh = (1. - 0.04) / npads;
  p1->SetPad(0,  1-padh,  1,  1);
  p2->SetPad(0,  1-padh*2,  1,  1-padh);
  if (cXY) {
  cXY->SetPad(0,  1-padh*3,  1,  1-padh*2);
  cXY->cd(1)->SetPad(0,   0, 0.1, 1);
  cXY->cd(2)->SetPad(0.1, 0, 0.3, 1);
  cXY->cd(3)->SetPad(0.3, 0, 0.5, 1);
  cXY->cd(4)->SetPad(0.5, 0, 0.6, 1);
  cXY->cd(5)->SetPad(0.6, 0, 0.7, 1);
  cXY->cd(6)->SetPad(0.7, 0, 0.8, 1);
  cXY->cd(7)->SetPad(0.8, 0, 0.9, 1);
  cXY->cd(8)->SetPad(0.9, 0,   1, 1);
  }
  
  // projections frames
  c->cd(1);
  
  // TODO: make ranges run-dependent
  
  //p1->DrawFrame(-21000, -950, 8000, 450);   // before 2021
  p1->DrawFrame(-1000, -500, 30000, 900);     // 2021
  
  c->cd(2);
  //p2->DrawFrame(-21000, -400, 8000, 400);   // before 2021
  p2->DrawFrame(-1000, -400, 30000, 400);     // 2021
  
  for (size_t i = 0; i < trackerPlanes.size(); ++i) {
    const GeomPlane& g = trackerPlanes[i];
    if (g.pos.Mag2() == 0.) continue;
    
    c->cd(1);
    projXZ(g)->Draw("SAME");
    
    c->cd(2);
    projYZ(g)->Draw("SAME");
  }
  
  TBox* boxMAGxz = boxXZ(magnet);
  boxMAGxz->SetFillStyle(0);
  boxMAGxz->SetLineColor(kBlue);
  c->cd(1);
  boxMAGxz->Draw();
  
  TBox* boxMAGyz = boxYZ(magnet);
  boxMAGyz->SetFillStyle(0);
  boxMAGyz->SetLineColor(kBlue);
  c->cd(2);
  boxMAGyz->Draw();
  
  // event id
  TText* textid = new TText(0.01, 0.01, "");
  textid->SetNDC();
  textid->SetTextFont(82);
  textid->SetTextSize(0.03);
  c->cd();
  textid->Draw();
  
  // reco info
  c->cd(1);
  TPaveText* pt = new TPaveText(1-0.12 - 0.18, 1-0.04 - 0.25, 1-0.12, 1-0.04, "NB NDC");
  pt->SetTextAlign(12);
  for (int i = 0; i < 4; ++i) pt->AddText("");
  pt->Draw();
  
  // projections names
  {
    c->cd(1);
  //p1->SetMargin(0.05,0.01,0.05,0.02);
    TPaveText* title = new TPaveText(0.06, 1-0.04 - 0.08, 0.06 + 0.12, 1-0.04, "NB NDC");
    title->SetFillColor(kGray+2);
    title->SetTextColor(kWhite);
    title->SetTextAlign(12);
    title->AddText("Top View (XZ)");
    title->Draw();
  }
  
  if (labelsOn) { add_tracker_labels(MMs, GEMs); }
  
  {
    c->cd(2);
    TPaveText* title = new TPaveText(0.06, 1-0.04 - 0.08, 0.06 + 0.12, 1-0.04, "NB NDC");
    title->SetFillColor(kGray+2);
    title->SetTextColor(kWhite);
    title->SetTextAlign(12);
    title->AddText("Side View (YZ)");
    title->Draw();
  }
  
  // Draw XY
  if (cXY) {
  cXY->cd(1);
  srdXY->Draw("BOX COL TEXT");
  cXY->cd(2);
  ecalXY[0]->Draw("BOX COL TEXT");
  //ecalHitMarker->Draw("same");
  cXY->cd(3);
  ecalXY[1]->Draw("BOX COL TEXT");
  cXY->cd(4);
  vetoXY->Draw("BOX COL TEXT");
  cXY->cd(5);
  hcalXY[0]->Draw("BOX COL TEXT");
  cXY->cd(6);
  hcalXY[1]->Draw("BOX COL TEXT");
  cXY->cd(7);
  hcalXY[2]->Draw("BOX COL TEXT");
  cXY->cd(8);
  hcalXY[3]->Draw("BOX COL TEXT");
  }
  
  // start pdf output
  if (pdf) c->Print(".pdf[");
  
  // event loop
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    
    // draw part of events
    if (nevt % nevery != 0) continue;
    
    // print progress
    cout << "===> Event #" << nevt << endl;
    
    static TGraph* hitColXZ = 0;
    static TGraph* hitColYZ = 0;
    delete hitColXZ;
    delete hitColYZ;
    hitColXZ = 0;
    hitColYZ = 0;
    
    static vector<TBox*> boxesXZ;
    static vector<TBox*> boxesYZ;
    for (size_t i = 0; i < boxesXZ.size(); ++i) delete boxesXZ[i];
    for (size_t i = 0; i < boxesYZ.size(); ++i) delete boxesYZ[i];
    boxesXZ.clear();
    boxesYZ.clear();
    // Clear histos for XY projections
    ecalXY[0]->Reset();
    ecalXY[1]->Reset();
    srdXY->Reset();
    vetoXY->Reset();
    hcalXY[0]->Reset();
    hcalXY[1]->Reset();
    hcalXY[2]->Reset();
    hcalXY[3]->Reset();
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
    // process only "physics" events
    if (!e.isPhysics) continue;
    
    // ECAL reco data
    const double emax[2] = {5, 95};
    double ecalFullEnergy[2] = {0};
    double ecal_xz[2][6] = {{0}, {0}};
    double ecal_yz[2][6] = {{0}, {0}};
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y) {
          ecalFullEnergy[d] += e.ECAL[d][x][y].energy;
          ecal_xz[d][x] += e.ECAL[d][x][y].energy;
          ecal_yz[d][y] += e.ECAL[d][x][y].energy;
          // XY projection
          if (e.ECAL[d][x][y].hasDigit) {
            ecalXY[d]->Fill(x, y, e.ECAL[d][x][y].energy);
          }
        }
    for (int d = 0; d < 2; ++d)
      for (int i = 0; i < 6; ++i) {
        const GeomPlane c = ecal[d].cell(6, 6, i, i);
        
        // XZ projection
        TBox * tboxXZ = boxXZ(c);
        scaleByEnergy(tboxXZ, ecal_xz[d][i], absScale ? emax[d] : ecalFullEnergy[d]);
        boxesXZ.push_back(tboxXZ);
        
        // YZ projection
        TBox * tboxYZ = boxYZ(c);
        scaleByEnergy(tboxYZ, ecal_yz[d][i], absScale ? emax[d] : ecalFullEnergy[d]);
        boxesYZ.push_back(tboxYZ);
      }
    // SRD reco data
    const double zero = 0;
    for (int i = 0; i < 3; ++i) {
      if (e.SRD[i].hasDigit) {
        srdXY->Fill(i, zero, e.SRD[i].energy / MeV);
      }
    }
    // VETO reco data
    for (int i = 0; i < 3; ++i) {
      vetoXY->Fill(i, zero, e.vetoEnergy(i) / MeV);
    }
    // HCAL reco data
    const double hmax[4] = {25, 25, 25, 25};
    double hcalFullEnergy[4] = {0};
    double hcal_xz[4][3] = {{0},{0},{0},{0}};
    double hcal_yz[4][3] = {{0},{0},{0},{0}};
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y) {
          hcalFullEnergy[d] += e.HCAL[d][x][y].energy;
          hcal_xz[d][x] += e.HCAL[d][x][y].energy;
          hcal_yz[d][y] += e.HCAL[d][x][y].energy;
          hcalXY[d]->Fill(x, y, e.HCAL[d][x][y].energy);
        }
    for (int d = 0; d < 4; ++d)
      for (int i = 0; i < 3; ++i) {
        const GeomPlane c = hcal[d].cell(3, 3, i, i);
        
        // XZ projection
        TBox * tboxXZ = boxXZ(c);
        scaleByEnergy(tboxXZ, hcal_xz[d][i], absScale ? hmax[d] : hcalFullEnergy[d]);
        boxesXZ.push_back(tboxXZ);
        
        // YZ projection
        TBox * tboxYZ = boxYZ(c);
        scaleByEnergy(tboxYZ, hcal_yz[d][i], absScale ? hmax[d] : hcalFullEnergy[d]);
        boxesYZ.push_back(tboxYZ);
      }

    const double srd = (e.SRD[0].energy + e.SRD[1].energy + e.SRD[2].energy) / MeV;

    const bool hasMaster = e.hasMasterTime();
    const double master = e.masterTime;

    // find ecal hit position. Look at calocoord.h for more information
    TVector3 ecalHitVec = ecalHitCoord(e);
    TMarker * ecalHitMarker;
    ecalHitMarker = new TMarker(3 + ecalHitVec(0)/38.2, 3 + ecalHitVec(1)/38.2, 5);
    ecalHitMarker->SetMarkerColor(1);
    ecalHitMarker->SetMarkerStyle(3);
    ecalHitMarker->SetMarkerSize(2);
    // collect MM and GEM best hits
    vector<TVector3> hit;
    
    if (e.MM1.hasHit()) hit.push_back(e.MM1.abspos());
    if (e.MM2.hasHit()) hit.push_back(e.MM2.abspos());
    if (e.MM3.hasHit()) hit.push_back(e.MM3.abspos());
    if (e.MM4.hasHit()) hit.push_back(e.MM4.abspos());
    if (e.MM5.hasHit()) hit.push_back(e.MM5.abspos());
    if (e.MM6.hasHit()) hit.push_back(e.MM6.abspos());
    if (e.MM7.hasHit()) hit.push_back(e.MM7.abspos());
    if (e.MM8.hasHit()) hit.push_back(e.MM8.abspos());
    if (e.MM9.hasHit()) hit.push_back(e.MM9.abspos());
    if (e.MM10.hasHit()) hit.push_back(e.MM10.abspos());
    if (e.MM11.hasHit()) hit.push_back(e.MM11.abspos());
    if (e.MM12.hasHit()) hit.push_back(e.MM12.abspos());
    
    if (e.GM01->bestHit) hit.push_back(e.GM01->bestHit.abs_pos());
    if (e.GM02->bestHit) hit.push_back(e.GM02->bestHit.abs_pos());
    if (e.GM03->bestHit) hit.push_back(e.GM03->bestHit.abs_pos());
    if (e.GM04->bestHit) hit.push_back(e.GM04->bestHit.abs_pos());
    
    {
      const size_t n = hit.size();
      Double_t x[n], y[n], z[n];
      for (size_t i = 0; i < n; ++i) {
        x[i] = hit[i].X();
        y[i] = hit[i].Y();
        z[i] = hit[i].Z();
      }
      hitColXZ = new TGraph(n, z, x);
      hitColYZ = new TGraph(n, z, y);
      set_atributes(hitColXZ);
      set_atributes(hitColYZ);
    }
    
    // track
    const SimpleTrack t = simple_tracking_4MM(e);
    static TLine* upXZ = 0;
    static TLine* downXZ = 0;
    static TLine* magdownXZ = 0;
    static TLine* upYZ = 0;
    static TLine* downYZ = 0;
    static TLine* magdownYZ = 0;
    delete upXZ;
    delete downXZ;
    delete magdownXZ;
    delete upYZ;
    delete downYZ;
    delete magdownYZ;
    
    upXZ = 0;
    upYZ = 0;
    if (t.in) {
      const TVector3 p2 = extrapolate_line(t.in, MAGU_pos.pos.Z());
      upXZ = new TLine(t.in.p1.Z(), t.in.p1.X(), p2.Z(), p2.X());
      upYZ = new TLine(t.in.p1.Z(), t.in.p1.Y(), p2.Z(), p2.Y());
    }
    
    downXZ = 0;
    magdownXZ = 0;
    downYZ = 0;
    magdownYZ = 0;
    if (t.out) {
      const TVector3 p0 = extrapolate_line(t.out, MAGU_pos.pos.Z());
      const TVector3 p1 = extrapolate_line(t.out, MAGD_pos.pos.Z());
      const TVector3 p2 = extrapolate_line(t.out, ECAL_pos.pos.Z());
      downXZ = new TLine(p1.Z(), p1.X(), p2.Z(), p2.X());
      magdownXZ = new TLine(p0.Z(), p0.X(), p1.Z(), p1.X());
      magdownXZ->SetLineStyle(2);
      downYZ = new TLine(p1.Z(), p1.Y(), p2.Z(), p2.Y());
      magdownYZ = new TLine(p0.Z(), p0.Y(), p1.Z(), p1.Y());
      magdownYZ->SetLineStyle(2);
    }
    
    // Draw XZ
    c->cd(1);
    hitColXZ->Draw("P");
    for (size_t i = 0; i < boxesXZ.size(); ++i) boxesXZ[i]->Draw("l");
    if (upXZ) upXZ->Draw("same");
    if (downXZ) downXZ->Draw("same");
    if (magdownXZ) magdownXZ->Draw("same");
    // Draw YZ
    c->cd(2);
    hitColYZ->Draw("P");
    if (upYZ) upYZ->Draw("same");
    if (downYZ) downYZ->Draw("same");
    if (magdownYZ) magdownYZ->Draw("same");
    for (size_t i = 0; i < boxesYZ.size(); ++i) boxesYZ[i]->Draw("l");
    
    if (cXY) {
    cXY->cd(2);
    ecalHitMarker->Draw("same");
    }
    
    // reco info
    pt->GetLine(0)->SetTitle(TString::Format("p = %.0f", t.momentum));
    pt->GetLine(1)->SetTitle(TString::Format("SRD = %.1f", srd));
    pt->GetLine(2)->SetTitle(TString::Format("ECAL = %.1f + %.0f",
      e.ecalTotalEnergy(0), e.ecalTotalEnergy(1)));
    pt->GetLine(3)->SetTitle(TString::Format("HCAL = %.0f + %.1f + %.1f + %.1f",
      e.hcalTotalEnergy(0), e.hcalTotalEnergy(1), e.hcalTotalEnergy(2), e.hcalTotalEnergy(3)));
    
    // event id
    time_t evtt = manager.GetEvent().GetTime().first;
    char timestr[100];
    strftime(timestr, 100, "%d %b %Y %H:%M:%S %Z", localtime(&evtt));
    
    TString eid;
    eid.Form("NA64, event %d-%d-%d, %s", e.run, e.spill, e.spillevent, timestr);
    textid->SetTitle(eid);
    
    if (cXY) {
    for (int i = 0; i <= 8; ++i)
      cXY->cd(i)->Modified();
    
    //cXY->Modified();
    //cXY->Update();
    }
    
    c->Modified();
    c->Update();
    gSystem->ProcessEvents();
    
    if (pdf) c->Print(".pdf");

    delete ecalHitMarker;
    
    gSystem->Sleep(delay);
  }
  
  if (pdf) c->Print(".pdf]");
  
  // TODO: program does not exit on normal window close
  // and two Ctrl+C is necessary
  if (gui) app.Run(kTRUE);
  
  return 0;
}
