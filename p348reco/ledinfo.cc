// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TProfile2D.h"

// c++
#include <iostream>
using namespace std;

// P348 reco
#include "p348reco.h"

// extaract detector name from channel name
TString detname(TString name)
{
  const int pos = name.First("-");
  return (pos != kNPOS) ? name(0, pos) : name;
}

void th1_check_overflow(TH1* h)
{
  const TAxis* a = h->GetXaxis();
  const int n = a->GetNbins();
  const double uf = h->GetBinContent(0);
  const double of = h->GetBinContent(n + 1);
  const double uof = fabs(uf) + fabs(of);
  
  if (uof > 0) {
    cout << "WARNING: insufficient axis range "
         << h->GetName() << ":"
         << " axis=" << a->GetTitle()
         << " range=[" << a->GetBinLowEdge(1) << ", " << a->GetBinUpEdge(n) << ")"
         << " underflow=" << uf
         << " overflow=" << of
         << endl;
  }
}


int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./ledinfo.exe file1 [file2 ...]" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();
  
  // histograms
  TFile fh("ledinfo.root" ,"recreate");
  
  TH1D hevents("hevents", "N(LED) per spill;spill;nevents", 200, 1,  201); 
  TProfile hoccupancy("hoccupancy", "Fraction of events with digits;spill", 200, 1, 201);
  
  const int nch = 2*6*6 + 4*3*3 + 3; // ECAL + HCAL + WCAL
  
  // X - channel, Y - amplitude in channel
  TH2I* hled = new TH2I("hled", "LED amplitudes;channel;amplitude, ADC", nch, 0, nch, 4100, 0, 4100);
  hled->SetOption("COL");
  hled->SetStats(kFALSE);
  
  // X - spill, Y - channel, Z - amplitude in spill/channel
  TH3I* hledspill = new TH3I("hledspill", "LED amplitudes;spill;channel;amplitude, ADC", 200, 1, 201, nch, 0, nch, 4100, 0, 4100);
  hledspill->SetOption("BOX1");
  hledspill->SetStats(kFALSE);
  
  // assign bin labels explicitly to ensure fixed set of calibration channels
  TString labels[nch];
  int idx = 0;
  for (int d = 0; d < 2; ++d)
    for (int x = 0; x < 6; ++x)
      for (int y = 0; y < 6; ++y)
        labels[idx++] = TString::Format("ECAL%d-%d-%d", d, x, y);
  
  for (int d = 0; d < 4; ++d)
    for (int x = 0; x < 3; ++x)
      for (int y = 0; y < 3; ++y)
        labels[idx++] = TString::Format("HCAL%d-%d-%d", d, x, y);
  
  for (int d = 0; d < 3; ++d)
    labels[idx++] = TString::Format("WCAL-%d", d);
  
  for (int i = 1; i <= nch; i++) hled->GetXaxis()->SetBinLabel(i, labels[i-1]);
  for (int i = 1; i <= nch; i++) hledspill->GetYaxis()->SetBinLabel(i, labels[i-1]);
  
  
  int runnow = 0;
  
   // event loop
   while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    if (nevt % 1000 == 1) cout << "===> Event #" << nevt << endl;
 
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    // check event type before reconstruction to speed up processing
    // proper event type is set starting from 2016A session (first run = 938)
    const int erun = manager.GetEvent().GetRunNumber();
    if (erun >= 938) {
      const CS::DaqEvent::EventType etype = manager.GetEvent().GetType();
      const bool isCalibration = (etype == CS::DaqEvent::CALIBRATION_EVENT);
      if (!isCalibration) continue;
    }
    
    
    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);
    
  
    // Only LED Events
    if (!e.isCalibration) continue;
    
    cout << "  Calibration event " << e.id();
    
    runnow = e.run;
    int nspill = e.spill;
    
    cout << " neventdigits=" << manager.GetEventDigits().size();
    
    // count number of LED events in spill
    hevents.Fill(nspill);
    
    int ndigits = 0;
    
    // HCAL
    for (int d = 0; d < 4; ++d)
      for (int x = 0; x < 3; ++x)
        for (int y = 0; y < 3; ++y) {
          const Cell& c = e.HCAL[d][x][y];
          hoccupancy.Fill(nspill, c.hasDigit);
          //cout << d << "-" << x << "-" << y << " hasDigit=" << c.hasDigit << endl;
          if (!c.hasDigit) continue;
          ndigits++;
          const char* name = c.calib->name.c_str();
          const double amp  = c.amplitude;
          //cout << name << " amp=" <<  amp << endl;
          hled->Fill(name, amp, 1.);
          hledspill->Fill(nspill, name, amp, 1.);
        }
    
    // ECAL
    for (int d = 0; d < 2; ++d)
      for (int x = 0; x < 6; ++x)
        for (int y = 0; y < 6; ++y) {
          const Cell& c = e.ECAL[d][x][y];
          hoccupancy.Fill(nspill, c.hasDigit);
          if (!c.hasDigit) continue;
          ndigits++;
          const char* name = c.calib->name.c_str();
          double amp  = c.amplitude;
          //cout << name << " amp=" <<  amp << endl;
          hled->Fill(name, amp, 1.);
          hledspill->Fill(nspill, name, amp, 1.);
        }
    
    // WCAL
    for (int d = 0; d < 3; ++d) {
          const Cell& c = e.WCAL[d];
          hoccupancy.Fill(nspill, c.hasDigit);
          if (!c.hasDigit) continue;
          ndigits++;
          const char* name = c.calib->name.c_str();
          double amp  = c.amplitude;
          //cout << name << " amp=" <<  amp << endl;
          hled->Fill(name, amp, 1.);
          hledspill->Fill(nspill, name, amp, 1.);
    }
    
    cout << " ndigits=" << ndigits << endl;
  }
  
  // check overflow
  cout << "INFO: checking axis overflow..." << endl;
  TH1D* hledspill_x = hledspill->ProjectionX("hledspill_x");
  th1_check_overflow(hledspill_x);
  
  // TODO: generate per-channel amplitude vs. spill plots
  
  cout << "INFO: generating plots..." << endl;
  
  hled->LabelsDeflate("X");
  hled->LabelsOption("a v", "X");
  
  // mean amplitude vs. channels plot
  // (error bars are error of the mean)
  TProfile* hled_mean = hled->ProfileX("hled_mean");
  for (int i = 1; i <= nch; i++) hled_mean->GetXaxis()->SetBinLabel(i, labels[i-1]);
  
  // same plot, error bars are stddev
  TProfile* hled_meanstddev = (TProfile*) hled_mean->Clone("hled_meanstddev");
  hled_meanstddev->SetErrorOption("s");
  
  // stddev vs. channel plot
  // TODO/NOTE: errors are zero
  TH1D* hled_stddev = hled_meanstddev->ProjectionX("hled_stddev", "C=E");
  hled_stddev->SetOption("P");
  hled_stddev->SetMarkerStyle(kFullDotMedium);
  
  // compute stddev/mean vs. channel plot
  // TODO: check errors computed properly
  TH1D* hled_mean_copy = hled_mean->ProjectionX("hled_mean_copy");
  //hled_mean_copy->SetDirectory(0);
  TH1D* hled_stddevrel = (TH1D*) hled_stddev->Clone("hled_stddevrel");
  hled_stddevrel->Divide(hled_mean_copy);
  
  hledspill->LabelsDeflate("Y");
  hledspill->LabelsOption("a v", "Y");
  
  // projection: axis X is channel name, axis Y is spill number
  TProfile2D* hledspill_proj = hledspill->Project3DProfile("xy");
  hledspill_proj->SetTitle("LED mean amplitudes;channel;spill;mean amplitude");
  hledspill_proj->SetOption("LEGO2 0 FB BB");
  hledspill_proj->SetStats(kFALSE);
  for (int i = 1; i <= nch; i++) hledspill_proj->GetXaxis()->SetBinLabel(i, labels[i-1]);
  
  
  cout << "INFO: generating output files..." << endl;
  
  // === write .led file with mean values
  TString fname;
  fname.Form("ledinfo%d.led", runnow);
  cout << "INFO: output file: " << fname << endl;
  
  ofstream fled(fname);
  
  TAxis* a = hled_mean->GetXaxis();
  TString det0;
  for (int i = 1; i <= a->GetNbins(); ++i) {
    const TString name = a->GetBinLabel(i);
    const TString det = detname(name);
    //cout << name << endl;
    
    if (det0 != det) {
      fled << endl;
      det0 = det;
    }
    
    //TH1D* pr = hled->ProjectionY(name, i, i, "");
    //pr->SetTitle("Channel " + name);
    
    const double nentr = hled_mean->GetBinEntries(i);
    const double mean = hled_mean->GetBinContent(i);
    const double stddev = hled_mean->GetBinError(i);
    const double meanerr = stddev / sqrt(nentr);
    /*
    cout << "  " << det << ":" << name
         << " n=" << nentr
         << " mean=" << mean
         << " stddev=" << stddev
         << " meanerror=" << meanerr
         << endl;
    */
    
    fled << mean << " ";
  }
  
  fled << endl;
  fled.close();
  
  
  
  // === write .led file with per-spill mean values
  fname.Form("ledinfospill%d.led", runnow);
  cout << "INFO: output file: " << fname << endl;
  
  ofstream fled2(fname);
  
  TAxis* ay = hledspill_proj->GetYaxis(); // spills
  TAxis* ax = hledspill_proj->GetXaxis(); // channels
  
  for (int iy = 1; iy <= ay->GetNbins(); ++iy) {
    
    // skip absent spills
    int nentries = 0;
    for (int ix = 1; ix <= ax->GetNbins(); ++ix) {
      const int binid = hledspill_proj->GetBin(ix, iy);
      const double anum = hledspill_proj->GetBinEntries(binid);
      nentries += anum;
    }
    if (nentries == 0) continue;
    
    const int spill = ay->GetBinLowEdge(iy);
    fled2 << spill << " ";
    
    for (int ix = 1; ix <= ax->GetNbins(); ++ix) {
      const TString name = ax->GetBinLabel(ix);
      //cout << name << endl;
      
      const int binid = hledspill_proj->GetBin(ix, iy);
      const double anum  = hledspill_proj->GetBinEntries(binid);
      const double amean  = hledspill_proj->GetBinContent(binid);
      const double ameanerr = hledspill_proj->GetBinError(binid);
      /*
      cout << "  spill=" << spill
           << " " << name
           << " n=" << anum
           << " mean=" << amean
           << " error=" << ameanerr
           << endl;
      */
      
      fled2 << amean << " ";
    }
    fled2 << endl;
  }
  
  fled2.close();
  
  // write .root with all histograms
  cout << "INFO: output file: " << fh.GetName() << endl;
  fh.Write();
  fh.Close();
  
  return 0;
}
