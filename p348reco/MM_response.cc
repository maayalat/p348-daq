// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
using namespace CS;

// ROOT
#include "TProfile.h"
#include "TString.h"
#include "TFile.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TStyle.h"
#include "TH2.h"
#include "TF1.h"
#include "TTree.h"
#include <vector>
#include <vector>
#include <TH1F.h>
#include <TH2F.h>
#include <TCanvas.h>


// c++
#include <iostream>
#include <string>
using namespace std;

// P348 reco
#include "p348reco.h"
#include "tracking_new.h"

struct TrackHisto
{
  std::string name;
  TH1D* mom;
  TH1D* chi2;
  TH1D* anglein;
  TH1D* angleout;
  TH1D* angleinout;

  void Init(std::string _name)
  {
    TString title(_name);

    mom = new TH1D(title+"_mom",
        "Momentum reconstructed for " + title,
        600.,-0.5,199.5
        );

    chi2 = new TH1D(title+"_chi2",
        "#chi^{2} of reconstructed momentum for " + title,
        600.,-0.5,199.5
        );

    anglein = new TH1D(title+"_angleZX",
        "Angle In for " + title,
        400,-0.5,99.5
        );

    angleout = new TH1D(title+"_angleout",
        "Angle Out for " + title,
        400,-0.5,99.5
        );

    angleinout = new TH1D(title+"_angleinout",
        "Angle In-Out for " + title,
        400,-0.5,99.5
        );

    std::cout << "Initialized TrackHisto with name: " << name << "\n";
  }

  void Fill(const std::vector<Track_t>& tracks)
  {
    if(tracks.empty())
      return;

    mom->Fill(tracks.at(0).momentum);
    mom->Fill(tracks.at(0).chisquare);
    // in mrad
    const double rad2mrad = 1000;
    anglein   ->Fill(rad2mrad * tracks.at(0).in.theta());
    angleout  ->Fill(rad2mrad * tracks.at(0).out.theta());
    angleinout->Fill(rad2mrad * tracks.at(0).in_out_angle());
  }

  void FitMom(const double r1 = 80., const double r2 = 120.)
  {
    //define double gaussian with shared mean
    TF1* dgaus = new TF1("dgaus", "[0]*TMath::Gaus(x, [1],[2]) + [3]*TMath::Gaus(x,[1],[4])",r1,r2);
    //define paramters
    dgaus->SetParameter(0,       mom->GetBinContent(mom->GetMaximumBin()));
    dgaus->SetParameter(1,       mom->GetBinCenter(mom->GetMaximumBin()));
    dgaus->SetParameter(2,       mom->GetStdDev());
    dgaus->SetParameter(3,       mom->GetBinContent(mom->GetMaximumBin()) / 4);
    dgaus->SetParameter(4,   4 * mom->GetStdDev());
    dgaus->SetParNames("Norm1","Momentum","MomRes","Norm2","MomResTail");
    mom->Fit("dgaus","QM+","",r1,r2);
    gStyle->SetOptFit();
  }

  TrackHisto()
    :
      name(""),
      mom(0),chi2(0),anglein(0),angleout(0),angleinout(0)
  {
    Init(name);
  }

  TrackHisto(std::string _name)
    :
      name(_name),
      mom(0),chi2(0),anglein(0),angleout(0),angleinout(0)
  {
    //initialize histogram
    Init(name);
  }

};

struct MM_histo_plane
{
  TH1D* clus_time;
  TH2D* strip_time;
  TH2D* time_charge;
  TH2D* strip_charge;
  TH2D* chan_charge;
  TH2D* clus_pos_time;
  TH2D* clus_time_charge;
  TH2D* clus_time_index;
  TH2D* clus_charge_index;
  TH2D* clus_pos_index;
  TH1D* mult;
  TH1D* clussize;


  MM_histo_plane()
    :
      clus_time(0),
      strip_time(0),
      time_charge(0),
      strip_charge(0),
      chan_charge(0),
      clus_pos_time(0),
      clus_time_charge(0),
      clus_time_index(0),
      clus_charge_index(0),
      clus_pos_index(0),
      mult(0),
      clussize(0)
  {}

  void Init(const char* _name, const MM_plane_calib& calib)
  {
    std::cout << "MESSAGE: initializing plane: " << _name << "\n";
    const unsigned int Nchan     = calib.MM_Nchannels;
    const unsigned int Nstrip    = calib.MM_Nstrips;
    clus_time     = new TH1D(TString::Format("%s_clus_time", _name),
        TString::Format("cluster time in %s; # Time (ns)", _name),
        250, -500, 500);
    strip_time    = new TH2D(TString::Format("%s_strip_time",_name),
        TString::Format("strip vs time in %s; # Strip; Time (ns)", _name),
        Nstrip,-0.5,Nstrip-0.5,
        250,-500,500);
    time_charge    = new TH2D(TString::Format("%s_time_charge",_name),
        TString::Format("time vs max charge in %s; Time (ns); Charge", _name),
        250,-500,500,
        250,0,1000);
    strip_charge  = new TH2D(TString::Format("%s_strip_charge",_name),
        TString::Format(" strips vs charge in %s; # Strip; Charge", _name),
        Nstrip,-0.5,Nstrip-0.5,
        250,0,1000);
    chan_charge  = new TH2D(TString::Format("%s_chan_charge",_name),
        TString::Format(" chan vs charge in %s; # Channel; Charge", _name),
        Nchan,-0.5,Nchan-0.5,
        250,0,1000);
    clus_pos_time    = new TH2D(TString::Format("%s_clus_pos_time",_name),
        TString::Format("Position in strip plane vs time of cluster in %s; C.O.M. Strip; Time (ns)", _name),
        Nstrip,-0.5,Nstrip-0.5,
        500,-500,500);
    clus_time_charge    = new TH2D(TString::Format("%s_clus_time_charge",_name),
        TString::Format("Time vs total charge of cluster in %s; Time (ns); Charge", _name),
        500,-500,500,
        500,0,30000);
    clus_time_index    = new TH2D(TString::Format("%s_clus_time_index",_name),
        TString::Format("Time vs index of cluster in %s; Time (ns); index", _name),
        500,-500,500,
        10,-0.5,9.5);
    clus_charge_index    = new TH2D(TString::Format("%s_clus_charge_index",_name),
        TString::Format("Charge vs index of cluster in %s; Charge; index", _name),
        500,0,30000,
        10,-0.5,9.5);
    clus_pos_index    = new TH2D(TString::Format("%s_clus_pos_index",_name),
        TString::Format("Position in strip plane vs index of cluster in %s; C.O.M. Strip; index", _name),
        Nstrip,-0.5,Nstrip-0.5,
        10,-0.5,9.5);
    mult          = new TH1D(TString::Format("%s_mult",_name),
        TString::Format("Number of clusters in %s ; NClusters", _name),
        30,-0.5,29.5);
    clussize      = new TH1D(TString::Format("%s_clussize",_name),
        TString::Format("Size of clusters in %s ; NStrips", _name),
        30,-0.5,29.5);
  }

  void Fill(const MicromegaPlane& plane)
  {
    //CHANNEL
    for(size_t i = 0; i < plane.calib->MM_Nchannels; i++)
      chan_charge->Fill(i, plane.strips[(plane.calib->MM_map.at(i))[0]].charge); //map channel to strip
    //STRIPS
    for(size_t i = 0; i < plane.clusters.size(); i++)
    {
      for(auto strip : plane.clusters[i].strips)
      {
        strip_time->Fill(strip.bin,strip.time());
        strip_charge->Fill(strip.bin,strip.charge);
        time_charge->Fill(strip.time(),strip.charge);
      }
      clus_time->Fill(plane.clusters[i].cluster_time());
      clus_pos_time->Fill(plane.clusters[i].position(), plane.clusters[i].cluster_time());
      clus_time_charge->Fill(plane.clusters[i].cluster_time(), plane.clusters[i].charge_total);
      clus_time_index->Fill(plane.clusters[i].cluster_time(), i);
      clus_charge_index->Fill(plane.clusters[i].charge_total, i);
      clus_pos_index->Fill(plane.clusters[i].position(), i);
    }
    mult->Fill(plane.clusters.size());
    clussize->Fill(plane.clusters[plane.bestClusterIndex()].size());
  }

};

struct MM_histo
{
  //name
  const char* name;
  bool IsInitialized;
  TFile* out;
  //general MM profile
  TH2D* profile;
  TH2D* profile_global;

  MM_histo_plane xplane;
  MM_histo_plane yplane;

  MM_histo()
    :
      name("dummy"),
      IsInitialized(false),
      out(0),
      profile(0),
      profile_global(0)
  {}

  void SetOutFile(TFile* file)
  {
    if(file)
      out = file;
    else
    {
      std::cerr << "invalid file, not initialized \n";
    }
  }



  void Init(const MM_calib* calib)
  {
    const unsigned int XNchan     = calib->xcalib.MM_Nchannels;
    const unsigned int XNstrip    = calib->xcalib.MM_Nstrips;
    const unsigned int Xstripstep = calib->xcalib.MM_strip_step;
    const unsigned int YNchan     = calib->ycalib.MM_Nchannels;
    const unsigned int YNstrip    = calib->ycalib.MM_Nstrips;
    const unsigned int Ystripstep = calib->ycalib.MM_strip_step;
    name = calib->name.c_str();
    if(out){out->mkdir(name);out->cd(name);}
    profile = new TH2D(TString::Format("%s_profile",name),
        TString::Format(" XY profile in %s; X [mm]; Y [mm]", name),
        XNstrip, -XNstrip * Xstripstep / 2, XNstrip * Xstripstep / 2,
        YNstrip, -YNstrip * Ystripstep / 2, YNstrip * Ystripstep / 2
        );
    profile_global = new TH2D(TString::Format("%s_profile_global",name),
        TString::Format(" XY profile in %s in global coordinate; X [mm]; Y [mm]", name),
        1200, -600, 600,
        400,  -200, 200
        );
    TString planenameX(name),planenameY(name),dirname(name);
    planenameX+="X";
    planenameY+="Y";
    dirname+="/";
    if(out){
      out->mkdir(dirname+planenameX);out->cd(dirname+planenameX);
    }
    xplane.Init(planenameX.Data(), calib->xcalib);

    if(out){
      out->mkdir(dirname+planenameY);out->cd(dirname+planenameY);
    }
    yplane.Init(planenameY.Data(), calib->ycalib);
    //finalize
    if(out)out->cd();
    IsInitialized = true;
  }

  void Fill(const Micromega* mm)
  {
    if(!IsInitialized)
    {
      std::cout << "initialize histogram collection with: " << mm->calib->name << "\n";
      Init(mm->calib);
    }

    if(!mm->hasHit())
      return;

    //fill
    profile->Fill(mm->xpos, mm->ypos);
    profile_global->Fill(mm->abspos().X(), mm->abspos().Y());
    xplane.Fill(mm->xplane);
    yplane.Fill(mm->yplane);
  }

  //write projection
  void WriteProj(const bool fit = false)
  {
    TString planenameX(name),planenameY(name),dirname(name);
    planenameX+="X";
    planenameY+="Y";
    dirname+="/";

    if(out)out->cd(dirname+planenameX);
    TH1D* px =  profile->ProjectionX();
    TH1D* pxg = profile_global->ProjectionX();
    if(fit){px->Fit("gaus","Q");pxg->Fit("gaus","Q");gStyle->SetOptFit(1);}

    if(out)out->cd(dirname+planenameY);
    TH1D* py =  profile->ProjectionY();
    TH1D* pyg = profile_global->ProjectionY();
    if(fit){py->Fit("gaus","Q");pyg->Fit("gaus","Q");gStyle->SetOptFit(1);}

    if(out)out->cd();
  }
};

int main(int argc, char *argv[])
{
  if (argc < 2) {
    cerr << "Usage: ./MM_response.exe file1 [file2 ...]" << endl;
    return 1;
  }

  int event;
  const unsigned int NMM = 11;
  std::string file(argv[1]);
  std::size_t found_run = file.find("1001-0");
  std::size_t found_dotroot = file.find(".dat");
  std::string run_name = file.substr(found_run,found_dotroot-found_run);
  std::string path = file.substr(0,found_run);

  std::string new_file = "run"+run_name+"_MM_response.root";

  TFile* file_new = new TFile(new_file.c_str(),"RECREATE");


  DaqEventsManager manager;
  manager.SetMapsDir("../maps");
  for (int i = 1; i < argc; ++i)
    manager.AddDataSource(argv[i]);
  manager.Print();

  std::vector<MM_histo*> histvec;

#ifdef TRACKINGTOOLS
  unsigned int kRUN = 0;
  ConfigFileParser parser;
  parser.SetConfigFile(conddbpath() + "trackingtools/2022elec/config.json");
  std::vector<Track_t> tracks;
  std::vector<std::pair<TVector3, double> > hits;
#endif

  //histogram for momentum
  file_new->mkdir("tracking");file_new->cd("tracking");
  TrackHisto* MMall = new TrackHisto("all");
  TrackHisto* MM3   = new TrackHisto("MM3");
  TrackHisto* MM4   = new TrackHisto("MM4");
  TrackHisto* MM5   = new TrackHisto("MM5");
  TrackHisto* MM6   = new TrackHisto("MM6");
  file_new->cd();

  for(unsigned int i(0); i < NMM; ++i){
    histvec.push_back(new MM_histo);
    histvec[i]->SetOutFile(file_new);
  }


  // show underflow/overflow on plots
  gStyle->SetOptStat("emruo");


  while (manager.ReadEvent()) {


    const int nevt= manager.GetEventsCounter();

    event=manager.GetEventsCounter();

    if(nevt%1000==1) cout << "Run "<<manager.GetEvent().GetRunNumber()<<"===> Event #" << manager.GetEventsCounter() << endl;
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();

    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }

#ifdef TRACKINGTOOLS
    if (kRUN != manager.GetEvent1Run().GetRun()) {
      kRUN = manager.GetEvent1Run().GetRun();
      InitTracking(kRUN);
    }
#endif

    // run reconstruction
    const RecoEvent e = RunP348Reco(manager);

    // process only "physics" events (no random or calibration trigger)
    if (!e.isPhysics) continue;

    for(unsigned int i(1); i <= NMM; ++i)
    {
      const Micromega* mm;
      switch(i)
      {
        case 1:
          mm = &e.MM1;
          break;
        case 2:
          mm = &e.MM2;
          break;
        case 3:
          mm = &e.MM3;
          break;
        case 4:
          mm = &e.MM4;
          break;
        case 5:
          mm = &e.MM5;
          break;
        case 6:
          mm = &e.MM6;
          break;
        case 7:
          mm = &e.MM7;
          break;
        case 8:
          mm = &e.MM8;
          break;
        case 9:
          mm = &e.MM9;
          break;
        case 10:
          mm = &e.MM10;
          break;
        case 11:
          mm = &e.MM11;
          break;
        default:
          std::cerr << "no such Micromega implemented in the reco, defaulting to MM1 \n";
          mm = &e.MM1;
          break;
      }

      //lowering index by 1, just to match index of MM
      histvec[i-1]->Fill(mm);
    }

    //FILL MOMENTUM
    //complete momentum
#ifdef TRACKINGTOOLS
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM3, hits, parser);FillAbsMMHits(e.MM4, hits, parser);
    FillAbsMMHits(e.MM5, hits, parser);//FillAbsMMHits(e.MM6, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MMall->Fill(tracks);

    //MM3
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM3, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM3->Fill(tracks);

    //MM4
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM4, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM4->Fill(tracks);

    //MM5
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM5, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM5->Fill(tracks);

    //MM6
    tracks.clear();hits.clear();
    FillAbsMMHits(e.MM1, hits, parser);FillAbsMMHits(e.MM2, hits, parser);
    FillAbsMMHits(e.MM6, hits, parser);
    tracks = GetReconstructedTracksFromHits(hits);
    MM6->Fill(tracks);
#endif

    //if (nevt > 10000) break;
  }
  //}
for(auto p : histvec)p->WriteProj(1);
MMall->FitMom();MM3->FitMom();MM4->FitMom();MM5->FitMom();MM6->FitMom();
file_new->Write();
file_new->Close();

return 0;
}
