These are installation instructions for CORAL library.

============================================================

                    +--------------------+
                    | CMake installation |
                    +--------------------+

1) Change your directory to the same directory, where is
   located this README file. It assumes you already setup the common dependancies according to your needs.
   You also need at least cmake v3.0.

2) A minimal example to compile Coral with CMAKE is available in ./cmake/bootstrap.sh
   $ cp ./cmake/bootstrap.sh ./bootstrap.sh
   $ ./bootstrap.sh 1 # The 1-option is meant to execute "cmake" + "make" + "make install" commands.
                      # Without this option only "make"+"make install", or "cmake"+"make"+"make install", if first execution.
   $ source ./install/setup.sh # This command will define the CORAL environmental variables.

This command will prepare the build directory into "./build" and the install directory into "./install"
If you are willing to install Coral using cmake command yourself, you can look online how to install a cmake project
https://cmake.org/cmake/help/latest/guide/tutorial/index.html#build-and-test

3) Automatic detected features :
- CERNLIB; if CERNLIB environmental variable if defined and cernlib properly installed
- TGEANT; if TGEANT environmental variable is defined (e.g. by sourcing $TGEANT/thisgeant.sh)
- CLHEP; if Geant4 is installed and setup it will use the G4 system CLHEP, if not found it looks for the CLHEP (or also CLHEP_DIR) environmental variables
- PHAST; if PHAST is compiled and the PHAST environmental variable is found (such that $PHAST/phast is found) 
- MySQL/SQLite; If MYSQL/SQLITE environmental variables are found
- XROOTD; If the XROOTD (or also XROOTD_DIR) environmental variable is found, and XROOTD is properly installed.

NB: By default, CMAKE tries to enable as much features as possible. 
    The enabled features are show in the log output when you type a cmake command.
    Among the default enabled features; it includes event display using CERNLIB; 
    The new PHAST evdis, if librairies are compiled in PHAST; Alignment and MySQL binaries are also compiled (e.g.getDBFilePath) 
    Binaries are then all stored into ./install/bin/

4) Additional features:
- makeDico; enable with the cmake option -DALL=ON, e. g. in the bootstrap script in the cmake line. The executable will also be in INSTALL/bin. The required dicofit.dat file will be copied there, too, so that one can execute makeDico from there with a good option file.

5) If you just use TGEANT, you can disable some COMGEANT features. The following flags are tested by meyerma.
- ZEBRA; with -DZEBRA=NO
- HIGZ; with -DHIGZ=NO
- HBOOK; with -DHBOOK=NO
By default, all are on to provide backward compatibility. See ./cmake/Inc/LoadRequirements.cmake for more details.

============================================================

                    +-------------+
                    | Quick Start |
                    +-------------+

1) Change your directory to the same directory where is
   located this README file.

2) Run configure script
    $ ./configure
   Type ./configure --help to get list of available options.
   See also several configuration examples at the end of
   this file.

3) Update your environment variables
   a) for bash/ksh run
    $ source ./setup.sh
      Note "./"
   b) for csh/tcsh run
    $ source setup.csh

4) Compile CORAL
    $ make -j4

5) Run your program. For example:
    $ cd src/user
    $ make
    $ Linux/myprogram coral.options
   
6) You can insert your code to src/user/main.cc file.

7) If you wnat to produce mDST output CORAL has to be linked with PHAST
  see PHAST distribution phast/coral/README for howto.

============================================================

                 +-----------------------+
                 | Detailed instructions |
                 +-----------------------+

  Operation system requirements
  -----------------------------

 CORAL should work on any UNIX platform where are installed the following
programs:

   1. gcc     (GNU Compilers Collection, including C,C++,Fortran)
   2. gmake   (GNU make)

 We do not restrict users to work with a some particular versions of these
programs, we have checked that code works with

      gcc 3.4, gcc 4.1, gcc 4.4, gcc 4.7, gcc 4.9 gcc 6.2
      gcc 7.0, gcc 8.3 (without CERNLIB-based event display)

(On lxplus @CERN, the default gcc (and related software, such as ROOT) versions can be overwritten by e.g.

source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_94 x86_64-centos7-gcc62-opt (for gcc 6.2) or
source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96c_LS x86_64-centos7-gcc8-opt (for gcc 8.3) etc.

CORAL was actually tested on Linux operation system with the following
distributions:

   1. CERN SLC 4, CERN SLC 5, CERN SLC 6, CERN CentOS 7
   2. Debian 4.0


 **Special remarks**

 The statements that you need GNU tools 'gcc' and 'gmake' are too strong.
 We do not use any special tricks available in these tools, so very
 probably you _can_ compile CORAL with _any_ modern C/C++/Fortran compiler
 and with 'make' program available on your OS. But we _never_ checked this.

  The only system-dependent files used in CORAL are C++/Fortran interface.
  The rest of the code is system-independent, the CORAL is a standard
 C/C++/Fortran program.

  Libraries
  ---------
  
   CORAL uses several external libraries. Some of them are mandatory
  (CORAL will not compile and work without these libraries) and the others
  are optional (they will give you extra features and possibilities).
  
  a) Mandatory Libraries

     To compile CORAL you definitely need

     -- CERN library 
     -- CLHEP (Class Library for High Energy Physics)
     
     If you don't have these packages, install them first and then continue
     with the CORAL installation.

  b) Optional libraries

    -- ROOT         (The replacement of CERN library http://root.cern.ch)
       Allows:
       * ROOT histograms package usage
       * ROOT output of reconstructed events
       
       CORAL was tested with ROOT up to v. 6.18

     -- DATE library (the library from ALICE experiment for raw events)
       Allows reading real events from a data stream or a file.
       If you do not plan to work with real data, you can ignore this
      library.

  Getting CORAL
  -------------
  
   CORAL is under GIT control.
   To get local clone of repository:
   git clone https://<CERN username>@gitlab.cern.ch/compass/coral.git  
   or	
   git clone ssh://git@gitlab.cern.ch:7999/compass/coral.git 
   (Your SSH public key has to be saved in your gitlab.cern.ch profile settigs)

  Configuration
  -------------

   Default is expected to be OK on lxplus:
    $ ./configure    

   Run
    $ ./configure --help
   to get a list of options

   Several examples of alternative CORAL configurations:

   1) Enable gcc option `-Wparentheses'. It warns, among others, when
     operators are nested whose precedence people often get confused about.
     Which happens to be often the case in CORAL, as of 2013/09.
      If you want to benefit from this option, append it to the list of
     default CFLAGS (execute "./configure --help" to get that list).
    $ ./configure --with-CFLAGS='-O2 -DUSE_ObjectsCounter -Wparentheses'

   2) Enable gcc option `-Wcast-qual'. It's disabled for gcc version >= 4.7.
    $ ./configure --with-CFLAGS='-O2 -DUSE_ObjectsCounter -Wcast-qual'

   3) CORAL with _full_ optimisation:
    $ ./configure --with-CFLAGS='-O3 -DUSE_ObjectsCounter'

   4) Use shared libraries instead of the default: static ones:
    $ ./configure --enable-shared
      If you compile CORAL with the --enable-shared option, you will not be
     able to debug the code from CORAL libraries.

   5) You can specify exact path to a library with the configuration script.
     For example:
    $ ./configure --with-ROOT=/afs/cern.ch/compass/delivery/tools/root-3.00.06 \
                     --with-CLHEP=/usr/local
     That configuration option will overwrite the default procedure for ROOT
    and CLHEP versions checking via corresponding environment variables.

 **Special remarks**

  If your libraries (CLHEP, ROOT, ...) are compiled with gcc version vX.Y.Z,
 you must use the gcc compiler with the same major version numbers X and Y
 for CORAL compilation.

  All the above tricks are needed only if you have something non-standard on
 your machine. In most cases, you do not need to know them.

  Setting environment variables
  -----------------------------

  If the configuration procedure was successful, you will receive
 this message:
  ********************************************************************
  * Run 'source setup.csh' or '. setup.sh' to set env. variables     *
  * Type 'make' to compile the library                               *
  ********************************************************************
  According to this message you have to run one of the above scripts:
  a) If you use csh or tcsh, run
    $ source setup.csh
  b) If you use ksh or bash, run
    $ . setup.sh
       ^
     Please pay attention to the leading dot!!!
  c) You don't know what is your shell: execute 
    $ echo $SHELL

  Compilation
  -----------
  
  Just type 'make' and wait for 5-20 minutes.  The time depends on the
 hardware you are using.

  Parallel compilation greatly reduces compilation time:
    $ make -j12
	
  You can pass special options for MAKE program.  For example:
    $ make CFLAGS='-O3'

  For verbose compilation use:
    $ make VERBOSE=1

  The standard 'make' command will build only the libraries (CORAL,
 DaqDataDecoding and Reco).  To build everything (including tests, tools and
 examples) run 'make all'.

  
  Running the example program
  ---------------------------
  
  $ make user
  $ cd src/user
  $ Linux/myprogram trafdic.mc.<year>.opt
  
  This program (with the supplied options file) will reconstruct MC-events
 and draw them on an event display.

============================================================

                  +---------------+
                  | Troubleshoots |
                  +---------------+

  If something does not work, send an E-mail to coral-weekly@cern.ch
  Include the following information to your mail:
  a) GIT revision (8 first digits of SHA1), e.g. 470c503c
  b) Your CORAL configuration command line (it's stored in "config.log").
  c) The command and error message you got with it.


============================================================
