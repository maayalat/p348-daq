// --- Standard C/C++ library ---
#include <iomanip>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <sstream>

// // --- ROOT files ----
// // #include "TROOT.h"
// #include "TH1.h"
// #include "TH2.h"
// #include "TDirectory.h"
// #include "TCanvas.h"
// #include "TF1.h"
// 
// --- Internal files ----

//#include "myROOT_utilsMN.h"
//#include "ExceptionMN.h"

#include "StatInfoMN.h"
// To be removed??
//#include "AnyID.h"
//#include "Det.h"

//#include "Exception.h"

using namespace std;

namespace MN {

////////////////////////////////////////////////////////////////////////////////

StatInfoMN::StatInfoMN(void)
{
  Clear();
}

////////////////////////////////////////////////////////////////////////////////

StatInfoMN::StatInfoMN(double weights_sum, double mean, double sigma)
{
  momenta[0] = weights_sum;
  momenta[1] = weights_sum * mean;
  momenta[2] = weights_sum * (sigma*sigma+mean*mean);
}

////////////////////////////////////////////////////////////////////////////////

StatInfoMN::StatInfoMN(const std::vector <double> &vm)
{
  if( vm.size() == momenta_size )
  {
    for(size_t i=0; i<momenta_size; i++ )
      momenta[i] = vm[i];
  }
  else
  {
    Clear();
    cerr <<" Error !! in StatInfoMN::StatInfoMN momenta_size mismatch " << vm.size() <<" but " << momenta_size << " expected " << endl;
  }
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Clear(void)
{
  for(size_t i=0; i<momenta_size; i++ )
    momenta[i] = 0;
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Set(double weights_sum, double mean, double sigma)
{
  momenta[0] = weights_sum;
  momenta[1] = weights_sum * mean;
  momenta[2] = weights_sum * (sigma*sigma+mean*mean);
}

////////////////////////////////////////////////////////////////////////////////

double StatInfoMN::GetMean(void) const 
{ 
  if(momenta[0]!=0)
  { 
    return momenta[1]/momenta[0];
  }  
  else
  {
    return momenta[1];
  }  
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Add(double a, double weight)
{
  double mom=weight;
  for(size_t i=0; i<momenta_size; i++ )
  {
    momenta[i] += mom;
    mom *= a;
  }
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Add(const StatInfoMN &a, double weight)
{
  for(size_t i=0; i<momenta_size; i++ )
  {
    momenta[i] += a.GetMomenta(i)*weight;
  }
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Translate(double c)
{
  momenta[2] += 2.*momenta[1]*c+momenta[0]*c*c;
  momenta[1] += momenta[0]*c;
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Scale(double c)
{
  momenta[1] *= c;
  momenta[2] *= c*c;
}

////////////////////////////////////////////////////////////////////////////////

double StatInfoMN::GetSigma(void) const
{
  if(momenta[0]==0) return momenta[2];
  const double mean = GetMean();
  double f = 1.;
  if( GetEntries() > 1. ) f= GetEntries()/(GetEntries()-1.);
  return sqrt( f*fabs(momenta[2]/GetEntries() - mean*mean) );  // 'abs' used to overcome precison problems
}

////////////////////////////////////////////////////////////////////////////////

void StatInfoMN::Result(double &n,double &mean,double &sigma) const
{
  n = StatInfoMN::momenta[0];
  if( n==0 )
  {
     mean  =0;
     sigma =0;
     return;
  }
  mean  = GetMean();
  sigma = GetSigma();
}

////////////////////////////////////////////////////////////////////////////////

ostream &operator << (ostream &o, const StatInfoMN &c)
{
  char s[200];
  sprintf(s,"mean=%9.3e   sigma=%9.3e   N=mom0=%9.3e",
          c.GetMean(),c.GetSigma(),c.GetEntries());
  o << s;
  return o;
}

////////////////////////////////////////////////////////////////////////////////

istream & operator >> (istream &in, StatInfoMN &c)
{
  c.Clear();
  string s;
  char dummy[111];

  getline(in,s);

  size_t len;
  sscanf(s.c_str(),"%s %s %c %zu",dummy,dummy,dummy,&len);

  return in;
}

////////////////////////////////////////////////////////////////////////////////

StatInfoMN &StatInfoMN::operator = (const StatInfoMN &c)
{
  if( &c!=this )
  {
    momenta[0]  = c.momenta[0];
    momenta[1]  = c.momenta[1];
    momenta[2]  = c.momenta[2];
  }
  return *this;
}

////////////////////////////////////////////////////////////////////////////////

StatInfoMN &StatInfoMN::operator += (const StatInfoMN &c)
{
  momenta[0]  += c.momenta[0];
  momenta[1]  += c.momenta[1];
  momenta[2]  += c.momenta[2];
  return *this;
}

// ////////////////////////////////////////////////////////////////////////////////
// 
// void StoreDataEvents::AddEvent( DataEvent  * ev)
// {
//   if( ev == NULL ) return;
//   if( ev->data_.size() != data_size_ )
//   { 
//     cerr <<" StoreDataEvents::AddEvent " << GetName() <<" Not consistent data size " << ev->data_.size() <<" But expected " << data_size_ << endl;
//     return;
//   }
//   events_in_burst_[ ev->event_id_.SelectLayers(0,2) ].push_back(ev);
//   size_t run = ev->event_id_.GetLayer(0).GetIntID();
//   size_t burst = ev->event_id_.GetLayer(1).GetIntID();
//   if(run_min_ > run ) run_min_= run;
//   if(run_max_ < run ) run_max_ = run;
//   SetMaxBurstInfo( run, burst );
// 
// //    size_t run = ev->event_id_.GetRunNumber();
// //    size_t spill = ev->event_id_.GetBurstNumber();
// //    std::pair< size_t, size_t > indx( run, spill);
// //    events_in_burst_[indx].push_back(ev);
// // 
// //    if(run_min_ > run ) run_min_= run;
// //    if(run_max_ < run ) run_max_ = run;
// //    SetSpillsInfo( run, spill );
// // 
// //    vev_id_.push_back(ev->event_id_);
// //    if( vev_id_.size() == 1 ) ncells_ =ev->data_.size();
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// int StoreDataEvents::GetMaxBurstInfo( size_t run ) const
// {
//   std::map <  size_t, size_t > ::const_iterator it = max_burst_.find(run);
//   if( it !=   max_burst_.end() )
//     return (int)(it->second);
//   else 
//     return -1; 
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// std::vector< TH2D  * > &StoreDataEvents::GetPedTH2D( size_t run )
// {
//   std::map <  size_t, std::vector< TH2D  * > > ::iterator it = run_pedh2_.find(run);
//   if( it !=   run_pedh2_.end() )
//     return (it->second);
//   return dummy_; 
// }
// 
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// void  StoreDataEvents::SetMaxBurstInfo ( size_t run, size_t spill ) 
// {   
//    int maxburst = GetMaxBurstInfo( run );
//    if( maxburst == -1 )
//    {
//      max_burst_[run] = spill;
//    }
//    else
//    {
//      if(maxburst < (int)spill ) max_burst_[run] = spill;
//    }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StoreDataEvents::BookPedTH2D ( void )
// {
// //   bool debug = true;
//   bool debug = false;
//   if( debug ) cout <<" BookPedTH2D " << GetName() <<" Run Min " << run_min_ << " Run Max " << run_max_ <<" data size " <<  data_size_ << endl;
//   if( GetPedTH2D( run_min_ ).size() == data_size_ ) return;
// // Workaround to solve memory problem
//   size_t max_amp = 100.;
//   double hamp = double(max_amp)/2.;
//   char dir_name[132];
//   TDirectory *dir_save = gDirectory;
//   bool ok=(gDirectory->cd("/"));
//   if(!ok)
//   {
//     cerr <<"StoreDataEvents::BookPedTH2D Problem to change directory " <<"/" << endl;
//     exit(1);
//   }
//   sprintf(dir_name,"StoreData_%s",GetName().c_str());
//   TDirectory *root_dir = myROOT_utilsMN::TDirectory_checked(dir_name);
//   ok =root_dir->cd();
//   if(!ok)
//   {
//     cerr <<"StoreDataEvents::BookPedTH2D Problem to change directory " << root_dir << endl;
//     exit(1);
//   }
//   for ( size_t run=run_min_; run <= run_max_; run++ )
//   {
//     size_t max_burst = GetMaxBurstInfo(run);
//     std::vector < TH2D *> vh2;
//     for( size_t icell=0; icell<data_size_; icell++ )
//     {
//       char name[132];
//       char hist_name[132];
//       sprintf(name,"SpillPED_cell_%d_run_%d",(int)icell,(int)run);
//       sprintf(hist_name," PED in cell %d for run %d ",(int)icell,(int)run);
//       TH2D *h2 = NULL;
//       std::vector< StatInfoMN > &vpedstat = run_pedstat_[run];
//       double mean_cell_data = vpedstat[icell].GetMean();
// //       double stat_cell_data = vpedstat[icell].GetEntries();
// //       double sigma_cell_data = vpedstat[icell].GetSigma();
//       try
//       {
//         int minamp = (int)(mean_cell_data-hamp);
// 	int maxamp = (int)(mean_cell_data+hamp);
// 	int nch = maxamp-minamp+1;
//         if( debug ) cout <<" cell " << icell << " Mean " << mean_cell_data <<" Nch " << nch << endl;
// 	
//         h2 = myROOT_utilsMN::TH2D_checked( name,hist_name,nch,minamp-0.5,maxamp+0.5,max_burst,0.5,max_burst+0.5);
//       }
//       catch( std::exception &e )
//       {
//         cerr << "Exception:\n" << e.what() << "\n";
//         cerr << " StoreDataEvents::BookPedTH2D memory problem !! " << GetName() << " icell " << icell << endl;
// 	exit(1);
//       }
//       if( h2 == NULL )
//       {
//         cerr << " StoreDataEvents::BookPedTH2D memory problem !! " << GetName() << " icell " << icell << endl;
// 	exit(1);
//       }
//       vh2.push_back(h2);
//     }
//     run_pedh2_[run]=vh2;
//   }
//   dir_save->cd();
//   if( debug ) cout <<" BookPedTH2D " << GetName() <<" OK " << endl;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// const StatInfoMN  StoreDataEvents::GetValue( size_t run, size_t spill, size_t cell ) const
// {
//   StatInfoMN v(0.,0.,0.);
//   AnyID indx(run, spill);
//   std::map <  AnyID,  std::vector< DataEvent  * > >::const_iterator it = events_in_burst_.find(indx);
//   for( size_t iev=0; iev< it->second.size(); iev++ ) 
//   {
//     v.Add( it->second[iev]->data_[cell]);
//   }
//   return v;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StoreDataEvents::ProcessPedestals ( void )
// {
// //   bool debug = true;
//   bool debug = false;
//   if( data_processed_ )
//   {
//     cerr <<" StoreDataEvents::ProcessPedestals internal error Second Data Processing is forbiden !!! " << endl;
//     exit(1);
//   }
// 
// // run_pedstat_ initialization
//   for( size_t icell=0; icell<data_size_; icell++ ) 
//   {
//     pedstat_.push_back(StatInfoMN(0.,0.,0.));
//   }
// 
//   for ( size_t run=run_min_; run <= run_max_; run++ )
//   {
//     std::vector<StatInfoMN> v;
//     for( size_t icell=0; icell<data_size_; icell++ ) 
//     {
//       v.push_back(StatInfoMN(0.,0.,0.));
//     }
//     run_pedstat_[run]=v;
//   }
// 
//   for (std::map <  AnyID,  std::vector< DataEvent  * > >::const_iterator it=events_in_burst_.begin(); it!=events_in_burst_.end(); it++ )
//   {
//     size_t run = it->first.GetLayer(0).GetIntID();
//     std::vector< StatInfoMN > &vpedstat = run_pedstat_[run];
//     if(vpedstat.size() != data_size_ )
//     {
//       cerr <<" StoreDataEvents::ProcessPedestals internal error Bad Data Size  " <<vpedstat.size() << endl;
//       exit(1);
//     }
//     
//     if( debug ) cout <<" Run " << run << " Burst " << it->first.GetLayer(1)<<" Burst Max " << GetMaxBurstInfo(run) <<" NEvents "<< it->second.size() << endl;
//     for( size_t iev=0; iev<it->second.size(); iev++ ) 
//     {
//       for( size_t icell=0; icell<data_size_; icell++ ) 
//       {
//         vpedstat[icell].Add( it->second[iev]->data_[icell]);
//         pedstat_[icell].Add( it->second[iev]->data_[icell]);
//       }
//     }
//   }
// 
//   if( debug ) cout <<" Now StoreDataEvents::ProcessPedestals " << GetName() <<" Run Min " << run_min_ << " Run Max " << run_max_ <<  endl;
//   cout <<" TODO!! MakeOptional!! Warning! StoreDataEvents::ProcessPedestals " << GetName() <<" Pedestals histo commented due to huge memory consumption Please check is it make any harm for You. " << endl;
// //   BookPedTH2D ();
// //   
// //   for (std::map <  AnyID,  std::vector< DataEvent  * > >::const_iterator it=events_in_burst_.begin(); it!=events_in_burst_.end(); it++ )
// //   {
// //     size_t run = it->first.GetLayer(0).GetIntID();
// //     size_t burst = it->first.GetLayer(1).GetIntID();
// //     std::vector< TH2D  * > &vh2 = GetPedTH2D( run );
// //     if( debug ) cout <<" Run " << run << " Burst " << it->first.GetLayer(1)<<" Burst Max " << GetMaxBurstInfo(run) <<" NEvents "<< it->second.size() << endl;
// //     for( size_t iev=0; iev<it->second.size(); iev++ ) 
// //     {
// //       for( size_t icell=0; icell<data_size_; icell++ ) 
// //       {
// //         vh2[icell]->Fill( it->second[iev]->data_[icell], double(burst));
// //       }
// //     }
// //   }
// //   data_processed_ = true;
// //   if( debug ) cout <<" Now StoreDataEvents::ProcessPedestals " << GetName() <<" Finished OK " << endl;
// 
// 
// //   if( debug )
// //   {
// //     cout <<" Now StoreDataEvents::ProcessPedestals exit4debug " << endl;
// //     exit(0);
// //   }
// }
// 
// // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// // 
// // const StatInfoMN &  StoreDataEvents::GetValue( size_t run, size_t spill, size_t cell ) const
// // {
// //   AnyID indx( run, spill);
// //   std::map <  AnyID,  std::vector<StatInfoMN> > ::const_iterator it = burst_pedstat_.find(indx);
// //   if( it !=  burst_pedstat_.end() )
// //   {
// //     if( cell < it->second.size() )
// //       return it->second[cell];
// //     else  
// //       return zero; 
// //   } 
// //   else
// //    return zero; 
// // }
// // 
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// int StoreDataEvents::OutputPedestalsInfoInSpills( string &s, const string &comment ) const
// {
//  bool debug = false;
// //   bool debug = true;
//  if( det_ == NULL )
//  { 
//    cerr << " Error in StoreDataEvents::OutputPedestalsInfoInSpills rquested but det_ = NULL " << endl;
//    return -1; // We probably need some minor information from detector
//  } 
//   if( debug )
//   {
//     cout << " StoreDataEvents::OutputPedestalsInfoInSpills " << det_->GetName() << " debug " << endl;
//   }
// //   if( !store_leds_all_->Processed() ) 
// //   {
// //     cout << " ERROR in StoreLED::OutputLEDInfoInSpills " << det_->GetName() << " StoreCaloEvents store_leds_all_ was not Processed " << endl;
// //     return -1;
// //   }
//   if( debug )
//   {
//     cout << " StoreDataEvents::OutputPedestalsInfoInSpills " << det_->GetName() << " Start the output  " << endl;
//   }
//   std::ostringstream o;
//   if( comment == "") { 
//     o << "### " << std::endl;
//   } else {
//     o << comment.c_str() << std::endl;
//   }
//   o << "PEDs DetectorName: " << det_->GetName() << " Ncells=" << det_->NCellsMN() << " " << std::endl;
//   o << "  Cell      MeanPeak   SigmaPeak  Statistic  NormFlag  CellName " << std::endl;
// 
//   if( debug )
//   {
//      cout <<" PEDs DetectorName: " << det_->GetName() <<" Ncells= " << det_->NCellsMN() << endl;
//      cout <<"  Cell      MeanPeak   SigmaPeak  Statistic  NormFlag  CellName   " << endl;
//   }
// 
//   std::vector< StatInfoMN > vpedstat = GetStatInfoVector();
//   for( size_t icell=0; icell < det_->NCellsMN(); icell++ )
//   {
//     double mean_cell_data = vpedstat[icell].GetMean();
//     double stat_cell_data = vpedstat[icell].GetEntries();
//     double sigma_cell_data = vpedstat[icell].GetSigma();
//    o << " " << icell << " "
//       << std::fixed << std::setw(7) << std::setprecision(2) << mean_cell_data << " "
//       << std::fixed << std::setw(7) << std::setprecision(2) << sigma_cell_data << " "
//       << std::fixed << std::setw(7) << std::setprecision(2) << stat_cell_data << " "
//       << std::setw(5) << "   "<< det_->GetCellNameMN(icell) << " " << std::endl;
//     for ( size_t run=run_min_; run <= run_max_; run++ )
//     {
//       size_t max_burst = GetMaxBurstInfo(run);
//  //     o << " run " << run << " spills " << max_burst << " " << std::endl;
//       o << " run " << run << " spills " << max_burst-1 << " " << std::endl;// ???? Starts from Spill 2 ?????????????????????
//        int icnt20 = 0;
// //       for( unsigned ispill= 1; ispill <= max_burst; ispill++ )
//        for( unsigned ispill= 2; ispill <= max_burst; ispill++ ) // ???? Starts from Spill 2 ?????????????????????//
//        {
//          StatInfoMN peds_in_spill =  GetValue( run, ispill, icell );
// 
//           if( peds_in_spill.GetEntries() > 0 && peds_in_spill.GetMean() > 0. )
//           {
//         
//             double ddd = (peds_in_spill.GetMean() - mean_cell_data);
// //            double ddd = (peds_in_spill.GetMean());
// //            cout <<" " << int(ddd);
//             o << " " << int(ddd);
//           }
//           else
//           {
// //            cout <<" n " << endl;
//             o << "n ";
//           }
//           icnt20++;
//           if( icnt20 == 20 )
//           {
//             icnt20 = 0;
//             o << std::endl;
// //            cout << endl;   
//           }
//         }
//         if( icnt20 != 0 ) o << std::endl;
// //        cout << endl;
//     }
//   }
//   s = o.str();
//   
//   if( debug )
//   {
//     cout << " StoreDataEvents::OutputPedestalsInfoInSpills " << det_->GetName() << "  debug finished OK " << endl;
//   }
//   return 0;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMNStore::StatInfoMNStore(void) :
//   run_start_(0),
//   run_finish_(-1),
//   monitor_hist_(0),
//   min_range_(0),
//   min_stat_(1),
//   min_disp_add_(0),
//   min_disp_frac_(0), 
//   data_updated_(false) 
// {
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMNStore::StatInfoMNStore(int run_start, int run_finish) :
//   run_start_(run_start),
//   run_finish_(run_finish),
//   monitor_hist_(0),
//   min_range_(0),
//   min_stat_(1),
//   min_disp_add_(0),
//   min_disp_frac_(0), 
//   data_updated_(false) 
// {
//   Init();
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMNStore::StatInfoMNStore(int run_start, vector<StatInfoMN> &stat_info) :
//   run_start_(run_start),
//   stat_info_(stat_info),
//   monitor_hist_(0),
//   min_range_(0),
//   min_stat_(1),
//   min_disp_add_(0),
//   min_disp_frac_(0),
//   data_updated_(false) 
// {
//   run_finish_ = run_start + stat_info.size() - 1;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMNStore &StatInfoMNStore::operator = (const StatInfoMNStore &c)
// {
//   if( &c!=this )
//   {
//     run_start_    = c.run_start_;
//     run_finish_   = c.run_finish_;
//     stat_info_    = c.stat_info_;
//     points_       = c.points_;
//     for( int i=0; i < 100; i++)
//       parameters_[i] = c.parameters_[i];
//     monitor_hist_ = 0;  // to avoid memory leaks
//     min_range_    = c.min_range_;
//     min_stat_     = c.min_stat_;
//     min_disp_add_  = c.min_disp_add_;
//     min_disp_frac_ = c.min_disp_frac_;
//     data_updated_ = true;
//   }
//   return *this;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMNStore &StatInfoMNStore::operator += (const StatInfoMNStore &c)
// {
//   data_updated_ = true;
//   if( run_start_ == c.run_start_ && run_finish_ == c.run_finish_ )
//   {
//     for( int i = 0; i < Size(); i++)
//       stat_info_[i] += c.stat_info_[i];
//   }
//   return *this;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::Init( int run_start, int run_finish ) 
// { 
//   stat_info_.clear();
//   points_.clear();
//   run_start_ = run_start;
//   run_finish_ = run_finish;
//   if( run_finish_ >= run_start_ ) 
//   {
//     points_.insert( points_.end(), 0 );
//     points_.insert( points_.end(), Size() );
//     points_.sort();
//   }
//   Init();
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::Init( void ) 
// { 
//   for( int i=0; i < Size(); i++ )
//     stat_info_.push_back(StatInfoMN());
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::Clear(void)
// {
//   data_updated_ = true;
//   for( int i=0; i < Size(); i++)
//     stat_info_[i].Clear();
//   points_.clear();
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::SetIntervals(list<int> &points)
// {
//   points_.clear();
//   if( Size() <= 0 ) return;
//   for( list<int>::const_iterator it=points.begin(); it != points.end(); it++)
//   {
//      if( (*it) >= 0 && (*it) <= Size() )  points_.insert( points_.end(), *it );
//   }
//   points_.sort();
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::SetInfo(int run_start, vector<StatInfoMN> &stat_info)
// {
//   data_updated_ = true;
//   if( monitor_hist_ != 0 )
//   {
//     if( run_start_ != run_start || (int)stat_info.size() != Size() )
//     {
//       cerr << " ERROR in StatInfoMNStore::SetInfo  new DATA but old Histos !!!! " << endl;
//       assert(false); 
//     }
//   }
//   run_start_= run_start;
//   run_finish_ = run_start + stat_info.size() - 1;
//   stat_info_ = stat_info;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::SetInfo(int run, StatInfoMN &stat_info)
// {
//   data_updated_ = true;
//   if( run < run_start_ || run > run_finish_ ) return;
//   int bin = run - run_start_;
//   if( (unsigned)bin >= stat_info_.size() ) assert(false);
//   stat_info_[bin] = stat_info;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::Add(int run, StatInfoMN &stat_info)
// {
//   data_updated_ = true;
//   if( run < run_start_ || run > run_finish_ ) return;
//   int bin = run - run_start_;
//   if( (unsigned)bin >= stat_info_.size() ) assert(false);
//   stat_info_[bin] += stat_info;
// }
// 
// /////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::Add(int run, double data)
// {
//   data_updated_ = true;
//   if( run < run_start_ || run > run_finish_ ) return;
//   int bin = run - run_start_;
//   if( (unsigned)bin >= stat_info_.size() ) assert(false);
//   stat_info_[bin].Add(data);
// }
// 
// ///////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN StatInfoMNStore::CalculateAverage(void)
// {
//   StatInfoMN average;
//   for( size_t i=0; i < stat_info_.size(); i++ )
//   {
//     if(stat_info_[i].GetEntries() >= min_stat_ )
//       average += stat_info_[i];
//   }
//   return average;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN StatInfoMNStore::CalculateAverageInInterval( int run )
// {
//   if(points_.empty())
//   {
//     cerr << " ERROR:: StatInfoMNStore seems not initialsed points_.empty() !! " << endl;
//     if( RunInRange( run ) )
//     {
//       return CalculateAverage();
//     }
//     else
//     {
//       StatInfoMN empty;
//       return empty;
//     }
//   }
// 
//   list<int>::const_iterator it;
//   list<int>::const_iterator it_previous;
//   int bin = run - run_start_;
//   int range_min = 0;
//   int range_max = -1;
//   for( it=points_.begin(); it != points_.end(); it++)
//   {
//      if( (*it) > bin )
//      {
//        range_max = (*it);
//        break;
//      }
//      it_previous = it;
//   }
//   if( it != points_.begin() )
//   {
//     range_min = (*it_previous);
//     return CalculateAverage( range_min + run_start_, range_max + run_start_ - 1 );
//   }
//   else
//   { 
//     StatInfoMN empty;
//     return empty;
//   }
//   
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN StatInfoMNStore::CalculateAverage( int run_min, int run_max )
// {
//   StatInfoMN average;
//   int bin_min = run_min - run_start_;
//   int bin_max = run_max - run_start_;
//   
//   if( bin_max < bin_min ) return average;
//   for( int i = bin_min; i <= bin_max; i++ )
//   {
//     if( i >= 0 && i < Size() )
//     {
//       if( stat_info_[i].GetEntries() >= min_stat_ )
//       {
//         average += stat_info_[i];
//       }
//     }
//   }
//   return average;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// double StatInfoMNStore::EstimateSigma(int from_bin, int to_bin )
// {
//   StatInfoMN delta;
//   int min = from_bin;
//   int max = to_bin;
//   if(min >= max) return 0;
//   if( min < 0 ) min=0;
//   if( max > (int)stat_info_.size()-1 ) max=stat_info_.size()-1;
// 
//   for( int i=min; i < max; i++ )
//   {
//     if(stat_info_[i].GetEntries() >= min_stat_  && stat_info_[i+1].GetEntries() >= min_stat_)
//     {
//       double d = stat_info_[i].GetMean() - stat_info_[i+1].GetMean();
//       delta.Add( d * d );
//     }
//   }
//   return sqrt( delta.GetMean()/2. );
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN StatInfoMNStore::MakeRelativeNormalizationToAverage(void)
// {
//   StatInfoMN average = CalculateAverage();
//   if( average.GetMean() == 0 ) assert(false);
//   for( size_t i=0; i!=stat_info_.size(); i++ )
//   {
//     stat_info_[i].Translate(-average.GetMean());
//     stat_info_[i].Scale(1./average.GetMean());
//   } 
//   return average;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::FindIntervals(int max_intervals,double hi_break,double hi_penalty )
// {
//   list<int> lpoints;
//   lpoints.insert(lpoints.end(),0);
//   lpoints.insert(lpoints.end(),stat_info_.size());
//   double params[200];
//   double hi0 = FitInIntervals( lpoints, params );
// //   cout << " hi0 " << hi0;
//   list<int> lpoints1 = lpoints;
//   list<int> lpoints2 = lpoints;
//   double hi,hi1;
//   hi = hi0;
//   for(int i=0; i< max_intervals; i++)
//   {
//     if(lpoints1.size() > (unsigned)maximum_intervals) break;
//     hi1 = AddNewPoint( lpoints1, params, hi_penalty );
//     if( hi - hi1 < hi_break ) break;
//     hi = hi1;
// //     cout << " hi" << i+1 << " " << hi1;
// //    hi2 = AddNewInterval( lpoints, params, hi_penalty );
//   }
// //   cout << endl;
//   points_ = lpoints1;
//   for(int i=0; i<200; i++)
//     parameters_[i] = params[i];
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// double StatInfoMNStore::FitInInterval(int from_bin,int to_bin,double *parameters)
// {
//   static const double eps = 1.e-10;
// //  double sigma_estimated = EstimateSigma( from_bin, to_bin);
//   parameters[0]=0.;
//   parameters[1]=0.;
//   int min = from_bin;
//   int max = to_bin;
//   if(min >= max) return 0;
//   if( min < 0 ) min=0;
//   if( max > (int)stat_info_.size()-1 ) max=stat_info_.size()-1;
//   
//   double sum_weight=0.;
//   double sum_x=0.;
//   double sum_y=0.;
//   int nbin = max-min+1;
//   if(nbin < 1) return 0;
//   if(nbin == 1)
//   {
//     parameters[0]=stat_info_[min].GetMean();
//     return 0;
//   }
//   vector < double > ww(nbin);
//   vector < double > x (nbin);
//   vector < double > y (nbin);
//   
//   for( int it=0; it < nbin; it++)
//   {
//     double w = 0;
//     if(stat_info_[min+it].GetEntries() < min_stat_ )
//     {
//       y[it] = 0;
//       w=0;  // Set weight to zero; Will be ignored in fit
//     }
//     else
//     {
//       y[it] = stat_info_[min+it].GetMean();
//       double add_disp = min_disp_add_ + min_disp_frac_ * y[it] * y[it]; // add some constants to dispersion to be more robust    
// //      add_disp = 0.;
// //      w = stat_info_[min+it].GetSigma()/sqrt(stat_info_[min+it].GetEntries())+0.2*sigma_estimated;
//       w = stat_info_[min+it].GetSigma()/sqrt(stat_info_[min+it].GetEntries());
//       w = 1./(w*w+add_disp);
//     }
// 
//     x[it] = it;
//     ww[it] = w;
//     sum_weight += w;
//     sum_x += x[it]*w;
//     sum_y += y[it]*w;
//   }
//   
//   if(sum_weight < eps) return 0;
//   
//   double average_x = sum_x/sum_weight;
//   double average_y = sum_y/sum_weight;
//   
//   double sum_xx=0.;
//   double sum_xy=0.;
//   double sum_yy=0.;
// 
//   for( int it=0; it < nbin; it++)
//   {
//     double xx = x[it]-average_x;
//     double yy = y[it]-average_y;
//     sum_xx += xx*xx*ww[it];
//     sum_xy += xx*yy*ww[it];
//     sum_yy += yy*yy*ww[it];
//   }
//   
//   double a = sum_xy/sum_xx;
//   double b = a*( (double)(nbin-1)/2. - average_x ) + average_y;
//   parameters[0]=b;
//   parameters[1]=a;
//   return a*a*sum_xx - 2.*a*sum_xy + sum_yy;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// double StatInfoMNStore::FitInIntervals( list<int> &points, double *parameters )
// {
//   if(points.size() < 2) return 0;
//   double hi = 0;
//   list<int>::const_iterator it = points.begin();
//   for( int i = 0; i < (int)points.size()-1; i++)
//   {
//     int from_point = *it++;
//     int to_point = *it;
//     hi += FitInInterval( from_point, to_point - 1, &parameters[2*i]);
//   }
//   return hi;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// double StatInfoMNStore::AddNewPoint( list<int> &points, double *parameters, double penalty )
// {
//   if( points.size() < 2 ) assert(false);
//   int min = *points.begin(); 
//   int max = *( --points.end() );
//   double hi_best = FitInIntervals( points, parameters );
//   int ibest = -1; 
//   for( int i = min; i < max; i++)
//   {
//     list<int>::const_iterator it;
//     double hi_penalty = 0;
//     for( it = points.begin(); it != points.end(); it++)
//     {
//       if( fabs( double(*it-i) ) < min_range_ ) hi_penalty += penalty*(min_range_ - fabs( double(*it-i) ));
//     }
//     if( it != points.end()) continue;
// 
//     list<int> points_plus = points;    
//     points_plus.insert( points_plus.end(), i );
//     points_plus.sort();
//     double hi = FitInIntervals( points_plus, parameters ) + hi_penalty;
//     
//     if(hi < hi_best)
//     {
//       hi_best = hi;
//       ibest = i;
//     }
//   }
// 
//   if( ibest == -1)
//   {
// //     cerr << " StatInfoMNStore::AddNewPoint just no need to add new point to "<< points.size() <<
// //                                                                " hi_best " << hi_best  << endl;
//     return hi_best;  // possibly statistic is empty
//   }
//       
//   points.insert( points.end(), ibest );
//   points.sort();
//   return FitInIntervals( points, parameters );
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// double StatInfoMNStore::AddNewInterval( list<int> &points, double *parameters, double penalty )
// {
//   if( points.size() < 2 )  assert(false);
//   int min = *points.begin(); 
//   int max = *(--points.end());
//   double hi_best = FitInIntervals( points, parameters );
//   int ibest = -1; 
//   int jbest = -1;
//   for( int i = min; i < max-1; i++)
//   {
//     list<int>::const_iterator it;
//     for( it = points.begin(); it != points.end(); it++)
//     {
//       if( fabs( double(*it-i) ) < min_range_ ) break;
//     }
//     if( it != points.end()) continue;
//     for( int j = i+1; j < max; j++)
//     {
//       double hi_penalty = 0;
//       for( it = points.begin(); it != points.end(); it++)
//       {
//         if( fabs( double(*it-i) ) < min_range_ ) hi_penalty += penalty*(min_range_ - fabs( double(*it-i) ));
//         if( fabs( double(*it-j) ) < min_range_ ) hi_penalty += penalty*(min_range_ - fabs( double(*it-j) ));
//       }
//       list<int> points_plus = points;    
//       points_plus.insert(points_plus.end(),i);
//       points_plus.insert(points_plus.end(),j);
//       points_plus.sort();
//       double hi = FitInIntervals( points_plus, parameters ) + hi_penalty;
//       if(hi < hi_best)
//       {
//         hi_best = hi;
//         ibest = i;
//         jbest = j;
//       }
//     }
//   }
//   if( ibest == -1 || jbest == -1 )
//   {
// //     cerr << " StatInfoMNStore::AddNewInterval just no need to add new point to "<< points.size() <<
// //                                                                " hi_best " << hi_best  << endl;
//     return hi_best;  // possibly statistic is empty
//   }
//   points.insert(points.end(),ibest);
//   points.insert(points.end(),jbest);
//   points.sort();
//   return FitInIntervals( points, parameters );
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::BookHisto(const string &name, const string &histo_path, int histo_level )
// { 
//   if( Size() <= 0 ) return; // StatInfoMNStore has no data
//   TDirectory *dir_save=gDirectory; // Save current ROOT directory.
//   if( monitor_hist_ == 0 )
//   {    
//     monitor_hist_ = new Monitor_Histo;
//     Monitor_Histo* h = monitor_hist_;
//     if(!gDirectory->cd("/")) assert(false);
//     if(histo_path.empty())
//     {
//       h->root_dir = gDirectory;
//     }
//     else
//     {
//       h->root_dir=myROOT_utilsMN::TDirectory_checked(histo_path.c_str());
//     }
//     if(!h->root_dir->cd()) assert(false);
// 
//     char hist_name[132];
//     char hname[132];
//     if( histo_level >= 0 ) 
//     {
//       sprintf(hname,"%s",name.c_str());
//       sprintf(hist_name," Monitor value %s ", name.c_str() );
//       h->h1_monitor = myROOT_utilsMN::TH1D_checked(hname,hist_name,run_finish_-run_start_+1,run_start_-0.5,run_finish_+0.5);
//     }
//     if( histo_level > 0 ) 
//     {
//       sprintf(hname,"Norm_%s",name.c_str());
//       sprintf(hist_name," Monitor normalised %s ", name.c_str() );
//       h->h1_monitor_norm = myROOT_utilsMN::TH1D_checked(hname,hist_name,run_finish_-run_start_+1,run_start_-0.5,run_finish_+0.5);
//     } 
//     if( histo_level > 0 ) 
//     {
//       sprintf(hname,"Entries_%s",name.c_str());
//       sprintf(hist_name," Monitor entries %s ", name.c_str() );
//       h->h1_monitor_entries = myROOT_utilsMN::TH1D_checked(hname,hist_name,run_finish_-run_start_+1,run_start_-0.5,run_finish_+0.5);
//     } 
//     if( histo_level > 0 ) 
//     {
//       sprintf(hname,"Sigma_%s",name.c_str());
//       sprintf(hist_name," Monitor sigma %s ", name.c_str() );
//       h->h1_monitor_sigma = myROOT_utilsMN::TH1D_checked(hname,hist_name,run_finish_-run_start_+1,run_start_-0.5,run_finish_+0.5);
//     } 
//   }
//   if( !dir_save->cd() ) assert(false);
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::FillHisto(void)
// {
// //   bool debug = true;
//   bool debug = false;
//   if( monitor_hist_ == 0 ) return;
//   data_updated_ = false;
//   double sigma_estimated = EstimateSigma( 0, stat_info_.size()-1);
//   Monitor_Histo* h = monitor_hist_;
//   StatInfoMN average = CalculateAverage();
//   if( debug ) cout << " StatInfoMNStore::FillHisto size =" << stat_info_.size() << endl;
//   for( size_t i=0; i!=stat_info_.size(); i++ )
//   {
//     if( debug ) cout << " i =" << i << endl;
//     if( stat_info_[i].GetEntries() > min_stat_ )
//     {
//       double mean = stat_info_[i].GetMean(); 
//       if( debug ) cout << " mean =" << mean << " entries " << stat_info_[i].GetEntries() << " Sigma " << stat_info_[i].GetSigma() << endl;
//       double sigma = stat_info_[i].GetSigma()/sqrt(stat_info_[i].GetEntries()) + 0.2*sigma_estimated; 
//       double add_disp = min_disp_add_+ min_disp_frac_*(mean*mean); // add some constants to dispersion to be more robust
//       sigma = sqrt( sigma*sigma + add_disp );
//       if(h->h1_monitor != 0)
//         h->h1_monitor->SetBinContent( i, mean);
//       if(h->h1_monitor != 0)
//         h->h1_monitor->SetBinError( i, sigma);
//       double mean_average = average.GetMean();
//       if( debug ) cout << " mean_average =" << mean_average << " h1_monitor_norm = " << h->h1_monitor_norm << endl;
//       if(h->h1_monitor_norm != 0)
//         h->h1_monitor_norm->SetBinContent( i, mean/mean_average - 1.);
// 
//       if(h->h1_monitor_norm != 0)
//         h->h1_monitor_norm->SetBinError( i, sigma/mean_average );
// 
//       if( debug ) cout << " entries =" << stat_info_[i].GetEntries() << endl;
//       if(h->h1_monitor_entries != 0)
//         h->h1_monitor_entries->SetBinContent( i, stat_info_[i].GetEntries());
// 
//       if( debug ) cout << " sigma =" << stat_info_[i].GetEntries() << endl;
//       if(h->h1_monitor_sigma != 0)
//         h->h1_monitor_sigma->SetBinContent( i, stat_info_[i].GetSigma() );
//       if( debug ) cout << " sigma/sqrt(N) =" << endl;
//       if(h->h1_monitor_sigma != 0)
//         h->h1_monitor_sigma->SetBinError( i, stat_info_[i].GetSigma()/sqrt(stat_info_[i].GetEntries()));
//     }
//     if( debug ) cout << " i =" << i << " OK " <<  endl;
//   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::DrawHisto(void)
// {
//   if( Size() <= 0 ) return;
//   if( monitor_hist_ == 0 ) return;
//   Monitor_Histo* h = monitor_hist_;
//   if( data_updated_ ) FillHisto();
//   if( h->h1_monitor != NULL ) h->h1_monitor->Draw();
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::AddFitToHisto(void)
// {
//   if( monitor_hist_ == 0 ) return;
//   if( points_.size() < 2 ) return;
//   Monitor_Histo* h = monitor_hist_;
//   list<int>::const_iterator list_it = points_.begin();
//   for( size_t i=0; i < points_.size()-1; i++)
//   {
//     double min =run_start_+(*list_it++)-1;
//     double max =run_start_+(*list_it)-1;
// //kill    strstream hname;
//     char hname[132];
// //kill    hname.form("f_%d",i);
//     sprintf(hname,"f_%zu",i);
// //kill    TF1 f = TF1(hname.str(),"[0]+[1]*x",min,max);
//     TF1 f = TF1(hname,"[0]+[1]*x",min,max);
//     double x_middle = (max+min)/2.;
//     double b_inner = parameters_[i*2];
//     double a = parameters_[i*2+1];
//     double b = -a*x_middle + b_inner;
// //    f.FixParameter(0,b);
// //    f.FixParameter(1,a);
//     f.SetParameters(b,a);
// //kill    if( i == 0 ) h->h1_monitor->Fit(hname.str(),"QOR");
// //kill    else h->h1_monitor->Fit(hname.str(),"QOR+");
//     if( i == 0 ) h->h1_monitor->Fit(hname,"QOR");
//     else h->h1_monitor->Fit(hname,"QOR+");
//     const Int_t kNotDraw = 1<<9;
// //kill    h->h1_monitor->GetFunction(hname.str())->ResetBit(kNotDraw);
//     h->h1_monitor->GetFunction(hname)->ResetBit(kNotDraw);
//   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// double StatInfoMNStore::CalculateFitValue( int run )
// {
//   bool debug = false;
//   if(points_.empty()) return 0;
//   list<int>::const_iterator it;
//   list<int>::const_iterator it_previous;
//   int bin = run - run_start_;
// //  int range_min = 0;
//   int range_max = -1;
//   int interval = -1;
//   for( it=points_.begin(); it != points_.end(); it++)
//   {
//      if( (*it) > bin )
//      {
//        range_max = (*it);
//        break;
//      }
//      it_previous = it;
//      interval++;
//   }  
//   if( debug ) cout <<" StatInfoMNStore::CalculateFitValue range_max " << range_max << endl;
// 
//   if( it != points_.begin() )
//   {
//     double min = (*it_previous);
//     double max = (*it);
//     double x_middle = (max+min)/2.;
//     
//     double b_inner = parameters_[interval*2];
//     double a = parameters_[interval*2+1];
// //    cout << " interval " << interval << " a=" << a << " b=" << b_inner;
//     double value = a*( bin - x_middle ) + b_inner;
// //    cout << " Fit value " << value << " Measured value " << stat_info_[bin].GetMean();
//     return value;
//   }
//   else
//   {
//     return 0;
//   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// StatInfoMN StatInfoMNStore::GetBinValue( int run ) const
// {
//   int bin = run - run_start_;
//   return stat_info_[bin];
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// void StatInfoMNStore::Print( void )
// {
//   cout << " StatInfoMNStore::PrintOut for the Range: run_min = " << run_start_ << " -- run_max= " << run_finish_ << endl;
//   cout << " Total of " << NIntervals() << " of intervals " << endl;
//   list<int>::const_iterator it = points_.begin();
//   for(int i=0; i < NIntervals(); i++ )
//   {
//     cout << " From " << (*it++);
//     cout << " to " << (*it)-1 << endl;
//   }
// 
//   for( int run = run_start_; run <= run_finish_; run++ )
//   {
//     int bin = run - run_start_;
//     cout << " For run " << run << " Fit value=" << CalculateFitValue( run ) << " Measured value " << stat_info_[bin].GetMean() << endl;
//   }
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// ostream & operator << (ostream &o,const vector<MN::StatInfoMN> &v)
// {
//   o << "Cells amount = " << v.size() << endl;
//   o << " Cell      Mean     Sigma       WeightsSum " << endl;
//   for( size_t i=0; i<v.size(); i++ )
//   {
//     const MN::StatInfoMN &c=v[i];
//     o << i << c.GetMean() << c.GetSigma() << c.GetEntries();
//   }
//   return o;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// istream & operator >> (istream &in,vector<MN::StatInfoMN> &v)
// {
//   v.clear();
//   string s;
//   char dummy[111];
// 
//   getline(in,s);
// 
//   size_t len;
//   sscanf(s.c_str(),"%s %s %c %zu",dummy,dummy,dummy,&len);
// 
//   getline(in,s);
//   
//   while( getline(in,s) )
//   {
//     unsigned i;
//     float weight,mean,sigma;
//     sscanf(s.c_str(),"%u %g %g %g",&i,&mean,&sigma,&weight);
//     if( i!=v.size() )
//       throw MN::ExceptionMN("istream & operator >> (istream &in,vector<Reco::StatInfo> &v):  wrong cell number %d. I expect %d.",i,v.size());
//     v.push_back(MN::StatInfoMN(weight,mean,sigma));
// 
//   }
//   
//   if( v.size()!=len )
//     throw MN::ExceptionMN("istream & operator >> (istream &in,vector<Reco::StatInfo> &v):  bad size: I expect %d but I have read %d",
//                      len,v.size());
//   return in;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// #include <cmath>
// 
// void StatInfoMN::Test( double mled, double sled, int stat )
// {
//   bool debug = false;
//   double u1,u2,r1,r2;
//   u1=2*M_PI*drand48(), u2=sqrt(-2*log(drand48())), r1=sin(u1)*u2, r2=cos(u1)*u2;
//   StatInfoMN normal;
//   normal.Clear();
// //   StatInfoMN precise;
// //   precise.Clear(mled);
//   cout << " Check  " << " M = " << mled <<
//                  " S = " << sled <<
//                     " Stat = " << stat << endl;
//   for( int n=0; n<stat; n++ )
//   {
//     u1=2*M_PI*drand48(), u2=sqrt(-2*log(drand48())), r1=sin(u1)*u2, r2=cos(u1)*u2;
//     double led = mled + sled*r1;
//     if( debug )cout << "rndm  " << " r1 = " << r1 <<" r2 = " << r2 << endl;
//     normal.Add(led);
// //     precise.Add(led);
//   }
//   cout << " Normal  " << " M = " << normal.GetMean() <<
//                  " S = " << normal.GetSigma() <<
//                     " Stat = " << normal.GetEntries() << endl;
//   
// //   cout << " Precise  " << " M = " << precise.GetMean() <<
// //                  " S = " << precise.GetSigma() <<
// //                     " Stat = " << precise.GetEntries() << endl;
//   
// }
// 
// ////////////////////////////////////////////////////////////////////////////////

} // namespace MN

// ////////////////////////////////////////////////////////////////////////////////
// 
// ostream & operator << (ostream &o,const vector<MN::StatInfo> &v)
// {
//   o << " StatInfoMN size = " << v.size() << endl;
//   o << " index      Mean     Sigma       WeightsSum " << endl;
//   for( size_t i=0; i<v.size(); i++ )
//   {
//     const MN::StatInfoMN &c=v[i];
//     o << i << c.GetMean() << c.GetSigma() << c.GetEntries();
//   }
//   return o;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// istream & operator >> (istream &in,vector<MN::StatInfoMN> &v)
// {
//   v.clear();
//   string s;
//   char dummy[111];
// 
//   getline(in,s);
// 
//   size_t len;
//   sscanf(s.c_str(),"%s %s %c %zu",dummy,dummy,dummy,&len);
// 
//   getline(in,s);
//   
//   while( getline(in,s) )
//   {
//     unsigned i;
//     float weight,mean,sigma;
//     sscanf(s.c_str(),"%u %g %g %g",&i,&mean,&sigma,&weight);
//     if( i!=v.size() )
//       throw MN::ExceptionMN("istream & operator >> (istream &in,vector<Reco::StatInfo> &v):  wrong cell number %d. I expect %d.",i,v.size());
//     v.push_back(Reco::StatInfo(weight,mean,sigma));
// 
//   }
//   
//   if( v.size()!=len )
//     throw MN::ExceptionMN("istream & operator >> (istream &in,vector<Reco::StatInfo> &v):  bad size: I expect %d but I have read %d",
//                      len,v.size());
//   return in;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
