
#include <stdlib.h>
#include <stdio.h>

#include "DigSADC.h"

using namespace std;
using namespace MN;

#define CDB_LINE_MAX 132

namespace MN {

////////////////////////////////////////////////////////////////////////////////

DigSADC::DigSADC ( const int &type ) :
  type_(type),
  decode_version_(0),
  overflow_amplitude_(1023),
  tab_(0) 
{ 
  if( type_ == (int)DigSADC::COMPASS_SADC ) 
    overflow_amplitude_ = 1023;
  else if( type_ == (int)DigSADC::COMPASS_MSADC ) 
    overflow_amplitude_ = 4095;
  else 
  {
    cerr <<"  DigSADC::DigSADC unknown SADC type let it be FATAL at startup " << endl;
    overflow_amplitude_ = 4095;
    exit(1);
  }
  
// Important setting for Parameters for algorithms of signals extraction 
  par_sadc_ped_max_ = 4.; 
}

////////////////////////////////////////////////////////////////////////////////

void DigSADC::Clear ( void )
{
  csample_.clear();
  signals_.clear();
  over_.clear();
// reset monitoring info
  monitor_info_stored_=false;
  ped_odd_dynamic_=0.;
  ped_even_dynamic_=0.;
  saw_=0.;
    
}

////////////////////////////////////////////////////////////////////////////////

void DigSADC::PrintSample ( const std::vector<uint16> &sample )
{
  cout <<" Sample ";
  for( int i=0; i< (int)sample.size(); i++)
  {
    cout << sample[i] <<" ";
  }
  cout << endl;
}

////////////////////////////////////////////////////////////////////////////////

void DigSADC::PrintCSample ( const std::vector<double> &csample )
{
  cout <<" CSample ";
  for( int i=0; i< (int)csample.size(); i++)
  {
    cout << (int)csample[i] <<" ";
  }
  cout << endl;
}

////////////////////////////////////////////////////////////////////////////////

bool DigSADC::Fit(  const std::vector<uint16> &sample  )
{
//   bool debug = true;
  bool debug = false;
  if( debug ) cout <<" DigSADC::Fit DecodeVersion = " << GetDecodeVersion() << endl;
  bool ok = false; 
  if( GetDecodeVersion() >= 4 )
  {
    ok = FitGoGo(sample);
    return ok;
  }  
  else  
  {
    ok = FitPlusSomeMonitoring(sample);
//    cout <<" MonitorInfoIsStored " << MonitorInfoIsStored() << endl;
    return ok;
  } 
} 

////////////////////////////////////////////////////////////////////////////////

std::vector<double> DigSADC::ReorderALaSaw(  const std::vector<double> &csample ) 
{
  std::vector<double> csampltmp = csample;
// Simulate SAW shape
  for( int is=1; is < (int)csample.size(); is+=2 )
  {
    csampltmp[is-1] = csample[is];
    csampltmp[is] = csample[is-1];
  }
  return csampltmp;
} 

////////////////////////////////////////////////////////////////////////////////

bool DigSADC::FitPlusSomeMonitoring(  const std::vector<uint16> &sample ) 
{
  bool debug = false;
//   bool debug = true;
  bool print_signal = false;
  if( debug ) std::cout <<" DigitizerSADCdevel::FitPlusSomeMonitoring debug " << std::endl;
  Clear();
  if( sample.size() < 2)  return false;
  over_ = DetectOverflow(sample);
  if( debug ) std::cout <<" par_sadc_ped_max_ " << par_sadc_ped_max_ << " csample size " << csample_.size() << std::endl;
// Define base lines
  { 
    ped_odd_dynamic_ =0.;
    double stat_oddd =0.;
    for( int is=0; is <= par_sadc_ped_max_; is+=2 )
    {
      ped_odd_dynamic_ += sample[is];
      stat_oddd++;
    }
    if( stat_oddd > 0 ) ped_odd_dynamic_ /= stat_oddd;

    ped_even_dynamic_ =0.;
    double stat_evenn =0.;
    for( int is=1; is <= par_sadc_ped_max_; is+=2 )
    {
      ped_even_dynamic_ += sample[is];
      stat_evenn++;
    }
    if( stat_evenn > 0 ) ped_even_dynamic_ /= stat_evenn++;
  }
//  ped = (ped_odd_dynamic_+ped_even_dynamic_)/2.;

  // Subtract pedestals
  for( int is=0; is < (int)sample.size(); is++)
  {
    if( (is%2) > 0 )
      csample_.push_back( double(sample[is]) - ped_even_dynamic_ );
    else  
      csample_.push_back( double(sample[is]) - ped_odd_dynamic_ );
    if( debug ) cout << " " << (int)(10.*csample_.back() );  
  }
  if( debug ) cout << endl;  


  // Find maximum sample
  double signal = -100000.;
  int max_position = -1;
  for( int is=0; is < (int)sample.size(); is++)
  {
    if( csample_[is] > signal )
    {
      signal = csample_[is];
      max_position = is;
    }
  }
  if( debug ) cout <<" signal "<< signal << endl;
// Selection which trigger signals overlap in S2  
//  if( signal > 1500.) print_signal = true;
  if( print_signal )
  {
    PrintCSample( csample_);
  }
  
    
// Do we have more signals in the sample ??
  double signal_max_cut = 10.;
  double fraction_signal_max_cut = 0.03; // We do not apply shape yet. So there could be after pulses at some level
  if(signal_max_cut < signal*fraction_signal_max_cut) signal_max_cut=signal*fraction_signal_max_cut;
  
  std::vector < int > signalsmax;
  for( int is=0; is < (int)csample_.size(); is++)
  {
    signalsmax.push_back(-1);
  }
  std::vector < std::pair<int,double> > signals;
  signals.push_back( std::pair<int,double>(max_position,signal) );
  signalsmax[max_position] = signal;

  if( GetDecodeVersion() > 1 )  // Search more then one signal
  {
    while(true)
    {
      double cursignal = -100000.;
      int max_position_cur = -1;
      for( int is=0; is < (int)csample_.size(); is++)
      {
        if(csample_[is] < signal_max_cut ) continue;
        if(signalsmax[is] > 0 ) continue;
        if( csample_[is] > cursignal )
        {
          int il = is-1;
          double sigl =-100000.;
          if(il>=0) sigl=csample_[il];
          int ir = is+1;
          double sigr =-100000.;
          if(ir<(int)csample_.size() ) sigr=csample_[ir];
          if( csample_[is] > sigl && csample_[is] >= sigr )
          {
            cursignal = csample_[is];
            max_position_cur = is;
          }
        }
      }

      if( max_position_cur >= 0 )
      {
        signals.push_back( std::pair<int,double>(max_position_cur,cursignal) );
        signalsmax[max_position_cur] = cursignal;
        continue;
      }
      else
      {
        break;
      }
      cerr <<" Should never be here ! " << signals.size() << endl;
    }
  }



  if( debug ) cout <<" And Now we have NSignals = " << signals.size() << endl;
// But they are still not treated!

// No dedicated processing of the very maximum
  for( int i=0; i< (int)signals.size(); i++ )
  {
    double signal = signals[i].second;
    int max_position = signals[i].first;
    double signal_true = signal;
// S2 workaround if( signal_true > 1100.) signal_true=1400.;
// Time now in clocks
    std::pair<double, double> times = CalcTime2_halfMaxDigSADC(csample_,max_position, signal_true);

    if( debug ) cout <<" time1 "<< times.first <<" time2 "<< times.second << endl;
    double time = times.first; 
    FitPar par(signal, 1., time, 1., max_position, 2 );
    par.SetTime2(times.second);

// Add main signal
    signals_.push_back( par );
  }


// For SawShape monitoring
// For development (For recovery need pimary samples to reorder )
// std::vector<double> csampltmp = ReorderALaSaw( csample_ );

  double sd=0.;
  if( csample_.size() >= 4 )
  {
    for( int is=3; is < (int)csample_.size(); is+=2 )
    {
      double stp0 = csample_[is-2]-csample_[is-3];
      double stp1 = csample_[is]-csample_[is-1];
      double stp3 = (csample_[is]+csample_[is-1]-csample_[is-2]-csample_[is-3])/2.;
      double diff = stp3-stp0-stp1;
      if( stp3 >= 0 ) 
        sd += diff/2.;
      else
        sd -= diff/2.;   
    }
  }
// 1.5 Is just empirical to have parameter order of 1. in case of SAW-SHAPE
  if( signal > 0.) sd = sd/signal/1.5;
  saw_=sd;
  monitor_info_stored_= true;

// Multiply by SADC clock
  for( int i=0; i< (int)signals_.size(); i++ )
  {
    signals_[i].time  *= sadc_clock_;
    signals_[i].stime *= sadc_clock_;
    signals_[i].time2  *= sadc_clock_;
    signals_[i].stime2 *= sadc_clock_;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool DigSADC::CheckCalib(void) const
{
  bool pedloaded = false;
  bool tabloaded = false;
  if( GetTab()  != 0 ) tabloaded = true;
  if( calib_true_.size() == 3 )
  {
    pedloaded = true;
  }
  return (pedloaded && tabloaded);
}

////////////////////////////////////////////////////////////////////////////////

void DigSADC::ResetCalib(void) 
{
  calib_true_.clear();
}

////////////////////////////////////////////////////////////////////////////////

void DigSADC::SetCalib(double ped_odd, double ped_even) 
{
  if( calib_true_.size() >  0 )
  {
    cerr <<" Warning DigSADC::SetCalib " << calib_true_.size() <<" were set once already" << endl;
    ResetCalib();
  }
  calib_true_.push_back(ped_odd);
  calib_true_.push_back(ped_even);
  calib_true_.push_back((ped_odd+ped_even)/2.);
}


////////////////////////////////////////////////////////////////////////////////

std::vector< std::pair<int,int> > DigSADC::DetectOverflow ( const std::vector<uint16> &sample ) const
{
  std::vector< std::pair<int,int> > overflow_samples;
  unsigned int new_over_cnt(0);
  for( int is=0; is < (int)sample.size(); is++)
  {
    if( sample[is] >=  overflow_amplitude_ )
    { 
      if( new_over_cnt == 0 )
      {
        new_over_cnt++;
        overflow_samples.push_back(std::pair<int,int>(is,is) );
      }
      else
      {
        new_over_cnt++;
        overflow_samples.back().second = is;
      }  
    }
    else
    {
      new_over_cnt=0;
    }
  }
  return overflow_samples;
}

////////////////////////////////////////////////////////////////////////////////

bool DigSADC::FitGoGo(  const std::vector<uint16> &sample ) 
{
  bool debug = false;
//   bool debug = true;
  bool print_signal = false;
  if( debug ) std::cout <<" DigSADC::FitGoGo  debug " << std::endl;
  bool calibok = CheckCalib();
  if( !calibok )
  {
    cerr <<" DigSADC::FitGoGo CheckCalib failed " << endl;
    exit(1);
  }
  Clear();
  
  if( sample.size() < 2)  return false;
  over_ = DetectOverflow(sample);
  int maxint =sample[0];
  int intmax =0;
  for( int is=0; is < (int)sample.size(); is++)
  {
    if( sample[is] > maxint )
    {
      maxint = sample[is];
      intmax=is;
    }
  }  
// Interesed in big values now
  if( maxint < 1000 ) debug = false;
  if( debug ) std::cout <<" maxint " << maxint << " intmax " << intmax << std::endl;
  
  double time(0.), ped(0.);

// Do the same slowly
  if( debug ) std::cout <<" par_sadc_ped_max_ " << par_sadc_ped_max_ << " csample size " << csample_.size() << std::endl;

// Define base lines
  { 
    ped_odd_dynamic_ =0.;
    double stat_oddd =0.;
    for( int is=0; is <= par_sadc_ped_max_; is+=2 )
    {
      ped_odd_dynamic_ += sample[is];
      stat_oddd++;
    }
    if( stat_oddd > 0 ) ped_odd_dynamic_ /= stat_oddd;

    ped_even_dynamic_ =0.;
    double stat_evenn =0.;
    for( int is=1; is <= par_sadc_ped_max_; is+=2 )
    {
      ped_even_dynamic_ += sample[is];
      stat_evenn++;
    }
    if( stat_evenn > 0 ) ped_even_dynamic_ /= stat_evenn++;
  }
  ped = (ped_odd_dynamic_+ped_even_dynamic_)/2.;
  
  if( debug ) cout <<" Ped " << ped <<" TruePed " << GetTrueCalibPed() << endl;
  if( debug ) cout <<" PedOddDynamic " << ped_odd_dynamic_ <<" TruePedOdd " << GetTrueCalibPedOdd() << endl;
  if( debug ) cout <<" PedEvenDynamic " << ped_even_dynamic_ <<" TruePedEven " << GetTrueCalibPedEven() << endl;

  // Subtract pedestals taken from calibrations
  for( int is=0; is < (int)sample.size(); is++)
  {
    if( (is%2) > 0 )
      csample_.push_back( double(sample[is]) - GetTrueCalibPedEven() );
    else  
      csample_.push_back( double(sample[is]) - GetTrueCalibPedOdd() );
//    if( debug ) cout << " " << (int)(10.*csample_.back() );  
    if( debug ) cout << " " << (int)(csample_.back() );  
  }
  if( debug ) cout << endl;  


  // Find maximum sample
  double signal = -100000.;
  int max_position = -1;
  for( int is=0; is < (int)csample_.size(); is++)
  {
    if( csample_[is] > signal )
    {
      signal = csample_[is];
      max_position = is;
    }
  }
  if( debug ) cout <<" signal "<< signal << endl;
// Selection which trigger signals overlap in S2  
//  if( signal > 1500.) print_signal = true;
  if( print_signal )
  {
    PrintCSample( csample_);
  }

// Do we have more signals in the sample ??
  double signal_max_cut = 10.;
  double fraction_signal_max_cut = 0.03; // We do not apply shape yet. So there could be after pulses at some level
  if(signal_max_cut < signal*fraction_signal_max_cut) signal_max_cut=signal*fraction_signal_max_cut;
  
  std::vector < int > signalsmax;
  for( int is=0; is < (int)csample_.size(); is++)
  {
    signalsmax.push_back(-1);
  }
  std::vector < std::pair<int,double> > signals;
  signals.push_back( std::pair<int,double>(max_position,signal) );
  signalsmax[max_position] = signal;

  while(true)
  {
    double cursignal = -100000.;
    int max_position_cur = -1;
    for( int is=0; is < (int)csample_.size(); is++)
    {
      if(csample_[is] < signal_max_cut ) continue;
      if(signalsmax[is] > 0 ) continue;
      if( csample_[is] > cursignal )
      {
        int il = is-1;
        double sigl =-100000.;
        if(il>=0) sigl=csample_[il];
        int ir = is+1;
        double sigr =-100000.;
        if(ir<(int)csample_.size() ) sigr=csample_[ir];
        if( csample_[is] > sigl && csample_[is] >= sigr )
        {
          cursignal = csample_[is];
          max_position_cur = is;
        }
      }
    }

    if( max_position_cur >= 0 )
    {
      signals.push_back( std::pair<int,double>(max_position_cur,cursignal) );
      signalsmax[max_position_cur] = cursignal;
      continue;
    }
    else
    {
      break;
    }
    cerr <<" Should never be here ! " << signals.size() << endl;
  }  
  if( debug ) cout <<" And Now we have NSignals = " << signals.size() << endl;

// // Time now in clocks
//   std::pair<double, double> times = CalcTime2_halfMaxDigSADC(csample_,max_position, signal);
//   if( debug ) cout <<" time1 "<< times.first <<" time2 "<< times.second << endl;  
//   time = times.first;
  

// Go to fitting with Shape
  if( debug ) cout <<" * tab_ = " << tab_ << endl;
  
// //  std::pair<double,double> newpar = FitShapePrecise(  csample_, cell, signal, time, max_position, 2 );
//   FitPar par(signal, 1., time, 1., max_position, 2 );
//   FitShapePrecise(  csample_, par );
// // Add main signal
//   signals_.push_back( par );
  
// All signals signals processed at the same base. Aimed to have som zero level approximation
  for( int i=0; i< (int)signals.size(); i++ )
  {
    double signal = signals[i].second;
    int max_position = signals[i].first;
// Time now in clocks
    std::pair<double, double> times = CalcTime2_halfMaxDigSADC(csample_,max_position, signal);
    if( debug ) cout <<" time1 "<< times.first <<" time2 "<< times.second << endl;  
    time = times.first;
    FitPar par(signal, 1., time, 1., max_position, 2 );
    par.SetTime2(times.second);
    FitShapePrecise(  csample_, par );
    signals_.push_back( par );

// Some workaround when front position is badly defined
//   double front_position_rel_max = - 1.5;
//     double signalmore = signals[i].second;
//     int max_positionmore = signals[i].first;
//     double timemore = double( max_positionmore ) + front_position_rel_max;
//     FitPar newpar( signalmore, 1., timemore, 1., max_positionmore, 2 ); 
//     FitShapePrecise(  csample_, newpar);
//     signals_.push_back( newpar );
  }
  
//  if( overflow_samples.size() > 0 ) cout <<" ZABYL ??? DigSADC Overflow detected Nover = " << overflow_samples.size() <<" Vover = " << sample[overflow_samples[0].first] << endl;

// Multiply by SADC clock
  for( int i=0; i< (int)signals_.size(); i++ )
  {
    signals_[i].time  *= sadc_clock_;
    signals_[i].stime *= sadc_clock_;
    signals_[i].time2  *= sadc_clock_;
    signals_[i].stime2 *= sadc_clock_;
  }

// 
// // Dont like this not clear wht we finally need 
// // TODO store Hi2 somewhere
//   times.first = time;
//   
//   if( debug ) cout <<" signal = "<< signal <<" time " << time  << endl;
// //   // Fill result internal structure
// //   timed_ = times;
// 
//   // Fill result structure
//   result_.ampl = signal;
//   result_.time = times.first*sadc_clock_;
//   result_.time1 = times.first*sadc_clock_;
//   result_.time2 = times.second*sadc_clock_;
//   result_.timeFWHM = (times.second-times.first)*sadc_clock_;
// //  cout <<" amp " <<result_.ampl << " time " << result_.time << " Set FWHM " << result_.timeFWHM << endl;
//   result_.base = ped;
//   result_.base_diff = ped_odd_dynamic_-ped_even_dynamic_;
//   result_.dbase = result_.base_diff/2.;
//   result_.max_pos = max_position;
//   result_.ready = 1;
//   return ResultIsReady();
//  cerr <<" Must return some real result " << endl;
  return true;
}

// ////////////////////////////////////////////////////////////////////////////////
// // TODO drop cell parameter, no need for single DigitizerSADC
// std::pair <std::pair<double,double>, std::pair<double,double> > DigSADC::FitShapePrecise (  const std::vector<double> &csample, double signal, double time, int max_position, int maxgate ) const
// {
//   bool debug = false;
//   
// //  std::pair<double,int> hi2par = ShapeHi2(  csample_, cell, signal, time, max_position, 2 );
//   std::pair<double,int> hi2par = ShapeHi2(  csample_, signal, time, max_position, 2 );
//   if( debug) cout <<" 0 Hi2 " << hi2par.first <<" ndf " << hi2par.second << endl;
// 
// //  std::pair<double,double> newpar = FitShape(  csample_, cell, 0.05, 11, signal, time, max_position, 2 );
//   std::pair <std::pair<double,double>, std::pair<double,double> > newpar = FitShape(  csample_, 0.05, 11, signal, time, max_position, 2 );
//   signal = newpar.first.first;
//   time = newpar.second.first;
// //  hi2par = ShapeHi2(  csample_, cell, signal, time, max_position, 2 );
//   hi2par = ShapeHi2(  csample_, signal, time, max_position, 2 );
//   if( debug) cout <<" 1st Fit Hi2 " << hi2par.first <<" ndf " << hi2par.second << endl;
// 
// //  newpar = FitShape(  csample_, cell, 0.01, 11, signal, time, max_position, 2 );
//   newpar = FitShape(  csample_, 0.01, 11, signal, time, max_position, 2 );
//   signal = newpar.first.first;
//   time = newpar.second.first;
// //  hi2par = ShapeHi2(  csample_, cell, signal, time, max_position, 2 );
//   hi2par = ShapeHi2(  csample_, signal, time, max_position, 2 );
//   if( debug) cout <<" 2nd Fit Hi2 " << hi2par.first <<" ndf " << hi2par.second << endl;
//   
//   return newpar;
// }
// 
/////////////////////////////////////////////////////////////////////////////////
// TODO drop cell parameter, no need for single DigitizerSADC
void DigSADC::FitShapePrecise (  const std::vector<double> &csample, FitPar &par ) const
{
  bool debug = false;
  
  ShapeHi2(  csample_, par);
  if( debug) cout <<" 0 Hi2 " << par.hi2 <<" ndf " << par.ndf << endl;

  FitShape(  csample_, par, 0.05, 11);
  if( debug) cout <<" 1st Fit Hi2 " << par.hi2 <<" ndf " << par.ndf << endl;

  FitShape(  csample_, par, 0.01, 11 );
  if( debug) cout <<" 2nd Fit Hi2 " << par.hi2 <<" ndf " << par.ndf << endl;
}

///////////////////////////////////////////////////////////////////////////////

double  DigSADC::GetTabValue( double v ) const
{
  if( tab_ == 0 ) return 0.;
  if( tab_->GetTab()->size() == 0 ) return 0.;
  if( tab_->GetTab()->size() == 1) return tab_->GetTab()->begin()->second;
  std::map<double, double>::const_iterator hi = tab_->GetTab()->lower_bound(v);
 // variable is larger than highest data point?
  if ( hi == tab_->GetTab()->end() )
    return tab_->GetTab()->rbegin()->second;
 
 // variable is equal or smaller than the lowest data point?
  if ( hi == tab_->GetTab()->begin() )
    return tab_->GetTab()->begin()->second;
  std::map<double, double>::const_iterator lo = hi;
  lo--;
 // linear interpolation
  return lo->second
        + (hi->second - lo->second) * (v - lo->first) / (hi->first - lo->first);
}

// ////////////////////////////////////////////////////////////////////////////////
// 
// std::pair<double,int> DigSADC::ShapeHi2 (  const std::vector<double> &csample, double signal, double time, int max_position, int maxgate ) const
// {
// //   bool debug = true;
//   bool debug = false;
//   double ssy =0.;
//   int ndf =-2;
//   for( int is=max_position-maxgate; is <= max_position+maxgate; is++)
//   {  
//     if( is <= 0 ) continue;
//     if( is >= (int)csample.size() ) break;
//     double timex = double(is)-time;
//     ndf++;
//     double x = timex;
// //    double vtab = GetManCellsShapeTableSADC()->Value( cell, x ); 
//     double vtab = GetTabValue( x ); 
//     double dy = csample[is]-signal*vtab;
//     double y = csample[is];
//     double sigy = 1.+0.01*fabs(y);
//     double w = 1./sigy/sigy;
//     ssy += dy*dy*w;
//   }    
//   if( debug ) cout <<" ssy " << ssy <<" ndf " << ndf << endl;
//   return std::pair< double, int > (ssy,ndf);
// }
// 
////////////////////////////////////////////////////////////////////////////////

void DigSADC::ShapeHi2 (  const std::vector<double> &csample, FitPar &p ) const
{
//   bool debug = true;
  bool debug = false;
  double ssy =0.;
  int ndf =-2;
  for( int is=p.max_position-p.maxgate; is <= p.max_position+p.maxgate; is++)
  {  
    if( is <= 0 ) continue;
    if( is >= (int)csample.size() ) break;
    double timex = double(is)-p.time;
    ndf++;
    double x = timex;
//    double vtab = GetManCellsShapeTableSADC()->Value( cell, x ); 
    double vtab = GetTabValue( x ); 
    double dy = csample[is]-p.signal*vtab;
    double y = csample[is];
    double sigy = 1.+0.01*fabs(y);
    double w = 1./sigy/sigy;
    ssy += dy*dy*w;
  }    
  if( debug ) cout <<" ssy " << ssy <<" ndf " << ndf << endl;
  p.hi2=ssy;
  p.ndf=ndf;
}

////////////////////////////////////////////////////////////////////////////////
//  
// std::pair< std::pair<double,double>, std::pair<double,double> > DigSADC::FitShape (  const std::vector<double> &csample, const double &stept, const size_t &nfitp, double signal, double time, int max_position, int maxgate ) const
// {
//   bool debug = false;
// //   bool debug = true;
//   double signalpar = signal;
//   double timepar = time;
// // Here we can fit parameters by shape
//   {
//     bool printsam = debug;
// //    int maxgate=2;
//     std::vector<double> pp;
//     std::vector<double> py;
//     std::vector<double> t;
//     std::vector<double> ss;
//     std::vector<double> sp;
// //    size_t nfitp = 11;
// //    double stept = 0.02; // can be amplitude/energy dependent
//     double range = stept*nfitp;
//     for( size_t np=0; np<nfitp; np++)
//     {
//       pp.push_back(0.);
//       py.push_back(0.);
//       ss.push_back(0.);
//       sp.push_back(0.);
//       t.push_back( -range/2.+stept*double(np)+stept/2. );
//     }
//     
//     double yy =0.;
// 
//     for( int is=max_position-maxgate; is <= max_position+maxgate; is++)
//     {  
//       if( is <= 0 ) continue;
//       if( is >= (int)csample.size() ) break;
//       double timex = double(is)-timepar;
// 
//       double x = timex;
// //      double vtab = GetManCellsShapeTableSADC()->Value( cell, x );
//       double vtab = GetTabValue( x );
// //      cout <<" vtab " << vtab <<" Xcheck " << GetTabValue( x ) << endl;
// // not used      double dy = csample[is]-signalpar*vtab;
//       double y = csample[is];
//       double sigy = 1.+0.01*fabs(y);
//       double w = 1./sigy/sigy;
//       yy += y*y*w;
//       for( size_t np=0; np<nfitp; np++)
//       {
//          double xp = x + t[np];
// //         double p = GetManCellsShapeTableSADC()->Value( cell, xp );
//          double p = GetTabValue( xp );
//          pp[np] += p*p*w;
//          py[np] += p*y*w;
//       }
//       if( printsam ) cout <<" x " << x  << " V " <<   csample[is] <<"( " << signal*vtab <<") "<<" dy " << csample[is]-signalpar*vtab << endl;;
//     }    
// 
// //    if( printsam ) cout << endl; 
//     if( printsam ) cout << "  sp7 scan  signal seed = " << signalpar << "  time seed = " << timepar << endl;
//     int npmin=-1;
//     double ssmin=1.e+13;
//     for( size_t np=0; np<nfitp; np++)
//     {
//       sp[np] = py[np]/pp[np];
// //      ss[np] = yy-2.*sp*py[np]+sp*sp*pp[np];
//       ss[np] = yy-py[np]*py[np]/pp[np];
//       if( printsam ) cout <<" t " << t[np] <<" signal "<< sp[np] <<" Hi2 " << ss[np]  << endl;
//       if( ss[np] < ssmin )
//       {
//         ssmin = ss[np];
//         npmin = np;
//       }
//     }
// // Well apply Hi2 fit in a while
// //    std::vector<double> tcorr = fit_parabola(t,ss);
//     
//     if( npmin >= 0 )
//     {
//       timepar = timepar - t[npmin];
//       signalpar = sp[npmin];
//       if( debug ) cout <<" npmin " << npmin <<" signalpar " << signalpar <<" timepar " << timepar << endl;
//     }
// // // Well apply Hi2 fit in a while
// // //    std::vector<double> tcorr = fit_parabola(t,ss);
// //     time = timepar;
// //     signal = signalpar;
// //     double ssy =0.;
// //     int ndf =-2;
// //     for( int is=max_position-maxgate; is <= max_position+maxgate; is++)
// //     {  
// //       if( is <= 0 ) continue;
// //       if( is >= (int)csample.size() ) break;
// //       double timex = double(is)-timepar;
// //       ndf++;
// //       double x = timex;
// //       double vtab = GetManCellsShapeTableSADC()->Value( cell, x ); 
// //       double dy = csample[is]-signalpar*vtab;
// //       double y = csample[is];
// //       double sigy = 1.+0.01*fabs(y);
// //       double w = 1./sigy/sigy;
// //       ssy += dy*dy*w;
// //     }    
// //     if( debug ) cout <<" ssy " << ssy <<" ndf " << ndf << endl;
// 
// // TODO check is everything is correct
// // TODO apply extention if at the edge
// 
//   }
//   double ssignalpar = 1.;
//   double stimepar = 1.;
//   
//   std::pair< double, double > signalres( signalpar, ssignalpar);
//   std::pair< double, double > timeres( timepar, stimepar);
//   
//   return std::pair<  std::pair< double, double >, std::pair< double, double > > ( signalres,timeres );
// }
// 
 
void DigSADC::FitShape (  const std::vector<double> &csample, FitPar &p, const double &stept, const size_t &nfitp ) const
{
  bool debug = false;
//   bool debug = true;
  double signalpar = p.signal;
  double timepar = p.time;
// Here we can fit parameters by shape
  {
    bool printsam = debug;
//    int maxgate=2;
    std::vector<double> pp;
    std::vector<double> py;
    std::vector<double> t;
    std::vector<double> ss;
    std::vector<double> sp;
//    size_t nfitp = 11;
//    double stept = 0.02; // can be amplitude/energy dependent
    double range = stept*nfitp;
    for( size_t np=0; np<nfitp; np++)
    {
      pp.push_back(0.);
      py.push_back(0.);
      ss.push_back(0.);
      sp.push_back(0.);
      t.push_back( -range/2.+stept*double(np)+stept/2. );
    }
    
    double yy =0.;

    for( int is=p.max_position-p.maxgate; is <= p.max_position+p.maxgate; is++)
    {  
      if( is <= 0 ) continue;
      if( is >= (int)csample.size() ) break;
      double timex = double(is)-timepar;

      double x = timex;
//      double vtab = GetManCellsShapeTableSADC()->Value( cell, x );
      double vtab = GetTabValue( x );
//      cout <<" vtab " << vtab <<" Xcheck " << GetTabValue( x ) << endl;
// not used      double dy = csample[is]-signalpar*vtab;
      double y = csample[is];
      double sigy = 1.+0.01*fabs(y);
      double w = 1./sigy/sigy;
      yy += y*y*w;
      for( size_t np=0; np<nfitp; np++)
      {
         double xp = x + t[np];
//         double p = GetManCellsShapeTableSADC()->Value( cell, xp );
         double pv = GetTabValue( xp );
         pp[np] += pv*pv*w;
         py[np] += pv*y*w;
      }
      if( printsam ) cout <<" x " << x  << " V " <<   csample[is] <<"( " << signalpar*vtab <<") "<<" dy " << csample[is]-signalpar*vtab << endl;;
    }    

//    if( printsam ) cout << endl; 
    if( printsam ) cout << "  sp7 scan  signal seed = " << signalpar << "  time seed = " << timepar << endl;
    int npmin=-1;
    double ssmin=1.e+13;
    for( size_t np=0; np<nfitp; np++)
    {
      sp[np] = py[np]/pp[np];
//      ss[np] = yy-2.*sp*py[np]+sp*sp*pp[np];
      ss[np] = yy-py[np]*py[np]/pp[np];
      if( printsam ) cout <<" t " << t[np] <<" signal "<< sp[np] <<" Hi2 " << ss[np]  << endl;
      if( ss[np] < ssmin )
      {
        ssmin = ss[np];
        npmin = np;
      }
    }
// Well apply Hi2 fit in a while
//    std::vector<double> tcorr = fit_parabola(t,ss);
    
    if( npmin >= 0 )
    {
      timepar = timepar - t[npmin];
      signalpar = sp[npmin];
      if( debug ) cout <<" npmin " << npmin <<" signalpar " << signalpar <<" timepar " << timepar << endl;
    }
// // Well apply Hi2 fit in a while
// //    std::vector<double> tcorr = fit_parabola(t,ss);
//     time = timepar;
//     signal = signalpar;
//     double ssy =0.;
//     int ndf =-2;
//     for( int is=max_position-maxgate; is <= max_position+maxgate; is++)
//     {  
//       if( is <= 0 ) continue;
//       if( is >= (int)csample.size() ) break;
//       double timex = double(is)-timepar;
//       ndf++;
//       double x = timex;
//       double vtab = GetManCellsShapeTableSADC()->Value( cell, x ); 
//       double dy = csample[is]-signalpar*vtab;
//       double y = csample[is];
//       double sigy = 1.+0.01*fabs(y);
//       double w = 1./sigy/sigy;
//       ssy += dy*dy*w;
//     }    
//     if( debug ) cout <<" ssy " << ssy <<" ndf " << ndf << endl;

// TODO check is everything is correct
// TODO apply extention if at the edge

  }
  double ssignalpar = 1.;
  double stimepar = 1.;
  
  p.signal = signalpar;
  p.ssignal = ssignalpar;
  p.time = timepar;
  p.stime = stimepar;
  ShapeHi2( csample, p);
  
}

/////////////////////////////////////////////////////////

std::pair<double, double> DigSADC::CalcTime2_halfMaxDigSADC( const std::vector<double> &csample, const unsigned int max_position, double max_value_true)
{
//    bool debug = true;
   bool debug = false;
// No assumptions on SADC shape
//   if( debug )
//   { 
//     cout <<" CalcTime2_halfMax " << max_position  << endl;
//     for( int is =0;  is < (int)sample.size(); is++)
//     {
//       if( is == (int)max_position ) cout <<"  !";
//       cout <<"  "<<  csample_[is];
//       if( is == (int)max_position ) cout <<"  !";
//     }
//     cout << endl;
//   }


// !!!  Otkuda eto -0.5 ????? Revision!!!!!!! Ili tak nado?
// Vrode-by eto tak nado. Esli max u nas samyi levyi bin, to my predpolagaem, chto levee
// naxoditsya 0, ni i time togda dolzhen byt' -0.5
// Analogichno s zadnim frontom time budet +0.5
// Nepravilno
//  Polnyi range eto 0--N*Clock
//  0-i bin 0 -- Clock   N-1 i bin  (N-1)*Clock -- N*Clock
// ili v binax nbin+0.5

// Tak chto eto nepravilno. Nado popravit'.

  double time = max_position-0.5;

  double fmax = csample[max_position];
//  fmax = max_value_true;
  
//  for( int is = max_position;  is >= -1; is--)
  for( int is = max_position-1;  is >= -1; is--)
  {
    if( is >= 0 )
    {
      if( csample_[is] <= fmax/2. )
      {
        time = is + (fmax/2.- csample[is])/(csample[is+1]- csample[is]); 
        break;
      }
    }
    else
    {
      if( debug ) cout << " time " << time <<" expected " << -0.5 << endl;
      break;
    }
  }

  double time2 = max_position+0.5;
  for( int is = max_position+1;  is <= (int)csample.size(); is++)
  {
    if( is < (int)csample.size() )
    {
      if( csample[is] <= fmax/2. )
      {
// error!        time2 = is + (fmax/2.- csample[is])/(csample[is-1] - csample[is]); 
        time2 = is - (fmax/2.- csample[is])/(csample[is-1] - csample[is]); 
        break;
      }
    }
    else
    {
      if( debug ) cout << " time2 " << time2 <<" expected " << (int)csample.size()-0.5 << endl;
      break;
    }
  }
  if( debug ) cout <<" Time2 ( bins) " << time << " max_position " << max_position << " time2 " << time2 <<" sample size " << csample.size() << endl;
//  cout << " What we get so far? max_position " << max_position << " result_.ampl = " << result_.ampl  << " sample max position " << csample[max_position] << endl;
// Try to estimate time_max from parabola fit for +/- 1(2?) bins around maximum  
// Ne zdes' Sorry
// end of time_max detection  

//   time *= sadc_clock_;
//   time2 *= sadc_clock_;

  return std::pair< double, double> (time,time2);
}

////////////////////////////////////////////////////////////////////////////////
// // ManagerShapeTableSADC nice to remember
// 
// double ManagerCellsShapeTableSADC::Value( int cell, double x ) const
// {
// //   bool debug = true;
//   bool debug = false;
//   if( debug ) cout <<" x " << x << " vtab_.size() " << vtab_.size() << endl;
//   if(vtab_.size() == 0 ) return 0.;
// // TODO Get rid of Some hardcoded values
//   if( x< -15. || x> 20. ) return 0.;
//   const std::map<double,double> * tab = GetTab( cell );
//   if( debug ) cout <<" tab * " << tab << endl;
//   if(  tab != NULL) return ValueTab(x, tab);
//   return 0.;
// }
// 
// // ////////////////////////////////////////////////////////////////////////////////
// 
// std::pair< double, double> ManagerCellsShapeTableSADC::WValue( int hfw, double x ) const
// {
// //   bool debug = true;
//   bool debug = false;
//   if( debug ) cout <<" x " << x << " vtab_.size() " << vtab_.size() << endl;
//   if(vtab_.size() == 0 ) return std::pair< double, double>(0.,0.);
//   const std::map<double,std::pair<double,double> > * tab = GetWTab( hfw );
//   if( debug ) cout <<" tab * " << tab << endl;
// // Chto eto mozhet nado vernut' std::pair< double, double>(0.,1.) 
//   if( x< -15. || x> 20. ) return std::pair< double, double>(0.,0.);
//   if(  tab != NULL) return WValueTab(x, tab);
//   return std::pair< double, double>(0.,0.);
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// const std::map<double,double> * ManagerCellsShapeTableSADC::GetTab( int v ) const
// {
// //   if( vtab_.size() == 0 ) return NULL;
// //   if( vtab_.size() == 1) return vtab_.begin()->second;
// //   std::map<double, std::map<double,double> *>::const_iterator hi = vtab_.lower_bound(v);
// //  // variable is larger than highest data point?
// //   if ( hi == vtab_.end() )
// //     return vtab_.rbegin()->second;
// //  
// //  // variable is equal or smaller than the lowest data point?
// //   if ( hi == vtab_.begin() )
// //     return vtab_.begin()->second;
// //   std::map<double, std::map<double,double> *>::const_iterator lo = hi;
// //   lo--;
// //   return lo->second;
// // Return tab directly
//   if( vtab_.size() == 0 ) return NULL;
//   if( vtab_.size() == 1) return vtab_.begin()->second.pmap_;
//   std::map<int, ProfileMaps >::const_iterator hi = vtab_.lower_bound(v);
//  // variable is larger than highest data point?
//   if ( hi == vtab_.end() )
//     return vtab_.rbegin()->second.pmap_;
//  
//  // variable is equal or smaller than the lowest data point?
//   if ( hi == vtab_.begin() )
//     return vtab_.begin()->second.pmap_;
//   std::map<int, ProfileMaps>::const_iterator lo = hi;
//   lo--;
//   return lo->second.pmap_;
// }
// ////////////////////////////////////////////////////////////////////////////////
// 
// const std::map<double, std::pair<double,double > > *  ManagerCellsShapeTableSADC::GetWTab( int v ) const
// {
//   if( vtab_.size() == 0 ) return NULL;
//   if( vtab_.size() == 1) return vtab_.begin()->second.wmap_;
//   std::map<int, ProfileMaps >::const_iterator hi = vtab_.lower_bound(v);
//  // variable is larger than highest data point?
//   if ( hi == vtab_.end() )
//     return vtab_.rbegin()->second.wmap_;
//  
//  // variable is equal or smaller than the lowest data point?
//   if ( hi == vtab_.begin() )
//     return vtab_.begin()->second.wmap_;
//   std::map<int, ProfileMaps>::const_iterator lo = hi;
//   lo--;
//   return lo->second.wmap_;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// const std::map <double,std::pair<double,double > > * ManagerCellsShapeTableSADC::GetTab( double v ) const
// {
//   if( vtab_.size() == 0 ) return NULL;
//   if( vtab_.size() == 1) return vtab_.begin()->second.wmap_;
//   std::map<double, ProfileMaps >::const_iterator hi = vtab_.lower_bound(v);
//  // variable is larger than highest data point?
//   if ( hi == vtab_.end() )
//     return vtab_.rbegin()->second.wmap_;
//  
//  // variable is equal or smaller than the lowest data point?
//   if ( hi == vtab_.begin() )
//     return vtab_.begin()->second.pmap_;
//   std::map<double, ProfileMaps>::const_iterator lo = hi;
//   lo--;
//   return lo->second.wmap_;
// }
////////////////////////////////////////////////////////////////////////////////
// 
// std::pair<double,double>  ManagerCellsShapeTableSADC::WValueTab( double v, const std::map<double, std::pair<double,double> > * tab ) const
// {
//   if( tab->size() == 0 ) return std::pair<double,double>(0.,0.);
//   if( tab->size() == 1) return tab->begin()->second;
//   std::map<double, std::pair<double,double> >::const_iterator hi = tab->lower_bound(v);
//  // variable is larger than highest data point?
//   if ( hi == tab->end() )
//     return tab->rbegin()->second;
//  
//  // variable is equal or smaller than the lowest data point?
//   if ( hi == tab->begin() )
//     return tab->begin()->second;
//   std::map<double, std::pair<double,double>  >::const_iterator lo = hi;
//   lo--;
//  // linear interpolation
//   const std::pair<double,double> &lo2 = lo->second;
//   const std::pair<double,double> &hi2 = hi->second;
//   double ff = (v - lo->first) / (hi->first - lo->first);
//   std::pair <double,double> ret = lo2;
//   ret.first += (hi2.first - lo2.first)*ff;
//   ret.second += (hi2.second - lo2.second)*ff;
//   return ret;
// }
// 
////////////////////////////////////////////////////////////////////////////////
// Static function

double ManagerCellsShapeTableSADC::ValueTab( double v, const std::map<double,double> * tab ) const
{
  if( tab->size() == 0 ) return 0.;
  if( tab->size() == 1) return tab->begin()->second;
  std::map<double, double>::const_iterator hi = tab->lower_bound(v);
 // variable is larger than highest data point?
  if ( hi == tab->end() )
    return tab->rbegin()->second;
 
 // variable is equal or smaller than the lowest data point?
  if ( hi == tab->begin() )
    return tab->begin()->second;
  std::map<double, double>::const_iterator lo = hi;
  lo--;
 // linear interpolation
  return lo->second
        + (hi->second - lo->second) * (v - lo->first) / (hi->first - lo->first);
}

///////////////////////////////////////////////////////////////////////////////

bool ManagerCellsShapeTableSADC::CheckTables( void ) const
{
//   bool debug = true;
  bool debug = false;
  if( debug ) cout <<" ManagerCellsShapeTableSADC::CheckTables just Checking the size for the moment NTABLES = " << vtab_.size() << endl;
  return ( vtab_.size() > 0 );
}

///////////////////////////////////////////////////////////////////////////////

bool ManagerCellsShapeTableSADC::AddTable( int cell, const std::vector <int> &profile, double entries, double fwhm, double mprfw )
{
  bool debug = false;
//   bool debug = true;
  if( debug ) cout <<" ManagerCellsShapeTableSADC::AddTable for cell " << cell << endl;
  if( debug ) cout <<" Primary vtab_.size = " << vtab_.size() << endl;
  std::map<double,double> * tab = new std::map<double,double> ;
  ProfileMap * profm = new ProfileMap(tab);
  profm->SetStat( entries );
  profm->SetFWHM( fwhm );
  profm->SetMPRFW( mprfw );
  
  int max=0;
  int itmax = -1;
  for( size_t it=0; it<profile.size(); it++)
  {
    if(profile[it] > max )
    { 
      max = profile[it];
      itmax = it;
    }  
  }
  
  if( max <= 0.)
  {
    cerr <<" ManagerCellsShapeTableSADC::AddTable cell " << cell <<" max = " << max <<" : drop table creation " << endl;
    return false;
  } 

  double xvalmax =   Xmin_ + double(itmax)*stepX_;
  for( size_t it=0; it<profile.size(); it++)
  {
     double x = Xmin_ + double(it)*stepX_;
     double v = double(profile[it])/double(max);
     if( v == 0. ) continue;
//     cout <<" x " << x << " v " << v << endl;
     if( !tab->insert(std::make_pair(x,v)).second )
     {
       cerr <<" ManagerCellsShapeTableSADC::AddTable cell " << cell <<" insertion failed at x = " << x <<" v = " << v <<" : drop table creation profile.size()=" << profile.size() << endl;
       return false;
     }
  }

  if( ! vtab_.insert( std::make_pair(cell,profm) ).second )
  {
     cerr <<" ManagerCellsShapeTableSADC::AddTable cell " << cell <<" insertion vtab failed: drop table creation  vtab_.size = " << vtab_.size() << endl;
     return false;
  }
  if( debug ) cout <<" After AddTable vtab_.size = " << vtab_.size() << endl;
  return true;
}

////////////////////////////////////////////////////////////////////////////////

const ProfileMap * ManagerCellsShapeTableSADC::GetProfile( int icell ) const
{
  if( vtab_.size() == 0 ) return NULL;
  std::map <int, ProfileMap * >::const_iterator it = vtab_.find( icell );
  if( it == vtab_.end() ) return NULL;
  return it->second;
}

////////////////////////////////////////////////////////////////////////////////

const std::map <double,double> * ManagerCellsShapeTableSADC::GetTab( int icell ) const
{
  const ProfileMap * p = GetProfile( icell );
  if( p == 0 ) return NULL;
  return p->pmap_;
}

////////////////////////////////////////////////////////////////////////////////

double ManagerCellsShapeTableSADC::Value( int icell, double x ) const
{
  const std::map <double,double> *tab = GetTab( icell );
  if( tab == 0 ) return 0.;
  return ValueTab( x, tab );
}

////////////////////////////////////////////////////////////////////////////////

// ////////////////////////////////////////////////////////////////////////////////
// //bool ManagerCellsShapeTableSADC::AddTable( int double hfwmin, const std::vector <int> &profile )
// bool ManagerCellsShapeTableSADC::AddTable( int cell, const std::vector <int> &profile )
// {
//   bool debug = false;
// //   bool debug = true;
//   bool print_warnings = false;
//   std::map<double,double> * tab = new std::map<double,double> ;
//   int max=0;
//   int itmax = -1;
//   for( size_t it=0; it<profile.size(); it++)
//   {
//     if(profile[it] > max )
//     { 
//       max = profile[it];
//       itmax = it;
//     }  
//   }
//   
//   if( max <= 0.)
//   {
//     cerr <<" ManagerCellsShapeTableSADC::AddTable cell " << cell <<" max = " << max <<" : drop table creation " << endl;
//     return false;
//   } 
// //   if( max < 9000 )
// //   {
// //     cerr <<" ManagerCellsShapeTableSADC::AddTable hfwmin " << hfwmin <<" Not expected max = " << max <<" : drop table creation " << endl;
// //     exit(1);
// //   }
// 
//   double xvalmax =   Xmin_ + double(itmax)*stepX_;
//   for( size_t it=0; it<profile.size(); it++)
//   {
//      double x = Xmin_ + double(it)*stepX_;
//      double v = double(profile[it])/double(max);
//      if( v == 0. ) continue;
// //     cout <<" x " << x << " v " << v << endl;
//      if( !tab->insert(std::make_pair(x,v)).second )
//      {
//        cerr <<" ManagerCellsShapeTableSADC::AddTable cell " << cell <<" insertion failed at x = " << x <<" v = " << v <<" : drop table creation profile.size()=" << profile.size() << endl;
//        return false;
//      }
//   }
// 
// 
//   std::map<double,std::pair<double, double> > * wtab = new std::map<double,std::pair<double, double> > ;
// // Create Table of widths
//   for( size_t iv=0; iv< 100; iv++)
//   {
//      double tagyval =(0.5+double(iv))/100.;
//      double xval = Xmin_;
//      size_t ix=0; 
//      double ymin = 0.;
//      double ymax = 0.;
//      double xmin = Xmin_;
// 
// // ?????????????????????????????? Check algorithm!!!!!!!!!!!!
//      double xmax = Xmin_;
//      bool minok = false;
//      bool maxok = false;
//      for( ix=0; ix<10000; ix++)
//      {
//         xval = Xmin_+0.005* double(ix);
//         double y =  ValueTab( xval,  tab);
// //        cout <<" xval " << xval << "   Y= " << y << endl;
//         if( minok )
//         {
//           if( y < tagyval )
//           {
//             if( xval < xvalmax ) continue;
// 
//             if( maxok )
//             {
//               cerr <<" Create Table of widths internal error " << endl;
//               exit(1);
//             }
//             else
//             {
//               maxok = true;
//               ymax = y;
//               xmax = xval;
//               break;
//             }
//           }
//         }
//         else
//         {
//           if( y > tagyval )
//           {
//             minok = true;
//             ymin = y;
//             xmin = xval;
//           }
//         }
//      }
//      
//      
//      if( debug) cout <<" tagval " << tagyval << " xmin " << xmin <<" ymin " << ymin <<" xmax " << xmax <<" ymax " << ymax << " Width " << xmax - xmin << endl;
//      double width = xmax - xmin;
//      std::pair<double, double> val( tagyval, xmin);
//      std::pair<double, std::pair<double, double> > widval( width,val);
// 
//      if( wtab->insert(widval).second )
//      {
//        if( print_warnings) cerr <<" Warning SADC width map filling width "<< width <<" was inserted already " << endl;
//      }
//   }
//   
//   ProfileMaps tabs(tab,wtab);
//   
//   if( debug ) cout <<" vtab_.size = " << vtab_.size() << endl;
// 
//   if( !vtab_.insert( std::make_pair(cell,tabs) ).second )
//   {
//      cerr <<" ManagerCellsShapeTableSADC::AddTable cell " << cell <<" insertion vtab failed: drop table creation  vtab_.size = " << vtab_.size() << endl;
//      return false;
//   }
// 
// //  cout << " exit4debug " << endl;
// //  exit (0);
//   return true;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// int ManagerCellsShapeTableSADC::InputShapeTableSADC(const string &s)
// {
//   bool debug = true;
// //   bool debug = false;
// 
//   istringstream is(s);
//   string str;
// 
// //  Read line of comments
//   getline(is,str);
//   int ret;
// //  Read input table by table
// // Get shapetable name together with calo name
// 
//   bool first_init = true;
//   
//   if( debug )  cout <<" ManagerCellsShapeTableSADC::InputShapeTableSADC debug vtab_.size() = " << vtab_.size() << endl;
// 
//   while( getline(is,str) )
//   {
//     if( debug )  cout <<" New Table " << str << endl;
// // Get table parameters
//     getline(is,str);
//     float  entries, xmin, xmax;
//     int cell, ncx;
//     ret = sscanf(str.c_str()," %d  %f  %f  %f  %d ", &cell, &entries, &xmin, &xmax, &ncx);
// // values hfwbinmin and hfwbinmax are obsolet and we mast get rid of this parameter and use new format( to be implemented !)
// 
// // ignore as obsolet    if( debug )  cout <<" cell " << hfwbinmin << " hfwbinmax " << hfwbinmax <<" entries " << entries <<" xmin " << xmin <<" xmax " << xmax  <<" ret " << ret << endl;
//     if( debug )  cout <<" cell " << cell <<" entries " << entries <<" xmin " << xmin <<" xmax " << xmax  <<" ncx " << ncx << " ret " << ret <<endl;
//     if( ret != 5 )
//     {
//       cerr <<" ManagerCellsShapeTableSADC::InputShapeTableSADC format error : " << str << endl;
//     }
//     
// //    if( cell < 0 ) break; // endof table marker
//     if( ncx < 100 )
//     {
//       cerr <<" ManagerCellsShapeTableSADC::InputShapeTableSADC format error not expected ncx = " << ncx << endl;
//       exit(1);
//     }
// 
//     double step = (xmax-xmin)/double(ncx);
// 
//     if( first_init )
//     {
//       stepX_=step;
//       Xmin_=xmin;
//       Xmax_=xmax;
//       NXbins_=ncx;
//       first_init=false;
//     }
//     else
//     {
//       if( stepX_ != step ) 
//         cerr <<"FATAL stepX_ = " << stepX_ <<" but step " << step << endl;
//       if( Xmin_ != xmin )
//         cerr <<"FATAL Xmin_ = " << Xmin_ <<" but xmin " << xmin << endl;
//       if( Xmax_ != xmax )
//         cerr <<"FATAL Xmax_ = " << Xmax_ <<" but xmax " << xmax << endl;
//       if(  NXbins_ != ncx )
//         cerr <<"FATAL NXbins_ = " << NXbins_ <<" but ncx " << ncx << endl;
//     }
//     
//     
//     
//     int nlines = ncx/20;
//     if( ncx != nlines*20 )
//     {
//       cerr <<" ManagerCellsShapeTableSADC::InputShapeTableSADC format error : Please align ncx = " << ncx <<" to 20*N " << endl;
//       exit(1);
//     }
//     if( debug ) cout <<" step " << step << endl;
// 
//     std::vector<int> profile(ncx);
//     if( debug ) cout <<" profile " << endl;
//     int max=0;
//     for(int il=0; il<nlines; il++)
//     {
//       getline(is,str);
//       istringstream iis(str); 
//       for(int it=0; it<20; it++)
//       {
//         iis >> profile[it+20*il];
//         if( debug ) cout <<" [ " <<it+20*il <<"] " << profile[it+20*il];
//         if(profile[it+20*il] > max ) max =  profile[it+20*il];
//       }  
//       if( debug ) cout << endl;
//     }  
// // Read sigma separator
//     getline(is,str);
// // Read sigma
//     std::vector<int> sigma(ncx);
//     if( debug ) cout <<" sigma " << endl;
//     for(int il=0; il<nlines; il++)
//     {
//       getline(is,str);
//       istringstream iis(str); 
//       for(int it=0; it<20; it++)
//       {
//         iis >> sigma[it+20*il];
//         if( debug ) cout <<" [ " <<it+20*il <<"] " << sigma[it+20*il];
//       }  
//       if( debug ) cout << endl;
//     }  
// // Read stat separator
//     getline(is,str);
// // Read stat
//     std::vector<int> stat(ncx);
//     if( debug ) cout <<" stat  "<< endl;
//     for(int il=0; il<nlines; il++)
//     {
//       getline(is,str);
//       istringstream iis(str); 
//       for(int it=0; it<20; it++)
//       {
//         iis >> stat[it+20*il];
//         if( debug ) cout <<" [ " <<it+20*il <<"] " << stat[it+20*il];
//       }  
//       if( debug ) cout << endl;
//     }  
// // Store table in persistent object
//     bool ok = AddTable( cell,  profile );
//     if( !ok )
//     {
//       cerr <<" ManagerCellsShapeTableSADC::InputShapeTableSADC failed to add table cell " << cell <<" profile size " << profile.size() << endl;
//       return 1;
//     }  
//     if( debug ) cout <<" AddTable ok " << ok << endl;
//   }
//   bool oktables = CheckTables();
//   if( debug ) cout <<" No more tables oktables " << oktables << endl;
//   
//   return 0;
// }
// 
////////////////////////////////////////////////////////////////////////////////

int ManagerCellsShapeTableSADC::InputCellShapeTableSADC(const string &s)
{
//   bool debug = true;
  bool debug = false;
  if( debug )  cout <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC gebug input:  " << s << endl;
  istringstream is(s);
  string str;

//  Read line of comments
  getline(is,str);
  int ret;
//  Read input table by table
// Get shapetable name together with calo name

  bool first_init = true;
  
  if( debug )  cout <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC debug " << vtab_.empty() << endl;
  if( debug )  cout <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC debug vtab_.size() = " << vtab_.size() << endl;

  while( getline(is,str) )
  {
    if( debug )  cout <<" New Table " << str << endl;
// Get table prompt string
    getline(is,str);
// Get table parameters
    getline(is,str);
    float  entries, xmin, xmax, fwhm, mprfw;
    int cell, ncx;
    ret = sscanf(str.c_str()," %d  %f  %f  %f  %d  %f  %f ", &cell, &entries, &xmin, &xmax, &ncx, &fwhm, &mprfw );
// values hfwbinmin and hfwbinmax are obsolet and we mast get rid of this parameter and use new format( to be implemented !)

// ignore as obsolet    if( debug )  cout <<" cell " << hfwbinmin << " hfwbinmax " << hfwbinmax <<" entries " << entries <<" xmin " << xmin <<" xmax " << xmax  <<" ret " << ret << endl;
    if( debug )  cout <<" cell " << cell <<" entries " << entries <<" xmin " << xmin <<" xmax " << xmax  <<" ncx " << ncx <<" fwhm " << fwhm  <<" mprfw " << mprfw <<" ret " << ret << endl;
    if( ret != 7 )
    {
      cerr <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC format error : " << str << endl;
    }
    
//    if( cell < 0 ) break; // endof table marker
    if( ncx < 100 )
    {
      cerr <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC format error not expected ncx = " << ncx << endl;
      exit(1);
    }

    double step = (xmax-xmin)/double(ncx);

    if( first_init )
    {
      stepX_=step;
      Xmin_=xmin;
      Xmax_=xmax;
      NXbins_=ncx;
      first_init=false;
    }
    else
    {
      if( stepX_ != step ) 
        cerr <<"FATAL stepX_ = " << stepX_ <<" but step " << step << endl;
      if( Xmin_ != xmin )
        cerr <<"FATAL Xmin_ = " << Xmin_ <<" but xmin " << xmin << endl;
      if( Xmax_ != xmax )
        cerr <<"FATAL Xmax_ = " << Xmax_ <<" but xmax " << xmax << endl;
      if(  NXbins_ != ncx )
        cerr <<"FATAL NXbins_ = " << NXbins_ <<" but ncx " << ncx << endl;
    }
    
    
    
    int nlines = ncx/20;
    if( ncx != nlines*20 )
    {
      cerr <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC format error : Please align ncx = " << ncx <<" to 20*N " << endl;
      exit(1);
    }
    if( debug ) cout <<" step " << step << endl;

    std::vector<int> profile(ncx);
    if( debug ) cout <<" profile " << endl;
    int max=0;
    for(int il=0; il<nlines; il++)
    {
      getline(is,str);
      istringstream iis(str); 
      for(int it=0; it<20; it++)
      {
        iis >> profile[it+20*il];
        if( debug ) cout <<" [ " <<it+20*il <<"] " << profile[it+20*il];
        if(profile[it+20*il] > max ) max =  profile[it+20*il];
      }  
      if( debug ) cout << endl;
    }  
// Read sigma separator
    getline(is,str);
// Read sigma
    std::vector<int> sigma(ncx);
    if( debug ) cout <<" sigma " << endl;
    for(int il=0; il<nlines; il++)
    {
      getline(is,str);
      istringstream iis(str); 
      for(int it=0; it<20; it++)
      {
        iis >> sigma[it+20*il];
        if( debug ) cout <<" [ " <<it+20*il <<"] " << sigma[it+20*il];
      }  
      if( debug ) cout << endl;
    }  
// Read stat separator
    getline(is,str);
// Read stat
    std::vector<int> stat(ncx);
    if( debug ) cout <<" stat  "<< endl;
    for(int il=0; il<nlines; il++)
    {
      getline(is,str);
      istringstream iis(str); 
      for(int it=0; it<20; it++)
      {
        iis >> stat[it+20*il];
        if( debug ) cout <<" [ " <<it+20*il <<"] " << stat[it+20*il];
      }  
      if( debug ) cout << endl;
    }  
// Store table in persistent object
    bool ok = AddTable( cell,  profile, entries, fwhm, mprfw );
    if( !ok )
    {
      cerr <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC failed to add table cell " << cell <<" profile size " << profile.size() << endl;
      return 1;
    }  
    if( debug ) cout <<" AddTable ok " << ok << endl;
  }
//  bool oktables = CheckTables();
//  if( debug ) cout <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC No more tables oktables " << oktables << endl;
  if( debug )
  { 
    cout <<" ManagerCellsShapeTableSADC::InputCellShapeTableSADC exit4debug " << endl;
    exit(0);
  }
  
  return 0;
}

////////////////////////////////////////////////////////////////////////////////

} // namespace MN

////////////////////////////////////////////////////////////////////////////////

