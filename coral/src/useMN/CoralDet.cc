#include "CsDet.h"
#include "CsCalorimeter.h"
#include "DaqDataDecoding/ChipSADC.h"
#include "CsDigit.h"
#include "CsOpt.h"
#include "CDB.h"
#include "DigSADC.h"
#include "DetMN.h"

#define CDB_LINE_MAX 132

using namespace std;

////////////////////////////////////////////////////////////////////////////////

void CsDet::ClearMN( void ) 
{
 if( det_mn_ !=0 ) det_mn_->Clear();
}

////////////////////////////////////////////////////////////////////////////////

std::vector<MN::DigSADC*> &CsDet::GetDigSADC2Modify ( void ) 
{ 
  return det_mn_->GetDigSADC2Modify();
}

////////////////////////////////////////////////////////////////////////////////

const std::vector<MN::DigSADC*> &CsDet::GetDigSADC ( void ) const
{ 
  return det_mn_->GetDigSADC();
}

////////////////////////////////////////////////////////////////////////////////

size_t  CsDet::NCellsMN( void ) const
{
 if( det_mn_ !=0 ) return det_mn_->NCellsMN();
 return 0;
}

////////////////////////////////////////////////////////////////////////////////

const std::string CsDet::GetCellNameMN ( int icell ) const
{
 if( det_mn_ !=0 ) return det_mn_->GetCellNameMN(icell);
 return std::string("");
}

////////////////////////////////////////////////////////////////////////////////

void CsDet::readCalibrationByTagsMN(time_t timePoint) {
  bool debug = false;
//   bool debug = true;
  if( det_mn_ == 0 )
  {
    cerr <<" CsDet::readCalibrationByTags " << GetTBName() <<" DetMN not initialized " << endl;
    exit(1);
  }
  CDB::Time tp(timePoint,0);
  tm *t = localtime(&tp.first);
  CsOpt* opt = CsOpt::Instance();
  list<string> tags;  list<string>::iterator itag;
  bool we_have_calib_tags = opt->getOpt(GetTBName(), "DetInputCalibTags:", tags);
  if( !we_have_calib_tags) return;
  if( debug ) std::cout <<"  we_have_calib_tags " << we_have_calib_tags <<" size "<< tags.size() << endl;
  if( we_have_calib_tags ) {
    for(itag=tags.begin(); itag!=tags.end(); itag++) {

      if( *itag == std::string("_CellShapeTableSADC") ) {
        for( int ic=0; ic< (int)NCellsMN(); ic++ )
        {
	  std::string celldetindb = GetTBName()+std::string(".")+GetCellNameMN(ic);
           
          if( debug ) cout <<" Try to read cdb_ Tag "<< *itag <<" for " << GetTBName() <<" For Cell " << ic <<" Det in DB : " << celldetindb << endl;
          try {
            string s("");
            if( debug ) std::cout <<"  Try to read cdb_ Tag "<< *itag <<" for " << celldetindb << endl;
            cdb_->read( celldetindb, s, tp, *itag);
            if (s == "") {
              cerr << " CsDet::readCalibrationByTagsMN " << celldetindb << " empty string from calib file" << endl;
            }
            else {
              if( debug ) cout << " Yeee!! CsDet::readCalibrationByTagsMN " << celldetindb << " Tag " << *itag <<" We have something To decode !! " << s.size() <<" For Cell " << ic << endl;
              int res = det_mn_->InputCellShapeTableSADC(ic, s);
              if( debug ) cout <<" InputCellShapeTableSADC " << GetTBName() <<" Cell " << ic <<" result " << res << endl;
            }  
          }
          catch( const std::exception &e ) {
             cerr << "CsDet::readCalibrationByTags(): " << *itag << " " << celldetindb << " error in reading for time point ";
             cerr << t << ": " << e.what() << endl;
          }
        }
        det_mn_->CheckCellShapeTableSADC();
//        cerr <<" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! CsDet::readCalibrationByTags " << GetTBName() <<" Check the fix for tag " << *itag << endl;
	continue;
//	return; 
      } 


      try {
        string s("");
        if( debug ) std::cout <<"  Try to read cdb_ Tag "<< *itag <<" for " << det_mn_->GetNameMN() << endl;
        cdb_->read( det_mn_->GetNameMN(), s, tp, *itag);
        if (s == "") {
          cerr << " CsDet::readCalibrationByTagsByTagsMN " << det_mn_->GetNameMN() << " empty string from calib file" << endl;
        }
        else {
          if( debug ) cout << " Yeee!! CsDet::readCalibrationByTagsMN " << det_mn_->GetNameMN() << " Tag " << *itag <<" We have something To decode !! " << s.size() << endl;
	  det_mn_->ReadCalibrationByTags(*itag, s);
        }  
        
      }
      catch( const std::exception &e ) {
         cerr << "CsDet::readCalibrationByTagsMN(): " << *itag << " " << GetTBName() << " error in reading for time point ";
         cerr << t << ": " << e.what() << endl;
      }
    }
  } 
  if( debug ) std::cout <<" CsDet::readCalibrationByTagsMN " << GetTBName() << " OK! det_mn_ = " << det_mn_ << std::endl;

  if( det_mn_ !=0 )
  { 
    det_mn_->SetAutoDecodeVersion();
    det_mn_->CheckCalibrations();
//     if( det_mn_->GetNameMN() == std::string("ECAL0") || det_mn_->GetNameMN() == std::string("ECAL1") )
//     {
//       cerr <<std::cout <<" CsDet::readCalibrationByTags " << det_mn_->GetNameMN() << " exit4debug " << std::endl;
//       exit(0);
//     }
  }  
}

////////////////////////////////////////////////////////////////////////////////

void CsDet::DecodeChipDigitMN         (const CS::Chip::Digit &ddigit)
{
//    bool debug = true;
   bool debug = false;
//   if( GetTBName() == std::string("S1") || GetTBName() == std::string("S2") ) debug = true;
   const CS::ChipSADC::Digit  *ds = dynamic_cast<const CS::ChipSADC::Digit*>(&ddigit);
   if( ds == 0 )
   {
     std::cerr <<" CsDet::DecodeChipDigitMN " << GetTBName() << "  CS::ChipSADC::Digit expected but not the case ! " << std::endl;
   }
   const CS::ChipSADC::Digit  &digit = *ds;
   // Get cell position
   int32 x_digit = digit.GetX();
   int32 y_digit = digit.GetY();
   int  addr = digit.GetChannel();

   int icell = x_digit;
   // Get samples
   const std::vector<CS::uint16>& sample = digit.GetSamples();

   if ( debug ) {
        std::cout << " CsDet::DecodeChipDigitMN " << GetTBName() << " debug." << std::endl;
        std::cout << "  x_digit = " << x_digit << " y_digit = " << y_digit << std::endl;
        std::cout << "  sample size = " << sample.size() << " integral size = " << digit.GetIntegrals().size() << std::endl;
   }
   double signal= 0.;
   double time = 1000.;
   if( GetDigSADC().size() != 0 ) // Take priority to avoid extra options
   {
     if( debug ) std::cout <<"  " << GetTBName() <<" Ready to Use DigSADC " << GetDigSADC().size() << std::endl;
     if( icell < 0 || icell >= (int)GetDigSADC().size() )
     {
       std::cerr <<" CsDet::DecodeChipDigitMN " << GetTBName() << " Problem in DigSADC configuration icell " << icell <<" But DigSADC size = " << GetDigSADC().size() << std::endl;
       return;
     }

     if( GetDigSADC2Modify()[icell] == NULL )
     {
       std::cerr <<" CsDet::DecodeChipDigitMN " << GetTBName() <<" DigSADC fatal configuration problem for cell " << icell << std::endl;
       exit(1);
     }
     MN::DigSADC * dig = GetDigSADC2Modify()[icell];
     dig -> Fit(sample);
     const std::vector < MN::DigSADC::FitPar>  &signals = dig -> GetSignals();
     if( signals.size() > 0 )
     {
       signal = signals[0].signal;
       time = signals[0].time;
     }
     if( debug ) std::cout << " MiscDet::DecodeChipDigit " << GetTBName() << " Main signal " << signal << " time " << time << std::endl;
//      if( GetTBName() == std::string("S2") ) debug = true;
// ToDo apply calibrations
     for( int i=0; i<  (int)signals.size(); i++ )
     {
       double data[2];
       data[0]= GetEnergyCalibrated(icell, signals[i].signal);
       data[1]= GetTimeCalibrated(icell, signals[i].time);
       if( debug ) std::cout <<" cell " << icell <<" i hit " << i << " RawAmp " << signals[i].signal << " Amp " << data[0] << " RawTime " << signals[i].time<< " Time " << data[1] << std::endl;
       myDigits_.push_back( new CsDigit(*this, icell, data, 2) );
     }
     if( debug ) std::cout << " CsDet::DecodeChipDigitMN " << GetTBName() << " N CsDigits " << myDigits_.size() << std::endl;
   }
   else
   {
     cerr <<"  CsDet::DecodeChipDigitMN " << GetNameMN() <<" No DigSADC " << std::endl; 
     exit(1);
   }
}

// 
// void MiscDet::DecodeChipDigit(const CS::Chip::Digit &ddigit) 
// {
// //   bool debug = true;
//   bool debug = false;
//    const CS::ChipSADC::Digit  *ds = dynamic_cast<const CS::ChipSADC::Digit*>(&ddigit);
//    if( ds == 0 )
//    {
//      std::cerr <<" MiscDet::DecodeChipDigit " << GetTBName() << "  CS::ChipSADC::Digit expected but not the case ! " << std::endl;
//    }
//    const CS::ChipSADC::Digit  &digit = *ds;
//    // Get cell position
//    int32 x_digit = digit.GetX();
//    int32 y_digit = digit.GetY();
//    int  addr = digit.GetChannel();
// 
//    int icell = x_digit;
//    // Get samples
//    const std::vector<CS::uint16>& sample = digit.GetSamples();
// 
//    if ( debug ) {
//         std::cout << " MiscDet::DecodeChipDigit " << GetTBName() << " debug." << std::endl;
//         std::cout << "  x_digit = " << x_digit << " y_digit = " << y_digit << std::endl;
//         std::cout << "  sample size = " << sample.size() << " integral size = " << digit.GetIntegrals().size() << std::endl;
//    }
//    double signal= 0.;
//    double time = 1000.;
//    if( GetDigSADC().size() != 0 ) // Take priority to avoid extra options
//    {
//      if( debug ) std::cout <<"  " << GetTBName() <<" Ready to Use DigSADC " << GetDigSADC().size() << std::endl;
//      if( icell < 0 || icell >= (int)GetDigSADC().size() )
//      {
//        std::cerr <<" MiscDet::DecodeChipDigit " << GetTBName() << " Problem in DigSADC configuration icell " << icell <<" But DigSADC size = " << GetDigSADC().size() << std::endl;
//        return;
//      }
// 
//      if( GetDigSADC2Modify()[icell] == NULL )
//      {
//        std::cerr <<" MiscDet::DecodeChipDigit " << GetTBName() <<" DigSADC fatal configuration problem for cell " << icell << std::endl;
//        exit(1);
//      }
//      MN::DigSADC * dig = GetDigSADC2Modify()[icell];
//      dig -> Fit(sample);
//      const std::vector < MN::DigSADC::FitPar>  &signals = dig -> GetSignals();
//      if( signals.size() > 0 )
//      {
//        signal = signals[0].signal;
//        time = signals[0].time;
//      }
//      if( debug ) std::cout << " MiscDet::DecodeChipDigit " << GetTBName() << " Main signal " << signal << " time " << time << std::endl;
// //      if( GetTBName() == std::string("S2") ) debug = true;
// // ToDo apply calibrations
//      for( int i=0; i<  (int)signals.size(); i++ )
//      {
//        double data[2];
//        data[0]= GetEnergyCalibrated(icell, signals[i].signal);
//        data[1]= GetTimeCalibrated(icell, signals[i].time);
//        if( debug ) std::cout <<" cell " << icell <<" i hit " << i << " RawAmp " << signals[i].signal << " Amp " << data[0] << " RawTime " << signals[i].time<< " Time " << data[1] << std::endl;
//        myDigits_.push_back( new CsDigit(*this, icell, data, 2) );
//      }
//      if( debug ) std::cout << " MiscDet::DecodeChipDigit " << GetTBName() << " N CsDigits " << myDigits_.size() << std::endl;
//    }
//    else
//    {
//      cerr <<"  MiscDet::DecodeChipDigit " << GetNameMN() <<" No DigSADC " << std::endl; 
//      exit(1);
//    }
// 
// 
// 
// }
// 
////////////////////////////////////////////////////////////////////////////////
// size_t CsDet::NCellsMN( void ) const
// {
//   return ncells_mn_;
// }
// 
////////////////////////////////////////////////////////////////////////////////
// 
// void CsDet::SetNCellsMN( const size_t &ncells_mn )
// {
//   ncells_mn_=ncells_mn;
// }
// 
////////////////////////////////////////////////////////////////////////////////

void CsDet::MakeDigSADC ( void )
{
  bool debug = false;

  if( det_mn_ == 0 )
  {
    cerr <<" CsDet::MakeDigSADC Fatal no DetMN ! " << endl;
    exit(1);
  }
  if( GetDigSADC().size() > 0 ) {
    cerr <<" CsDet::makeDigSADC " << GetTBName() <<" For Nch " << det_mn_->NCellsMN() << " Double initialization probably! Nch = " << GetDigSADC().size() <<" Already !! " << endl;
    exit(1);
  }
  for( int ich=0; ich< (int)det_mn_->NCellsMN() ; ich++) {
    MN::DigSADC *d = new MN::DigSADC( MN::DigSADC::COMPASS_MSADC );
    d->SetDecodeVersion(1);
    GetDigSADC2Modify().push_back(d);
  }
  if( debug ) cout <<" CsDet::MakeDigSADC " << GetTBName() <<" debug OK nch = " << det_mn_->NCellsMN() << " NdigSADC = " << GetDigSADC().size() << endl;
}

////////////////////////////////////////////////////////////////////////////////

void CsDet::InitDetMN (int nch )
{
  bool debug = false;
  if( debug ) cout <<" CsDet::InitDetMN debug " << endl;
  if( det_mn_ == 0 ) det_mn_ = new MN::DetMN(GetTBName(), nch);
  std::vector<std::string> cell_names;
  for( int ic=0; ic< (int)det_mn_->NCellsMN(); ic++ )
  {
    char cell_name[132];
    sprintf(cell_name,"Cell_%d",ic);
    std::string scell_name = GetTBName()+std::string("_")+std::string(cell_name);
    cell_names.push_back(scell_name);
  }
  det_mn_->SetCellNames(cell_names);
  MakeDigSADC();
}

////////////////////////////////////////////////////////////////////////////////

void CsCalorimeter::InitDetMN ( int nch )
{
  bool debug = false;
  if( debug ) cout <<" CsCalorimeter::InitDetMN debug " << endl;
  if( det_mn_ == 0 ) det_mn_ = new MN::DetMN(GetTBName(), nch);
  std::vector<std::string> cell_names;
  for( int ic=0; ic< (int)det_mn_->NCellsMN(); ic++ )
  {
    cell_names.push_back(GetCellName(ic));
  }
  det_mn_->SetCellNames(cell_names);
  MakeDigSADC();

  if( XYRegularGrid() )
  {
    if( debug ) cout <<" Implement DetMN XY settings " << endl;
    int nx = GetNColumns();
    int ny = GetNRows();
    det_mn_->InitXY(nx,ny);
    for( int ic=0; ic< (int)NCells(); ic++ )
    {
      int x = GetColumnOfCell(ic);
      int y = GetRowOfCell(ic);
      det_mn_->SetCellXY( ic, x, y);
    }
  }
  if( debug )
  { 
    cout <<" CsCalorimeter::InitDetMN debug OK " << endl;
  }  
  
}

////////////////////////////////////////////////////////////////////////////////

double CsCalorimeter::GetEnergyCalibrated ( int icell, double amp ) const
{
  return SignalToEnergy(icell, amp, GetTimeInSpill());
}

////////////////////////////////////////////////////////////////////////////////

double CsCalorimeter::GetTimeCalibrated ( int icell, double time ) const
{
  return (time - cells_info[Reco::Calorimeter::TIME][OLD][icell].GetMean());
}

////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////

double CsDet::GetTimeCalibrated ( int icell, double time ) const
{
  if( det_mn_ != 0 ) return  det_mn_->GetTimeCalibrated(icell, time);
  return time;
}

////////////////////////////////////////////////////////////////////////////////

double CsDet::GetEnergyCalibrated ( int icell, double amp ) const
{
  if( det_mn_ != 0 ) return  det_mn_->GetEnergyCalibrated(icell, amp); 
  return amp;
}

////////////////////////////////////////////////////////////////////////////////

MN::DigSADC* CsDet::GetDigSADC2Modify ( int icell )
{ 
  if(icell <0 || icell >= (int)GetDigSADC().size() ) return 0;
  return GetDigSADC2Modify()[icell];
}

////////////////////////////////////////////////////////////////////////////////

const MN::DigSADC* CsDet::GetDigSADC ( int icell ) const 
{ 
  if(icell <0 || icell >= (int)GetDigSADC().size() ) return 0;
  return GetDigSADC()[icell];
}

// ////////////////////////////////////////////////////////////////////////////////
// 
// int CsDet::InputShapeTableSADC(const string &s)
// {
// 
//   MN::ManagerCellsShapeTableSADC mantab_cells_sadc;
// //  if( mantab_cells_sadc_ != NULL )
//   { 
// //    int iret = mantab_cells_sadc_->InputShapeTableSADC(s);
//     int iret = mantab_cells_sadc.InputShapeTableSADC(s);
//     if( iret == 0 )
//     {
//       for(  size_t ic=0; ic < NCellsMN(); ic++ )
//       {
//         MN::DigSADC *dg = GetDigSADC2Modify( ic );
//         if( dg != 0 )
//         {
// //          const std::map<double,double> *tab = mantab_cells_sadc_->GetTab(ic);
//           const std::map<double,double> *tab = mantab_cells_sadc.GetTab(ic);
//           dg->SetTab(tab);
//           dg->SetDecodeVersion(4); // Once we read the table we will use the table
//         }
//       }
//     }
//     return iret;
//   }   
//   return 0;
// }
// 
// ////////////////////////////////////////////////////////////////////////////////
// 
// int CsDet::InputSADCInfo( const string &s)
// {
// //   bool debug = true;
//   bool debug = false;
// //  bool ignore_cell_name_check = true;
//   
// //   if( debug )
// //   { 
// //     cout << " Det::InputSADCInfo " << GetNameMN() << " debug  XY?= " << XYRegularGridMN() << endl;
// //     cout << s;
// //   }
// //   if( XYRegularGridMN() )
// //   {
// //     return InputSADCInfoXY( s );
// //   }
//   istringstream is(s.c_str());
//   char calorim_name[132],dummy[132],cellname[132];
//   string str;
// 
// //  Read line of comments
//   getline(is,str);
//   getline(is,str);
//   sscanf(str.c_str(),"%s %s ",calorim_name,dummy);  
//   getline(is,str);
// 
//   getline(is,str);
// 
//   int not_matched_cells_cnt = 0;
//   while( getline(is,str) )
//   {
//     // there is one type of calibration with 9 rows of data, and one with 7
//     // call the one with 7 the old one
// //    bool oldformat(false);
// 
//     int icell,idsadc, iledev;
//     float ped, dped, convert, unknown_entry_a, unknown_entry_b;
// 
//     if ( sscanf(str.c_str()," %d %d %d %g %g %g %g %g %s \n", &icell, &idsadc, &iledev,  &ped,  &dped,  &convert, &unknown_entry_a, &unknown_entry_b, cellname)!=9 )
//     {
// //      oldformat = true;
// 
//       int ret = sscanf(str.c_str()," %d %d %d %g %g %g %s \n", &icell, &idsadc, &iledev,  &ped,  &dped,  &convert, cellname );
//       if( ret != 7 )
//       {
//         cerr <<" CalorimeterMN::InputSADCInfo format error " << endl;
// 	cerr << str << endl;
// 	exit(1);
//       }
// //      assert(ret==7);
//     }
// 
// //     if( !ignore_cell_name_check )
// //     {
// //       int jcell = FindCellByNameMN( cellname );
// //       if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
// //       if( !(jcell >=0 && jcell < (int)NCellsMN()) )
// //       {
// //         cerr << " Unexpected Cell name " << cellname << " in CalorimeterMN::InputSADCInfo " <<
// //                                                                         GetNameMN() << "  " << endl;
// //         cerr << " Input strig: " << endl;
// //         cerr << str << endl;
// //         cerr << " It well might be that you use wrong calibration file for this detector " << GetNameMN() << endl;    
// //         stringstream ss;
// //         ss << " Unexpected Cell name " << GetNameMN() << " CalorimeterMN::InputSADCInfo " << str;
// //         cerr << ss.str() << endl;
// //         exit(1);
// // //      throw Exception(ss.str().c_str());
// //       }
// // 
// //       if( icell != jcell )
// //       {
// //         icell = jcell;
// //         not_matched_cells_cnt ++;
// //       }
// //     }
//     
//     MN::DigSADC *dig_sadc = GetDigSADC2Modify( icell );
// 
//     if( dig_sadc != NULL )
//     {
// //       double ped_odd = ped - dped;
// //       double ped_even = ped + dped;
//       double ped_odd = ped  + dped;
//       double ped_even = ped - dped;
// //       double dpedd = dped;
// //       double cfcv = convert;
//     
// //      dig_sadc->SetCalibInfo( ped_odd, ped_even, dpedd, cfcv);
//       dig_sadc->SetCalib( ped_odd, ped_even);
//     }
//   }
//   if( not_matched_cells_cnt > 0 )
//   {
//     cerr << " WARNING!!! CalorimeterMN::InputSADCInfo " << GetNameMN() <<
//                " Not matching in cells id was detected " << not_matched_cells_cnt << " times " << endl;
//     cerr << " You use wrong calibaration file or calibrations were produced with different geometry descriptor !!! " << endl;
//     exit(1);
//   }
// 
//   return 0;
// }
// 
////////////////////////////////////////////////////////////////////////////////
// 
// int CsDet::InputSADCInfoXY( const string &s)
// {
// //   bool debug = true;
//   bool debug = false;
//   if( !XYRegularGridMN() )
//   {
//     cerr <<" Det::InputSADCInfoXY called in " << GetNameMN() <<" but No XY structure in det " << endl;
//     exit(1);
//   }
//   if( debug )
//   { 
//     cout << " Det::InputSADCInfoXY " << GetNameMN() << " debug " << endl;
//     cout << s;
//   }
//   istringstream is(s.c_str());
//   char calorim_name[132],dummy[132],cellname[132];
//   string str;
// 
// //  Read line of comments
//   getline(is,str);
//   getline(is,str);
//   sscanf(str.c_str(),"%s %s ",calorim_name,dummy);  
//   getline(is,str);
// 
//   getline(is,str);
// 
//   int not_matched_cells_cnt = 0;
//   while( getline(is,str) )
//   {
//     // there is one type of calibration with 9 rows of data, and one with 7
//     // call the one with 7 the old one
// //    bool oldformat(false);
// 
//     int icell, x, y, idsadc, iledev;
//     float ped, dped, convert, unknown_entry_a, unknown_entry_b;
//     int iret = sscanf(str.c_str()," %d %d %d %d %g %g %g %g %g %s \n", &x, &y, &idsadc, &iledev,  &ped,  &dped,  &convert, &unknown_entry_a, &unknown_entry_b, cellname);
//     if( iret != 10 )
//     {
// //      oldformat = true;
// 
// //       int ret = sscanf(str.c_str()," %d %d %d %g %g %g %s \n", &icell, &idsadc, &iledev,  &ped,  &dped,  &convert, cellname );
// //       if( ret != 7 )
// //       {
//         cerr <<" Det::InputSADCInfoXY format error " << endl;
// 	cerr << str << endl;
// 	exit(1);
// //       }
// //      assert(ret==7);
//     }
// 
//     icell = GetCellOfColumnRowMN( x, y);
//     if( icell < 0 ) continue;
//     if(debug) cout << " Cell x=" << x << " y=" << y <<" icell " << icell <<" True Cell name " << GetCellNameMN(icell) << " But we are looking for:" << std::string(cellname) << endl;
//     if( icell < 0 ) continue;
// 
//     int jcell = FindCellByNameMN( std::string(cellname) );
//     if(debug) cout << " Cell name " << cellname << " jcell " << jcell << endl;
//     if( !(jcell >=0 && jcell < (int)NCellsMN()) )
//     {
//       cerr << " Unexpected Cell name " << cellname << " in CalorimeterMN::InputSADCInfo " <<
//                                                                         GetNameMN() << "  " << endl;
//       cerr << " Input strig: " << endl;
//       cerr << str << endl;
//       cerr << " It well might be that you use wrong calibration file for this detector " << GetNameMN() << endl;    
//       stringstream ss;
//       ss << " Unexpected Cell name " << GetNameMN() << " CalorimeterMN::InputSADCInfo " << str;
//       cerr << ss.str() << endl;
//       exit(1);
// //      throw Exception(ss.str().c_str());
//     }
// 
//     if( icell != jcell )
//     {
//       icell = jcell;
//       not_matched_cells_cnt ++;
//     }
//     
//     MN::DigSADC *dig_sadc = GetDigSADC2Modify( icell );
// 
//     if( dig_sadc != NULL )
//     {
// //       double ped_odd = ped - dped;
// //       double ped_even = ped + dped;
//       double ped_odd = ped + dped;
//       double ped_even = ped - dped;
// //       double dpedd = dped;
// //       double cfcv = convert;
//     
// //      dig_sadc->SetCalibInfo( ped_odd, ped_even, dpedd, cfcv);
//        dig_sadc->SetCalib( ped_odd, ped_even);
//    }
//   }
//   if( not_matched_cells_cnt > 0 )
//   {
//     cerr << " WARNING!!! Det::InputSADCInfoXY " << GetNameMN() <<
//                " Not matching in cells id was detected " << not_matched_cells_cnt << " times " << endl;
//     cerr << " You use wrong calibaration file or calibrations were produced with different geometry descriptor !!! " << endl;
//     exit(1);
//   }
// 
//   return 0;
// }
// 
////////////////////////////////////////////////////////////////////////////////


