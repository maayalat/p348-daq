// Options file for running "Linux/align", i.e. the 2nd stp of the
// alignment procedure, on the ROOT TTree output of "Linux/traf" (1st step).
// Specific for the adjustment of the beamTelescope.
//  - Offset, Theta and Pitch adjustment.

// To be edited to specify:
//   I) detectors.dat
//  II) Input ROOT file
// III) Output
//  IV) Tracks selection (depends upon whether longitudinal or transverse)

//=== magnetic field switch. 
//=== WARNING: This option must match that in traf options file used to generate the tree.
main magnets on    // (on|off) default (i.e. no option) is off.

//=== parameters to align (comment the line if parameter is not to be fitted)
align  U	// Offset in U
align  T	// Angle perp to the beam
align  P	// Pitch <- u*=(1+pitch_offset)
//align  Z	// Position along the beam
//align  R	// Prop to T0 for drift like detectors
//align  L	// Lorentz angle R scaling factor for drift like detectors

align nStdDev   3     // cut track selection
align nTracks   0     // number of selected tracks (0 means all)
align projRange 2     // minimum angle diff to distinguish orientations (deg)
make iterations       // Millepede iterations

//=== Debugging options
//debug dump dets
debug dump millepede
debug counter rate 5000

//=== Detector to be considered/excluded 
//=== (WARNING excluded takes precedence over used)
//=== TBNames can be abreviated. symbol '*' can replace any _single_ character.
align excludeDets FI03 FI04 FI05 FI55 FI06 FI07 FI08
align useDets FI SI

//=== overwrites the det.dat resolutions
//=== TBNames can be abreviated. symbol '*' can replace any _single_ character.
// align change resolution MA 2.89 
// align change resolution MB 9.67
// align change resolution DW 1.5

//=== parameters to fix
//=== TBNames can be abreviated
fix U FI01
fix T FI01
fix P FI01
//fix Z	*				// Z is not adjusted at all. Cf. supra.

//=== Detector table
detector table	~ybedfer/public/2017/detectors.278473.mu+.dat

//=== Rootfiles
//=== WildCards are accepted
input tree	/eos/user/y/ybedfer/2017/traf.beamT.12004-278473.root

//=== Output file
output file align.beamT.12004-278473.out

//=== track selection cut 
selection  cut  (T_zone==23&&T_chi2/T_ndf<15&&T_cop&&1/T_cop>140)
// Note: The entry infra was meant to replace the one supra in the transverse
//      case. But the rationale behind it is not clear: why couldn't we require
//      the halo tracks to have a momentum greater than some cut in the that
//      case? Anyway, in order to align "2007/detectors.59963.transv.dat,v1.3",
//      I (Y.B.) decided to disregard it. 
//selection  cut  T_zone==23T_chi2/T_ndf<15&&T_cop!=0	// Transverse case ???
