#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class Align;
#pragma link C++ class CheckTracks;
#pragma link C++ class DrawTracks3D;
#pragma link C++ class Tracks;
#pragma link C++ class DetFileManager-;
#pragma link C++ class DetectorInfo;
#pragma link C++ class DeadZoneInfo;
#pragma link C++ class MagnetInfo;
#pragma link C++ class Macro;
#pragma link C++ class Fit;
#pragma link C++ class TH2Fit;
#pragma link C++ class Utils;
#endif
