dnl Process this file with autoconf to produce a configure script.
AC_INIT
AC_CONFIG_HEADER(src/config.h)

dnl ****************************************************************************
dnl Macros
dnl ****************************************************************************

AC_DEFUN([MY_ERROR],
[
  echo
  echo 'x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x'
  echo 'x x x      DaqDataDecoding Configuration failure      x x x'
  echo '        '$1
  echo 'x x x x x x x x x x x x x x x x x x x x x x x x x x x x x x'
  echo
  exit 1
])


dnl AC_SET_PACKAGE(package_name,env_variable,help_string,default_value)
dnl On output:
dnl variable USE_package_name will be set
dnl variable DIR_package_name will be non-empty for particular check
AC_DEFUN([AC_SET_PACKAGE],
[
  DIR_$1=
  AC_ARG_WITH($1,[$3],,with_$1=$4)
  if test "${with_$1}" = "no" || test "${with_$1}" = ""; then
    # Package is disable
    echo Package $1 is disable
    USE_$1=no
    dnl AC_DEFINE(USE_$1,0)
  else
    USE_$1=yes
    dnl AC_DEFINE(USE_$1,1)
    # Package is enable
    AC_CHECKING($1)
    if test "${with_$1}" = "yes"; then
      # Try to search in standard places.
      # At first we check environment variable $2
      AC_MSG_CHECKING(environment variable $2)
      if test "$$2" = ""; then
        AC_MSG_RESULT(variable was not set)
      else
        # The environment variable is defined. We shall use it.
        AC_MSG_RESULT($$2)
        with_$1=$$2
      fi
    else
      # Exact value was given: --with-package=/dir1/dir2/
      true
    fi
    
    if test "${USE_$1}" = "yes" && test "${with_$1}" != "yes"; then
      DIR_$1=${with_$1}
    fi
  fi
  AC_SUBST(USE_$1)
  AC_SUBST(DIR_$1)
  AC_SUBST(LIB_$1)
  AC_SUBST(INC_$1)
])

dnl ****************************************************************************
dnl Create shared or static library
dnl ****************************************************************************

AC_ARG_ENABLE(shared, AS_HELP_STRING([--enable-shared], [build shared library; default is static library]))
if test "${enable_shared}" = "yes" ; then
  LIB_TYPE=shared
else
  LIB_TYPE=static
fi
AC_SUBST(LIB_TYPE)

dnl ****************************************************************************

dnl Checks for programs.
AC_PROG_INSTALL

dnl Check for compilers

AC_PROG_CXX
AC_PROG_CXXCPP

dnl Do compilation using C++ 
AC_LANG_CPLUSPLUS 

dnl Set defalt compilation flags (assuming g++)
CXXFLAGS="-O2 -Wall -Wextra -Wno-unused -Wno-unused-parameter"

dnl ****************************************************************************
dnl EXPAT
dnl ****************************************************************************

AC_SET_PACKAGE(EXPAT, EXPAT, AS_HELP_STRING([--with-EXPAT], [enable on default]), "yes")
if test "$USE_EXPAT" = "yes"; then
  if test "$DIR_EXPAT" != ""; then
    AC_CHECK_HEADER($DIR_EXPAT/include/expat.h,INC_EXPAT=$DIR_EXPAT/include,MY_ERROR(Cannot find EXPAT include file expat.h in $DIR_EXPAT/include.))
    CXXFLAGS="$CXXFLAGS -I$INC_EXPAT"
    if uname -m | grep -q -e "64" ; then
      AC_CHECK_FILE($DIR_EXPAT/lib64/libexpat.so,LIB_EXPAT="$DIR_EXPAT/lib64/libexpat.so",
        AC_CHECK_FILE($DIR_EXPAT/lib/libexpat.so,LIB_EXPAT="$DIR_EXPAT/lib/libexpat.so",
          MY_ERROR(Cannot find EXPAT library in $DIR_EXPAT/lib64 or $DIR_EXPAT/lib.)))
    else
      AC_CHECK_FILE($DIR_EXPAT/lib/libexpat.so,,MY_ERROR(Cannot find EXPAT library in $DIR_EXPAT/lib.))
      LIB_EXPAT="$DIR_EXPAT/lib/libexpat.so"
    fi
  else
    AC_CHECK_HEADER(expat.h,,MY_ERROR(Cannot find EXPAT include file expat.h in default include directories.))
    AC_CHECK_LIB(expat,XML_ParserCreate,LIB_EXPAT="-lexpat",MY_ERROR(Cannot find EXPAT library in default library directories.))
  fi
fi

dnl ****************************************************************************
dnl DATE_LIB
dnl ****************************************************************************

AC_SET_PACKAGE(DATE_LIB, DATE_LIB, AS_HELP_STRING([--with-DATE_LIB], [disable on default]), "no")
if test "$USE_DATE_LIB" = "yes"; then
  if test "$DIR_DATE_LIB" = ""; then
    DIR_DATE_LIB=/afs/cern.ch/compass/online/daq/dateV371
  fi
  AC_CHECK_FILE($DIR_DATE_LIB,,MY_ERROR(Problem with DATE library))

  dnl Check Red Hat release and choose lib directory.
  dnl Set default path

  sys_name=Linux

dnl  if grep --quiet "5." /etc/redhat-release; then
dnl    sys_name=Linux.orig
dnl  fi

  AC_CHECK_FILE($DIR_DATE_LIB/lib/libmonitor.a,,MY_ERROR(Problem with DATE library))

  LIB_DATE_LIB="$DIR_DATE_LIB/lib/libmonitor.a"
  LIBS="$LIBS $LIB_DATE_LIB"
  CXXFLAGS="$CXXFLAGS -I$DIR_DATE_LIB/include"
  AC_DEFINE(USE_DATE_LIB,1)
else
  AC_DEFINE(USE_DATE_LIB,0)
fi

dnl ****************************************************************************
dnl XROOTD
dnl No default is provided
dnl => The user has to specify a directory (which is typically "$ROOTSYS/lib")
dnl ****************************************************************************

AC_SET_PACKAGE(XROOTD, DUMMY_ENV, AS_HELP_STRING([--with-XROOTD], [libXrdPosix directory]), "no")
if test "$USE_XROOTD" = "yes"; then
  if test "$DIR_XROOTD" = ""; then
    echo No directory specified as argument to --with-XROOTD
  fi

  AC_CHECK_FILE($DIR_XROOTD,,MY_ERROR(Cannot find libXrdPosix directory))
  AC_CHECK_FILE($DIR_XROOTD/libXrdPosix.so,,MY_ERROR(Cannot find libXrdPosix))
  LIB_XROOTD=$DIR_XROOTD/libXrdPosix.so
  LIBS="$LIBS $LIB_XROOTD"
  AC_DEFINE(USE_XROOTD,1)
else
  AC_DEFINE(USE_XROOTD,0)
fi


dnl ****************************************************************************
dnl RFIO
dnl ****************************************************************************

AC_SET_PACKAGE(RFIO, RFIO, AS_HELP_STRING([--with-RFIO], [disable on default]), "no")
if test "$USE_RFIO" = "yes"; then
  if test "$DIR_RFIO" = ""; then
    mandatory_package=no
    DIR_RFIO=/usr
  else
    mandatory_package=yes
  fi
fi

if test "$USE_RFIO" = "yes"; then
  AC_CHECK_FILE($DIR_RFIO/include/shift.h,,
    if test "$mandatory_package" = "yes"; then
      MY_ERROR(Can not find RFIO include file shift.h)
    else
      echo RFIO package was not found
      DIR_RFIO=
      USE_RFIO=no
    fi
  )
fi

if test "$USE_RFIO" = "yes" ; then
  if [[ "$(uname -m)" != "i686" ]] ; then
    AC_CHECK_FILE($DIR_RFIO/lib64/libshift.so,LIB_RFIO=$DIR_RFIO/lib64/libshift.so,
      AC_CHECK_FILE($DIR_RFIO/lib64/libshift.so.2.1,LIB_RFIO=$DIR_RFIO/lib64/libshift.so.2.1,
        AC_CHECK_FILE($DIR_RFIO/lib64/libshift.a,LIB_RFIO=$DIR_RFIO/lib64/libshift.a, MY_ERROR(Can not find RFIO library)))
    )
    AC_CHECK_FILE($DIR_RFIO/lib64/libcastorrfio.so,LIB_RFIO="$LIB_RFIO $DIR_RFIO/lib64/libcastorrfio.so",)
  else
    AC_CHECK_FILE($DIR_RFIO/lib/libshift.so,LIB_RFIO=$DIR_RFIO/lib/libshift.so,
      AC_CHECK_FILE($DIR_RFIO/lib/libshift.so.2.1,LIB_RFIO=$DIR_RFIO/lib/libshift.so.2.1,
        AC_CHECK_FILE($DIR_RFIO/lib/libshift.a,LIB_RFIO=$DIR_RFIO/lib/libshift.a,MY_ERROR(Can not find RFIO library)))
    )
    AC_CHECK_FILE($DIR_RFIO/lib/libcastorrfio.so,LIB_RFIO="$LIB_RFIO $DIR_RFIO/lib/libcastorrfio.so",)
  fi

  if test "$DIR_RFIO" != "/usr/"; then
    CXXFLAGS="$CXXFLAGS -I$DIR_RFIO/include"
  fi
  # recent versions (starting from about 2014) of the library have an issue
  # with not being able to correctly track whether to include a header file
  # again in shift/serrno.h. as this header file is in the shift sub-directory,
  # and includes further files using <>, the shift directory has also to be
  # added
  CXXFLAGS="$CXXFLAGS -I$DIR_RFIO/include/shift"
  LIBS="$LIBS $LIB_RFIO"
  AC_DEFINE(USE_RFIO,1)
else
  dnl check is removed for "standalone" (without RFIO) build of DATE
  dnl if test "$USE_DATE_LIB" = "yes"; then
  dnl   MY_ERROR(You can not use DATE library without RFIO one)
  dnl fi
  AC_DEFINE(USE_RFIO,0)
fi

dnl ****************************************************************************
dnl ****************************************************************************
dnl ****************************************************************************

AC_CONFIG_FILES(["Makefile.inc"])
AC_OUTPUT

echo
echo '*************************************************'
echo '***   DaqDataDecoding configuration success   ***'
echo '*************************************************'
echo
