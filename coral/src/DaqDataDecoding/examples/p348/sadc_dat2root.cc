// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
#include "Chip.h"
#include "ChipSADC.h"
using namespace CS;

// c++
#include <iostream>
using namespace std;

// ROOT
#include "TTree.h"
#include "TFile.h"
#include "TInterpreter.h"
#include "TSystem.h"

int main(int argc, char *argv[])
{
  if (argc != 3) {
    cerr << "Usage: ./sadc_dat2root input.dat output.root" << endl;
    return 1;
  }
  
  // setup read of input .dat file
  DaqEventsManager manager;
  manager.SetMapsDir("../../../../../maps");
  manager.AddDataSource(argv[1]);
  manager.Print();
  
  // prepare output .root file
  TFile f(argv[2], "RECREATE");
  
  Int_t event;
  Double_t time;
  char detector[100];
  Int_t x;
  Int_t y;
  Int_t nsamples;
  UShort_t samples[100];
  
  TTree t("SADC", "SADC");
  t.Branch("event", &event, "event/I");
  t.Branch("time", &time, "time/D");
  t.Branch("detector", detector, "detector/C");
  t.Branch("x", &x, "x/I");
  t.Branch("y", &y, "y/I");
  t.Branch("nsamples", &nsamples, "nsamples/I");
  t.Branch("samples", samples, "samples[nsamples]/s");
  
  // event loop, read next event
  while (manager.ReadEvent()) {
    // print progress each 1000 events
    if (manager.GetEventsCounter() % 1000 == 1)
      cout << "===> Event #" << manager.GetEventsCounter() << endl;
    
    // print event header information
    //manager.GetEvent().GetHeader().Print();
    
    // skip auxialy (non-physics) events
    const DaqEvent::EventType type = manager.GetEvent().GetType();
    if (type != DaqEvent::PHYSICS_EVENT) continue;
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    //cout << "Decode: " << (decoded ? "OK" : "failed") << endl;
    
    // skip events with decoding problems
    if (!decoded) continue;
    
    // set tree variables
    event = manager.GetEventsCounter();
    time = manager.GetEvent().GetTime().first + (manager.GetEvent().GetTime().second / 1.0e6);
    
    // pprocess digits from all detectors
    typedef Chip::Digits::const_iterator Digi_it;
    for (Digi_it i = manager.GetEventDigits().begin(); i != manager.GetEventDigits().end(); ++i ) {
      const Chip::Digit* d = i->second;
      //d->Print();
      
      // skip data from non-SADC detectors
      const ChipSADC::Digit* sadc = dynamic_cast<const ChipSADC::Digit*>(d);
      if (sadc == 0) continue;
      
      // description of ChipSADC::Digit:
      // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipSADC.h#L150
      
      //sadc->Print();
      
      strcpy(detector, sadc->GetDetID().GetName().c_str());
      x = sadc->GetX();
      y = sadc->GetY();
      
      const vector<uint16>& wave = sadc->GetSamples();
      nsamples = wave.size();
      for (int i = 0; i < nsamples; ++i) samples[i] = wave[i];
      
      // fill tree
      t.Fill();
    }
  }
  
  f.Write();
  
  cout << "End of the event loop" << endl;
  
  // print summary
  CS::Exception::PrintStatistics();
  CS::Chip::PrintStatistics();
  
  return 0;
}
