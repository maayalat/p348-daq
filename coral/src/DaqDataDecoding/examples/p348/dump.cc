// DaqDataDecoding
#include "DaqEventsManager.h"
#include "DaqEvent.h"
#include "Chip.h"
#include "ChipSADC.h"
#include "ChipAPV.h"
#include "ChipF1.h"
#include "ChipNA64TDC.h"
#include "ChipNA64WaveBoard.h"
#include "ChipV260.h"
using namespace CS;


// c++
#include <iostream>
#include <iomanip>
#include <vector>
#include <bitset>
using namespace std;

int main(int argc, char *argv[])
{
  if (argc != 3) {
    cerr << "Usage: ./dump <data file name> <nevents>" << endl;
    return 1;
  }
  
  DaqEventsManager manager;
  
  // purpose of map files is to set correspondance between input channels of
  // front-end electronics cards and experiment detectors channel
  manager.SetMapsDir("../../../../../maps");
  
  // read the data file which is defined on the command line
  manager.AddDataSource(argv[1]);
  
  // print file read settings
  manager.Print();
  
  const int maxevents = atoi(argv[2]);
  
  // explicitly set time zone of NA64 experiment location (CERN-Prevessin / France)
  // to generate timestamp string by strftime() in NA64-local time units
  setenv("TZ", "Europe/Paris", 1);
  
  // event loop, read event by event
  while (manager.ReadEvent()) {
    const int nevt = manager.GetEventsCounter();
    cout << "===> Event #" << nevt << endl;
    
    // print event header information
    //manager.GetEvent().GetHeader().Print();
    //manager.GetEvent().Print();
    
    const DaqEvent& e = manager.GetEvent();
    const DaqEvent::Header h = e.GetHeader();
    const uint32* buf = e.GetBuffer();
    
    //const bool isCalibration = (e.GetType() == CS::DaqEvent::CALIBRATION_EVENT);
    const int nheadwords = e.GetHeaderLength() / sizeof(*buf); // (normally 17*words = 68 bytes)
    
    // event time stamp, available only with precision of 1 second
    // time in seconds since "unix epoch" - https://en.wikipedia.org/wiki/Unix_time
    // NOTE: the value itself is added to event record at spillbuffer computer (just localtime)
    //       after the (event building at MUX + fifo + transfer), so some 10-100ms of jitter is very possible
    const time_t time = e.GetTime().first;
    char timestring[50];
    strftime(timestring, 50, "%Y-%m-%d %H:%M:%S %Z", localtime(&time));
    
    cout << "Event:"
         << " run=" << e.GetRunNumber()
         << " spill=" << e.GetBurstNumber()
         << " event=" << e.GetEventNumberInBurst()
         << " size=" << e.GetLength() //<< " bytes"
         << " type=" << e.GetType() << " (" << DaqEvent::stringEventType(e.GetType()) << ")"
         << " time=" << time << " (" << timestring << ")"
         << endl
         << "Trigger:"
         << " trigger=" << e.GetTrigger()
         << " mask=" << hex << "0x" << e.GetTriggerMask() << dec
         << endl
         << "Header:"
         << " size=" << e.GetHeaderLength() //<< " bytes"
         << " sizewords=" << nheadwords //<< " words"
         << " version=" << h.GetVersion() << " (" << hex << "0x" << h.GetVersion() << dec << ")"
         << " is_old=" << h.IsHeaderOld()
         << endl;
    
    // print header dump
    cout << "Header dump:";
    for (int i = 0; i < nheadwords; ++i)
      cout << hex << setw(8) << buf[i] << dec << " ";
    cout << endl;
    
    /*
    // print run-dependent metadata
    cout << "maps_of_the_run = " << endl;
    manager.GetMaps().Print();
    cout << "opts_of_the_run = " << endl;
    manager.GetDaqOptions().Print();
    cout << "detectors_all = ";
    for (auto& i : manager.GetDetectors())
      cout << i << " ";
    cout << endl;
    */
    
    // decode event (prepare digis)
    const bool decoded = manager.DecodeEvent();
    
    // skip events with decoding problems
    if (!decoded) {
      cout << "WARNING: fail to decode event #" << nevt << endl;
      continue;
    }
    
    cout << "Chips:" << endl;
    for (Chip* i : manager.GetEvent().GetChips()) {
      cout << "chip srcid=" << i->GetSourceID()
           << " name=" << i->GetName()
           << " GetDataStart=" << i->GetDataStart()  // chip payload data
           << " GetDataSize=" << i->GetDataSize() << "(words)"
           << endl;
      //i->GetSLink().Print();
      //i->Print();
      //i->Dump();
      
      if (i->GetSourceID() == 500) {
        // the "sourceID=500" is data block of scaler counter
        //i->Dump();
        
        // pcdmfs01:/online/RCCARS/compass-rccars-daq/compass-rccars-daq-slavereadout/
        //   eventcontainer.*, eventprocessorthread.*, scalerdata.h
        
        const uint32_t* data = i->GetDataStart();
        // data block format, 64 uint32 words:
        //  3 words: header {time, 0, spill}
        // 16 words: V260 counters, 16 channels of 24bits each
        // 45 words: dummy block, all zero
        
        // TODO: investigate the 'time' value is ~120 seconds ahead of the time of the first event in the spill
        
        //for (int j = 0; j < 64; ++j)
        //  cout << data[j] << endl;
        
        /*
        cout << "HEADER:" << endl;
        for (int j = 0; j < 3; ++j) {
          const std::bitset<32> bits = data[j];
          cout << bits << " = " << data[j] << endl;
        }
        */
        
        //cout << "COUNTERS: " << endl;
        const uint32_t* counters = data + 3;
        
        // the processing/printout of 'counters' is ommited here in the favore of regular ChipV260 processing
      }
    }
    
    // print digits from all detectors
    cout << "Digits:" << endl;
    typedef Chip::Digits::const_iterator Digi_it;
    for (Digi_it i = manager.GetEventDigits().begin(); i != manager.GetEventDigits().end(); ++i ) {
      const Chip::Digit* d = i->second;
      //d->Print();
      
      
      const ChipSADC::Digit* sadc = dynamic_cast<const ChipSADC::Digit*>(d);
      if (sadc) {
        // this is a digit from sampling ADC front-end (calorimeters)
        // following is the description of the data available in the digit
        // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipSADC.h#L202
        
        // sadc->Print();
        
        // print detector name, coordinates of detector channel and recorded waveform of the signal
        cout << "(SADC) detector=" << sadc->GetDetID().GetName()
             << " x=" << sadc->GetX()
             << " y=" << sadc->GetY()
             << " srcid=" << sadc->GetSourceID()
             << " port=" << sadc->GetPort()
             << " chip=" << sadc->GetChip()
             << " channel=" << sadc->GetChannel()
             << " data=";
        
        const vector<uint16>& wave = sadc->GetSamples();
        
        for (size_t i = 0; i < wave.size(); ++i)
          cout << wave[i] << " ";
        
        cout << endl;
      }
      
      const ChipNA64WaveBoard::Digit* wb = dynamic_cast<const ChipNA64WaveBoard::Digit*>(d);
      if (wb) {
        // this is a digit from sampling ADC front-end (calorimeters) --> WaveBoard
        // following is the description of the data available in the digit
        // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipNA64WaveBoard.h#L202

        // sadc->Print();

        // print detector name, coordinates of detector channel and recorded waveform of the signal
        cout << "(WB) detector=" << wb->GetDetID().GetName()
             << " x=" << wb->GetX()
             << " y=" << wb->GetY()
             << " srcid=" << wb->GetSourceID()
             << " port=" << wb->GetPort()
             << " chip=" << wb->GetChip()
             << " channel=" << wb->GetChannel()
             << " data=";

        const vector<uint16>& wave = wb->GetSamples();

        for (size_t i = 0; i < wave.size(); ++i)
          cout << wave[i] << " ";

        cout << endl;
      }

      const ChipF1::Digit* f1 = dynamic_cast<const ChipF1::Digit*>(d);
      if (f1) {
        // a digit from F1 front-end (Straw)
        // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipF1.h#L51
        
        //f1->Print();
        
        cout << "(F1) detector=" << f1->GetDetID().GetName()
             << " srcid=" << f1->GetSourceID()
             << " port=" << f1->GetPort()
             << " channel=" << f1->GetChannel()
             << " channelpos=" << f1->GetChannelPos()
             << " wire=" << f1->GetWire()
             << " time=" << f1->GetTime()
             << " tunit=" << f1->GetTimeUnit()
             << " time_decoded=" << f1->GetTimeDecoded()
             << " time_reference=" << f1->GetTimeReference()
             << endl;
      }
      
      const ChipNA64TDC::Digit* tdc = dynamic_cast<const ChipNA64TDC::Digit*>(d);
      if (tdc) {
        // a digit from NA64TDC front-end (Straw)
        // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipNA64TDC.h#L51
        
        //tdc->Print();
        
        cout << "(NA64TDC) detector=" << tdc->GetDetID().GetName()
             << " srcid=" << tdc->GetSourceID()
             << " port=" << tdc->GetPort()
             << " channel=" << tdc->GetChannel()      // electronics channel
             << " wire=" << tdc->GetWire()            // detector channel
             << " time=" << tdc->GetTime()
             << " tunit=" << tdc->GetTimeUnit()
             << endl;
      }
      
      const ChipAPV::Digit* apv = dynamic_cast<const ChipAPV::Digit*>(d);
      if (apv) {
        // a digit from APV front-end (Micromega)
        // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipAPV.h#L50
        
        //apv->Print();
        
        // APV readout always have 3 samples
        const uint32* A = apv->GetAmplitude();
        
        cout << "(APV) detector=" << apv->GetDetID().GetName()
             << " srcid=" << apv->GetSourceID()
             << " adcid=" << apv->GetAdcID()
             << " chip=" << apv->GetChip()
             << " channel=" << apv->GetChipChannel() // electronics channel
             << " wire=" << apv->GetChannel()        // detector channel
             << " amplitude[]=" << A[0] << " " << A[1] << " " << A[2]
             << " timetag=" << apv->GetTimeTag()
                // time since previous event in the units of TCS clock 38.88 MHz, fragile:
                // NOTE: the field in APVADC hardware is only 15 bits and will overoll (zero-start) after 2^15 * 1/38.88MHz = 0.8ms
                // NOTE: the max value is 2^15 =~32000, but actually returned value of GetTimeTag() could be much larger like 300000, data corruption or some decoding issue?
             
             << " is_sparse=" << apv->IsSparsifed()     // mode: Sparse, Latch All
                // Latch All is pedestal mode, no baseline and sigma suppression
                // Sparse is normal data taking mode, with baseline and sigma suppression, amplitude of first and second sample are truncated by two bits
             << endl;
      }
      
      const ChipV260::Digit* v260 = dynamic_cast<const ChipV260::Digit*>(d);
      if (v260) {
        // a digit from CAEN V260 counter card
        cout << "(V260) detector=" << v260->GetDetID().GetName()
             << " srcid=" << v260->GetSourceID()
             << " channel=" << v260->GetChannel()
             << " name=" << v260->name
             << " counter=" << v260->counter
             << endl;
      }
      
      const bool isKnown = sadc || wb || f1 || tdc || apv || v260;
      if (!isKnown) {
        cout << "(unknown) detector=" << d->GetDetID().GetName()
             << endl;
        d->Print();
      }
    }
    
    // process only several first events
    if (nevt > maxevents) break;
  }
  
  cout << "End of the event loop" << endl;
  
  // print events read and decoding summary
  CS::Exception::PrintStatistics();
  CS::Chip::PrintStatistics();
  
  return 0;
}
