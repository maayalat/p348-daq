#!/bin/bash -e

# input is the file with the list of event IDs
# each line is single event ID
# event ID format: run-spill-event
# example: 2441-3-1045

# output is the command line to run `./pick-event.exe`

# default data location is NA64 EOS
DATAROOT=${DATAROOT-/eos/experiment/na64/data/cdr/}

idlist=$1

events=$(cat $idlist | xargs -L1)

# check the input file is ROOT events list format, example:
# *       14 *      8297 *         1 *        16 *
if [[ $(head -n1 $idlist | sed 's,[^*],,g') == "*****" ]] ; then
  echo "INFO: input list is ROOT-format"
  # convert to "run-spill-event" format
  events=$(cat $idlist | awk '{print $4 "-" $6 "-" $8}')
fi

nevents=$(echo "$events" | wc -l)
echo "===> Events (total $nevents):"
echo "$events"

runs=$(echo "$events" | cut -d '-' -f 1 | sort -u)
echo "===> Runs:"
echo "$runs"

runslist=$(echo "$runs" | paste -s -d'|')
echo "===> Runslist:"
echo "$runslist"

echo "INFO: listing files ..."
files=$(find $DATAROOT -regextype posix-egrep -regex ".*($runslist).*" | sort -t- -k2)
nfiles=$(echo "$files" | wc -l)
echo "===> Files (total $nfiles):"
echo "DATAROOT=$DATAROOT"
echo "$files"

pickcmd="./pick-event.exe -o selection.dat $(echo "$events" | sed 's,^,-e ,') $files"
echo "===> Command:"
echo $pickcmd

echo ""
$pickcmd
