#ifndef CompassSoft_ChipNA64WaveBoard__include
#define CompassSoft_ChipNA64WaveBoard__include

#include "Chip.h"

namespace CS {

////////////////////////////////////////////////////////////////////////////////

/*! \brief Waveboard digitizer for NA64

    See details here: https://twiki.cern.ch/twiki/bin/view/P348/WB

    \author Andrea Celentano
    \author Andrei Antonov
*/

class ChipNA64WaveBoard : public Chip
{
  //============================================================================
  // Types
  //============================================================================
public:



  struct HeaderADC//A.C. Copied from ChipSADC and modified
     {
         uint32      size:16;        /// block size in 32-bits word
         uint32      port:8;         /// UCF port number (for WB, 1 port == 1 WB)
         uint32      spare1:8;       /// remaining first word
         uint32      eventNum:31;     /// trigger number (second word)
         uint32      spare2:1;
         uint32      trigTime:31;     /// trigger time (thid word)
         uint32      spare3:1;
         void        Print           (const char *prefix="") const;
     };


  struct Data
     {
         uint32      sample0:14;
         uint32      spare0:2;       //must be '10'
         uint32      sample1:14;
         uint32      spare1:2;       //must be '10'


         void        Print           (const char *prefix="") const;
     };



  struct ChannelInfo   //A.C. Copied from ChipSADC and modified
  {
      uint32      samples:16;
      uint32      channel:4;
      uint32      spare:12;       //0xFED

      void        Print           (const char *prefix="") const;
  };


  //The Chip field is not used for the moment, but it is kept here for future use.
  class DataID //A.C. Copied from ChipSADC
  {
    public:
      DataID(const Chip::DataID &d) {u.data_id=d;}
      DataID(uint16 srcID,uint16 port,uint16 chip,uint16 chan) {u.s.src_id=srcID; u.s.port=port; u.s.chip=chip; u.s.chan=chan;u.s.none=0;}
      operator Chip::DataID (void) const {return u.data_id;}
      uint16          GetSourceID             (void) const {return u.s.src_id;}
      void            SetSourceID             (uint16 s)   {u.s.src_id=s;}
      uint16          GetPort                 (void) const {return u.s.port;}
      void            SetPort                 (uint16 p)   {u.s.port=p;}
      uint16          GetChip                 (void) const {return u.s.chip;}
      void            SetChip                 (uint16 s)   {u.s.chip=s;}
      uint16          GetChannel              (void) const {return u.s.chan;}
      void            SetChannel              (uint16 s)   {u.s.chan=s;}
      union
      {
          Chip::DataID data_id;
          struct
          {
              Chip::DataID none:24,chan:8,chip:8,port:8,src_id:16;
          } s;
      } u;
      void            Print                   (const std::string &prefix="") const;
  };


  class Digit: public Chip::Digit //Copied and simplified from ChipSADC
      {
        public:

                          Digit                   (const DataID &data_id,const DetID &id)
                                                  : Chip::Digit(data_id,id),x(0x7FFF),y(0x7FFF)
                                                     {}

          void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

          const char*     GetNtupleFormat         (void) const {return "x:y";}
          std::vector<float>   GetNtupleData      (void) const;

          const std::vector<uint16>& GetSamples   (void) const {return samples;}
                std::vector<uint16>& GetSamples   (void)       {return samples;}


          uint16          GetSourceID             (void) const {return static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).GetSourceID();}

          uint16          GetPort                 (void) const {return static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).GetPort();}

          uint16          GetChip                 (void) const {return static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).GetChip();}
          void            SetChip                 (uint16 c)   {static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).SetChip(c);}

          uint16          GetChannel              (void) const {return static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).GetChannel();}
          void            SetChannel              (uint16 c)   {static_cast<ChipNA64WaveBoard::DataID>(GetDataID()).SetChannel(c);}

          int16           GetX                    (void) const {return x;}
          void            SetX                    (int16 xx)   {x=xx;}

          int16           GetY                    (void) const {return y;}
          void            SetY                    (int16 yy)   {y=yy;}

        private:

          int16           x,y;                    // decoded x,y coordanates
          std::vector<uint16>  samples;
      };

  class Map : public Chip::Map //A.C. copied from ChipSADC
  {
    public:
                      Map                     (const ObjectXML &o);

      void            Print                   (std::ostream &o=std::cout,const std::string &prefix="") const;

      void            AddToMaps               (Maps &maps,DaqOption &option) const;


    public:

      uint16          port;
      uint16          chip;

      uint16          channel;
      int16           x,y;
  };


  //============================================================================
   // Constructors and destructor
   //============================================================================

   public:

     /// Destructor
                        ~ChipNA64WaveBoard                (void) {Clear();}

     /*! \brief Base constructor.
         \sa DaqEvent::DaqEvent(void const * const buf,bool copy_buf)
     */
                         ChipNA64WaveBoard                (void const * const buf,bool copy_buf,DaqOption &opt,DaqEvent &ev);

  //============================================================================
  // Methods
  //============================================================================

  private:

    /// \return Chip name.
    std::string         GetName                 (void) const {return "ChipNA64WaveBoard";}

    /// Clear the chip status (do \b not modify the real data buffer).
    void                Clear                   (void);

  public:
    void                Scan                    (DaqOption &opt);
    void                Decode                  (const Maps &maps,Digits &digits_list,DaqOption &opt);

  //============================================================================
  // Data
  //============================================================================

  private:

    std::list<Digit*>   pre_digits;


};


} // namespace CS

#endif
