/*!
  \file      CsPP.h
  \brief     Compass PreProcessor Handling Class
  \author    Markus Kraemer

  \par       History:
  20091127   Design start of this class
*/

#ifndef __CsPP_H__
#define __CsPP_H__

#include "CsPPI.h"
#include "CsPPI_EC02time.h"
#include <list>

class CsPP {
  
  private:
//     CsPP(const string* ppiTableName);
    CsPP();
    virtual ~CsPP() {};

    std::list<CsPPI*> _CsPPI;
  

  public:
    
//     CsPP*               Instance(const string* ppiTableName);
    static CsPP*        Instance();
    int                 ProcessEvent();
    std::list<CsPPI*>&  GetPPI();
    int                 end();

  private:
    static CsPP*        _instance;

};

#endif // __CsPP_H__

