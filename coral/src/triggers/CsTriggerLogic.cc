/*!
        \file    CsTriggerLogic.cc
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */

#include "CsTriggerLogic.h"
#include "CsEvent.h"
#include "CsGeom.h"
#include "CsOpt.h"
#include "CsMCDigit.h"
#include "CsMCUtils.h"
#include "CsHistograms.h"

#ifdef COMPASS_USE_OSPACE_STD
#  include <ospace/std/algorithm>
#else
# include <algorithm>
#endif

using namespace std;

//
// ! CsTriggerLogic
//

CsTriggerLogic::CsTriggerLogic() : _triggerLogicFlag(false) {

    _triggerLogicFlag = CsOpt::Instance()->getOpt("", "make trigger logic");
    if(!_triggerLogicFlag) {

        CsErrLog::msg(elInfo,__FILE__,__LINE__,"Soft trigger logic for dimuon triggers disabled..");
        return;
    }

    //
    // Prepare trigger matrix
    //
    bool ready = true;

    const vector<std::string> vExpectedTriggerMatrix = {"LAS", "OY", "MY"};
    CsErrLog::msg(elInfo,__FILE__,__LINE__,"Loading matrix pattern for dimuon triggers..");
    for(int i = 0, N = vExpectedTriggerMatrix.size(); i < N; i++) {

        std::string path;
        if(CsOpt::Instance()->getOpt("CsTriggerLogic", vExpectedTriggerMatrix[i],  path)) {

            if( !_triggerMatrix.Load(vExpectedTriggerMatrix[i], path) ) ready = false;
        }
    }

    if(ready) CsErrLog::msg(elInfo,__FILE__,__LINE__,"Soft trigger logic for dimuon triggers enabled..");
    else CsErrLog::msg(elFatal,__FILE__,__LINE__, "Cannot load soft trigger logic for dimuon triggers..");

    _triggerMatrix.Print();
};

CsTriggerLogic::~CsTriggerLogic() {
};

void CsTriggerLogic::AddBit(std::string tname, int tbit) {

    CsTriggerSignal trigger_signal(tname, tbit, &_triggerMatrix);
    this->_vTriggerSignal.push_back(trigger_signal);
}

void CsTriggerLogic::Reset()
{
    for(int i = 0, N = _vTriggerSignal.size(); i < N; i++) _vTriggerSignal[i].Reset();
    _vetoSignal.Reset();
    _vHodoDigits.clear();
}

void CsTriggerLogic::Print()
{
    cout << "##### Summary: Trigger information (from DAQ decoding) #####" << endl;
    for(int i = 0, N = _vTriggerSignal.size(); i < N; i++)
        cout << "* " << _vTriggerSignal[i].GetName() << " : bit #" <<_vTriggerSignal[i].GetBit() << endl;
    cout << endl;

    cout << "##### Summary: Raw digits information #####" << endl;
    for(int i = 0; i < (int) _vHodoDigits.size(); i++)
        cout << fixed << "* tbname #"<< _vHodoDigits[i]->getDet()->GetTBName() << "; digits=" << _vHodoDigits[i]->getAddress() << "; time=" << _vHodoDigits[i]->getDatum() << endl;
    cout << endl;

    for(int i = 0, N = _vTriggerSignal.size(); i < N; i++) _vTriggerSignal[i].Print();
    _vetoSignal.Print();

    cout << "##### Summary: Final trigger mask #####" << endl;
    cout << "* Trigger mask without veto delay : 0b" << bitset<12>(GetTriggerMaskWithoutVetoDelay()) << endl;
    cout << "* Trigger mask with veto delay    : 0b" << bitset<12>(GetTriggerMask()) << endl;
    cout << endl;
}

bool CsTriggerLogic::Compute(std::list<CsDigit*> lDigits, vector<unsigned int> vScaler)
{

    CsErrLog::msg(elInfo,__FILE__,__LINE__, "Retrieve digits on \"HG\"..");
    vector<CsDigit*> vDigitsHG = CsTriggerLogic::GetDigits("HG", lDigits);
    _vHodoDigits.insert(_vHodoDigits.end(), vDigitsHG.begin(), vDigitsHG.end());

    CsErrLog::msg(elInfo,__FILE__,__LINE__, "Retrieve digits on \"HO\"..");
    vector<CsDigit*> vDigitsHO = CsTriggerLogic::GetDigits("HO", lDigits);
    _vHodoDigits.insert(_vHodoDigits.end(), vDigitsHO.begin(), vDigitsHO.end());

    CsErrLog::msg(elInfo,__FILE__,__LINE__, "Retrieve digits on \"HM\"..");
    vector<CsDigit*> vDigitsHM = CsTriggerLogic::GetDigits("HM", lDigits);
    _vHodoDigits.insert(_vHodoDigits.end(), vDigitsHM.begin(), vDigitsHM.end());

    for(int i = 0, N = _vTriggerSignal.size(); i < N; i++) _vTriggerSignal[i].Compute(_vHodoDigits);
    _vetoSignal.Compute(vScaler);

    return 1;
}

std::vector<CsDigit*> CsTriggerLogic::GetDigits(std::string tbname, std::list<CsDigit*> lDigits)
{
    std::vector<CsDigit*> vDigits(lDigits.begin(), lDigits.end());
    return GetDigits(tbname, vDigits);
}

std::vector<CsDigit*> CsTriggerLogic::GetDigits(std::string tbname, std::vector<CsDigit*> vDigits)
{
    std::vector<CsDigit*> vFilteredDigits(vDigits.begin(), vDigits.end());
    for(int i = 0; i < (int) vFilteredDigits.size(); i++) {

        if( vFilteredDigits[i]->getDet()->GetTBName().rfind(tbname, 0) != 0) {
            vFilteredDigits.erase(vFilteredDigits.begin()+i);
            i--;
        }
    }

    return vFilteredDigits;
}

int CsTriggerLogic::GetTriggerMaskWithoutVetoDelay()
{
    int trigmask = 0;
    for(int i = 0, I = _vTriggerSignal.size(); i < I; i++) {

        for(int j = 0, J = _vTriggerSignal[i].GetSignal().size(); j < J; j++) {

            pair<double, int> signal = _vTriggerSignal[i].GetSignal()[j];
            trigmask += TMath::Power(1, _vTriggerSignal[i].GetBit());
            break;
        }
    }

    return trigmask;
}

int CsTriggerLogic::GetTriggerMask()
{
    int trigmask = 0;
    for(int i = 0, I = _vTriggerSignal.size(); i < I; i++) {

        for(int j = 0, J = _vTriggerSignal[i].GetSignal().size(); j < J; j++) {

            pair<double, int> signal = _vTriggerSignal[i].GetSignal()[j];
            if(!_vetoSignal.IsVetoApplied(signal.first)) {

                trigmask += TMath::Power(1, _vTriggerSignal[i].GetBit());
                break;
            }
        }
    }

    return trigmask;
}

const std::vector<std::vector<std::pair<double,int> > > CsTriggerLogic::GetTriggerSignalWithoutVetoDelay()
{
    std::vector<std::vector<std::pair<double,int> > > trigger_signal;
    trigger_signal.resize(_vTriggerSignal.size());

    for(int i = 0, I = _vTriggerSignal.size(); i < I; i++) {

        for(int j = 0, J = _vTriggerSignal[i].GetSignal().size(); j < J; j++) {

            std::pair<double, int> signal = _vTriggerSignal[i].GetSignal()[j];
            trigger_signal[i].push_back(signal);
        }
    }

    return trigger_signal;
}

const std::vector<std::vector<std::pair<double,int> > > CsTriggerLogic::GetTriggerSignal()
{
    std::vector<std::vector<std::pair<double,int> > > trigger_signal;
    trigger_signal.resize(_vTriggerSignal.size());

    for(int i = 0, I = _vTriggerSignal.size(); i < I; i++) {

        for(int j = 0, J = _vTriggerSignal[i].GetSignal().size(); j < J; j++) {

            std::pair<double, int> signal = _vTriggerSignal[i].GetSignal()[j];
            if(!_vetoSignal.IsVetoApplied(signal.first)) trigger_signal[i].push_back(signal);
        }
    }

    return trigger_signal;
}
