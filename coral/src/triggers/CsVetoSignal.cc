/*!
        \file    CsVetoSignal.cc
        \brief   Compass Trigger Logic Class.
        \author  Marco Meyer, Benjamin Moritz Veit
 */
#include "CsVetoSignal.h"
#include "CsEvent.h"
#include "CsGeom.h"
#include "CsOpt.h"
#include "CsMCDigit.h"
#include "CsMCUtils.h"
#include "CsHistograms.h"

#ifdef COMPASS_USE_OSPACE_STD
#  include <ospace/std/algorithm>
#else
# include <algorithm>
#endif

using namespace std;

//
// ! Useful ScalerStruct function
//

bool time_sort(const ScalerStruct &a, const ScalerStruct &b)
{
    return a.time < b.time;
}

CsVetoSignal::CsVetoSignal()
{
}

CsVetoSignal::~CsVetoSignal() {
}

bool CsVetoSignal::Compute(vector<unsigned int> vScaler) {

    CsErrLog::msg(elInfo,__FILE__,__LINE__,"Building veto signal..");

    for( unsigned int i = 0, N = vScaler.size(); i < N; i++ ) {

        bool bVTsum = ((vScaler[i]>>27)& 0x1f) == 21;         // ID=21 is the index refering to VTsum in the HodoDataStruct_type
        if(!bVTsum) continue;

        string tbname;
        int channel;
        double time;
        CsEvent::Instance()->getHodoDatum( i, tbname, channel, time );
        VTsum.push_back({tbname,channel,time});
    }

    std::sort(VTsum.begin(), VTsum.end(), time_sort);         // sort according to the time (instead of #ch)
    for(int i = 0, N = (int) VTsum.size(); i < N; i++) {

        if(VTsum[i].ch == 0) Vtot.push_back(VTsum[i].time + 8585);
        if(VTsum[i].ch == 8) VNtot.push_back(VTsum[i].time + 8585);
    }

    return true;
}

void CsVetoSignal::Reset()
{
    VTsum.clear();
    Vtot.clear();
    VNtot.clear();
}

void CsVetoSignal::Print() const
{
    cout << "##### Summary: Veto raw information #####" << endl;
    for(int i = 0; i < (int) this->VTsum.size(); i++)
        cout << fixed << "* tbname: " << this->VTsum[i].tbname << "; ch #" << this->VTsum[i].ch << "; time = " << this->VTsum[i].time << " ns" << endl;
    cout << endl;

    cout << "##### Summary: Veto gate signal #####" << endl;
    for(int i = 0; i < (int) this->Vtot.size(); i++)
        cout << fixed << "* Vtot = " << this->Vtot[i] << " ns" << endl;
    for(int i = 0; i < (int) this->VNtot.size(); i++)
        cout << fixed << "* VNtot = " << this->VNtot[i] << " ns" << endl;
    cout << endl;

    cout << "##### Summary: Veto multiplicity = " << this->GetMultiplicity() << " #####" << endl;
    cout << endl;
    cout << "##### Summary: Veto duration = " << this->GetDuration() << " ns" << " #####" << endl;
    cout << endl;
}

double CsVetoSignal::GetDuration() const
{
    double duration = 0;
    for(int i = 0, N = this->GetMultiplicity(); i < N; i++)
        duration += VNtot[i] - Vtot[i];

    return duration;
}

int CsVetoSignal::GetMultiplicity() const {

    if(Vtot.size() == Vtot.size()) return Vtot.size();
    return -1; // Some ambiguous cases occur sometimes (Vtot, Vtot, !Vtot)..
}

bool CsVetoSignal::IsVetoApplied(double trigger_time) const {

    for(int i = 0, N = this->GetMultiplicity(); i < N; i++) {

        if( (trigger_time + CsTriggerLogic::tdc_res_hodo > Vtot[i]) && (trigger_time + CsTriggerLogic::tdc_res_hodo < VNtot[i]) ) return 1;
        if( (trigger_time - CsTriggerLogic::tdc_res_hodo > Vtot[i]) && (trigger_time - CsTriggerLogic::tdc_res_hodo < VNtot[i]) ) return 1;
    }

    return 0;
}
