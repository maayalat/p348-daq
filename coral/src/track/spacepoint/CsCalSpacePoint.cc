/*!
   \file    CsCalSpacePoint.cc 
   \brief   Calibrarion spacepoint, derived from CsSpacePoint Class.
   \author  Hugo Pereira
*/

#include "CsCalSpacePoint.h"
#include "CsSpacePoint.h"

//_____________________________________________________________________________
CsCalSpacePoint::CsCalSpacePoint( const CsDetFamily &df, std::list<CsCluster*> c, double z, int mode):
  CsSpacePoint( df, c, z, mode ),
  detOff_( 0 ),
  found_( false ),
  clFound_( NULL )
{}

//_____________________________________________________________________________
CsCalSpacePoint& CsCalSpacePoint::operator=( const CsCalSpacePoint &sp)
{
  if( this != &sp ) { *this = sp; }
  return *this;
}

