/*!
   \file    CsSPMaker.h
   \brief   Compass event/event Space Point Maker
   \author  Hugo Pereira
*/

#ifndef CsSPMaker_h
#define CsSPMaker_h

#include "CsSTD.h"
#include <CLHEP/Matrix/Matrix.h>

class CsDetFamily;
class CsCluster;
class CsSpacePoint;
//_____________________________________________________________________________
class CsSPMaker {
public:

  static CsSPMaker* Instance( void );
	inline std::vector<CsDetFamily*> getDetFamilies( void ) const { return df_; }
  void cleanEvent( void );
protected:
	
  CsSPMaker( void );  //!< The Constructor

private:

	bool readDetFamilies( void );	
	static CsSPMaker* instance_;			  //!< the singleton instanciation
	std::vector<CsDetFamily*> df_;  		//!< vector of detector families used to build spacepoints
  std::vector<CsSpacePoint*> sp_;          //!< vector of build spacepoint
	
};

#endif	
