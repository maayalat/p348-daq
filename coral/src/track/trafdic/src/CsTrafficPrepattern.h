/*!
   \file    CsTrafficPrepattern.h
   \author  Benigno.Gobbo@cen.ch
*/

#ifndef CsTrafficPrepattern_h
#define CsTrafficPrepattern_h

#include "CsSTD.h"
#include "CsTrkPrepattern.h"

/*! \class CsTrafficPrepattern 
    Coral interface to Traffic Pre-Pattern
*/

class CsTrafficPrepattern : public CsTrkPrepattern {

 public:

  CsTrafficPrepattern();          //!< constructor
  ~CsTrafficPrepattern();         //!< destructor

  /*! 
     Interface to Traffic pre-pattern (track pieces finding)
     \sa TEv::PrePattern()
  */
  bool doPrepattern( const std::list<CsCluster*> &clusters, const std::list<CsZone*> &zones );

  /*!
     Retutns to CORAL found track pieces
  */
  bool getPatterns( std::list<CsTrack*>& tracks );

  /*! 
    Returns to CORAL clusters, not assosiated with found track pieces 
  */
  std::list<CsCluster*> getUnusedClusters(void) const;

};

#endif //CsTrafficPrepattern_h
