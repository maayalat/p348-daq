/*! 
  \brief Basic method for fitting the list of tracks

  Modifications for NA64
*/

#include <iostream>
#include "TEv.h"
#include "TOpt.h"
#include "TDisplay.h"
#include "CsHistograms.h"

using namespace std;

void TEv::TracksFit3()
{


  bool print = false;
  if(print) cout<<" \n\n================> TEvTraksFit3 ============"<<endl;
  // int print = TOpt::Print[4]; 

  bool hist(false); 
  if(TOpt::Hist[4] > 0) hist=true;

  static bool first=true;
  static CsHist1D*  h[50];
  static CsHist2D* hh[50];

  if(first && hist){ // book histograms
    cout<<endl<<"NA64 track fit"<<endl<<endl;
    first=false;
    CsHistograms::SetCurrentPath("/Traffic/TracksFit3");    
    h[ 1]  = new CsHist1D("h01", "Track momentum [GeV]",     1000, 75., 125.);
    h[ 2]  = new CsHist1D("h02", "#sigma(P) [GeV]", 1000, 0,  2);
    h[ 3]  = new CsHist1D("h03", "#chi^{2}/NDF", 500, 0,  100);

    hh[1] = new CsHist2D("hh01","Fitted momentum [GeV] VS incoming track Azi angle [mrad]", 200, -2.0, 2.0, 200, 70., 110.);
    hh[2] = new CsHist2D("hh02","Fitted momentum [GeV] VS incoming track Dip angle [mrad]", 200, -2.0, 2.0, 200, 70., 110.);
    hh[3] = new CsHist2D("hh03","Fitted Y  VS X  [cm]",   200, -2.5, 2.5, 200, -2.5, 2.5);
    hh[4] = new CsHist2D("hh04","Fitted Y' VS X' [mrad]", 200, -2.5, 2.5, 200, -2.5, 2.5);
    hh[5] = new CsHist2D("hh05","Fitted X [cm] VS X' [mrad]", 200, -2.5, 2.5, 200, -2.5, 2.5);
    hh[6] = new CsHist2D("hh06","Fitted Y [cm] VS Y' [mrad]", 200, -2.5, 2.5, 200, -2.5, 2.5);

    CsHistograms::SetCurrentPath("/");
  }

  bool ret;
  list<TTrack>::iterator it = listTrack.begin(); 
  while (it != listTrack.end()) { // loop over tracks

    if ((*it).NGroups()==0) {
      cout<<"TEv::TracksFit3 ==> track with NGroups == 0"<<endl;
      assert(false);
    }

    // Insert misses planes
    //
    // It is needed as missed planes (it's where hit is not found) may add
    // mult. scattering.
    // To be done before final fit.
    //
    if(TOpt::ReMode[21] == 0) 
      (*it).InsertMissedPlanes();

    if ((*it).Hfirst.empty() ||      // No estimation of track parameters
	!(*it).Hfirst.with_mom()) {  // no momentum
      it++; continue;                // => Skip
    }

    //
    // Full (with calculation of smoothed helices) Kalman fit forward and backward
    //
    assert((*it).Hfirst(5,5) != 0);

    ret = (*it).FullKF3(1); // -----> forward
    if(!ret) { // forward  fit has failed. Erase track. Next one.
      cout<<"TEv::TracksFit3() ==> Forward Kalman fit 1 failed.  Track ID "
	  <<(*it).Id<<" removed"<<endl;
      listTrack.erase(it++); 
      //goto next_track;
      continue;
    }


    ret = (*it).FullKF3(-1); // <------ backward
    if(!ret) { // backward  fit has failed. Erase track. Next one
      cout<<"TEv::TracksFit3() ==> Backward Kalman fit 1 failed. Track ID "
	  <<(*it).Id<<" removed"<<endl;
      listTrack.erase(it++);
      //goto next_track;
      continue;
    }


    // Cut
    if(TOpt::dCut[17] != 0. && ((*it).Chi2tot/(*it).NHits) > TOpt::dCut[17]){ // fit OK. Check Chi2       
      listTrack.erase(it++);     // Bad Chi2. Erase.
      // goto next_track;
      continue;
    }
    
    (*it).UseHitTime();     // Time measurement

    //Calculate "smoothed" helices (if requested) 

    for(int i = 0; i < int(sizeof(TOpt::SmoothPos)/sizeof(double)); i++){ // loop over "smoothing" points
      double x = TOpt::SmoothPos[i];
      if(x == 0.) break;
      THlx Hsm;
      if((*it).GetSmoothed(Hsm,x) < 0) continue;
      (*it).lHsm.push_back(Hsm); // store smoothed
    }// end of loop over "smoothing" points


    // Fill histograms
    if(hist){
      const THlx& Hfirst = (*it).H('u');
      const THlx& Hlast  = (*it).H('d');
      h[ 1]->Fill(fabs(1./Hfirst(5)));
      double sigmaP = sqrt(Hfirst(5,5))/(Hfirst(5)*Hfirst(5));
      h[ 2]->Fill(sigmaP);
      double chi2ndf = (*it).Chi2tot/((*it).NHits - 5);
      h[ 3]->Fill(chi2ndf);
      hh[1]->Fill(Hfirst.azi()*1000., fabs(1./Hfirst(5)));
      hh[2]->Fill(Hfirst.dip()*1000., fabs(1./Hfirst(5)));
      // parameters correlations
      hh[3]->Fill(Hfirst(1),Hfirst(2));
      hh[4]->Fill(Hfirst(3)*1000., Hfirst(4)*1000.);
      hh[5]->Fill(Hfirst(3)*1000., Hfirst(1));
      hh[6]->Fill(Hfirst(4)*1000., Hfirst(2));
    }
    it++; // next track
    //next_track:;
  } // end of loop over tracks



  // In case of MC event:
  // - assosiate found track with MC track.
  // - extrap. tracks upwards to prim. vertex if requested,
  //   (for tracks starting befor SM1)
  if(! this->isMC ) return;

  if(TOpt::Graph[6] > 0){ // switch on debug drawing of calls to field and maperial maps
    TDisplay::Ref().SetMmaptraj(true); TDisplay::Ref().SetRkutraj(false);
  }
  for(it = listTrack.begin(); it != listTrack.end(); it++) {  // loop over tracks
    (*it).FindKine();
    if(TOpt::ReMode[9] == 1 && (*it).InGroup(0) == 1){
      if( (*it).Hfirst.with_mom() ) { // track with momentum
	THlx Hextr;
	Hextr(0)=vVtxMC(0).V(0);   // coord of prim. vertx. (assumed to be the first one)
	if((*it).Hfirst.Extrapolate(Hextr)) (*it).Hfirst = Hextr;
      }
    }
  }
  
  TDisplay::Ref().SetMmaptraj(false); TDisplay::Ref().SetRkutraj(false); // switch off debug drawing

  if(print) cout<<" \n\n==========> End of TEvTraksFit3 ============"<<endl;

}
