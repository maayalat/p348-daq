/*!
   \file    CsSampleBridging.h
   \brief   Sample of track bridging class
   \author  Benigno Gobbo

*/

#ifndef CsSampleBridging_h
#define CsSampleBridging_h

#include "CsSTD.h"
#include "CsTrkBridging.h"

/*! \class CsSampleBridging 
    \brief Sample of track bridging class

    Use this to develop your classes
*/

class CsSampleBridging : public CsTrkBridging {

 public:

  /*! \fn CsSampleBridging()
    \brief ...
  */
  CsSampleBridging();

  /*! \fn ~CsSampleBridging()
    \brief ...
  */
  virtual ~CsSampleBridging();

  /*! \fn bool doBridging( list<CsTrack>& tracks, list<CsCluster*>& clusters )
    \brief ... 
  */
  bool doBridging( list<CsTrack*>& tracks, list<CsCluster*>& clusters );

 private:



};

#endif //CsSampleBridging_h
