/*!
   \file    CsReconFitting.cc
   \brief   Recon track fitting class
   \author  ...

*/

#include "CsReconFitting.h"
#include "CsErrLog.h"
#include "RecOpt.h"


CsReconFitting::CsReconFitting() {
}

CsReconFitting::~CsReconFitting() {
}

bool CsReconFitting::doFitting( std::list<CsTrack*>& tracks,
				 const std::list<CsCluster*>& clusters ) {

if (RecOpt::Switch > 1 ) return( true); //do nothing	
  CsErrLog::Instance()->mes( elFatal, "Fitting not supported on Recon package" );
  return( false );
}



