/*!
   \file    CsReconBridging.cc
   \brief   Recon track bridging class
   \author  ...

*/

#include "CsReconBridging.h"
#include "CsErrLog.h"
#include "RecOpt.h"

CsReconBridging::CsReconBridging() {
}

CsReconBridging::~CsReconBridging() {
}

bool CsReconBridging::doBridging( std::list<CsTrack*>& tracks, 
				   std::list<CsCluster*>& clusters ) {

if (RecOpt::Switch > 1 ) return( true); //do nothing	
  CsErrLog::Instance()->mes( elFatal, "Bridging is done together  with Prepattern in Recon package " );
  return( false );
}





