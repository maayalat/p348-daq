// --- Standard C/C++ library ---
#include <cassert>
#include <cmath>
#include <iostream>

// --- Internal files ---
#include "Reconstruction.h"

namespace Reco {

////////////////////////////////////////////////////////////////////////////////

Reconstruction::Reconstruction(const Calorimeter* c) : calorimeter_(c) {
}

////////////////////////////////////////////////////////////////////////////////

} // namespace Reco
