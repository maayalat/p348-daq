#!/bin/bash -e

if (( "$#" < 1 )) ; then
  echo "Usage: ./mkref.sh run1 run2 run3"
  exit 1
fi

# input parameters are run numbers
# for each input run number: generate physics and calibrations events reference file

for i in "$@" ; do
  ./bcoool.sh /data/cdr/cdr*-00$i.dat -root ref${i}.root
  ./bcoool.sh /data/cdr/cdr*-00$i.dat -root ref${i}led.root -evttypes CALIBRATION_EVENT
done
