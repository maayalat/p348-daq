
vector<TString> ListObjects(TFile* f, TString path = "/")
{
  vector<TString> list;
  
  TDirectory* d = f->GetDirectory(path);
  if (!d) return list;
  
  TList* l = d->GetListOfKeys();
  //l->Print();
  
  for (TObject* i : *l) {
    TKey* k = (TKey*) i;
    const bool isdir = (TString(k->GetClassName()) == "TDirectoryFile");
    const TString fullpath = path + "/" + k->GetName();
    
    if (isdir) {
      const auto list0 = ListObjects(f, fullpath);
      for (auto j : list0) list.push_back(j);
      continue;
    }
    
    /*
    cout << " fullpath=" << fullpath
         << " class=" << k->GetClassName()
         << " isdir=" << isdir
         << endl;
    */
    
    list.push_back(fullpath);
  }
  
  return list;
}

vector<TString> MatchPattern(const vector<TString>& input, const vector<TRegexp>& keep, const vector<TRegexp>& reject)
{
  vector<TString> output;
  
  for (const auto& i : input) {
    bool hasMatch = false;
    
    for (const auto& j : keep)
      if (i.Index(j) != -1) {
        hasMatch = true;
        break;
      }
    
    for (const auto& j : reject)
      if (i.Index(j) != -1) {
        hasMatch = false;
        break;
      }
    
    if (hasMatch) output.push_back(i);
  }
  
  return output;
}

void PrintList(const vector<TString>& list)
{
  for (auto i : list) {
    cout << i << endl;
  }
}

struct Record {
  TH1* h;
  bool isTProfile;
  int bin1;     // bin id of "spill=1"
  int nspills;  // total number of spills
  
  // strip channel name
  TString name() const
  {
    TString name = h->GetName();
    name.ReplaceAll("_vs_spill", "");
    name.ReplaceAll("_hit_position_", "_hit");
    name.ReplaceAll("Micromega", "MM");
    name.ReplaceAll("XY_", "_");
    name.ReplaceAll("UV_", "_");
    //TPRegexp("unit\{\([^}]+)}").Substitute(name, "[$1]", "g");
    return name;
  }
};

  /*
  for (int i = 1; i <= 200; ++i) {
    cout << i
         << " |"
         << " " << h->GetBinLowEdge(i)  // == spill number
         << " " << h->GetBinEntries(i)
         << " " << h->GetBinContent(i)
         << " " << h->GetBinError(i)
         << endl;
  }
  */

vector<Record> LoadHistograms(TFile* f, const vector<TString>& list)
{
  vector<Record> v;
  
  for (const auto& i : list) {
    TH1* h = (TH1*) f->Get(i);
    TAxis* a = h->GetXaxis();
    const int bin1 = a->FindFixBin(1); // bin of spill==1
    const int nspills = (a->GetNbins() - bin1 + 1);
    
    /*
    cout << "name=" << h->GetName()
         << " class=" << h->ClassName()  // h->IsA()->xxx
         << " nbins=" << a->GetNbins()
         << " min=" << a->GetXmin()
         << " max=" << a->GetXmax()
         << " bin1=" << bin1
         << " nspills=" << nspills
         << endl;
    */
    
    // sanity check:
    //  - each bean is a single spill and the width should be =1
    //  - the bin correspondinf to spill=1 is present
    const bool ok = (a->GetXmax() - a->GetXmin() == a->GetNbins()) && (bin1 > 0);
    if (!ok) continue;
    
    const bool isTProfile = (TString(h->ClassName()) == "TProfile");
    
    const Record r = {h, isTProfile, bin1, nspills};
    v.push_back(r);
  }
  
  return v;
}

void DrawHistograms(const vector<Record>& r)
{
  gStyle->SetPalette(kVisibleSpectrum);
  
  TMultiGraph* mg = new TMultiGraph;
  mg->SetName("trends");
  mg->SetTitle("Trends;spill;value");
  for (const auto& i : r) {
    TGraph* g = new TGraph;
    g->SetTitle(i.h->GetName());
    g->SetFillColor(0);
    g->SetMarkerStyle(kFullDotMedium);
    
    for (int j = 0; j < i.nspills; ++j) {
      const int x = 1 + j; // = spill number
      const double y = i.h->GetBinContent(i.bin1+j);
      g->AddPoint(x, y);
    }
    
    mg->Add(g);
  }
  
  TCanvas* c = new TCanvas;
  c->SetGrid();
  mg->Draw("APL pmc plc");
}

void DumpHistograms(const int runnum, vector<Record> r, string outname)
{
  cout << "DumpHistograms():"
       << " runnum=" << runnum
       << " outname=" << outname
       << endl;
  
  int idx = -1;
  for (int i = 0; i < r.size(); ++i) {
    if (TString(r[i].h->GetName()) == "spilltime") {
      //cout << i << " " << r[i].h->GetName() << endl;
      idx = i;
    }
  }
  
  if (idx == -1) {
    cout << "ERROR: the 'spilltime' histogram is missing" << endl;
    terminate();
  }
  
  const Record t = r[idx];
  TProfile* spilltime = dynamic_cast<TProfile*>(t.h);
  
  // remove 'spilltime' from the list
  r.erase(r.begin()+idx);
  
  cout << "total columns n=" << r.size() << endl;
  
  ofstream fout(outname.c_str());
  
  // print header
  fout << "#run spill time stat ";
  for (const auto& i : r) {
    fout << i.name() << " ";
  }
  fout << endl;
  
  // print data
  for (int j = 0; j < t.nspills; ++j) {
    const int spill_idx = 1 + j; // = spill number
    const int64_t spill_time = spilltime->GetBinContent(t.bin1+j);
    const int spill_stat = spilltime->GetBinEntries(t.bin1+j);
    
    const bool hasData = (spill_stat > 0);
    if (!hasData) continue;
    
    fout << runnum << " " << spill_idx << " " << spill_time << " " << spill_stat << " ";
    
    for (const auto& i : r) {
      double y = i.h->GetBinContent(i.bin1+j);
      
      // TODO: to be removed after 2023A:
      // work-around for incorrect mapping: truncate value to low 24 bits
      // better fix would be to propagate higher 24 bits to the next scaler channel
      if (TString(i.h->GetName()).Contains(TRegexp("SCALERS_ch.*_vs_spill"))) {
        int64_t tmp = y;
        tmp &= 0xFFFFFF;
        
        //cout << "patch " << i.name() << " y=" << y << " fix=" << tmp << endl;
        y = tmp;
      }
      
      // check, is the bin was actually populated
      int nentries = 1;
      if (i.isTProfile) {
        TProfile* p = (TProfile*) i.h;
        nentries = p->GetBinEntries(i.bin1+j);
      }
      
      // output "magic number" as indication of "no data"
      if (nentries == 0) y = 1e100;
      
      fout << y << " ";
    }
    fout << endl;
  }
}

// extract run number from the file name
// file name example:
//   path/run999995-enddate20230510T165811.root
int GetRunNumber(TString fname)
{
  // keep only filename
  const int pos1 = fname.Last('/');
  if (pos1 != kNPOS) fname.Remove(0, pos1+1);
  
  int runnum = 0;
  int enddate = 0;
  int endtime = 0;
  sscanf(fname.Data(), "run%d-enddate%dT%d.root", &runnum, &enddate, &endtime);
  cout << "runnum=" << runnum << " enddate=" << enddate << " endtime=" << endtime << endl;
  
  return runnum;
}

void dumpcoool(const TString fname)
{
  TFile* f = new TFile(fname);
  cout << "input file name=" << f->GetName() << endl;
  
  //f->ReadAll();
  //f->ls();
  //f->GetDirectory("/")->ls();
  //f->GetDirectory("/SC")->ls();
  //f->GetDirectory("/SC/SCALERS")->ls();
  
  //ListObjects(f,"/SC/SCALERS");
  //ListObjects(f,"/SC");
  const auto list0 = ListObjects(f, "/");
  cout << "histograms total n=" << list0.size() << endl;
  //PrintList(list0);
  //cout << "=============================" << endl;
  
  // events_per_spill is TH1F, the rest is TProfile
  const auto list = MatchPattern(list0, {".*_vs_spill", "spilltime", "events_per_spill"}, {"_spilltime"});
  cout << "histograms vs_spill n=" << list.size() << endl;
  //PrintList(list);
  
  const auto r = LoadHistograms(f, list);
  
  //DrawHistograms(r);
  
  const int runnum = GetRunNumber(f->GetName());
  DumpHistograms(runnum, r, "dump.txt");
}
