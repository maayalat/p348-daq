 //  version 19 May 2016
 //
 //    
#ifndef __PlaneHCAL1__
#define __PlaneHCAL1__

#include "Plane2V.h"
#include "PlaneHCAL1.h"

#include "TTree.h"

#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>

#include <TProfile.h>
    
     
/// Plane class for HCAL1

class PlaneHCAL1 : public  Plane2V {
   
 private:
  
  static const int fAmpCut;

   TH1F *fHsac, *fHch, *fHma, *fHhth, *fBad, *fGood;
   TH1F *fSum_amp, *fHit_all, *fSum_amp_hit;
   
  TH2F *fHchac, *fHxyac, *fHxyn;
  TH2F *fSum_sampl;

   TH1F *fSum_led;
   TH1F_Ref *fSum_time;
     
  //TH2F *fHrefled_low,*fHrefled_hig;
  TH2F *fAmpLed,*fHchampled;
  TH2F *fPed_xy;  
  TH2F *fTime_xy; 
  TH2F *fPed_ch; 
  TH2F *fRMS_Ped_xy;  
  TH2F *fRMS_Ped_ch;

  TH1F *fPed_sum;  
  TH1F *fRMS_Ped_sum;  
  TProfile *fProf;

#if !defined(__CINT__) && !defined(__CLING__)
  double HCAL1_Led[28][20];
//  TProfile *fSProfile[28][30];
  TH2F *fSProfile[28][30];
  TH1F *fCH_time[28][20];
  TH1F *fCH_amp[28][20];
#endif
 public:
  
    PlaneHCAL1(const char *detname,int nrow, int ncol, int center, int width):
    Plane2V(detname,nrow, ncol, center, width) {}

  ~PlaneHCAL1() {}

  void Init(TTree* tree =0);
//  void  Calib_coef();

#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
  void StoreDigit(CS::Chip::Digit* digit);
#endif
  void StoreDigit(int col, int row,  std::vector<float>& data);

  ClassDef(PlaneHCAL1,0)
};

#endif




