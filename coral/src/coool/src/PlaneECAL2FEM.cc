#include <cmath>

#include "PlaneECAL2FEM.h"
#include <exception>
#include "TProfile.h"
#include "ChipSADC.h"
#include <vector>
#include <stdexcept>
using namespace std;

#define Dpedstart  0         // LED pedestal window
#define Dpedend    6
#define Dledstart   7        // LED puls  window
#define Dledend     31

ClassImp(PlaneECAL2FEM);


PlaneECAL2FEM::PlaneECAL2FEM(const char *detname,int ncol, int nrow, int center, int width)
  : Plane(detname),fNrows(nrow),fNcols(ncol) 
{

  hFEMhit=NULL; hFEMmeanamp=NULL; hFEMdisp=NULL;
  hPed=NULL; hPedRMS=NULL; hTime=NULL;
  hFEMallamp=NULL;
  for(int i=0; i<nb_FEM_ECAL2cnl; i++) { hFEMamp[i]=NULL; hSADCsmpl[i]=NULL;}

  pedstart=Dpedstart;
  pedend=Dpedend;
  ledstart=Dledstart;
  ledend=Dledend;
}


void PlaneECAL2FEM::Init(TTree* tree) {
  std::string myname = "EC02FEM_";
  std::string name, title;
//  char scutamp[10],scutled[10];
//  sprintf(scutamp,"%3.1i",SADCampcut);
//  sprintf(scutled,"%3.1i",SADCLEDcut);

  name = myname + "Nhits_vs_channel";
  title = myname + "FEM hits number vs channel";
  hFEMhit=new TH1F_Ref(name.c_str(),title.c_str(),30,0.,30., fRateCounter);
  ((TH1F_Ref*)hFEMhit)->SetReference(fReferenceDirectory);
  AddHistogram(hFEMhit);

  name = myname + "Ampl_vs_channel";
  title = myname + "FEM amplitudes vs FEM channel";
  hFEMmeanamp=new TH1F_Ref(name.c_str(),title.c_str(),30,0.,30.,fRateCounter, true);
//  hFEMmeanamp=new TH1F(name.c_str(),title.c_str(),30,0.,30.);
 ((TH1F_Ref*)hFEMmeanamp)->SetReference(fReferenceDirectory);
  AddHistogram(hFEMmeanamp);

  name = myname + "Ampl_(all_channels)";
  title = myname + "FEM amplitudes, all FEM channels";
  hFEMallamp=new TH1F_Ref(name.c_str(),title.c_str(),1024,0.,1024., fRateCounter);
 ((TH1F_Ref*)hFEMallamp)->SetReference(fReferenceDirectory);
  AddHistogram(hFEMallamp);

  name = myname + "RMS_vs_channel";
  title = myname + "FEM RMS vs channel";
  hFEMdisp=new TH1F_Ref(name.c_str(),title.c_str(),30,0.,30., fRateCounter, true);
//  hFEMdisp=new TH1F(name.c_str(),title.c_str(),30,0.,30.);
  ((TH1F_Ref*)hFEMdisp)->SetReference(fReferenceDirectory);
  AddHistogram(hFEMdisp);

  for(int i=0; i<nb_FEM_ECAL2cnl; i++) {
     char cnlnmb[10];
     sprintf(cnlnmb,"%3.1i",i);
     name = myname + "FEM,chnl#"+cnlnmb;
     title = myname + "FEM pulshight, chnl#"+cnlnmb;
     hFEMamp[i]=new TH1F_Ref(name.c_str(),title.c_str(),1024,0.,1024., fRateCounter);
     ((TH1F_Ref*)hFEMamp[i])->SetReference(fReferenceDirectory);
     AddHistogram(hFEMamp[i]);
  }
   
  if (fExpertHistos) {
    name    = myname + "PED";
    title   = myname  + "Pedestals vs channel#";
    hPed = new TH2F(name.c_str(),title.c_str(),30,0.,30.,1000,0.,1000.);
    hPed->SetOption("col");
    AddHistogram(hPed);
 
    name    = myname + "PED_RMS";
    title   = myname  + "Ped RMS vs channel#";
    hPedRMS = new TH2F(name.c_str(),title.c_str(),30,0.,30.,50,0.,50.);
    hPedRMS->SetOption("col");
    AddHistogram(hPedRMS);
 
    name    = myname + "Time_vs_chnl";
    title   = fName  + "Time vs channel#";
    hTime = new TH2F(name.c_str(),title.c_str(),30,0.,30.,32,0.,32.);
    hTime->SetOption("col");
    AddHistogram(hTime);

     for(int i=0; i<nb_FEM_ECAL2cnl; i++) {
       char cnlnmb[10];
       sprintf(cnlnmb,"%3.1i",i);
       name = myname + "SADC_samples"+cnlnmb;
       hSADCsmpl[i]=new TH2F(name.c_str(),name.c_str(),32,0.,32.,1024,0.,1024.);
       AddHistogram(hSADCsmpl[i]);
    }
  }

  fIsInTree = false;
}

void PlaneECAL2FEM::EndEvent(const CS::DaqEvent &event) {
  int evtype=event.GetType();
  if(evtype!=8)  return;
//  int trmsk=event.GetTrigger();
  const CS::uint32 Atr = event.GetHeader().GetTypeAttributes()[0]&0xf8000000;
//  cout<<"----FEM calib event  "<<trmsk<<std::hex<<"   Atr="<<Atr<<std::dec<<endl;
  if(evtype==8&&Atr!=0x68000000) return;      // select only calorimeters calib.ev.
  if (thr_flag) MYLOCK();
//
//==================================== SADC ===================================
//
  double sum=0., time=0.;
  hFEMmeanamp->Reset();
  hFEMdisp->Reset();
    
  for ( int i=0; i<fNhits; i++) {            // SADC
//    int col=fSADCCol[i];
    int row=fSADCRow[i];
    if(hFEMhit) hFEMhit->Fill(row);
//
    double RMS=0.,ped=0.,puls=0.;
    ped=SADCped(pedstart,pedend,&fSADCev[i][0],RMS);
    puls=SADCpuls(ledstart,ledend,&fSADCev[i][0], ped,sum,time);
    if(hFEMamp[row]) {
          hFEMamp[row]->Fill(puls);
          hFEMallamp->Fill(puls);
	  double meanamp=hFEMamp[row]->GetMean();
	  if(hFEMmeanamp) hFEMmeanamp->SetBinContent(row+1,meanamp);  
	  double disp=hFEMamp[row]->GetStdDev();  
	  if(hFEMdisp) hFEMdisp->SetBinContent(row+1,disp);  
    } 
    if (fExpertHistos) {
       if(hPed) hPed->Fill(row,ped);
       if(hPedRMS) hPedRMS->Fill(row,RMS);
       if(hTime) hTime->Fill(row,time);
       if(hSADCsmpl[row]){
             for(int j=0; j<nSADC_Sampl; j++) hSADCsmpl[row]->Fill(j,fSADCev[i][j]);
       }
    }
//
 }
  if (thr_flag) MYUNLOCK();
}


//==============================================================================

double PlaneECAL2FEM::SADCped(int ipbg, int  ipend,int *data, double &RMS) {
   double mnped=0.;
   RMS=0.;
   for (int i=ipbg;i<=ipend;i++)  mnped+=data[i];
   mnped/=(ipend-ipbg+1);
   for (int i=ipbg;i<=ipend;i++)  RMS+=(data[i]-mnped)*(data[i]-mnped);
   RMS=sqrt(RMS/(ipend-ipbg+1));
 return mnped;
}

//==============================================================================

double PlaneECAL2FEM::SADCpuls(int ipbg, int  ipend,int *data,
                         double ped,double &sum,double &time) {
   double puls=0., pulspos=0.;
   sum=0.; time=0.;
   for (int i=ipbg;i<=ipend;i++) {
              double amp=data[i]-ped;
              if(amp>puls) {puls=amp; pulspos=i;}
              if(amp<0.) amp=0.;
              sum+=amp;  time+=amp*i;
   }
   if(sum>0.) time=time/sum;
   time=pulspos;
 return puls;
}

//==============================================================================

void PlaneECAL2FEM::StoreDigit(CS::Chip::Digit* digit) {
//
  std::vector<float> data=digit->GetNtupleData();
  if(data.size()<3) return;
  const CS::ChipSADC::Digit* sadcdig = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
  if(sadcdig != NULL ) {
         int ix=sadcdig->GetX(); int iy=sadcdig->GetY();
         if(ix < 0||iy<0||ix>0||iy>=nb_FEM_ECAL2cnl) return;
         this->StoreDigit(ix,iy,data);
  } 
}

//==============================================================================

void PlaneECAL2FEM::StoreDigit(int col, int row, std::vector<float>& data) {
//
  int sadclen=data.size()-2;
  if(sadclen<1) return;
//
  int nhit=fNhits;
  if (nhit< nb_FEM_ECAL2cnl) {
    fSADCRow[nhit]=row;
    fSADCCol[nhit]=col;
    int len=nSADC_Sampl;
    if(sadclen<len){ len=sadclen;
                    for(int i=len;i<nSADC_Sampl;i++) fSADCev[nhit][i]=0; }
    for(int i=0;i<len;i++) fSADCev[nhit][i]=(int)data[i+2];
    fNhits++;
  }
}

