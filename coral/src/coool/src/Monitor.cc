#include "Monitor.h"
#include "expat.h"
#include "MonitorPanel.h"
#include <stdio.h>
#include <sys/resource.h>
#include <unistd.h>

#include "Exception.h"
#include "DaqEvent.h"

#include "Plane1V.h"
#include "PlaneVeto.h"
#include "PlaneTrigHodo.h"
#include "PlaneMISC.h"
#include "PlaneDriftChamber.h"
#include "PlaneDriftChamber2V.h"
#include "PlaneScifiJ.h"
#include "PlaneScifiG.h"
#include "PlaneMwpc.h"
#include "PlaneStrawTubes.h"
#include "PlaneECAL0.h"
#include "PlaneHCAL1.h"
#include "PlaneHCAL2.h"
#include "PlaneHCALT.h"
#include "PlaneHCAL2Sum.h"
#include "PlaneECAL1.h"
#include "PlaneFEM.h"
#include "PlaneECAL2.h"
#include "PlaneECAL2FEM.h"
#include "PlaneAPV.h"
#include "PlaneGEM.h"
#include "PlanePGEM.h"
#include "PlaneSili.h"
#include "PlaneRiAPV.h"
#include "PlaneMumega.h"
#include "PlaneMuonWallA.h"
#include "PlaneMuonWallB.h"
#include "PlanePMumega.h"
#include "PlaneBMS.h"
#include "PlaneScaler.h"
#include "PlaneRICH.h"
#include "PlaneRICH_MAPMT.h"
#include "PlaneCamera.h"
#include "PlaneGandalf.h"

#include "PlanePrimakoffHodo.h"
#include "PlaneBeamKiller.h"
#include "PlaneVBHodo.h"
#include "PlaneVBOX.h"
#include "PlaneCEDAR.h"
#include "PlaneRW.h"
#include "PlaneRPD_SADC.h"
#include "PlaneRPD_F1.h"
#include "PlaneSandwich.h"
#include "PlaneTrigger_SADC_F1.h"
#include "PlaneTrigger_SADC.h"
#include "PlaneTcsPhase.h"

#include "GroupScifiJ.h"
#include "GroupScifiG.h"
#include "GroupTrigHodo.h"
#include "GroupDAQ.h"
#include "GroupMwpc.h"
#include "GroupMumega.h"
#include "GroupGEM.h"
#include "GroupPGEM.h"
#include "GroupSili.h"
#include "GroupMuonWallA.h"
#include "GroupStraws.h"
#include "GroupBeamKiller.h"
#include "GroupCEDAR.h"
#include "GroupRiAPV.h"
#include "GroupRICH_MAPMT.h"
#include "GroupRW.h"
#include "GroupRPD.h"
#include "GroupECAL1.h"
#include "GroupCamera.h"
#include "GroupNA64.h"

#include "MyLock.h"

// NA64 specific:
#include "GroupHodo.h"
#include "GroupMM.h"
#include "PlaneCalo.h"
#include "PlaneMM.h"
#include "GroupHCAL.h"
#include "PlaneV260.h"

#include "ChipNA64TDC.h"
#include "ChipNA64WaveBoard.h"

#define THREADWAITTIME 50

#if USE_DATABASE == 1
#include "MySQLDB.h"
#define DBSERVER "wwwcompass.cern.ch"
#define DBUSER "anonymous"
#define DBNAME "runlb"
#endif

#include "Geometry.h"

#if __GNUC__ >= 3
__const unsigned short int *__ctype_b = *__ctype_b_loc ();
#endif


std::map<std::string,Plane*> Monitor::fDetMap;
std::vector<Plane*> Monitor::fPlanes;
std::map<std::string,Group*> Monitor::fGroupMap;
std::map<std::string,Plane*> Monitor::fDetMapRegistered;
std::map<std::string,Plane*> Monitor::fDetMapNotRegistered;

const char* Monitor::fAllPlanesName = "All Planes";

// workaround to the fact that this srcIDtoscan is deleted at each new run
std::set<CS::uint16>      srcID_to_scan_to_reload;


ClassImp(Monitor)

Monitor::Monitor() :
TObject(),fTh(0),fThreadRun(false), fClosingWindow(false), 
  fRunClear(true), fExpectingRunNumber(0), fTree(0),
  fDoTree(false), fDoTracking(false), fDoClustering(false),
  fDoTextOutput(false), fDoCalib(false), fStopAtEndRun(false),
  fReadFailedEvt(false), fExpertHistos(false), fStopGo(false),
  fRootFile(0), fRootFileFlushed(false), fNevent(0),
  fNeventMax(0), fMinEvtspacing(0), fSpecificSpill(-1),
  flag_end(false), fThreadFinished(false), fIsInitialized(false),
  fControlPanel(0),
  fTMPattern(0xffffffff), fTMLowerOnly(true), fTMStrict(false), fWkEnvStr("AFS") {

  fEvtTypesOK.insert(CS::DaqEvent::END_OF_BURST);
  fEvtTypesOK.insert(CS::DaqEvent::PHYSICS_EVENT);
  fEvtTypesOK.insert(CS::DaqEvent::CALIBRATION_EVENT);

  fEvtTypes[CS::DaqEvent::START_OF_RUN]=("START_OF_RUN");
  fEvtTypes[CS::DaqEvent::END_OF_RUN]=("END_OF_RUN");
  fEvtTypes[CS::DaqEvent::START_OF_RUN_FILES]=("START_OF_RUN_FILES");
  fEvtTypes[CS::DaqEvent::END_OF_RUN_FILES]=("END_OF_RUN_FILES");
  fEvtTypes[CS::DaqEvent::START_OF_BURST]=("START_OF_BURST");
  fEvtTypes[CS::DaqEvent::END_OF_BURST]=("END_OF_BURST");
  fEvtTypes[CS::DaqEvent::PHYSICS_EVENT]=("PHYSICS_EVENT");
  fEvtTypes[CS::DaqEvent::CALIBRATION_EVENT]=("CALIBRATION_EVENT");
  fEvtTypes[CS::DaqEvent::END_OF_LINK]=("END_OF_LINK");
  fEvtTypes[CS::DaqEvent::EVENT_FORMAT_ERROR]=("EVENT_FORMAT_ERROR");

  fDetInTree["Calorimeters"]=true;
  fDetInTree["Micromegas"]=true;
  fDetInTree["StrawTubes"]=true;
  fDetInTree["Scalers"]=true;
  fDetInTree["GEMs"]=true;
  fDetInTree["BMS"]=true;
  fDetInTree["Silicons"]=true;

#if USE_DATABASE == 1
  fDataBase = 0;
#endif

  // CPU performance tracking
  fBenchDecoding.Stop();
  fBenchClustering.Stop();
  fBenchTracking.Stop();
}


void Monitor::Init(std::string mapfile, std::string rootfile, std::vector<std::string>& datafilelist, std::string groupfile, std::string geomfile) {

  //CS::Exception::SetActivity(false);

  fNfiles=datafilelist.size();
  if (fNfiles < 1) {
    std::cerr<<"Monitor::Init: no datafile given, can't do anything !"<<std::endl;
    exit(1);
  }

  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<"             CC     OO     OO     OO    L   "<<std::endl;
  std::cout<<"            C  C   O  O   O  O   O  O   L   "<<std::endl;
  std::cout<<"            C      O  O   O  O   O  O   L   "<<std::endl;
  std::cout<<"            C      O  O   O  O   O  O   L   "<<std::endl;
  std::cout<<"            C  C   O  O   O  O   O  O   L   "<<std::endl;
  std::cout<<"             CC     OO     OO     OO    LLLL"<<std::endl;
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<"************* Starting monitoring session **************************"<<std::endl;
  std::cout<<"mapping file : "<<mapfile<<std::endl;
  std::cout<<"root file    : "<<rootfile<<std::endl;
  {
    for (size_t i = 0; i < datafilelist.size(); ++i) {
      std::cout<<"data file "<<(i+1)<<"  : "<<datafilelist[i]<<std::endl;
    }
  }
  std::cout<<"geometry     : "<<geomfile<<std::endl;
  std::cout<<"group file   : "<<groupfile<<std::endl;
  std::cout<<"working env  : "<<fWkEnvStr<<std::endl;
  std::cout<<"********************************************************************"<<std::endl;
  std::cout<<std::endl;

  // Just getting out the run number, and the list of detectors
  const std::string& datafile1 = datafilelist[0];
#ifndef __CINT__
  fManager=new CS::DaqEventsManager(ApplicationName);
  fManager->SetMapsDir(mapfile);
  fManager->AddDataSource(datafile1.c_str());

  std::cout<<std::endl;
  std::cout<<"Opening data file :"<<std::endl;
  std::cout<<"********************************************************************"<<std::endl;

  if( !fManager->ReadEvent() )
    {
      CS::Exception::PrintStatistics();
      cout << "WARNING: no events in the data source, exit." << endl;
      exit(1);
    }
  time_t ttt = 0;
  if(fExpectingRunNumber == 0)
    {
      CS::DaqEvent &event=fManager->GetEvent();
      fRunNumber=event.GetRunNumber();
      ttt = event.GetTime().first;
    }
  else
    {
      int count = 0;
      while(1) 
      {
        sleep(1);
        if( !fManager->ReadEvent() )
          {
            CS::Exception::PrintStatistics();
            exit(1);
          }
        CS::DaqEvent &event=fManager->GetEvent();
        fRunNumber=event.GetRunNumber();
        ttt = event.GetTime().first;
        if((int)fRunNumber == fExpectingRunNumber)
          break;
        int d = (int)fRunNumber - fExpectingRunNumber;
        if(d > 100000) // it is, probably, still dry run. let's wait a little.
          ;
        else if(d > 0 && d < 100) // most probably we miss the expecting run
          {
            fprintf(stderr, "Bad current run number %u. (We are expecting %d)\n", fRunNumber, fExpectingRunNumber);
            exit(3);
          }
        count++;
        if(1)
          {
            fRunTime = *localtime(&ttt); 
            fprintf(stderr,"DEBUG: Run number: %u, Burst number: %u Event number: %u, time: %s\n",
                  fRunNumber,event.GetBurstNumber(), event.GetEventNumberInBurst(),asctime(&fRunTime));
          }
        if(count > 100)
          {
            fprintf(stderr,"Timeout of waiting of expecting run number %d\n", fExpectingRunNumber);
            exit(4);
          }
      }
    }

  //fManager->GetDaqOptions().Print();
  Monitor::fPlanes.resize(fManager->GetDetectors().size(),NULL);
  fRunTime = *localtime(&ttt);

  std::cout<<std::endl;
  std::cout<<"Run number "<<fRunNumber<< " start time "<<asctime(&fRunTime)<<std::endl;

  // set working env in planes and groups
  Plane::setWkEnvStr(fWkEnvStr.c_str());
  Group::setWkEnvStr(fWkEnvStr.c_str());

  /// set expert histo flag
  Plane::setExpertHistoFlag(fExpertHistos);
  Group::setExpertHistoFlag(fExpertHistos);

  /// set do tracking flag
  Plane::setDoTrackingFlag(fDoTracking);
  Group::setDoTrackingFlag(fDoTracking);

  // creating all planes:
  FillDetList();

  // initialize database
#if USE_DATABASE == 1
  std::cout<<std::endl;
  std::cout<<std::endl;
  std::cout<<"Connect to MySQL database :"<<std::endl
           <<"********************************************************************"<<std::endl;
  fDataBase = new MySQLDB(DBSERVER, DBUSER, "", DBNAME);
  fDataBase->ConnectDB();
  fDataBase->setSpecialPlace(fWkEnvStr);
  std::cout<<std::endl;
#endif

  // initialize geometry
  Geometry* geom = 0;
  if (geomfile != "") {
    std::cout << "Intialize geometry :" << std::endl
              <<"********************************************************************"<<std::endl;
    try {
      geom = new Geometry(geomfile.c_str());
    }
    catch(...) {
      std::cerr<<"WARNING Monitor::Init. Geometry problem, tracking deactivated"<<std::endl;
      geom=0;
    }
    std::cout<<std::endl;
  }

  //global tree. will be branched by detectors in fDetectors
  if (rootfile == "DEFAULT") {
    // WARNING !!! will not work in case of a stream !!!
    rootfile = datafile1;
    rootfile.erase(0,rootfile.rfind("/")+1);
    if (rootfile.rfind(".") != (size_t)-1) rootfile.erase(rootfile.rfind("."),rootfile.size());
    std::string dir;
    if(!fRootDir.empty()) {
      dir=fRootDir + "/";
    }
    else {
      dir=getenv("HOME");
      dir += "/w0/";
    }
    rootfile = dir + rootfile;
    rootfile += ".root";
  }
  
  cout << "Root tree: " << rootfile << endl;
  if (rootfile != "")
    fRootFile=new TFile(rootfile.c_str(),"recreate");
  
  if(fRootFile && fRootFile->IsOpen()) {
    if(fDoTree) {
      fTree=new TTree("T","COOOL");
      fTree->Branch("runno",&fRunNumber,"runno/I",32000);
      fTree->Branch("spill",&fSpill,"spill/I",32000);
      fTree->Branch("evt",&fNevent,"evt/I",32000);
      fTree->Branch("evtinspl",&fEvtinspl,"evtinspl/I",32000);
      fTree->Branch("timesec",&fTimesec,"timesec/I",32000);
      fTree->Branch("timemic",&fTimemic,"timemic/I",32000);
      fTree->Branch("evtype",&fEvtType,"evtype/I",32000);
      fTree->Branch("pattern",&fPattern,"pattern/i",32000);
      fTree->Branch("attributes",&fAttributes,"attributes/I",32000);
      fTree->Branch("filterbits",&fFilterBits,"filterbits/I",32000);
    }
  }
  else {
    if (rootfile != "") {
      std::cout<<"WARNING : Monitor::Init : Cannot open root output file "
               <<rootfile<<std::endl;
    }
    fRootFile = 0;
  }

  // set maps for APV detector planes
  PlaneAPV::SetRunMaps(&fManager->GetMaps());
  
  // initialize planes
  cout << "Init planes ..." << endl;
  
  // 'i' is <std::string,Plane*>
  for (const auto& i : Monitor::fDetMap) {
    Plane* plane = i.second;
    //cout << " plane " << plane->GetName() << endl;

    // reading calibrations
#if USE_DATABASE == 1
    plane->setDBpt(fDataBase);
#endif
    plane->OpenReference();
    if(fDoCalib)
      plane->ReadCalib(fRunTime);

    // association to a GeomPlane.
    if(geom && plane->NeedGeom())
      plane->SetGeometry( geom->GetPlane(plane->GetName()) );
    // plane initialization. must be done after geometry association
    // histograms are booked here. we create the good directory in
    // the root file and cd to it before calling the function

    if(fRootFile) {
      std::string dettype = plane->GetType();
      fRootFile->cd();

      // directory for this detector type
      if(!(fRootFile->GetKey(dettype.c_str())))
        fRootFile->mkdir(dettype.c_str());

      // directory for this detector
      TDirectory *subdir = dynamic_cast<TDirectory*>(fRootFile->Get(dettype.c_str()));
      subdir->mkdir(plane->GetName());

      std::string path = dettype;
      path += "/";
      path += plane->GetName();

      fRootFile->cd(path.c_str());
    }
    plane->Init(fTree);
    if(fRootFile) fRootFile->cd();
  }

  // load groups
  cout << "Loading groups from " << groupfile << endl;
  if (groupfile != "") LoadGroups(groupfile.c_str());

  // initialize groups
  for(std::map<std::string, Group*>::iterator ig=fGroupMap.begin(); ig!=fGroupMap.end(); ig++) {
    Group* group = ig->second;

#if USE_DATABASE == 1
    group->setDBpt(fDataBase);
#endif
    
    // TODO: rewise the current implementation which is
    //       the per-group call to parent OpenReference();
    //       for example GroupMM.cc:92, etc
    //group->OpenReference();
    
    if(fRootFile) {
      std::string groupname = group->GetName();
      std::string grouptype = group->GetType();
      fRootFile->cd();
      if(groupname != fAllPlanesName) {
        std::string groupdirname = "GROUP_";
        groupdirname += groupname;

        // directory for this detector type
        TDirectory *subdir = (TDirectory*)fRootFile->Get(grouptype.c_str());
        if(!subdir) {
          subdir = fRootFile->mkdir(grouptype.c_str());
        }
        // directory for this group
        subdir->mkdir(groupdirname.c_str());

        std::string path = grouptype;
        path += "/";
        path += groupdirname;
        fRootFile->cd(path.c_str());
      }
    }
    group->Init();
    if(fRootFile) fRootFile->cd();
  }

  // restarting decoding
  delete fManager;
  fManager=new CS::DaqEventsManager;
  for (const auto& j : datafilelist) {
    fManager->AddDataSource(j);
  }
  fManager->SetMapsDir(mapfile);

  std::cout<<std::endl;
  std::cout<<"Starting decoding :"<<std::endl;
  std::cout<<"********************************************************************"<<std::endl;

  fManager->Print();
#endif // ifndef __CINT__

#if USE_DATABASE == 1
  fDataBase->DisconnectDB();
#endif

  // notify all groups and planes that a new run has started
  NewRun(fRunNumber);

  // workaround to the fact that this srcIDtoscan is deleted at each new run
  srcID_to_scan_to_reload = fManager->GetDaqOptions().GetSrcIDScan();

  std::cout <<"\nEvent types to read:";
  for (const auto i : fEvtTypesOK) {
    std::cout << " " << fEvtTypes[i];
  }
  std::cout<<std::endl;
  
  printf("Trigger mask used: 0x%x\n\n", fTMPattern);
  
  SetEventNumber(fNeventMax);

  fIsInitialized=true;
}



Monitor::~Monitor() {
  volatile bool* thfinish = &fThreadFinished;
  if(fIsInitialized) {
    // stop thread
     if(fTh) {
      ThreadStopRequest();
      int ii;
      for (ii=0; ii<THREADWAITTIME; ii++) {  // we don't wait indefinitly...
        if (*thfinish) {
          break;
        }
        gSystem->Sleep(400);
      }
      ThreadStop();
    }

    // close rootfile
    if (fRootFile) {
      CloseRootFile();  // if it has not been already called by the thread
      fRootFile->Close();
      SafeDelete(fRootFile);
      fRootFile = 0;
      fTree = 0;
    }

#if USE_DATABASE == 1
    if (fDataBase) SafeDelete(fDataBase);
#endif

    cout << "Processing time spend (seconds per event):\n"
         << "  decoding : \tRT "<<fBenchDecoding.RealTime()/fNevent
               <<" \tCPUT "<<fBenchDecoding.CpuTime()/fNevent<<endl
         << "  clustering : \tRT "<<fBenchClustering.RealTime()/fNevent
               <<" \tCPUT "<<fBenchClustering.CpuTime()/fNevent<<endl
         << "  tracking : \tRT "<<fBenchTracking.RealTime()/fNevent
               <<" \tCPUT "<<fBenchTracking.CpuTime()/fNevent<<endl;

    // TODO: free the memory, but it crash for the moment
    //for (auto i : Monitor::fGroupMap) delete i.second;
    //for (auto i : Monitor::fPlanes) delete i;

    //   exit(0); give crashes
  }
}


void Monitor::CloseRootFile() {

  if (fRootFileFlushed) return;  // this has already been done

  // protect this method if it is called by the 2 threads in the same time
  if (fThreadRun && thr_flag) MYLOCK();

  if (!fRootFileFlushed) {

    typedef std::map<std::string,Plane*>::iterator PI;
    for(PI ip=fDetMap.begin(); ip!=fDetMap.end(); ip++) {
      (*ip).second->End();
    }

    TextOutput(fRunNumber);

    // Close properly the root file at the end of the run or at a break

    if (fRootFile) {
      fRootFile->cd();
      fRootFile->Write();
      std::cout<<"histograms saved in "<<fRootFile->GetName()<<std::endl;
      //     fRootFile->Close();
      //     SafeDelete(fTree);
      //     SafeDelete(fRootFile);
      //     fRootFile = 0;
      //     fTree = 0;
    }

    fRootFileFlushed = true;
  }

  if (fThreadRun && thr_flag) MYUNLOCK();
}


void Monitor::FillDetList() {

  std::cout<<std::endl;
  std::cout<<"=> Creating Plane objects..."<<std::endl;

  NewGroup(fAllPlanesName,"ALL");
#ifndef __CINT__
  for (const auto& i : fManager->GetMaps()) {
    std::string detname=i.second->GetDetID().GetName();
    unsigned int detnum=i.second->GetDetID().GetNumber();
    Plane* det = NewDetector(detname.c_str(), detnum);
  }
  std::cerr<<std::endl;
#endif
}


Group* Monitor::NewGroup(const char* grpname, const char* grptype)
{
  // check the group exist already
  Group* grp = GetGroup(grpname);
  if (grp) return grp;
  
  const string type = (grptype == 0) ? grpname : grptype;

  if (type == "ALL") { // groups all planes for future gui
    grp = new Group(grpname);
  }
  else if (type == "DAQ") { // display DAQ variables
    grp = new GroupDAQ(grpname);
  }
  else if (type == "ST") { // straws
    grp = new GroupStraws(grpname);
  }
  else if (type == "MM") { // MicroMegas
    grp = new GroupMM(grpname);
  }
  else if (type == "EC") { // Calorimeters
    grp = new Group(grpname);
  }
  else if (type == "HC") { // Calorimeters
    grp = new GroupHCAL(grpname);
  }
  else if (type == "HD") { // Hodoscope
    grp = new GroupHodo(grpname);
  }
  else if (type == "GEM") { // Group for GEM
    grp = new GroupGEM(grpname);
  }
  else if (type == "BM") { // beam momentum station
    grp = new Group(grpname);
  }
  else if (type == "SILtrack") { // Group for Silicon track
    grp = new GroupSiliTrack(grpname);
  }
  else if (type == "SIL") { // Group for 1 Silicon station
    grp = new GroupSili(grpname);
  }
  else if (type == "NA64") { // Group for NA64 Tracking Station
    grp = new GroupNA64(grpname);
  }
  
  // rest of groups are not used in P348 (as of 2021)
  
  else if (type == "CEDAR") { //CEDARs
    grp = new GroupCEDAR(grpname);
  }
  else if (type == "HK") { //Beam Killers
    grp = new GroupBeamKiller(grpname);
  }
  else if (type == "H") { // Trigger Hodoscope
    grp = new GroupTrigHodo(grpname);
  }
  else if (type == "RA") { // Rich APV
    grp = new GroupRiAPV(grpname);
  }
  else if (type == "RM") { // Rich MAPMT
    grp = new GroupRICH_MAPMT(grpname);
  }
  else if (type == "FI") { // SciFis japanese
    grp = new GroupScifiJ(grpname);
  }
  else if (type == "SF") { // SciFis german
    grp = new GroupScifiG(grpname);
  }
  else if (type == "PS") { // Mwpc's Star
    grp = new GroupMwpc(grpname);
  }
  else if (type == "PA") { // Mwpc's A
    grp = new GroupMwpc(grpname);
  }
  else if (type == "PB") { // Mwpc's B
    grp = new GroupMwpc(grpname);
  }
  else if (type == "DC") { // drif chambers
    grp = new Group(grpname);  // no specific group for the moment
  }
  else if (type == "GEMtrack") { // Group for GEM tracking
    grp = new GroupGEMTrack(grpname);
  }
  else if (type == "PGEM") { //Group for 1 PGEM station
    grp = new GroupPGEM(grpname);
  }
  else if (type == "MWA") { // Group for Muon Wall A
    grp = new GroupMuonWallA(grpname);
  }
  else if (type == "DR") { // Group for RichWall
    grp = new GroupRichWall(grpname);
  }
  else if (type == "RPD") { // Group for RPD
    grp = new GroupRPD(grpname);
  }
  else if (type == "CA") { // Group for Camera
    grp = new GroupCamera(grpname);
  }  
  else { // non recognized group
    cerr << "group type not recognized: " << type << " name: " << grpname << endl;
    grp = new Group(grpname);
  }
  
  grp->SetType(type.c_str());

  Monitor::fGroupMap[grpname] = grp;
  //std::cerr<<".";
  return grp;
}


Plane* Monitor::NewDetector(const char* detname, unsigned int detnum)
{
  if(detnum >= fPlanes.size())
    throw CS::Exception("Monitor::NewDetector : DetID not valid");

  // this plane already exists
  typedef std::map<std::string,Plane*>::iterator MI;
  MI i=Monitor::fDetMapRegistered.find(detname);
  if(i!=fDetMapRegistered.end()) return i->second;  // already registered
  
  MI inot=Monitor::fDetMapNotRegistered.find(detname);
  if(inot!=fDetMapNotRegistered.end()) return 0;  // already seen as not registered

  Plane* det = 0;
  
  const bool calotree = (fDetInTree.find("Calorimeters")->second != 0);
  const string name = string(detname);
  const string detname4 = string(detname).substr(0, 4);
  const string detname3 = string(detname).substr(0, 3);
  const string detname2 = string(detname).substr(0, 2);
  
  // extract dimensions of coordinate detectors
  const int nchan = fManager->GetMaps().GetWires(CS::DetID(detname));
  
  // extract size of X/Y dimensions of SADC detectors
  // TODO: move into DDD / Chip.h / Chip::Maps
  
  const Chip::Maps& m = fManager->GetMaps();
  // class Maps : public std::multimap<DataID,Digit*>
  //m.Print();
  
  set<int> nx;
  set<int> ny;
  map<pair<int,int>,bool> isWB; //A.C. using a map since I do not know a-priori the size of the matrix
  
  for (Chip::Maps::const_iterator i = m.begin(); i != m.end(); ++i) {
    //i->second->Print();
    
    const ChipSADC::Digit* d = dynamic_cast<const ChipSADC::Digit*>(i->second);
    if (d) {
      //d->Print();
      if (name != d->GetDetID().GetName()) continue;
      //cout << "detname=" << d->GetDetID().GetName() << " x=" << d->GetX() << " y=" << d->GetY() << endl;
      const int theX=d->GetX();
      const int theY=d->GetY();
      nx.insert(theX);
      ny.insert(theY);
      isWB[std::make_pair(theX,theY)]=0;
    }
    else{
      //A.C. Waveboard
      const ChipNA64WaveBoard::Digit* dWB = dynamic_cast<const ChipNA64WaveBoard::Digit*>(i->second);
      if (dWB) {
        //d->Print();
        if (name != dWB->GetDetID().GetName()) continue;
        //cout << "detname=" << d->GetDetID().GetName() << " x=" << d->GetX() << " y=" << d->GetY() << endl;
        const int theX=dWB->GetX();
        const int theY=dWB->GetY();
        nx.insert(theX);
        ny.insert(theY);
        isWB[std::make_pair(theX,theY)]=1;
      }
    }
  }
  
  cout << "Monitor::NewDetector(): new plane:"
       << " detname=" << name  << " detnum=" << detnum
       << " nx=" << nx.size() <<  " ny=" << ny.size() << " nchan=" << nchan
       << endl;
  
  if (detname2 == "TT") {
    // Trigger time
    det = new Plane1V(detname, nchan, 0, 100);
  }
  // SADC-based detectors
  else if (name == "ECALSUM" ||
      name == "SRD" ||
      name == "S1" || name == "S2" || name == "T2" || name == "S3" || name == "S4" ||
      name == "Smu" || name == "BeamK" ||   // 2023 mu run
      name == "V1" || name == "V2" || name == "ZC" ||
      detname4 == "ECAL" ||
      detname4 == "HCAL" ||
      detname4 == "MUON" ||
      name == "VHCAL0" || name == "VHCAL1" ||
      name == "LTG" || name == "CHCNT" ||   // hadron run 2023 june30 - july05
      name == "BGO" ||
      name == "LYSO" ||
      name == "VETO" ||
      name == "ATARG" ||
      name == "TRIG" ||      // trigger inputs (only run 2015)
      name == "WCAL" ||      // visible mode detectors/channels
      name == "WCAT" ||
      name == "VTWC" || name == "VTEC" ||
      name == "DM") {
    if (calotree) {
      det = new PlaneCalo(detname, nx.size(), ny.size(),isWB);
    }
  }
  
  // hodoscopes
  else if (detname4 == "HOD0" || detname4 == "HOD1" || detname4 == "HOD2") {
    if (calotree) {
      det = new PlaneCalo(detname, nx.size(), ny.size(),isWB, 12, 500);
    }
  }
  
  // micromegas
  else if(detname2 == "MM") {
    if((fDetInTree.find("Micromegas"))->second) {
      det = new PlaneMM(detname, nchan);
    }
  }
  
  // GEMs
  else if(detname2 == "GM") {
    if((fDetInTree.find("GEMs"))->second) {
      det = new PlaneGEM(detname, nchan, 500, 500);
    }
  }
  
  // Straw tubes
  else if(detname2 == "ST") {
    if((fDetInTree.find("StrawTubes"))->second) {
      // TODO: current mapping gives nchan=65,
      //       check mappings for replace "64 -> nchan"
      det = new PlaneStrawTubes(detname, 64, 500, 1500);
    }
  }
  
  else if(detname2 == "BM") { // BMS
    if((fDetInTree.find("BMS"))->second) {
      det=new PlaneBMS(detname,nchan,0,20000);
    }
  }
  
  // Silicon's
  else if(detname2 == "SI") {
    if((fDetInTree.find("Silicons"))->second) {
      PlaneSili *sili = new PlaneSili(detname,nchan,500,500);
      sili -> SetRunMaps(&Monitor::fManager->GetMaps());
      det = sili;
    }
  }
  
  // Scalers
  else if(name == "SCALERS") {
    if((fDetInTree.find("Scalers"))->second) {
      det = new PlaneV260(detname, nchan);
    }
  }
  
  else if(name == "TCSphase") { // TCS phase
    // should always be activate as it is needed for time reconstruction of
    // all APV detectors plus ECALS
    
    if (nchan != 1) {
      std::cerr << "Monitor::NewDetector() Exactly one channel must be mapped onto TCSphase (" << nchan << " actually are). Check mapping!" << std::endl;
      exit(1);
    }
    det = new PlaneTcsPhase(detname);
  }
  
  Monitor::fPlanes[detnum]=det;
  
  if(det) {
    det->SetTree(fTree);
    if (!fDetMap[det->GetName()]) GetGroup(fAllPlanesName)->Add(det);
    Monitor::fDetMap[det->GetName()]=det;
    Monitor::fDetMapRegistered[detname]=det;
    fManager->GetMaps().GetSrcIDs(detname, fManager->GetDaqOptions().GetSrcIDScan());
    //    std::cerr<<"detname entree: "<<detname<<" sortie: "<<det->GetName()<<std::endl;
    //std::cerr<<".";
  } else {
    Monitor::fDetMapNotRegistered[detname]=0;
    std::cout << "Monitor::NewDetector() detector type not recognized: " << detname << std::endl;
  }

  return det;
}

#include "eventflags.h"

EventFlags CalcEventFlags(CS::DaqEventsManager* manager)
{
  bool trigPHYS = false;
  bool trigBEAM = false;
  bool trigRAND = false;

  const CS::DaqEvent& event = manager->GetEvent();
  const int run   = event.GetRunNumber();
  const int spill = event.GetBurstNumber();
  const int spill_event = event.GetEventNumberInBurst();
  const int event_time = event.GetTime().first;

  // extract trigger type from the trigger TDC
  for (const auto& i : manager->GetEventDigits()) {
    const CS::Chip::Digit* digit = i.second;
    const CS::ChipNA64TDC::Digit* tdc = dynamic_cast<const CS::ChipNA64TDC::Digit*>(digit);
    if (!tdc) continue;
    
    const std::string name = tdc->GetDetID().GetName();
    const bool is_tt_tdc = (name == "STT0XY") && (tdc->GetSourceID() == 760) && (tdc->GetPort() == 0);
    if (!is_tt_tdc) continue;
    
    const int tdcChannel = tdc->GetChannel();
    // Compatibility with sessions before 2024
    if (run <= 10315) {
      switch (tdcChannel) {
        // Primitives (before prescaler)
        case 33:  // physical trigger
          trigPHYS = true;
          break;
        case 41: // beam trigger
          trigBEAM = true;
          break;
        case 37: // random trigger
          trigRAND = true;
          break;
      }
    } else {
      switch (tdcChannel) {
        // Primitives (before prescaler)
        case 28:  // physical trigger
          trigPHYS = true;
          break;
        case 26: // beam trigger
          trigBEAM = true;
          break;
        case 27: // random trigger
          trigRAND = true;
          break;
      }
    }
  }
   
  static int prevrun = 0;
  static int prevspill = 0;
  static int nCalib = 0;
  static int event1_time = 0;
  
  EventFlags f;
  const int eventType = event.GetType();
  f.isPhysics = (eventType == CS::DaqEvent::PHYSICS_EVENT);
  f.isCalibration = (eventType == CS::DaqEvent::CALIBRATION_EVENT);
  
  // reset state on new spill
  const bool isSameSpill = (run == prevrun) && (spill == prevspill);
  if (!isSameSpill) {
    prevrun = run;
    prevspill = spill;
    nCalib = 0;
    event1_time = event_time;
  }
  
  if (f.isPhysics) nCalib = 0;
  if (f.isCalibration) nCalib += 1;
  
  f.isOffSpill = (nCalib > 1);
  f.isOnSpill = !f.isOffSpill;
  f.isAfterSpill = f.isOffSpill && (event_time - event1_time > 5);
  
  f.isTriggerPhys = trigPHYS;
  f.isTriggerBeam = trigBEAM;
  f.isTriggerRand = trigRAND;
  
  /*
  cout << "CalcEventFlags() "
       << " spill=" << spill << " event=" << spill_event
       << " flags:"
       << " isPhysics=" << f.isPhysics
       << " isCalibration=" << f.isCalibration
       << " isOnSpill=" << f.isOnSpill
       << endl;
  */
  
  return f;
}

void Monitor::Run()
{
#ifndef __CINT__
  int n = 0;
  int burstNumber = 0;
  try
  {
    fLastDecodedEventnum=0;
    while(!flag_end)
    {
      // checking if the stopgo button is true (suspend events reading)
      // wait in this case
      if (fStopGo)
        {
          printf("*** Sleeping...\n");
          gSystem->Sleep(100000);
          continue;
        }
      
      // resetting all planes at the beginning of the event
      // 'i' is <std::string,Plane*>
      for (const auto& i : Monitor::fDetMap) {
        i.second->Reset();
      }

      // do we need to reset groups ?
      if(Plane::ModifiedSemaphore()) {
        Plane::ModifiedAcknowledge();
        for (const auto& ig : fGroupMap) {
          ig.second->ResetIfNeeded();
        }
        for (const auto& ip : fDetMap)
          ip.second->Modified(0);
      }

      if( !fManager->ReadEvent() )
        {
          if(fNeventMax == 0 || fNevent < fNeventMax)
          {
            cout << "****************************************************\n";
            cout << "Read Event error. Data source [0] = \"" << fManager->GetDataSources()[0] << "\"" << endl;
            cout << "****************************************************\n" << endl;
            if(fManager->GetDataSources()[0].c_str()[0] == '@')
              {
                burstNumber = 0;
                n++;
                fManager->AddDataSource(fManager->GetDataSources()[0]);
                cout << "Waiting 10 seconds." << endl;
                sleep(10);
                
                // sometimes it takes significant time until events flow recovery,
                // wait endlessly for new event
                continue;
                //if(n < 10) continue;
              }
          }
          ThreadStopRequest();
          break;
        }
      
      n = 0;
      CS::DaqEvent &event = fManager->GetEvent();
      // get some information about this event from the decoding library
      fSpill=event.GetBurstNumber();
      fEvtinspl=event.GetEventNumberInBurst();
      // keep the time of previous event for proper end-of-run output
      // TODO: organize this mess one day!
      const time_t timePrev = fTimesec;
      fTimesec=event.GetTime().first;
      fTimemic=event.GetTime().second;
      fEvtType=event.GetType();
      fPattern=event.GetTrigger();
      fAttributes= 0;
      
      // fPattern old way, to be removed?
      //       CS::DaqEvent::Header head=event.GetHeader();
      //       fPattern=head.typeAttribute[1];
      // fAttributes old way, to be removed?
      //       fAttributes=head.typeAttribute[0];
      // for new version of decoding lib (new DATE v5)
      //       fAttributes= head.GetVersion()<0xffff ? head.GetHeaderOld().typeAttribute[1] : head.GetHeader36().event_trigger_pattern[0];
      
      /*
      cout << "INFO: new event"
           << " run=" << fRunNumber << " spill=" << fSpill << " event=" << fEvtinspl
           << " time=" << fTimesec
           << " type=" << fEvtType
           << " trigger=" << fPattern << " attributes=" << fAttributes
           << endl;
      */
      
      if ( fStopAtEndRun && (fEvtType == CS::DaqEvent::END_OF_RUN)) {
        std::cerr << "Begin of run event and StopAtEndRun set -> end of data read-out for run " << fRunNumber <<std::endl;
        ThreadStopRequest();
        break;
      }
      
      if (fSpill != burstNumber) {
        std::cout << "** New spill number: " << event.GetRunNumber() << "-" << fSpill << std::endl;
        burstNumber = fSpill;
      }
      
      if ( event.GetRunNumber() != fRunNumber ) {
        // run number has changed...

        TextOutput(fRunNumber, timePrev);

        if ( fStopAtEndRun ) {
          std::cerr << "New run seen and StopAtEndRun set -> end of data read-out for run " << fRunNumber <<std::endl;
          ThreadStopRequest();
          break;
        }

        unsigned int oldrunnumber = fRunNumber;
        fRunNumber = event.GetRunNumber();
        Monitor::fPlanes.resize(fManager->GetDetectors().size(),NULL);
        std::cerr << "New run number " << fRunNumber << " (last one was "<<oldrunnumber<<")"<<std::endl;

        NewRun(fRunNumber);
        // workaround to the fact that this srcIDtoscan is deleted at each new run
        fManager->GetDaqOptions().GetSrcIDScan() = srcID_to_scan_to_reload;
      }

      // check for specific spill
      if (fSpecificSpill >= 0 && fSpill != fSpecificSpill) continue;

      // check the event type
      if (fEvtTypesOK.count(fEvtType) == 0) continue;


      // trigger mask checking
      unsigned int tPattern(fPattern);
      if (fTMLowerOnly)
        tPattern &= 0xffff;
      bool accepted(true);
      if (fTMStrict)
        accepted = (tPattern==fTMPattern);
      else
        accepted = (tPattern&fTMPattern);
      if(!accepted && fPattern)
        continue;

      // throw away events that are too close
      if (fLastDecodedEventnum > fEvtinspl) // we are in a new spill
        fLastDecodedEventnum=0;
      if (fEvtinspl - fLastDecodedEventnum < fMinEvtspacing)
        continue;
      else
        fLastDecodedEventnum=fEvtinspl;

      if(flag_end) break;

      //...decode
      fBenchDecoding.Start(false);
      const bool decoded = fManager->DecodeEvent();
      fBenchDecoding.Stop();
      if( !decoded )
        {
          if (! fReadFailedEvt) {
            std::cerr << "Event " << fRunNumber << "-" << fSpill << "-" << fEvtinspl << ": decoding has failed! skipped" << endl;
            continue;
          } else {
            //           std::cerr << "Event "<<fEvtinspl<<": decoding has failed! using it anyway\n";
          }
        }

      // at last, count this event
      fNevent++;
      if((fNevent%1000)==0) {
        std::cout<<fNevent<<" events read" << std::endl;
      }
      if ( fNeventMax != 0 && fNevent >= fNeventMax)
        ThreadStopRequest();

      if(flag_end) break;

      //... set online filter data...
      CS::OnlineFilter& of=event.GetOnlineFilter();
      fFilterBits=0;
      if (of.IsMonitoring()) fFilterBits |= 0x00000001;
      if (of.IsAccepted()) fFilterBits |= 0x00000002;
      
      // compute the event/trigger type
      const EventFlags flags = CalcEventFlags(fManager);

      // Pass digits from the decoding library to Planes
      //cout << "Monitor::Run() planes StoreDigit()" << endl;
      // 'i' is <DetID,Digit*>
      for (const auto& i : fManager->GetEventDigits()) {
        const unsigned int detnum = i.first.GetNumber();
        Plane* plane = fPlanes[detnum];
        //cout << "detnum=" << detnum <<  " plane=" << plane << endl;
        
        // sanity
        const bool valid = (detnum < fPlanes.size()) && plane;
        if (!plane) continue;
        
        // check event type acceptance
        if (!plane->IsAccepted(fEvtType)) continue;
        
        plane->StoreDigit(i.second);
      }

      if(flag_end) break;

      //...Planes analysis
      //cout << "Monitor::Run() planes analysis" << endl;
      fBenchClustering.Start(false);
      // 'i' is <std::string,Plane*>
      for (const auto& i : Monitor::fDetMap) {
        Plane* plane = i.second;
        //cout << "detector=" << plane->GetName() << endl;
        
        // check event type acceptance
        if (!plane->IsAccepted(fEvtType)) continue;
        
        // analyze
        plane->UpdateRateCounter(fEvtType, flags);
        plane->EndEvent(event, flags);
        // clusterize
        if (fDoClustering) plane->Clusterize();
      }
      fBenchClustering.Stop();

      //...Groups analysis
      //cout << "Monitor::Run() groups analysis" << endl;
      fBenchTracking.Start(false);
      // 'i' is <std::string,Group*>
      for (const auto& i : Monitor::fGroupMap) {
        Group* group = i.second;
        
        // check event type acceptance
        if (!group->IsAccepted(fEvtType)) continue;
        
        // analyze
        group->UpdateRateCounter(fEvtType, flags);
        group->EndEvent(event, flags);
        if (fDoTracking) group->Tracking();
      }
      fBenchTracking.Stop();

      if(flag_end) break;
      if(!fStopGo)
        {
          if(fThreadRun && thr_flag)
          MYLOCK();
          if (fTree)
          fTree->Fill();
          if(fThreadRun && thr_flag)
          MYUNLOCK();
        }
      else
        printf("**** skip filling...\n");
    }
  }
  catch(const CS::DaqEvent::ExceptionEndOfStream& ) {
    // This is the normal exit from the loop
    std::cout<<"End of file "<<std::endl;
    //     if(fThreadRun)
    //       ThreadStop();
    //     else return;
    ThreadStopRequest();
    return;
  }

  // Something is wrong...
  // Print error message and finish this data file.
  catch(CS::DaqError &e) {
    //std::cerr<<e.what()<<std::endl;
    ThreadStopRequest();
    return;
  }

  catch( const std::exception &e ) {
    std::cerr << "exception:\n" << e.what() << "\n";
    ThreadStopRequest();
    return;
  }
  catch( const char * s ) {
    std::cerr << "exception:\n" << s << "\n";
    exit(1);
  }
  catch( ... ) {
    std::cerr << "Oops, unknown exception!\n";
    exit(1);
  }
  ThreadStopRequest();
#endif

  CS::Exception::PrintStatistics();
  CS::Chip::PrintStatistics();
  gSystem->Sleep(1000);
}


void Monitor::ResetTree() {

  if (thr_flag) MYLOCK();
  delete fTree;
  fTree=new TTree("T","COOOL");
  if (thr_flag) MYUNLOCK();
}

// #ifndef __CINT__
// void Monitor::Decode(CS::Chip& chip) {
//
//   try {
//     // Loop over all data lines in given chip.
//     // Note that argument 'maps' for constructor of CS::Chip::DataIter class
//     // is _optional_, you may skip it. Read more in the documentation page.
//
//     CS::Chip::Digits digits;
//     chip.Decode(Monitor::fManager->GetMaps(),digits,fManager->GetDaqOptions());
//
//     typedef CS::Chip::Digits::const_iterator DI;
//     for(DI it=digits.begin(); it!=digits.end(); it++ ) {
//       unsigned int detnum = it->first.GetNumber();
//
//       // Pass the digit for trigger time decoding.
//       CS::ChipF1::GetTT().DataAdd(*it->second);
//
//       if(detnum < fPlanes.size())
//         if(fPlanes[detnum])
//           fPlanes[detnum]->StoreDigit(it->second);
//
//     }
//   }
//   catch( CS::Exception &e ) {
//     //e.Print();
//   }
//   catch( ... ) {
//     std::cerr << "Unknown error in decoding!\n";
//   }
// }
// #endif


int Monitor::ThreadStart() {

  fThreadRun=true;
  if(!fTh){
    std::cout<<"Creating data treatment thread"<<std::endl;
    gSystem->Sleep(1000);
    fTh= new TThread("memberfunction",
                     (void(*) (void *))&Thread0,
                     (void*) this);
    std::cout<<"Starting data treatment thread"<<std::endl;
    gSystem->Sleep(1000);
    fTh->Run();
    return 0;
  }
  return 1;
}


void Monitor::ThreadStopRequest() {
  flag_end = true;
}


int Monitor::ThreadStop() {
  // stop all active threads
  ThreadStopRequest();
  if(fThreadRun){
    gSystem->Sleep(1000); // just to be sure...
    TThread::Delete(fTh);
    //delete fTh;
    //    fTh=0;
    fThreadRun=false;    // aborting flag
    return 0;
  }
  return 1;
}


void Monitor::Thread0(void* arg) {
  // thread function which calls user specified action Func0

  Monitor *th=(Monitor*)arg;
  TThread::SetCancelOn();
  TThread::SetCancelDeferred();
  int meid=TThread::SelfId(); // get pthread id
  std::cout << "\nThread 0, id:" <<(unsigned)meid<< " is running..\n"<<std::endl;
  TThread::CancelPoint();
  th->Run(); // call the user defined threaded function
  //  th->CloseRootFile();
  if (th->fClosingWindow == false) th->CloseRootFile();
  th->fThreadFinished = true;

  //   th->ThreadStop();
}


void Monitor::ResetHistos() {
  fNevent = 0;

  typedef std::map<std::string,Plane*>::iterator PI;
  for(PI ip=fDetMap.begin(); ip!=fDetMap.end(); ip++)
    (*ip).second->ResetHistograms();

  typedef std::map<std::string,Group*>::iterator GI;
  for(GI ig=fGroupMap.begin(); ig!=fGroupMap.end(); ig++)
    (*ig).second->ResetHistograms();

  std::cout << "\nAll histograms have been cleared"<<std::endl;
}


void Monitor::NewRun(int runnumber) {
  fBenchDecoding.Reset();
  fBenchClustering.Reset();
  fBenchTracking.Reset();

  if (thr_flag) MYLOCK();

  // i=<std::string,Plane*>
  for (auto& i : fDetMap) i.second->NewRun(runnumber);
  
  // i=<std::string,Group*>
  for (auto& i : fGroupMap) i.second->NewRun(runnumber);

  if (fRunClear) ResetHistos();

  if (thr_flag) MYUNLOCK();
}


TH1* Monitor::GetHisto(const std::string hName) {
  TH1* th1 = 0;

  for (unsigned int ii=0; ii<fPlanes.size(); ii++) {
    Plane* pl = fPlanes[ii];
    if (pl) {
      th1 = pl->GetHisto(hName);
      if (th1) return th1;
    }
  }

  for(std::map<std::string,Group*>::iterator ig=fGroupMap.begin(); ig!=fGroupMap.end(); ig++) {
    Group* gr = ig->second;
    if (gr) {
      th1 = gr->GetHisto(hName);
      if (th1) return th1;
    }
  }

  return 0;
}


void Monitor::TextOutput(int runnumber, time_t time0) {

  if ((!fDoTextOutput) && fTextDir.empty() && fTextFile.empty()) return;
  if(fTextDir.empty()) fTextDir += ".";

  // open output file
  if ( fTextFile.empty() || (fTextFile == "" )) {
    char outfilename[100];
    sprintf(outfilename,"%s/coool_output_%d.dat",fTextDir.c_str(),runnumber);
    fTextFile = outfilename;
  }

  ofstream out(fTextFile.c_str());
  if(!out) {
    std::cerr<<"WARNING cannot open text output file "<<fTextFile<<std::endl;
    return;
  }
  
  out << "BEGIN Run\n"
      << "runnumber=" << runnumber << "\n"
      << "timestamp=" << (time0 ? time0 : time(0)) << "\n"
      << "END Run\n"
      << endl;
  
  // i=<std::string,Plane*>
  for (auto& i : fDetMap) {
    out<<"BEGIN Plane "<<i.second->GetName()<<std::endl;
    i.second->TextOutput(out);
    out<<"END Plane "<<i.second->GetName()<<std::endl<<std::endl;
  }
  
  std::cout << "INFO: TextOutput() file=" << fTextFile << std::endl;
}


void Monitor::CheckEvtTypes(const char* evttypesstr) {
  typedef std::map<int,std::string>::iterator evtIT;

  const char *evtp = evttypesstr;
  const char *evtmp;
  std::string ledet;
  std::set<int> evttypesok;

  while ((evtmp = strsep((char **)&evtp, ","))) {
    ledet = evtmp;
    for (evtIT ii = fEvtTypes.begin(); ii != fEvtTypes.end(); ii++) {
      if (ledet == ii->second) { evttypesok.insert(ii->first); }
    }
  }
  CheckEvtTypes(evttypesok);
}



namespace MonitorFile {  // private namespace for the groups description decoding

  class UserData
  {
  public:
    UserData(void)
    { Clear(); }
    void    Clear(void)
    { monitor = 0; read_mode = 0; ClearGroup();
    }
    void    ClearGroup(void)
    { groupname = ""; grouptype = ""; group = 0; ClearPlane();
    }
    void    ClearPlane(void)
    { planename = ""; planetype = "";
    }
    Monitor* monitor;
    int      read_mode;
    std::string   groupname;
    std::string   grouptype;
    Group*   group;
    std::string   planename;
    std::string   planetype;
  };



  // XML_StartElementHandler
  void startElement(void *userData, const char *name, const char **atts)
  {
    UserData &d = *reinterpret_cast<UserData*>(userData);
    std::string stname(name);

    for( int i=0; atts[i]!=0; i+=2 ) {
      if (atts[i+1]==0 )
        {
#ifndef __CINT__
          CS::Exception("XML:startElement(): an attribute is zero!!");
#endif
          return;
        }
    }

    if (stname == "groupcontents") {
      //    int i;
      d.Clear();
      d.read_mode = 1;
      //     for (i=0; atts[i]!=0; i+=2 ) {
      //       std::string opt(atts[i]), val(atts[i+1]);
      //       if (opt == "version") vers = atof(atts[i+1]);
      //     }
      return;
    }

    if (d.read_mode == 1 && stname == "group") {
      d.ClearGroup();
      for (int i=0; atts[i]!=0; i+=2 ) {
        std::string opt(atts[i]), val(atts[i+1]);
        if (opt == "name") d.groupname = val;
        if (opt == "type") d.grouptype = val;
      }
      if (d.groupname != "") {
        Group* grp = d.monitor->GetGroup(d.groupname.c_str());
        if (!grp) {
          grp = d.monitor->NewGroup(d.groupname.c_str(), d.grouptype.c_str());
        }
        d.group = grp;
      }
      return;
    }

    if (d.read_mode == 1 && stname == "plane") {
      d.ClearPlane();
      if (d.group == 0) return;
      for (int i=0; atts[i]!=0; i+=2 ) {
        std::string opt(atts[i]), val(atts[i+1]);
        if (opt == "name") d.planename = val;
        if (opt == "type") d.planetype = val;
      }
      if (d.planename != "") {
        Plane* pln = d.monitor->GetPlane(d.planename.c_str());
        if (pln) d.group->Add(pln);
        
        /*
        if (!pln) {
          std::cerr << "WARNING: Monitor.cc/startElement() unknown plane name"
                    << " planename=" << d.planename
                    << " groupname=" << d.groupname
                    << std::endl;
        }
        */
      }
      return;
    }
  }


  /* XML_EndElementHandler */
  void endElement(void *userData, const char *name)
  {
    UserData &d = *reinterpret_cast<UserData*>(userData);
    std::string stname(name);
    if (stname == "groupcontents") d.read_mode = 0;
    // std::cerr << "endElement  name=" << name <<std::endl;
  }


  /* XML_DefaultHandler */
  void default_handler(void *userData,const XML_Char *s,int len)
  {
    //   UserData &d = *reinterpret_cast<UserData*>(userData);
    //   char buf[500];
    //   for (int i=0; i<len; i++) buf[i]=s[i];
    //   buf[len]=0;
    // std::cerr << "*** default_handler  len=" << len << " xml_char= " << buf <<std::endl;
    // std::cerr << "******\n";
    // Search and remove empty strings
    //   for( int j=0; j<len; j++ )
    //     if( s[j]!=' ' && s[j]!='        ' && s[j]!='\n' )
    //       std::cerr << "    mot vide\n";
    // Empty string. Ignore it.
    return;
  }

}  // end of private namespace




Bool_t Monitor::LoadGroups(const char* filename)
{
  using namespace MonitorFile;
  XML_Parser parser = XML_ParserCreate(NULL);
  UserData d;

  const unsigned buf_size=1000000;
  char *buf = new char[buf_size];
  XML_SetUserData                       (parser, &d);
  XML_SetElementHandler                 (parser, startElement, endElement);
  //   XML_SetCharacterDataHandler           (parser, characterdata_handler);
  //   XML_SetProcessingInstructionHandler   (parser, instruction_handler);
  XML_SetDefaultHandler                 (parser, default_handler);
  //   XML_SetCommentHandler                 (parser, comment_handler);


  ifstream in(filename);
  if (!in) {
#ifndef __CINT__
    CS::Exception("Can not read groups settings file\n");
#endif
    return false;
  }

  std::cout<<std::endl;
  std::cout<<"Creating Group objects :"<<std::endl;
  std::cout<<"********************************************************************";
  std::cout<<std::endl;

  d.monitor = this;
  try
    {
      int done=0;
      do
        {
          in.read(buf,buf_size);
          if( ((unsigned int)in.gcount())==buf_size )
#ifndef __CINT__
            throw CS::Exception("Monitor::LoadGroups(): too short buffer.");
#endif

          done = ((unsigned int)in.gcount()) < buf_size;
          // std::cerr << "appel XML_Parse: in.gcount()=" << in.gcount() << " done=" <<done <<std::endl;
          if( !XML_Parse(parser, buf, in.gcount(), done) )
#ifndef __CINT__
            CS::Exception("Monitor::LoadGroups(): Error: %s  at line %d\n",
                          XML_ErrorString(XML_GetErrorCode(parser)),
                          XML_GetCurrentLineNumber(parser)).Print();
#endif
        } while (!done);
    }
  catch(...)
    {
      delete [] buf;
      throw;
    }

  delete [] buf;
  in.close();
  XML_ParserFree(parser);
  std::cout<<std::endl;
  return true;
}


void Monitor::ControlPanel(const TGWindow *p, const TGWindow *main) {
  if (!fControlPanel) {
    fControlPanel = new MonitorPanel(p, main, 100, 100,
                                     this, fTMPattern, fTMLowerOnly, fTMStrict);

    typedef std::set<int>::iterator IT;
    for(IT it = fEvtTypesOK.begin(); it!= fEvtTypesOK.end(); it++)
      fControlPanel->CheckEvtType(*it);
  }
}
