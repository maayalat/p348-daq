#include "GroupNA64.h"

#include "TThread.h"

#include <cmath>
#include "GroupGEM.h"
#include "GroupPanel.h"
#include "PlaneGEM.h"
#include "TriggerTime.h"
#include "DaqEvent.h"
#include "GeomPlaneGEM.h"
#include "GroupStraws.h"
#include "PlaneStrawTubes.h"
#include "ChipF1.h"
#include "ChipNA64TDC.h"
#include "TGraphErrors.h"
#include "TFitResultPtr.h"
#include "TFitResult.h"
#include "TF1.h"
#include "math.h"

//ClassImp(GroupNA64);

void GroupNA64::Init(){
  //  const size_t n = fPlanes.size();

  for (size_t ip=0;ip<fPlanes.size();ip++){
    const PlaneMM *pmm = dynamic_cast<const PlaneMM*>(fPlanes[ip]);
    if (pmm){
      vType.push_back(1);
    }
    const PlaneGEM *pgm = dynamic_cast<const PlaneGEM*>(fPlanes[ip]);
    if (pgm){
      vType.push_back(2);
    }
    const PlaneStrawTubes *pst = dynamic_cast<const PlaneStrawTubes*>(fPlanes[ip]);
    if (pst){
      vType.push_back(3);
    }
  }

  OpenReference();

  int count=0;
  for (size_t ipa=0;ipa<fPlanes.size();ipa++){
    
    for (size_t ipb=0;ipb<fPlanes.size();ipb++){
      if(ipb<=ipa) continue;
      int nX(100); int nY(100); float eX(100); float eY(100);
      TString sta(fPlanes[ipa]->GetName());
      TString stb(fPlanes[ipb]->GetName());
      if (sta.BeginsWith("MM")) {nX=320;eX=nX*0.25;}
      else if (sta.BeginsWith("GM")) {nX=256;eX=nX*0.4;}
      else if (sta.BeginsWith("ST")) {nX=64;eX=nX*3;}
      if (stb.BeginsWith("MM")) {nY=320;eY=nY*0.25;}
      else if (stb.BeginsWith("GM")) {nY=256;eY=nY*0.4;}
      else if (stb.BeginsWith("ST")) {nY=64;eY=nY*3;}
      const TString name = sta + "_VS_" + stb;
    
    fHistList.push_back(new TH2F(name,name,nX,0,eX,nY,0,eY));
    fHistList[count]->SetOption("COLZ");
    count++;
  }
}
}

void GroupNA64::EndEvent(const CS::DaqEvent &event){
  int count(0);
  for (size_t ipa=0;ipa<fPlanes.size();ipa++){
    double posa=-1;
    if (vType[ipa]==1){
      const PlaneMM *pmma = dynamic_cast<const PlaneMM*>(fPlanes[ipa]);
      if (pmma->HasHit()) 
	posa=0.25*pmma->GetHitPos();
    }
    else if (vType[ipa]==2){
      const PlaneGEM *pgma = dynamic_cast<const PlaneGEM*>(fPlanes[ipa]);
      const std::set<CCluster1> &clu = pgma->GetGeometry()->GetClusters();
      double charge = -1;
      for (std::set<CCluster1>::iterator k=clu.begin();k!=clu.end();k++) if (charge<k->dt[2]) {posa=0.4*k->dt[3];charge=k->dt[2];}
    }
    else if (vType[ipa]==3){
      const PlaneStrawTubes *psta = dynamic_cast<const PlaneStrawTubes*>(fPlanes[ipa]);
      std::list<tuple<int,int>> listChannels;
      listChannels.clear();
      for( std::list<CS::Chip::Digit*>::const_iterator it=psta->GetDigits().begin(); it!=psta->GetDigits().end(); it++ )
	{
	  const CS::ChipNA64TDC::Digit *d=dynamic_cast<const CS::ChipNA64TDC::Digit *>(*it);
	  listChannels.push_back(make_tuple(d->GetWire(),d->GetTime()));
	}
      listChannels.sort();
      int clusterSize=0;
      if (listChannels.size()>0) clusterSize=1;
      int oldChannel = -1;
      int old_old_Time=0;

      int oldTime = 0;
      int newTime = 0;
      int newChannel =  -1;

      for (std::list<tuple<int,int>>::iterator it=listChannels.begin(); it != listChannels.end(); ++it)
	{
	  //        newChannel =  get<0>(*it);                                                                                             
	  //   newTime = get<1>(*it);                                                                                                      
	  newChannel =  get<0>(*it);
	  newTime = get<1>(*it);
	  if (newChannel==oldChannel+1 )
	    {
	      clusterSize++;
	    }
	  else
	    {
	      if (oldChannel!=-1)
		{
		  if (clusterSize==2)
		    {
		      posa = 3*oldChannel;
		      clusterSize=1;
		    }
		}
	    }
	  oldChannel = newChannel;
	  old_old_Time = oldTime;
	  oldTime = newTime;
	}
      if (clusterSize==2)
        {
          posa = 3*oldChannel;
          clusterSize=1;
        }
    }
      
    for (size_t ipb=0;ipb<fPlanes.size();ipb++){
      if(ipb<=ipa) continue;
      double posb=-1;
      if (vType[ipb]==1){
	const PlaneMM *pmmb = dynamic_cast<const PlaneMM*>(fPlanes[ipb]);
	if (pmmb->HasHit())
	  posb=0.25*pmmb->GetHitPos();
      }
      else if (vType[ipb]==2){
	const PlaneGEM *pgmb = dynamic_cast<const PlaneGEM*>(fPlanes[ipb]);
	const std::set<CCluster1> &club = pgmb->GetGeometry()->GetClusters();
    double chargeb = -1;
    for (std::set<CCluster1>::iterator k=club.begin();k!=club.end();k++) if (chargeb<k->dt[2]) {posb=0.4*k->dt[3];chargeb=k->dt[2];}
      }
      else if (vType[ipb]==3){
	const PlaneStrawTubes *pstb = dynamic_cast<const PlaneStrawTubes*>(fPlanes[ipb]);
	std::list<tuple<int,int>> listChannelsb;
	listChannelsb.clear();
	for( std::list<CS::Chip::Digit*>::const_iterator it=pstb->GetDigits().begin(); it!=pstb->GetDigits().end(); it++ )
	  {
	    const CS::ChipNA64TDC::Digit *d=dynamic_cast<const CS::ChipNA64TDC::Digit *>(*it);
	    listChannelsb.push_back(make_tuple(d->GetWire(),d->GetTime()));
	  }
	listChannelsb.sort();
	int clusterSizeb=0;
	if (listChannelsb.size()>0) clusterSizeb=1;
	int oldChannelb = -1;
	int old_old_Timeb=0;

	int oldTimeb = 0;
	int newTimeb = 0;
	int newChannelb =  -1;

	for (std::list<tuple<int,int>>::iterator it=listChannelsb.begin(); it != listChannelsb.end(); ++it)
	  {
	    //        newChannel =  get<0>(*it);                                                                                             
	    //   newTime = get<1>(*it);                                                                                                      
	    newChannelb =  get<0>(*it);
	    newTimeb = get<1>(*it);
	    if (newChannelb==oldChannelb+1 )
	      {
		clusterSizeb++;
	      }
	    else
	      {
		if (oldChannelb!=-1)
		  {
		    if (clusterSizeb==2)
		      {
			posb = 3*oldChannelb;
			clusterSizeb=1;
		      }
		  }
	      }
	    oldChannelb = newChannelb;
	    old_old_Timeb = oldTimeb;
	    oldTimeb = newTimeb;
	  }
    if (clusterSizeb==2)
      {
    posb = 3*oldChannelb;
    clusterSizeb=1;
      }
      }

      if (posa!=-1 && posb!=-1) fHistList[count]->Fill(posa,posb);
      count++;
    }
  }

  //if (needRateHistoUpdate()){
  if (!(fRateCounter%2000)){
      for (std::vector<TH1*>::iterator ith=fHistList.begin();ith!=fHistList.end();ith++){

          //int dc=0;
          //TH2F *ht2 = dynamic_cast<TH2F*>((*ith));


          /* Simple Fit. Should work, but gives strange results... Dont know yet
          double mean=ht2->GetMean();double rms=ht2->GetRMS();
          TF1 *func = new TF1("func","[0]*x+[1]",mean-rms*1.5,mean+rms*1.5);
          func->SetParameters(1,0);
          ht2->Fit(func,"QNR");
          double angle = 360./M_PI*acos(func->GetParameter(0));
          std::string str_sl(Form(" %f ",angle));
          (*ith)->SetTitle( (str_sl).c_str() );*/

          // Alternative Method with FitSlice (gaussian)
/*
          TObjArray slice_fit_a;
          TObjArray slice_fit_b;
          int meanx=std::ceil(ht2->GetMean(1)/ht2->GetXaxis()->GetBinWidth(1));
          int meany=std::ceil(ht2->GetMean(2)/ht2->GetYaxis()->GetBinWidth(1));
          int rmsx=std::ceil(ht2->GetRMS(1)/ht2->GetXaxis()->GetBinWidth(1));
          int rmsy=std::ceil(ht2->GetRMS(2)/ht2->GetYaxis()->GetBinWidth(1));
          int maxbinx=(meanx+rmsx>ht2->GetXaxis()->GetNbins()) ? ht2->GetXaxis()->GetNbins() : meanx+rmsx;
          int minbinx=(meanx-rmsx<1) ? 1 : meanx-rmsx;
          int maxbiny=(meany+rmsy>ht2->GetYaxis()->GetNbins()) ? ht2->GetYaxis()->GetNbins() : meany+rmsy;
          int minbiny=(meany-rmsy<1) ? 1 : meany-rmsy;
          TF1 *my_gaus_x = new TF1("my_gaus_x","gaus(0)",meany-1.2*rmsy,meany+1.2*rmsy);
          TF1 *my_gaus_y = new TF1("my_gaus_y","gaus(0)",meanx-1.2*rmsx,meanx+1.2*rmsx);
          std::cout << "Debug " << dc << std::endl;
          dc++;
          double slopex(-999),anglex(-999);
          TF1 *linx=new TF1("linx","[0]*x+[1]");
          if (ht2->GetEntries()>1000){
          ht2->FitSlicesX(my_gaus_x,minbinx,maxbinx,10,"QNRG4",&slice_fit_a);
          linx->SetParameter(1,0);
          TH1D *h1x = dynamic_cast<TH1D*>(slice_fit_a[1]);
          if (h1x->GetEntries()>5) {
              h1x->Fit(linx,"QNR");
              slopex = linx->GetParameter(0);
              anglex = acos(slopex);
          }
          }
          std::cout << "Debug " << dc << std::endl;
          dc++;
          double slopey(-999),angley(-999);
          TF1 *liny=new TF1("liny","[0]*x+[1]");
          if (ht2->GetEntries()>1000){
          ht2->FitSlicesY(my_gaus_y,minbiny,maxbiny,10,"QNRG4",&slice_fit_b);
          liny->SetParameter(1,0);
          TH1D *h1y = dynamic_cast<TH1D*>(slice_fit_b[1]);
          if (h1y->GetEntries()>5) {
              h1y->Fit(liny,"QNR");
              slopey = liny->GetParameter(0);
              angley = acos(slopey);
          }
          }
          std::cout << "Debug " << dc << std::endl;
          dc++;
          if (anglex!=-999 || angley!=-999){
              std::string str_slope(Form(" %f / %f ",anglex,angley));
              (*ith)->SetTitle( (str_slope).c_str() );
          }
          std::cout << "Debug " << dc << std::endl;
          dc++;
          delete linx; delete liny; delete my_gaus_x; delete my_gaus_y;
          //delete &slice_fit_a; delete &slice_fit_b; delete h1x; delete h1y;
          std::cout << "Debug " << dc << std::endl;
          dc++;*/


          /* Fit Method with TGraphErrors. Should be precise but slow. (Not working properly yet)
          //std::cout << "GroupNA64::Analyzing START: " << ht2->GetTitle() << std::endl;
          const int na = ht2->GetNbinsX();
          const int nb = ht2->GetNbinsY();
          float ax[na],axr[na],ay[na],ayr[na],bx[nb],bxr[nb],by[nb],byr[nb];
          double anglea(0),angleb(0);
          for (int i=0;i<na;i++){
              TH1D *ht1 = ht2->ProjectionX("",i+1,i+1);
              ax[i]=ht2->GetXaxis()->GetBinCenter(i+1);axr[i]=ht2->GetXaxis()->GetBinWidth(i+1)/sqrt(12);
              ay[i]=ht1->GetMean();ayr[i]=ht1->GetRMS();
              delete ht1;
          }
          TGraphErrors *ga = new TGraphErrors(na,ax,ay,axr,ayr);
          TF1 *funca = new TF1("funca","[0]*x+[1]");
          ga->Fit(funca,"QNS");
          double slopea = funca->GetParameter(0);
//              double offset = func->GetParameter(1);
          anglea = 360./M_PI*acos(slopea);
          delete funca;
          delete ga;
          for (int i=0;i<nb;i++){
              TH1D *ht1 = ht2->ProjectionY("",i+1,i+1);
              by[i]=ht2->GetYaxis()->GetBinCenter(i+1);byr[i]=ht2->GetYaxis()->GetBinWidth(i+1)/sqrt(12);
              bx[i]=ht1->GetMean();bxr[i]=ht1->GetRMS();
              delete ht1;
          }
          TGraphErrors *gb = new TGraphErrors(nb,bx,by,bxr,byr);
          TF1 *funcb = new TF1("funcb","[0]*x+[1]");
          gb->Fit(funcb,"QNS");
          double slopeb = funcb->GetParameter(0);
//              double offset = func->GetParameter(1);
          angleb = 360./M_PI*acos(slopeb);
          delete funcb;
          delete gb;
          //std::cout << ht2->GetTitle() << " : " << anglea << " / " << angleb << std::endl;
          std::string str_slope(Form(" : %f / %f",anglea,angleb));
          (*ith)->SetTitle( (str_slope).c_str() );
          //if ((*ith)->GetEntries()>100) (*ith)->Fit("pol1","QN");
          //std::cout << "GroupNA64::Analyzing END: " << ht2->GetTitle() << std::endl;
          */
      }
  }
  
  
}
