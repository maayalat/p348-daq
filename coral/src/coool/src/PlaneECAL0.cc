// --------------------- Last changes: ----------------------- //
//
// 16.07.17 - Arseny Rybnikov 
// There were added new histograms for LED, PED and physics events
//
// 08.06.17 rms ped modified
// official version 26 August 2009
// official version 20 September 2009
// 3 points around i_max 26.oct.09 
// parabola around i_max 28.oct.09
//          
// ECAL0      29 Sept 2012
// ECAL0SUM   01 Oct  2012
// ECAL0FEM   01 Oct  2012
// created by Gavrishchuk O. 10 October 2012
// ECAL0      9 April 2016
//
// version 19 May 2016
// ----------------------------------------------------------- //
  
#include "PlaneECAL0.h"
#include "ChipSADC.h"

using namespace std;
ClassImp(PlaneECAL0);
const int PlaneECAL0::fAmpCut = 10;

// --------------------------------------------- INIT --------------------------------------------- //
void PlaneECAL0::Init(TTree* tree) 
{  
  /////// READ OUT MAP FILE ///////////////////////////////////
  if (!PlaneECAL0::GetMap(ECAL0_MAP_FILE))
  {
        printf("ECAL0 map file FALL\n");
        printf("Achtung: Bad MAP file! (%s)\n",ECAL0_MAP_FILE);        
  }
  else
  {
        printf("ECAL0 map file %s            opened\n",ECAL0_MAP_FILE);
  }
  /////////////////////////////////////////////////////////////
              
  if(strncmp(GetName(),"EC00P1__",8) == 0) { fNrows = 51; fNcols=51; }
  // if(strncmp(GetName(),"EC00FEM_",8)== 0) {fNrows = 2; fNcols=7;}
  // if(strncmp(GetName(),"EC00SUM_",8)== 0) {fNrows = 2; fNcols=2;}

  char cell_name[132];
  char cHistoName[256];
  std::string amprowcol;
  std::string name;
  std::string title;
  std::string sLedPrefix  = "LED_";
  std::string sPedPrefix  = "PED_";
  std::string sPhysPrefix = "PHYS";  
  
  // --------------------------------- pedestals histograms ---------------------------------------- //
  name = sPedPrefix + "_abs_ch";
  HistoPedAbsCh = new TH2F(name.c_str(),name.c_str(),ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5,100,0,100);
  HistoPedAbsCh->SetOption("boxcol");
  HistoPedAbsCh->SetMinimum(0.0);
  HistoPedAbsCh->SetTitle("#lower[0.1]{#splitline{#scale[0.8]{             Pedestal profiles}}{#lower[0.5]{#scale[0.5]{#color[12]{Number of module = (absolute channel number/9ch)+1}}}}}");
  HistoPedAbsCh->GetXaxis()->SetTitle("Absolute channel number");
  HistoPedAbsCh->GetYaxis()->SetTitle("Amplitude, adc channel");
  AddHistogram(HistoPedAbsCh);
  
  name = sPedPrefix + "_RMS_abs_ch";
  HistoPedRmsAbsCh = new TH2F(name.c_str(),name.c_str(),ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5,20,0,20);
  HistoPedRmsAbsCh->SetOption("box");
  HistoPedRmsAbsCh->SetTitle("Pedestal profiles of RMS");
  HistoPedRmsAbsCh->GetXaxis()->SetTitle("Absolute channel number");
  HistoPedRmsAbsCh->GetYaxis()->SetTitle("RMS, adc channel");
  AddHistogram(HistoPedRmsAbsCh);
  
  name = sPedPrefix + "_sum";
  HistoPedSum = new TH1F(name.c_str(),name.c_str(),500,0,100);
  HistoPedSum->SetTitle("Summary pedestal distribution");
  HistoPedSum->GetXaxis()->SetTitle("Amplitude, adc channel");
  HistoPedSum->GetYaxis()->SetTitle("Number of counts, N");
  HistoPedSum->GetXaxis()->SetRangeUser(40,60);
  HistoPedSum->SetFillColor(18);
  AddHistogram(HistoPedSum);

  name = sPedPrefix + "_RMS_sum";
  HistoPedRmsSum = new TH1F(name.c_str(),name.c_str(),5000,0,50);
  HistoPedRmsSum->SetTitle("Summary distribution of pedestal RMS");
  HistoPedRmsSum->GetXaxis()->SetTitle("Amplitude, adc channel");
  HistoPedRmsSum->GetYaxis()->SetTitle("Number of counts, N");
  HistoPedRmsSum->GetXaxis()->SetRangeUser(0,2);
  HistoPedRmsSum->SetFillColor(18);
  AddHistogram(HistoPedRmsSum);

  name = sPedPrefix + "_MAP";
  HistoPedMap = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoPedMap->SetOption("boxcol");
  HistoPedMap->SetTitle("Pedestal MAP");
  HistoPedMap->GetXaxis()->SetTitle("X");
  HistoPedMap->GetYaxis()->SetTitle("Y");
  AddHistogram(HistoPedMap);

  name = sPedPrefix + "_RMS_MAP";
  HistoPedRmsMap_02 = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoPedRmsMap_02->SetOption("boxcol");
  HistoPedRmsMap_02->SetTitle("Pedestal RMS MAP [rms values]");
  HistoPedRmsMap_02->GetXaxis()->SetTitle("X");
  HistoPedRmsMap_02->GetYaxis()->SetTitle("Y");
  AddHistogram(HistoPedRmsMap_02);  
  
  name = sPedPrefix + "_RMS_MAP_thr";
  HistoPedRmsMap_01 = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoPedRmsMap_01->SetOption("boxcol");
  HistoPedRmsMap_01->SetTitle("Pedestal RMS MAP [Thr: 5 ch]");
  HistoPedRmsMap_01->GetXaxis()->SetTitle("X");
  HistoPedRmsMap_01->GetYaxis()->SetTitle("Y");
  AddHistogram(HistoPedRmsMap_01);

  
  // --------------------------------- physics histograms ------------------------------------------ //
  // ---------------------------- redefinition from PlaneV2 class ---------------------------------- //  
  
  name = sPhysPrefix + "_BEAM_PROF";
  HistoHitMapThr = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoHitMapThr->SetOption("Col");
  HistoHitMapThr->SetTitle("Beam profile [HITS MAP with Thr: 3 ch]");
  HistoHitMapThr->GetXaxis()->SetTitle("X");
  HistoHitMapThr->GetYaxis()->SetTitle("Y");
  HistoHitMapThr->SetMarkerSize(0.5);
  AddHistogram(HistoHitMapThr);
  
  name = sPhysPrefix + "_HITS_abs_ch";
  HistoPhysAbsCh = new TH1F/*_Ref*/(name.c_str(),name.c_str(), ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5/*,fRateCounter*/);
  HistoPhysAbsCh->SetTitle("Hits from physics events");
  HistoPhysAbsCh->GetXaxis()->SetTitle("Absolute channel number");
  HistoPhysAbsCh->GetYaxis()->SetTitle("Number of counts, N");
  HistoPhysAbsCh->SetFillColor(18);
  //   ((TH1F_Ref*)HistoPhysAbsCh)->SetReference(fReferenceDirectory);
  AddHistogram(HistoPhysAbsCh);  
  
  Plane2V::Init(tree);
  name = sPhysPrefix + "_HITS";
  fHhit->SetName(name.c_str());
  ((TH1F_Ref*)fHhit)->SetReference(fReferenceDirectory);  
  fHhit->SetTitle("Hits distribution");
  fHhit->GetXaxis()->SetTitle("Number of fired cells, N");
  fHhit->GetYaxis()->SetTitle("Number of counts, N");
  fHhit->SetFillColor(19);
  fHhit->GetXaxis()->SetRangeUser(0,100);
  
  name = sPhysPrefix + "_HITS__MAP";
  fHrc1->SetName(name.c_str());
  fHrc1->SetTitle("Hits MAP");
  fHrc1->GetXaxis()->SetTitle("X");
  fHrc1->GetYaxis()->SetTitle("Y");
  fHrc1->GetZaxis()->SetTitle("Number of hists, N");
  fHrc1->SetMarkerSize(0.5);
  fHrc1->SetOption("Col");
  
  name = sPhysPrefix + "_AMPL_MAP";
  fHrca->SetName(name.c_str());
  fHrca->SetTitle("Amplitude MAP of signals from physics events [Thr: 10 ch]");
  fHrca->GetXaxis()->SetTitle("X");
  fHrca->GetYaxis()->SetTitle("Y");
  fHrca->GetZaxis()->SetTitle("Amplitude, adc channel");
  fHrca->SetMarkerSize(0.5);
  fHrca->SetOption("Col");
  
  name = sPhysPrefix + "_AMPL_abs_ch";
  fHavsadr->SetName(name.c_str());
  fHavsadr->SetTitle("Amplitudes of signals from physics events");
  fHavsadr->GetXaxis()->SetTitle("Absolute channel number");
  fHavsadr->GetYaxis()->SetTitle("Amplitude, adc channel");
  fHavsadr->GetXaxis()->SetLimits(0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5);
  fHavsadr->SetOption("box");
  fHavsadr->SetLineColor(38);
  fHavsadr->GetYaxis()->SetRangeUser(0,160);
  
  name = sPhysPrefix + "_AMPL_sum";
  fHa->SetName(name.c_str());
  ((TH1F_Ref*)fHa)->SetReference(fReferenceDirectory);
  fHa->SetFillColor(5);
  fHa->SetTitle("Summary amplitude distribution from physics events");
  fHa->GetXaxis()->SetTitle("Amplitude, adc channel");
  fHa->GetYaxis()->SetTitle("Number of counts, N");
  fHa->SetBins(4096,0,4096);
  fHa->GetXaxis()->SetRangeUser(0,100);
  
  // --------------------------------- ------------- ---------------------------------------------- //  
  
//   name = sPhysPrefix + "_AMPL_sum_thr";
//   HistoPhysSum=new TH1F(name.c_str(),name.c_str(), ECAL0_MAX_AMPLITUDE/2, 0, ECAL0_MAX_AMPLITUDE);  
//   HistoPhysSum->SetFillColor(8);
//   HistoPhysSum->SetTitle("Summary amplitude distribution from physics events [Thr: 10 ch]");
//   HistoPhysSum->GetXaxis()->SetTitle("Amplitude, adc channel");
//   HistoPhysSum->GetYaxis()->SetTitle("Number of counts, N");
//   HistoPhysSum->GetXaxis()->SetRangeUser(0,100);
//   AddHistogram(HistoPhysSum);   
  
  // --------------------------------- led histograms ---------------------------------------------- //  
  
  
  
  // =================================================================== //
  
  name = sLedPrefix + "_abs_ch";
  HistoLedAbsCh = new TH1F_Ref(name.c_str(), name.c_str(), ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5, fRateCounter);
  HistoLedAbsCh->SetTitle("#lower[0.1]{#splitline{#scale[0.8]{             LED response}}{#lower[0.5]{#scale[0.5]{#color[12]{Number of module = (absolute channel number/9ch)+1}}}}}");
  HistoLedAbsCh->GetXaxis()->SetTitle("Absolute channel number");
  HistoLedAbsCh->GetYaxis()->SetTitle("Number of counts, N");
  HistoLedAbsCh->SetFillColor(19);
  ((TH1F_Ref*)HistoLedAbsCh)->SetReference(fReferenceDirectory);
  AddHistogram(HistoLedAbsCh); 
  
  for (int s=0; s<4; s++)
  {
    sprintf(cHistoName,"%s_SID_%d",name.c_str(),632+s);
    HistoLedAbsChSrcID[s] = new TH1F_Ref(cHistoName, cHistoName, ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5, fRateCounter);
    sprintf(cHistoName,"#lower[0.1]{#splitline{#scale[0.8]{             LED response [SrcID: %d]}}{#lower[0.5]{#scale[0.5]{#color[12]{Number of module = (absolute channel number/9ch)+1}}}}}",632+s);
    HistoLedAbsChSrcID[s]->SetTitle(cHistoName);
    HistoLedAbsChSrcID[s]->GetXaxis()->SetTitle("Absolute channel number");
    HistoLedAbsChSrcID[s]->GetYaxis()->SetTitle("Number of counts, N");
    HistoLedAbsChSrcID[s]->SetFillColor(19);
    ((TH1F_Ref*)HistoLedAbsChSrcID[s])->SetReference(fReferenceDirectory);
    AddHistogram(HistoLedAbsChSrcID[s]);
  }
  HistoLedAbsChSrcID[0]->GetXaxis()->SetRangeUser(0,441+1);
  HistoLedAbsChSrcID[1]->GetXaxis()->SetRangeUser(441,873+1);
  HistoLedAbsChSrcID[2]->GetXaxis()->SetRangeUser(873,1314+1);
  HistoLedAbsChSrcID[3]->GetXaxis()->SetRangeUser(1314,1746+1);
  
  // =================================================================== //
  
  name = sLedPrefix + "_AMPL_MAP";
  HistoLedMap = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoLedMap->SetOption("Col");
  HistoLedMap->SetMarkerSize(0.5);
  HistoLedMap->SetTitle("LED amplitude map [adc channel]");
  HistoLedMap->GetXaxis()->SetTitle("X");
  HistoLedMap->GetYaxis()->SetTitle("Y");
  HistoLedMap->GetZaxis()->SetTitle("Amplitude, adc channel");
  AddHistogram(HistoLedMap);
  
  name = sLedPrefix + "_AMPL_sum";
  HistoLedSum = new TH1F_Ref(name.c_str(),name.c_str(), ECAL0_MAX_AMPLITUDE/4, 0, ECAL0_MAX_AMPLITUDE*2,fRateCounter);
  HistoLedSum->SetFillColor(30);
  HistoLedSum->SetTitle("Summary LED amplitude distribution");
  HistoLedSum->GetXaxis()->SetTitle("Amplitude, adc channel");
  HistoLedSum->GetYaxis()->SetTitle("Number of counts, N");
  AddHistogram(HistoLedSum);
  ((TH1F_Ref*)HistoLedSum)->SetReference(fReferenceDirectory);
  
  name = sLedPrefix + "_PROF_sum";
  HistoLedSampleSum = new TH2F(name.c_str(),name.c_str(),32,0,32,ECAL0_MAX_AMPLITUDE/4,0,ECAL0_MAX_AMPLITUDE);
  HistoLedSampleSum->SetTitle("Summary LED signal profile");
  HistoLedSampleSum->GetXaxis()->SetTitle("Number of the adc sample");
  HistoLedSampleSum->GetYaxis()->SetTitle("Amplitude, adc channel");
  AddHistogram(HistoLedSampleSum);
  
  name = sLedPrefix + "_TIME_sum";
  HistoLedTimeSum = new TH1F_Ref(name.c_str(),name.c_str(),32*ECAL0_SAMPLE_TIME,0,32*ECAL0_SAMPLE_TIME,fRateCounter);
  HistoLedTimeSum->SetFillColor(86);
  HistoLedTimeSum->SetTitle("Summary time distribution of LED signals");
  HistoLedTimeSum->GetXaxis()->SetTitle("Time, ns");
  HistoLedTimeSum->GetYaxis()->SetTitle("Number of counts, N");
  AddHistogram(HistoLedTimeSum);
  ((TH1F_Ref*)HistoLedTimeSum)->SetReference(fReferenceDirectory);
  
  name = sLedPrefix + "_TIME_sum_samp";
  HistoLedTimeSumSam = new TH1F_Ref(name.c_str(),name.c_str(),320,0,32,fRateCounter);
  HistoLedTimeSumSam->SetTitle("Summary time distribution of LED signals in samples");
  HistoLedTimeSumSam->GetXaxis()->SetTitle("Number of the adc sample, 1 sample = 12.5 ns");
  HistoLedTimeSumSam->GetYaxis()->SetTitle("Number of counts, N");
  AddHistogram(HistoLedTimeSumSam);
  ((TH1F_Ref*)HistoLedTimeSumSam)->SetReference(fReferenceDirectory);
  
  name = sLedPrefix + "_TIME_abs_ch";
  HistoLedTimeAbsCh = new TH2F(name.c_str(),name.c_str(),ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5,32*ECAL0_SAMPLE_TIME,0,32*ECAL0_SAMPLE_TIME);
  HistoLedTimeAbsCh->SetOption("boxcol");
  HistoLedTimeAbsCh->SetTitle("Time profiles of LED signals");
  HistoLedTimeAbsCh->GetXaxis()->SetTitle("Absolute channel number");
  HistoLedTimeAbsCh->GetYaxis()->SetTitle("Time, ns");
  HistoLedTimeAbsCh->SetMinimum(0.0);
  AddHistogram(HistoLedTimeAbsCh);
  
  name = sLedPrefix + "_TIME_MAP";
  HistoLedTimeMap = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoLedTimeMap->SetOption("Col");
  HistoLedTimeMap->SetMarkerSize(0.5);
  HistoLedTimeMap->SetTitle("Time map of LED signals [ns]");
  HistoLedTimeMap->GetXaxis()->SetTitle("X");
  HistoLedTimeMap->GetYaxis()->SetTitle("Y");
  HistoLedTimeMap->GetZaxis()->SetTitle("Time, ns");
  AddHistogram(HistoLedTimeMap);    
  
  name = sLedPrefix + "_AMPL_abs_ch";
  HistoLedAmpAbsCh = new TH1F/*_Ref*/(name.c_str(), name.c_str(), ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5/*, fRateCounter*/);
  HistoLedAmpAbsCh->SetTitle("#lower[0.1]{#splitline{#scale[0.8]{             LED Amplitudes}}{#lower[0.5]{#scale[0.5]{#color[12]{Number of module = (absolute channel number/9ch)+1}}}}}");
  HistoLedAmpAbsCh->GetXaxis()->SetTitle("Absolute channel number");
  HistoLedAmpAbsCh->GetYaxis()->SetTitle("Amplitude, adc channel");
  HistoLedAmpAbsCh->SetFillColor(19);
  // ((TH1F_Ref*)HistoLedAmpAbsCh)->SetReference(fReferenceDirectory);
  AddHistogram(HistoLedAmpAbsCh); 
  
  for (int s=0; s<4; s++)
  {
    sprintf(cHistoName,"%s_SID_%d",name.c_str(),632+s);
    HistoLedAmpAbsChSrcID[s] = new TH1F/*_Ref*/(cHistoName, cHistoName, ECAL0_MOD*ECAL0_CH+10, 0+0.5, ECAL0_MOD*ECAL0_CH+10+0.5/*, fRateCounter*/);
    sprintf(cHistoName,"#lower[0.1]{#splitline{#scale[0.8]{             LED Amplitudes [SrcID: %d]}}{#lower[0.5]{#scale[0.5]{#color[12]{Number of module = (absolute channel number/9ch)+1}}}}}",632+s);
    HistoLedAmpAbsChSrcID[s]->SetTitle(cHistoName);
    HistoLedAmpAbsChSrcID[s]->GetXaxis()->SetTitle("Absolute channel number");
    HistoLedAmpAbsChSrcID[s]->GetYaxis()->SetTitle("Amplitude, adc channel");
    HistoLedAmpAbsChSrcID[s]->SetFillColor(19);
    // ((TH1F_Ref*)HistoLedAmpAbsChSrcID[s])->SetReference(fReferenceDirectory);
    AddHistogram(HistoLedAmpAbsChSrcID[s]);
  }
  HistoLedAmpAbsChSrcID[0]->GetXaxis()->SetRangeUser(0,441+1);
  HistoLedAmpAbsChSrcID[1]->GetXaxis()->SetRangeUser(441,873+1);
  HistoLedAmpAbsChSrcID[2]->GetXaxis()->SetRangeUser(873,1314+1);
  HistoLedAmpAbsChSrcID[3]->GetXaxis()->SetRangeUser(1314,1746+1);
  // --------------------------------- ------------- ---------------------------------------------- //  
  
  std::string adrname = /*fName +*/ "__adr";
  fHch=new TH1F_Ref( adrname.c_str(),adrname.c_str(), fNrows*fNcols, 0,
                     fNrows*fNcols, fRateCounter);
  ((TH1F_Ref*)fHch)->SetReference(fReferenceDirectory);
  fHch->SetFillColor(3);
  AddHistogram(fHch);
   
  std::string aadrcut = /*fName +*/ "__adrVSampCut";
  fHchac = new TH2F(aadrcut.c_str(), aadrcut.c_str(), 
		    fNrows*fNcols, 0, fNrows*fNcols,
		    fVamp->GetNbins(), fVamp->GetMin(), 
		    fVamp->GetMax());                    // ECAL0_2016.xml
                    fHchac->SetOption("boxcol");
  AddHistogram(fHchac);
  
  // 7    
  std::string ampmaxname = /*fName +*/ "__maxAmp";
  fHma=new TH1F_Ref(ampmaxname.c_str(),ampmaxname.c_str(),ECAL0_MAX_AMPLITUDE, 0, ECAL0_MAX_AMPLITUDE, fRateCounter);
  // fVamp->GetNbins(), fVamp->GetMin(),
  // fVamp->GetMax(), fRateCounter);
  ((TH1F_Ref*)fHma)->SetReference(fReferenceDirectory);
  fHma->SetFillColor(5);
  AddHistogram(fHma);
    
  // 9
  std::string xyacut = /*fName +*/ "__xVSyAcut";
  fHxyac = new TH2F(xyacut.c_str(), xyacut.c_str(),fNcols, 0, fNcols, fNrows, 0, fNrows);
  // fHxyac->SetOption("boxcolz");
  fHxyac->SetOption("boxcol");
  fHxyac->SetFillColor(4);
  AddHistogram(fHxyac);
  
  // 10 
  std::string xynoise = /*fName +*/ "__xVSynoise";
  fHxyn = new TH2F(xynoise.c_str(), xynoise.c_str(),fNcols, 0, fNcols, fNrows, 0, fNrows);
  //  fHxyn->SetOption("boxcol");
  //  fHxyn->SetOption("boxcolz");
  fHxyn->SetOption("boxcol");
  AddHistogram(fHxyn);  
  
  name = /*fName + */"__led_adr";
  fHchampled = new TH2F(name.c_str(),name.c_str(),fNrows*fNcols,0,fNrows*fNcols,ECAL0_MAX_AMPLITUDE/4,0,ECAL0_MAX_AMPLITUDE*2);
  //  Int_t ci;   // for color index setting
  //  ci = TColor::GetColor("#c82dd2");
  //   fHchampled->SetMarkerColor(ci);
  //   fHchampled->SetMarkerColor(1);
  //   fHchampled->SetMarkerStyle(20);
  //   fHchampled->SetMarkerSize(0.4);
  fHchampled->SetOption("boxcol");
  fHchampled->SetMinimum(0.0);
  AddHistogram(fHchampled);
    
  //------------------------------------------------------------------------------ //

  name = /*fName +*/ "__bad";
  fBad = new TH1F(name.c_str(),name.c_str(),ECAL0_MAX_AMPLITUDE/2,0,ECAL0_MAX_AMPLITUDE);
  AddHistogram(fBad);
    
  name = /*fName +*/ "__good";
  fGood = new TH1F(name.c_str(),name.c_str(),ECAL0_MAX_AMPLITUDE/2,0,ECAL0_MAX_AMPLITUDE);
  AddHistogram(fGood);

  //------------------------------------------------------------------------------ //  
  
  name = sLedPrefix + "_AMPL_MAP_only@CERN";
  HistoLedMapText = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoLedMapText->SetOption("textCol");
  HistoLedMapText->SetMarkerSize(0.5);
  HistoLedMapText->SetTitle("LED amplitude map [adc channel]");
  HistoLedMapText->GetXaxis()->SetTitle("X");
  HistoLedMapText->GetYaxis()->SetTitle("Y");
  HistoLedMapText->GetZaxis()->SetTitle("Amplitude, adc channel");
  AddHistogram(HistoLedMapText);
  
  name = sLedPrefix + "_TIME_MAP_only@CERN";
  HistoLedTimeMapText = new TH2F(name.c_str(),name.c_str(),fNcols,0,fNcols,fNrows,0,fNrows);
  HistoLedTimeMapText->SetOption("textCol");
  HistoLedTimeMapText->SetMarkerSize(0.5);
  HistoLedTimeMapText->SetTitle("Time map of LED signals [ns]");
  HistoLedTimeMapText->GetXaxis()->SetTitle("X");
  HistoLedTimeMapText->GetYaxis()->SetTitle("Y");
  HistoLedTimeMapText->GetZaxis()->SetTitle("Time, ns");
  AddHistogram(HistoLedTimeMapText);    

  AddHistogram(fHhit);
  AddHistogram(fHa);
  //------------------------------------------------------------------------------ //
  
  
  for(int x=0; x<ECAL0_XCELLS; x++) 
  {
    for(int y=0; y<ECAL0_YCELLS; y++) 
    {
      iHitSum_thr1_ecal0[x][y] = 0;
      iHitSum_thr2_ecal0[x][y] = 0;
      fAmpSum_ecal0[x][y] = 0;
      iAllHitSum_ecal0[x][y] = 0;
      // sprintf(cell_name,"ped_X_%d_Y_%d",x,y);
      // name  = fName + cell_name;
      // title = fName + cell_name;
      // fCH_ped[x][y] = new TH1F(name.c_str(), title.c_str(),ECAL0_MAX_AMPLITUDE,0,ECAL0_MAX_AMPLITUDE);
      // AddHistogram(fCH_ped[x][y]);
    }
  }
  

  if (fExpertHistos) 
  {    
    for( int x = 0; x < fNcols; x++) 
    {
      for( int y = 0; y < fNrows; y++) 
      {
	int mod=0,cell=0;
      
	if(strncmp(GetName(),"EC00P1__",8)== 0)
	{
		if (y <=  32  && y >= 18 && x <=   35 && x >= 15) continue; // ECAL0 window
		
		if (y >=0  && y <=  14 && x >=  0 && x <=  2) continue; //
		if (y >=0  && y <=  11 && x >=  0 && x <=  5) continue; //
		if (y >=0  && y <=   8 && x >=  0 && x <=  8) continue; //
		if (y >=0  && y <=   5 && x >=  0 && x <= 11) continue; //
		if (y >=0  && y <=   2 && x >=  0 && x <= 14) continue; //

		if (y >=0  && y <=   2 && x >= 36) continue; //
		if (y >=3  && y <=   5 && x >= 39) continue; //
		if (y >=6  && y <=   8 && x >= 42) continue; //
		if (y >=9  && y <=  11 && x>=  45) continue; //
		if (y >=11 && y <=  14 && x>=  48) continue; //

		if (y >=36  && x >= 48) continue; //
		if (y >=39  && x >= 45) continue; //
		if (y >=42  && x >= 42) continue; //
		if (y >=45  && x>=  39) continue; //
		if (y >=48  && x>=  36) continue; //
	
		if (y >=36 && x >=  0 && x <=  2) continue; //
		if (y >=39 && x >=  0 && x <=  5) continue; //
		if (y >=42 && x >=  0 && x <=  8) continue; //
		if (y >=45 && x >=  0 && x <= 11) continue; //
		if (y >=48 && x >=  0 && x <= 14) continue; //	
	}
	    
	if( (x >= 0 && x < 51) && (y >= 0 && y < 51) ) 
	{
	  mod    = iModArr[x][y];
	  cell   = iChArr[x][y];
	} 
	
	if(mod==1000 && cell==1000)
	{  // decoding file was not read    
	  //----------------------------------------------------------- 
	  sprintf(cell_name,"Time_X_%d_Y_%d",x,y);
	  name  = fName + cell_name;
	  title = fName + cell_name;
	  fCH_time[x][y] = new TH1F(name.c_str(), title.c_str(),32,0,32);
	  AddHistogram(fCH_time[x][y]);
	  //-----------------------------------------------------------
	  sprintf(cell_name,"Ampl_X_%d_Y_%d",x,y);
	  name  = fName + cell_name;
	  title = fName + cell_name;
	  fCH_amp[x][y] = new TH1F(name.c_str(), title.c_str(),ECAL0_MAX_AMPLITUDE,0,ECAL0_MAX_AMPLITUDE);
	  AddHistogram(fCH_amp[x][y]);
	  //-----------------------------------------------------------
	  sprintf(cell_name,"Samp_X_%d_Y_%d",x,y);
	  name  = fName + cell_name;
	  title = fName + cell_name;
	  fSProfile[x][y] = new TH2F(name.c_str(),title.c_str(),32,0,32,ECAL0_MAX_AMPLITUDE/4,0,ECAL0_MAX_AMPLITUDE);
	  fSProfile[x][y]->SetMinimum(0.0);  
	  AddHistogram(fSProfile[x][y]);
	  //-------------------------------------------------
	} 
	else 
	{
	  //sprintf(cell_name,"Time_x%d_y%d_M%dCh%d",x,y,mod,cell);
	  sprintf(cell_name,"Time_x%d_y%d",x,y);
	  //name  = fName + cell_name;
	  //title = fName + cell_name;
	  name  =  cell_name;
	  title =  cell_name;

	  fCH_time[x][y] = new TH1F(name.c_str(), title.c_str(),320,0,32);
	  AddHistogram(fCH_time[x][y]);
	  //-----------------------------------------------------------
	  //sprintf(cell_name,"Ampl_x%d_y%d_M%dCh%d",x,y,mod,cell);
	  sprintf(cell_name,"Ampl_x%d_y%d",x,y);
	  //name  = fName + cell_name;
	  //title = fName + cell_name;
	  name  =  cell_name;
	  title =  cell_name;

	  fCH_amp[x][y] = new TH1F(name.c_str(), title.c_str(),ECAL0_MAX_AMPLITUDE,0,ECAL0_MAX_AMPLITUDE);
	  AddHistogram(fCH_amp[x][y]);
	  //-----------------------------------------------------------
	  //sprintf(cell_name,"Samp_x%d_y%d_M%dCh%d",x,y,mod,cell);
	  sprintf(cell_name,"Samp_x%d_y%d",x,y);
	  //name  = fName + cell_name;
	  //title = fName + cell_name;
	  name  =  cell_name;
	  title =  cell_name; 
	  fSProfile[x][y] = new TH2F(name.c_str(),title.c_str(),32,0,32,ECAL0_MAX_AMPLITUDE/4,0,ECAL0_MAX_AMPLITUDE);
	  fSProfile[x][y]->SetMinimum(0.0);  
	  AddHistogram(fSProfile[x][y]);
	}
      }
    }   
  } // expert 
} // end void P1aneECAL0::Init(TTree* tree)



//-------------------------------------------------------
//-------------------------------------------------------

void PlaneECAL0::EndEvent(const CS::DaqEvent &event) 
{
  // Stat_t bb1;                         
  // Stat_t ee1;                         
  // Stat_t limit; 
  static unsigned int suiEventNum = 0;

  // cout <<"NumHits: "<< fNhits << endl;
  if(fNhits==0) return;
  // if(fNhits>1) return;	    

  if (thr_flag) MYLOCK();
  
  if(strncmp(GetName(),"EC00P1__",8)== 0) {fNrows = 50; fNcols=50;}
  if(strncmp(GetName(),"EC00FEM_",8)== 0) {fNrows = 2; fNcols=2;}
  if(strncmp(GetName(),"EC00SUM_",8)== 0) {fNrows = 2; fNcols=2;}

  const CS::DaqEvent::Header head = event.GetHeader();
  const CS::DaqEvent::EventType eventType = head.GetEventType();

  // const CS::uint32 Atr = head.GetTypeAttributes()[0]&0xf8000000;
  // if(eventType == 8 && Atr==0x68000000) 
  // ----------------- calibration event -----------------
  // if(1)//eventType == 8 && Atr==0x68000000)  

  if(eventType == 8) // LEDs = calibration event ------
  {    
    suiEventNum++;
    if(fNhits==0) return;
    // if(strncmp(GetName(),"EC00P1__",8)== 0) 
    {
      for (int i=0; i<fNhits; i++) 
      {
	int row = fRow[i];    // y
	int col = fCol[i];    // x
	int mod    = 0;    
	int ch     = 0;
	int iAbsCh = -1;
	if( (col >= 0 && col < 51) && (row >= 0 && row < 51) ) 
	{
	  mod    = iModArr[col][row];
	  ch     = iChArr[col][row];
	  iAbsCh = iAbsChArr[mod-1][ch-1];
	} 
	
	// if (row<=32  && row >=18 && col<=35 && col>=15) continue; // ECAL0 window 
	// printf("row = %d, col = %d\n",row,col); 
	// int amp=fAmp[i];

        double amp = (double)fAmp[i]/100;
        int    adr = fRow[i]+fCol[i]*fNrows;        

	if(fVrow->Test(row) && fVcol->Test(col) && fVamp->Test(amp))
	{
	  if(amp > 1) 
	  {	   
	  //cout<<"ECAL0 LED --> x = "<<col<<"  y = "<<row<<"  led = "<<ECAL0_Led[col][row]<<endl; 	  
	  // ----------------- LED filling ------------------------------------------------------ //

	  fHchampled->Fill(adr,amp); // original LED values
	  HistoLedSum->Fill(amp);	  
	  	  
	  if (iAbsCh != 0) 
	  {
	    HistoLedAbsCh->Fill(iAbsCh,amp);
	    if (iAbsCh > 0    && iAbsCh <= 441 ) HistoLedAbsChSrcID[0]->Fill(iAbsCh,amp);	// SrcID: 632
	    if (iAbsCh > 441  && iAbsCh <= 873 ) HistoLedAbsChSrcID[1]->Fill(iAbsCh,amp);	// SrcID: 633
	    if (iAbsCh > 873  && iAbsCh <= 1314) HistoLedAbsChSrcID[2]->Fill(iAbsCh,amp);	// SrcID: 634
	    if (iAbsCh > 1314 && iAbsCh <= 1746) HistoLedAbsChSrcID[3]->Fill(iAbsCh,amp);	// SrcID: 635
	      
	    HistoLedAmpAbsCh->SetBinContent(iAbsCh,amp);
	    if (iAbsCh > 0    && iAbsCh <= 441 ) HistoLedAmpAbsChSrcID[0]->SetBinContent(iAbsCh,amp);	// SrcID: 632
	    if (iAbsCh > 441  && iAbsCh <= 873 ) HistoLedAmpAbsChSrcID[1]->SetBinContent(iAbsCh,amp);	// SrcID: 633
	    if (iAbsCh > 873  && iAbsCh <= 1314) HistoLedAmpAbsChSrcID[2]->SetBinContent(iAbsCh,amp);	// SrcID: 634
	    if (iAbsCh > 1314 && iAbsCh <= 1746) HistoLedAmpAbsChSrcID[3]->SetBinContent(iAbsCh,amp);	// SrcID: 635
	  }	  
	  	  
	  // fProf->Fill(adr,amp);
	  if (amp>6) HistoLedMap->SetBinContent(col+1,row+1,int(amp));
	  if (amp>6) HistoLedMapText->SetBinContent(col+1,row+1,int(amp));
	  //-------------------------------------------------
	  // bb1 = fProf->GetBinContent(adr);                           
	  // ee1 = fProf->GetBinError(adr);                           
	  // ee1 = fProf->GetEntries()/476.;                           
	  } // amp>1
	}//if(fVrow->Test(row) && fVco
      }//for (int i=0; i<fNhits; i++
    }//if(strncmp(GetName(),"HC01P1__",8)== 0)               
  } // if(eventType == 8 && Atr==0x68000000) // LEDs = calibration event -----------------
  else 
  {
    //  int adrmax=0;
    int hcut = 0;
    int sum  = 0; 
    int sumcut = 0; 
    int ampmax = 0; 
    int xampmax = -1;
    int yampmax = -1;
    //-------------------------------- phys events -------------------------------------------

    for (int i=0; i<fNhits; i++) 
    {
      int row    = fRow[i]; 
      int col    = fCol[i];
      int mod    = 0;    
      int ch     = 0;
      int iAbsCh = -1;
      if( (col >= 0 && col < 51) && (row >= 0 && row < 51) ) 
      {
	mod    = iModArr[col][row];
	ch     = iChArr[col][row];
	iAbsCh = iAbsChArr[mod-1][ch-1];
      }    
      double amp=(double)fAmp[i]/100;

      if(fVrow->Test(row) && fVcol->Test(col) && fVamp->Test(amp)) 
      {
	  int adr=fRow[i]+fCol[i]*fNrows;
	  if (amp > ampmax) 
	  {
	    ampmax = amp;  
	    xampmax = col; 
	    yampmax = row;
	  }
	  
	  // if(fNhits>ECAL0_MAX_HIT) continue;

	  if (amp > fAmpCut) 
	  {
	    hcut++;     
	    sumcut+=amp;
	    fHchac->Fill(adr,amp);
	    fHxyac->Fill(col,row,amp);
	    fHch->Fill(adr);
	    fAmpSum_ecal0[col][row] += amp;
	    iHitSum_thr1_ecal0[col][row]++;		// sum of hists under threshold
	    //printf("X%2d Y%2d - A: %8.2f Hits: %3d Sum: %8.2f AveA: %8.2f\n",col,row,amp,iHitSum_thr1_ecal0[col][row],fAmpSum_ecal0[col][row],fAmpSum_ecal0[col][row]/iHitSum_ecal0[col][row]);
	    
	    if (iAbsCh != 0) HistoPhysAbsCh->Fill(iAbsCh);	   
	    if(fNhits<=2) fGood->Fill(amp); else fBad->Fill(amp) ;
	  }
	  sum += amp;
	  iAllHitSum_ecal0[col][row]++;
	  if (amp > 3) iHitSum_thr2_ecal0[col][row]++;
	  
	  //fHrc1->Fill(col,row);	  	  
	  if (iAbsCh != 0) fHavsadr->Fill(iAbsCh,amp); 
	  fVrow->Store(row);
	  fVcol->Store(col);
	  fVamp->Store(amp);

	  fNhitsKept++;
      }    
    }

    for(int x=0; x<ECAL0_XCELLS; x++) 
    {
      for(int y=0; y<ECAL0_YCELLS; y++) 
      {
	if (iHitSum_thr1_ecal0[x][y] != 0) fHrca->SetBinContent(x+1,y+1,int(fAmpSum_ecal0[x][y]/iHitSum_thr1_ecal0[x][y]));
	if (iAllHitSum_ecal0[x][y] != 0) fHrc1->SetBinContent(x+1,y+1,iAllHitSum_ecal0[x][y]);
	if (iHitSum_thr2_ecal0[x][y] != 0) HistoHitMapThr->SetBinContent(x+1,y+1,iHitSum_thr2_ecal0[x][y]);
      }
    }
    if (sum>0)    fHa->Fill(sum); 
    // if (sumcut>0) HistoPhysSum->Fill(sumcut);
    if (ampmax>0) fHma->Fill(ampmax);    
    fHhit->Fill(fNhitsKept);
    if (ampmax < fAmpCut) fHxyn->Fill(xampmax,yampmax);
  }
  
  if (thr_flag) MYUNLOCK();
}

//---------------------------------------------------------------------------------

void PlaneECAL0::StoreDigit(int col, int row, std::vector<float>& data) {

 int sadclen=data.size()-2;
 // cout<<"sadclen  = "<< sadclen<<endl;
  
 if(sadclen<1) return;
 
 double ped     = 0;
 double rms_ped = 0;
 int aaa;
 double amp=0.;
 int min_amp;
 int max_amp;
 int i_max;
 int i_min;
 int sadc_amp;
 double time_mean0;

double a_par,b_par,c_par,y_max;
double x1,x2,x3;
double y1,y2,y3;
int i,i1,i2,i3;
//char cell_m[100];

  int mod    = 0;    
  int ch     = 0;
  int iAbsCh = -1;
    

  if (fNhits < fNchan*fMAX_MULT) {
    fRow[fNhits]=row;
    fCol[fNhits]=col;
    int fSADCev[ECAL0_SAMPLES_NUMBER];
    int len=ECAL0_SAMPLES_NUMBER;
    if(sadclen<len){ len=sadclen;
    for(i=len;i<ECAL0_SAMPLES_NUMBER;i++) fSADCev[i]=0; }
    for(i=0;i<len;i++) {fSADCev[i]=(int)data[i+2];}
  //
  //------------------------ pedestalls  -------rms-------------------------------------    
  //
      ped=0;
      for(i=0;i<ECAL0_PED_INTERVAL;i++) ped+=fSADCev[i];  
      ped=ped/ECAL0_PED_INTERVAL;

  //fCH_ped[col][row]->Fill(ped);
  //rms_ped=fCH_ped[col][row]->GetRMS();

  //double mean_ped=fCH_ped[col][row]->GetMean();
  //double max_ped=fCH_ped[col][row]->GetMaximum();
  //double num_ped=fCH_ped[col][row]->GetEntries();

  if( (col >= 0 && col < 51) && (row >= 0 && row < 51) ) 
  {
      mod    = iModArr[col][row];
      ch     = iChArr[col][row];
      iAbsCh = iAbsChArr[mod-1][ch-1];
  }
  aaa=row+fNrows*col;
  HistoPedSum->Fill(ped);   					//  total pedestall filling
  // HistoPedAbsCh->Fill(aaa,ped);	 			// pedestall filling - previous declaration
  if (iAbsCh != 0) HistoPedAbsCh->Fill(iAbsCh,ped);		// pedestall filling

  // HistoPedMap->SetBinContent(col+1,row+1,mean_ped); 		// pedestall x-y filling
  HistoPedMap->SetBinContent(col+1,row+1,ped); 			// pedestall x-y filling
  HistoPedMap->SetBinContent(1,1,60);

  if (iAbsCh != 0) rms_ped = HistoPedAbsCh->ProjectionY("projectionY",iAbsCh,iAbsCh)->GetRMS();

  HistoPedRmsAbsCh->Fill(iAbsCh,rms_ped);			// RMS ped filling
  HistoPedRmsSum    ->Fill(rms_ped);

  HistoPedRmsMap_02->SetBinContent(col+1,row+1,rms_ped);
  if (rms_ped>5) HistoPedRmsMap_01  -> SetBinContent(col+1,row+1,7);
  else HistoPedRmsMap_01  -> SetBinContent(col+1,row+1,1);

  //if (num_ped>0) if ((rms_ped>7)&&(max_ped/num_ped<0.35)) HistoPedRmsMap_01->SetBinContent(col+1,row+1,10);
  //if ((rms_ped>7)&&(max_ped/num_ped<0.35)) HistoPedRmsMap_01->SetBinContent(col+1,row+1,10);
  //HistoPedRmsMap_01  -> SetBinContent(1,1,10);
  //
  //----------------------------- maximum & minimum samle search -------------
  //
  y_max=0;
  min_amp=10000;
  max_amp=-10000;
  i_max=0;
  i_min=0;
  for(i=ECAL0_PED_INTERVAL; i<len-ECAL0_PED_INTERVAL; i++) 
  {     
    sadc_amp=fSADCev[i]-ped;
    if(sadc_amp < min_amp )  {min_amp = sadc_amp;  i_min = i;}
    if(sadc_amp > max_amp )  {max_amp = sadc_amp;  i_max = i;}
  } 
  //
  //--------------------------- parabola to obtain time and amp -------------------------
  //
  // y=a*x**2 + b*x +c ;
  // x_max=-b/(2*a);
  // y_max=(4*a*c-b*b)/(4*a) ;

  i1=i_max-1; i2=i_max;  i3=i_max   + 2;

  x1=(double)i1; x2=(double)i2; x3=(double)i3;
  y1=(double)fSADCev[i1]-ped; 
  y2=(double)fSADCev[i2]-ped; 
  y3=(double)fSADCev[i3]-ped;

  a_par=(y3-(x3*(y2-y1)+x2*y1-x1*y2)/(x2-x1))/(x3*(x3-x1-x2)+x1*x2);
  b_par=a_par*(x1+x2)-(y2-y1)/(x2-x1);
  c_par=(x1*y2-x2*y1)/(x1-x2) + a_par*x2*x1 ;

  time_mean0=0;

  if(a_par != 0) {
  time_mean0=b_par/(2.*a_par);
  y_max=(4.0*a_par*c_par - b_par*b_par)/(4.0*a_par) ;
}

//----------------------------------------------------------------------------
  if(fabs(min_amp-max_amp)>ECAL0_THR_00) 
  { 
    if(time_mean0>5 && time_mean0<25)
    {
      double t1=0.,s1=0.;
      amp=y_max;
      //if(fabs(time_mean0-t1)<3.*s1)

      for(i=0; i<ECAL0_SAMPLES_NUMBER; i++)  HistoLedSampleSum->Fill(i,fSADCev[i]-ped);

      if(fabs(time_mean0-t1)<6.*s1) fGood->Fill(amp); else fBad->Fill(amp) ;

      HistoLedTimeSum->Fill(time_mean0*ECAL0_SAMPLE_TIME);
      HistoLedTimeSumSam->Fill(time_mean0);
      if (iAbsCh != 0) HistoLedTimeAbsCh->Fill(iAbsCh,time_mean0*ECAL0_SAMPLE_TIME);
      HistoLedTimeMap->SetBinContent(col+1,row+1,int(time_mean0*ECAL0_SAMPLE_TIME));
      HistoLedTimeMapText->SetBinContent(col+1,row+1,int(time_mean0*ECAL0_SAMPLE_TIME)); 

      t1=HistoLedTimeSumSam->GetMean(1); s1=HistoLedTimeSumSam->GetRMS(1);
    }
  }
//------------------------------------------------------------------------------	  

  //------------------------------expert----------------------------
  
  if (fExpertHistos && amp>fAmpCut) 
  { 
  fCH_time[col][row]->Fill(time_mean0);    // fill time_mean0

  double time_mean=fCH_time[col][row]->GetMean(); // new time arounn mean
  double time_sigm=fCH_time[col][row]->GetRMS(); // new time arounn mean

  if(fabs(time_mean0-time_mean)<6.*time_sigm) fCH_amp[col][row] ->Fill(amp); 

  for(i=0; i<sadclen; i++)  fSProfile[col][row]->Fill(i,fSADCev[i]-ped);  
	  
  } // end Expert histos  
  //-------------------------------------------------------

      fAmp[fNhits]=(int)(amp*100);
      fNhits++;

  }//if (fNhits <
}// end void  StoreDigit

//
//-----------------------------------------------------------------------------------------
//
void PlaneECAL0::StoreDigit(CS::Chip::Digit* digit) {
  std::vector<float> data=digit->GetNtupleData();
//  cout<<" data.size()   = "<<data.size()<<endl;
  if(data.size()<3) return;
  const CS::ChipSADC::Digit* sadcdig = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
//  cout<<"  sadcdig  = "<<sadcdig<<endl;
  if(sadcdig != NULL ) {
         int ix=sadcdig->GetX(); int iy=sadcdig->GetY();
         if(ix < 0||iy<0||ix>fNcols||iy>fNrows) return; 
         this->StoreDigit(ix,iy,data);
  }
}

// = Arseny Rybnikov = //
// to bound coordinates with numbers of the module and channel
bool PlaneECAL0::GetMap(const char *MapFileName)
{
    FILE *fdMapFile;
    fdMapFile = fopen(MapFileName,"rt");
    if (fdMapFile == NULL) return false;

    const int iLineSize = 256;

    char cSite[8];

    int iMod   = 0;
    int iModCh = 0;
    int iGes   = 0;
    int iPort  = 0;
    int iADC   = 0;
    int iADCch = 0;
    int iAbsCh = 0;
    int iX     = 0;
    int iY     = 0;

    for (int x=0; x<51; x++)
    {
      for (int y=0; y<51; y++)
      {
	  iModArr[x][y]   = 0;
	  iChArr[x][y]    = 0;	  
      }
    }
    for (int m=0; m<ECAL0_MOD; m++)
    {
      for (int ch=0; ch<ECAL0_CH; ch++)
      {	  
	  iAbsChArr[m][ch] = 0;
      }
    }

    char cLine[iLineSize];
    int  iCounter = 0;

    while (!feof(fdMapFile))
    {
       //iCounter++;
       if (fgets(cLine,iLineSize,fdMapFile) != NULL)
       {
            //printf("%s",cLine);
            if (strncmp(cLine,"Site",4) == 0 || strncmp(cLine,"\n",1) == 0) continue;
            sscanf(cLine,"%s %d %d %d %d %d %d %d %d %d",cSite, &iMod, &iModCh, &iGes, &iPort, &iADC, &iADCch, &iAbsCh, &iX, &iY);
            // printf("%s %d %d %d %d %d %d %d\n", cSite, iMod, iModCh, iGes, iPort, iADC, iADCch, iAbsCh);

	    //iXArr[iMod-1][iModCh-1] = iX;
	    //iYArr[iMod-1][iModCh-1] = iY;
	    iAbsChArr[iMod-1][iModCh-1] = iAbsCh;
	    //printf("Mod%d CH%d X%d Y%d AbsCh%d\n",iMod,iModCh,iXArr[iMod-1][iModCh-1],iYArr[iMod-1][iModCh-1],iAbsChArr[iMod-1][iModCh-1]);

            iModArr[iX][iY]    = iMod;
            iChArr[iX][iY]     = iModCh;
	    //iXArr[iGes][iPort][iADC][iADCch]      = iX;
	    //iYArr[iGes][iPort][iADC][iADCch]      = iY;
	    //iADCArr[iX][iY][0]                    = iGes;
	    //iADCArr[iX][iY][1]                    = iPort;
	    //iADCArr[iX][iY][2]                    = iADC;
	    //iADCArr[iX][iY][3]                    = iADCch;
            //sprintf(strSiteArr[iGes][iPort][iADC][iADCch],"%s", cSite);
            //printf("Site: %s Module: %d Channel: %d\n",strSiteArr[iGes][iPort][iADC][iADCch],iModArr[iGes][iPort][iADC][iADCch],iChArr[iGes][iPort][iADC][iADCch]);
       }
    }
    fclose(fdMapFile);

    return true;
}
// = Arseny Rybnikov = //




