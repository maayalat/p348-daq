#include "StartFrame.h"
#include "config.h"
#include "TColor.h"
#include <iostream>

const char* StartFrame::maptype[] = { "map files",   "*.xml",
				      "All files",   "*.*",
				       0,               0 };
const char* StartFrame::datatype[] = { "data files",   "*.dat",
				       "data files",   "*.raw",
				       "All files",   "*.*",
		 		       0,               0 };
const char* StartFrame::geomtype[] = { "data files",   "*.dat",
				       "All files",   "*.*",
				       0,               0 };


ClassImp(StartFrame)

namespace { // local routine
void usage(char* pgm_name, const char* err_string) {
  std::cerr << "\n\nWarning !! the arguments of the monitoring program have changed !!\n\n";
  if (err_string) std::cerr << err_string <<std::endl;
  std::cerr << "usage: " << gApplication->Argv(0) 
       << " [-map map_file] [-root root_file]"
       << " [-reference ref_root_file_name]"
       << " [-runtype < muon+ | muon- >]"
       << " [-geom detector_file]"
       << " [-group groups_file] [-cfg user_configuration_file]" 
       << " [-evttypes (event types to read, given with format: START_OF_RUN,START_OF_BURST,PHYSICS_EVENT,CALIBRATION_EVENT,...)]\n"
       << " [-workenv working environment string]"
       << " [data file or DATE source]\n";
  exit(2);
}
}


StartFrame::StartFrame(const TGWindow *p,MainFrame *mainf, UInt_t w, 
		       UInt_t h) 
  : TObject(), fMainframe(mainf) {
  fReferenceFile = "";
  fRootFile = "";
  fGroupFile = "/online/detector/monitor/groups.xml";
  //fGeomFile = "/online/detector/monitor/detectors.dat";
  fGeomFile = "";
  fMapFile = "/online/detector/maps";
  //fParamFile = "/online/detector/monitor/default_params";
  fParamFile = "";
  fConfigFile = "";
  std::string evttypes = "";
  std::string wkenv = "AFS";

  int nbarg = mainf->Argc();
  int pargc = 1;

  while (pargc < nbarg) {
    char *pargv, *pargvn;
    pargv =  mainf->Argv(pargc);
    pargvn = 0;
    if ((pargc + 1) < nbarg) pargvn =  mainf->Argv(pargc+1);

    if (pargv[0] == '-') {   // it is an option
      if (strcmp(pargv, "-map") == 0) {  // option to give Map file
        if (pargvn && (pargvn[0] != '-')) fMapFile = pargvn;
          else usage(mainf->Argv(0), "badly formed -m option");
        pargc+=2;
        continue;
      }
      if(strcmp(pargv,"-runtype") == 0) { // option to select reference file by type of the run
        if (pargvn && (pargvn[0] != '-')) MainFrame::RUN_TYPE = pargvn;
          else usage(mainf->Argv(0), "badly formed -runtype option");
        pargc+=2;	
        continue;
      }
      if (strcmp(pargv, "-geom") == 0) {  // option to give Geom file
        if (pargvn && (pargvn[0] != '-')) fGeomFile = pargvn;
          else usage(mainf->Argv(0), "badly formed -geom option");
        pargc+=2;
        continue;
      }
      if (strcmp(pargv, "-reference") == 0) {  // option to give reference file
        if (pargvn && (pargvn[0] != '-')) fReferenceFile = pargvn;
          else usage(mainf->Argv(0), "badly formed -reference option");
        pargc+=2;
        continue;
      }
      if (strcmp(pargv, "-root") == 0) {  // option to give root file
        if (pargvn)
	  if (pargvn[0] != '-') {
	    fRootFile = pargvn;
	    pargc+=2;
	  }
	  else usage(mainf->Argv(0), "badly formed -root option, root file beginning by -");
	else usage(mainf->Argv(0), "badly formed -root option");
        continue;
      }
      if (strcmp(pargv, "-group") == 0) {  // option to give groups file
        if (pargvn && (pargvn[0] != '-')) fGroupFile = pargvn;
          else usage(mainf->Argv(0), "badly formed -group option");
        pargc+=2;
        continue;
      }
      if (strcmp(pargv, "-cfg") == 0) {  // option to give user config file
        if (pargvn && (pargvn[0] != '-')) fConfigFile = pargvn;
          else usage(mainf->Argv(0), "badly formed -cfg option");
        pargc+=2;
        continue;
      }
      if (strcmp(pargv, "-evttypes") == 0) {  // event types to read
        if (pargvn && (pargvn[0] != '-')) evttypes = pargvn;
          else usage(mainf->Argv(0), "badly formed -evttypes option");
        pargc+=2;
        continue;
      }
      if (strcmp(pargv, "-workenv") == 0) {  // option to give working environment string
        if (pargvn && (pargvn[0] != '-')) wkenv = pargvn;
          else usage(mainf->Argv(0), "badly formed -workenv option");
        pargc+=2;
        continue;
      }
      if (strcmp(pargv, "--help") == 0) {  // option to request help
        usage(mainf->Argv(0), 0);
      }
      // unknown option found
      char stmp[200];
      sprintf(stmp, "unknown option %s", pargv);
      usage(mainf->Argv(0), stmp);
    }
    else {   // data file name
      fDataFile.push_back(pargv);
      pargc++;
      continue;
    }
  }
  
  if (fDataFile.empty()) fDataFile.push_back("@pcdmre01:");
  
//   cerr << "fMapFile " << fMapFile << " fRootFile " << fRootFile << " fGroupFile " << fGroupFile;
//   cerr << " fDataFile " << fDataFile <<endl;

  fMonitor=fMainframe->GetMonitor();
  if (evttypes != "") fMonitor->CheckEvtTypes(evttypes.c_str());
  fMonitor->SetWkEnvStr(wkenv.c_str());

  fMain = new TGTransientFrame(p, mainf->GetFrame(), w, h);
  fMain->Connect("CloseWindow()", "StartFrame", this, 
  		 "CloseWindow()");

  fL1 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandY | kLHintsExpandX, 0,0,0,0);
  fL2 = new TGLayoutHints(kLHintsExpandX | kLHintsBottom,
			  2, 2, 2, 2);
  fL3 = new TGLayoutHints(kLHintsBottom | kLHintsLeft,
			  2, 2, 2, 2);
  fL4 = new TGLayoutHints( kLHintsBottom | kLHintsRight,
			  2, 1, 1, 1);
  fL5 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0,0,0,0);
  
  // Upper frame (Monitored detectors & Tasks)
  fUpFrame = new TGCompositeFrame(fMain, 60, 20, kHorizontalFrame);

  // Check frame
  fCheckFrame = new TGGroupFrame(fUpFrame, "Monitored detectors", kHorizontalFrame);
  fCheckFrame->SetLayoutManager(new TGHorizontalLayout(fCheckFrame));
  
  fMonDetFrame = new TGVerticalFrame(fCheckFrame, 60, 40, kVerticalFrame);
  std::map<std::string,bool>& detintree=fMonitor->GetDetInTree();
  typedef std::map<std::string,bool>::iterator IM;
  int n=0;
  for(IM i=detintree.begin(); i!=detintree.end(); i++) {
    TGCheckButton* check = new TGCheckButton(fMonDetFrame, i->first.c_str(), n);
    check->SetOn(i->second);
    check->Connect("Toggled(Bool_t)", "StartFrame", this, "CheckedDet(Bool_t)");
    fMonDetFrame->AddFrame(check);
    fCheckButton.push_back(check);
    n++;
  }
  fCheckFrame->AddFrame(fMonDetFrame,fL5);
  fMonDetFrame->Resize(75, (unsigned int) n*fCheckButton[0]->GetDefaultHeight());

  fAllNoneFrame = new TGVerticalFrame(fCheckFrame, 60, 20, kFixedWidth);

  //fDefButton = new TGTextButton(fAllNoneFrame," &Default ",1);
  //fDefButton->Connect("Clicked()","StartFrame",this,"DefClicked()");
  //fAllNoneFrame->AddFrame(fDefButton,fL2);  

  fAllButton = new TGTextButton(fAllNoneFrame," &All ",1);
  fAllButton->Connect("Clicked()","StartFrame",this,"AllClicked()");
  fAllNoneFrame->AddFrame(fAllButton,fL2);  

  fNoneButton = new TGTextButton(fAllNoneFrame,"&None",1);
  fNoneButton->Connect("Clicked()","StartFrame",this,"NoneClicked()");
  fAllNoneFrame->AddFrame(fNoneButton,fL2);
  
  fCheckFrame->AddFrame(fAllNoneFrame,fL4);
  fAllNoneFrame->Resize(75, (unsigned int) 1.5*fAllButton->GetDefaultHeight());


  // Tasks frame
  fTaskFrame = new TGGroupFrame(fUpFrame, "Tasks", kVerticalFrame); 
  fTaskFrame->SetLayoutManager(new TGVerticalLayout(fTaskFrame));

  fCheckTree = new TGCheckButton(fTaskFrame,"Fill Tree");
  fCheckTree->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedTree(Bool_t)");
  fCheckTree->SetToolTipText("fill a tree with the Planes variables in the root file");
  fTaskFrame->AddFrame(fCheckTree);

  fCheckText = new TGCheckButton(fTaskFrame,"Text Output");
  fCheckText->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedText(Bool_t)");
  fCheckText->SetToolTipText("create at the end a text file with some numerical values computed from histograms");
  fTaskFrame->AddFrame(fCheckText);

  fCheckTrack = new TGCheckButton(fTaskFrame,"Tracking");
  fCheckTrack->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedTrack(Bool_t)");
  fTaskFrame->AddFrame(fCheckTrack);
  
  fCheckCluster = new TGCheckButton(fTaskFrame,"Clustering");
  fCheckCluster->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedCluster(Bool_t)");
  fCheckCluster->SetOn(kTRUE, kTRUE);
  fTaskFrame->AddFrame(fCheckCluster);
    
  fCheckRef = new TGCheckButton(fTaskFrame,"Show ref. plots");
  fCheckRef->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedRef(Bool_t)");
  fCheckRef->SetToolTipText("show reference histos in red in some histograms");
  if(!fReferenceFile.empty())
    fCheckRef->SetOn(kTRUE, kTRUE);
  fTaskFrame->AddFrame(fCheckRef);

  fCheckCalib = new TGCheckButton(fTaskFrame,"Use Calibrations");
  fCheckCalib->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedCalib(Bool_t)");
  fCheckCalib->SetToolTipText("read and use the calibration data");
  fTaskFrame->AddFrame(fCheckCalib);

  fCheckReadFailedEvt = new TGCheckButton(fTaskFrame,"Read evt with decoding partly failed");
  fCheckReadFailedEvt->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedReadFailedEvt(Bool_t)");
  fCheckReadFailedEvt->SetToolTipText("this happens for example for some calibration events with no trigger time");
  fTaskFrame->AddFrame(fCheckReadFailedEvt);

  fCheckExpert = new TGCheckButton(fTaskFrame,"Activate expert histos");
  fCheckExpert->Connect("Toggled(Bool_t)","StartFrame",this,"CheckedExpert(Bool_t)");
  fCheckExpert->SetToolTipText("activate additional histograms normal peoples do not care about");
  fCheckExpert->SetOn(kTRUE, kTRUE);
  fTaskFrame->AddFrame(fCheckExpert);


  // Input frame
  fInputFrame = new TGGroupFrame(fMain,"Input files", kVerticalFrame);
  fInputFrame->SetLayoutManager(new TGVerticalLayout(fInputFrame));
  
  fRawFrame = new TGCompositeFrame(fInputFrame, 60, 20, kHorizontalFrame);
  string fname;
  if (fDataFile.size() == 1) fname = fDataFile[0];
  if (fDataFile.size() > 1) fname = "<multiple files>";
  fRawLabel = new TGLabel(fRawFrame,new TGHotString(fname.c_str()));
  fRawButton = new TGTextButton(fRawFrame, "&Data source:", 100);
  fRawButton->Connect("Clicked()","StartFrame",this,"HandleButtons()");
  fRawButton->SetToolTipText("Open raw data file");
  fRawFrame->AddFrame(fRawButton,fL3);
  fRawFrame->AddFrame(fRawLabel,fL3);

  fMapFrame = new TGCompositeFrame(fInputFrame, 60, 20, kHorizontalFrame);
  fMapLabel = new TGLabel(fMapFrame,new TGHotString(fMapFile.c_str()));  
  fMapButton = new TGTextButton(fMapFrame, "&Map file :", 101);
  fMapButton->SetToolTipText("Open mapping file");
  fMapButton->Connect("Clicked()","StartFrame",this,"HandleButtons()");
  fMapFrame->AddFrame(fMapButton,fL3);
  fMapFrame->AddFrame(fMapLabel,fL3);

  fGeomFrame = new TGCompositeFrame(fInputFrame, 60, 20, kHorizontalFrame);
  fGeomLabel = new TGLabel(fGeomFrame,new TGHotString(fGeomFile.c_str()));  
  fGeomButton = new TGTextButton(fGeomFrame, "&Geom file :", 102);
  fGeomButton->SetToolTipText("Detector geometrical properties data (for reconstruction)");
  fGeomButton->Connect("Clicked()","StartFrame",this,"HandleButtons()");
  fGeomFrame->AddFrame(fGeomButton,fL3);
  fGeomFrame->AddFrame(fGeomLabel,fL3);

  fInputFrame->AddFrame(fRawFrame);
  fInputFrame->AddFrame(fMapFrame);
  fInputFrame->AddFrame(fGeomFrame);

  // down frame
  fDownFrame = new TGCompositeFrame(fMain, 60, 20, kHorizontalFrame);  

  // decoding Frame
  fDecodingFrame = new TGGroupFrame(fDownFrame,"Decoding", kVerticalFrame);
  
  fSetupButton = new TGTextButton(fDecodingFrame, "&Setup event trigger type filter", 103);
  fSetupButton->Connect("Clicked()","StartFrame",this,"HandleButtons()");
  
  fDecodingFrameDown = new TGCompositeFrame(fDecodingFrame,60,20, kHorizontalFrame);
  fEvtMax = new TGTextEntry(fDecodingFrameDown,new TGTextBuffer(10));
  fEvtLabel = new TGLabel(fDecodingFrameDown, "Number of events :");
  fDecodingFrameDown->AddFrame(fEvtLabel,fL3);
  fDecodingFrameDown->AddFrame(fEvtMax,fL2);

  fMinSpacingEntry = new TGTextEntry(fDecodingFrameDown,new TGTextBuffer(10));
  fMinSpacingLabel = new TGLabel(fDecodingFrameDown, "Min spacing of events :");
  fDecodingFrameDown->AddFrame(fMinSpacingLabel,fL3);
  fDecodingFrameDown->AddFrame(fMinSpacingEntry,fL2);

  // Entry to set reference run number
  fDecodingFrameDown2 = new TGCompositeFrame(fDecodingFrame,60,20, kHorizontalFrame);
  fRefEntry = new TGTextEntry(fDecodingFrameDown2);
  if(!fReferenceFile.empty())
    fRefEntry->SetText(fReferenceFile.c_str());
  fRefLabel = new TGLabel(fDecodingFrameDown2, "Reference file path :");
  fDecodingFrameDown2->AddFrame(fRefLabel,fL3);
  fDecodingFrameDown2->AddFrame(fRefEntry,fL2);

  fDecodingFrame->AddFrame(fSetupButton,fL3);
  fDecodingFrame->AddFrame(fDecodingFrameDown,fL5);
  fDecodingFrame->AddFrame(fDecodingFrameDown2,fL5);

  // start button
  fStartButton = new TGTextButton(fDownFrame,"&START", 102);
  fStartButton->SetBackgroundColor(TColor::Number2Pixel(kGreen-9));
  fStartButton->ChangeOptions(fStartButton->GetOptions() | kFixedSize);
  fStartButton->Resize(100, 100);
  fStartButton->SetToolTipText("Start Decoding");
  fStartButton->Connect("Clicked()","StartFrame",this,"Start()");

  fUpFrame->AddFrame(fCheckFrame,fL5);
  fUpFrame->AddFrame(fTaskFrame);

  fDownFrame->AddFrame(fDecodingFrame,fL1);
  fDownFrame->AddFrame(fStartButton,fL4);

  fMain->AddFrame(fUpFrame,fL5);
  fMain->AddFrame(fInputFrame);
  fMain->AddFrame(fDownFrame,fL5);

  // position relative to the parent's window
  Window_t wdum;
  int ax, ay;
  TGMainFrame *main=fMainframe->GetFrame();
  gVirtualX->TranslateCoordinates(main->GetId(), fMain->GetParent()->GetId(),
				  (((TGFrame *) main)->GetWidth() 
				   + fMain->GetWidth()) >> 1,
				  (((TGFrame *) main)->GetHeight() 
				   + fMain->GetHeight()) >> 1,
                           ax, ay, wdum);
  fMain->Move(ax, ay);

  fMain->SetWindowName("Input");
  fMain->MapSubwindows();
  fMain->Resize(fMain->GetDefaultSize());
  fMain->MapWindow();
}


StartFrame::~StartFrame() {

  delete fRefEntry;
  delete fRefLabel;
  delete fEvtMax;
  delete fEvtLabel;
  delete fMinSpacingEntry;
  delete fMinSpacingLabel;
  delete fSetupButton;
  delete fDecodingFrameDown;
  delete fDecodingFrame;
  delete fStartButton;
  delete fDownFrame;
  delete fMapButton;
  delete fMapLabel;
  delete fMapFrame;
  delete fRawButton;
  delete fRawLabel;
  delete fRawFrame;
  delete fInputFrame;
  fCheckButton.clear();
  //delete fDefButton;
  delete fAllButton;
  delete fNoneButton;
  delete fMonDetFrame;
  delete fAllNoneFrame;
  delete fCheckFrame;
  delete fCheckTree;
  delete fCheckText;
  delete fCheckTrack;
  delete fCheckCluster;
  delete fCheckExpert;
  delete fTaskFrame;
  delete fUpFrame;
  delete fL5;
  delete fL4;
  delete fL3;
  delete fL2;
  delete fL1;

  //cout<<"deleting fMain"<<endl;
  delete fMain;
  //cout<<"StartFrame deleted"<<endl;
}


void StartFrame::CheckedDet(Bool_t on)
{
  TGButton* btn = (TGButton*) gTQSender;
  const int id = btn->WidgetId();
  
  //cout << "StartFrame::CheckedDet() id = " << id << ", on = " << on << endl;
  
  std::map<std::string,bool>& detintree=fMonitor->GetDetInTree();
  typedef std::map<std::string,bool>::iterator IM;
  int i=0;
  for(IM mi=detintree.begin();mi!=detintree.end();mi++) {
    if (i == id) {
      mi->second = on;
      return;
    }
    i++;
  }
}

void StartFrame::HandleButtons(int id) {

  if (id == -1) {
    TGButton *btn = (TGButton *) gTQSender;
    id = btn->WidgetId();
  }
  TGFileInfo fi;
  switch(id) {
  case 100 :
    fi.fFileTypes = CHAR_TGFILEINFO(datatype);
    new TGFileDialog(gClient->GetRoot(), fMain, kFDOpen,&fi);	    
    if(fi.fFilename!=0) {
      std::string name=fi.fFilename;
      unsigned int pos=name.find("@",0,1);
      if(pos<name.size()) {
	name.erase(0,pos);
  fDataFile.clear();
	fDataFile.push_back(name.c_str());
      }
      else {
        fDataFile.clear();
        fDataFile.push_back(fi.fFilename);
      }
      
      TGString *l=new TGString(fDataFile[0].c_str());
      fRawLabel->SetText(l);
      fRawLabel->Resize(fRawLabel->GetDefaultSize());
      fMain->MapSubwindows();
      fMain->Layout();
      fMain->Resize(fMain->GetDefaultSize());
      fMain->MapWindow(); 
    }
    break;
  case 101 :
    fi.fFileTypes = CHAR_TGFILEINFO(maptype);
    new TGFileDialog(gClient->GetRoot(), fMain, kFDOpen,&fi);	
    if(fi.fFilename!=0) {
      fMapFile=fi.fFilename; 
      TGString *l=new TGString(fMapFile.c_str());
      fMapLabel->SetText(l);
      fMapLabel->Resize(fMapLabel->GetDefaultSize());
      fMain->MapSubwindows();
      fMain->Layout();
      fMain->Resize(fMain->GetDefaultSize());
      fMain->MapWindow();      
    }
    break;    
  case 102 :
    fi.fFileTypes = CHAR_TGFILEINFO(geomtype);
    new TGFileDialog(gClient->GetRoot(), fMain, kFDOpen,&fi);	
    if(fi.fFilename!=0) {
      fGeomFile=fi.fFilename; 
      TGString *l=new TGString(fGeomFile.c_str());
      fGeomLabel->SetText(l);
      fGeomLabel->Resize(fGeomLabel->GetDefaultSize());
      fMain->MapSubwindows();
      fMain->Layout();
      fMain->Resize(fMain->GetDefaultSize());
      fMain->MapWindow();      
    }
    break;    
  case 103:
    fMonitor->ControlPanel(gClient->GetRoot(),fMain);
    break;
  default:
    break;
  }
}


void StartFrame::CheckedTree(Bool_t on) { fMonitor->DoTree(on); }

void StartFrame::CheckedText(Bool_t on) { fMonitor->DoTextOutput(on); }

void StartFrame::CheckedTrack(Bool_t on) { fMonitor->DoTracking(on); }

void StartFrame::CheckedCluster(Bool_t on) { fMonitor->DoClustering(on); }

void StartFrame::CheckedRef(Bool_t on) { fMonitor->DoShowRef(on); }

void StartFrame::CheckedCalib(Bool_t on) { fMonitor->DoCalib(on); }

void StartFrame::CheckedReadFailedEvt(Bool_t on) { fMonitor->DoReadFailedEvt(on); }

void StartFrame::CheckedExpert(Bool_t on) { fMonitor->DoExpertHisto(on); }

void StartFrame::DefClicked()
{
  AllClicked();
}

void StartFrame::AllClicked()
{
  for (size_t i = 0; i < fCheckButton.size(); i++)
    fCheckButton[i]->SetOn(kTRUE, kTRUE);
}

void StartFrame::NoneClicked()
{
  for (size_t i = 0; i < fCheckButton.size(); i++)
    fCheckButton[i]->SetOn(kFALSE, kTRUE);
}
