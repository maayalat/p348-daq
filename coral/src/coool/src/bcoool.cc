#include "Monitor.h"
#include "VariousSettings.h"

#include <map>
#include <string>
using namespace std;

#include <TError.h>
#include <TStyle.h>

char ApplicationName[32] = "BCOOOL main version";

bool thr_flag = false;
extern int monitoringLogLevel;

void usage()
{
  cout << "Usage: ./bcoool (options) (data files)\n"
       << "options:\n"
       << "  [-map map_file] - detectors mapping data\n"
       << "  [-ps cfg_file] - output .ps file with histograms defined by the input .cfg file\n"
       << "  [-pdf cfg_file] - output .pdf file with histograms defined by the input .cfg file\n"
       << "  [-orderlyshiftplots path_to_output_file]\n"
       << "  [-root root_file] [-rootdir root_dir]\n"
       << "  [-text text_file] [-textdir text_dir]\n"
       << "  [-group groups_file] - definition of detectors groups\n"
       << "  [-nevent nb_of_events] [-evtspacing spacing of events]\n"
       << "  [-cfg user_configuration_file]\n"
       << "  [-trigmask (trigger mask binary value)]\n"
       << "  [-evttypes (event types to read, given with format: START_OF_RUN,START_OF_BURST,PHYSICS_EVENT,CALIBRATION_EVENT,...)]\n"
       << "  [-notree] [-nocluster] [-nocalib] [-withref] [-withnofailedevt] [-experthistos]\n"
       << "  [-cooolrc cooolrc file]\n"
       << "  [-specificspill spill number to read specifically]\n"
       << "  [-workenv working environment string]\n"
       << "  [-geom detectors_geometry_file]\n"
       << "  [-file_list file giving list of files]\n"
       << "  [-run_number number]\n"
       << "data files:"
       << "  [data file or DATE source] [second data file] ...\n";
}

// trigger load of default .pcm libraries
// and avoid flow of messages from TCling::LoadPCM()
void TriggerSilentLoadPCM() {
  // bump ignore level to suppress tons of the TCling::LoadPCM() messages
  gErrorIgnoreLevel = kError+1;
  
  // create a dummy instance of TFile to trigger default .pcm load,
  // inspared by comment in function TROOT::RegisterModule()
  //   https://root.cern/doc/master/TROOT_8cxx_source.html#l02487
  TFile* a = new TFile;
  delete a;
  
  // restore ignore level to default
  gErrorIgnoreLevel = kUnset;
}

int main(int argc, char* argv[])
{
  cout << "bcoool argc=" << argc << endl;
  
  //monitoringLogLevel = 10;
  char PID[32];
  sprintf(PID,"pid: %u", getpid());
  setenv("DATE_SITE",PID,1);
  
  // set default style, same as in coool.cc
  gStyle->SetOptStat("emriou");
  gStyle->SetLineScalePS(1.);
  gStyle->SetPadGridX(kTRUE);
  gStyle->SetPadGridY(kTRUE);
  gStyle->SetGridColor(kGray);
  // display bin content as integer value for "TEXT" draw option
  gStyle->SetPaintTextFormat(".0f");
  
    if (argc < 2) {
      usage();
      return 1;
    }
    
    // default parameters
    std::string mapfile="";
    std::string rootfile="DEFAULT";
    std::string rootdir=".";
    std::string textfile="";
    std::string textdir="";
    std::string cooolrc ="";
    std::string pscfg="";
    std::string pdfcfg="";
    std::vector<std::string> datafilelist;
    std::string orderlyshiftplots;
    std::string filelist="";
    std::string groupfile="";
    std::string paramfile = ""; // default settings of variables and histograms (#bins, ranges)
    std::string geomfile = ""; // detector geometry description for tracking/reconstruction
    std::string configfile = "";
    std::string evttypes = "";
    std::string wkenv = "AFS";
    int nbofevt = 0;
    int evtspacing = 0;
    bool dotree = true;
    bool docluster = true;
    bool docalib = true;
    bool doshowref = false;
    bool dofailedevt = true;
    bool stopatendrun = false; 
    bool experthistos = false; 
    int specificspill = -1;
    int run_number = 0;
    unsigned int trigmask = 0xffffffff;
    
    // arguments parsing
    for (int i = 1; i < argc; ++i) {
      const std::string pargv =  argv[i];
      
      // data file name
      if (pargv[0] != '-') {
        datafilelist.push_back(pargv);
        continue;
      }
      
      const std::string pargvn =  (i + 1 < argc) ? argv[i + 1] : "";
      int narg = -1;
      
      // it is an option
      if (pargv == "-map")        { narg=1; mapfile = pargvn; }  // Map file
      if (pargv == "-run_number") { narg=1; run_number = atoi(pargvn.c_str()); }  // option to wait start of the run with this number
      if (pargv == "-ps")         { narg=1; pscfg = pargvn; }   // .cfg input file for .ps output
      if (pargv == "-pdf")        { narg=1; pdfcfg = pargvn; }   // .cfg input file for .pdf output
      if (pargv == "-orderlyshiftplots")  { narg=1; orderlyshiftplots = pargvn; }   // option to give ps output file
      if (pargv == "-root")       { narg=1; rootfile = pargvn; } // root file
      if (pargv == "-rootdir")    { narg=1; rootdir = pargvn; }  // output directory for root files
      if (pargv == "-text")       { narg=1; textfile = pargvn; } // output file name for text infos
      if (pargv == "-textdir")    { narg=1; textdir = pargvn; }  // output directory for text infos
      if (pargv == "-group")      { narg=1; groupfile = pargvn; }  // groups file
      if (pargv == "-cfg")        { narg=1; configfile = pargvn; } // user config file
      if (pargv == "-geom")       { narg=1; geomfile = pargvn; }   // user config file
      if (pargv == "-evttypes")   { narg=1; evttypes = pargvn; }   // event types to read
      if (pargv == "-cooolrc")    { narg=1; cooolrc = pargvn; }    // activate reading of .cooolrc file
      if (pargv == "-nevent")     { narg=1; nbofevt = atoi(pargvn.c_str()); }    // nb of events to read
      if (pargv == "-evtspacing") { narg=1; evtspacing = atoi(pargvn.c_str()); } // min event spacing
      if (pargv == "-specificspill") { narg=1; specificspill = atoi(pargvn.c_str()); } // option to read only one specific spill (for latency scans)
      if (pargv == "-trigmask")   { narg=1; trigmask = strtoul(pargvn.c_str(), 0, 0); } // the trigger mask to use
      if (pargv == "-workenv")    { narg=1; wkenv = pargvn; }     // working environment string
      if (pargv == "-file_list")  { narg=1; filelist = pargvn; }  // a file giving a list of data files
      
      if (pargv == "-notree")        { narg=0; dotree = false; }    // disable tree creation
      if (pargv == "-nocluster")     { narg=0; docluster = false; } // disable clustering
      if (pargv == "-nocalib")       { narg=0; docalib = false; }   // disable calibration
      if (pargv == "-withref")       { narg=0; doshowref = true; }  // enable reference plots
      if (pargv == "-withfailedevt") { narg=0; dofailedevt = true; } // enable reading of evt badly decoded (for example, without trigger time)
      if (pargv == "-nofailedevt")   { narg=0; dofailedevt = false; } // disable reading of evt badly decoded
      if (pargv == "-stopatendrun")  { narg=0; stopatendrun = true; } // stop decoding if EOR event seen
      if (pargv == "-experthistos")  { narg=0; experthistos = true; } // activate expert histograms
      
      if (pargv == "--help") { usage(); return 0; }  // print usage instructions
      
      if (narg == -1) {
        // unknown option found
        cerr << "ERROR: unknown option " << pargv << endl;
        return 1;
      }
      
      i += narg;
    }
    
    // TODO: bcoool crashes if tree is filled without clustering
    if (!docluster) dotree = false;
    
    // read file list
    if (filelist != "") {
      ifstream in(filelist.c_str());
      
      if (!in) {
        std::cerr << "ERROR: can't open file_list file " << filelist << std::endl;
        return 1;
      }
      
      std::string fname;
      while (in >> fname)
        datafilelist.push_back(fname);
    }
    
    // check parameters
    if (mapfile == "") {
      cerr << "ERROR: map is not specified" << endl;
      return 1;
    }
    
    if (datafilelist.empty()) {
      cerr << "ERROR: no input files specified" << endl;
      return 1;
    }
    
    TriggerSilentLoadPCM();
    
    Monitor *monitor=new Monitor();
    VariousSettings *settings = new VariousSettings(monitor);

    // select all detectors for monitoring
    // 'i' is <std::string,bool>
    for (auto& i : monitor->GetDetInTree()) i.second = true;
    
    if (cooolrc != "") settings->ReadPersoParam(cooolrc.c_str());
    if(run_number != 0)
      monitor->SetExpectingRunNumber(run_number);
    monitor->DoTree(dotree);
    monitor->DoClustering(docluster);
    monitor->DoShowRef(doshowref);
    monitor->DoReadFailedEvt(dofailedevt);
    monitor->DoCalib(docalib);
    monitor->StopAtEndRun(stopatendrun);
    monitor->DoExpertHisto(experthistos);
    monitor->SetRootDir(rootdir.c_str());
    monitor->DoTextOutput(!textfile.empty());
    monitor->SetTextFile(textfile.c_str());
    monitor->SetTextDir(textdir.c_str());
    if (evttypes != "") monitor->CheckEvtTypes(evttypes.c_str());
    monitor->SetTriggerMask(trigmask);
    monitor->SetWkEnvStr(wkenv.c_str());
    monitor->SetEventNumber(nbofevt);
    monitor->SetEventSpacing(evtspacing);
    monitor->SetSpecificSpill(specificspill);

    monitor->Init(mapfile,rootfile,datafilelist,groupfile,geomfile);
    
    if (paramfile != "") settings->LoadDefaultSettings(paramfile);

    if (configfile != "") settings->LoadConfigSettings(configfile, 2, false);
    
    monitor->Run();
    monitor->CloseRootFile();

    cout << "Total events: " << monitor->GetEventNumber() << endl;
    
    if (pscfg != "") {
      cout << "INFO: generating .ps output file based on pscfg=" << pscfg << endl;
      settings->GuiSettingsCreatePS(pscfg);
    }
    
    if (pdfcfg != "") {
      cout << "INFO: generating .pdf output file based on pdfcfg=" << pdfcfg << endl;
      settings->GuiSettingsCreatePDF(pdfcfg);
    }

    if(!orderlyshiftplots.empty()) {
      //settings->GuiSettingsCreateOrderlyShiftPlots(orderlyshiftplots);
      //std::cout<<"orderly shift plots: "<<orderlyshiftplots<<" created"<<std::endl;

      std::string filename = orderlyshiftplots;
      std::string filepath = "";
      std::string filetype = "";

      std::string::size_type posfilename = orderlyshiftplots.rfind("/");  // to remove all path, the file is created in the current dir
      if (posfilename != std::string::npos) {
        filename=orderlyshiftplots.substr(posfilename+1); //get the file name
        filepath=orderlyshiftplots.substr(0,posfilename+1); //get the file path
      }

      std::string::size_type pospt = filename.rfind("."); // find the sufix
     
      if (pospt != std::string::npos) {
        filetype=filename.substr(pospt+1);  //get the file type
        filename.erase(pospt); // to remove the last .something suffix
      }

      if (filetype == "ps" || filetype == "pdf") {
        settings->GuiSettingsCreateOrderlyShiftPlotsUniversal(filepath, filename, filetype, 1);
      }
      else {
        std::cout << "No proper file extension found ( .ps / .pdf ) -> exiting!" << endl;
      }


    }

    delete settings;
    delete monitor;
    
    return 0;
}
