#include "GroupMM.h"

#include "TThread.h"

//ClassImp(GroupMM);

GroupMM::GroupMM(const char* name)
: Group(name),
  planeX(0), planeY(0),
  histoXY(0), histoXY_phys(0), histoXY_beam(0), histoXY_LED(0), histoXY_rand(0),
  dummyXY(0),histoX(0), histoY(0),
  histoEff_vs_spill(0), histoEff_vs_spill_phys(0), histoEff_vs_spill_beam(0),
  Kx(1.), Ky(0.)
{
 fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT};
}

void GroupMM::NewRun(int runnumber)
{
  double angle = 0;
  
  const string mm = fName;
  
  if (runnumber <= 4214) {
  // 2018, invisible mode
  if (mm == "Micromega1") angle = -M_PI/4;
  if (mm == "Micromega2") angle =  M_PI/4;
  if (mm == "Micromega3") angle = -M_PI/4;
  if (mm == "Micromega4") angle = -M_PI/4;
  if (mm == "Micromega5") angle =  M_PI/4;
  if (mm == "Micromega6") angle = -M_PI/4;
  }
  else if (runnumber <= 4310){
  // 2018, visible mode
  if (mm == "Micromega1") angle = -M_PI/4;
  if (mm == "Micromega2") angle = -M_PI/4;
  if (mm == "Micromega3") angle =  M_PI/4;
  if (mm == "Micromega4") angle =  M_PI/4;
  if (mm == "Micromega5") angle = -M_PI/4;
  if (mm == "Micromega6") angle = -M_PI/4;
  }
  else if (runnumber <= 5187){
  // 2021, invisible mode
  if (mm == "Micromega1") angle = -M_PI/4;
  if (mm == "Micromega2") angle = -M_PI/4;
  if (mm == "Micromega3") angle = -M_PI/4;
  if (mm == "Micromega4") angle = -3*M_PI/4;
  if (mm == "Micromega5") angle = -M_PI/4;
  if (mm == "Micromega6") angle = -3*M_PI/4;
  }
  else if (runnumber <= 6034){
  // 2021, muon mode
  if (mm == "Micromega1") angle = -3*M_PI/4;
  if (mm == "Micromega2") angle = -M_PI/4;
  if (mm == "Micromega3") angle = -3*M_PI/4;
  if (mm == "Micromega4") angle = -M_PI/4;
  if (mm == "Micromega5") angle = 0.;
  if (mm == "Micromega6") angle = 0.;
  if (mm == "Micromega7") angle = 0.;
  }
  else if (runnumber <= 8458) {
  // 2022, electron mode
  if (mm == "Micromega1") angle = -3*M_PI/4;
  if (mm == "Micromega2") angle = -3*M_PI/4;
  if (mm == "Micromega3") angle = -3*M_PI/4;
  if (mm == "Micromega4") angle = -3*M_PI/4;
  if (mm == "Micromega5") angle = 0.;
  if (mm == "Micromega6") angle = 0.;
  if (mm == "Micromega7") angle = 0.;
  } 
  else if (runnumber <= 9717) {
  // 2023, electron mode
  if (mm == "Micromega1") angle = -3*M_PI/4;
  if (mm == "Micromega2") angle = -5*M_PI/4;
  if (mm == "Micromega3") angle = -3*M_PI/4;
  if (mm == "Micromega4") angle = -5*M_PI/4;
  if (mm == "Micromega5") angle = M_PI;
  if (mm == "Micromega6") angle = 0.;
  if (mm == "Micromega7") angle = 0.;
  }
  else if (runnumber <= 10315) {
  // 2023, muon mode
  // TODO: Check angles in current configuration
  if (mm == "Micromegas01") angle = -3*M_PI/4;
  if (mm == "Micromegas02") angle = -5*M_PI/4;
  if (mm == "Micromegas03") angle = -3*M_PI/4;
  if (mm == "Micromegas04") angle = -5*M_PI/4;
  if (mm == "Micromegas05") angle = M_PI;
  if (mm == "Micromegas06") angle = M_PI;
  if (mm == "Micromegas07") angle = M_PI;
  if (mm == "Micromegas08") angle = -3*M_PI/4;
  if (mm == "Micromegas09") angle = -5*M_PI/4;
  if (mm == "Micromegas10") angle = -3*M_PI/4;
  if (mm == "Micromegas11") angle = -5*M_PI/4;
  } 
  else {
  // 2024, electron mode
  if (mm == "Micromegas01") angle = -3*M_PI/4;
  if (mm == "Micromegas02") angle = -5*M_PI/4;
  if (mm == "Micromegas03") angle = -3*M_PI/4;
  if (mm == "Micromegas04") angle = -5*M_PI/4;
  if (mm == "Micromegas05") angle = M_PI;
  if (mm == "Micromegas06") angle = 0.;
  if (mm == "Micromegas07") angle = 0.;
  }
  Kx = cos(angle);
  Ky = sin(angle);
  
  cout << "GroupMM() name = " << mm
       << ", angle = " << angle
       << ", Kx = " << Kx
       << ", Ky = " << Ky
       << endl;
}

void GroupMM::Init()
{
  const size_t n = fPlanes.size();
  if (n != 2) {
    cout << "GroupMM::Init(): name = " << GetName() << ", #planes = " << n << endl;
    //cout << "WARNING GroupMM::Init(): number of planes is not two" << endl;
  }

  // search X and Y planes of MM
  for (size_t i = 0; i < n; ++i) {
    const PlaneMM* p = dynamic_cast<const PlaneMM*>(fPlanes[i]);
    if (!p) continue;
    
    const TString name = p->GetName();
    if (name.Contains("X")) planeX = p;
    if (name.Contains("Y")) planeY = p;
  }
 
  // Quick fix to adapt histos for the large MMs 
  int limits = 55;
  if (fName == "Micromega5" || fName == "Micromega6" || fName == "Micromega7") limits = 125;
  if (fName == "Micromegas05" || fName == "Micromegas06" || fName == "Micromegas07") limits = 125;
  const int len = 10*limits;

  // load reference data
  OpenReference();
  
  if (planeX && planeY) {
    TString name;
    name = planeX->GetName() + TString("Y");
    histoXY = new TH2F(name, name+";X,mm;Y,mm", len, -limits, limits, len, -limits, limits);
    histoXY->SetOption("BOX COLZ");
    AddHistogram(histoXY);
    histoXY_phys = new TH2F(name+"_phys", name+" phys trigger;X,mm;Y,mm", len, -limits, limits, len, -limits, limits);
    histoXY_phys->SetOption("BOX COLZ");
    AddHistogram(histoXY_phys);
    histoXY_beam = new TH2F(name+"_beam", name+" beam trigger only;X,mm;Y,mm", len, -limits, limits, len, -limits, limits);
    histoXY_beam->SetOption("BOX COLZ");
    AddHistogram(histoXY_beam);
    histoXY_rand = new TH2F(name+"_rand", name+" random trigger only;X,mm;Y,mm", len, -limits, limits, len, -limits, limits);
    histoXY_rand->SetOption("BOX COLZ");
    AddHistogram(histoXY_rand);
    histoXY_LED = new TH2F(name+"_LED", name+" all LED triggers;X,mm;Y,mm", len, -limits, limits, len, -limits, limits);
    histoXY_LED->SetOption("BOX COLZ");
    AddHistogram(histoXY_LED);
    
    // projections plots
    name = planeX->GetName() + TString("proj");
    TH1F_Ref* h1 = new TH1F_Ref(name, name+";X,mm", len/2, -limits, limits, fRateCounter);
    h1->SetReference(fReferenceDirectory);
    histoX = h1;
    AddHistogram(histoX);
    
    name = planeY->GetName() + TString("proj");
    TH1F_Ref* h2 = new TH1F_Ref(name, name+";Y,mm", len/2, -limits, limits, fRateCounter);
    h2->SetReference(fReferenceDirectory);
    histoY = h2;
    AddHistogram(histoY);
    
    // vs. spill plots
    name = fName + TString("_X_vs_spill");
    TProfile_Ref* p1 = new TProfile_Ref(name, name+";spill;X mean #pm stddev,mm", 200, 0, 200);
    p1->SetReference(fReferenceDirectory);
    p1->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p1->SetMarkerStyle(kFullDotMedium);
    //p1->SetOption("P");
    histoX_vs_spill = p1;
    AddHistogram(histoX_vs_spill);
    
    name = fName + TString("_Y_vs_spill");
    TProfile_Ref* p2 = new TProfile_Ref(name, name+";spill;Y mean #pm stddev,mm", 200, 0, 200);
    p2->SetReference(fReferenceDirectory);
    p2->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p2->SetMarkerStyle(kFullDotMedium);
    //p2->SetOption("P");
    histoY_vs_spill = p2;
    AddHistogram(histoY_vs_spill);
    
    name = fName + TString("_Eff_vs_spill");
    TProfile_Ref* p3 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p3->SetReference(fReferenceDirectory);
    p3->SetMaximum(1.1);
    p3->SetMarkerStyle(kFullDotMedium);
    p3->SetOption("P");
    histoEff_vs_spill = p3;
    AddHistogram(histoEff_vs_spill);
    
    name = fName + TString("_Eff_vs_spill_phys");
    TProfile_Ref* p4 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p4->SetReference(fReferenceDirectory);
    p4->SetMaximum(1.1);
    p4->SetMarkerStyle(kFullDotMedium);
    p4->SetOption("P");
    histoEff_vs_spill_phys = p4;
    AddHistogram(histoEff_vs_spill_phys);
 
    name = fName + TString("_Eff_vs_spill_beam");
    TProfile_Ref* p5 = new TProfile_Ref(name, name+";spill;efficiency", 200, 0, 200);
    p5->SetReference(fReferenceDirectory);
    p5->SetMaximum(1.1);
    p5->SetMarkerStyle(kFullDotMedium);
    p5->SetOption("P");
    histoEff_vs_spill_beam = p5;
    AddHistogram(histoEff_vs_spill_beam);

  }
}

void GroupMM::EndEvent(const CS::DaqEvent &event, const EventFlags& flags)
{
  if (thr_flag) TThread::Lock();
  
  //cout << "GroupMM::EndEvent() name = " << GetName() << endl;
  
  const bool hasXhit = planeX ? planeX->HasHit() : false;
  const bool hasYhit = planeY ? planeY->HasHit() : false;
  const bool hasHit = hasXhit && hasYhit;
  const int spill = event.GetBurstNumber();
  
  if (histoEff_vs_spill) histoEff_vs_spill->Fill(spill, hasHit);
  if (flags.isTriggerPhys && histoEff_vs_spill_phys) histoEff_vs_spill_phys->Fill(spill, hasHit);
  if (flags.isTriggerBeamOnly() && histoEff_vs_spill_beam) histoEff_vs_spill_beam->Fill(spill, hasHit);
  
  if (hasHit) {
    // translate into center and scale to mm units
    // Quick fix to adapt histos for the large MMs 
    double xoffset = 159.5;
    double specular = -1.; //prefactor, to adjust for the desidered beam profile
    if (fName == "Micromegas11") specular = 1.;
    if (fName == "Micromega5" || fName == "Micromega6" || fName == "Micromega7") xoffset = 479.5;
    if (fName == "Micromegas05" || fName == "Micromegas06" || fName == "Micromegas07") xoffset = 479.5;
    const double hitx = (planeX->GetHitPos() - xoffset) * 0.25;
    const double hity = (planeY->GetHitPos() - 159.5) * 0.25;
    
    const double x = specular * (-Kx*hitx +Ky*hity);
    //const double x = factor*Kx*hitx +Ky*hity; //coordinate system apparently preferred
    //const double y = -Ky*hitx -Kx*hity;
    const double y = +Ky*hitx +Kx*hity; //to point up instead of down
    histoXY->Fill(x, y);
    if (flags.isTriggerPhys) histoXY_phys->Fill(x, y);
    if (flags.isTriggerBeamOnly()) histoXY_beam->Fill(x, y);
    if (flags.isTriggerRand) histoXY_rand->Fill(x, y);
    if (flags.isLED0() || flags.isLED1()) histoXY_LED->Fill(x, y);
    histoX->Fill(x);
    histoY->Fill(y);
    
    histoX_vs_spill->Fill(spill, x);
    histoY_vs_spill->Fill(spill, y);
  }
  
  if (needRateHistoUpdate()) {
  if(histoXY)
    {
      const double eff = histoXY->Integral() / (fRateCounter + 1);
      const TString eff_str = Form("%s eff_{total} (Nhit > 0) = %.3f", fName.c_str(), eff);
      histoXY->SetTitle(eff_str);
    }
  if(histoXY_phys)
    {
      const double eff_phys = histoXY_phys->Integral() / (fRateCounterPhysicsOnly + 1);
      const TString eff_str = Form("%s eff_{phys} (Nhit > 0) = %.3f", fName.c_str(), eff_phys);
      histoXY_phys->SetTitle(eff_str);
    }
  if(histoXY_beam)
    {
      const double eff_beam = histoXY_beam->Integral() / (fRateCounterBeamOnly + 1);
      const TString eff_str = Form("%s eff_{beam} (Nhit > 0) = %.3f", fName.c_str(), eff_beam);
      histoXY_beam->SetTitle(eff_str);
    }
  if(histoXY_rand)
    {
      const double eff_rand = histoXY_rand->Integral() / (fRateCounterRand + 1);
      const TString eff_str = Form("%s eff_{rand} (Nhit > 0) = %.3f", fName.c_str(), eff_rand);
      histoXY_rand->SetTitle(eff_str);
    }
  if(histoXY_LED)
    {
      const double eff_led = histoXY_LED->Integral() / (fRateCounterLED + 1);
      const TString eff_str = Form("%s eff_{LED} (Nhit > 0) = %.3f", fName.c_str(), eff_led);
      histoXY_LED->SetTitle(eff_str);
    }
  }
  
  if (thr_flag) TThread::UnLock();
}
