#include "GroupHCAL.h"

#include "TThread.h"

//ClassImp(GroupHCAL);

GroupHCAL::GroupHCAL(const char* name)
: Group(name),
  histo0vs1(0), histo01(0),histo012(0), histo0123(0)
{
  fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT};
  
  for (int i = 0; i < 4; ++i) hcal[i] = 0;
}

void GroupHCAL::Init()
{
  const size_t n = fPlanes.size();
  cout << "GroupHCAL::Init(): name = " << GetName() << ", #planes = " << n << endl;
  
  // search HCAL planes
  for (size_t i = 0; i < n; ++i) {
    const PlaneCalo* p = dynamic_cast<const PlaneCalo*>(fPlanes[i]);
    if (!p) continue;
    
    const TString name = p->GetName();
    if (name.Contains("HCAL")) {
      const int idx = TString(name[4]).Atoi();
      
      const bool inRange = (0 <= idx) && (idx <= 3);
      if (!inRange) {
        cout << "WARNING: GroupHCAL::Init(): the index is out of expected range [0-3]"
             << " plane name = " << name << ", idx = " << idx << endl;
        continue;
      }
      
      hcal[idx] = p;
    }
  }
  
  // load reference data
  OpenReference();
  
  if (hcal[0] && hcal[1]) {
    //const TString name = plane->GetName() + TString("Y");
    histo0vs1 = new TH2F("HCAL0vs1", "HCAL0vs1;hcal0,ADC;hcal1,ADC", 250, 0, 5000, 250, 0, 5000);
    histo0vs1->SetOption("BOX COLZ");
    AddHistogram(histo0vs1);
    
    TH1F_Ref* h1 = new TH1F_Ref("HCAL01", "HCAL0+1;hcal0+1,ADC", 250, 0, 5000, fRateCounter);
    h1->SetReference(fReferenceDirectory);
    histo01 = h1;
    AddHistogram(histo01);
    
    if (hcal[2]) {
      TH1F_Ref* h2 = new TH1F_Ref("HCAL012", "HCAL0+1+2;hcal0+1+2,ADC", 250, 0, 5000, fRateCounter);
      h2->SetReference(fReferenceDirectory);
      histo012 = h2;
      AddHistogram(histo012);
      
      if (hcal[3]) {
        TH1F_Ref* h3 = new TH1F_Ref("HCAL0123", "HCAL0+1+2+3;hcal0+1+2+3,ADC", 250, 0, 5000, fRateCounter);
        h3->SetReference(fReferenceDirectory);
        histo0123 = h3;
        AddHistogram(histo0123);
      }
    }
  }
}

double vecsum(const vector<float>& v)
{
  double s = 0.;
  for (size_t i = 0; i < v.size(); ++i) s += v[i];
  return s;
}

void GroupHCAL::EndEvent(const CS::DaqEvent &event)
{
  if (thr_flag) TThread::Lock();
  
  //cout << "GroupHCAL::EndEvent() name = " << GetName() << endl;
  
  if (hcal[0] && hcal[1]) {
    double sum0 = vecsum(hcal[0]->GetLastEvent());
    double sum1 = vecsum(hcal[1]->GetLastEvent());
    histo0vs1->Fill(sum0, sum1);
    histo01->Fill(sum0+sum1);
    
    if (hcal[2]) {
      double sum2 = vecsum(hcal[2]->GetLastEvent());
      histo012->Fill(sum0+sum1+sum2);
      
      if (hcal[3]) {
        double sum3 = vecsum(hcal[3]->GetLastEvent());
        histo0123->Fill(sum0+sum1+sum2+sum3);
      }
    }
  }
  
  if (thr_flag) TThread::UnLock();
}
