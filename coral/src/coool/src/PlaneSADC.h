#ifndef __PlaneSADC__
#define __PlaneSADC__

#include "Plane2V.h"

#include "TTree.h"
#include "TProfile.h"

#include <iostream>
#include <fstream>

/// Plane class for any detectors connested to SADC frontend

class PlaneSADC : public Plane2V {
private:
  
  TH1F *fHsac, *fHch, *fHma, *fHhth, *fBad, *fGood;
  TH2F *fHchac, *fHxyac, *fHxyn;

  TH1F *fCH_amp[33][27], *fSum_led, *fSum_time;
  TH1F *fCH_time[33][27];
     
  //TH2F *fHrefled_low,*fHrefled_hig;
  TH2F *fAmpLed,*fHchampled;
  TH2F *fPed_xy;  
  TH2F *fTime_xy; 
  TH2F *fPed_ch; 
  TH2F *fRMS_Ped_xy;  
  TH2F *fRMS_Ped_ch;

  TH1F *fPed_sum;  
  TH1F *fRMS_Ped_sum;  
   
  TProfile *fProf;
//  TProfile *fSProfile[33][27];
  TH2F *fSProfile[33][27];
  
public:
  PlaneSADC(const char *detname, int nrow, int ncol, int center, int width)
  : Plane2V(detname,nrow, ncol, center, width)
  {}
  
  ~PlaneSADC() {}
  
  void Init(TTree* tree =0);
  
#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
  void StoreDigit(CS::Chip::Digit* digit);
#endif
  
  ClassDef(PlaneSADC,0)
};

#endif
