#include <math.h>

#include "PlaneScaler.h"
#include "PlanePanel.h"
#include "TThread.h"
#include "Riostream.h"
#include <string>

ClassImp(PlaneScaler);

const int PlaneScaler::fNchan = 32;


PlaneScaler::PlaneScaler(const char *detname, int ebe) 
  : Plane(detname), fIsEbe(ebe),  fBeamhitspspill(0), fSpillnum(0) {

  //fChanMult = new int[PlaneScaler::fNchan]; 
  //fLastCounts = new int[PlaneScaler::fNchan]; 
  //fCounts = new int[PlaneScaler::fNchan]; 
  fChanMult.resize(PlaneScaler::fNchan); 
  fLastCounts.resize(PlaneScaler::fNchan); 
  fCounts.resize(PlaneScaler::fNchan); 

  for(int i=0; i<PlaneScaler::fNchan; i++) {
    fChanMult[i]=0;
    fLastCounts[i]=0;
    fCounts[i]=0;  
  }
  fTime = 0;
  fLastTime = 0;
  fNEvents = 0;
}

void PlaneScaler::Reset() {
  for(int i=0; i<PlaneScaler::fNchan; i++) {
    fChanMult[i]=0;
    fLastCounts[i]=fCounts[i];
    fCounts[i]=0;
  }
  fLastTime = fTime;
}

void PlaneScaler::NewRun(int) {
  // it is meaningless to go on filling this histo when starting a new run...
  if (thr_flag) MYLOCK();
  fHIntensity->Reset();   
  if (thr_flag) MYUNLOCK();
}

void PlaneScaler::End() {
  // fill last spill intensity in fHIntensity
  if(fHIntensity) 
    fHIntensity -> Fill(fSpillnum, fBeamhitspspill);
}

void PlaneScaler::Init(TTree* tree) {
  
  fRateCounter = 0;
  fNEvents = 0;

  std::string cname = fName + "_counts";
  fHcounts = new TH1F_Ref(cname.c_str(),cname.c_str(),
		      PlaneScaler::fNchan,0,
                      PlaneScaler::fNchan,fRateCounter);
  ((TH1F_Ref*)fHcounts)->SetReference(fReferenceDirectory);
  AddHistogram(fHcounts);

  std::string tname = fName + "_times";
  fHtimes = new TH1F(tname.c_str(),tname.c_str(),
		      60,0,6);
  AddHistogram(fHtimes);


  std::string rname = fName + "_rates";
  fHrates = new TProfile(rname.c_str(),rname.c_str(),
		     PlaneScaler::fNchan,0,PlaneScaler::fNchan,
		     0,1e12);
  AddHistogram(fHrates);

  std::string sname = fName + "_spillprof";
  fHSpillProfile = new TProfile(sname.c_str(),sname.c_str(),
				60,0,6,
				0,1e8);
  AddHistogram(fHSpillProfile);
    
  std::string iname = fName + "_intensity"; 
  fHIntensity = new TH1F(iname.c_str(),iname.c_str(),
			 200,1,201);
  AddHistogram(fHIntensity);

  fHMiddle = NULL; fHMiddleT = NULL; fHMiddleTD = NULL; fHOuter = NULL; fHOuterT = NULL; fHOuterTD = NULL; fHRandom = NULL; fHDutyFactor = NULL; fHVeto = NULL;
  if( fName == "SCMSC1__" ) {
    // Middle trigger dead time 
    std::string mdtname = fName + "_dt_middle";
    fHMiddle = new TProfile(mdtname.c_str(),mdtname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHMiddle);

    // Middle trigger 
    std::string mtname = fName + "_MT_inspill";
    fHMiddleT = new TProfile(mtname.c_str(),mtname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHMiddleT);
    
    // Middle trigger delayed
    std::string mtdname = fName + "_MT_delayed_inspill";
    fHMiddleTD = new TProfile(mtdname.c_str(),mtdname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHMiddleTD);

    // Outer trigger dead time 
    std::string odtname = fName + "_dt_outer";
    fHOuter = new TProfile(odtname.c_str(),odtname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHOuter);

    // Outer trigger 
    std::string otname = fName + "_OT_inspill";
    fHOuterT = new TProfile(otname.c_str(),otname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHOuterT);
    
    // Outer trigger delayed
    std::string otdname = fName + "_OT_delayed_inspill";
    fHOuterTD = new TProfile(otdname.c_str(),otdname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHOuterTD);

    // Random trigger dead time 
    std::string rdtname = fName + "_dt_random";
    fHRandom = new TProfile(rdtname.c_str(),rdtname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHRandom);
    
    // Duty factor histogram
    std::string dfname = fName + "_duty_factor";
    fHDutyFactor = new TH1D(dfname.c_str(),dfname.c_str(),spill_bins,spill_start,spill_end);
    fHDutyFactor->SetDrawOption("E");
    AddHistogram(fHDutyFactor);
  }


  if( fName == "SCsum" ) {
    // Veto Counts in Spill 
    std::string Vsumname = fName + "_Vtot_inspill";
    fHVeto = new TProfile(Vsumname.c_str(),Vsumname.c_str(),spill_bins,spill_start,spill_end);
    AddHistogram(fHVeto);
  }

  if(tree) {
    std::string mname = fName + "_mults";
    fIsInTree = true;

    std::string mleavlist=mname + "[32]/I";    
    std::string tname=fName + "_t";
    std::string tleavlist=tname + "/I";
    std::string dtname=fName + "_dt";
    std::string dtleavlist=tname + "/F";
    std::string pname=fName + "_pattern";
    std::string pleavlist=pname + "/I";

    //      tree->Branch(mname.c_str(),fChanMult,"c01/F:c02:c03:c04:c05:c06:c07:c08:c09:c10:c11:c12:c13:c14:c15:c16:c17:c18:c19:c20:c21:c22:c23:c24:c25:c26:c27:c28:c29:c30:c31:c32",32000);
    tree->Branch(mname.c_str(),fChanMult.data(),mleavlist.c_str(),32000);
    tree->Branch(tname.c_str(),&fTime,tleavlist.c_str(),32000);
    tree->Branch(dtname.c_str(),&fDeltaTime,dtleavlist.c_str(),32000);
    tree->Branch(pname.c_str(),&fPattern,pleavlist.c_str(),32000);
  }
}


PlaneScaler::~PlaneScaler() {
//  delete fChanMult;
//  delete fLastCounts;
//  delete fCounts;
}


void PlaneScaler::StoreDigit(CS::Chip::Digit* digit) {

  std::vector<float> data=digit->GetNtupleData();

  if(data.size() != 2)
    throw CS::Exception("PlaneScaler::StoreDigit(): Scaler data not recognized.");
  
  int chan = static_cast<int>(data[0]);
  int counts = static_cast<int>(data[1]);

  //cout<<fName<<" "<<chan<<" "<<counts<<endl;

  if(chan <= 31)
    fCounts[chan] = counts;
  else if(chan == 33) // time
    fTime = counts;
  else if(chan == 32) // pattern
     fPattern = counts;
   else 
    cout << "Wrong mapping of Scaler: " << fName.c_str() << "  Channel:  " << chan << "  is not allowed" << endl;
}

void PlaneScaler::EndEvent(const CS::DaqEvent &event) {
// cerr<<"PlaneScaler::EndEvent fRateCounter "<<fRateCounter<<endl;

  if (thr_flag) MYLOCK();
  fDeltaTime = (fTime-fLastTime)/38.88E6;
  int beamhits=0;
  
  if(!fSpillnum) fSpillnum = event.GetBurstNumber();

  fNEvents += 1;

  for(int i=0; i<PlaneScaler::fNchan; i++) {
    int ch=i;

    fChanMult[ch]=fCounts[i]-fLastCounts[i];

    if (fDeltaTime>0) {
      fHcounts->Fill(ch,fChanMult[ch]);
      fHrates->Fill(ch,fChanMult[ch]/fDeltaTime);    
      beamhits+=fChanMult[ch];
    }
  }
  if (fHSpillProfile && fDeltaTime>0) {
    fHSpillProfile->Fill(fTime/38.88E6,beamhits/fDeltaTime);
  }

  if( fHMiddle ) {

    //Middle trigger dead time in spill calculation 
    if( fChanMult[27] > 0 ) {
      float RM = (float)(fChanMult[15]) / (float)(fChanMult[27]);
      float RM2 = 1.f - RM;
      fHMiddle->Fill( fTime/38.88E6, RM2 );
      fHMiddleTD->Fill( fTime/38.88E6, (float)(fChanMult[15]) );
      fHMiddleT->Fill( fTime/38.88E6, (float)(fChanMult[27]) );
      if(false &&  fabs(fTime/38.88E6 - 2) < 0.1 ) {
	std::cout<<"fChanMult[15]="<<fChanMult[15]<<"  fChanMult[27]="<<fChanMult[27]<<"  R="<<RM<<"  R2="<<RM2<<std::endl;
      }
    }
 
    //Outer trigger dead time in spill calculation 
    if( fChanMult[6] > 0 ) {
      float RO = (float)(fChanMult[0]) / (float)(fChanMult[6]);
      float RO2 = 1.f - RO;
      fHOuter->Fill( fTime/38.88E6, RO2 );
      fHOuterTD->Fill( fTime/38.88E6, (float)(fChanMult[0]) );
      fHOuterT->Fill( fTime/38.88E6, (float)(fChanMult[6]) );
      if(false &&  fabs(fTime/38.88E6 - 2) < 0.1 ) {
	std::cout<<"fChanMult[0]="<<fChanMult[0]<<"  fChanMult[6]="<<fChanMult[6]<<"  R="<<RO<<"  R2="<<RO2<<std::endl;
      }
    }

    //Random trigger dead time in spill calculation 
    if( fChanMult[2] > 0 ) {
      float R = (float)(fChanMult[9]) / (float)(fChanMult[2]);
      float R2 = 1.f - R;
      fHRandom->Fill( fTime/38.88E6, R2 );
    }

    //Duty Factor calculation
    if((fNEvents%1000)==0) {
      TH1D* p1 = fHMiddle->ProjectionX();
      TH1D* p2 = fHRandom->ProjectionX();
      p2->Divide(p1);
      for( int bin = 1; bin <= p2->GetXaxis()->GetNbins(); bin++ ) {
	//if(bin==30) std::cout<<"p2->GetBinContent("<<bin<<")="<<p2->GetBinContent(bin)<<std::endl;
	fHDutyFactor->SetBinContent( bin, p2->GetBinContent(bin) );
	fHDutyFactor->SetBinError( bin, p2->GetBinError(bin) );
	//fHDutyFactor->Draw("E");
      }
      delete p1; delete p2;
    }
  }

  //For VSum histogram fill Veto counts in spill 
  if( fHVeto ) {
    if( fChanMult[0] > 0 ) {
      fHVeto->Fill( fTime/38.88E6, (float)(fChanMult[0]) );
    }
  }  


  if(fDeltaTime<0) { // first event after a reset (ie new spill) 
    fHIntensity -> Fill(fSpillnum, fBeamhitspspill);
    fBeamhitspspill = 0;
    fSpillnum = event.GetBurstNumber();
  } else { // still in the same spill - go on counting
    fBeamhitspspill += beamhits;
  }

  fHtimes->Fill(fTime/38.88E6);
  if (thr_flag) MYUNLOCK();
}

void PlaneScaler::ControlPanel(const TGWindow* p, const TGWindow* main) {

  if (!fControlPanel) fControlPanel = new PlanePanel(p, main, 100, 100, this);
}











