#include "ChipSADC.h"
#include "DaqEvent.h"

#include <TProfile.h>

#include "Plane2V.h"
#include "PlaneSADC.h"
#include "config.h"

#include <cstring>
#include <iostream>
#include <vector>
using namespace std;

#define  fNsamples 32

ClassImp(PlaneSADC);

void PlaneSADC::Init(TTree* tree)
{
  Plane2V::Init(tree);
  
  char cell_name[132];
  TString name;
  
//  cout << "All fNameNew : " << fname<< endl;
    
  name = fName + "_Sumampcut";
  fHsac=new TH1F(name, name, fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
  fHsac->SetFillColor(8);
  AddHistogram(fHsac);
  
  name = fName + "_adr";
  fHch=new TH1F_Ref( name, name, fNrows*fNcols, 0, fNrows*fNcols, fRateCounter);
  ((TH1F_Ref*)fHch)->SetReference(fReferenceDirectory);
  fHch->SetFillColor(3);
  AddHistogram(fHch);
  
  std::string aadrcut = fName + "_adrVSamp";
  fHchac = new TH2F(aadrcut.c_str(), aadrcut.c_str(), fNrows*fNcols, 0, fNrows*fNcols, fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
  fHchac->SetOption("boxcol");
  AddHistogram(fHchac);
  
  name = fName + "_maxamp";
  fHma=new TH1F_Ref(name, name, fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax(), fRateCounter);
  ((TH1F_Ref*)fHma)->SetReference(fReferenceDirectory);
  fHma->SetFillColor(5);
  AddHistogram(fHma);
  
  name = fName + "_hitsth";
  fHhth=new TH1F_Ref(name, name, 50, 0, 50, fRateCounter);
  //   fNrows*fNcols, 0, fNrows*fNcols);
  ((TH1F_Ref*)fHhth)->SetReference(fReferenceDirectory);
  fHhth->SetFillColor(7);
  AddHistogram(fHhth);
  
  std::string xyacut = fName + "_xVSyAcut";
  fHxyac = new TH2F(xyacut.c_str(), xyacut.c_str(), fNcols, 0, fNcols, fNrows, 0, fNrows);
  fHxyac->SetOption("boxcolz");
  fHxyac->SetFillColor(4);
  AddHistogram(fHxyac);
  
  std::string xynoise = fName + "_xVSynoise";
  fHxyn = new TH2F(xynoise.c_str(), xynoise.c_str(),fNcols, 0, fNcols, fNrows, 0, fNrows);
  fHxyn->SetOption("boxcolz");
  AddHistogram(fHxyn);
  
  name = fName + "xy_Time";
  fTime_xy = new TH2F(name, name,fNcols,0,fNcols,fNrows,0,fNrows);
  fTime_xy->SetOption("boxcolz");
  AddHistogram(fTime_xy);

  name = fName + "sum_time";
  fSum_time = new TH1F(name, name,320,0,32);
  AddHistogram(fSum_time);

if (fExpertHistos) {
  for( int x = 0; x < fNcols; x++) {
  for( int y = 0; y < fNrows; y++) {
  sprintf(cell_name,"Time_X_%d_Y_%d",x,y);
  name  = fName + cell_name;
  fCH_time[x][y] = new TH1F(name, name,32,0,32);
  AddHistogram(fCH_time[x][y]);

  sprintf(cell_name,"Ampl_X_%d_Y_%d",x,y);
  name  = fName + cell_name;
  fCH_amp[x][y] = new TH1F(name, name,1024,0,4096);
  AddHistogram(fCH_amp[x][y]);

  sprintf(cell_name,"Samp_X_%d_Y_%d",x,y);
  name  = fName + cell_name;
  fSProfile[x][y] = new TH2F(name,name,32,0,32,256,0,4096);
  fSProfile[x][y]->SetMinimum(0.0);
  AddHistogram(fSProfile[x][y]);
   }}
  }
}

void PlaneSADC::EndEvent(const CS::DaqEvent& event)
{
  if (thr_flag) TThread::Lock();
  
  //const CS::DaqEvent::Header head = event.GetHeader();
  //const CS::DaqEvent::EventType eventType = head.GetEventType();
  //const CS::uint32 Atr = head.GetTypeAttributes()[0]&0xf8000000;
  //cout<<"eventType  = "<<eventType<<endl;
  //int tt = head.GetTriggerNumber();
  //cout<<"tt= "<<tt<<endl;
  
  typedef std::list<CS::Chip::Digit*>::const_iterator DI;
  
      //  int adrmax=0;
    int hcut = 0;
    int sumcut = 0; 
    int ampmax = 0; 
    int xampmax = -1;
    int yampmax = -1;

  
  //cout << "PlaneSADC::EndEvent() lDigits.size() = " << lDigits.size() << endl;
  
  for (DI d = lDigits.begin(); d != lDigits.end(); ++d) {
    const CS::ChipSADC::Digit* sadcdig = dynamic_cast<const CS::ChipSADC::Digit*>(*d);
    if (!sadcdig) continue;
    
    const int ix = sadcdig->GetX();
    const int iy = sadcdig->GetY();
    const int row=iy;
    const int col=ix;
    const int adr=row + col*fNrows;
    
    if(ix < 0||iy<0||ix>=fNcols||iy>=fNrows) continue;
    
    const vector<short unsigned int> & samples = sadcdig->GetSamples();
    if (samples.size() < 1) continue;
    
    // find sample with max amplitude
    size_t i_max = 0;
    for (size_t i = 1; i < samples.size(); ++i) {
      if (samples[i] > samples[i_max]) i_max = i;
    }
    
    // fit max sample region with parabola to obtain time and amplitude
    // y=a*x**2 + b*x +c ;
    // x_max=-b/(2*a);
    // y_max=(4*a*c-b*b)/(4*a);
    /*
    int i1,i2,i3;
    double x1,x2,x3;
    double y1,y2,y3;
    i1=i_max-1; i2=i_max; i3=i_max + 2;
    x1=i1; x2=i2; x3=i3;
    y1=samples[i1]; y2=samples[i2]; y3=samples[i3];
    
    const double a_par = (y3-(x3*(y2-y1)+x2*y1-x1*y2)/(x2-x1))/(x3*(x3-x1-x2)+x1*x2);
    const double b_par = a_par*(x1+x2)-(y2-y1)/(x2-x1);
    const double c_par = (x1*y2-x2*y1)/(x1-x2) + a_par*x2*x1;
  
    double y_max = 0;
    double time_mean0 = 0;

    if (a_par != 0) {
      time_mean0=b_par/(2.*a_par);
      y_max=(4.0*a_par*c_par - b_par*b_par)/(4.0*a_par);
    }
    */
    
    const double time_mean0 = i_max;
    const double amp = samples[i_max];
    
    fSum_time->Fill(time_mean0);
    fTime_xy->Fill(col,row,time_mean0); 

    if (fExpertHistos) {
      fCH_time[col][row]->Fill(time_mean0);    // fill time_mean0
      fCH_amp[col][row]->Fill(amp);
      
      for (size_t i = 0; i < samples.size(); ++i)
        fSProfile[col][row]->Fill(i, samples[i]);
    }

      if (amp > ampmax)  {
        ampmax = amp;
        xampmax = col;
        yampmax = row;
      }
      
      sumcut+=amp;
      
      fHa->Fill(amp);
      
      fHch->Fill(adr);
      fHchac->Fill(adr,amp);
      fHavsadr->Fill(adr,amp);
      
      fHrc1->Fill(col,row);
      fHxyac->Fill(col,row,amp);
      fHrca->Fill(col,row,amp);
      
      fVrow->Store(row);
      fVcol->Store(col);
      fVamp->Store(amp);

      fNhitsKept++;
      fNhits++;
      hcut++;
    }
    
    if (fNhits > 0) fHsac->Fill(sumcut);
    if(ampmax>0) fHma->Fill(ampmax);
    fHhth->Fill(hcut);
    fHhit->Fill(fNhitsKept);
    fHxyn->Fill(xampmax,yampmax);
  
  if (thr_flag) TThread::UnLock();
}

void PlaneSADC::StoreDigit(CS::Chip::Digit* digit)
{
  lDigits.push_back(digit);
}
