#include "PlaneMM.h"
#include "ChipAPV.h"

#include "p348reco.h"

ClassImp(PlaneMM);

// predefined designs for calibration
static MM_plane_calib Design2016;
static MM_plane_calib Design2021Small;
static MM_plane_calib Design2021Large;
static MM_plane_calib Design2021Large5;
static MM_plane_calib Design2021Large6;
static MM_plane_calib Design2021Large7;

PlaneMM::PlaneMM(const char *detname, int nchan)
: PlaneAPV(detname,nchan,0,0)
{
  fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT};
  
  // init designs options only once
  if (Design2016.MM_Nchannels == 0) Design2016.SetDesign2016();
  if (Design2021Small.MM_Nchannels == 0) Design2021Small.SetDesign2021Small();
  if (Design2021Large.MM_Nchannels == 0) Design2021Large.SetDesign2021Large();
  
  if (Design2021Large5.MM_Nchannels == 0) {
    Design2021Large5.SetDesign2021Large();
    Design2021Large5.insertBadWire(83);
    Design2021Large5.insertBadWire(124);
  }
  if (Design2021Large6.MM_Nchannels == 0) {
    Design2021Large6.SetDesign2021Large();
    Design2021Large6.insertBadWire(49);
    Design2021Large6.insertBadWire(157);
  }
  if (Design2021Large7.MM_Nchannels == 0) {
    Design2021Large7.SetDesign2021Large();
    Design2021Large7.insertBadWire(126);
    Design2021Large7.insertBadWire(127);
    Design2021Large7.insertBadWire(191);
  }

  // TODO: add run number dependency for previous years
  // TODO: propagate settings from conddb
}

void PlaneMM::Init(TTree* tree)
{
  PlaneAPV::Init(tree);

  TString name;

  // Single channel occupancies
  name = fName + "_wire_occupancies";
  fHoccup = new TH1F_Ref(name, name + ";wire;occupancy", fVch->GetNbins(), fVch->GetMin(), fVch->GetMax(),fRateCounter,true);
  //fHoccup->SetNormFactor(fVch->GetNbins());
  fHoccup->SetStats(false);
  ((TH1F_Ref*)fHoccup)->SetReference(fReferenceDirectory);
  AddHistogram(fHoccup);

  // Channel vs strip amplitude - A0
  name = fName + "_wire_vs_a0";
  fHchvsa0 = new TH2F(name, name + ";wire;a0", fNchan, 0, fNchan, 200, 0, 400);
  fHchvsa0->SetOption("COLZ");
  AddHistogram(fHchvsa0);
  
  // mean amplitude - A0
  name = fName + "_wire_vs_a0-mean";
  fHmeanAmpa0 = new TProfile(name, name + ";wire;mean a0", fVch->GetNbins(), fVch->GetMin(), fVch->GetMax());
  fHmeanAmpa0->SetStats(false);
  AddHistogram(fHmeanAmpa0);

  // Channel vs strip amplitude - A1
  name = fName + "_wire_vs_a1";
  fHchvsa1 = new TH2F(name, name + ";wire;a1", fNchan, 0, fNchan, 200, 0, 400);
  fHchvsa1->SetOption("COLZ");
  AddHistogram(fHchvsa1);
  
  // mean amplitude - A1
  name = fName + "_wire_vs_a1-mean";
  fHmeanAmpa1 = new TProfile(name, name + ";wire;mean a1", fVch->GetNbins(), fVch->GetMin(), fVch->GetMax());
  fHmeanAmpa1->SetStats(false);
  AddHistogram(fHmeanAmpa1);

  // Channel vs strip amplitude - A2
  name = fName + "_wire_vs_a2";
  fHchvsa2 = new TH2F(name, name + ";wire;a2", fNchan, 0, fNchan, 200, 0, 400);
  fHchvsa2->SetOption("COLZ");
  AddHistogram(fHchvsa2);
  
  // mean amplitude - A2
  name = fName + "_wire_vs_a2-mean";
  fHmeanAmpa2 = new TProfile(name, name + ";wire;mean a2", fVch->GetNbins(), fVch->GetMin(), fVch->GetMax());
  fHmeanAmpa2->SetStats(false);
  AddHistogram(fHmeanAmpa2);
  
  // Channel amplitude ratio ("banana") - All
  name = fName + "_AmpRatio";
  fHampRatio = new TH2F(name, name + ";a1/a2;a0/a2", 100, -0.01, 1.99, 100, -0.01, 1.99);
  fHampRatio->SetOption("COLZ");
  AddHistogram(fHampRatio);
  
  // cluster variable
  if (fNchan == 192) fVcPos  = AddVariable("Clu_Position", 960, 0, 960, fNchan);
  else fVcPos  = AddVariable("Clu_Position", 320, 0, 320, fNchan);
  fVcAmp  = AddVariable("Clu_TotCharge", 1000,    0., 4000., fNchan);
  fVcSize = AddVariable("Clu_Size",   30,  0,  30, fNchan);

  // Book histograms for clusters data
  // Number of clusters
  name = fName + "_Clu_Multiplicity";
  fHcHits = new TH1F(name, name + ";#clusters", 20, 0, 20);
  fHcHits->SetOption("HIST TEXT0");
  fHcHits->SetNdivisions(20, "X");
  fHcHits->GetXaxis()->CenterLabels();
  AddHistogram(fHcHits);

  // Cluster position
  name = fName + "_" + fVcPos->GetName();
  fHcPos = new TH1F_Ref(name, name+";wire", fVcPos->GetNbins(), fVcPos->GetMin(), fVcPos->GetMax(), fRateCounter);
  AddHistogram(fHcPos);

  // Cluster amplitude
  name = fName + "_" + fVcAmp->GetName();
  fHcAmp = new TH1F(name, name, fVcAmp->GetNbins(), fVcAmp->GetMin(), fVcAmp->GetMax());
  AddHistogram(fHcAmp);

  // Cluster size
  name = fName + "_" + fVcSize->GetName();
  fHcSize = new TH1F(name, name + ";#strips", fVcSize->GetNbins(), fVcSize->GetMin(), fVcSize->GetMax());
  AddHistogram(fHcSize);

  // mean amplitude per cluster
  name = fName + "_Clu_MeanAmp";
  fHcMeanAmp = new TProfile(name, name, fVcPos->GetNbins(), fVcPos->GetMin(), fVcPos->GetMax());
  //fHcMeanAmp->SetStats(false);
  AddHistogram(fHcMeanAmp);
  
  // best cluster histograms
  // Cluster position
  name = fName + "_Best" + fVcPos->GetName();
  fHbcPos = new TH1F(name, name, fVcPos->GetNbins(), fVcPos->GetMin(), fVcPos->GetMax());
  AddHistogram(fHbcPos);

  // Cluster amplitude
  name = fName + "_Best" + fVcAmp->GetName();
  fHbcAmp = new TH1F(name, name, fVcAmp->GetNbins(), fVcAmp->GetMin(), fVcAmp->GetMax());
  AddHistogram(fHbcAmp);

  // Cluster size
  name = fName + "_Best" + fVcSize->GetName();
  fHbcSize = new TH1F(name, name + ";#strips", fVcSize->GetNbins(), fVcSize->GetMin(), fVcSize->GetMax());
  AddHistogram(fHbcSize);

  // mean amplitude per cluster
  name = fName + "_BestClu_MeanAmp";
  fHbcMeanAmp = new TProfile(name, name, fVcPos->GetNbins(), fVcPos->GetMin(), fVcPos->GetMax());
  //fHbcMeanAmp->SetStats(false);
  AddHistogram(fHbcMeanAmp);

  // Channel amplitude ratio ("banana") - Best cluster
  name = fName + "_BestClu_AmpRatio";
  fHbcAmpRatio = new TH2F(name, name + ";a1/a2;a0/a2", 100, -0.01, 1.99, 100, -0.01, 1.99);
  fHbcAmpRatio->SetOption("COLZ");
  AddHistogram(fHbcAmpRatio);
}

void PlaneMM::Reset()
{
  PlaneAPV::Reset();
  fDigits.clear();
  
  has_hit = false;
  hit_position = -1.;
}

void PlaneMM::EndEvent(const CS::DaqEvent& event)
{
  if (thr_flag) TThread::Lock();

  // save number of hits
  fNhitsKept = 0;

  // loop over digits
  for (std::list<CS::Chip::Digit*>::const_iterator i = lDigits.begin(); i != lDigits.end(); i++) {
    const CS::ChipAPV::Digit* apv = dynamic_cast<const CS::ChipAPV::Digit*>(*i);
    if (!apv) continue;
    
    const int channel = apv->GetChannel();
    const CS::uint32* raw_q = apv->GetAmplitude();
    
    const double a0      = raw_q[0];
    const double a1      = raw_q[1];
    const double a2      = raw_q[2];
    const double a02     = (a2>0.) ? a0/a2 : 0.;
    const double a12     = (a2>0.) ? a1/a2 : 0.;
    
    // store data into variables
    fVch->Store(channel);
    fVa0->Store(a0);
    fVa1->Store(a1);
    fVa2->Store(a2);

    // save into histogram
    fHch->Fill(channel);
    //fHoccup->Fill(channel);
    fHmeanAmpa0->Fill(channel, a0);
    fHmeanAmpa1->Fill(channel, a1);
    fHmeanAmpa2->Fill(channel, a2);
    fHa0->Fill(a0);
    fHa1->Fill(a1);
    fHa2->Fill(a2);
    fHchvsa0->Fill(channel, a0);
    fHchvsa1->Fill(channel, a1);
    fHchvsa2->Fill(channel, a2);

    fHampRatio->Fill(a12, a02);

    fNhitsKept++;
  }
  
  fHhits->Fill(fNhitsKept);

  if (fRateCounter % 25 == 0) {
    fHoccup->Reset("ICE");
    fHoccup->Add(fHch, 1/double(fRateCounter));
    //fHoccup->SetNormFactor(1/double(fRateCounter));
  }

  if (thr_flag) TThread::UnLock();
}

void PlaneMM::Clusterize()
{
  if (thr_flag) TThread::Lock();
  
  // Do clustering
  MicromegaPlane plane;
  
  const string mm = fName; // current plane name
  
  // set calibration data for proper mapping
  // TODO: add run number dependency for previous years
  // TODO: propagate settings from conddb
  
#if 0
  // 2021A session
  // NOTE: up to run 4738 the MM2 was in design 2021Small and MM5 was in design 2016
       if (mm == "MM01X" || mm == "MM01Y") plane.Init(Design2021Small);
  else if (mm == "MM02X" || mm == "MM02Y") plane.Init(Design2016);
  else if (mm == "MM03X" || mm == "MM03Y") plane.Init(Design2016);
  else if (mm == "MM04X" || mm == "MM04Y") plane.Init(Design2016);
  else if (mm == "MM05X" || mm == "MM05Y") plane.Init(Design2021Small);
  else if (mm == "MM06X" || mm == "MM06Y") plane.Init(Design2016);
  else {
    std::cerr << "WARNING: name : " << mm << " not in option, setting Design2016" << std::endl;
    plane.Init(Design2016);
  }
#endif

#if 0
  // 2021B and 2022A sessions (muon mode)
       if (mm == "MM01X" || mm == "MM01Y") plane.Init(Design2016);
  else if (mm == "MM02X" || mm == "MM02Y") plane.Init(Design2016);
  else if (mm == "MM03X" || mm == "MM03Y") plane.Init(Design2021Small);
  else if (mm == "MM04X" || mm == "MM04Y") plane.Init(Design2016);
  else if (mm == "MM05X") plane.Init(Design2021Large);
  else if (mm == "MM05Y") plane.Init(Design2021Small);
  else if (mm == "MM06X") plane.Init(Design2021Large);
  else if (mm == "MM06Y") plane.Init(Design2021Small);
  else if (mm == "MM07X") plane.Init(Design2021Large);
  else if (mm == "MM07Y") plane.Init(Design2021Small);
  else {
    std::cerr << "WARNING: name : " << mm << " not in option, setting Design2016" << std::endl;
    plane.Init(Design2016);
  }
#endif

#if 0
  // 2022B and 2023A sessions (electron mode)
       if (mm == "MM01X" || mm == "MM01Y") plane.Init(Design2021Small);
  else if (mm == "MM02X" || mm == "MM02Y") plane.Init(Design2016);
  //else if (mm == "MM02X" || mm == "MM02Y") plane.Init(Design2021Small); // up to run 6137
  else if (mm == "MM03X" || mm == "MM03Y") plane.Init(Design2016);
  else if (mm == "MM04X" || mm == "MM04Y") plane.Init(Design2016);
  else if (mm == "MM05X") plane.Init(Design2021Large);
  else if (mm == "MM05Y") plane.Init(Design2021Small);
  else if (mm == "MM06X") plane.Init(Design2021Large);
  else if (mm == "MM06Y") plane.Init(Design2021Small);
  else if (mm == "MM07X") plane.Init(Design2021Large);
  else if (mm == "MM07Y") plane.Init(Design2021Small);
  else {
    std::cerr << "WARNING: name : " << mm << " not in option, setting Design2016" << std::endl;
    plane.Init(Design2016);
  }
#endif
#if 0
  // 2023B session (muon mode)
       if (mm == "MM01X" || mm == "MM01Y") plane.Init(Design2021Small);
  else if (mm == "MM02X" || mm == "MM02Y") plane.Init(Design2016);
  //else if (mm == "MM03X" || mm == "MM03Y") plane.Init(Design2021Small); // up to run 9885
  else if (mm == "MM03X" || mm == "MM03Y") plane.Init(Design2016);
  else if (mm == "MM04X" || mm == "MM04Y") plane.Init(Design2016);
  else if (mm == "MM05X") plane.Init(Design2021Large5);
  else if (mm == "MM05Y") plane.Init(Design2021Small);
  else if (mm == "MM06X") plane.Init(Design2021Large6);
  else if (mm == "MM06Y") plane.Init(Design2021Small);
  else if (mm == "MM07X") plane.Init(Design2021Large7);
  else if (mm == "MM07Y") plane.Init(Design2021Small);
  else if (mm == "MM08X" || mm == "MM08Y") plane.Init(Design2016);
  //else if (mm == "MM09X" || mm == "MM09Y") plane.Init(Design2016); // up to run 9885
  else if (mm == "MM09X" || mm == "MM09Y") plane.Init(Design2021Small);
  else if (mm == "MM10X" || mm == "MM10Y") plane.Init(Design2016);
  else if (mm == "MM11X" || mm == "MM11Y") plane.Init(Design2016);
  else {
    std::cerr << "WARNING: name : " << mm << " not in option, setting Design2016" << std::endl;
    plane.Init(Design2016);
  }
#endif

// 2024A sessions (electron mode)
       if (mm == "MM01X" || mm == "MM01Y") plane.Init(Design2021Small);
  else if (mm == "MM02X" || mm == "MM02Y") plane.Init(Design2016);
  else if (mm == "MM03X" || mm == "MM03Y") plane.Init(Design2016);
  else if (mm == "MM04X" || mm == "MM04Y") plane.Init(Design2016);
  else if (mm == "MM05X") plane.Init(Design2021Large);
  else if (mm == "MM05Y") plane.Init(Design2021Small);
  else if (mm == "MM06X") plane.Init(Design2021Large);
  else if (mm == "MM06Y") plane.Init(Design2021Small);
  else if (mm == "MM07X") plane.Init(Design2021Large);
  else if (mm == "MM07Y") plane.Init(Design2021Small);
  else {
    std::cerr << "WARNING: name : " << mm << " not in option, setting Design2016" << std::endl;
    plane.Init(Design2016);
  }

  // loop over digits
  for (std::list<CS::Chip::Digit*>::const_iterator i = lDigits.begin(); i != lDigits.end(); i++) {
    const CS::ChipAPV::Digit* apv = dynamic_cast<const CS::ChipAPV::Digit*>(*i);
    if (!apv) continue;
    
    const CS::uint16 wire = apv->GetChannel();
    const CS::uint32* raw_q = apv->GetAmplitude();
    
    DoHitAccumulation(wire, raw_q, plane);
  }
  
  plane.DoMMClustering();
  
  // save number of clusters (before and after time cut)
  unsigned int NrClu = 0;

  // this is were the real important stuff happens - cluster handling
  for (size_t i = 0; i < plane.clusters.size(); ++i) {
    // get data from clusters
    const double strip = plane.clusters[i].position();
    const double amp   = plane.clusters[i].charge_total;
    const int size     = plane.clusters[i].size();

    // store in variables
    fVcPos->Store(strip);
    fVcAmp->Store(amp);
    fVcSize->Store(size);

    if(!fVcPos->Test(strip)) continue;
    if(!fVcAmp->Test(amp)) continue;
    if(!fVcSize->Test(size)) continue;
	
    // fill histograms
    fHcPos->Fill(strip);
    fHcAmp->Fill(amp);
    fHcSize->Fill(size);
    fHcMeanAmp->Fill(strip, amp);

    // increase number of clusters
    NrClu++;
  }
  
  fHcHits ->Fill(NrClu);
  
  if (needRateHistoUpdate()) {
  //compute efficiency
  const double zeroclus = fHcHits->GetBinContent(1); // num.entries for Nclusters==0
  const double oneplus = fHcHits->Integral(2, 1000); // num.entries for Nclusters>0
  const double eff = oneplus / (zeroclus + oneplus + 1);
  const TString eff_str = Form("%s eff (Nclus > 0) = %.3f", fName.c_str(), eff);
  fHcHits->SetTitle(eff_str);
  }
  
  if (plane.hasHit()) {
    const MMCluster& bc = plane.clusters[plane.bestClusterIndex()];
    const double strip = bc.position();
    const double amp   = bc.charge_total;
    const int size     = bc.size();
    const MM_plane_calib& calib = *plane.calib;
    // Loop over strips in best cluster
    for (auto bc_strip : bc.strips) {
      // Loop over all digits
      bool isMatch = false;
      if (bc_strip.charge == 0.) continue;
      for (std::list<CS::Chip::Digit*>::const_iterator i = lDigits.begin(); i != lDigits.end(); i++) {
        const CS::ChipAPV::Digit* apv = dynamic_cast<const CS::ChipAPV::Digit*>(*i);
        if (!apv) continue;
        const int chan = apv->GetChannel();
        // Loop over possible strips matching channel-strip pair
        for(size_t j = 0; j < calib.MM_map.at(chan).size(); j++) 
          if (bc_strip.bin == calib.MM_map.at(chan)[j]) isMatch = true;
        if (!isMatch) continue;
        const CS::uint32* raw_q = apv->GetAmplitude();
        const double a0      = raw_q[0];
        const double a1      = raw_q[1];
        const double a2      = raw_q[2];
        const double a02     = (a2>0.) ? a0/a2 : 0.;
        const double a12     = (a2>0.) ? a1/a2 : 0.;
        // Fill AmpRatio histogram
        if (a12 != 0. && a02 != 0.) fHbcAmpRatio->Fill(a12, a02);
        break;
      }
    }
    
    // fill histograms
    fHbcPos->Fill(strip);
    fHbcAmp->Fill(amp);
    fHbcSize->Fill(size);
    fHbcMeanAmp->Fill(strip, amp);
    
    hit_position=strip;
  }
  
  has_hit = plane.hasHit();
  
  if (thr_flag) TThread::UnLock();
}
