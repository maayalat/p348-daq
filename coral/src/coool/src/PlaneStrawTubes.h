#ifndef __PlaneStrawTubes__
#define __PlaneStrawTubes__

#include "Plane.h"

#include "TTree.h"

/*! \brief Plane for Straw Tubes detectors
    \todo solve the problem in Monitor
    \author Alexander Zvyagin and VV and PV
*/

class PlaneStrawTubes : public  Plane
{
  public:

    PlaneStrawTubes         (const char* detname, int nchan, int center, int width);
    void                Init                    (TTree* tree = 0);

    double rtStraw(int time); 

////PlaneStrawTubes         (const char* detname);
////void                Init                    (void);


    TH1F*               fChannels;              // hits profile
    TH1F*               fChannels_rt;              // hits profile
    TH1F*               fmultiplicity;              // hits profile
    TH1F*               fmultiplicity26;              // hits profile
    TH1F*               fmultiplicity27;              // hits profile
    TH1F*               fmultiplicity28;              // hits profile
    TH1F*               fmultiplicity64;              // hits profile
    TH1F*               fRates;                 // rates profile
    TH1F*               fHits;                  // hits multiplicity
    TH1F*               fTime;                  //
    TH1F*               fTime1;                  //
    TH1F*               fTime2;                  //
    TH1F*               fTime6mm;               //
    TH1F*               fTime10mm;              //
      TH1F*               fTime25;              //
    TH1F*               fTime22;              //
    TH1F*               fTime23;              //
    TH1F*               fTime24;              //
    TH1F*               fTime26;              //
    TH1F*               fTime27;              //
    TH1F*               fTime28;              //
    TH1F*               fTime29;              //
    TH1F*               fTime30;              //
    TH1F*               fTime31;              //
    TH1F*               fTime33;              //

    //TH1F*               fTime41;              //
    TH1F*               fTime64;              //
    TH1F*               fTCSPhase;             //

    
    TH2F* 		fChannelDoubleClusters;	        //channel Cluster
    TH2F* 		fChannelCluster4;	        //channel Cluster
    TH2F* 		fChannelCluster7;	        //channel Cluster
    TH2F* 		fChannelCluster41;	        //channel Cluster
    TH2F* 		fChannelCluster38;	        //channel Cluster
    TH2F* 		fChannelCluster39;	        //channel Cluster
    TH2F* 		fChannelCluster40;	        //channel Cluster
    TH2F* 		fChannelCluster32;	        //channel Cluster
    TH2F* 		fChannelCluster33;	        //channel Cluster
    TH2F* 		fChannelCluster37;	        //channel Cluster
    TH2F* 		fSpillTime;	        //channel Cluster
    TH2F* 		fSpillMean;	        //channel Cluster
    int 		bias;
    int                 nBin;      
    bool		isBig;	
    bool		isSTTO;	
    TProfile* histo_vs_spill;

//    uint32_t event_number;
    bool end_processing;
  private:
   
    #if !defined(__CINT__) && !defined(__CLING__)
    void                StoreDigit(CS::Chip::Digit* digit);
  	
    void                EndEvent                (const CS::DaqEvent &event);
    #endif

  ClassDef(PlaneStrawTubes,2)
};

#endif

