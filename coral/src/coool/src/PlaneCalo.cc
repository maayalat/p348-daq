#include "PlaneCalo.h"

#include "PlanePanel.h"


#include "DaqEvent.h"

#include "TThread.h"

#include <iostream>

using namespace std;

ClassImp(PlaneCalo);

PlaneCalo::PlaneCalo(const char* name_, int nx, int ny,map<pair<int,int>,bool> isWB, int minamp, int maxamp)
: Plane(name_), fNy(ny), fNx(nx), fNch(nx*ny), lastspill(0)
{
  fAcceptedEventTypes = {CS::DaqEvent::PHYSICS_EVENT, CS::DaqEvent::CALIBRATION_EVENT};
  
  hits.reserve(fNch);
  
  TString name;
  
  name = fName + "_y";
  fVy = new Variable(name, fNy, 0, fNy, fNch);
  AddVariable(fVy);
  
  name = fName + "_x";
  fVx = new Variable(name, fNx, 0, fNx, fNch);
  AddVariable(fVx);
  
  //A.C. these variables are common for all channels, so I set the min and max to the values compatible with both MSADC and WB
  name = fName + "_amp";
  fVamp = new Variable(name, 500, minamp, maxamp, fNch);
  AddVariable(fVamp);
  
  name = fName + "_time";
  fVtime = new Variable(name, 256, 0, 256, fNch); //A.C. modified for WB from 32 to 256
  AddVariable(fVtime);
  for (int x = 0; x < fNx; x++){
    for (int y = 0; y < fNy; y++) {
      const auto idx = make_pair(x,y);
      // ensure the item exists in `isWB[]`, otherwise fallback to MSADC parameters
      const bool setWB = isWB[idx]; //A.C. by C++ standard, the [] operator will insert a new object in map at key if key does not exist, with default constructor (for bool, it means false)
      histoParams MSADC,WB;
      MSADC.maxSample=32;
      MSADC.maxADC=4096;
      MSADC.maxAmpl=MSADC.maxADC;
      MSADC.binsADC_Ampl=1024;
      MSADC.binsADC_RawShape=256;
      MSADC.binsSample=64;

      WB.maxSample=256;
      WB.maxADC=16384;
      WB.maxAmpl=4096;
      WB.binsADC_Ampl=1024;
      WB.binsADC_RawShape=512;
      WB.binsSample=256;

      histosParams[idx] = setWB ? WB : MSADC;
    }
  }
}

void PlaneCalo::SetCenterLabels(TH2* h)
{
  h->SetNdivisions(fNx, "X");
  h->SetNdivisions(fNy, "Y");
  h->GetXaxis()->CenterLabels();
  h->GetYaxis()->CenterLabels();
}

TNamed* CloneWithPrefix(TNamed* orig, const TString prefix)
{
  TNamed* clone = (TNamed*)orig->Clone(prefix + orig->GetName());
  clone->SetTitle(prefix + orig->GetTitle());
  return clone;
}

void PlaneCalo::Init(TTree* tree) {
  //cout << "PlaneCalo::Init() fName = " << fName << endl;
  
  TString name;
  
  // hits multiplicity
  TString hitsname = fName + "_hits";
  hHits = new TH1F_Ref(hitsname, hitsname + ";hits", fNch + 1, 0, fNch + 1, fRateCounterPhysics);
  ((TH1F_Ref*)hHits)->SetReference(fReferenceDirectory);
  AddHistogram(hHits);
  
  name = fName + "_hits_nocuts";
  hHits_nc = new TH1F_Ref(name, name + ";hits", fNch + 1, 0, fNch + 1, fRateCounterPhysics);
  ((TH1F_Ref*)hHits_nc)->SetReference(fReferenceDirectory);
  AddHistogram(hHits_nc);
  
  // hits vs XY
  name = fName + "_hits_xy";
  fHrc1=new TH2F(name, name + ";x;y",
		 fVx->GetNbins(), fVx->GetMin(), fVx->GetMax(),
		 fVy->GetNbins(), fVy->GetMin(), fVy->GetMax());
  fHrc1->SetOption("LEGO2 0");
  SetCenterLabels(fHrc1);
  fHrc1->SetMinimum(0);
  AddHistogram(fHrc1);
  
  // hits vs channel
  name = fName + "_hits_channel";
  fHch = new TH1F(name, name + ";channel", fNch, 0, fNch);
  fHch->SetFillColor(8);
  fHch->SetMinimum(0);
  AddHistogram(fHch);
  
  // amplitude
  TString ampname = fVamp->GetName();
  fHa=new TH1F_Ref(ampname,ampname + ";amplitude",
	       fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax(), fRateCounterPhysics);
  ((TH1F_Ref*)fHa)->SetReference(fReferenceDirectory);
  AddHistogram(fHa);
  
  // amplitude vs channel
  name = fName + "_amp_channel";
  fH_amp_channel = new TH2F(name, name + ";channel;amplitude",
		fNch, 0, fNch,
		fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
  AddHistogram(fH_amp_channel);
  
  // hit position (center of mass)
  name = fName + "_hit_position";
  fHitPos = new TH2I(name, name + ";x;y;events",
		 50*fVx->GetNbins(), fVx->GetMin(), fVx->GetMax(),
		 50*fVy->GetNbins(), fVy->GetMin(), fVy->GetMax());
  fHitPos->SetOption("COL");
  AddHistogram(fHitPos);
  
  // hit position of last events (center of mass)
  name = fName + "_hit_position_last" + TString::Format("%d", LASTSIZE);
  fHitPosLast = new TH2I(name, name + ";x;y;events",
		 20*fVx->GetNbins(), fVx->GetMin(), fVx->GetMax(),
		 20*fVy->GetNbins(), fVy->GetMin(), fVy->GetMax());
  fHitPosLast->SetOption("BOX COLZ");
  AddHistogram(fHitPosLast);
  
    // hit position vs. spill plots
    name = fName + TString("_hit_position_X_vs_spill");
    TProfile_Ref* p1 = new TProfile_Ref(name, name+";spill;X mean #pm stddev,cell", 200, 0, 200);
    p1->SetReference(fReferenceDirectory);
    p1->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p1->SetMaximum(fVx->GetMax());
    p1->SetMarkerStyle(kFullDotMedium);
    //p1->SetOption("P");
    fHitPosX_vs_spill = p1;
    AddHistogram(fHitPosX_vs_spill);
    
    name = fName + TString("_hit_position_Y_vs_spill");
    TProfile_Ref* p2 = new TProfile_Ref(name, name+";spill;Y mean #pm stddev,cell", 200, 0, 200);
    p2->SetReference(fReferenceDirectory);
    p2->BuildOptions(0, 0, "S"); // "S" = error bars are stddev
    p2->SetMaximum(fVy->GetMax());
    p2->SetMarkerStyle(kFullDotMedium);
    //p2->SetOption("P");
    fHitPosY_vs_spill = p2;
    AddHistogram(fHitPosY_vs_spill);
    
  // mean amplitude vs XY
  name = fName + "_meanamp_xy";
  fH_meanamp_xy = new TProfile2D(name, name + ";x;y;mean amplitude",
		 fVx->GetNbins(), fVx->GetMin(), fVx->GetMax(),
		 fVy->GetNbins(), fVy->GetMin(), fVy->GetMax(), "S");
  fH_meanamp_xy->SetOption("LEGO2 0 FB BB");
  SetCenterLabels(fH_meanamp_xy);
  fH_meanamp_xy->SetMinimum(0);
  AddHistogram(fH_meanamp_xy);
  
  // mean amplitude vs channel
  name = fName + "_meanamp_channel";
  fH_meanamp_ch = new TProfile(name, name + ";channel;mean amplitude",
		fNch, 0, fNch, "S");
  fH_meanamp_ch->SetMarkerStyle(kFullDotMedium);
  //fH_meanamp_ch->SetMarkerColor(kBlue);
  AddHistogram(fH_meanamp_ch);

  if(tree) {
    const TString leavlist0 =fVy->GetName()  + "[" + hitsname + "]/F";
    const TString leavlist1 =fVx->GetName()  + "[" + hitsname + "]/F";
    const TString leavlist2 =fVamp->GetName()  + "[" + hitsname + "]/F";
    
    fIsInTree = true;
    tree->Branch(hitsname,&fNhitsKept, hitsname + "/I",32000);
    tree->Branch(fVy->GetName().c_str(),fVy->GetValues(), leavlist0,32000);
    tree->Branch(fVx->GetName().c_str(),fVx->GetValues(), leavlist1,32000);
    tree->Branch(fVamp->GetName().c_str(),fVamp->GetValues(), leavlist2,32000);
  }
  
  name = fName + "_sumamp";
  hSumamp = new TH1F_Ref(name, name + ";sum of amplitudes",
                   fVamp->GetNbins(), fVamp->GetMin(), 2.0*fVamp->GetMax(), fRateCounterPhysics);
  hSumamp->SetFillColor(8);
  hSumamp->SetFillStyle(3001);
  AddHistogram(hSumamp);
  
  name = fName + "_sumamp_nocuts";
  hSumamp_nc = new TH1F(name, name + ";sum of amplitudes",
                   fVamp->GetNbins(), fVamp->GetMin(), 1.5*fVamp->GetMax());
  hSumamp_nc->SetFillColor(8);
  AddHistogram(hSumamp_nc);
  
  name = fName + "_maxamp";
  hMaxamp = new TH1F(name, name + ";max amplitude",
                  fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
  hMaxamp->SetFillColor(5);
  AddHistogram(hMaxamp);
  
  name = fName + "_maxamp_nocuts";
  hMaxamp_nc = new TH1F(name, name + ";max amplitude",
                  fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
  hMaxamp_nc->SetFillColor(5);
  AddHistogram(hMaxamp_nc);
  
  name = fName + "_noise_xy";
  fHxyn = new TH2F(name, name + " {maxamp < ampcut};x;y",
                   fNx, 0, fNx, fNy, 0, fNy);
  fHxyn->SetOption("LEGO2 0");
  SetCenterLabels(fHxyn);
  AddHistogram(fHxyn);
  
  name = fName + "_time";
  fTime = new TH1F(name,name + ";t0",64,0,32);
  AddHistogram(fTime);
  
  name = fName + "_amp_time";
  fAmpTime = new TH2I(name, name + ";t0;amplitude",
    32, 0, 32,
    fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
  AddHistogram(fAmpTime);
  
  name = fName + "_meantime_xy";
  fTime_xy = new TProfile2D(name, name + ";x;y;mean t0", fNx,0,fNx, fNy,0,fNy);
  fTime_xy->SetOption("LEGO2 0");
  SetCenterLabels(fTime_xy);
  fTime_xy->SetMinimum(0);
  AddHistogram(fTime_xy);
  
  name = fName + "_meantime_channel";
  fTime_ch = new TProfile(name, name + ";channel;mean t0", fNch,0,fNch, "S");
  fTime_ch->SetMarkerStyle(kFullDotMedium);
  //fTime_ch->SetMarkerColor(kBlue);
  fTime_ch->SetMinimum(0);
  AddHistogram(fTime_ch);
  
  // pedestals
  const double MaxPed = 1000;
  name = fName + "_Ped";
  fPed_sum = new TH1F(name,name+";pedestal",200,0,MaxPed);
  AddHistogram(fPed_sum);
  
  name = fName + "_Ped_channel";
  fPed_ch = new TH2F(name, name + ";channel;pedestal", fNch,0,fNch, 200,0,MaxPed);
  fPed_ch->SetOption("boxcol");
  fPed_ch->SetMinimum(0);
  AddHistogram(fPed_ch);
  
  name = fName + "_meanPed_xy";
  fPed_xy = new TProfile2D(name,name + ";x;y;mean pedestal",fNx,0,fNx,fNy,0,fNy, "S");
  fPed_xy->SetOption("LEGO2 0");
  SetCenterLabels(fPed_xy);
  fPed_xy->SetMinimum(0);
  AddHistogram(fPed_xy);
  
  name = fName + "_meanPed_channel";
  fmeanPed_ch = new TProfile(name, name + ";channel;mean pedestal", fNch,0,fNch, "S");
  fmeanPed_ch->SetMarkerStyle(kFullDotMedium);
  //fmeanPed_ch->SetMarkerColor(kBlue);
  fmeanPed_ch->SetMinimum(0);
  AddHistogram(fmeanPed_ch);
  
  if (fExpertHistos) {
    //name = fName + "_Bad";
    //fBad = new TH1F(name,name, fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
    //AddHistogram(fBad);
    
    //name = fName + "_Good";
    //fGood = new TH1F(name,name, fVamp->GetNbins(), fVamp->GetMin(), fVamp->GetMax());
    //AddHistogram(fGood);
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        auto histoParams=histosParams[make_pair(x,y)];
        name = fName + TString::Format("_X%d_Y%d_Ampl", x, y);
        TH1F_Ref* h = new TH1F_Ref(name, name + ";amplitude",histoParams.binsADC_Ampl,0,histoParams.maxAmpl, fRateCounterPhysics);
        h->SetReference(fReferenceDirectory);
        fCH_amp[x][y] = h;
        AddHistogram(fCH_amp[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        auto histoParams=histosParams[make_pair(x,y)];
        name = fName + TString::Format("_X%d_Y%d_Time", x, y);
        fCH_time[x][y] = new TH1F(name, name + ";time",histoParams.binsSample,0,histoParams.maxSample);
        AddHistogram(fCH_time[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        auto histoParams=histosParams[make_pair(x,y)];
        name = fName + TString::Format("_X%d_Y%d_RawShape", x, y);
        fCH_rawshape[x][y] = new TH2F(name,name + ";time sample;ADC counts",histoParams.maxSample,0,histoParams.maxSample,histoParams.binsADC_RawShape,0,histoParams.maxADC);
        fCH_rawshape[x][y]->SetMinimum(0.0);  
        AddHistogram(fCH_rawshape[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        auto histoParams=histosParams[make_pair(x,y)];
        name = fName + TString::Format("_X%d_Y%d_Profile", x, y);
        fCH_prof[x][y] = new TProfile(name, name + ";time sample;mean amplitude", histoParams.maxSample, 0, histoParams.maxSample, "S");
        fCH_prof[x][y]->SetMarkerStyle(kFullDotMedium);
        //fCH_prof[x][y]->SetMarkerColor(kBlue);
        AddHistogram(fCH_prof[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        name = fName + TString::Format("_X%d_Y%d_vs_spill", x, y);
        fCH_vs_spill[x][y] = new TProfile(name, name + ";spill;amplitude mean #pm stddev", 200, 0, 200, "S");
        fCH_vs_spill[x][y]->SetMarkerStyle(kFullDotMedium);
        //fCH_vs_spill[x][y]->SetMarkerColor(kBlue);
        AddHistogram(fCH_vs_spill[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        name = fName + TString::Format("_X%d_Y%d_vs_event", x, y);
        fCH_vs_event[x][y] = new TProfile(name, name + ";event in spill;amplitude mean #pm stddev", 200, 0, 10000, "S");
        fCH_vs_event[x][y]->SetMarkerStyle(kFullDotMedium);
        //fCH_vs_event[x][y]->SetMarkerColor(kBlue);
        fCH_vs_event[x][y]->SetOption("P");;
        AddHistogram(fCH_vs_event[x][y]);
      }
  }
  
  // === create separate sets of histograms for various event/trigger type
  BookGroupHistograms(led, "LED:");
  BookGroupHistograms(led0, "LED0:");
  BookGroupHistograms(led1, "LED1:");
  BookGroupHistograms(phys, "PHYS:");
  BookGroupHistograms(beam, "BEAM:");
  BookGroupHistograms(rnd, "RND:");
}

void PlaneCalo::BookGroupHistograms(HistoGroup& grp, TString name)
{  
  grp.name = name;
  grp.hSumamp = (TH1F*)CloneWithPrefix(hSumamp, grp.name);
  AddHistogram(grp.hSumamp);
  grp.fH_meanamp_xy = (TProfile2D*)CloneWithPrefix(fH_meanamp_xy, grp.name);
  AddHistogram(grp.fH_meanamp_xy);
  grp.fH_meanamp_ch = (TProfile*)CloneWithPrefix(fH_meanamp_ch, grp.name);
  AddHistogram(grp.fH_meanamp_ch);
  grp.fH_amp_channel = (TH2F*)CloneWithPrefix(fH_amp_channel, grp.name);
  AddHistogram(grp.fH_amp_channel);

  if (fExpertHistos) {
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        // TODO: simple clone do not preserve original 'TH1F_Ref' type
        //       and so, no reference line is displayed
        //grp.fCH_amp[x][y] = (TH1F*)CloneWithPrefix(fCH_amp[x][y], grp.name);
        
        const TH1F* o = fCH_amp[x][y];
        const TAxis* ox = o->GetXaxis();
        TH1F_Ref* h = new TH1F_Ref(grp.name + o->GetName(), grp.name + o->GetTitle(), ox->GetNbins(), ox->GetXmin(),ox->GetXmax(), fRateCounterCalibration);
        h->SetReference(fReferenceDirectory);
        grp.fCH_amp[x][y] = h;
        
        AddHistogram(grp.fCH_amp[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        grp.fCH_rawshape[x][y] = (TH2F*)CloneWithPrefix(fCH_rawshape[x][y], grp.name);
        AddHistogram(grp.fCH_rawshape[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        grp.fCH_vs_spill[x][y] = (TProfile*)CloneWithPrefix(fCH_vs_spill[x][y], grp.name);
        AddHistogram(grp.fCH_vs_spill[x][y]);
      }
    
    for (int x = 0; x < fNx; x++)
      for (int y = 0; y < fNy; y++) {
        grp.fCH_vs_event[x][y] = (TProfile*)CloneWithPrefix(fCH_vs_event[x][y], grp.name);
        AddHistogram(grp.fCH_vs_event[x][y]);
      }
  }
}


// return array of amplitudes of all cells
vector<float> PlaneCalo::GetLastEvent() const
{
  vector<float> v(fNch, 0);
  
  for (size_t i = 0; i < hits.size(); i++) {
    const CaloHit& h = hits[i];
    v[h.channel] = h.amp;
  }
  
  return v;
}

void PlaneCalo::EndEvent(const CS::DaqEvent &event, const EventFlags& flags)
{
  if (thr_flag) TThread::Lock();
  
  const uint32 spill = event.GetBurstNumber();
  const uint32 spill_event = event.GetEventNumberInBurst();
  //cout << "spill = " << spill << " event in spill = " << spill_event << endl;
  const CS::DaqEvent::Header head = event.GetHeader();
  const CS::DaqEvent::EventType eventType = head.GetEventType();
  
  // reset spill stability histogram on new spill
  if (spill != lastspill) {
    //cout << "reset spill stab histo: new spill = " << spill << " last spill = " << lastspill << endl;
    lastspill = spill;
    
    if (fExpertHistos) {
      for (int x = 0; x < fNx; x++)
        for (int y = 0; y < fNy; y++) {
          fCH_vs_event[x][y]->Reset();
          led.fCH_vs_event[x][y]->Reset();
          led0.fCH_vs_event[x][y]->Reset();
          led1.fCH_vs_event[x][y]->Reset();
          phys.fCH_vs_event[x][y]->Reset();
          beam.fCH_vs_event[x][y]->Reset();
          rnd.fCH_vs_event[x][y]->Reset();
        }
    }
  }
  
  /*
  const CS::uint32 typeAttr0 = head.GetTypeAttributes()[0] & 0xf8000000;
  cout << "PlaneCalo::EndEvent() EventType  = " << eventType
       << " TypeAttributes[0] = " << typeAttr0
       << endl;
  */
  
  hits.clear();
  
  for (const auto i : lDigits) {
    const CaloHit h = RecoWaveform(i);
    hits.push_back(h);
  }
  
  fNhits = hits.size();
  //if (fNhits >= fNch) return ???;
  
  /*
  cout << "PlaneCalo::EndEvent() "
       << " spill=" << spill << " event=" << spill_event
       << " flags:"
       << " isPhysics=" << flags.isPhysics
       << " isCalibration=" << flags.isCalibration
       << " isOnSpill=" << flags.isOnSpill
       << endl;
  */
  
  if (eventType == CS::DaqEvent::PHYSICS_EVENT) {
    FillPhysicsHistograms(spill,spill_event);
    if (flags.isTriggerPhys) FillGroupHistograms(phys, spill,spill_event);
    if (flags.isTriggerBeamOnly()) FillGroupHistograms(beam, spill,spill_event);
    if (flags.isTriggerRand) FillGroupHistograms(rnd, spill,spill_event);
  }
  
  if (eventType == CS::DaqEvent::CALIBRATION_EVENT) {
    FillGroupHistograms(led, spill,spill_event);
    if (flags.isOffSpill) FillGroupHistograms(led0, spill,spill_event);
    if (flags.isOnSpill) FillGroupHistograms(led1, spill,spill_event);
  }
  
  if (thr_flag) TThread::UnLock();
}

void PlaneCalo::FillGroupHistograms(HistoGroup& grp, uint32 spill,uint32 spill_event)
{
  double sumamp = 0;
  
  for (size_t i = 0; i < hits.size(); i++) {
    const CaloHit& h = hits[i];
    //cout << "hit x= " << h.x << " y= " << h.y << " ch= " << h.channel << " pass cuts = " << pass << " fNy= " << fNy << endl;
    
    sumamp += h.amp;
    
    if (fExpertHistos) {
      const int len = h.raw->size();

      for(int i=0; i<len; i++) {
        grp.fCH_rawshape[h.x][h.y]->Fill(i, (*h.raw)[i]);
      }
      
      grp.fCH_amp[h.x][h.y] ->Fill(h.amp);
      grp.fCH_vs_spill[h.x][h.y]->Fill(spill, h.amp);
      grp.fCH_vs_event[h.x][h.y]->Fill(spill_event, h.amp);
    }
    
    grp.fH_meanamp_xy->Fill(h.x, h.y, h.amp);
    grp.fH_meanamp_ch->Fill(h.channel, h.amp);
    grp.fH_amp_channel->Fill(h.channel, h.amp);
    
    fNhitsKept++;
  }
  
  grp.hSumamp->Fill(sumamp);
}

void PlaneCalo::FillPhysicsHistograms(uint32 spill,uint32 spill_event)
{
  double sumamp = 0;
  double ampmax = 0;
  double sumamp_nc = 0;
  double ampmax_nc = 0;
  int xampmax = -1;
  int yampmax = -1;
  
  // summs for center of mass calculation
  double Swx = 0;
  double Swy = 0;
  double Sw = 0;
  
  for (size_t i = 0; i < hits.size(); i++) {
    const CaloHit& h = hits[i];
    //cout << "hit x= " << h.x << " y= " << h.y << " ch= " << h.channel << " pass cuts = " << pass << " fNy= " << fNy << endl;
    
    // pedestall filling
    const double pedav = (h.ped0 + h.ped1) / 2;
    //cout << "pedav = " << pedav << endl;    
    fPed_sum->Fill(pedav);
    fPed_ch->Fill(h.channel, pedav);
    fmeanPed_ch->Fill(h.channel, pedav);
    fPed_xy->Fill(h.x, h.y, pedav);
    
    if (fExpertHistos) {
      const int len = h.raw->size();

      for(int i=0; i<len; i++) {
        fCH_rawshape[h.x][h.y]->Fill(i, (*h.raw)[i]);
        fCH_prof[h.x][h.y]->Fill(i, h.pulse(i));
      }
      
      fCH_time[h.x][h.y]->Fill(h.time);
      fCH_amp[h.x][h.y] ->Fill(h.amp);
      fCH_vs_spill[h.x][h.y]->Fill(spill, h.amp);
      fCH_vs_event[h.x][h.y]->Fill(spill_event, h.amp);
    }
    
    if (h.amp > ampmax_nc) ampmax_nc = h.amp;
    sumamp_nc += h.amp;
    
    // check cuts
    const bool pass = fVx->Test(h.x) && fVy->Test(h.y) && fVamp->Test(h.amp) && fVtime->Test(h.time);
    if (!pass) continue;
    
    Swx += h.amp * h.x;
    Swy += h.amp * h.y;
    Sw += h.amp;
    
    if (h.amp > ampmax)  {
      ampmax = h.amp;
      xampmax = h.x;
      yampmax = h.y;
    }
    
    sumamp += h.amp;
    
    fHch->Fill(h.channel);
    
    fH_meanamp_xy->Fill(h.x, h.y, h.amp);
    fHrc1->Fill(h.x, h.y);
    fHa->Fill(h.amp);
    fH_amp_channel->Fill(h.channel, h.amp);
    fH_meanamp_ch->Fill(h.channel, h.amp);
    
    fTime->Fill(h.time);
    fTime_ch->Fill(h.channel, h.time);
    fTime_xy->Fill(h.x, h.y, h.time);
    
    fAmpTime->Fill(h.time, h.amp);
    
    fVy->Store(h.x);
    fVx->Store(h.y);
    fVamp->Store(h.amp);
    fVtime->Store(h.time);
    fNhitsKept++;
  }
  
  hHits->Fill(fNhitsKept);
  hHits_nc->Fill(fNhits);
  
  if (fNhitsKept > 0) hSumamp->Fill(sumamp);
  if (fNhitsKept > 0) hMaxamp->Fill(ampmax);
  if (fNhits > 0) hSumamp_nc->Fill(sumamp_nc);
  if (fNhits > 0) hMaxamp_nc->Fill(ampmax_nc);
  
  if ((fNhitsKept > 0) && (Sw != 0)) {
    const double hitx = 0.5 + Swx/Sw;
    const double hity = 0.5 + Swy/Sw;
    fHitPos->Fill(hitx, hity);
    
    fHitPosX_vs_spill->Fill(spill, hitx);
    fHitPosY_vs_spill->Fill(spill, hity);
    
    const int bini = fHitPosLast->Fill(hitx, hity);
    lastbuf.push_back(bini);
  }
  
  // remove old entries from last hits histogram
  while (lastbuf.size() > LASTSIZE) {
    const int bini = lastbuf[0];
    lastbuf.pop_front();
    
    fHitPosLast->AddBinContent(bini, -1);
  }
  
  if (ampmax < fVamp->GetMin()) fHxyn->Fill(xampmax, yampmax);
}

CaloHit PlaneCalo::RecoWaveformSADC(const CS::ChipSADC::Digit* sadc) const
{

   //cout << "sadc  = " << sadc << endl;
   //if (sadc == 0) return;

   const std::vector<uint16>& raw = sadc->GetSamples();
   const int len = raw.size();

   const int x = sadc->GetX();
   const int y = sadc->GetY();
   const int channel = x + fNx*y;

   // calculate pedestals
   // sampling implemented by two ADCs with interliving to achive 2 times higher sampling rate
   // in result two pedestals should be calculated (for odd and even time samples)
   const int pedlen = min(8, len);
   double ped0 = 0;
   double ped1 = 0;

   for (int i = 0; i < pedlen; ++i) {
     double& p = (i % 2 == 0) ? ped0 : ped1;
     p += raw[i];
   }

   ped0 /= pedlen/2;
   ped1 /= pedlen/2;

   // calculate signal shape: pulse = raw - pedestal
   double pulse[len];

   for (int i = 0; i < len; i++) {
     const double& p = (i % 2 == 0) ? ped0 : ped1;
     pulse[i] = (raw[i] > p) ? (raw[i] - p) : 0;
   }

   // search for sample with maximum amplitude
   double max_amp = 0;
   int max_i = 0;
   for (int i = pedlen; i < len; i++) {
     const double& A = pulse[i];
     if (A > max_amp) {max_amp = A; max_i = i;}
   }

   CaloHit h;
   h.x = x;
   h.y = y;
   h.channel = channel;
   h.amp = max_amp;
   h.time = max_i;
   h.ped0 = ped0;
   h.ped1 = ped1;
   h.raw = &raw;
   h.isWB = 0;
   //cout << "amp = " << h.amp << " time = " << h.time << endl;

   return h;

}

CaloHit PlaneCalo::RecoWaveformWB(const CS::ChipNA64WaveBoard::Digit* wb) const
{

  //cout << "sadc  = " << sadc << endl;
    //if (sadc == 0) return;

    const std::vector<uint16>& raw = wb->GetSamples();
    const int len = raw.size();

    const int x = wb->GetX();
    const int y = wb->GetY();
    const int channel = x + fNx*y;


    // calculate pedestal
    const int pedlen = min(10, len);
    double ped = 0;
    for (int i = 0; i < pedlen; ++i) {
      ped += raw[i]/4;
    }

    ped /= pedlen;

    // calculate signal shape: pulse = -(raw - pedestal) ; - sign necessary since WB does not do any signal inversion.
    double pulse[len];
    for (int i = 0; i < len; i++) {
      pulse[i] = ped-raw[i]/4;
    }

    // search for sample with maximum amplitude
    double max_amp = 0;
    int max_i = 0;
    for (int i = pedlen; i < len; i++) {
      if (pulse[i] > max_amp) {max_amp = pulse[i]; max_i = i;}
    }

    CaloHit h;
    h.x = x;
    h.y = y;
    h.channel = channel;
    h.amp = max_amp; 
    h.time = max_i;
    h.ped0 = ped;
    h.ped1 = ped;
    h.raw = &raw;
    h.isWB = 1;

    //cout << "amp = " << h.amp << " time = " << h.time << endl;

    return h;
}

CaloHit PlaneCalo::RecoWaveform(CS::Chip::Digit* digit) const
{
  const CS::ChipSADC::Digit* sadc = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
  const CS::ChipNA64WaveBoard::Digit* wb = dynamic_cast<const CS::ChipNA64WaveBoard::Digit*>(digit);
  CaloHit h;

  if (sadc) h=RecoWaveformSADC(sadc);
  else if (wb) h=RecoWaveformWB(wb);

  return h;
}

void PlaneCalo::StoreDigit(CS::Chip::Digit* digit)
{
  // sanity checks
  const CS::ChipSADC::Digit* sadc = dynamic_cast<const CS::ChipSADC::Digit*>(digit);
  const CS::ChipNA64WaveBoard::Digit* wb = dynamic_cast<const CS::ChipNA64WaveBoard::Digit*>(digit);
  //cout << "sadc  = " << sadc << endl;
  if (!sadc && !wb) return;
  
  const int len = (wb==0 ? sadc->GetSamples().size() : wb->GetSamples().size());
  // cout << "raw.size() = " << len << endl;
  if (len < 1) return;
  
  const int x = (wb==0 ? sadc->GetX() : wb->GetX());
  const int y = (wb==0 ? sadc->GetY() : wb->GetY());
  if ((x < 0) || (y < 0) || (x >= fNx) || (y >= fNy)) return;
  
  lDigits.push_back(digit);
}

void PlaneCalo::ControlPanel(const TGWindow *p, const TGWindow *main)
{
  if (!fControlPanel) fControlPanel = new PlanePanel(p, main, 100, 100, this);
}

void PlaneCalo::Reset()
{
  Plane::Reset();
  hits.clear();
}

void PlaneCalo::ResetHistograms()
{
  Plane::ResetHistograms();
  lastbuf.clear();
}
