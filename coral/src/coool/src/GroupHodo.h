#ifndef __GroupHodo__
#define __GroupHodo__

#include "Group.h"
#include "PlaneCalo.h"

class GroupHodo : public Group {

private:

  const PlaneCalo* planeX;
  const PlaneCalo* planeY;

  TH1F* histoX;
  TH1F* histoY;
  TH2F* histoXY;

public:
  GroupHodo(const char* name);

  void Init();

#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
#endif

  //ClassDef(GroupHodo,0)
};

#endif
