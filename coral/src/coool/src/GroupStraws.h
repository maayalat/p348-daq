#ifndef __GroupStraws__
#define __GroupStraws__

#include "Group.h"
#include "TGraph.h"

namespace CS { class DaqEvent; }

class TH1F;
class TH2F;

class GroupStraws : public Group
{
  public:
                        GroupStraws             (const char* name);
    void                Init                    (void);
    double rtStraw(int time);
    #if !defined(__CINT__) && !defined(__CLING__)
    /// Fills the histograms
    void                EndEvent                (const CS::DaqEvent &event, const EventFlags &flags);
    #endif

    TH2F* 		fBeamImagery;	        //channel Cluster
    TH2F* 		fBeamImagery_onehit;	        //channel Cluster
    TH2F* 		fBeamImagery_rt;	        //channel Cluster
    TH2F*	    	fBeamImagery_rt_onehit;

    TH2F* 		fBeamImagery_phys;	        //channel Cluster
    TH2F* 		fBeamImagery_rt_phys;	        //channel Cluster
    TH2F* 		fBeamImagery_beam;	        //channel Cluster
    TH2F* 		fBeamImagery_rt_beam;	        //channel Cluster
     TH1F*               fChannels;  
 //    TH1F*               fChannel32;  
     TH1F*               fTime1;
     TH1F*               fTimeX;
     TH1F*               fTimeY;

     TH1F*               fTimeY28;
     TH1F*               fTimeY30;
     TH1F*               fTimeY32;

     TH1F*               fTimeX28;
     TH1F*               fTimeX30;
     TH1F*               fTimeX32;


  

//    TGraph* tg1;
    TProfile* histoX_vs_spill;
    TProfile* histoY_vs_spill;
    TProfile* histoEff_vs_spill;
    TH2F* 		fChannelDoubleClustersX;	        //channel Cluster
    TH2F* 		fChannelDoubleClustersY;	        //channel Cluster

    
  ClassDef              (GroupStraws,1)
};

#endif
