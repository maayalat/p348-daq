#ifndef __GroupNA64__
#define __GroupNA64__

#include "Group.h"
#include "Plane.h"
#include "PlaneMM.h"
#include "PlaneGEM.h"
#include "PlaneStrawTubes.h"

class GroupNA64 : public Group {
 private:
  std::vector<int> vType;

 public:

 GroupNA64(const char* name):Group(name){}

  void Init();
#if !defined(__CINT__) && !defined(__CLING__)
  void EndEvent(const CS::DaqEvent &event);
#endif
  //  ClassDef(GroupNA64,0);
};

#endif
