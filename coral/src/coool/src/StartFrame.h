#ifndef __StartFrame__
#define __StartFrame__

#include <TROOT.h>
#include <TApplication.h>
#include <TVirtualX.h>

#include <RQ_OBJECT.h>
#include <TGFrame.h>
#include <TGButton.h>
#include <TGFileDialog.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGPicture.h>

#include <vector>
#include <string>

#include "MainFrame.h"


/*! \brief Class corresponding to the start dialog window

    The start button is connected to MainFrame::Init(),
    where the StartFrame window is unmapped, and not deleted.

    \todo Handle root output file name

    \author Colin Bernet
*/

class StartFrame : public TObject {


 private:

  /// Associated window
  TGTransientFrame    *fMain;

  /// Geometry file name
  std::string fGeomFile;

  /// mapping file name
  std::string fMapFile;

  /// data file name. Streams are handled
  std::vector<std::string> fDataFile;

  /// root file name
  std::string fRootFile;

  /// groups description file name
  std::string fGroupFile;

  /// default parameters file name
  std::string fParamFile;

  /// user configuration file name
  std::string fConfigFile;

  /// reference file name
  std::string fReferenceFile;

  /// MainFrame object this belongs to
  MainFrame *fMainframe;

  /// Monitor instance of the MainFrame -2b removed-
  Monitor *fMonitor;

  TGGroupFrame *fCheckFrame,*fTaskFrame,*fInputFrame,*fDecodingFrame;
  TGCompositeFrame *fDecodingFrameDown, *fDecodingFrameDown2;
  TGCompositeFrame *fRawFrame, *fMapFrame, *fGeomFrame, *fDownFrame, *fUpFrame;
  TGLayoutHints *fL1,*fL2, *fL3, *fL4, *fL5;
  TGLabel *fRawLabel, *fMapLabel, *fGeomLabel;
  TGButton *fRawButton, *fMapButton, *fGeomButton, *fStartButton;
  std::vector<TGCheckButton*> fCheckButton;

  TGCheckButton          *fCheckTree;
  TGCheckButton          *fCheckText;
  TGCheckButton          *fCheckTrack;
  TGCheckButton          *fCheckCluster;
  TGCheckButton          *fCheckRef;
  TGCheckButton          *fCheckCalib;
  TGCheckButton          *fCheckReadFailedEvt;
  TGCheckButton          *fCheckExpert;

  TGVerticalFrame        *fMonDetFrame;
  TGVerticalFrame        *fAllNoneFrame;
  //TGButton               *fDefButton;
  TGButton               *fAllButton;
  TGButton               *fNoneButton;
  TGButton               *fSetupButton;

  TGTextEntry            *fEvtMax;
  TGLabel                *fEvtLabel;

  TGTextEntry            *fMinSpacingEntry;
  TGLabel                *fMinSpacingLabel;

  TGTextEntry            *fRefEntry;
  TGLabel                *fRefLabel;

  /// file types for mapping files
  static const char* maptype[];

  /// file types for data files
  static const char* datatype[];

  /// file types for geometry files
  static const char* geomtype[];

 public:
  StartFrame(const TGWindow *p,MainFrame *main,
	     UInt_t w, UInt_t h);
  virtual ~StartFrame();

  // hide window immediately
  void Hide() { fMain->UnmapWindow(); gSystem->ProcessEvents(); }
  
  virtual void Start()  { Hide(); Emit("Start()"); }   // *SIGNAL*


  //Slots

  /// receives signal from fMain->SendCloseMessage()
  void CloseWindow() {
    exit(1);
  }

  /// All basic TGButton's connected to this slot
  void HandleButtons(int id=-1);

  /// TGCheckButton's connected to this slot
  void CheckedDet(Bool_t);

  /// fCheckTree connected to this slot
  void CheckedTree(Bool_t);

  /// fCheckText connected to this slot
  void CheckedText(Bool_t);

  /// fCheckTrack connected to this slot
  void CheckedTrack(Bool_t);

  /// fCheckCluster connected to this slot
  void CheckedCluster(Bool_t);

  /// fCheckRef connected to this slot
  void CheckedRef(Bool_t);

  /// fCheckCalib connected to this slot
  void CheckedCalib(Bool_t);

  /// fCheckReadFailedEvt connected to this slot
  void CheckedReadFailedEvt(Bool_t);

  /// fCheckExpert connected to this slot
  void CheckedExpert(Bool_t);

  /// fDefButton connected to this slot
  void DefClicked();

  /// fAllButton connected to this slot
  void AllClicked();

  /// fNoneButton connected to this slot
  void NoneClicked();

  /// \return root file name
  std::string& GetRootFile() {return fRootFile;}
  /// \return data file name
  std::vector<std::string>& GetDataFile() {return fDataFile;}
  /// \return mapping file name
  std::string& GetMapFile() {return fMapFile;}
  /// \return geometry file name
  std::string& GetGeomFile() {return fGeomFile;}
  /// \return groups description file name
  std::string& GetGroupFile() {return fGroupFile;}
  /// \return parameters file name
  std::string& GetParamFile() {return fParamFile;}
  /// \return configuration file name
  std::string& GetConfigFile() {return fConfigFile;}
  /// \return reference file name if any
   std::string& GetReferenceFile() {return fReferenceFile;}
  /// \return number of events to be decoded
  int GetNEvent() {
    return atoi(fEvtMax->GetText());
  }
  /// \return minimum spacing between two decoded events in one spill
  int GetMinEventSpacing() {
    return atoi(fMinSpacingEntry->GetText());
  }
  /// \return return reference name entry
  const char* GetRefEntry() {
    return fRefEntry->GetText();
  }

#if ROOT_VERSION_CODE >= ROOT_30200
  RQ_OBJECT("StartFrame")
#else
  RQ_OBJECT()
#endif

  ClassDef(StartFrame,0)
};


#endif

