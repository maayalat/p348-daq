#pragma once

#include "Plane.h"
#include <vector>

class TProfile;

// class for CAEN V260 counters

class PlaneV260 : public  Plane {
private:

  static const int fNchan;

  TProfile* hCounters;
  TProfile* hCounters_vs_spill[16];
  
public:
  
  PlaneV260(const char *detname, int nch);
  
#if !defined(__CINT__) && !defined(__CLING__)
  void StoreDigit(CS::Chip::Digit* digit);
  void EndEvent(const CS::DaqEvent &event);
#endif
  
  void Init(TTree* tree =0);
  
  void ControlPanel(const TGWindow* p, const TGWindow* main); 
  
  ClassDef(PlaneV260,0)
};
