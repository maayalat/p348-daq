#include "GroupHodo.h"

#include "TThread.h"

//ClassImp(GroupHodo);

GroupHodo::GroupHodo(const char* name)
: Group(name),
  planeX(0), planeY(0),
  histoX(0), histoY(0), histoXY(0)
{}

void GroupHodo::Init()
{
  const size_t n = fPlanes.size();
  if (n != 2) {
    cout << "GroupHodo::Init(): name = " << GetName() << ", #planes = " << n << endl;
    //cout << "WARNING GroupHodo::Init(): number of planes is not two" << endl;
  }

  // search X and Y planes of hodoscope
  for (size_t i = 0; i < n; ++i) {
    const PlaneCalo* p = dynamic_cast<const PlaneCalo*>(fPlanes[i]);
    if (!p) continue;
    
    const TString name = p->GetName();
    if (name.Contains("X")) planeX = p;
    if (name.Contains("Y")) planeY = p;
  }
  
  if (planeX) {
    const TString name = planeX->GetName();
    const int lenX = 1 + 2*planeX->GetNch();
    histoX = new TH1F(name, name + ";X", lenX, 0, lenX);
    AddHistogram(histoX);
  }
  
  if (planeY) {
    const TString name = planeY->GetName();
    const int lenY = 1 + 2*planeY->GetNch();
    histoY = new TH1F(name, name + ";Y", lenY, 0, lenY);
    AddHistogram(histoY);
  }
  
  if (planeX && planeY) {
    const TString name = planeX->GetName() + TString(":") + planeY->GetName();
    const int lenX = 1 + 2*planeX->GetNch();
    const int lenY = 1 + 2*planeY->GetNch();
    histoXY = new TH2F(name, name + ";X;Y", lenX, 0, lenX, lenY, 0, lenY);
    histoXY->SetOption("LEGO2 0");
    AddHistogram(histoXY);
  }
}

vector<int> resolveHodo(const vector<float>& e, const float cut)
{
  vector<int> hit;
  
  //cout << "resolveHodo " << e.size() << " : ";
  //for (size_t i = 0; i < e.size(); ++i) cout << e[i] << " " ;
  //cout << endl;
  
  for (size_t i = 0; i < e.size(); ++i) {
    // hits in prev, curr, next cells of hodoscope
    const bool prev = (i > 0 && e[i-1] > cut);
    const bool curr = (e[i] > cut);
    const bool next = (i + 1 < e.size() && e[i+1] > cut);
    
    if (curr && next) hit.push_back(2*(i + 1));
    if (curr && !prev && !next) hit.push_back(2*i + 1);
  }
  
  return hit;
}

void GroupHodo::EndEvent(const CS::DaqEvent &event)
{
  if (thr_flag) TThread::Lock();
  
  vector<int> hitx;
  if (planeX) hitx = resolveHodo(planeX->GetLastEvent(), planeX->GetVampMin());
  for (size_t i = 0; i < hitx.size(); ++i) histoX->Fill(hitx[i]);
  
  vector<int> hity;
  if (planeY) hity = resolveHodo(planeY->GetLastEvent(), planeY->GetVampMin());
  for (size_t j = 0; j < hity.size(); ++j) histoY->Fill(hity[j]);
  
  for (size_t i = 0; i < hitx.size(); ++i)
    for (size_t j = 0; j < hity.size(); ++j)
      histoXY->Fill(hitx[i], hity[j]);
  
  if (thr_flag) TThread::UnLock();
}
