#
# CsVertex Library
#

file (GLOB SRC "Cs*.cc")
add_library(CsVertex ${SRC})

#
# VrtKalman Library
#

file (GLOB SRC "Kalman/*.cc")
add_library(VrtKalman ${SRC})
if(NOT BUILD_SHARED_LIBS)
	target_link_libraries(VrtKalman Trafdic)
endif()
#
# VrtRoland Library
#

file (GLOB SRC "Roland/*.cc")
add_library(VrtRoland ${SRC})

#

#
# Install headers
#
file (GLOB HEADER "*.h")
install(FILES ${HEADER} DESTINATION ./include/)
