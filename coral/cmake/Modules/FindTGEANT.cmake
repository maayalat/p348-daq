################################################################################
#    Copyright (C) 2014 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH    #
#                                                                              #
#              This software is distributed under the terms of the             #
#         GNU Lesser General Public Licence version 3 (LGPL) version 3,        #
#                  copied verbatim in the file "LICENSE"                       #
################################################################################
# - Try to find TGEANT
# Once done this will define
#
#  TGEANT_FOUND - system has TGEANT
#### (not needed)  TGEANT_INCLUDE_DIR - the TGEANT include directory
#### (not needed)  TGEANT_INCLUDE_DIRS - the TGEANT include directory
#  TGEANT_LIBRARIES - The libraries needed to use TGEANT
#### (not needed)  TGEANT_DEFINITIONS - Compiler switches required for using TGEANT
#

FIND_PATH(TGEANT_LIBRARY_DIR NAMES libT4Event.so libT4Settings.so PATHS
    $ENV{TGEANT}/lib
    $ENV{TGEANT_DIR}/lib
 NO_DEFAULT_PATH
)

if (TGEANT_LIBRARY_DIR)
  set(TGEANT_FOUND TRUE)
  get_filename_component(TGEANT ${TGEANT_LIBRARY_DIR} DIRECTORY)
  file(GLOB TGEANT_LIBRARIES "${TGEANT}/lib/libT4*.so")
  set(TGEANT_INCLUDE_DIR "${TGEANT}/include/")
endif()

if (TGEANT_FOUND)
    if (NOT TGEANT_FIND_QUIETLY)
        MESSAGE(STATUS "Looking for TGEANT... - found ${TGEANT}")
    endif()
else()
     if (TGEANT_FIND_REQUIRED)
       message(FATAL_ERROR "Looking for TGEANT... - Not found (define TGEANT environment variable)")
     else()
       message(STATUS "Looking for TGEANT... - Not found (define TGEANT environment variable)")
     endif()
endif()
