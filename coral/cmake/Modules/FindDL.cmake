# try to find DL
#
# output variables
#   DL_LIBRARY
#   DL_LIBRARY_DIR
#   DL_INCLUDE_DIR

macro(_FIND_DL)
  message(STATUS "Checking for DL")

  set(DL_LIBRARY)
  set(DL_LIBRARY_DIR)
  set(DL_INCLUDE_DIR)

  find_library(_dllib dl PATHS
    ${DL_DIR}/usr/lib
    ${DL_DIR}/lib
    ${DL_DIR}
    NO_DEFAULT_PATH
  )
  find_library(_dllib dl)
  if(NOT _dllib STREQUAL "_dllib-NOTFOUND")
    get_filename_component(DL_LIBRARY_DIR ${_dllib} PATH)
    get_filename_component(DL_LIBRARY     ${_dllib} NAME_WE)
    string(REGEX REPLACE "^lib" "" DL_LIBRARY ${DL_LIBRARY})
  endif(NOT _dllib STREQUAL "_dllib-NOTFOUND")

  find_file(_dlinc dlfcn.h PATHS
    ${DL_DIR}/usr/include
    ${DL_DIR}/include
    ${DL_DIR}
    NO_DEFAULT_PATH
  )
  find_file(_dlinc dlfcn.h)
  if(NOT _dlinc STREQUAL "_dlinc-NOTFOUND")
    get_filename_component(DL_INCLUDE_DIR ${_dlinc} PATH)
  endif(NOT _dlinc STREQUAL "_dlinc-NOTFOUND")

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(DL DEFAULT_MSG DL_LIBRARY DL_LIBRARY_DIR DL_INCLUDE_DIR)
endmacro(_FIND_DL)

if(NOT DEFINED DL_FOUND)
  _FIND_DL()
  if(DL_FOUND)
    set(DL_FOUND       ${DL_FOUND}       CACHE INTERNAL "Found DL")
    set(DL_LIBRARY     ${DL_LIBRARY}     CACHE INTERNAL "DL libraries")
    set(DL_LIBRARY_DIR ${DL_LIBRARY_DIR} CACHE INTERNAL "DL library directory")
    set(DL_INCLUDE_DIR ${DL_INCLUDE_DIR} CACHE INTERNAL "DL include directory")
  endif(DL_FOUND)
endif(NOT DEFINED DL_FOUND)
