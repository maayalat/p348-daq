################################################################################
#    Copyright (C) 2014 GSI Helmholtzzentrum fuer Schwerionenforschung GmbH    #
#                                                                              #
#              This software is distributed under the terms of the             #
#         GNU Lesser General Public Licence version 3 (LGPL) version 3,        #
#                  copied verbatim in the file "LICENSE"                       #
################################################################################
# - Try to find PHAST
# Once done this will define
#
#  PHAST_FOUND - system has PHAST
#### (not needed)  PHAST_INCLUDE_DIR - the PHAST include directory
#### (not needed)  PHAST_INCLUDE_DIRS - the PHAST include directory
#  PHAST_LIBRARIES - The libraries needed to use PHAST
#### (not needed)  PHAST_DEFINITIONS - Compiler switches required for using PHAST
#

FIND_PATH(PHAST_LIBRARY_DIR NAMES libPhast.so PATHS
 $ENV{PHAST}/lib
 ${PHAST}/lib
 NO_DEFAULT_PATH
)

if (PHAST_LIBRARY_DIR)

  set(PHAST_FOUND TRUE)
  get_filename_component(PHAST ${PHAST_LIBRARY_DIR} DIRECTORY)

  if(EXISTS ${PHAST}/lib/libPhast.so)
      list(APPEND PHAST_LIBRARIES ${PHAST}/lib/libPhast.so)
      message(STATUS "PHAST main library found.. ${PHAST}/user/libPhast.so")
  endif()

  if(EXISTS ${PHAST}/user/libUser.so)
      list(APPEND PHAST_LIBRARIES ${PHAST}/user/libUser.so)
      message(STATUS "PHAST user library found.. ${PHAST}/user/libUser.so")
  endif()

  if(EXISTS ${PHAST}/src/libMisc.so)
      list(APPEND PHAST_LIBRARIES ${PHAST}/src/libMisc.so)
      message(STATUS "PHAST misc library found.. ${PHAST}/src/libMisc.so")
  endif()

  if(EXISTS ${PHAST}/graph/libGraph.so)
      set(WITH_GRAPH 1)
      list(APPEND PHAST_LIBRARIES ${PHAST}/graph/libGraph.so)
      message(STATUS "PHAST graphics library found.. ${PHAST}/graph/libGraph.so")
  else()
	message(STATUS "libGraph.so not found in ${PHAST}/graph/libGraph.so")
  endif()

  list(APPEND PHAST_LIBRARY_DIR ${PHAST_LIBRARY_DIR}/../user ${PHAST_LIBRARY_DIR}/../src)
endif()

if (PHAST_FOUND)

    if (NOT PHAST_FIND_QUIETLY)
        MESSAGE(STATUS "Looking for PHAST... - found ${PHAST}")
    endif()

else()
     if (PHAST_FIND_REQUIRED)
       message(FATAL_ERROR "Looking for PHAST... - Not found (define PHAST environment variable)")
     else()
       message(STATUS "Looking for PHAST... - Not found (define PHAST environment variable)")
     endif()
endif()
