execute_process(COMMAND date
  OUTPUT_VARIABLE _output OUTPUT_STRIP_TRAILING_WHITESPACE)
file(WRITE ${SED_SCRIPT} "{
  s%C2OPT_HEADER%// \"${TARGET}\" file created from \"${SOURCE}\", on ${_output}%
  /^# 1/ d
  /^#/ s/.*//
  /^\\/\\//! s%\\([^/ ] \\)//%\\1      //%
}")

